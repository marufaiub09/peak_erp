<?php
header('Content-type:text/html; charset=utf-8');
session_start();
include('../../../includes/common.php');

$user_id = $_SESSION['logic_erp']["user_id"];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];
//--------------------------------------------------------------------------------------------
//load drop down knitting company
if ($action=="load_drop_down_knit_com")
{
	$exDataArr = explode("**",$data);	
	$knit_source=$exDataArr[0];
	$company=$exDataArr[1];
	$issuePurpose=$exDataArr[2];
	if($company=="" || $company==0) $company_cod = ""; else $company_cod = " and id=$company";
	if($knit_source==1)
		echo create_drop_down( "cbo_dyeing_company", 170, "select id,company_name from lib_company where status_active=1 and is_deleted=0 order by company_name","id,company_name", 1, "-- Select --", $company, "" );
	else if($knit_source==3 && $issuePurpose==1)
		echo create_drop_down( "cbo_dyeing_company", 170, "select a.id,a.supplier_name from lib_supplier a, lib_supplier_party_type b where a.id=b.supplier_id and b.party_type in(1,9,20) and a.status_active=1 group by a.id,a.supplier_name order by a.supplier_name","id,supplier_name", 1, "-- Select --", 0, "",0 );
	else if($knit_source==3 && $issuePurpose==2)
		echo create_drop_down( "cbo_dyeing_company", 170, "select a.id,a.supplier_name from lib_supplier a, lib_supplier_party_type b where a.id=b.supplier_id and b.party_type in(1,9,21,24) and a.status_active=1 group by a.id,a.supplier_name order by a.supplier_name","id,supplier_name", 1, "-- Select --", 0, "",0 );
	else if($knit_source==3)		
		echo create_drop_down( "cbo_dyeing_company", 170, "select a.id,a.supplier_name from lib_supplier a, lib_supplier_party_type b where a.id=b.supplier_id and a.status_active=1 group by a.id,a.supplier_name order by a.supplier_name","id,supplier_name", 1, "-- Select --", 0, "",0 );
	else
		echo create_drop_down( "cbo_dyeing_company", 170, $blank_array,"", 1, "-- Select --", 0, "",0 );
	exit();	
}


//load drop down store
if ($action=="load_drop_down_store")
{	  
	echo create_drop_down( "cbo_store_name", 170, "select a.id, a.store_name from lib_store_location a, lib_store_location_category b where a.id= b.store_location_id and a.company_id='$data' and b.category_type=13 and a.status_active=1 and a.is_deleted=0 group by a.id, a.store_name order by a.store_name","id,store_name", 1, "-- Select Store --", 0, "",0 ); 
	exit();
}

//load drop down Color
if ($action=="load_drop_down_color")
{
	$data=explode("_",$data);
	$all_po_id_or_booking_no=$data[0];
	$issue_purpose=$data[1];
	$color_id=$data[2];
	//echo $data[1];
	if($issue_purpose==8)
	{
		$sql="select c.id, c.color_name from wo_non_ord_samp_booking_dtls b, lib_color c where b.fabric_color=c.id and b.booking_no='$all_po_id_or_booking_no' and b.status_active=1 and b.is_deleted=0 group by c.id, c.color_name";
	}
	else
	{
		$sql="select c.id, c.color_name from wo_booking_mst a, wo_booking_dtls b, lib_color c where a.booking_no=b.booking_no and b.fabric_color_id=c.id and b.po_break_down_id in($all_po_id_or_booking_no) and a.item_category=2 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by c.id, c.color_name";	 
	}
	
	echo create_drop_down( "cbo_color_id", 170, $sql,"id,color_name", 0, "-- Select Color --", $color_id, "",0 );
	exit();
}

if ($action=="is_roll_maintain")
{	  
	$sql = sql_select("select variable_list, fabric_roll_level, batch_maintained from variable_settings_production
			where company_name=$data and variable_list in (3,13) and status_active=1 and is_deleted=0"); 
				
	//echo $sql;
	echo "$('#hidden_is_roll_maintain').val(0);\n";
	echo "$('#hidden_is_batch_maintain').val(0);\n";
	foreach($sql as $key=>$val)
	{
		//if($val[csf('variable_list')]==3) echo "$('#hidden_is_roll_maintain').val(".$val[csf("fabric_roll_level")].");\n";
 		if($val[csf('variable_list')]==13) echo "$('#hidden_is_batch_maintain').val(".$val[csf("batch_maintained")].");\n";
	}	
	//echo "new_item_controll();\n";
	exit();
}
//load drop down supplier
if ($action=="load_drop_down_supplier")
{	  
	echo create_drop_down( "cbo_supplier", 170, "select DISTINCT(c.supplier_name),c.id from  lib_supplier_tag_company a,lib_supplier_party_type b, lib_supplier c where c.id=b.supplier_id and a.supplier_id = b.supplier_id and a.tag_company=$data and b.party_type in (1,20,90) and c.status_active=1 and c.is_deleted=0","id,supplier_name", 1, "-- Select --", 0, "",0 );  	 
	exit();
}

//batch pop up here-------------//
if ($action=="batch_popup")
{
  	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	extract($_REQUEST);  
   	 
?>
     
<script>
	
	function js_set_value(id,batchNo,batchColor)
	{		
		$("#txt_batch_id").val(id);
		$("#txt_batch_no").val(batchNo);
		$("#txt_batch_color").val(batchColor);
		parent.emailwindow.hide();
 	}
</script>
</head>
<body>
<div align="center" style="width:100%;" >
<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
	<table width="600" cellspacing="0" cellpadding="0" border="1" rules="all" class="rpt_table" align="center">
            <thead>
                <tr>                	 
                    <th width="150">Search By</th>
                    <th width="150" align="center" id="search_by_td_up">Enter Batch No</th>                    
                    <th><input type="reset" name="re_button" id="re_button" value="Reset" style="width:100px" class="formbutton"  /></th>           
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td align="center">
                        <?php  
                            $sql="select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company=$cbo_company_id $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name";
							//echo $sql;
							//$search_by = array(1=>'Batch No', 2=>'Buyer');
							$search_by = array(1=>'Batch No');
							$dd="change_search_event(this.value, '0*1', '0*".$sql."', '../../../')";
							echo create_drop_down("cbo_search_by", 120, $search_by, "", 0, "--Select--", "", $dd, 0);
                        ?>
                    </td>
                    <td width="180" align="center" id="search_by_td"> 
                        <input type="text" id="txt_search_common" name="txt_search_common" class="text_boxes" style="width:100px" />
                    </td>                    
                     <td align="center">
                        <input type="button" name="btn_show" class="formbutton" value="Show" style="width:100px;" onClick="show_list_view ( document.getElementById('cbo_search_by').value+'_'+document.getElementById('txt_search_common').value+'_'+'<?php echo $cbo_company_id; ?>', 'create_batch_search_list', 'search_div', 'grey_fabric_issue_controller', 'setFilterGrid(\'view\',-1)');" />				
                    	
                        <!-- Hidden field here-------->
                        <input type="hidden" id="txt_batch_id" value="" /> 
                        <input type="hidden" id="txt_batch_no" value="" />
                        <input type="hidden" id="txt_batch_color" value="" />
                        <!-- ---------END------------->
                    </td>
            	</tr>  
                 
            </tbody>
         </tr>         
        </table>    
        <div align="center" valign="top" id="search_div"> </div> 
        </form>
   </div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}

if($action=="create_batch_search_list")
{ 
 	$ex_data = explode("_",$data);
	$txt_search_by = $ex_data[0];
	$txt_search_common = $ex_data[1];
 	$company = $ex_data[2]; // company id
 
  	$cond="";
  	if($txt_search_by==1 && $txt_search_common!="")
		$cond = " and a.batch_no like '%$txt_search_common%'"; 
	if($txt_search_by==2 && $txt_search_common!=0)
		$cond = " and a.buyer_name='$txt_search_common'"; 
 	
	$color_arr = return_library_array( "select id, color_name from lib_color",'id','color_name');
	if($db_type==0)
	{
		$sql = "select a.id, a.batch_no, a.booking_no, a.color_id, group_concat(c.po_number) as po_number
				from 
					pro_batch_create_mst a, pro_batch_create_dtls b left join wo_po_break_down c on b.po_id=c.id
				where  
					a.id=b.mst_id and a.company_id=$company and a.entry_form=0 $cond
					group by a.id, a.batch_no, a.booking_no, a.color_id 
					order by a.batch_no";
	}
	else
	{
		$sql = "select a.id, a.batch_no, a.booking_no, a.color_id, LISTAGG(c.po_number, ',') WITHIN GROUP (ORDER BY c.id) as po_number
				from 
					pro_batch_create_mst a, pro_batch_create_dtls b left join wo_po_break_down c on b.po_id=c.id
				where  
					a.id=b.mst_id and a.company_id=$company and a.entry_form=0 $cond
					group by a.id, a.batch_no, a.booking_no, a.color_id 
					order by a.batch_no";	
	}
 	//echo $sql;die;
	$result = sql_select($sql);
	
  	?> 
    <div align="left" style="margin-left:50px; margin-top:10px">    
    <table cellspacing="0" cellpadding="0" border="1" rules="all" width="750" class="rpt_table" >
        <thead>
            <th width="50">SL</th>
            <th width="100">Batch No</th>
            <th width="100">Color</th>               
            <th width="120">Booking No</th>
            <th width="250">Po Number</th>
        </thead>
	</table>
    
	<div style="width:770px; max-height:250px; overflow-y:scroll" id="container_batch" >	 
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="750" class="rpt_table" id="view">  
        <?php
            $i=1;
            $previouDataRow="";
			foreach ($result as $row)
            {  
                if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF"; 
          		?>
                <tr bgcolor="<?php echo $bgcolor; ?>" style="cursor:pointer" onClick="js_set_value(<?php echo $row[csf('id')];?>,'<?php echo $row[csf('batch_no')];?>','<?php echo $color_arr[$row[csf('color_id')]]; ?>')"  > 
                    <td width="50"><?php echo $i; ?>  </td>  
                    <td width="100"><p><?php echo $row[csf('batch_no')]; ?></p></td>
                    <td width="100"><p><?php echo $color_arr[$row[csf('color_id')]]; ?></p></td>               
                    <td width="120"><p><?php echo $row[csf('booking_no')]; ?>&nbsp;</p></td>
                    <td width="250"><p><?php echo $row[csf('po_number')]; ?>&nbsp;</p></td> 
                 </tr> 
				<?php 
                $i++;
            }
        	?> 
        </table>        
      </div>
    </div>
	<?php	
	exit();	
}

if($action=="job_no_for_duplication_check")
{
	$data=explode("**",$data);
	$update_id=$data[0];
	$dtls_id=$data[1];
	
	if($dtls_id=="") $dtls_id_cond=""; else $dtls_id_cond=" and i.id!=$dtls_id";
	
	echo "select b.job_no_mst as job_no from inv_grey_fabric_issue_dtls i, order_wise_pro_details a, wo_po_break_down b where i.id=a.dtls_id and a.po_breakdown_id=b.id and i.mst_id=$update_id and a.entry_form=16 and a.trans_type=2 and a.status_active=1 and a.is_deleted=0 $dtls_id_cond";
	
	$job_no=return_field_value("b.job_no_mst as job_no","inv_grey_fabric_issue_dtls i, order_wise_pro_details a, wo_po_break_down b","i.id=a.dtls_id and a.po_breakdown_id=b.id and i.mst_id=$update_id and a.entry_form=16 and a.trans_type=2 and a.status_active=1 and a.is_deleted=0 $dtls_id_cond","job_no");
	echo $job_no;
	exit();
}	 

// wo/pi popup here----------------------// 
if ($action=="fabbook_popup")
{
  	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	extract($_REQUEST);  
?>
     
<script>
	var update_id='<?php echo $update_id; ?>';
	var dtls_tbl_id='<?php echo $dtls_tbl_id; ?>';
	function fn_check()
	{
		show_list_view ( document.getElementById('cbo_buyer_name').value+'_'+document.getElementById('cbo_search_by').value+'_'+document.getElementById('txt_search_common').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+<?php echo $company; ?>+'_'+<?php echo $issue_purpose; ?>+'_'+<?php echo $cbo_basis; ?>, 'create_fabbook_search_view', 'search_div', 'grey_fabric_issue_controller', 'setFilterGrid(\'view\',-1)');
	}
		
	function js_set_value(booking_dtls,curr_job_no,check_or_not)
	{ 
		if(check_or_not==1)
		{
			if(update_id!="")
			{
				var job_no = trim(return_global_ajax_value(update_id+"**"+dtls_tbl_id, 'job_no_for_duplication_check', '', 'grey_fabric_issue_controller'));
				if(job_no!="")
				{
					if(job_no!==curr_job_no)
					{
						alert("Job Mix Not Allowed");	
						return;
					}
				}
			}
			//alert(dtls_tbl_id+"**"+curr_job_no);
		}
		
		$("#hidden_booking_number").val(booking_dtls); 
		parent.emailwindow.hide();
	}
</script>

</head>

<body>
<div align="center" style="width:100%;" >
<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
	<table width="850" cellspacing="0" cellpadding="0" border="1" class="rpt_table" align="center" rules="all">
            <thead>
                <tr>                	 
                    <th>Buyer Name</th>
                    <th>Search By</th>
                    <th align="center" id="search_by_td_up">Enter <?php if($cbo_basis==3) echo "Program"; else "Booking"; ?> No </th>
                    <th width="200">Date Range</th>
                    <th><input type="reset" name="re_button" id="re_button" value="Reset" style="width:100px" class="formbutton"  /></th>           
                </tr>
            </thead>
            <tbody>
                <tr class="general">
                    <td>
                        <?php  
						   echo create_drop_down( "cbo_buyer_name", 150, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$company' $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,30,90)) order by buy.buyer_name","id,buyer_name", 1, "-- Select --", $selected, "" );
                        ?>
                    </td>
                    <td>
                        <?php  
							if($cbo_basis==1)
							{
                            	$search_by = array(1=>'Booking No', 2=>'Buyer Order', 3=>'Job No');
							}
							else
							{
								$search_by = array(1=>'Booking No', 2=>'Buyer Order', 3=>'Job No', 4=>'Program No');	
							}
							$dd="change_search_event(this.value, '0*0*0*0', '0*0*0*0', '../../../')";
							echo create_drop_down( "cbo_search_by", 130, $search_by, "", 0, "-- --", "4", $dd, 0);
                        ?>
                    </td>
                     <td width="180" align="center" id="search_by_td">				
                        <input type="text" style="width:130px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />	
                    </td>    
                    <td align="center">
                        <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px" placeholder="From Date" />
                        <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px" placeholder="To Date" />
                     </td> 
                     <td align="center">
                        <input type="button" name="btn_show" class="formbutton" value="Show" onClick="fn_check()" style="width:100px;" />				
                    </td>
            </tr>
        	<tr>                  
            	<td align="center" height="40" valign="middle" colspan="6">
					<?php echo load_month_buttons(1);  ?>
                    <!-- Hidden field here-------->
                    <input type="hidden" id="hidden_booking_number" value="" />
                    <!-- ---------END------------->
                </td>
            </tr>    
            </tbody>
         </tr>         
        </table>    
        <div align="center" valign="top" id="search_div"> </div> 
        </form>
   </div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}

if($action=="create_fabbook_search_view")
{
 	$ex_data = explode("_",$data);
	$buyer = $ex_data[0];
	$txt_search_by = $ex_data[1];
	$txt_search_common = trim($ex_data[2]);
	$txt_date_from = $ex_data[3];
	$txt_date_to = $ex_data[4];
	$company = $ex_data[5];
	$booking_type = $ex_data[6];
	$cbo_basis=$ex_data[7];
	
 	if($cbo_basis==3)
	{
		$po_array=array();
		$po_sql=sql_select("select a.id, a.job_no_mst, a.po_number, b.style_ref_no from wo_po_break_down a, wo_po_details_master b where a.job_no_mst=b.job_no");
		foreach($po_sql as $row)
		{
			$po_array[$row[csf('id')]]['no']=$row[csf('po_number')];
			$po_array[$row[csf('id')]]['job_no']=$row[csf('job_no_mst')]; 
			$po_array[$row[csf('id')]]['style']=$row[csf('style_ref_no')]; 
		}
		
		$rqsn_array=array();
		if($db_type==0)
		{
			$reqsn_dataArray=sql_select("select a.knit_id, a.requisition_no, group_concat(distinct(yarn_count_id)) as yarn_count_id, group_concat(distinct(lot)) as lot from ppl_yarn_requisition_entry a, product_details_master b where a.prod_id=b.id and a.status_active=1 and a.is_deleted=0 group by a.knit_id, a.requisition_no");
		}
		else
		{
			$reqsn_dataArray=sql_select("select a.knit_id, a.requisition_no, LISTAGG(b.yarn_count_id, ',') WITHIN GROUP (ORDER BY b.yarn_count_id) as yarn_count_id, LISTAGG(CAST(b.lot AS VARCHAR2(4000))) WITHIN GROUP (ORDER BY b.id) as lot from ppl_yarn_requisition_entry a, product_details_master b where a.prod_id=b.id and a.status_active=1 and a.is_deleted=0 group by a.knit_id, a.requisition_no");	
		}
		foreach($reqsn_dataArray as $row)
		{
			$rqsn_array[$row[csf('knit_id')]]['rqsn_no'].=$row[csf('requisition_no')];
			$rqsn_array[$row[csf('knit_id')]]['count'].=implode(",",array_unique(explode(",",$row[csf('yarn_count_id')])));
			$rqsn_array[$row[csf('knit_id')]]['lot'].=implode(",",array_unique(explode(",",$row[csf('lot')])));
		}
		
		$buyer_arr=return_library_array( "select id, short_name from lib_buyer",'id','short_name');
		if($db_type==0)
		{
			$plan_details_array=return_library_array( "select dtls_id, group_concat(distinct(po_id)) as po_id from ppl_planning_entry_plan_dtls where company_id=$company group by dtls_id", "dtls_id", "po_id"  );
		}
		else
		{
			$plan_details_array=return_library_array( "select dtls_id, LISTAGG(po_id, ',') WITHIN GROUP (ORDER BY po_id) as po_id from ppl_planning_entry_plan_dtls where company_id=$company group by dtls_id", "dtls_id", "po_id"  );
		}

		if($buyer==0) $buyer="%%"; 
		if(trim($txt_search_common)!="")
		{
			if($txt_search_by==1)
			{
				$search_field_cond=" and a.booking_no like '%$txt_search_common%'";
			}
			
			else if($txt_search_by==2)	
			{
				if($db_type==0)
				{
					$po_id=return_field_value("group_concat(id) as po_id","wo_po_break_down","po_number like '%$txt_search_common%' and status_active=1 and is_deleted=0","po_id");
				}
				else
				{
					$po_id=return_field_value("LISTAGG(id, ',') WITHIN GROUP (ORDER BY id) as po_id","wo_po_break_down","po_number like '%$txt_search_common%' and status_active=1 and is_deleted=0","po_id");	
				}
				
				if($po_id!="") $search_field_cond=" and c.po_id in(".$po_id.")";	
			}
			
			else if($txt_search_by==3)	
			{
				if($db_type==0)
				{
					$po_id=return_field_value("group_concat(b.id) as po_id","wo_po_details_master a, wo_po_break_down b","a.job_no like '%$txt_search_common%' and a.job_no=b.job_no_mst and a.status_active=1 and b.status_active=1","po_id");
				}
				else
				{
					$po_id=return_field_value("LISTAGG(b.id, ',') WITHIN GROUP (ORDER BY b.id) as po_id","wo_po_details_master a, wo_po_break_down b","a.job_no like '%$txt_search_common%'  and a.job_no=b.job_no_mst and a.status_active=1 and b.status_active=1","po_id");	
				}
				
				if($po_id!="") $search_field_cond=" and c.po_id in(".$po_id.")";
			}
			else if($txt_search_by==4)
			{
				$search_field_cond=" and b.id like '$txt_search_common'";
			}
		}
		else
		{
			$search_field_cond="";
		}
		
		if( $txt_date_from!="" && $txt_date_to!="" ) 
		{
			if($db_type==0)
			{
				$date_cond= " and b.program_date between '".change_date_format($txt_date_from,'yyyy-mm-dd')."' and '".change_date_format($txt_date_to,'yyyy-mm-dd')."'";
			}
			else
			{
				$date_cond= " and b.program_date between '".change_date_format($txt_date_from,'','',1)."' and '".change_date_format($txt_date_to,'','',1)."'";	
			}
		}
		else $date_cond="";
		
		$sql = "select a.id, a.booking_no, a.buyer_id, a.body_part_id, a.determination_id, a.fabric_desc, a.gsm_weight, a.dia, b.color_range, b.id as knit_id, b.knitting_source, b.knitting_party, b.stitch_length, b.program_date from ppl_planning_info_entry_mst a, ppl_planning_info_entry_dtls b, ppl_planning_entry_plan_dtls c where a.id=b.mst_id and b.id=c.dtls_id and a.company_id=$company and a.buyer_id like '$buyer' and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 $date_cond $search_field_cond group by b.id, a.id, a.booking_no, a.buyer_id, a.body_part_id, a.determination_id, a.fabric_desc, a.gsm_weight, a.dia, b.color_range, b.knitting_source, b.knitting_party, b.stitch_length, b.program_date"; 
		//echo $sql;
		$result = sql_select($sql);
		?>
		<table cellspacing="0" cellpadding="0" border="1" rules="all" width="990" class="rpt_table">
			<thead>
				<th width="30">SL</th>
                <th width="55">Plan Id</th>
                <th width="60">Program Id</th>
                <th width="75">Program Date</th>
                <th width="50">Reqsn. No</th>
				<th width="110">Booking No</th>
				<th width="70">Buyer</th>
				<th width="100">PO No</th>
				<th width="90">Job No</th>
                <th width="130">Fabric Desc</th>
				<th width="55">Gsm</th>
				<th width="55">Dia</th>
				<th>Color Range</th>
			</thead>
		</table>
		<div style="width:990px; max-height:280px; overflow-y:scroll" id="container_batch" align="left">	 
			<table cellspacing="0" cellpadding="0" border="1" rules="all" width="972" class="rpt_table" id="view">
            <?php
				$i=1;
				foreach ($result as $row)
				{  
					if ($i%2==0)  
						$bgcolor="#E9F3FF";
					else
						$bgcolor="#FFFFFF";	 
						
					$reqn_no=$rqsn_array[$row[csf('knit_id')]]['rqsn_no'];
					
					$po_id=array_unique(explode(",",$plan_details_array[$row[csf('knit_id')]]));
					$po_no=''; $job_no=''; $style_ref_no='';
					
					foreach($po_id as $val)
					{
						if($po_no=='') $po_no=$po_array[$val]['no']; else $po_no.=",".$po_array[$val]['no'];
						if($job_no=='') $job_no=$po_array[$val]['job_no'];
						if($style_ref_no=='') $style_ref_no=$po_array[$val]['style'];
					}
					
					$po_no_id=implode(",",array_unique(explode(",",$plan_details_array[$row[csf('knit_id')]])));
					
				?>
					<tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" onClick="js_set_value('<?php echo $row[csf('knit_id')]; ?>_<?php echo $row[csf('knit_id')]; ?>_<?php echo $row[csf('buyer_id')]; ?>_<?php echo $style_ref_no; ?>_<?php echo $po_no; ?>_<?php echo $po_no_id; ?>','<?php echo $job_no; ?>',1);"> 
						<td width="30"><?php echo $i; ?></td>
						<td width="55" align="center"><?php echo $row[csf('id')]; ?></td>
                        <td width="60" align="center"><?php echo $row[csf('knit_id')]; ?></td>
                        <td width="75" align="center"><?php echo change_date_format($row[csf('program_date')]); ?></td>
						<td width="50" align="center"><?php echo $reqn_no; ?>&nbsp;</td>
                        <td width="110"><p><?php echo $row[csf('booking_no')]; ?></p></td>               
						<td width="70"><p><?php echo $buyer_arr[$row[csf('buyer_id')]]; ?></p></td>
                        <td width="100"><p><?php echo $po_no; ?></p></td>
                        <td width="90"><p><?php echo $job_no; ?></p></td>
						<td width="130"><p><?php echo $row[csf('fabric_desc')]; ?></p></td>
						<td width="55"><p><?php echo $row[csf('gsm_weight')]; ?></p></td>
						<td width="55"><p><?php echo $row[csf('dia')]; ?></p></td>
						<td><p><?php echo $color_range[$row[csf('color_range')]]; ?></p></td>
					</tr>
				<?php
				$i++;
				}
				?>
            </table>
        </div>  
	<?php
	}
	else
	{
		$sql_cond="";
		if(trim($txt_search_common)!="")
		{
			if(trim($txt_search_by)==1) // for Booking No
			{
				$sql_cond=" and a.booking_no like '%$txt_search_common%'";
			}
			else if(trim($txt_search_by)==2) // for buyer order
			{
				$sql_cond =" and b.po_number like '%$txt_search_common%'";
				//$sql_cond = " and b.po_number LIKE '%$txt_search_common%'";	// wo_po_break_down			
			}
			else if(trim($txt_search_by)==3) // for job no
			{
				$sql_cond =" and b.job_no_mst like '%$txt_search_common%'";
				//$sql_cond .= " and a.job_no LIKE '%$txt_search_common%'";				
			}
		}
		else
		{
			$sql_cond="";
		}
		
		if( $txt_date_from!="" && $txt_date_to!="" ) 
		{
			if($db_type==0)
			{
				$sql_cond .= " and a.booking_date between '".change_date_format($txt_date_from,'yyyy-mm-dd')."' and '".change_date_format($txt_date_to,'yyyy-mm-dd')."'";
			}
			else
			{
				$sql_cond .= " and a.booking_date between '".change_date_format($txt_date_from,'','',1)."' and '".change_date_format($txt_date_to,'','',1)."'";
			}
		}
		
		if( trim($buyer)!=0 ) $sql_cond .= " and a.buyer_id='$buyer'";
		if( trim($company)!=0 ) $sql_cond .= " and a.company_id='$company'";
		if( trim($booking_type)==1 ) $sql_cond .= " and a.booking_type!=4";
		else if( trim($booking_type)==4 ) $sql_cond .= " and a.booking_type=4";
		
		//echo $sql_cond.jahid."<br>";
		
		if(trim($booking_type)==8 || trim($booking_type)==3 || trim($booking_type)==26 || trim($booking_type)==29 || trim($booking_type)==30 || trim($booking_type)==31 )
		{
			$sql = "select a.id, a.booking_no, a.booking_date, a.buyer_id, a.po_break_down_id, a.item_category, a.delivery_date, a.job_no as job_no_mst
				from wo_non_ord_samp_booking_mst a, wo_non_ord_samp_booking_dtls b 
				where 
					a.booking_no=b.booking_no and 
					a.item_category=2 and 
					a.status_active=1 and 
					a.is_deleted=0 and 
					b.status_active=1 and 
					b.is_deleted=0 
					$sql_cond 
					group by a.id, a.booking_no, a.booking_date, a.buyer_id, a.po_break_down_id, a.item_category, a.delivery_date, a.job_no"; 
		}
		else
		{
			$po_arr=array();
			$po_data=sql_select("select a.style_ref_no, b.id, b.po_number, b.pub_shipment_date, b.po_quantity, (a.total_set_qnty*b.po_quantity) as po_qnty_in_pcs from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst");	
			foreach($po_data as $row)
			{
				$po_arr[$row[csf('id')]]=$row[csf('po_number')]."**".$row[csf('pub_shipment_date')]."**".$row[csf('po_quantity')]."**".$row[csf('po_qnty_in_pcs')]."**".$row[csf('style_ref_no')];
			}
			
			$sql = "select a.id, a.booking_no, a.booking_date, a.buyer_id, a.po_break_down_id, a.item_category, a.delivery_date, a.job_no as job_no_mst 
			from wo_booking_mst a, wo_po_break_down b 
			where 
				a.job_no=b.job_no_mst and 
				a.item_category=2 and 
				a.status_active=1 and 
				a.is_deleted=0 and 
				b.status_active=1 and 
				b.is_deleted=0 
				$sql_cond 
				group by a.id, a.booking_no, a.booking_date, a.buyer_id, a.po_break_down_id, a.item_category, a.delivery_date, a.job_no"; 
		}
		//echo $sql;
		//item_category=2 knit fabrics 
		//echo $sql;die;
		$result = sql_select($sql);
		$buyer_arr = return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
		?>
		<div align="left">
            <table cellspacing="0" cellpadding="0" border="1" rules="all" width="970" class="rpt_table" >
                <thead>
                    <th width="30">SL</th>
                    <th width="105">Booking No</th>
                    <th width="90">Book. Date</th>               
                    <th width="100">Buyer</th>
                    <th width="90">Item Cat.</th>
                    <th width="90">Job No</th>
                    <th width="90">Order Qnty</th>
                    <th width="80">Ship. Date</th>
                    <th >Order No</th>
                </thead>
            </table>
            <div style="width:990px; max-height:280px; overflow-y:scroll" id="container_batch" >	 
                <table cellspacing="0" cellpadding="0" border="1" rules="all" width="970" class="rpt_table" id="view">  
                <?php
                    $i=1;
                    foreach ($result as $row)
                    {  
                        if ($i%2==0)  
                            $bgcolor="#E9F3FF";
                        else
                            $bgcolor="#FFFFFF";	 
                         
                        $po_qnty_in_pcs=0; $po_no=''; $po_no_id=''; $min_shipment_date=''; $style_ref_no="";
                        if($row[csf('po_break_down_id')]!="")
                        {
                            /*$po_sql = "select a.style_ref_no, b.po_number,b.id, b.pub_shipment_date, b.po_quantity, (a.total_set_qnty*b.po_quantity) as po_qnty_in_pcs from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.id in (".$row[csf('po_break_down_id')].")";	 							
                            $nameArray=sql_select($po_sql);
                            foreach ($nameArray as $po_row)*/
    
                            $po_id=explode(",",$row[csf('po_break_down_id')]);
                            foreach ($po_id as $id)
                            {
                                $po_data=explode("**",$po_arr[$id]);
                                $po_number=$po_data[0];
                                $pub_shipment_date=$po_data[1];
                                $po_qnty=$po_data[2];
                                $poQntyPcs=$po_data[3];
                                $style_ref_no=$po_data[4];
                                
                                if($po_no=="") $po_no=$po_number; else $po_no.=",".$po_number;
                                if($po_no_id=="") $po_no_id=$id; else $po_no_id.=",".$id;
                                
                                if($min_shipment_date=='')
                                {
                                    $min_shipment_date=$pub_shipment_date;
                                }
                                else
                                {
                                    if($pub_shipment_date<$min_shipment_date) $min_shipment_date=$pub_shipment_date; else $min_shipment_date=$min_shipment_date;
                                }
                                
                                $po_qnty_in_pcs+=$poQntyPcs;
                                $style_ref_no = $style_ref_no;
                            }
                        }
                    ?>
                        <tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" onClick="js_set_value('<?php echo $row[csf('id')]; ?>_<?php echo $row[csf('booking_no')]; ?>_<?php echo $row[csf('buyer_id')]; ?>_<?php echo $style_ref_no; ?>_<?php echo $po_no; ?>_<?php echo $po_no_id; ?>','<?php echo $row[csf('job_no_mst')]; ?>',0);"> 
                            <td width="30"><?php echo $i; ?></td>
                            <td width="105"><p><?php echo $row[csf('booking_no')]; ?></p></td>
                            <td width="90" align="center"><?php echo change_date_format($row[csf('booking_date')]); ?></td>               
                            <td width="100"><p><?php echo $buyer_arr[$row[csf('buyer_id')]]; ?></p></td>
                            <td width="90"><p><?php echo $item_category[$row[csf('item_category')]]; ?></p></td>
                            <td width="90"><p><?php echo $row[csf('job_no_mst')]; ?>&nbsp;</p></td>
                            <td width="90" align="right"><?php echo $po_qnty_in_pcs; ?>&nbsp;</td>
                            <td width="80" align="center"><?php echo change_date_format($min_shipment_date); ?>&nbsp;</td>
                            <td><p><?php echo $po_no; ?>&nbsp;</p></td>
                        </tr>
                    <?php
                    $i++;
                    }
                    ?>
                </table>
			</div>
		</div>
	<?php	
	}
	exit();		
}


//Roll Pop up Search Here----------------------------------// 
if ($action=="roll_popup")
{
  	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	extract($_REQUEST);  
	
	//for update mode -----------
	$concate = $cbo_company_id;
	if(!$hidden_roll_id) $$hidden_roll_id="";
	if(!$hidden_roll_qnty) $hidden_roll_qnty="";
	if(!$txt_batch_id) $txt_batch_id="";
	$concate .= "**".$hidden_roll_id."**".$hidden_roll_qnty."**".$txt_batch_id;
	 
?>
     
<script>
	
	function fn_show_check()
	{		
		show_list_view ( document.getElementById('cbo_search_by').value+'_'+document.getElementById('txt_search_common').value+'_'+'<?php echo $concate; ?>', 'create_roll_search_list', 'search_div', 'grey_fabric_issue_controller', 'setFilterGrid(\'view\',-1)');
		set_previous_data();
	}
	
	
	function set_previous_data()
	{		 
		var old=document.getElementById('previous_data_row_id').value;
		if(old!="")
		{   
			old=old.split(",");
			for(var i=0; i<old.length; i++)
			{  
				fn_onkeyup(old[i]);
				//js_set_value( old[i] ) 
			}
		}
	}
		 
	
	function js_set_value( str ) 
	{ 
		var chkProduct = $("#txt_prod_id"+str).val();
		var po_number = $("#txt_po_number"+str).val();
		var flag=0;
		$("input[name='chk[]']:checked").each(function ()
		{
			 var rid = $(this).attr('id'); 
			 var ridArr = rid.split("_");
			 var str = ridArr[1];
			 if($("#txt_prod_id"+str).val()!=chkProduct && chkProduct!="")
			 {
				 alert("Product Mix Not Allow");
				 flag=1;
				 return false;
			 }
			 /*else if($("#txt_po_number"+str).val()!=po_number && po_number!="")
			 {
				 alert("Order No Mix Not Allow");
				 flag=1;
				 return false;
			 }  */
		});
		
		if(flag==1) return;
		 
		if($("#txt_issue_qnty_"+str).val()*1 <= 0 || $("#txt_issue_qnty_"+str).val()=='-')
		{
			document.getElementById('chk_'+str).checked=false;
			document.getElementById( 'search' + str ).style.backgroundColor = '#FFFFCC';
			return;
		}
		  
		if( document.getElementById("chk_"+str).checked )
		{
			document.getElementById('chk_'+str).checked=false;
			document.getElementById( 'search' + str ).style.backgroundColor = '#FFFFCC';
		}
		else
		{
			document.getElementById('chk_'+str).checked=true;
			document.getElementById( 'search' + str ).style.backgroundColor = 'yellow';
 		}
		 
 	}
	
	
	function fn_onkeyup(str)
	 { 
		 
		 if( $("#txt_issue_qnty_"+str).val()*1 > $("#txt_balance_qnty_"+str).val()*1 )
		 {
			 $("#txt_issue_qnty_"+str).val($("#txt_balance_qnty_"+str).val());
			 return;
		 }
		 else if($("#txt_issue_qnty_"+str).val()*1<=0 || $("#txt_issue_qnty_"+str).val()=='-')
		 {
			document.getElementById('chk_'+str).checked=false;
			document.getElementById( 'search' + str ).style.backgroundColor = '#FFFFCC';
			return;
		 }
		 
		 var chkProduct = $("#txt_prod_id"+str).val();
		 var po_number = $("#txt_po_number"+str).val();
		 $("input[name='chk[]']:checked").each(function ()
		 {
			 var rid = $(this).attr('id'); 
			 var ridArr = rid.split("_");
			 var str = ridArr[1];
			 if($("#txt_prod_id"+str).val()!=chkProduct && chkProduct!="")
			 {
				 alert("Product Mix Not Allow");
				 return false;
			 }
			 /*else if($("#txt_po_number"+str).val()!=po_number && po_number!="")
			 {
				 alert("Order No Mix Not Allow");
				 return false;
			 }*/ 
			 
		 });
		 
		 document.getElementById('chk_'+str).checked=true;
		 document.getElementById('search'+str).style.backgroundColor='yellow';
	 }
	 
	 
	var selected_id = new Array;
	var issue_qnty = new Array;
	var chkProduct = "";
	
	function fnonClose()
	 {
		$("input[name='chk[]']:checked").each(function ()
		{
			var rid = $(this).attr('id'); 
			var ridArr = rid.split("_");
			var str = ridArr[1];
			
			if( jQuery.inArray( $('#txt_individual_id' + str).val(), selected_id ) == -1 ) {
				selected_id.push( $('#txt_individual_id' + str).val() );
				issue_qnty.push( $('#txt_issue_qnty_' + str).val() ); 
			}
			else 
			{
				for( var i = 0; i < selected_id.length; i++ ) {
					if( selected_id[i] == $('#txt_individual_id' + str).val() )
					{ 
 						break;
					}
				}
				selected_id.splice( i, 1 );
				issue_qnty.splice( i, 1 ); 
			}
			
			var id = qnty = '';
			for( var i = 0; i < selected_id.length; i++ ) {
				id += selected_id[i] + ',';	
				qnty += issue_qnty[i] + ',';				
			}
			id = id.substr( 0, id.length - 1 );		
			qnty = qnty.substr( 0, qnty.length - 1 );			
			
			$('#txt_selected_id').val( id );	
			$('#txt_issue_qnty').val( qnty ); 
		});
		
		parent.emailwindow.hide();
	 }
	
</script>

</head>

<body>
<div align="center" style="width:100%;" >
<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
	<table width="600" cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
            <thead>
                <tr>                	 
                    <th width="150">Search By</th>
                    <th width="150" align="center" id="search_by_td_up">Select Buyer</th>                    
                    <th><input type="reset" name="re_button" id="re_button" value="Reset" style="width:100px" class="formbutton"  /></th>           
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td align="center">
                        <?php  
                            $sql="select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company=$cbo_company_id $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name";
							//echo $sql;
							$search_by = array(1=>'Buyer', 2=>'Job No', 3=>'Style Ref');
							$dd="change_search_event(this.value, '1*0*0', '".$sql."*0*0', '../../../')";
							echo create_drop_down("cbo_search_by", 120, $search_by, "", 1, "--Select--", "", $dd, 0);
                        ?>
                    </td>
                    <td width="180" align="center" id="search_by_td">		
                        <input type="text" id="txt_search_common" name="txt_search_common" class="text_boxes" style="width:100px" />
                    </td>                    
                     <td align="center">
                        <input type="button" name="btn_show" class="formbutton" value="Show" style="width:100px;" onClick="fn_show_check()" />				
                    	
                        <!-- Hidden field here-------->
                        <input type="hidden" id="txt_selected_id" value="" /> 
                        <input type="hidden" id="txt_issue_qnty" value="" />
                        <!-- ---------END------------->
                    </td>
            	</tr>  
                 
            </tbody>
         </tr>         
        </table>    
        <div align="center" valign="top" id="search_div"> </div> 
        <table width="750">
             <tr>
                <td align="center" >
                    <input type="button" name="close" class="formbutton" value="Close" id="main_close" onClick="fnonClose();" style="width:100px" />
                </td>
            </tr>
        </table>
        </form>
   </div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}

 
if($action=="create_roll_search_list")
{ 
	
 	$ex_data = explode("_",$data);
	$txt_search_by = $ex_data[0];
	$txt_search_common = trim($ex_data[1]);
	$expArr = explode("**",$ex_data[2]);
 	$company = $expArr[0]; // company id
	$rolID = $expArr[1];  //roll id
	$rollQnty = $expArr[2]; //roll qnty
	$batchID = $expArr[3]; //batch ID
	
	$cond="";
	$issue_qnty_arr = array();
	if($rolID!="")
	{
		$issueQnty = explode(",",$rollQnty); //roll wise input issue qnty
		$exp_rollID = explode(",",$rolID); 
		for($j=0;$j<count($exp_rollID);$j++)
		{
			$issue_qnty_arr[$exp_rollID[$j]] = $issueQnty[$j];
		}
	}
	
	$rollCond=$rollCondIssue="";
	if($rolID!="") 
	{ 
		//$rollCond = " and c.id not in ($rolID)"; //use for update mode
		$rollCondIssue = " and roll_id not in ($rolID)"; //use for update mode
	}  
	
	if($txt_search_by==1 && $txt_search_common!="")
		$cond = " and a.buyer_name='$txt_search_common'"; 
	elseif($txt_search_by==2 && $txt_search_common!="")
		$cond = " and a.job_no='$txt_search_common'"; 
	elseif($txt_search_by==3 && $txt_search_common!="")
		$cond = " and a.style_ref_no='$txt_search_common'"; 		
	 
	if($batchID!="")
	{
		if($db_type==0)
		{
			$rollNo = sql_select("select group_concat(b.po_id) as po_id, group_concat(b.roll_no) as roll_no from pro_batch_create_mst a, pro_batch_create_dtls b where a.id=b.mst_id and a.id='$batchID' and b.roll_no<>0");
		}
		else
		{
			$rollNo = sql_select("select LISTAGG(b.po_id, ',') WITHIN GROUP (ORDER BY b.po_id) as po_id, LISTAGG(b.roll_no, ',') WITHIN GROUP (ORDER BY b.roll_no) as roll_no from pro_batch_create_mst a, pro_batch_create_dtls b where a.id=b.mst_id and a.id='$batchID' and b.roll_no<>0");	
		}
		foreach ($rollNo as $row);
		$rollNo = $row[csf('roll_no')];
		$po_id = $row[csf('po_id')]; 
		if($rollNo!="") $cond .= " and c.roll_no in ($rollNo)";
		if($po_id!="") $cond .= " and b.id in ($po_id)";
	}
	else $mstID="";	
		
	if($db_type==0)
	{
  		$sql = "select b.po_number, c.id, c.po_breakdown_id, c.roll_no, d.prod_id, e.product_name_details, e.lot,sum(c.qnty) as rcvqnty 
			from 
				wo_po_details_master a, wo_po_break_down b, pro_roll_details c, pro_grey_prod_entry_dtls d, product_details_master e
			where  
				a.job_no=b.job_no_mst and b.id=c.po_breakdown_id and c.dtls_id=d.id and d.prod_id=e.id and c.entry_form=2 and c.roll_no!=0 and e.current_stock>0
				$cond 
				group by c.id,c.po_breakdown_id,c.roll_no
				order by e.product_name_details";
	}
	else
	{
		$sql = "select b.po_number, c.id, c.po_breakdown_id, c.roll_no, d.prod_id, e.product_name_details, e.lot, sum(c.qnty) as rcvqnty 
			from 
				wo_po_details_master a, wo_po_break_down b, pro_roll_details c, pro_grey_prod_entry_dtls d, product_details_master e
			where  
				a.job_no=b.job_no_mst and b.id=c.po_breakdown_id and c.dtls_id=d.id and d.prod_id=e.id and c.entry_form=2 and c.roll_no!=0 and e.current_stock>0
				$cond 
				group by c.id, c.po_breakdown_id,c.roll_no, b.po_number, d.prod_id, e.product_name_details, e.lot
				order by e.product_name_details";
	}
 	//echo $sql;die;
	$result = sql_select($sql);
	
  	?> 
    <div align="left" style="margin-left:50px">    
    <table cellspacing="0" cellpadding="0" border="1" rules="all" width="750" class="rpt_table" >
        <thead>
            <th width="50">SL</th>
            <th width="60">Roll No</th>
            <th width="100">Order No</th>               
            <th width="250">Fabric Description</th>
            <th width="100">Yarn Lot</th>
            <th width="100">Roll Qnty</th>
            <th width="100">Issue Qnty</th> 
        </thead>
	</table>
    
	<div style="width:770px; max-height:250px; overflow-y:scroll" id="container_batch" >	 
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="750" class="rpt_table" id="view">  
        <?php
            $i=1;
            $previouDataRow="";
			foreach ($result as $row)
            {  
				//echo "sum(qnty) pro_roll_details po_breakdown_id=".$row[csf('po_breakdown_id')]." and roll_no=".$row[csf('roll_no')]." and entry_form=16 $rollCondIssue";
				$issueqnty = return_field_value("sum(qnty)","pro_roll_details","po_breakdown_id=".$row[csf('po_breakdown_id')]." and roll_no=".$row[csf('roll_no')]." and entry_form=16 $rollCondIssue");
				//echo $issueqnty."==";
				if($issueqnty=="") $issueqnty=0; 
				$balanceQnty = $row[csf('rcvqnty')]-$issueqnty;
				//if($balanceQnty<=0)return;
				
								
				if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF"; 
				$isChecked="";
				if($issue_qnty_arr[$row[csf('id')]]!="")
				{
					 $row[csf('qnty')]=$issue_qnty_arr[$row[csf('id')]];
					 $bgcolor="yellow"; 
					 $isChecked = "checked";					
					 if($previouDataRow == "") $previouDataRow=$i; else $previouDataRow .= ",".$i;				 
				}
				 
          		?>
                <tr bgcolor="<?php echo $bgcolor; ?>" id="search<?php echo $i;?>" style="cursor:pointer" > 
                    <td width="50"><?php echo $i; ?>
                    	<input type="checkbox" id="chk_<?php echo $i;?>" name="chk[]" class="check" style="visibility:hidden" onClick="js_set_value(<?php echo $i;?>)" <?php echo $isChecked; ?> />
                    	<input type="hidden" name="txt_individual_id" id="txt_individual_id<?php echo $i ?>" value="<?php echo $row[csf('id')]; ?>"/> 
                        <input type="hidden" id="txt_prod_id<?php echo $i ?>" value="<?php echo $row[csf('prod_id')]; ?>" /> 
                        <input type="hidden" id="txt_po_number<?php echo $i ?>" value="<?php echo $row[csf('po_number')]; ?>" />                     
                    </td>
                    <td width="60" align="right" onClick="js_set_value(<?php echo $i;?>)"><p><?php echo $row[csf('roll_no')]; ?></p></td>
                    <td width="100" onClick="js_set_value(<?php echo $i;?>)"><p><?php echo $row[csf('po_number')]; ?></p></td>               
                    <td width="250" onClick="js_set_value(<?php echo $i;?>)"><p><?php echo $row[csf('product_name_details')]; ?></p></td>
                    <td width="100" onClick="js_set_value(<?php echo $i;?>)"><p><?php echo $row[csf('lot')]; ?></p></td>
                    <td width="100" onClick="js_set_value(<?php echo $i;?>)"><input type="text" id="txt_balance_qnty_<?php echo $i; ?>" class="text_boxes_numeric" value="<?php echo $balanceQnty; ?>" style="width:70px" disabled readonly /></td>                    
                    <td width="100"><input type="text" name="txt_issue_qnty[]" id="txt_issue_qnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:80px" value="<?php if($isChecked=="checked") echo $row[csf('qnty')]; else echo $balanceQnty; ?>"  onKeyUp="fn_onkeyup(<?php echo $i;?>)" /></td>   
                                    
                 </tr> 
				<?php 
                $i++;
            }
        	?>
            	<!-- for update, contain row id which is seleted -->
                <input type="hidden" id="previous_data_row_id" value="<?php echo $previouDataRow; ?>" />
        </table>        
      </div>
    </div>
	<?php	
	exit();	
	
}

// child form data populate after call after close roll pop up
if($action == "populate_child_from_data")  
{
	$i=0;
	$exp_data = explode("**",$data);
	$roll_ID = $exp_data[0];
	if($roll_ID=="") exit; // if roll id emtpy
	$issueQnty = explode(",",$exp_data[1]); //roll wise input issue qnty
	$exp_rollID = explode(",",$roll_ID); 
	
	$issue_qnty_arr = array();
	for($j=0;$j<count($exp_rollID);$j++)
	{
		$issue_qnty_arr[$exp_rollID[$j]] = $issueQnty[$j];
	}
	
	if($db_type==0)
	{
	//echo $issueQnty; 
		$sql = "select GROUP_CONCAT(c.id) as id, GROUP_CONCAT(c.roll_no) as roll_no, GROUP_CONCAT(c.po_breakdown_id) as po_breakdown_id, count(c.roll_no) as roll_no_count, sum(c.qnty) as qnty, d.prod_id, e.product_name_details
				from pro_roll_details c, pro_grey_prod_entry_dtls d, product_details_master e
				where				
				c.entry_form=2 and 
				c.dtls_id=d.id and 
				d.prod_id=e.id and 
				c.roll_no!=0 and
				c.id in ($roll_ID) group by d.prod_id, e.product_name_details"; 
	}
	else
	{
		$sql = "select LISTAGG(c.id, ',') WITHIN GROUP (ORDER BY c.id) as id, LISTAGG(c.roll_no, ',') WITHIN GROUP (ORDER BY c.roll_no) as roll_no, LISTAGG(c.po_breakdown_id, ',') WITHIN GROUP (ORDER BY c.po_breakdown_id) as po_breakdown_id, count(c.roll_no) as roll_no_count, sum(c.qnty) as qnty, d.prod_id, e.product_name_details
			from pro_roll_details c, pro_grey_prod_entry_dtls d, product_details_master e
			where				
			c.entry_form=2 and 
			c.dtls_id=d.id and 
			d.prod_id=e.id and 
			c.roll_no!=0 and
			c.id in ($roll_ID) group by d.prod_id, e.product_name_details";	
	}
	//echo $sql;
	$result = sql_select($sql);  
	foreach($result as $row)
	{ 
		$i=$i+1;
		
		$exp_concat = explode(",",$row[csf("id")]);
		$issue_qnty = 0;
		$roll_wise_qnty="";
		foreach($exp_concat as $key=>$val)
		{
			if($roll_wise_qnty!="") $roll_wise_qnty.=",";
			$roll_wise_qnty .= $issue_qnty_arr[$val];
			$issue_qnty += $issue_qnty_arr[$val];
		} 
		
		if($db_type==0)
		{
			$lot=return_field_value("group_concat(distinct(yarn_lot)) as lot","pro_grey_prod_entry_dtls","prod_id='".$row[csf('prod_id')]."' and yarn_lot<>'' and status_active=1 and is_deleted=0","lot");
			$count_id=return_field_value("group_concat_concat(distinct(yarn_count)) as count","pro_grey_prod_entry_dtls","prod_id='".$row[csf('prod_id')]."' and yarn_count<>'' and yarn_count<>0 and status_active=1 and is_deleted=0","count");
		}
		else
		{
			$lot=return_field_value("LISTAGG(cast(yarn_lot as VARCHAR2(4000)), ',') WITHIN GROUP (ORDER BY yarn_lot) as lot","pro_grey_prod_entry_dtls","prod_id='".$row[csf('prod_id')]."' and yarn_lot is not null and status_active=1 and is_deleted=0","lot");
			$count_id=return_field_value("LISTAGG(yarn_count, ',') WITHIN GROUP (ORDER BY yarn_count) as count","pro_grey_prod_entry_dtls","prod_id='".$row[csf('prod_id')]."' and yarn_count is not null and yarn_count<>0 and status_active=1 and is_deleted=0","count");	
		}
		
		$lot=implode(",",array_unique(explode(",",$lot)));
		$count=implode(",",array_unique(explode(",",$count_id)));
		
		echo "$('#txtNoOfRoll').val('".$row[csf('roll_no_count')]."');\n";
		
		echo "$('#txtRollNo').val('".$row[csf('id')]."');\n";
		echo "$('#txtRollPOid').val('".$row[csf('po_breakdown_id')]."');\n";
		echo "$('#txtRollPOQnty').val('".$roll_wise_qnty."');\n";
		
		echo "$('#txtItemDescription').val('".$row[csf('product_name_details')]."');\n";
		echo "$('#hiddenProdId').val('".$row[csf('prod_id')]."');\n";
		echo "$('#txtReqQnty').val('".$issue_qnty."');\n";
		echo "$('#txtYarnLot').val('".$lot."');\n";
		
		//echo "$('#cbo_yarn_count').val('".$row[csf('yarn_count_id')]."');\n";
		echo "set_multiselect('cbo_yarn_count','0','1','".$count."','0');\n";
		//echo "disable_enable_fields('show_textcbo_yarn_count','1','','');\n";
		echo "if($('#cbo_issue_purpose').val()==3 || $('#cbo_issue_purpose').val()==8) $('#txtIssueQnty').val('".$issue_qnty."');\n";
		echo "get_php_form_data('".$row[csf('po_breakdown_id')]."'+\"**\"+".$row[csf('prod_id')].", \"populate_data_about_order\", \"requires/grey_fabric_issue_controller\" );"; 
	}
	
	exit();
	
}
 

if($action=="populate_data_about_order")
{
	$data=explode("**",$data);
	$order_id=$data[0];
	$prod_id=$data[1];
	$issue_purpose=$data[2];
	/*if ($issue_purpose==8)
	{
		$sql=sql_select("select sum(case when transaction_type in (1,4) then cons_quantity end) as grey_fabric_recv, sum(case when transaction_type=5 then cons_quantity end) as grey_fabric_issued from inv_transaction where item_category=13 and prod_id=$prod_id and is_deleted=0 and status_active=1");
	}
	else
	{
		$sql=sql_select("select 
					sum(case when entry_form in(2,22) then quantity end) as grey_fabric_recv, 
					sum(case when entry_form in(16) then quantity end) as grey_fabric_issued,
					sum(case when entry_form=45 then quantity end) as grey_fabric_recv_return, 
					sum(case when entry_form=51 then quantity end) as grey_fabric_issue_return,
					sum(case when entry_form in(13,81) and trans_type=5 then quantity end) as grey_fabric_trans_recv, 
					sum(case when entry_form in(13,80) and trans_type=6 then quantity end) as grey_fabric_trans_issued
				from order_wise_pro_details where trans_id<>0 and prod_id=$prod_id and po_breakdown_id in($order_id) and is_deleted=0 and status_active=1");
	}*/
	$sql=sql_select("select 
					sum(case when entry_form in(2,22) then quantity end) as grey_fabric_recv, 
					sum(case when entry_form in(16) then quantity end) as grey_fabric_issued,
					sum(case when entry_form=45 then quantity end) as grey_fabric_recv_return, 
					sum(case when entry_form=51 then quantity end) as grey_fabric_issue_return,
					sum(case when entry_form in(13,81) and trans_type=5 then quantity end) as grey_fabric_trans_recv, 
					sum(case when entry_form in(13,80) and trans_type=6 then quantity end) as grey_fabric_trans_issued
				from order_wise_pro_details where trans_id<>0 and prod_id=$prod_id and po_breakdown_id in($order_id) and is_deleted=0 and status_active=1");
	$grey_fabric_recv=$sql[0][csf('grey_fabric_recv')]+$sql[0][csf('grey_fabric_trans_recv')]+$sql[0][csf('grey_fabric_issue_return')];
	$grey_fabric_issued=$sql[0][csf('grey_fabric_issued')]+$sql[0][csf('grey_fabric_trans_issued')]+$sql[0][csf('grey_fabric_recv_return')];
	$yet_issue=$grey_fabric_recv-$grey_fabric_issued;
	
	//$order_nos=return_field_value("group_concat(po_number)","wo_po_break_down","id in($order_id)");
	//echo "$('#txt_order_numbers').val('".$order_nos."');\n";
	 
	echo "$('#txt_fabric_received').val('".$grey_fabric_recv."');\n";
	echo "$('#txt_cumulative_issued').val('".$grey_fabric_issued."');\n";
	echo "$('#txt_yet_to_issue').val('".$yet_issue."');\n"; 
 	
	$stock_qty=return_field_value("current_stock","product_details_master","id='$prod_id'");
	echo "$('#txt_global_stock').val('".$stock_qty."');\n"; 
	
	if($order_id!="")
	{
		if($db_type==0)
		{
			$orderSQL = sql_select("select max(a.buyer_name) as buyer_name,GROUP_CONCAT(distinct(a.style_ref_no)) as style_ref_no,GROUP_CONCAT(b.id) as b_id,GROUP_CONCAT(b.po_number) as b_ponum from wo_po_details_master a,wo_po_break_down b where a.job_no=b.job_no_mst and b.id in ($order_id)");
		}
		else
		{
			$orderSQL = sql_select("select max(a.buyer_name) as buyer_name,LISTAGG(cast(a.style_ref_no as varchar2(4000)), ',') WITHIN GROUP (ORDER BY a.id) as style_ref_no,LISTAGG(b.id, ',') WITHIN GROUP (ORDER BY b.id) as b_id,LISTAGG(cast(b.po_number as varchar2(4000)), ',') WITHIN GROUP (ORDER BY b.id) as b_ponum from wo_po_details_master a,wo_po_break_down b where a.job_no=b.job_no_mst and b.id in ($order_id)");
		}
		
		echo "$('#txt_style_ref').val('".implode(",",array_unique(explode(",",$orderSQL[0][csf("style_ref_no")])))."');\n";
		echo "$('#cbo_buyer_name').val('".$orderSQL[0][csf("buyer_name")]."');\n";
		echo "$('#txt_order_no').val('".$orderSQL[0][csf("b_ponum")]."');\n";
		echo "$('#hidden_order_id').val('".$orderSQL[0][csf("b_id")]."');\n";
	}
	exit();	
}


if($action=="populate_data_about_sample")
{
	$data=explode("**",$data);
	$booking_id=$data[0];
	$prod_id=$data[1];
	
	$recv_id='';
	if($db_type==0)
	{
		$recv_id=return_field_value("group_concat(distinct(id)) as id","inv_receive_master","booking_without_order=1 and booking_id=$booking_id and status_active=1 and is_deleted=0 and entry_form in(2)","id");
	}
	else
	{
		$recv_id=return_field_value("LISTAGG(id, ',') WITHIN GROUP (ORDER BY id) as id","inv_receive_master","booking_without_order=1 and booking_id=$booking_id and status_active=1 and is_deleted=0 and entry_form in(2)","id");
	}
	
	if($recv_id=="") $recv_id=0;
	$all_booking_id=$booking_id.",".$recv_id;
	$sql = "select sum(qnty) as qnty, sum(qnty2) as qnty2 from 
			(
				select sum(case when b.receive_basis!=9 and b.booking_id=$booking_id then c.grey_receive_qnty else 0 end ) as qnty, sum(case when b.receive_basis=9 and b.booking_id in($recv_id) then c.grey_receive_qnty else 0 end) as qnty2 from inv_receive_master b, pro_grey_prod_entry_dtls c where b.id=c.mst_id and b.entry_form in(2,22) and b.booking_without_order=1 and c.trans_id!=0 and b.status_active=1 and b.is_deleted=0 and b.booking_id in ($all_booking_id) and c.prod_id=$prod_id
				union all
				select sum(c.cons_quantity) as qnty, 0 as qnty2 from inv_receive_master b, inv_transaction c where b.id=c.mst_id and b.entry_form=51 and b.receive_purpose=8 and c.item_category=13 and c.transaction_type=4 and b.booking_id=$booking_id and b.status_active=1 and b.is_deleted=0 and c.prod_id=$prod_id
				union all
				select sum(c.transfer_qnty) as qnty, 0 as qnty2 from inv_item_transfer_mst b, inv_item_transfer_dtls c where b.id=c.mst_id and c.item_category=13 and b.transfer_criteria=6 and b.to_order_id=$booking_id and b.status_active=1 and b.is_deleted=0 and c.from_prod_id=$prod_id
			) product_details_master";
			
	$result=sql_select($sql);
	
	$iss_sql="select sum(qnty) as qnty from
			(
				select sum(c.issue_qnty) as qnty from inv_issue_master b, inv_grey_fabric_issue_dtls c where b.id=c.mst_id and b.item_category=13 and b.entry_form=16 and b.issue_purpose in(3,8,26,29,30,31) and b.issue_basis=1 and b.booking_id=$booking_id and b.status_active=1 and b.is_deleted=0
				union all
				select sum(c.cons_quantity) as qnty from inv_issue_master b, inv_transaction c, inv_receive_master d where b.id=c.mst_id and b.received_id=d.id and ((d.booking_id=$booking_id and d.receive_basis!=9) or (d.booking_id in ($recv_id) and d.receive_basis=9)) and d.booking_without_order=1 and b.item_category=13 and b.entry_form=45 and c.transaction_type=3 and c.item_category=13
				union all
				select sum(c.transfer_qnty) as qnty from inv_item_transfer_mst b, inv_item_transfer_dtls c where b.id=c.mst_id and b.transfer_criteria=7 and c.item_category=13 and b.from_order_id=$booking_id and b.status_active=1 and b.is_deleted=0
			) inv_issue_master";
	$result_iss=sql_select($iss_sql);		
								
	$grey_fabric_recv=$result[0][csf('qnty')]+$result[0][csf('qnty2')];
	$grey_fabric_issued=$result_iss[0][csf('qnty')];
	$yet_issue=$grey_fabric_recv-$grey_fabric_issued;
	
	echo "$('#txt_fabric_received').val('".$grey_fabric_recv."');\n";
	echo "$('#txt_cumulative_issued').val('".$grey_fabric_issued."');\n";
	echo "$('#txt_yet_to_issue').val('".$yet_issue."');\n"; 
 	
	$stock_qty=return_field_value("current_stock","product_details_master","id='$prod_id'");
	echo "$('#txt_global_stock').val('".$stock_qty."');\n"; 
	
	exit();	
}

 
if($action=="itemDescription_popup")
{ 
	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	extract($_REQUEST);  //echo $txt_booking_no;
	$stock_arr = return_library_array( "select id, current_stock from product_details_master",'id','current_stock');
	
	?>
    <script>
		function js_set_value(prod_id)
		{			
			$("#txt_selected_id").val(prod_id);
			parent.emailwindow.hide();			
		}
		
		$(document).ready(function(e) {
            setFilterGrid('tbl_search',-1);
        });
	</script>
	<?php
	$po_id='';
	if($cbo_basis==1)
	{
		if($cbo_issue_purpose==4 || $cbo_issue_purpose==11)
		{
			$po_id=return_field_value("po_break_down_id","wo_booking_mst","booking_no='$txt_booking_no'");
		}
	}
	else if($cbo_basis==3)
	{
		if($db_type==0)
		{
			$po_id=return_field_value("group_concat(distinct(po_id)) as po_id","ppl_planning_entry_plan_dtls","dtls_id='$txt_booking_no' and status_active=1 and is_deleted=0","po_id");
		}
		else
		{
			$po_id=return_field_value("LISTAGG(po_id, ',') WITHIN GROUP (ORDER BY po_id) as po_id","ppl_planning_entry_plan_dtls","dtls_id='$txt_booking_no' and status_active=1 and is_deleted=0","po_id");
			$po_id=implode(",",array_unique(explode(",",$po_id)));
		}
	}
	else
	{
		$po_id=$hidden_order_id;
	}
	
	//echo $po_id;	
	
	if($po_id!="")
	{
		$program_no_arr=array();
		if($cbo_basis==3)
		{
			if($db_type==0)
			{
				$recv_id=return_field_value("group_concat(id) as id","inv_receive_master","booking_without_order=0 and booking_id=$txt_booking_no and status_active=1 and is_deleted=0 and entry_form in(2) and receive_basis=2","id");
			}
			else
			{
				$recv_id=return_field_value("LISTAGG(id, ',') WITHIN GROUP (ORDER BY id) as id","inv_receive_master","booking_without_order=0 and booking_id=$txt_booking_no and status_active=1 and is_deleted=0 and entry_form in(2) and receive_basis=2","id");
			}
			if($recv_id=="") $recv_id=0;
			//echo $recv_id;
			$all_booking_id=$txt_booking_no.",".$recv_id;
			if($db_type==0)
			{
				$sql = "select id, product_name_details, current_stock, po_id, rack, self, yarn_lot, yarn_count, stitch_length, sum(qnty) as qnty, sum(qnty2) as qnty2 from ( select a.id, a.product_name_details, a.current_stock, group_concat(distinct(b.po_breakdown_id)) as po_id, c.rack, c.self, c.yarn_lot as yarn_lot, c.yarn_count as yarn_count, c.stitch_length, 
				sum(case when d.receive_basis=2 and d.booking_id=$txt_booking_no and b.entry_form=2 then b.quantity else 0 end ) as qnty,
				sum(case when d.receive_basis=9 and d.booking_id in($recv_id) and b.entry_form=22 then b.quantity else 0 end) as qnty2
				from product_details_master a, inv_receive_master d, order_wise_pro_details b, pro_grey_prod_entry_dtls c where a.id=b.prod_id and b.dtls_id=c.id and b.id=c.mst_id and a.current_stock>0 and a.item_category_id=13 and b.entry_form in(2,22) and b.trans_type=1 and b.po_breakdown_id in ($po_id) and b.trans_id!=0 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and d.booking_id in ($all_booking_id) group by a.id, a.product_name_details, a.current_stock, c.yarn_lot, c.yarn_count, c.stitch_length, c.rack, c.self
						union all
						select a.id, a.product_name_details, a.current_stock, group_concat(distinct(b.po_breakdown_id)) as po_id, c.to_rack as rack, c.to_shelf as self, c.yarn_lot as yarn_lot, c.y_count as yarn_count, c.stitch_length, sum(b.quantity) as qnty, 0 as qnty2 from product_details_master a, order_wise_pro_details b, inv_item_transfer_dtls c where a.id=b.prod_id and b.dtls_id=c.id and a.current_stock>0 and a.item_category_id=13 and b.entry_form in(13,81) and b.trans_type=5 and c.item_category=13 and b.po_breakdown_id in ($po_id) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.id, a.product_name_details, a.current_stock, c.yarn_lot, c.y_count, c.stitch_length, c.to_rack, c.to_shelf
						union all
						select a.id, a.product_name_details, a.current_stock, group_concat(distinct(b.po_breakdown_id)) as po_id, c.rack, c.self, c.batch_lot as yarn_lot, c.yarn_count as yarn_count, c.stitch_length, sum(b.quantity) as qnty, 0 as qnty2 from product_details_master a, order_wise_pro_details b, inv_transaction c where a.id=b.prod_id and b.trans_id=c.id and a.current_stock>0 and a.item_category_id=13 and c.transaction_type=4 and b.entry_form=51 and b.trans_type=4 and c.item_category=13 and b.po_breakdown_id in ($po_id) and b.status_active=1 and b.is_deleted=0 group by b.prod_id, c.batch_lot, c.yarn_count, c.stitch_length, c.rack, c.self
						) product_details_master group by id, po_id, rack, self, yarn_lot, yarn_count, stitch_length";
			}
			else
			{
				$sql = "select id, product_name_details, current_stock, po_id, rack, self, yarn_lot, yarn_count, stitch_length, sum(qnty) as qnty, sum(qnty2) as qnty2 from (
				select a.id, a.product_name_details, a.current_stock,LISTAGG(b.po_breakdown_id, ',') WITHIN GROUP (ORDER BY b.po_breakdown_id) as po_id, c.rack, c.self, c.yarn_lot as yarn_lot, c.yarn_count as yarn_count, c.stitch_length,
				sum(case when d.receive_basis=2 and d.booking_id=$txt_booking_no and b.entry_form=2 then b.quantity else 0 end ) as qnty,
				sum(case when d.receive_basis=9 and d.booking_id in($recv_id) and b.entry_form=22 then b.quantity else 0 end) as qnty2
				from product_details_master a, inv_receive_master d, order_wise_pro_details b, pro_grey_prod_entry_dtls c where a.id=b.prod_id and b.dtls_id=c.id and d.id=c.mst_id and a.current_stock>0 and a.item_category_id=13 and b.entry_form in(2,22) and b.trans_type=1 and b.po_breakdown_id in ($po_id) and b.trans_id!=0 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and d.booking_id in ($all_booking_id) group by a.id, a.product_name_details, a.current_stock, c.yarn_lot, c.yarn_count, c.stitch_length, c.rack, c.self
						union all
						select a.id, a.product_name_details, a.current_stock, LISTAGG(b.po_breakdown_id, ',') WITHIN GROUP (ORDER BY b.po_breakdown_id) as po_id, c.to_rack as rack, c.to_shelf as self, c.yarn_lot as yarn_lot, c.y_count as yarn_count, c.stitch_length, sum(b.quantity) as qnty, 0 as qnty2 from product_details_master a, order_wise_pro_details b, inv_item_transfer_dtls c where a.id=b.prod_id and b.dtls_id=c.id and a.current_stock>0 and a.item_category_id=13 and b.entry_form in(13,81) and b.trans_type=5 and c.item_category=13 and b.po_breakdown_id in ($po_id) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.id, a.product_name_details, a.current_stock, c.yarn_lot, c.y_count, c.stitch_length, c.to_rack, c.to_shelf
						union all
						select a.id, a.product_name_details, a.current_stock, LISTAGG(b.po_breakdown_id, ',') WITHIN GROUP (ORDER BY b.po_breakdown_id) as po_id, c.rack, c.self, c.batch_lot as yarn_lot, c.yarn_count as yarn_count, c.stitch_length, sum(b.quantity) as qnty, 0 as qnty2 from product_details_master a, order_wise_pro_details b, inv_transaction c where a.id=b.prod_id and b.trans_id=c.id and a.current_stock>0 and a.item_category_id=13 and c.transaction_type=4 and b.entry_form=51 and b.trans_type=4 and c.item_category=13 and b.po_breakdown_id in ($po_id) and b.status_active=1 and b.is_deleted=0 group by a.id, a.product_name_details, a.current_stock, c.batch_lot, c.yarn_count, c.stitch_length, c.rack, c.self
						) product_details_master group by id, product_name_details, current_stock, po_id, rack, self, yarn_lot, yarn_count, stitch_length";
			}
		}
		else
		{
			if($db_type==0)
			{
				$sql = "select id, product_name_details, current_stock, po_id, rack, self, yarn_lot, yarn_count, stitch_length, sum(qnty) as qnty from ( select a.id, a.product_name_details, a.current_stock, group_concat(distinct(b.po_breakdown_id)) as po_id, c.rack, c.self, c.yarn_lot as yarn_lot, c.yarn_count as yarn_count, c.stitch_length, sum(b.quantity) as qnty from product_details_master a, order_wise_pro_details b, pro_grey_prod_entry_dtls c where a.id=b.prod_id and b.dtls_id=c.id and a.current_stock>0 and a.item_category_id=13 and b.entry_form in(2,22) and b.trans_type=1 and b.po_breakdown_id in ($po_id) and b.trans_id!=0 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.id, a.product_name_details, a.current_stock, c.yarn_lot, c.yarn_count, c.stitch_length, c.rack, c.self
						union all
						select a.id, a.product_name_details, a.current_stock, group_concat(distinct(b.po_breakdown_id)) as po_id, c.to_rack as rack, c.to_shelf as self, c.yarn_lot as yarn_lot, c.y_count as yarn_count, c.stitch_length, sum(b.quantity) as qnty from product_details_master a, order_wise_pro_details b, inv_item_transfer_dtls c where a.id=b.prod_id and b.dtls_id=c.id and a.current_stock>0 and a.item_category_id=13 and b.entry_form in(13,81) and b.trans_type=5 and c.item_category=13 and b.po_breakdown_id in ($po_id) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.id, a.product_name_details, a.current_stock, c.yarn_lot, c.y_count, c.stitch_length, c.to_rack, c.to_shelf
						union all
						select a.id, a.product_name_details, a.current_stock, group_concat(distinct(b.po_breakdown_id)) as po_id, c.rack, c.self, c.batch_lot as yarn_lot, c.yarn_count as yarn_count, c.stitch_length, sum(b.quantity) as qnty from product_details_master a, order_wise_pro_details b, inv_transaction c where a.id=b.prod_id and b.trans_id=c.id and a.current_stock>0 and a.item_category_id=13 and c.transaction_type=4 and b.entry_form=51 and b.trans_type=4 and c.item_category=13 and b.po_breakdown_id in ($po_id) and b.status_active=1 and b.is_deleted=0 group by b.prod_id, c.batch_lot, c.yarn_count, c.stitch_length, c.rack, c.self
						) product_details_master group by id, po_id, rack, self, yarn_lot, yarn_count, stitch_length";
						
				/*$sql = "select a.id, a.product_name_details, a.current_stock, group_concat(distinct(b.po_breakdown_id)) as po_id, c.rack, c.self, c.yarn_lot as yarn_lot, c.yarn_count as yarn_count, sum(b.quantity) as qnty from product_details_master a, order_wise_pro_details b, pro_grey_prod_entry_dtls c where a.id=b.prod_id and b.dtls_id=c.id and a.current_stock>0 and a.item_category_id=13 and b.entry_form in(2,22) and b.trans_type=1 and b.po_breakdown_id in ($po_id) and b.trans_id!=0 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.id, a.product_name_details, a.current_stock, c.yarn_lot, c.yarn_count, c.rack, c.self
						union all
						select a.id, a.product_name_details, a.current_stock, group_concat(distinct(b.po_breakdown_id)) as po_id, c.to_rack as rack, c.to_shelf as self, c.yarn_lot as yarn_lot, c.y_count as yarn_count, sum(b.quantity) as qnty from product_details_master a, order_wise_pro_details b, inv_item_transfer_dtls c where a.id=b.prod_id and b.dtls_id=c.id and a.current_stock>0 and a.item_category_id=13 and b.entry_form=13 and b.trans_type=5 and c.item_category=13 and b.po_breakdown_id in ($po_id) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.id, a.product_name_details, a.current_stock, c.yarn_lot, c.y_count, c.to_rack, c.to_shelf";*/
			}
			else
			{
				$sql = "select id, product_name_details, current_stock, po_id, rack, self, yarn_lot, yarn_count, stitch_length, sum(qnty) as qnty from (
				select a.id, a.product_name_details, a.current_stock,LISTAGG(b.po_breakdown_id, ',') WITHIN GROUP (ORDER BY b.po_breakdown_id) as po_id, c.rack, c.self, c.yarn_lot as yarn_lot, c.yarn_count as yarn_count, c.stitch_length, sum(b.quantity) as qnty from product_details_master a, order_wise_pro_details b, pro_grey_prod_entry_dtls c where a.id=b.prod_id and b.dtls_id=c.id and a.current_stock>0 and a.item_category_id=13 and b.entry_form in(2,22) and b.trans_type=1 and b.po_breakdown_id in ($po_id) and b.trans_id!=0 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.id, a.product_name_details, a.current_stock, c.yarn_lot, c.yarn_count, c.stitch_length, c.rack, c.self
						union all
						select a.id, a.product_name_details, a.current_stock, LISTAGG(b.po_breakdown_id, ',') WITHIN GROUP (ORDER BY b.po_breakdown_id) as po_id, c.to_rack as rack, c.to_shelf as self, c.yarn_lot as yarn_lot, c.y_count as yarn_count, c.stitch_length, sum(b.quantity) as qnty from product_details_master a, order_wise_pro_details b, inv_item_transfer_dtls c where a.id=b.prod_id and b.dtls_id=c.id and a.current_stock>0 and a.item_category_id=13 and b.entry_form in(13,81) and b.trans_type=5 and c.item_category=13 and b.po_breakdown_id in ($po_id) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.id, a.product_name_details, a.current_stock, c.yarn_lot, c.y_count, c.stitch_length, c.to_rack, c.to_shelf
						union all
						select a.id, a.product_name_details, a.current_stock, LISTAGG(b.po_breakdown_id, ',') WITHIN GROUP (ORDER BY b.po_breakdown_id) as po_id, c.rack, c.self, c.batch_lot as yarn_lot, c.yarn_count as yarn_count, c.stitch_length, sum(b.quantity) as qnty from product_details_master a, order_wise_pro_details b, inv_transaction c where a.id=b.prod_id and b.trans_id=c.id and a.current_stock>0 and a.item_category_id=13 and c.transaction_type=4 and b.entry_form=51 and b.trans_type=4 and c.item_category=13 and b.po_breakdown_id in ($po_id) and b.status_active=1 and b.is_deleted=0 group by a.id, a.product_name_details, a.current_stock, c.batch_lot, c.yarn_count, c.stitch_length, c.rack, c.self
						) product_details_master group by id, product_name_details, current_stock, po_id, rack, self, yarn_lot, yarn_count, stitch_length";
			}
		
			if($db_type==0)
			{
				$program_data=sql_select("select b.prod_id, c.po_breakdown_id, group_concat(a.booking_id) as program_no from inv_receive_master a, pro_grey_prod_entry_dtls b, order_wise_pro_details c where a.id=b.mst_id and b.id=c.dtls_id and a.entry_form=2 and a.receive_basis=2 and c.entry_form=2 and c.trans_type=1 and c.po_breakdown_id in ($po_id) and c.status_active=1 and c.is_deleted=0 group by b.prod_id, c.po_breakdown_id");
			}
			else
			{
				$program_data=sql_select("select b.prod_id, c.po_breakdown_id, LISTAGG(cast(a.booking_id as VARCHAR2(4000)), ',') WITHIN GROUP (ORDER BY a.booking_id) as program_no from inv_receive_master a, pro_grey_prod_entry_dtls b, order_wise_pro_details c where a.id=b.mst_id and b.id=c.dtls_id and a.entry_form=2 and a.receive_basis=2 and c.entry_form=2 and c.trans_type=1 and c.po_breakdown_id in ($po_id) and c.status_active=1 and c.is_deleted=0 group by b.prod_id, c.po_breakdown_id");
			}
			
			foreach($program_data as $pRow)
			{
				$program_no_arr[$pRow[csf('prod_id')]][$pRow[csf('po_breakdown_id')]]=$pRow[csf('program_no')];
			}
		}
	}
	else if($po_id=="" && ($cbo_issue_purpose==8 || $cbo_issue_purpose==3 || $cbo_issue_purpose==26 || $cbo_issue_purpose==29 || $cbo_issue_purpose==30 || $cbo_issue_purpose==31))
	{
		if($db_type==0)
		{
			$recv_id=return_field_value("group_concat(distinct(id)) as id","inv_receive_master","booking_without_order=1 and booking_id=$txt_booking_id and status_active=1 and is_deleted=0 and entry_form in(2)","id");
		}
		else
		{
			$recv_id=return_field_value("LISTAGG(id, ',') WITHIN GROUP (ORDER BY id) as id","inv_receive_master","booking_without_order=1 and booking_id=$txt_booking_id and status_active=1 and is_deleted=0 and entry_form in(2)","id");
		}
		
		//if($recv_id=="") $booking_id_cond=" and b.booking_id=$txt_booking_id";
		//else $booking_id_cond=" and (b.booking_id=$txt_booking_id and receive_basis!=9) or (receive_basis=9 and b.booking_id in ($recv_id))";
		if($recv_id=="")
		{
			$sql = "select id, product_name_details, current_stock, rack, self, yarn_lot, yarn_count, stitch_length, sum(qnty) as qnty, sum(qnty2) as qnty2 from (
					select a.id, a.product_name_details, a.current_stock, c.rack, c.self, c.yarn_lot as yarn_lot, c.yarn_count as yarn_count, c.stitch_length, sum(c.grey_receive_qnty) as qnty, 0 as qnty2 from product_details_master a, inv_receive_master b, pro_grey_prod_entry_dtls c where a.id=c.prod_id and b.id=c.mst_id and a.current_stock>0 and a.item_category_id=13 and b.entry_form in(2,22) and b.booking_without_order=1 and c.trans_id!=0 and b.booking_id=$txt_booking_id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.id, a.product_name_details, a.current_stock, b.booking_id, c.yarn_lot, c.yarn_count, c.stitch_length, c.rack, c.self
					union all
					select a.id, a.product_name_details, a.current_stock, c.rack, c.self, c.batch_lot as yarn_lot, c.yarn_count as yarn_count, c.stitch_length, sum(c.cons_quantity) as qnty, 0 as qnty2 from product_details_master a, inv_receive_master b, inv_transaction c where a.id=c.prod_id and b.id=c.mst_id and a.current_stock>0 and a.item_category_id=13 and b.entry_form=51 and b.receive_purpose=8 and c.item_category=13 and c.transaction_type=4 and b.booking_id=$txt_booking_id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.id, a.product_name_details, a.current_stock, b.booking_id, c.batch_lot, c.yarn_count, c.stitch_length, c.rack, c.self
					union all
					select a.id, a.product_name_details, a.current_stock, c.to_rack as rack, c.to_shelf as self, c.yarn_lot as yarn_lot, c.y_count as yarn_count, c.stitch_length, sum(c.transfer_qnty) as qnty, 0 as qnty2 from inv_item_transfer_mst b, inv_item_transfer_dtls c, product_details_master a where b.id=c.mst_id and c.from_prod_id=a.id and a.current_stock>0 and a.item_category_id=13 and b.transfer_criteria=6 and c.item_category=13 and b.to_order_id=$txt_booking_id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.id, a.product_name_details, a.current_stock, c.yarn_lot, c.y_count, c.stitch_length, c.to_rack, c.to_shelf
					) product_details_master group by id, product_name_details, current_stock, rack, self, yarn_lot, yarn_count, stitch_length";
		}
		else
		{
			$all_booking_id=$txt_booking_id.",".$recv_id;
			$sql = "select id, product_name_details, current_stock, rack, self, yarn_lot, yarn_count, stitch_length, sum(qnty) as qnty, sum(qnty2) as qnty2 from (
				select a.id, a.product_name_details, a.current_stock, c.rack, c.self, c.yarn_lot as yarn_lot, c.yarn_count as yarn_count, c.stitch_length, 
				sum(case when b.receive_basis!=9 and b.booking_id=$txt_booking_id then c.grey_receive_qnty else 0 end ) as qnty,
				sum(case when b.receive_basis=9 and b.booking_id in($recv_id) then c.grey_receive_qnty else 0 end) as qnty2
			from product_details_master a, inv_receive_master b, pro_grey_prod_entry_dtls c where a.id=c.prod_id and b.id=c.mst_id and a.current_stock>0 and a.item_category_id=13 and b.entry_form in(2,22) and b.booking_without_order=1 and c.trans_id!=0 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and b.booking_id in ($all_booking_id) group by a.id, a.product_name_details, a.current_stock, c.yarn_lot, c.yarn_count, c.stitch_length, c.rack, c.self
					union all
					select a.id, a.product_name_details, a.current_stock, c.rack, c.self, c.batch_lot as yarn_lot, c.yarn_count as yarn_count, c.stitch_length, sum(c.cons_quantity) as qnty, 0 as qnty2 from product_details_master a, inv_receive_master b, inv_transaction c where a.id=c.prod_id and b.id=c.mst_id and a.current_stock>0 and a.item_category_id=13 and b.entry_form=51 and b.receive_purpose=8 and c.item_category=13 and c.transaction_type=4 and b.booking_id=$txt_booking_id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.id, a.product_name_details, a.current_stock, c.batch_lot, c.yarn_count, c.stitch_length, c.rack, c.self
					union all
					select a.id, a.product_name_details, a.current_stock, c.to_rack as rack, c.to_shelf as self, c.yarn_lot as yarn_lot, c.y_count as yarn_count, c.stitch_length, sum(c.transfer_qnty) as qnty, 0 as qnty2 from inv_item_transfer_mst b, inv_item_transfer_dtls c, product_details_master a where b.id=c.mst_id and c.from_prod_id=a.id and a.current_stock>0 and a.item_category_id=13 and c.item_category=13 and b.transfer_criteria=6 and b.to_order_id=$txt_booking_id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.id, a.product_name_details, a.current_stock, c.yarn_lot, c.y_count, c.stitch_length, c.to_rack, c.to_shelf
					) product_details_master group by id, product_name_details, current_stock, rack, self, yarn_lot, yarn_count, stitch_length";
		}
	}
	else
	{
		echo "No Order Found Against this Booking/Program No.";die;
	}
	
	//echo $sql;//die;
	$result = sql_select($sql);
	if(count($result)<1) {echo "No Production Found.";die;}
	
 	$yarn_count = return_library_array("select id, yarn_count from lib_yarn_count",'id','yarn_count');	
	$buyer_arr = return_library_array( "select id, short_name from lib_buyer",'id','short_name');
	
	$po_array=array(); $grey_iss_array=array();
	if (($cbo_issue_purpose==8 || $cbo_issue_purpose==3 || $cbo_issue_purpose==26 || $cbo_issue_purpose==29 || $cbo_issue_purpose==30 || $cbo_issue_purpose==31) && $cbo_basis==1)
	{
		$po_sql=sql_select("select a.id, a.buyer_id as buyer_name, b.style_id as style_ref_no, '' as po_number, '' as job_no from wo_non_ord_samp_booking_mst a, wo_non_ord_samp_booking_dtls b where a.booking_no=b.booking_no and a.company_id=$cbo_company_id");
		if($recv_id=="")
		{
			$iss_sql=sql_select("select c.prod_id as id, b.booking_id, c.rack, c.self, c.yarn_lot, c.yarn_count, c.stitch_length, sum(c.issue_qnty) as qnty from inv_issue_master b, inv_grey_fabric_issue_dtls c where b.id=c.mst_id and b.item_category=13 and b.entry_form=16 and b.issue_purpose in(3,8,26,29,30,31) and b.issue_basis=1 and b.booking_id=$txt_booking_id and b.status_active=1 and b.is_deleted=0 group by c.prod_id, b.booking_id, c.yarn_lot, c.yarn_count, c.stitch_length, c.rack, c.self
							union all
							select c.prod_id as id, d.booking_id, c.rack, c.self, c.batch_lot as yarn_lot, c.yarn_count as yarn_count, c.stitch_length, sum(c.cons_quantity) as qnty from inv_issue_master b, inv_transaction c, inv_receive_master d where b.id=c.mst_id and b.received_id=d.id and d.booking_id=$txt_booking_id and d.booking_without_order=1 and b.item_category=13 and b.entry_form=45 and c.transaction_type=3 and c.item_category=13 group by c.prod_id, d.booking_id, c.batch_lot, c.yarn_count, c.stitch_length, c.rack, c.self
							union all
							select c.from_prod_id as id, b.from_order_id as booking_id, c.to_rack as rack, c.to_shelf as self, c.yarn_lot as yarn_lot, c.y_count as yarn_count, c.stitch_length, sum(c.transfer_qnty) as qnty from inv_item_transfer_mst b, inv_item_transfer_dtls c where b.id=c.mst_id and b.transfer_criteria=7 and c.item_category=13 and b.from_order_id=$txt_booking_id and b.status_active=1 and b.is_deleted=0 group by c.from_prod_id, b.from_order_id, c.yarn_lot, c.y_count, c.stitch_length, c.to_rack, c.to_shelf");
		}
		else
		{
			$iss_sql=sql_select("select c.prod_id as id, b.booking_id, c.rack, c.self, c.yarn_lot, c.yarn_count, c.stitch_length, sum(c.issue_qnty) as qnty from inv_issue_master b, inv_grey_fabric_issue_dtls c where b.id=c.mst_id and b.item_category=13 and b.entry_form=16 and b.issue_purpose in(3,8,26,29,30,31) and b.issue_basis=1 and b.booking_id=$txt_booking_id and b.status_active=1 and b.is_deleted=0 group by c.prod_id, b.booking_id, c.yarn_lot, c.yarn_count, c.stitch_length, c.rack, c.self
							union all
							select c.prod_id as id, d.booking_id, c.rack, c.self, c.batch_lot as yarn_lot, c.yarn_count as yarn_count, c.stitch_length, sum(c.cons_quantity) as qnty from inv_issue_master b, inv_transaction c, inv_receive_master d where b.id=c.mst_id and b.received_id=d.id and ((d.booking_id=$txt_booking_id and d.receive_basis!=9) or (d.booking_id in ($recv_id) and d.receive_basis=9)) and d.booking_without_order=1 and b.item_category=13 and b.entry_form=45 and c.transaction_type=3 and c.item_category=13 group by c.prod_id, d.booking_id, c.batch_lot, c.yarn_count, c.stitch_length, c.rack, c.self
							union all
							select c.from_prod_id as id, b.from_order_id as booking_id, c.to_rack as rack, c.to_shelf as self, c.yarn_lot as yarn_lot, c.y_count as yarn_count, c.stitch_length, sum(c.transfer_qnty) as qnty from inv_item_transfer_mst b, inv_item_transfer_dtls c where b.id=c.mst_id and b.transfer_criteria=7 and c.item_category=13 and b.from_order_id=$txt_booking_id and b.status_active=1 and b.is_deleted=0 group by c.from_prod_id, b.from_order_id, c.yarn_lot, c.y_count, c.stitch_length, c.to_rack, c.to_shelf");
		}
		
		foreach($iss_sql as $row)
		{	//$row[csf('booking_id')];
			$grey_iss_array[$row[csf('id')]][$txt_booking_id][$row[csf('yarn_lot')]][$row[csf('yarn_count')]][$row[csf('rack')]][$row[csf('self')]][$row[csf('stitch_length')]]+=$row[csf('qnty')];
		}
	}
	else
	{
		$po_sql=sql_select("select a.buyer_name, a.job_no, a.style_ref_no, b.id, b.po_number from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.company_name=$cbo_company_id");
		
		/*$iss_sql=sql_select("select a.id, c.rack, c.self, c.yarn_lot, c.yarn_count, sum(b.quantity) as qnty from product_details_master a, order_wise_pro_details b, inv_grey_fabric_issue_dtls c where a.id=b.prod_id and b.dtls_id=c.id and a.item_category_id=13 and b.entry_form=16 and b.trans_type=2 and b.po_breakdown_id in ($po_id) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.id, c.yarn_lot, c.yarn_count, c.rack, c.self
				union all
				select a.id, c.to_rack as rack, c.to_shelf as self, c.yarn_lot as yarn_lot, c.y_count as yarn_count, sum(b.quantity) as qnty from product_details_master a, order_wise_pro_details b, inv_item_transfer_dtls c where a.id=b.prod_id and b.dtls_id=c.id and a.current_stock>0 and a.item_category_id=13 and b.entry_form=13 and b.trans_type=6 and c.item_category=13 and b.po_breakdown_id in ($po_id) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.id, c.yarn_lot, c.y_count, c.to_rack, c.to_shelf");*/

		$iss_sql=sql_select("select c.prod_id as id, c.rack, c.self, c.yarn_lot, c.yarn_count, c.stitch_length, sum(b.quantity) as qnty from order_wise_pro_details b, inv_grey_fabric_issue_dtls c where b.dtls_id=c.id and b.entry_form=16 and b.trans_type=2 and b.po_breakdown_id in ($po_id) and b.status_active=1 and b.is_deleted=0 group by c.prod_id, c.yarn_lot, c.yarn_count, c.stitch_length, c.rack, c.self
				union all
				select b.prod_id as id, c.to_rack as rack, c.to_shelf as self, c.yarn_lot as yarn_lot, c.y_count as yarn_count, c.stitch_length, sum(b.quantity) as qnty from order_wise_pro_details b, inv_item_transfer_dtls c where b.dtls_id=c.id and b.entry_form in(13,80) and b.trans_type=6 and c.item_category=13 and b.po_breakdown_id in ($po_id) and b.status_active=1 and b.is_deleted=0 group by b.prod_id, c.yarn_lot, c.y_count, c.stitch_length, c.to_rack, c.to_shelf
				union all
				select b.prod_id as id, c.rack, c.self, c.batch_lot as yarn_lot, c.yarn_count as yarn_count, c.stitch_length, sum(b.quantity) as qnty from order_wise_pro_details b, inv_transaction c where b.trans_id=c.id and c.transaction_type=3 and b.entry_form=45 and b.trans_type=3 and c.item_category=13 and b.po_breakdown_id in ($po_id) and b.status_active=1 and b.is_deleted=0 group by b.prod_id, c.batch_lot, c.yarn_count, c.stitch_length, c.rack, c.self");		
		foreach($iss_sql as $row)
		{
			$grey_iss_array[$row[csf('id')]][$row[csf('yarn_lot')]][$row[csf('yarn_count')]][$row[csf('rack')]][$row[csf('self')]][$row[csf('stitch_length')]]+=$row[csf('qnty')];
		}	
		//print_r($grey_iss_array);	
	}
	foreach($po_sql as $row)
	{
		$po_array[$row[csf('id')]]['no']=$row[csf('po_number')];
		$po_array[$row[csf('id')]]['style_ref']=$row[csf('style_ref_no')];
		$po_array[$row[csf('id')]]['buyer']=$row[csf('buyer_name')];
		$po_array[$row[csf('id')]]['job_no']=$row[csf('job_no')];
	}
	
	echo "<div align=\"center\" style=\"width:100%\">";
	echo "<input type=\"hidden\" id=\"txt_selected_id\" />\n";
	?>
    <table cellspacing="0" cellpadding="0" border="1" rules="all" width="960" class="rpt_table">
        <thead>
            <th width="30">SL</th>
            <th width="130">Item Description</th>
            <th width="80">Order No</th>
            <th width="50">Buyer</th>
            <th width="80">Job</th>              
            <th width="90">Style Ref.</th>
            <th width="60">Program No</th>
            <th width="60">Stitch Length</th>
            <th width="70">Yarn Lot</th>
            <th width="90">Count</th>
            <th width="60">Rack</th>
            <th width="60">Shelf</th>
            <th>Balance Qty.</th>
        </thead>
    </table>
    <div style="width:960px; max-height:310px; overflow-y:scroll" id="container_batch" >
		<table cellspacing="0" cellpadding="0" border="1" rules="all" width="942" class="rpt_table" id="tbl_search" >  
        <?php
            $i=1;
            foreach ($result as $row)
            {  
                if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";	
				
				$iss_qnty=0; $program_no=''; $recv_qnty=$row[csf('qnty')]+$row[csf('qnty2')];
				
				if(($cbo_issue_purpose==8 || $cbo_issue_purpose==3 || $cbo_issue_purpose==26 || $cbo_issue_purpose==29 || $cbo_issue_purpose==30 || $cbo_issue_purpose==31) && $cbo_basis==1)
				{
					$row[csf('booking_id')]=$txt_booking_id;// balance is showing against sample booking bokking Id is different when receive basis production
					$job_arr="";
					$style_ref=$po_array[$row[csf('booking_id')]]['style_ref'];
					$buyer_name=$buyer_arr[$po_array[$row[csf('booking_id')]]['buyer']]; 
					$iss_qnty=$grey_iss_array[$row[csf('id')]][$row[csf('booking_id')]][$row[csf('yarn_lot')]][$row[csf('yarn_count')]][$row[csf('rack')]][$row[csf('self')]][$row[csf('stitch_length')]];
				}
				//if($cbo_issue_purpose!=8)
				else
				{	
					$po_id=explode(",",$row[csf('po_id')]);
					$po_no=''; $job_no_array=array(); $buyer_name=''; $style_ref='';
					
					foreach($po_id as $val)
					{
						if($po_no=='') $po_no=$po_array[$val]['no']; else $po_no.=", ".$po_array[$val]['no'];
	
						if(!in_array($po_array[$val]['job_no'],$job_no_array))
						{
							$job_no_array[]=$po_array[$val]['job_no'];
							if($buyer_name=='') $buyer_name=$buyer_arr[$po_array[$val]['buyer']]; else $buyer_name.=",".$buyer_arr[$po_array[$val]['buyer']];
							if($style_ref=='') $style_ref=$po_array[$val]['style_ref']; else $style_ref.=",".$po_array[$val]['style_ref'];
						}
						
						if($program_no_arr[$row[csf('id')]][$val]>0)
						{
							$program_no.=$program_no_arr[$row[csf('id')]][$val].",";
						}
					}	 
					$job_arr=implode(",",$job_no_array); 
					$iss_qnty=$grey_iss_array[$row[csf('id')]][$row[csf('yarn_lot')]][$row[csf('yarn_count')]][$row[csf('rack')]][$row[csf('self')]][$row[csf('stitch_length')]];
					$program_no=implode(",",array_unique(explode(",",substr($program_no,0,-1))));
				}
				/*else
				{
					$row[csf('booking_id')]=$txt_booking_id;// balance is showing against sample booking bokking Id is different when receive basis production
					$recv_qnty+=$row[csf('qnty2')];
					$job_arr="";
					$style_ref=$po_array[$row[csf('booking_id')]]['style_ref'];
					$buyer_name=$buyer_arr[$po_array[$row[csf('booking_id')]]['buyer']]; 
					$iss_qnty=$grey_iss_array[$row[csf('id')]][$row[csf('booking_id')]][$row[csf('yarn_lot')]][$row[csf('yarn_count')]][$row[csf('rack')]][$row[csf('self')]][$row[csf('stitch_length')]];
				}*/
				
				$count=''; $count_id=array_unique(explode(",",$row[csf('yarn_count')]));
				foreach($count_id as $val)
				{
					if($count=='') $count=$yarn_count[$val]; else $count.=",".$yarn_count[$val];
				}
				
				$yarn_lot=implode(",",array_unique(explode(",",$row[csf('yarn_lot')])));
				$data=$row[csf('id')]."_".$row[csf('product_name_details')]."_".$yarn_lot."_".implode(",",$count_id)."_".$row[csf('rack')]."_".$row[csf('self')]."_".$row[csf('current_stock')]."_".$row[csf('stitch_length')];
				$buyer_name=implode(",",array_unique(explode(",",$buyer_name)));
				$balance_qnty=$recv_qnty-$iss_qnty;
            ?>
                <tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" onClick="js_set_value('<?php echo $data; ?>')"> 
                    <td width="30"><?php echo $i; ?></td>
                    <td width="130"><p><?php echo $row[csf('product_name_details')]; ?></p></td>
                    <td width="80"><p><?php echo $po_no; ?></p></td>  
                    <td width="50" align="center"><p><?php echo $buyer_name; ?>&nbsp;</p></td>
                    <td width="80" align="center"><p><?php echo $job_arr; ?>&nbsp;</p></td> 
                    <td width="90"><p><?php echo $style_ref; ?>&nbsp;</p></td>
                    <td width="60"><p><?php echo $program_no; ?>&nbsp;</p></td> 
                    <td width="60"><p><?php echo $row[csf('stitch_length')]; ?>&nbsp;</p></td>
                    <td width="70"><p><?php echo $yarn_lot; ?>&nbsp;</p></td>
                    <td width="90"><p><?php echo $count; ?>&nbsp;</p></td>
                    <td width="60"><p><?php echo $row[csf('rack')]; ?>&nbsp;</p></td>
                    <td width="60"><p><?php echo $row[csf('self')]; ?>&nbsp;</p></td>
                    <td align="right"><?php echo number_format($balance_qnty,2); ?>&nbsp;</td>
                </tr>
            <?php
            $i++;
            }
            ?>
        </table>
    </div>
    <?php
	exit();	
	
} 
 
// child form data populate after call after close item description pop up
if($action=="populate_child_from_data_item_desc") 
{
	$yarn_count = return_library_array("select id, yarn_count from lib_yarn_count",'id','yarn_count');	
	$prod_id = $data;	 
	//echo $prod_id; 
	$sql = "select id,product_name_details,current_stock
			from product_details_master
			where id=$prod_id";				
				
	//echo $sql;die;
	$result = sql_select($sql);  
	foreach($result as $row)
	{ 
		if($db_type==0)
		{
			$lot=return_field_value("group_concat(distinct(yarn_lot)) as lot","pro_grey_prod_entry_dtls","prod_id='".$row[csf('id')]."' and yarn_lot<>'' and status_active=1 and is_deleted=0","lot");
			$count_id=return_field_value("group_concat(distinct(yarn_count)) as count","pro_grey_prod_entry_dtls","prod_id='".$row[csf('id')]."' and yarn_count<>'' and yarn_count<>0 and status_active=1 and is_deleted=0","count");
		}
		else
		{							
			$lot=return_field_value("LISTAGG(cast(yarn_lot as varchar2(4000)), ',') WITHIN GROUP (ORDER BY yarn_lot) as lot","pro_grey_prod_entry_dtls","prod_id='".$row[csf('id')]."' and yarn_lot is not null and status_active=1 and is_deleted=0","lot");
			$count_id=return_field_value("LISTAGG(cast(yarn_count as varchar2(4000)), ',') WITHIN GROUP (ORDER BY yarn_count)","pro_grey_prod_entry_dtls","prod_id='".$row[csf('id')]."' and yarn_count is not null and yarn_count<>0 and status_active=1 and is_deleted=0","count");	
			
			$lot=implode(",",array_unique(explode(",",$lot)));
		}
		$count_id=implode(",",array_unique(explode(",",$count_id)));
		/*$count_id=array_unique(explode(",",$count_id)); $count='';
		foreach($count_id as $val)
		{
			if($count=='') $count=$val; else $count.=",".$val;
		}*/
		
 		echo "$('#txtItemDescription').val('".$row[csf('product_name_details')]."');\n";
		echo "$('#hiddenProdId').val('".$row[csf('id')]."');\n";
		echo "$('#txtYarnLot').val('".$lot."');\n";
		//echo "$('#cbo_yarn_count').val('".$count."');\n"; 
		echo "set_multiselect('cbo_yarn_count','0','1','".$count."','0');\n";
		//echo "disable_enable_fields('show_textcbo_yarn_count','1','','');\n";
	}
	
	exit();
		
}

if ($action=="po_popup")
{
	echo load_html_head_contents("PO Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);  
	$prev_method=$distribution_method;  
	$issueQnty=$issueQnty; 
	
	if($isRoll==1) $readonlyCond="readonly"; else $readonlyCond="";
	 
?> 
	<script> 
		var receive_basis=<?php echo $receive_basis; ?>;
		
		function distribute_qnty(str)
		{ 
			if(str==1)
			{
				var tot_po_qnty=$('#tot_po_qnty').val()*1;
				var txt_prop_grey_qnty=$('#txt_prop_grey_qnty').val()*1;
				var tblRow = $("#tbl_search tr").length;
				var len=totalGrey=0;
				
				$("#tbl_search").find('tr').each(function()
				{
					len=len+1;
					
					var txtOrginal=$(this).find('input[name="txtOrginal[]"]').val()*1;
					if(txtOrginal==0)
					{
						$(this).remove();
					}
					else
					{
						var po_qnty=$(this).find('input[name="txtPoQnty[]"]').val()*1;
						var perc=(po_qnty/tot_po_qnty)*100;						
						var grey_qnty=(perc*txt_prop_grey_qnty)/100;
						totalGrey = totalGrey*1+grey_qnty*1;
						totalGrey = totalGrey.toFixed(2);						
						if(tblRow==len)
						{
							var balance = txt_prop_grey_qnty-totalGrey;
							if(balance!=0) grey_qnty=grey_qnty+(balance);							
						}
						
						if(grey_qnty>0)
						{
							$(this).find('input[name="txtGreyQnty[]"]').val(grey_qnty.toFixed(2));
						}
						else
						{
							$(this).find('input[name="txtGreyQnty[]"]').val(grey_qnty.toFixed(2));
						}
					}
				}); 
			}
			else
			{
				$('#txt_prop_grey_qnty').val('');
				$("#tbl_search").find('tr').each(function()
				{
					$(this).find('input[name="txtGreyQnty[]"]').val('');
				});
			}
			
			sum_total();
		}
		
				
		var selected_id = new Array();
		
		function check_all_data() 
		 {
			var tbl_row_count = document.getElementById( 'tbl_search' ).rows.length; 

			tbl_row_count = tbl_row_count-1;
			for( var i = 1; i <= tbl_row_count; i++ ) {
				js_set_value( i );
			}
		}
		
		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
		function set_all()
		{
			var old=document.getElementById('txt_po_row_id').value; 
			if(old!="")
			{   
				old=old.split(",");
				for(var k=0; k<old.length; k++)
				{   
					js_set_value( old[k] ) 
				} 
			}
		}
		
		function js_set_value( str ) {
			toggle( document.getElementById( 'search' + str ), '#FFFFCC' );
			
			if( jQuery.inArray( $('#txt_individual_id' + str).val(), selected_id ) == -1 ) {
				selected_id.push( $('#txt_individual_id' + str).val() );
			}
			else {
				for( var i = 0; i < selected_id.length; i++ ) {
					if( selected_id[i] == $('#txt_individual_id' + str).val() ) break;
				}
				selected_id.splice( i, 1 );
			}
			var id = '';
			for( var i = 0; i < selected_id.length; i++ ) {
				id += selected_id[i] + ',';
			}
			id = id.substr( 0, id.length - 1 );
			
			$('#po_id').val( id );
			
		}
		
		function fnc_close()
		{
			
			var checkEqual =  $("#txt_total_sum").val()*1-$("#txt_prop_grey_qnty").val()*1;
			//if( checkEqual!=0 ){ alert("Issue Qnty and Sum Qnty Not Match"); return; }
			
			var save_string='';	 var tot_grey_qnty=0; var no_of_roll=''; 
			var po_id_array = new Array();
			
			$("#tbl_search").find('tr').each(function()
			{
				var txtPoId=$(this).find('input[name="txtPoId[]"]').val();
				var txtGreyQnty=$(this).find('input[name="txtGreyQnty[]"]').val();				
				tot_grey_qnty=tot_grey_qnty*1+txtGreyQnty*1;
				 			
				if(txtGreyQnty*1>0)
				{
					if(save_string=="")
					{
						save_string=txtPoId+"**"+txtGreyQnty;
					}
					else
					{
						save_string+=","+txtPoId+"**"+txtGreyQnty;
					}
					
					if( jQuery.inArray( txtPoId, po_id_array) == -1 ) 
					{
						po_id_array.push(txtPoId);
					}
				}
			});
			
			if(save_string=="")
			{
				alert("Please Select At Least One Item");
				return;
			}
			tot_grey_qnty=tot_grey_qnty.toFixed(2);
			$('#save_string').val( save_string );
			$('#tot_grey_qnty').val( tot_grey_qnty );
			$('#all_po_id').val( po_id_array );
			$('#distribution_method').val( $('#cbo_distribiution_method').val() );			
			parent.emailwindow.hide();
		}
		
		function sum_total()
		{
			var tblRow = $("#tbl_search tr").length; 
			var ddd={dec_type:1}
			math_operation( "txt_total_sum", "txtGreyQnty_", "+", tblRow, ddd );
		}
		
		$(document).ready(function(e) {
           // distribute_qnty($('#cbo_distribiution_method').val());
			sum_total();
        });
		
    </script>

</head>
<body>
	
<form name="searchdescfrm"  id="searchdescfrm">
    <fieldset style="width:620px;margin-left:5px">
        <!-- previous data here---->
        <input type="hidden" name="prev_save_string" id="prev_save_string" class="text_boxes" value="<?php echo $save_data; ?>">
        <input type="hidden" name="prev_total_qnty" id="prev_total_qnty" class="text_boxes" value="<?php echo $issueQnty; ?>">
        <input type="hidden" name="prev_method" id="prev_method" class="text_boxes" value="<?php echo $distribution_method; ?>">
        <!--- END-------->
        <input type="hidden" name="save_string" id="save_string" class="text_boxes" value="<?php echo $save_data; ?>">
        <input type="hidden" name="tot_grey_qnty" id="tot_grey_qnty" class="text_boxes" value="">
        <input type="hidden" name="all_po_id" id="all_po_id" class="text_boxes" value="">
        <input type="hidden" name="distribution_method" id="distribution_method" class="text_boxes" value="">
	<?php  
	if($receive_basis==1)
	{
		if( $issue_purpose==3 )
		{
			$reqQnty = "select a.id, sum(b.grey_fabric) as fabric_qty from wo_non_ord_samp_booking_mst a, wo_non_ord_samp_booking_dtls b where a.booking_no=b.booking_no and a.item_category=2 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.booking_no='$booking_no' group by a.id"; 
		}
		//else if( $issue_purpose==8)
		//{					
			//$reqQnty = "select job_no_id, sum(yarn_wo_qty) as fabric_qty from wo_non_ord_samp_booking_mst where status_active=1 and is_deleted=0 and booking_no='$booking_no' group by job_no_id"; 
		//}
		else
		{
			$reqQnty = "select po_break_down_id, sum(grey_fab_qnty) as fabric_qty from wo_booking_dtls where status_active=1 and is_deleted=0 and booking_no='$booking_no' group by po_break_down_id"; 
		}
		$reqQnty_res = sql_select($reqQnty);
		foreach($reqQnty_res as $req_val)
		{
			$req_qty_array[$req_val[csf('po_break_down_id')]]=$req_val[csf('fabric_qty')];
		}
	}
	else if($receive_basis==3)
	{
		$req_booking_sql=sql_select("select c.booking_no from ppl_planning_info_entry_dtls a, ppl_yarn_requisition_entry b, ppl_planning_info_entry_mst c where a.mst_id= c.id and a.id=b.knit_id and b.knit_id='$program_no' and a.status_active=1 and a.is_deleted=0 and b.is_deleted=0 and b.is_deleted=0 group by c.booking_no");
		$req_booking=$req_booking_sql[0][csf('booking_no')];	
				
		if( $issue_purpose==3 )
		{
			$reqQnty = "select a.id, sum(b.grey_fabric) as fabric_qty from wo_non_ord_samp_booking_mst a, wo_non_ord_samp_booking_dtls b where a.booking_no=b.booking_no and a.item_category=2 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.booking_no='$req_booking' group by a.id"; 
		}
/*		else if( $issue_purpose==8)
		{					
			$reqQnty = "select job_no_id, sum(yarn_wo_qty) as fabric_qty from wo_yarn_dyeing_dtls where status_active=1 and is_deleted=0 and ydw_no='$req_booking' group by job_no_id"; 
		}
*/		else
		{
			$reqQnty = "select po_break_down_id, sum(grey_fab_qnty) as fabric_qty from wo_booking_dtls where status_active=1 and is_deleted=0 and booking_no='$req_booking' group by po_break_down_id"; 
		}
		$reqQnty_res = sql_select($reqQnty);
		foreach($reqQnty_res as $req_val)
		{
			$req_qty_array[$req_val[csf('po_break_down_id')]]=$req_val[csf('fabric_qty')];
		}
	}	
	
	 
	if($receive_basis==2 || $receive_basis==3)
	{
	?>
		<div id="search_div" style="margin-top:10px">
            <div style="width:600px; margin-top:10px" align="center">
                <table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="300" align="center">
                    <thead>
                        <th>Total Issue Qnty</th>
                        <th>Distribution Method</th>
                    </thead>
                    <tr class="general">
                        <td><input type="text" name="txt_prop_grey_qnty" id="txt_prop_grey_qnty" class="text_boxes_numeric" value="<?php if($prev_method==1) echo $issueQnty; ?>" style="width:120px"  onBlur="distribute_qnty(document.getElementById('cbo_distribiution_method').value)"  <?php echo $readonlyCond; ?> /></td>
                        <td>
                            <?php
                                $distribiution_method=array(1=>"Proportionately",2=>"Manually");
                                echo create_drop_down( "cbo_distribiution_method", 160, $distribiution_method,"",0,"--Select--",$prev_method,"distribute_qnty(this.value);",0 );
                            ?>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="margin-left:1px; margin-top:10px">
                <table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="610">
                    <thead>
                        <th width="110">PO No</th>
                        <th width="70">Ship. Date</th>
                        <th width="80">Gmts. Qty</th>
                        <th width="80">Req. Qty</th>
                        <th width="80">Prod. Qty</th>
                        <th width="80">Cumu. Issued Qty</th>
                        <th>Issue Qty</th>
                    </thead>
                </table>
                <div style="width:630px; max-height:220px; overflow-y:scroll" id="container" align="left">  
                    <table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="610" id="tbl_search">   
                <?php
                    $i=1; $tot_po_qnty=0;
                    if($hidden_order_id!="")
                    {
                        $po_sql="select b.id, b.po_number, (a.total_set_qnty*b.po_quantity) as po_qnty_in_pcs, b.pub_shipment_date
                        from wo_po_details_master a, wo_po_break_down b 
                        where a.job_no=b.job_no_mst and b.id in ($hidden_order_id)";
                        
                        $propData=sql_select("select po_breakdown_id, sum(case when entry_form in(2,22) then quantity end) as grey_fabric_recv, sum(case when entry_form in(16) then quantity end) as grey_fabric_issued, sum(case when entry_form=45 then quantity end) as grey_fabric_recv_return, sum(case when entry_form=51 then quantity end) as grey_fabric_issue_return, sum(case when entry_form in(13) and trans_type=5 then quantity end) as grey_fabric_trans_recv, sum(case when entry_form in(13) and trans_type=6 then quantity end) as grey_fabric_trans_issued from order_wise_pro_details where trans_id<>0 and prod_id=$prod_id and po_breakdown_id in($hidden_order_id) and is_deleted=0 and status_active=1 group by po_breakdown_id");
                        foreach($propData as $prop_row)
                        {
                            $propDataArray[$prop_row[csf('po_breakdown_id')]]['recv']=$prop_row[csf('grey_fabric_recv')]+$prop_row[csf('grey_fabric_issue_return')]+$prop_row[csf('grey_fabric_trans_recv')];
                            $propDataArray[$prop_row[csf('po_breakdown_id')]]['iss']=$prop_row[csf('grey_fabric_issued')]+$prop_row[csf('grey_fabric_recv_return')]+$prop_row[csf('grey_fabric_trans_issued')];
                            $yet_issue=($prop_row[csf('grey_fabric_recv')]+$prop_row[csf('grey_fabric_issue_return')]+$prop_row[csf('grey_fabric_trans_recv')])-($prop_row[csf('grey_fabric_issued')]+$prop_row[csf('grey_fabric_recv_return')]+$prop_row[csf('grey_fabric_trans_issued')]);
                            $propDataArray[$prop_row[csf('po_breakdown_id')]]['bl']=$yet_issue;
                        }
                    }				 
                    //echo $po_sql; 
                    $po_data_array=array();					
                    $explSaveData = explode(",",$save_data);
                    foreach($explSaveData as $val)
                    {
                        $woQnty = explode("**",$val);
                        $po_data_array[$woQnty[0]]=$woQnty[1];	
                    }
                    
                    $nameArray=sql_select($po_sql);
                    foreach($nameArray as $row)
                    {  
                        if ($i%2==0)  
                            $bgcolor="#E9F3FF";
                        else
                            $bgcolor="#FFFFFF";
                            
                        $tot_po_qnty+=$row[csf('po_qnty_in_pcs')];
                        
                        //$woQnty = explode("**",$explSaveData[$i-1]); print_r($woQnty);
                        //if($woQnty[0]==$row[csf('id')]) $qnty = $woQnty[1]; else $qnty = "";						
                        $qnty = $po_data_array[$row[csf('id')]];
                     ?>
                        <tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $i; ?>">
                            <td width="110">
                                <p><?php echo $row[csf('po_number')]; ?></p>
                                <input type="hidden" name="txtPoId[]" id="txtPoId_<?php echo $i; ?>" value="<?php echo $row[csf('id')]; ?>">
                                <input type="hidden" name="txtOrginal[]" id="txtOrginal_<?php echo $i; ?>" value="1">
                            </td>
                            <td width="70" align="center"><?php echo change_date_format($row[csf('pub_shipment_date')]); ?></td>
                            <td width="80" align="right">
                                <?php echo $row[csf('po_qnty_in_pcs')]; ?>
                                <input type="hidden" name="txtPoQnty[]" id="txtPoQnty_<?php echo $i; ?>" value="<?php echo $row[csf('po_qnty_in_pcs')]; ?>">
                            </td>
                            <td width="80" align="right"><?php echo $req_qty_array[$row[csf('id')]]; ?></td>
                            <td width="80" align="right"><?php echo number_format($propDataArray[$row[csf('id')]]['recv'],2,'.',''); ?></td>
                            <td width="80" align="right"><?php echo number_format($propDataArray[$row[csf('id')]]['iss'],2,'.',''); ?></td>
                            <td align="center">
                                <input type="text" name="txtGreyQnty[]" id="txtGreyQnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:70px" value="<?php  echo $qnty; ?>" onKeyUp="sum_total();" placeholder="<?php echo number_format($propDataArray[$row[csf('id')]]['bl'],2,'.',''); ?>">
                            </td>					
                            
                        </tr>
                    <?php 
                    $i++; 
                    } 
                    ?>
                    <input type="hidden" name="tot_po_qnty" id="tot_po_qnty" class="text_boxes" value="<?php echo $tot_po_qnty; ?>">
                </table> 
                <table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="610"> 
                    <tr class="tbl_bottom">
                        <td width="110">&nbsp;</td><td width="70" align="right">&nbsp;</td><td width="80" align="right">&nbsp;</td><td width="80" align="right">&nbsp;</td><td width="80" align="right">&nbsp;</td><td width="80" align="right">Sum</td><td style="text-align:center"><input type="text" id="txt_total_sum" class="text_boxes_numeric" style="width:70px" readonly /></td>
                    </tr>
               </table>                 
            </div>
                <table width="620" id="table_id">
                     <tr>
                        <td align="center" >
                            <input type="button" name="close" class="formbutton" value="Close" id="main_close" onClick="fnc_close();" style="width:100px" />
                        </td>
                    </tr>
                </table>
            </div>
       </div>     
	<?php
	}
	else
	{  
	?>
		<div style="width:600px; margin-top:10px" align="center">
			<table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="300" align="center">
				<thead>
					<th>Total Issue Qnty</th>
					<th>Distribution Method</th>
				</thead>
				<tr class="general">
					<td><input type="text" name="txt_prop_grey_qnty" id="txt_prop_grey_qnty" class="text_boxes_numeric" value="<?php if($prev_method==1) echo $issueQnty; ?>" style="width:120px"  onBlur="distribute_qnty($('#cbo_distribiution_method').val())" <?php echo $readonlyCond; ?> /></td>
					<td>
						<?php
							$distribiution_method=array(1=>"Proportionately",2=>"Manually");
                            echo create_drop_down( "cbo_distribiution_method", 160, $distribiution_method,"",0,"--Select--",$prev_method,"distribute_qnty(this.value);",0 );
						?>
					</td>
				</tr>
			</table>
		</div>
		<div style="margin-left:1px; margin-top:10px">
			<table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="610">
				<thead>
                    <th width="110">PO No</th>
                    <th width="70">Ship. Date</th>
                    <th width="80">Gmts. Qty</th>
                    <th width="80">Req. Qty</th>
                    <th width="80">Prod. Qty</th>
                    <th width="80">Cumu. Issued Qty</th>
					<th>Issue Qty</th>
				</thead>
			</table>
			<div style="width:630px; max-height:220px; overflow-y:scroll" id="container" align="left"> 
				<table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="610" id="tbl_search">  
					<?php 
					$i=1; $tot_po_qnty=0; $propDataArray=array();
					
					//$propData=sql_select("select po_breakdown_id, sum(case when entry_form in(2,22) then quantity end) as grey_fabric_recv, sum(case when entry_form in(16) then quantity end) as grey_fabric_issued from order_wise_pro_details where trans_id<>0 and prod_id=$prod_id and is_deleted=0 and status_active=1 group by po_breakdown_id");
					 $propData=sql_select("select po_breakdown_id, sum(case when entry_form in(2,22) then quantity end) as grey_fabric_recv, sum(case when entry_form in(16) then quantity end) as grey_fabric_issued, sum(case when entry_form=45 then quantity end) as grey_fabric_recv_return, sum(case when entry_form=51 then quantity end) as grey_fabric_issue_return, sum(case when entry_form in(13) and trans_type=5 then quantity end) as grey_fabric_trans_recv, sum(case when entry_form in(13) and trans_type=6 then quantity end) as grey_fabric_trans_issued from order_wise_pro_details where trans_id<>0 and prod_id=$prod_id and is_deleted=0 and status_active=1 group by po_breakdown_id");
					foreach($propData as $prop_row)
					{
						/*$propDataArray[$prop_row[csf('po_breakdown_id')]]['recv']=$prop_row[csf('grey_fabric_recv')];
						$propDataArray[$prop_row[csf('po_breakdown_id')]]['iss']=$prop_row[csf('grey_fabric_issued')];
						$yet_issue=$prop_row[csf('grey_fabric_recv')]-$prop_row[csf('grey_fabric_issued')];*/
						
						$propDataArray[$prop_row[csf('po_breakdown_id')]]['recv']=$prop_row[csf('grey_fabric_recv')]+$prop_row[csf('grey_fabric_issue_return')]+$prop_row[csf('grey_fabric_trans_recv')];
						$propDataArray[$prop_row[csf('po_breakdown_id')]]['iss']=$prop_row[csf('grey_fabric_issued')]+$prop_row[csf('grey_fabric_recv_return')]+$prop_row[csf('grey_fabric_trans_issued')];
						$yet_issue=($prop_row[csf('grey_fabric_recv')]+$prop_row[csf('grey_fabric_issue_return')]+$prop_row[csf('grey_fabric_trans_recv')])-($prop_row[csf('grey_fabric_issued')]+$prop_row[csf('grey_fabric_recv_return')]+$prop_row[csf('grey_fabric_trans_issued')]);
						$propDataArray[$prop_row[csf('po_breakdown_id')]]['bl']=$yet_issue;
					}
					//print_r($propDataArray);		
 					
					$po_sql="select b.id, b.po_number, a.total_set_qnty, b.po_quantity, b.pub_shipment_date from wo_po_details_master a, wo_booking_dtls c, wo_po_break_down b where a.job_no=b.job_no_mst and b.id=c.po_break_down_id and c.booking_no='$booking_no' group by b.id, b.po_number, a.total_set_qnty, b.po_quantity, b.pub_shipment_date";
					
					//echo $po_sql;
					if($save_string=="" && $type==1) $save_data=$prevQnty;
					$po_data_array=array();					
					$explSaveData = explode(",",$save_data);
					foreach($explSaveData as $val)
					{
						$woQnty = explode("**",$val);
						$po_data_array[$woQnty[0]]=$woQnty[1];	
					}
					//print_r($explSaveData);
 					$nameArray=sql_select($po_sql);
					foreach($nameArray as $row)
					{  
						if ($i%2==0)  
							$bgcolor="#E9F3FF";
						else
							$bgcolor="#FFFFFF";
						
						$po_qnty_in_pcs=$row[csf('po_quantity')]*$row[csf('total_set_qnty')];	
						$tot_po_qnty+=$po_qnty_in_pcs;
						$qnty = $po_data_array[$row[csf('id')]];						
						
					 ?>
						<tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $i; ?>">
							<td width="110">
								<p><?php echo $row[csf('po_number')]; ?></p>
								<input type="hidden" name="txtPoId[]" id="txtPoId_<?php echo $i; ?>" value="<?php echo $row[csf('id')]; ?>">
								<input type="hidden" name="txtOrginal[]" id="txtOrginal_<?php echo $i; ?>" value="1">
							</td>
                            <td width="70" align="center"><?php echo change_date_format($row[csf('pub_shipment_date')]); ?></td>
							<td width="80" align="right">
								<?php echo $po_qnty_in_pcs; ?>
								<input type="hidden" name="txtPoQnty[]" id="txtPoQnty_<?php echo $i; ?>" value="<?php echo $po_qnty_in_pcs; ?>">
							</td>
                            <td width="80" align="right"><?php echo $req_qty_array[$row[csf('id')]]; ?></td>
                            <td width="80" align="right"><?php echo number_format($propDataArray[$row[csf('id')]]['recv'],2,'.',''); ?></td>
                            <td width="80" align="right"><?php echo number_format($propDataArray[$row[csf('id')]]['iss'],2,'.',''); ?></td>
							<td align="center">
								<input type="text" name="txtGreyQnty[]" id="txtGreyQnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:70px" value="<?php echo $qnty; ?>" onKeyUp="sum_total();" placeholder="<?php echo number_format($propDataArray[$row[csf('id')]]['bl'],2,'.',''); ?>" >
							</td>					
							
						</tr>
					<?php 
					$i++; 
					} 
					?>
					<input type="hidden" name="tot_po_qnty" id="tot_po_qnty" class="text_boxes" value="<?php echo $tot_po_qnty; ?>">
 				</table>
               <table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="610"> 
                        <tr class="tbl_bottom">
                            <td width="110">&nbsp;</td><td width="70" align="right">&nbsp;</td><td width="80" align="right">&nbsp;</td><td width="80" align="right">&nbsp;</td><td width="80" align="right">&nbsp;</td><td width="80" align="right">Sum</td><td style="text-align:center"><input type="text" id="txt_total_sum" class="text_boxes_numeric" style="width:70px" readonly /></td>
                        </tr>
               </table> 
			</div>
			<table width="610" id="table_id">
				 <tr>
					<td align="center" >
						<input type="button" name="close" class="formbutton" value="Close" id="main_close" onClick="fnc_close();" style="width:100px" />
					</td>
				</tr>
			</table>
		</div>
	<?php
	}	
	?>
    </fieldset>
</form>
        
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
exit();
}

if ($action=="po_search_popup")
{
	echo load_html_head_contents("PO Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);  

?> 
	<script> 
		
		function fn_show_check()
		{
			if( form_validation('cbo_buyer_name','Buyer Name')==false )
			{
				return;
			}			
			show_list_view ( $('#txt_search_common').val()+'_'+$('#cbo_search_by').val()+'_'+<?php echo $cbo_company_id; ?>+'_'+$('#cbo_buyer_name').val()+'_'+'<?php echo $hidden_order_id; ?>', 'create_po_search_view', 'search_div', 'grey_fabric_issue_controller', 'setFilterGrid(\'tbl_search\',-1);hidden_field_reset();');
			set_all();
		}
		
				
		var selected_id = new Array(); var selected_name = new Array(); var buyer_id=''; var style_ref_array= new Array();
		
		function check_all_data() 
		 {
			var tbl_row_count = document.getElementById( 'tbl_search' ).rows.length; 

			tbl_row_count = tbl_row_count-1;
			for( var i = 1; i <= tbl_row_count; i++ ) {
				js_set_value( i );
			}
		}
		
		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
		function set_all()
		{
			
			var old=document.getElementById('txt_po_row_id').value; 
			if(old!="")
			{   
				old=old.split(",");
				for(var k=0; k<old.length; k++)
				{   
					js_set_value( old[k] ) 
				} 
			}
		}
		
		function js_set_value( str ) 
		{

			toggle( document.getElementById( 'search' + str ), '#FFFFCC' );
			
			if( jQuery.inArray( $('#txt_individual_id' + str).val(), selected_id ) == -1 ) {
				selected_id.push( $('#txt_individual_id' + str).val() );
				selected_name.push( $('#txt_individual' + str).val() );
				
			}
			else {
				for( var i = 0; i < selected_id.length; i++ ) {
					if( selected_id[i] == $('#txt_individual_id' + str).val() ) break;
				}
				selected_id.splice( i, 1 );
				selected_name.splice( i, 1 );
			}
			
			var id = ''; var name = '';
			for( var i = 0; i < selected_id.length; i++ ) {
				id += selected_id[i] + ',';
				name += selected_name[i] + ',';
			}
			
			if(buyer_id=='') buyer_id=$('#txt_buyer' + str).val();
			
			var style_ref=$('#txt_styleRef' + str).val();
			
			if( jQuery.inArray( style_ref, style_ref_array) == -1 ) 
			{
				style_ref_array.push(style_ref);
			}
			
			id = id.substr( 0, id.length - 1 );
			name = name.substr( 0, name.length - 1 );
			
			$('#hidden_order_id').val(id);
			$('#hidden_order_no').val(name);
			$('#hide_buyer').val(buyer_id);
			$('#hide_style_ref').val(style_ref_array);		
			
		}
		
		function hidden_field_reset()
		{
			$('#hidden_order_id').val('');
			$('#hidden_order_no').val( '' );
			$('#hide_buyer').val( '' );
			$('#hide_style_ref').val( '' );
			selected_id = new Array();
			selected_name = new Array();
		}
		
		function fnc_close()
		{
			parent.emailwindow.hide();
		}
		
    </script>

</head>
<body>
	
	<form name="searchdescfrm"  id="searchdescfrm">
		<fieldset style="width:620px;margin-left:5px">
        <input type="hidden" name="hidden_order_id" id="hidden_order_id" class="text_boxes" value="">
        <input type="hidden" name="hidden_order_no" id="hidden_order_no" class="text_boxes" value="">
        <input type="hidden" name="hide_buyer" id="hide_buyer" class="text_boxes" value="">
        <input type="hidden" name="hide_style_ref" id="hide_style_ref" class="text_boxes" value="">
		<table cellpadding="0" cellspacing="0" width="620" class="rpt_table">
			<thead>
				<th>Buyer</th>
				<th>Search By</th>
				<th>Search</th>
				<th>
					<input type="reset" name="reset" id="reset" value="Reset" style="width:100px" class="formbutton" />
					<input type="hidden" name="po_id" id="po_id" value="">
				</th> 
			</thead>
			<tr class="general">
				<td align="center">
					<?php
						echo create_drop_down( "cbo_buyer_name", 170, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$cbo_company_id' $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,30,90)) order by buy.buyer_name","id,buyer_name", 1, "-- Select Buyer --", $buyer_name, "","" ); 
					?>       
				</td>
				<td align="center">	
					<?php
						$search_by_arr=array(1=>"PO No",2=>"Job No");

						echo create_drop_down( "cbo_search_by", 170, $search_by_arr,"",0, "--Select--", "",$dd,0 );
					?>
				</td>                 
				<td align="center">				
					<input type="text" style="width:130px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />	
				</td> 						
				<td align="center">
					<input type="button" name="button2" class="formbutton" value="Show" onClick="fn_show_check()" style="width:100px;" />
				</td>
			</tr>
		</table>
        <div id="search_div" style="margin-top:10px"></div>
		</fieldset>
	</form>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
exit();
}

if($action=="create_po_search_view")
{
	$data = explode("_",$data);
	
	$search_string=trim($data[0]);
	$search_by=$data[1];
	
	$search_con="";
	if($search_by==1 && $search_string!="")
		$search_con = " and b.po_number like '%$search_string%'";
	else if($search_by==2 && $search_string!="")
		$search_con =" and a.job_no like '%$search_string%'"; 
		
	$company_id =$data[2];
	$buyer_id =$data[3];	
	$all_po_id=$data[4];
	
	$hidden_po_id=explode(",",$all_po_id);
	 
	if($buyer_id==0) { echo "<b>Please Select Buyer First</b>"; die; }
	

	$sql = "select a.job_no, a.style_ref_no, a.buyer_name, a.order_uom, b.id, b.po_number, (a.total_set_qnty*b.po_quantity) as po_qnty_in_pcs, b.pub_shipment_date 
	from wo_po_details_master a, wo_po_break_down b
	where a.job_no=b.job_no_mst and a.company_name=$company_id and a.buyer_name=$buyer_id $search_con and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0";

	//echo $sql;die;
	?>
    <div>
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="600" class="rpt_table" >
            <thead>
                <th width="40">SL</th>
                <th width="100">Job No</th>
                <th width="110">Style No</th>
                <th width="110">PO No</th>
                <th width="90">PO Quantity</th>
                <th width="50">UOM</th>
                <th>Shipment Date</th>
            </thead>
        </table>
        <div style="width:618px; overflow-y:scroll; max-height:220px;" id="buyer_view" align="center">
            <table cellspacing="0" cellpadding="0" border="1" rules="all" width="600" class="rpt_table" id="tbl_search" >
            <?php
				$i=1; $po_row_id='';
				$nameArray=sql_select( $sql );
				foreach ($nameArray as $selectResult)
				{
					if ($i%2==0)  
						$bgcolor="#E9F3FF";
					else
						$bgcolor="#FFFFFF";
					 
					if(in_array($selectResult[csf('id')],$hidden_po_id)) 
					{ 
						if($po_row_id=="") $po_row_id=$i; else $po_row_id.=",".$i;
					}
					?>
                    <tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" id="search<?php echo $i;?>" onClick="js_set_value(<?php echo $i;?>)"> 
                        <td width="40" align="center"><?php echo "$i"; ?>
                            <input type="hidden" name="txt_individual_id" id="txt_individual_id<?php echo $i ?>" value="<?php echo $selectResult[csf('id')]; ?>"/>	
                            <input type="hidden" name="txt_individual" id="txt_individual<?php echo $i ?>" value="<?php echo $selectResult[csf('po_number')]; ?>"/>
                            <input type="hidden" name="txt_buyer" id="txt_buyer<?php echo $i ?>" value="<?php echo $selectResult[csf('buyer_name')]; ?>"/>
                            <input type="hidden" name="txt_styleRef" id="txt_styleRef<?php echo $i ?>" value="<?php echo $selectResult[csf('style_ref_no')]; ?>"/>	
                        </td>	
                        <td width="100"><p><?php echo $selectResult[csf('job_no')]; ?></p></td>
                        <td width="110"><p><?php echo $selectResult[csf('style_ref_no')]; ?></p></td>
                        <td width="110"><p><?php echo $selectResult[csf('po_number')]; ?></p></td>
                        <td width="90" align="right"><?php echo $selectResult[csf('po_qnty_in_pcs')]; ?></td> 
                        <td width="50" align="center"><p><?php echo $unit_of_measurement[$selectResult[csf('order_uom')]]; ?></p></td>
                        <td align="center"><?php echo change_date_format($selectResult[csf('pub_shipment_date')]); ?></td>	
                    </tr>
                    <?php
                    $i++;
				}
			?>
				<input type="hidden" name="txt_po_row_id" id="txt_po_row_id" value="<?php echo $po_row_id; ?>"/>
            </table>
        </div>
         <table width="620" cellspacing="0" cellpadding="0" style="border:none" align="center">
            <tr>
                <td align="center" height="30" valign="bottom">
                    <div style="width:100%"> 
                        <div style="width:50%; float:left" align="left">
                            <input type="checkbox" name="check_all" id="check_all" onClick="check_all_data()" /> Check / Uncheck All
                        </div>
                        <div style="width:50%; float:left" align="left">
                            <input type="button" name="close" onClick="fnc_close();" class="formbutton" value="Close" style="width:100px" />
                        </div>
                    </div>
                </td>
            </tr>
        </table>
	</div>           
<?php
	
exit();
}

//data save update delete here------------------------------//
if($action=="save_update_delete")
{	 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	if( $operation==0 ) // Insert Here----------------------------------------------------------
	{
				
		$con = connect();
		if($db_type==0)	{ mysql_query("BEGIN"); }
				
		//batch duplication check---------------------------------------------//
		if( str_replace("'","",$hidden_is_batch_maintain)==2 && str_replace("'","",$cbo_issue_purpose)!=3)
		{
			$chk = is_duplicate_field("batch_no","pro_batch_create_mst","batch_no=$txt_batch_no and batch_no!=0");
			if($chk==1)
			{
				echo "20**Duplicate Batch Number.";
				exit();
			}
		}
		
		$currentStock = return_field_value("current_stock","product_details_master","id=".$hiddenProdId);
		$txtIssueQnty = str_replace("'","",$txtIssueQnty);
		
		if($txtIssueQnty>$currentStock)
		{
			echo "20**Issue Quantity Exceeds The Current Stock Quantity";
			exit();
		}
		
		//table lock here 	 
		//if( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0"; die;}
		
		
		//####################################################-------------------- 	
		//batch creation table insert here------------------------//
		if(str_replace("'","",$txt_batch_id)!="") $batchID = $txt_batch_id; else $batchID = 0;
		
		if( str_replace("'","",$hidden_is_batch_maintain)==2 && str_replace("'","",$cbo_issue_purpose)!=3)
		{	
			 //pro_batch_create_mst table insert here------------------------//
			 //batch_against,batch_for,batch_weight
			 if(str_replace("'","",$cbo_issue_purpose)==8) $booking_without_order=1; else $booking_without_order=0;
			 $batchID = return_next_id("id", "pro_batch_create_mst", 1);
			 $field_array_batch_mst= "id,batch_no,entry_form,batch_date,company_id,booking_no_id,booking_no,booking_without_order,batch_weight,inserted_by,insert_date";
			 $data_array_batch_mst= "(".$batchID.",".$txt_batch_no.",16,".$txt_issue_date.",".$cbo_company_name.",".$txt_booking_id.",".$txt_booking_no.",".$booking_without_order.",".$txtIssueQnty.",'".$user_id."','".$pc_date_time."')";
			 
			// $batchQry=sql_insert("pro_batch_create_mst",$field_array_batch_mst,$data_array_batch_mst,1);	
			 //pro_batch_create_mst table insert end------------------------//
			
			
			 //pro_batch_create_dtls table insert start------------------------//		  
 			 $batchDtlsID = return_next_id("id", "pro_batch_create_dtls", 1);
			 $field_array_batch_dtls= "id,mst_id,po_id,item_description,roll_no,batch_qnty,inserted_by,insert_date";
 			 $data_array_batch_dtls="(".$batchDtlsID.",".$batchID.",".$hiddenProdId.",".$txtItemDescription.",".$txtRollNo.",".$txtIssueQnty.",'".$user_id."','".$pc_date_time."')";				
			
			 //$batchQryDtls=sql_insert("pro_batch_create_dtls",$field_array_batch_dtls,$data_array_batch_dtls,1);
			 	
			 //pro_batch_create_dtls table insert end------------------------//
		  
		}
		else
		{
		 	$batchQry=true;$batchQryDtls=true;
		}
		//batch creation table insert end------------------------//
			
		//echo "10**".$field_array."=".$data_array."##".$batchQry; 
		//mysql_query("ROLLBACK");die; 	
			
		$mrr_no='';
		//issue master table entry here Start---------------------------------------//
		if( str_replace("'","",$txt_system_no) == "" ) //new insert
		{	
			$id=return_next_id("id", "inv_issue_master", 1);
			
			if($db_type==0) $year_cond="YEAR(insert_date)"; 
			else if($db_type==2) $year_cond="to_char(insert_date,'YYYY')";
			else $year_cond="";//defined Later
					
			$new_mrr_number=explode("*",return_mrr_number( str_replace("'","",$cbo_company_name), '', 'KGI', date("Y",time()), 5, "select issue_number_prefix,issue_number_prefix_num from inv_issue_master where company_id=$cbo_company_name and entry_form=16 and $year_cond=".date('Y',time())." order by id DESC ", "issue_number_prefix", "issue_number_prefix_num" ));
			
			$field_array="id,issue_number_prefix, issue_number_prefix_num, issue_number, issue_basis, issue_purpose, entry_form, item_category, company_id, buyer_id, style_ref, booking_id, booking_no, batch_no, issue_date, knit_dye_source, knit_dye_company, challan_no, order_id, inserted_by, insert_date";
			$data_array="(".$id.",'".$new_mrr_number[1]."','".$new_mrr_number[2]."','".$new_mrr_number[0]."',".$cbo_basis.",".$cbo_issue_purpose.",16,13,".$cbo_company_name.",".$cbo_buyer_name.",".$txt_style_ref.",".$txt_booking_id.",".$txt_booking_no.",".$batchID.",".$txt_issue_date.",".$cbo_dyeing_source.",".$cbo_dyeing_company.",".$txt_challan_no.",".$hidden_order_id.",'".$user_id."','".$pc_date_time."')";		
			//$rID=sql_insert("inv_issue_master",$field_array,$data_array,1); 
			$mrr_no=$new_mrr_number[0];
		}
		else
		{
			$id = str_replace("'","",$hidden_system_id);
			$field_array="issue_basis*issue_purpose*entry_form*item_category*company_id*buyer_id*style_ref*booking_id*booking_no*batch_no*issue_date*knit_dye_source*knit_dye_company*challan_no*order_id*updated_by*update_date";
			$data_array="".$cbo_basis."*".$cbo_issue_purpose."*16*13*".$cbo_company_name."*".$cbo_buyer_name."*".$txt_style_ref."*".$txt_booking_id."*".$txt_booking_no."*".$txt_batch_no."*".$txt_issue_date."*".$cbo_dyeing_source."*".$cbo_dyeing_company."*".$txt_challan_no."*".$hidden_order_id."*'".$user_id."'*'".$pc_date_time."'";
			//echo $field_array."<br>".$data_array;."-".;
			//$rID=sql_update("inv_issue_master",$field_array,$data_array,"id",$id,0);
			$mrr_no=str_replace("'","",$txt_system_no);
		}
		//issue master table entry here END---------------------------------------//
		//if($rID) $flag=1; else $flag=0;
		
		$transactionID = return_next_id("id", "inv_transaction", 1);
				//####################################################-----------------
		//this is for transaction table insert-------------
		
		$tr_field_array = "id,mst_id,requisition_no,company_id,prod_id,item_category,transaction_type,transaction_date,store_id,cons_quantity,rack,self,inserted_by,insert_date";		
		$tr_data_array = "(".$transactionID.",".$id.",".$txt_program_no.",".$cbo_company_name.",".$hiddenProdId.",13,2,".$txt_issue_date.",".$cbo_store_name.",".$txtIssueQnty.",".$txt_rack.",".$txt_self.",'".$user_id."','".$pc_date_time."')";
		//$transID = sql_insert("inv_transaction",$tr_field_array,$tr_data_array,1);
		//inventory TRANSACTION table data entry  END-------------------------------//
		

		//####################################################------------------------
		//inv_grey_fabric_issue_dtls table insert start------------------------------//
		$dtls_id=return_next_id("id", "inv_grey_fabric_issue_dtls", 1);
		
		$cbo_yarn_count=explode(",",str_replace("'","",$cbo_yarn_count));
		asort($cbo_yarn_count);
		$cbo_yarn_count=implode(",",$cbo_yarn_count);
		
		$txtYarnLot=explode(",",str_replace("'","",$txtYarnLot));
		asort($txtYarnLot);
		$txtYarnLot=implode(",",$txtYarnLot);
		
		$field_array_dtls="id,mst_id,trans_id,distribution_method,program_no ,no_of_roll,roll_no,roll_po_id,prod_id,roll_wise_issue_qnty, issue_qnty, color_id, yarn_lot, yarn_count, store_name, rack,self,stitch_length,remarks,inserted_by,insert_date";		
 		$data_array_dtls="(".$dtls_id.",".$id.",".$transactionID.",".$distribution_method_id.",".$txt_program_no.",".$txtNoOfRoll.",".$txtRollNo.",".$txtRollPOid.",".$hiddenProdId.",".$txtRollPOQnty.",".$txtIssueQnty.",".$cbo_color_id.",'".$txtYarnLot."','".$cbo_yarn_count."',".$cbo_store_name.",".$txt_rack.",".$txt_self.",".$txt_stitch_length.",".$txt_remarks.",'".$user_id."','".$pc_date_time."')";
		//$dtlsrID=sql_insert("inv_grey_fabric_issue_dtls",$field_array_dtls,$data_array_dtls,1); 		
		//inv_grey_fabric_issue_dtls table insert end------------------------------//
		
		
		
		//####################################################----------------- 
		//product master table data UPDATE START-----------------------------//  

		$currentStock = $currentStock-$txtIssueQnty;
 		$prod_update_data = "".$txtIssueQnty."*".$currentStock."*'".$user_id."'*'".$pc_date_time."'";
		$prod_field_array = "last_issued_qnty*current_stock*updated_by*update_date";
 		//$prodUpdate = sql_update("product_details_master",$prod_field_array,$prod_update_data,"id",$hiddenProdId,0);
		//product master table data UPDATE END-------------------------------//	
			
		 
		
		
		//####################################################--------------------
		//order_wise_pro_details table data insert Start---------------// 
		$proportQ=true; 
		$data_array_prop="";
		$save_string=explode(",",str_replace("'","",$save_data));
		if(count($save_string)>0 && str_replace("'","",$save_data)!="")  
		{		 
			//order_wise_pro_details table data insert START-----//  
			$field_array_proportionate="id,trans_id,trans_type,entry_form,dtls_id,po_breakdown_id,prod_id,quantity,issue_purpose,inserted_by,insert_date";
			$po_array=array();
			for($i=0;$i<count($save_string);$i++)
			{
				$order_dtls=explode("**",$save_string[$i]);			
				$order_id=$order_dtls[0];
				$order_qnty=$order_dtls[1];
				
				if( array_key_exists($order_id,$po_array) )
					$po_array[$order_id]+=$order_qnty;
				else
					$po_array[$order_id]=$order_qnty;
			}
			$i=0; 
			foreach($po_array as $key=>$val)
			{
				if( $i>0 ) $data_array_prop.=",";
				if( $id_proport=="" ) $id_proport = return_next_id( "id", "order_wise_pro_details", 1 ); else $id_proport = $id_proport+1;
				$order_id=$key;
				$order_qnty=$val;
				$data_array_prop.="(".$id_proport.",".$transactionID.",2,16,".$dtls_id.",".$order_id.",".$hiddenProdId.",".$order_qnty.",".$cbo_issue_purpose.",".$user_id.",'".$pc_date_time."')";
				$i++;
			} 
			/*if($data_array_prop!="")
			{
				$proportQ=sql_insert("order_wise_pro_details",$field_array_proportionate,$data_array_prop,1);			
			}*/
		}//end if
		//order_wise_pro_details table data insert END -----//
		
		  
		//####################################################--------------------
		//roll table entry----------------------------------------//
		if( str_replace("'","",$hidden_is_roll_maintain)==1)
		{
			$data_array_roll="";
			$rollDtlsID = return_next_id("id", "pro_roll_details", 1);
			$field_array_roll= "id,mst_id,dtls_id,po_breakdown_id,entry_form,roll_no,roll_id,qnty,inserted_by,insert_date";
			
			$poIDarr = explode(",",str_replace("'","",$txtRollPOid));
			$rollNoarr = explode(",",str_replace("'","",$txtRollNo));
			$rollPoQntyarr = explode(",",str_replace("'","",$txtRollPOQnty));			
			$lopSize = count($poIDarr); 
			for($i=0;$i<$lopSize;$i++)
			{  			 					
				$rollno = return_field_value("roll_no","pro_roll_details","id=".$rollNoarr[$i]);
				if($i>0) $data_array_roll .=",";   
				$data_array_roll.= "(".$rollDtlsID.",".$id.",".$dtls_id.",".$poIDarr[$i].",16,'".$rollno."',".$rollNoarr[$i].",".$rollPoQntyarr[$i].",'".$user_id."','".$pc_date_time."')";
			 	$rollDtlsID = $rollDtlsID+1;
			}	
			//$rollDtls=sql_insert("pro_roll_details",$field_array_roll,$data_array_roll,1);	
		}
		else
		{
			 $rollDtls=true; 
		}		 
		//roll table entry end------------------------------------//
		
		 
		//echo "20**".$rID." && ".$dtlsrID." && ".$prodUpdate." && ".$proportQ." && ".$batchQry." && ".$batchQryDtls." && ".$rollDtls; 
		
 		 //Query Execution Start 
		 
		 if( str_replace("'","",$txt_system_no) == "" )
		 {
			 $rID=sql_insert("inv_issue_master",$field_array,$data_array,1); 
		 }
		 else
		 {
			 $rID=sql_update("inv_issue_master",$field_array,$data_array,"id",$id,0);
		 }
		 
		 $transID = sql_insert("inv_transaction",$tr_field_array,$tr_data_array,1);
		 $dtlsrID=sql_insert("inv_grey_fabric_issue_dtls",$field_array_dtls,$data_array_dtls,1); 		 
		 $prodUpdate = sql_update("product_details_master",$prod_field_array,$prod_update_data,"id",$hiddenProdId,0);
		 
		 if( str_replace("'","",$hidden_is_batch_maintain)==2 && str_replace("'","",$cbo_issue_purpose)!=3)
		 {
			 $batchQry=sql_insert("pro_batch_create_mst",$field_array_batch_mst,$data_array_batch_mst,1);
			 $batchQryDtls=sql_insert("pro_batch_create_dtls",$field_array_batch_dtls,$data_array_batch_dtls,1);	 
		 }
		 
		 if(count($save_string)>0 && str_replace("'","",$save_data)!="") 
		 {
			if($data_array_prop!="")
			{
				$proportQ=sql_insert("order_wise_pro_details",$field_array_proportionate,$data_array_prop,1);			
			}
		 }

		 if( str_replace("'","",$hidden_is_roll_maintain)==1)
		 {
			 $rollDtls=sql_insert("pro_roll_details",$field_array_roll,$data_array_roll,1);	
		 }
		//release lock table
		//check_table_status( $_SESSION['menu_id'],0);
		
		if($db_type==0)
		{
			if($rID && $dtlsrID && $transID && $prodUpdate && $proportQ && $batchQry && $batchQryDtls && $rollDtls)
			{
				mysql_query("COMMIT");  
				echo "0**".$mrr_no."**".$id;
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".$mrr_no."**".$id;
			}
		}
		else if($db_type==2 || $db_type==1 )
		{
			if($rID && $dtlsrID && $transID && $prodUpdate && $proportQ && $batchQry && $batchQryDtls && $rollDtls)
			{
				oci_commit($con); 
				echo "0**".$mrr_no."**".$id;
			}
			else
			{
				oci_rollback($con); 
				echo "10**".$mrr_no."**".$id;
			}
		}
		disconnect($con);
		die;
	}	
	else if ($operation==1) // Update Here----------------------------------------------------------
	{
		$con = connect();		
		if($db_type==0)	{ mysql_query("BEGIN"); }
		//table lock here  
		
		 
		/*//check update id
		if( str_replace("'","",$txt_system_no)=="" )
		{
			echo "15";exit(); 
		}*/
		 
		
		//batch duplication check---------------------------------------------//
		if( str_replace("'","",$hidden_is_batch_maintain)==2 && str_replace("'","",$cbo_issue_purpose)!=3)
		{
			$chk = is_duplicate_field("batch_no","pro_batch_create_mst","batch_no=$txt_batch_no and id!=$txt_batch_id and batch_no!=0");
			if($chk==1)
			{
				echo "20**Duplicate Batch Number.";
				exit();
			}
		}
		
		//if( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0"; die;}
		
		
 		//####################################################-------------------- 	
		//batch creation table insert here------------------------//
		if( str_replace("'","",$hidden_is_batch_maintain)==2 && str_replace("'","",$cbo_issue_purpose)!=3 && str_replace("'","",$txt_batch_id)!="" )
		{	
			 if(str_replace("'","",$cbo_issue_purpose)==8) $booking_without_order=1; else $booking_without_order=0;
			 //pro_batch_create_mst table update here------------------------//
 			 $field_array_batch_mst= "batch_no*entry_form*batch_date*company_id*booking_no_id*booking_no*booking_without_order*batch_weight*updated_by*update_date";
			 $data_array_batch_mst= "".$txt_batch_no."*16*".$txt_issue_date."*".$cbo_company_name."*".$txt_booking_id."*".$txt_booking_no."*".$booking_without_order."*".$txtIssueQnty."*'".$user_id."'*'".$pc_date_time."'";			 
 			 //$batchQry=sql_update("pro_batch_create_mst",$field_array_batch_mst,$data_array_batch_mst,"id",$txt_batch_id,0);
			 //pro_batch_create_mst table update end------------------------//
			
			
			 //pro_batch_create_dtls table insert start------------------------//		  
 			 $field_array_batch_dtls= "po_id*item_description*roll_no*batch_qnty*updated_by*update_date";
 			 $data_array_batch_dtls= "".$hiddenProdId."*".$txtItemDescription."*".$txtRollNo."*".$txtIssueQnty."*'".$user_id."'*'".$pc_date_time."'";				
 			 //$batchQryDtls=sql_update("pro_batch_create_dtls",$field_array_batch_dtls,$data_array_batch_dtls,"mst_id",$txt_batch_id,0);
			 //pro_batch_create_dtls table insert end------------------------//
		   
		}
		else
		{
		 	$batchQry=true;$batchQryDtls=true;
		}
		//batch creation table insert end------------------------//
		
		 
		 
  		//################################################################
		//issue master update START--------------------------------------//
		$id = str_replace("'","",$hidden_system_id);
		$field_array="issue_basis*issue_purpose*entry_form*item_category*company_id*buyer_id*style_ref*booking_id*booking_no*batch_no*issue_date*knit_dye_source*knit_dye_company*challan_no*order_id*updated_by*update_date";
		$data_array="".$cbo_basis."*".$cbo_issue_purpose."*16*13*".$cbo_company_name."*".$cbo_buyer_name."*".$txt_style_ref."*".$txt_booking_id."*".$txt_booking_no."*".$txt_batch_id."*".$txt_issue_date."*".$cbo_dyeing_source."*".$cbo_dyeing_company."*".$txt_challan_no."*".$hidden_order_id."*'".$user_id."'*'".$pc_date_time."'";
		//echo "20**".$field_array."<br>".$data_array;
		//$rID=sql_update("inv_issue_master",$field_array,$data_array,"id",$id,0);
		//issue master update END---------------------------------------// 
		 
		 
		 
		//####################################################----------------- 
		//product table update data before -----------------------------// 
		$before_prod_id = return_field_value("prod_id","inv_grey_fabric_issue_dtls","id=".$dtls_tbl_id);
		$sqlRes = sql_select("select b.id,b.current_stock,a.issue_qnty from inv_grey_fabric_issue_dtls a, product_details_master b where a.prod_id=b.id and a.id=$dtls_tbl_id"); 
 		foreach($sqlRes as $resR);		 
		$before_prod_id = $resR[csf("id")];
		$before_issue_qnty = $resR[csf("issue_qnty")];
		$before_current_stock = $resR[csf("current_stock")];
		$adjust_current_stock = $before_current_stock+$before_issue_qnty;
		
		
		//####################################################------------------------
		//inv_grey_fabric_issue_dtls table update start------------------------------//
		$dtls_id=$dtls_tbl_id;
		
		$cbo_yarn_count=explode(",",str_replace("'","",$cbo_yarn_count));
		asort($cbo_yarn_count);
		$cbo_yarn_count=implode(",",$cbo_yarn_count);
		
		$txtYarnLot=explode(",",str_replace("'","",$txtYarnLot));
		asort($txtYarnLot);
		$txtYarnLot=implode(",",$txtYarnLot);
		
		$field_array_dtls="distribution_method*program_no*no_of_roll*roll_no*roll_po_id*prod_id*roll_wise_issue_qnty*issue_qnty*color_id*yarn_lot*yarn_count*store_name*rack*self*stitch_length*remarks*updated_by*update_date";		
 		$data_array_dtls="".$distribution_method_id."*".$txt_program_no."*".$txtNoOfRoll."*".$txtRollNo."*".$txtRollPOid."*".$hiddenProdId."*".$txtRollPOQnty."*".$txtIssueQnty."*".$cbo_color_id."*'".$txtYarnLot."'*'".$cbo_yarn_count."'*".$cbo_store_name."*".$txt_rack."*".$txt_self."*".$txt_stitch_length."*".$txt_remarks."*'".$user_id."'*'".$pc_date_time."'";
 		//$dtlsrID=sql_update("inv_grey_fabric_issue_dtls",$field_array_dtls,$data_array_dtls,"id",$dtls_id,0);		
		//inv_grey_fabric_issue_dtls table update end------------------------------// 
		 
		 
		
		//####################################################-----------------
		//this is for transaction table update start -------------
		$transactionID = $trans_tbl_id;
		$tr_field_array = "company_id*requisition_no*prod_id*item_category*transaction_type*transaction_date*store_id*cons_quantity*rack*self*updated_by*update_date";		
		$tr_data_array = "".$cbo_company_name."*".$txt_program_no."*".$hiddenProdId."*13*2*".$txt_issue_date."*".$cbo_store_name."*".$txtIssueQnty."*".$txt_rack."*".$txt_self."*'".$user_id."'*'".$pc_date_time."'";
 		//$transID = sql_update("inv_transaction",$tr_field_array,$tr_data_array,"id",$transactionID,0);	
		//inventory TRANSACTION table data update  END-------------------------------//
		
		
		//####################################################----------------- 
		//product master table data UPDATE START-----------------------------//  
		$hiddenProdId = str_replace("'","",$hiddenProdId); //current product id
		$txtIssueQnty = str_replace("'","",$txtIssueQnty); //current issue qnty
		
		//current adjust
		$currentStock = return_field_value("current_stock","product_details_master","id=".$hiddenProdId);
			
		$updateID_array=array();
		$update_data=array();
		if($before_prod_id==$hiddenProdId) 
		{
			if($txtIssueQnty>$adjust_current_stock)
			{
				check_table_status( $_SESSION['menu_id'],0);
				echo "20**Issue Quantity Exceeds The Current Stock Quantity";
				exit();
			}
			
			$adjust_current_stock = $adjust_current_stock - $txtIssueQnty;
			$prod_update_data = "".$txtIssueQnty."*".$adjust_current_stock."*'".$user_id."'*'".$pc_date_time."'";
			$prod_field_array = "last_issued_qnty*current_stock*updated_by*update_date";
 			//$prodUpdate = sql_update("product_details_master",$prod_field_array,$prod_update_data,"id",$hiddenProdId,0);
		}
		else
		{
			//before adjust
			$updateID_array[]=$before_prod_id; 
			$update_data[$before_prod_id]=explode("*",("0*".$adjust_current_stock."*'".$user_id."'*'".$pc_date_time."'"));
			
			if($txtIssueQnty>$currentStock)
			{
				check_table_status( $_SESSION['menu_id'],0);
				echo "20**Issue Quantity Exceeds The Current Stock Quantity";
				exit();
			}
			
			$currentStock = $currentStock-$txtIssueQnty;
			$updateID_array[]=$hiddenProdId; 
			$update_data[$hiddenProdId]=explode("*",("".$txtIssueQnty."*".$currentStock."*'".$user_id."'*'".$pc_date_time."'"));
			$prod_field_array = "last_issued_qnty*current_stock*updated_by*update_date";
			//echo bulk_update_sql_statement("product_details_master","id",$prod_field_array,$update_data,$updateID_array);die;
			//$prodUpdate=execute_query(bulk_update_sql_statement("product_details_master","id",$prod_field_array,$update_data,$updateID_array));
		}
		//product master table data UPDATE END-------------------------------// 
 		
 		//####################################################--------------------
		//order_wise_pro_details table data insert Start---------------// 
		$proportQ=true; 
		$data_array_prop="";
		$save_string=explode(",",str_replace("'","",$save_data));
		if(count($save_string)>0 && str_replace("'","",$save_data)!="")  
		{		 
			//order_wise_pro_details table data insert START-----//  
			$field_array_proportionate="id,trans_id,trans_type,entry_form,dtls_id,po_breakdown_id,prod_id,quantity,issue_purpose,inserted_by,insert_date";
			$po_array=array();
			for($i=0;$i<count($save_string);$i++)
			{
				$order_dtls=explode("**",$save_string[$i]);			
				$order_id=$order_dtls[0];
				$order_qnty=$order_dtls[1];
				
				if( array_key_exists($order_id,$po_array) )
					$po_array[$order_id]+=$order_qnty;
				else
					$po_array[$order_id]=$order_qnty;
			}
			$i=0; 
			foreach($po_array as $key=>$val)
			{
				if( $i>0 ) $data_array_prop.=",";
				if( $id_proport=="" ) $id_proport = return_next_id( "id", "order_wise_pro_details", 1 ); else $id_proport = $id_proport+1;
				$order_id=$key;
				$order_qnty=$val;
				$data_array_prop.="(".$id_proport.",".$transactionID.",2,16,".$dtls_id.",".$order_id.",".$hiddenProdId.",".$order_qnty.",".$cbo_issue_purpose.",".$user_id.",'".$pc_date_time."')";
				$i++;
			} 
			/*if($data_array_prop!="")
			{
				$proportQ=sql_insert("order_wise_pro_details",$field_array_proportionate,$data_array_prop,1);			
			}*/
		}//end if
		//order_wise_pro_details table data insert END -----//
		
		 
		  
		 
		 
		//####################################################--------------------
		//roll table entry----------------------------------------//
		if( str_replace("'","",$hidden_is_roll_maintain)==1)
		{
			$data_array_roll="";
			$rollDtlsID = return_next_id("id", "pro_roll_details", 1);
			$field_array_roll = "id,mst_id,dtls_id,po_breakdown_id,entry_form,roll_no,roll_id,qnty,inserted_by,insert_date";
			
			$poIDarr = explode(",",str_replace("'","",$txtRollPOid));
			$rollNoarr = explode(",",str_replace("'","",$txtRollNo));
			$rollPoQntyarr = explode(",",str_replace("'","",$txtRollPOQnty));			
			$lopSize = count($poIDarr); 
			for($i=0;$i<$lopSize;$i++)
			{  			 					
				$rollno = return_field_value("roll_no","pro_roll_details","id=".$rollNoarr[$i]);
				if($i>0) $data_array_roll .=",";   
				$data_array_roll.= "(".$rollDtlsID.",".$id.",".$dtls_id.",".$poIDarr[$i].",16,'".$rollno."',".$rollNoarr[$i].",".$rollPoQntyarr[$i].",'".$user_id."','".$pc_date_time."')";
			 	$rollDtlsID = $rollDtlsID+1;
			}
			//$rollDtls=sql_insert("pro_roll_details",$field_array_roll,$data_array_roll,1);	
		}
		else
		{
			 $rollDtls=true;
		}
		//roll table entry end------------------------------------//
		
		if( str_replace("'","",$hidden_is_batch_maintain)==2 && str_replace("'","",$cbo_issue_purpose)!=3 && str_replace("'","",$txt_batch_id)!="" )
		{
			$batchQry=sql_update("pro_batch_create_mst",$field_array_batch_mst,$data_array_batch_mst,"id",$txt_batch_id,0);
			$batchQryDtls=sql_update("pro_batch_create_dtls",$field_array_batch_dtls,$data_array_batch_dtls,"mst_id",$txt_batch_id,0);
		}
		
		$rID=sql_update("inv_issue_master",$field_array,$data_array,"id",$id,0);
		$dtlsrID=sql_update("inv_grey_fabric_issue_dtls",$field_array_dtls,$data_array_dtls,"id",$dtls_id,0);			
		$transID = sql_update("inv_transaction",$tr_field_array,$tr_data_array,"id",$transactionID,0);
		
		if($before_prod_id==$hiddenProdId) 
		{
			$prodUpdate = sql_update("product_details_master",$prod_field_array,$prod_update_data,"id",$hiddenProdId,0);
		}
		else
		{
			$prodUpdate=execute_query(bulk_update_sql_statement("product_details_master","id",$prod_field_array,$update_data,$updateID_array));
		}
		
		//****************************************** BEFORE ENTRY ADJUST START *****************************************//
 		$deletePropor = execute_query("DELETE FROM order_wise_pro_details WHERE dtls_id = $dtls_tbl_id and entry_form=16");
		$deleteRoll = execute_query("DELETE FROM pro_roll_details WHERE dtls_id = $dtls_tbl_id and entry_form=16");
		//****************************************** BEFORE ENTRY ADJUST START *****************************************//
		
		if(count($save_string)>0 && str_replace("'","",$save_data)!="")  
		{
			if($data_array_prop!="")
			{
				$proportQ=sql_insert("order_wise_pro_details",$field_array_proportionate,$data_array_prop,1);			
			}
		}
		
		if( str_replace("'","",$hidden_is_roll_maintain)==1)
		{
			$rollDtls=sql_insert("pro_roll_details",$field_array_roll,$data_array_roll,1);
		}
		
		//oci_rollback($con);
		//echo "10**".$rID."==".$dtlsrID."==".$transID."==".$prodUpdate."==".$proportQ."==".$batchQry."==".$batchQryDtls."==".$rollDtls; die; 
		//mysql_query("ROLLBACK");
		
		//release lock table
		check_table_status( $_SESSION['menu_id'],0);
		
		if($db_type==0)
		{
			if($rID && $dtlsrID && $transID && $prodUpdate && $proportQ && $batchQry && $batchQryDtls && $rollDtls)
			{
				mysql_query("COMMIT");  
				echo "1**".str_replace("'","",$txt_system_no)."**".str_replace("'","",$hidden_system_id);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'","",$txt_system_no)."**".str_replace("'","",$hidden_system_id);
			}
		}
		else if($db_type==2 || $db_type==1 )
		{
			if($rID && $dtlsrID && $transID && $prodUpdate && $proportQ && $batchQry && $batchQryDtls && $rollDtls)
			{
				oci_commit($con); 
				echo "1**".str_replace("'","",$txt_system_no)."**".str_replace("'","",$hidden_system_id);
			}
			else
			{
				oci_rollback($con);
				echo "10**".str_replace("'","",$txt_system_no)."**".str_replace("'","",$hidden_system_id);
			}
		}
		disconnect($con);
		die;
 	}
	else if ($operation==2) // Delete Here----------------------------------------------------------
	{
		 //-------
	}		
}

if($action=="mrr_popup")
{
  	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	extract($_REQUEST);  
?>
     
<script>
	function js_set_value(sys_number)
	{
 		$("#hidden_sys_number").val(sys_number); // mrr number
		parent.emailwindow.hide();
	}
</script>

</head>

<body>
<div align="center" style="width:100%;" >
<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
	<table width="880" cellspacing="0" cellpadding="0" border="1" class="rpt_table" align="center" rules="all">
            <thead>
                <tr>                	 
                    <th width="150">Supplier</th>
                    <th width="150">Search By</th>
                    <th width="250" align="center" id="search_by_td_up">Enter WO/PI Number</th>
                    <th width="200">Issue Date Range</th>
                    <th><input type="reset" name="re_button" id="re_button" value="Reset" style="width:100px" class="formbutton"  /></th>           
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <?php  
 							 echo create_drop_down( "cbo_supplier", 150, "select a.id,a.supplier_name from lib_supplier a, lib_supplier_party_type b where a.id=b.supplier_id and b.party_type=21 and a.status_active=1 group by a.id,a.supplier_name order by a.supplier_name","id,supplier_name", 1, "-- Select --", 0, "",0 );
                        ?>
                    </td>
                    <td> 
                        <?php  
                            $search_by = array(1=>'Issue No',2=>'Challan No',3=>'In House',4=>'Out Bound Subcontact',5=>'Job No',6=>'Wo No',7=>'Buyer');
							$dd="change_search_event(this.value, '0*0*1*1*0*0*1', '0*0*select id,company_name from lib_company where status_active=1 and is_deleted=0 order by company_name*select a.id,a.supplier_name from lib_supplier a, lib_supplier_party_type b where a.id=b.supplier_id and b.party_type=2 and a.status_active=1 group by a.id,a.supplier_name order by a.supplier_name*0*0*select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company=$company $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name', '../../../') ";
							echo create_drop_down( "cbo_search_by", 150, $search_by,"",0, "--Select--", "",$dd,0 );
                        ?>
                    </td>
                    <td width="" align="center" id="search_by_td">				
                        <input type="text" style="width:230px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />	
                    </td>    
                    <td align="center">
                        <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px" placeholder="From Date" />
                        <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px" placeholder="To Date" />
                    </td> 
                    <td align="center">
                        <input type="button" name="btn_show" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_supplier').value+'_'+document.getElementById('cbo_search_by').value+'_'+document.getElementById('txt_search_common').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+<?php echo $company; ?>, 'create_mrr_search_view', 'search_div', 'grey_fabric_issue_controller', 'setFilterGrid(\'view\',-1)')" style="width:100px;" />				
                    </td>
            </tr>
        	<tr>                  
            	<td align="center" height="40" valign="middle" colspan="5">
					<?php echo load_month_buttons(1);  ?>
                    <!-- Hidden field here-------->
                     <input type="hidden" id="hidden_sys_number" value="hidden_sys_number" />
                    <!-- ---------END------------->
                </td>
            </tr>    
            </tbody>
         </tr>         
        </table>    
        <div align="center" valign="top" id="search_div" style="margin-top:10px"> </div> 
        </form>
   </div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}

if($action=="create_mrr_search_view")
{
	$ex_data = explode("_",$data);
	$supplier = $ex_data[0];
	$txt_search_by = $ex_data[1];
	$txt_search_common = trim($ex_data[2]);
	$fromDate = $ex_data[3];
	$toDate = $ex_data[4];
	$company = $ex_data[5];
 	$sql_cond="";
	if( str_replace("'","",$fromDate)!="" && str_replace("'","",$toDate)!="" )
	{
		if($db_type==0)
		{
			$sql_cond .= " and issue_date  between '".change_date_format($fromDate,'yyyy-mm-dd')."' and '".change_date_format($toDate,'yyyy-mm-dd')."'";
		}
		else
		{
			$sql_cond .= " and issue_date  between '".change_date_format($fromDate,'','',1)."' and '".change_date_format($toDate,'','',1)."'";
		}
	}
	
	if($supplier!="" && $supplier*1!=0) $sql_cond .= " and supplier_id='$supplier'";
	if($company!="" && $company*1!=0) $sql_cond .= " and company_id='$company'";
	
 	$supplier_arr = return_library_array("select id, supplier_name from lib_supplier",'id','supplier_name');
	$company_arr = return_library_array("select id, company_name from lib_company",'id','company_name');
	$buyer_arr = return_library_array("select id, buyer_name from lib_buyer",'id','buyer_name');
	$store_arr = return_library_array("select id, store_name from lib_store_location",'id','store_name');
	$issuQnty_arr = return_library_array("select mst_id, sum(issue_qnty) as qty from inv_grey_fabric_issue_dtls where status_active=1 and is_deleted=0 group by mst_id",'mst_id','qty');
	
 	if($txt_search_common!="" || $txt_search_common!=0)
	{
		if($txt_search_by==1)
		{	
			$sql_cond .= " and issue_number like '%$txt_search_common'";			
		}
		else if($txt_search_by==2)
		{		
			$sql_cond .= " and challan_no like '%$txt_search_common%'";	
 		}
		else if($txt_search_by==3)
		{
			$sql_cond .= " and knit_dye_source=1 and knit_dye_company='$txt_search_common'";
		}
		else if($txt_search_by==4)
		{
			$sql_cond .= " and knit_dye_source=2 and knit_dye_company='$txt_search_common'";
		}
		else if($txt_search_by==5)
		{
			$sql_cond .= " and buyer_job_no like '%$txt_search_common%'";	
		}
		else if($txt_search_by==6)
		{
			$sql_cond .= " and booking_no like '%$txt_search_common%'";	
		}
		else if($txt_search_by==7)
		{
			$sql_cond .= " and buyer_id = '$txt_search_common'";	
		}
	}
	
	if($db_type==0) $year_field="YEAR(insert_date)"; 
	else if($db_type==2) $year_field="to_char(insert_date,'YYYY')";
	else $year_field="";//defined Later
	
	$sql = "select id, issue_number_prefix_num, issue_number, $year_field as year, issue_basis,issue_purpose,entry_form, item_category,company_id,location_id, supplier_id,store_id,buyer_id, buyer_job_no, style_ref, booking_id, booking_no,issue_date, sample_type, knit_dye_source, knit_dye_company, challan_no, loan_party, lap_dip_no, gate_pass_no, item_color, color_range, remarks,other_party from inv_issue_master where status_active=1 and entry_form=16 $sql_cond order by id";
	//echo $sql;
	$result = sql_select( $sql );
	?>
    	<div>
            <div style="width:945px;">
                <table cellspacing="0" cellpadding="0" width="100%" class="rpt_table" border="1" rules="all">
                    <thead>
                        <th width="30">SL</th>
                        <th width="70">Issue No</th>
                        <th width="60">Year</th>
                        <th width="80">Date</th>     
                        <th width="100">Purpose</th>        
                        <th width="80">Challan No</th>
                        <th width="100">Issue Qnty</th>
                        <th width="120">Booking No</th>
                        <th width="120">Dyeing Company</th>
                        <th width="">Buyer</th>
                     </thead>
                </table>
             </div>
            <div style="width:945px;overflow-y:scroll;max-height:230px;" id="search_div" >
                <table cellspacing="0" cellpadding="0" width="927" class="rpt_table" id="view" border="1" rules="all">
				<?php	
                $i=1;   
                foreach( $result as $row )
                {
                    if ($i%2==0)  
                        $bgcolor="#E9F3FF";
                    else
                        $bgcolor="#FFFFFF";	
                        
                   // $issuQnty = return_field_value("sum(cons_quantity)","inv_transaction","mst_id=".$row[csf("id")]." and item_category=13 and transaction_type=2 and status_active=1 and is_deleted=0");	
					$issuQnty =$issuQnty_arr[$row[csf("id")]];
       			?>
                    <tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer"  onclick="js_set_value('<?php echo $row[csf("issue_number")];?>');"> 
                        <td width="30"><?php echo $i; ?></td>	
                        <td width="70"><p>&nbsp;&nbsp;<?php echo $row[csf("issue_number_prefix_num")];?></p></td>  
                        <td width="60" align="center"><p><?php echo $row[csf("year")];?></p></td>             	            			
                        <td width="80" align="center"><p><?php echo change_date_format($row[csf("issue_date")]); ?></p></td>								
                        <td width="100"><p><?php echo $yarn_issue_purpose[$row[csf("issue_purpose")]]; ?></p></td>					
                        <td width="80"><p><?php echo $row[csf("challan_no")]; ?></p></td>
                        <td width="100" align="right"><p><?php echo number_format($issuQnty,2,'.',''); ?>&nbsp;</p></td>
                        <td width="120"><p><?php echo $row[csf("booking_no")]; ?></p></td>
                        <td width="120"><p><?php 
                            if($row[csf("knit_dye_source")]==1) $knit_com=$company_arr[$row[csf("knit_dye_company")]]; else $knit_com=$supplier_arr[$row[csf("knit_dye_company")]];
                         	echo $knit_com; 
                         ?></p>
                        </td>
                        <td width=""><p><?php echo $buyer_arr[$row[csf("buyer_id")]]; ?></p></td>
                     </tr>
                    <?php
                    $i++;
                    }
                    ?>
                </table>
            </div>
        </div>
    <?php
	 
	exit();
	
}

if($action=="populate_data_from_data")
{
	$sql = "select id,issue_number,issue_basis,issue_purpose,entry_form,item_category,company_id,location_id,supplier_id,store_id,buyer_id,buyer_job_no,style_ref,booking_id,booking_no,batch_no,issue_date,sample_type,knit_dye_source,knit_dye_company,challan_no,loan_party,lap_dip_no,gate_pass_no,item_color,color_range,remarks,other_party,order_id
			from inv_issue_master 
			where issue_number='$data' and entry_form=16";
	//echo $sql;
	$res = sql_select($sql);	
	foreach($res as $row)
	{		
		echo "$('#hidden_system_id').val(".$row[csf("id")].");\n";
		echo "$('#cbo_company_name').val(".$row[csf("company_id")].");\n";
		//echo"get_php_form_data( 'requires/grey_fabric_issue_controller', ".$row[csf("company_id")].", 'load_drop_down_store', 'store_td' );\n";
		echo "get_php_form_data(".$row[csf("company_id")].", 'is_roll_maintain', 'requires/grey_fabric_issue_controller');\n"; 
 		//echo"load_drop_down( 'requires/grey_fabric_issue_controller', ".$row[csf("company_id")].", 'load_drop_down_store', 'store_td' );\n";
		echo "$('#cbo_basis').val(".$row[csf("issue_basis")].");\n";
		echo "$('#cbo_issue_purpose').val(".$row[csf("issue_purpose")].");\n";
		echo "enable_disable();\n";
		echo "$('#txt_issue_date').val('".change_date_format($row[csf("issue_date")])."');\n";
		echo "$('#txt_booking_id').val(".$row[csf("booking_id")].");\n";
		echo "$('#txt_booking_no').val('".$row[csf("booking_no")]."');\n"; 
		echo "$('#cbo_dyeing_source').val(".$row[csf("knit_dye_source")].");\n";
		echo "load_drop_down( 'requires/grey_fabric_issue_controller', ".$row[csf("knit_dye_source")]."+'**'+".$row[csf("company_id")].", 'load_drop_down_knit_com', 'dyeing_company_td' );\n";
		echo "$('#cbo_dyeing_company').val(".$row[csf("knit_dye_company")].");\n";
		 	
		echo "$('#txt_challan_no').val('".$row[csf("challan_no")]."');\n";
		
		if($db_type==0)
		{
			$batchdata = return_field_value("concat_ws('**',batch_no,color_id)","pro_batch_create_mst","id='".$row[csf("batch_no")]."'");
		}
		else
		{
			$batchdata = return_field_value("batch_no || '**' || color_id as batch_data","pro_batch_create_mst","id='".$row[csf("batch_no")]."'","batch_data");	
		}
		$batchdata=explode("**",$batchdata);
		$batchNo=$batchdata[0];
		$batchColor=return_field_value("color_name","lib_color","id='".$batchdata[1]."'");
		echo "$('#txt_batch_no').val('".$batchNo."');\n";	
		echo "$('#txt_batch_id').val(".$row[csf("batch_no")].");\n";
		echo "$('#txt_batch_color').val('".$batchColor."');\n";	
		
		echo "$('#cbo_buyer_name').val(".$row[csf("buyer_id")].");\n";
		echo "$('#txt_style_ref').val('".$row[csf("style_ref")]."');\n";
		
		if($row[csf("issue_purpose")]==8)
		{
			echo "load_drop_down( 'requires/grey_fabric_issue_controller', '".$row[csf("booking_no")]."'+'_'+".$row[csf("issue_purpose")].", 'load_drop_down_color','color_td');\n";
			echo "set_multiselect('cbo_color_id','0','0','','0');\n";
		}
		
		if($row[csf("order_id")]!="")
		{
			$orSql = sql_select("select po_number from wo_po_break_down where id in (".$row[csf("order_id")].")");
			$orderNumbers="";
			foreach($orSql as $key=>$val)
			{
				if($orderNumbers!="") $orderNumbers .=",";
				$orderNumbers .= $val[csf("po_number")];
			}
			echo "$('#txt_order_no').val('".$orderNumbers."');\n";
			echo "$('#hidden_order_id').val('".$row[csf("order_id")]."');\n";	
		}
		//echo "enable_disable();\n";
				
  	}
		
	exit();	
}




if($action=="show_dtls_list_view")
{
	$sql = "select b.id, a.issue_number, a.challan_no, b.program_no, b.issue_qnty, b.color_id, b.yarn_lot, b.yarn_count, b.store_name, b.rack, b.self, d.product_name_details
			from inv_issue_master a, inv_grey_fabric_issue_dtls b, product_details_master d 
			where a.id=b.mst_id and b.prod_id=d.id and a.entry_form=16 and a.id=$data";
	//echo $sql;
	$result = sql_select($sql);	
	
 	$yarn_count = return_library_array("select id, yarn_count from lib_yarn_count",'id','yarn_count');	
	$store_arr = return_library_array("select id, store_name from lib_store_location",'id','store_name');
	$color_arr=return_library_array( "select id, color_name from lib_color",'id','color_name');
	//$arr=array(5=>$yarn_count,6=>$store_arr);
	
	?>
    <table cellspacing="0" cellpadding="0" width="1000" class="rpt_table" border="1" rules="all">
        <thead>
            <th width="35">SL</th>
            <th width="80">Program No.</th>
            <th width="200">Item Description</th>     
            <th width="90">Quantity</th>        
            <th width="80">Challan No</th>
            <th width="90">Color</th>
            <th width="80">Yarn Lot</th>
            <th width="70">Count</th>
            <th width="50">Rack</th>
            <th width="50">Shelf</th>
            <th>Store</th>
         </thead>
    </table>
    <div style="width:1000px;overflow-y:scroll;max-height:210px;" id="search_div" >
        <table cellspacing="0" cellpadding="0" width="982" class="rpt_table" id="view" border="1" rules="all">
        <?php	
            $i=1;   
            foreach( $result as $row )
			{
                if ($i%2==0)  
                    $bgcolor="#E9F3FF";
                else
                    $bgcolor="#FFFFFF";	
				
				$count_id=array_unique(explode(",",$row[csf("yarn_count")])); $count='';
				foreach($count_id as $val)
				{
					if($count=='') $count=$yarn_count[$val]; else $count.=",".$yarn_count[$val];
				}	
				
				$color='';
				$color_id=array_unique(explode(",",$row[csf('color_id')]));
				foreach($color_id as $val)
				{
					if($val>0) $color.=$color_arr[$val].",";
				}
				$color=chop($color,',');
        	?>
                <tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer"  onclick="get_php_form_data('<?php echo $row[csf("id")]; ?>','populate_child_from_data_for_update','requires/grey_fabric_issue_controller');"> 
                    <td width="35"><?php echo $i; ?></td>	
                    <td width="80">&nbsp;&nbsp;<?php if($row[csf("program_no")]=="" || $row[csf("program_no")]==0) echo "&nbsp;"; else echo $row[csf("program_no")]; ?></td>	
                    <td width="200"><p><?php echo $row[csf("product_name_details")]; ?></p></td>								
                    <td width="90" align="right"><p><?php echo number_format($row[csf("issue_qnty")],2); ?></p></td>					
                    <td width="80"><p><?php echo $row[csf("challan_no")]; ?>&nbsp;</p></td>
                    <td width="90"><p><?php echo $color; ?>&nbsp;</p></td>
                    <td width="80"><p><?php echo $row[csf("yarn_lot")]; ?>&nbsp;</p></td>
                    <td width="70"><p><?php echo $count; ?>&nbsp;</p></td>
                    <td width="50"><p><?php echo $row[csf("rack")]; ?>&nbsp;</p></td>
                    <td width="50"><p><?php echo $row[csf("self")]; ?>&nbsp;</p></td>
                    <td><p><?php echo $store_arr[$row[csf("store_name")]]; ?>&nbsp;</p></td>
                 </tr>
			<?php
            $i++;
            }
            ?>
        </table>
    </div>
    <?php
	//echo  create_view("tbl_search", "Issue No,Item Description,Quantity,Challan No,Yarn Lot,Count,Store", "130,300,90,90,90,100,100","1000","250",0, $sql, "get_php_form_data", "id", "'populate_child_from_data_for_update'", 1, "0,0,0,0,0,yarn_count,store_name", $arr, "issue_number,product_name_details,issue_qnty,challan_no,yarn_lot,yarn_count,store_name", 'requires/grey_fabric_issue_controller','','0,0,2,0,0,0,0');
	 
	exit();
}



if($action=="populate_child_from_data_for_update")
{
	$color_arr=return_library_array( "select id, color_name from lib_color",'id','color_name');
	$sql = "select b.id, a.issue_number, a.challan_no, a.order_id, a.booking_id, a.issue_basis, a.issue_purpose, b.trans_id, b.distribution_method, b.program_no, b.no_of_roll, b.roll_no, b.roll_po_id, b.roll_wise_issue_qnty, b.prod_id, b.issue_qnty, b.color_id, b.yarn_lot, b.yarn_count, b.store_name, b.rack, b.self, b.stitch_length, b.remarks, d.product_name_details,d.current_stock
			from inv_issue_master a, inv_grey_fabric_issue_dtls b, product_details_master d 
			where a.id=b.mst_id and b.prod_id=d.id and a.entry_form=16 and b.id=$data";
	//echo $sql;
	$result = sql_select($sql);		
	foreach($result as $row)
	{			
		echo "$('#cbo_store_name').val('".$row[csf('store_name')]."');\n";
		echo "$('#txtNoOfRoll').val('".$row[csf('no_of_roll')]."');\n";
		echo "$('#txtRollNo').val('".$row[csf('roll_no')]."');\n";
		echo "$('#txtRollPOid').val('".$row[csf('roll_po_id')]."');\n";
		echo "$('#txtRollPOQnty').val('".$row[csf('roll_wise_issue_qnty')]."');\n";
		
		echo "$('#txtItemDescription').val('".$row[csf('product_name_details')]."');\n";
		echo "$('#txt_global_stock').val('".$row[csf('current_stock')]."');\n";
		echo "$('#hiddenProdId').val('".$row[csf('prod_id')]."');\n";
		
		echo "$('#txtReqQnty').val('".$row[csf('issue_qnty')]."');\n";
		echo "$('#txtIssueQnty').val('".$row[csf('issue_qnty')]."');\n";
		echo "$('#hiddenIssueQnty').val('".$row[csf('issue_qnty')]."');\n";
		
		echo "$('#txtYarnLot').val('".$row[csf('yarn_lot')]."');\n";
		//echo "$('#cbo_yarn_count').val('".$row[csf('yarn_count')]."');\n";
		if($row[csf('yarn_count')]==0) $count=""; else $count=$row[csf('yarn_count')];
		
		echo "$('#txt_rack').val('".$row[csf('rack')]."');\n";
		echo "$('#txt_self').val('".$row[csf('self')]."');\n";
		echo "$('#txt_remarks').val('".$row[csf('remarks')]."');\n";
		echo "$('#txt_stitch_length').val('".$row[csf('stitch_length')]."');\n";
		
		if($row[csf('program_no')]==0) $program_no=""; else $program_no=$row[csf('program_no')];
		echo "$('#txt_program_no').val('".$program_no."');\n";
		
		//echo "disable_enable_fields('show_textcbo_yarn_count','1','','');\n";
		//issue qnty popup data arrange 
		$sqlIN = sql_select("select trans_id,po_breakdown_id,quantity from order_wise_pro_details where dtls_id=".$row[csf("id")]." and entry_form=16 and trans_type=2");
		$poWithValue="";
		$poID="";
		$transaction_id="";
		foreach($sqlIN as $res)
		{
			if($poWithValue!="") $poWithValue .=",";
			if($poID!="") $poID .=",";
			$poWithValue .= $res[csf("po_breakdown_id")]."**".$res[csf("quantity")];
			$poID .=$res[csf("po_breakdown_id")];
			$transaction_id = $res[csf("trans_id")];
		}
		
		echo "$('#save_data').val('".$poWithValue."');\n"; 
		echo "$('#all_po_id').val('".$poID."');\n"; 
		echo "$('#distribution_method_id').val('".$row[csf('distribution_method')]."');\n";
		
		if($poID!="")
		{
			echo "load_drop_down( 'requires/grey_fabric_issue_controller', '$poID'+'_'+".$row[csf('issue_purpose')]."+'_'+'".$row[csf('color_id')]."', 'load_drop_down_color','color_td');\n";
			echo "set_multiselect('cbo_color_id','0','0','','0');\n";
		}
		
		//echo "$('#cbo_color_id').val('".$row[csf('color_id')]."');\n";
		echo "set_multiselect('cbo_yarn_count*cbo_color_id','0*0','1','".$count."*".$row[csf('color_id')]."','0*0');\n";
		//--------hidden id for update------- 
		echo "$('#dtls_tbl_id').val('".$row[csf('id')]."');\n";
 		echo "$('#trans_tbl_id').val('".$row[csf('trans_id')]."');\n";
		echo "$('#hidden_yet_issue_qnty').val(".($row[csf('current_stock')]+$row[csf('issue_qnty')]).");\n";
		
		if($row[csf('order_id')]!="")
		{
			echo "get_php_form_data('".$row[csf('order_id')]."'+\"**\"+".$row[csf('prod_id')].", \"populate_data_about_order\", \"requires/grey_fabric_issue_controller\" );";
		}
		if($row[csf('issue_basis')]==1 && ($row[csf('issue_purpose')]==3 || $row[csf('issue_purpose')]==8 || $row[csf('issue_purpose')]==26 || $row[csf('issue_purpose')]==29 || $row[csf('issue_purpose')]==30 || $row[csf('issue_purpose')]==31))
		{
			echo "get_php_form_data('".$row[csf('booking_id')]."'+\"**\"+".$row[csf('prod_id')].", \"populate_data_about_sample\", \"requires/grey_fabric_issue_controller\" );";
		}
		
		echo "set_button_status(1, permission, 'fnc_grey_fabric_issue_entry',1,1);\n";
	}
	exit();
}

if ($action=="grey_fabric_issue_print")
{
    extract($_REQUEST);
	$data=explode('*',$data);
	//print_r ($data);
	
	$sql="select id, issue_number, issue_date, issue_basis, issue_purpose, knit_dye_source, knit_dye_company, booking_id, batch_no, buyer_id, challan_no, style_ref, order_id from  inv_issue_master where id='$data[1]' and company_id='$data[0]'";
	//echo $sql;die;
	$supplier_library=array(); $supplier_short_library=array();
	$supplier_data=sql_select( "select id,supplier_name,short_name from lib_supplier");
	foreach ($supplier_data as $row)
	{
		$supplier_library[$row[csf('id')]]=$row[csf('supplier_name')];
		$supplier_short_library[$row[csf('id')]]=$row[csf('short_name')];
	}
	
	$dataArray=sql_select($sql);
	$grey_issue_basis=array(1=>"Booking",2=>"Independent",3=>"Knitting Plan");
	$company_library=return_library_array( "select id, company_name from lib_company", "id", "company_name"  );
	$buyer_library=return_library_array( "select id, buyer_name from lib_buyer", "id", "buyer_name"  );
	$store_library=return_library_array( "select id, store_name from  lib_store_location", "id", "store_name"  );
	$country_arr=return_library_array( "select id, country_name from  lib_country", "id", "country_name"  );
	$batch_arr=return_library_array( "select id, batch_no from  pro_batch_create_mst", "id", "batch_no"  );
	$booking_arr=return_library_array( "select id, booking_no from  wo_booking_mst", "id", "booking_no"  );
	$booking_non_order_arr=return_library_array( "select id, booking_no from  wo_non_ord_samp_booking_mst", "id", "booking_no"  );
	$po_arr=return_library_array( "select id, po_number from  wo_po_break_down", "id", "po_number"  );
	$job_arr=return_library_array( "select id, job_no_mst from  wo_po_break_down","id","job_no_mst");
 	$yarn_count_arr = return_library_array("select id, yarn_count from lib_yarn_count",'id','yarn_count');
	$brand_arr = return_library_array("select id, brand_name from lib_brand",'id','brand_name');	
	
?>
    <div style="width:930px;">
    <table width="900" cellspacing="0" align="right" style="margin-bottom:10px">
        <tr>
            <td colspan="6" align="center" style="font-size:22px"><strong><?php echo $company_library[$data[0]]; ?></strong></td>
        </tr>
        <tr class="form_caption">
        	<td colspan="6" align="center" style="font-size:14px">  
				<?php
					$nameArray=sql_select( "select plot_no,level_no,road_no,block_no,country_id,province,city,zip_code,email,website from lib_company where id=$data[0]"); 
					foreach ($nameArray as $result)
					{ 
					?>
						Plot No: <?php echo $result['plot_no']; ?> 
						Level No: <?php echo $result['level_no']?>
						Road No: <?php echo $result['road_no']; ?> 
						Block No: <?php echo $result['block_no'];?> 
						City No: <?php echo $result['city'];?> 
						Zip Code: <?php echo $result['zip_code']; ?> 
						Province No: <?php echo $result['province'];?> 
						Country: <?php echo $country_arr[$result['country_id']]; ?><br> 
						Email Address: <?php echo $result['email'];?> 
						Website No: <?php echo $result['website'];
					}
                ?> 
            </td>  
        </tr>
        <tr>
            <td colspan="6" align="center" style="font-size:18px"><strong><u><?php echo $data[3]; ?> Challan</u></strong></td>
        </tr>
        <tr>
             <td rowspan="3" colspan="2" width="300" valign="top"><strong>Dyeing Company:</strong> 
				<?php
					$supp_add=$dataArray[0][csf('knit_dye_company')];
					$nameArray=sql_select( "select address_1,web_site,email,country_id from lib_supplier where id=$supp_add"); 
					foreach ($nameArray as $result)
					{ 
                    	$address="";
						if($result!="") $address=$result['address_1'];//.'<br>'.$country_arr[$result['country_id']].'<br>'.$result['email'].'<br>'.$result['web_site']
					}
              if ($dataArray[0][csf('knit_dye_source')]==1) echo $company_library[$dataArray[0][csf('knit_dye_company')]].'<br>'.'Address :- '.$address; else if ($dataArray[0][csf('knit_dye_source')]==3) echo $supplier_library[$dataArray[0][csf('knit_dye_company')]].'<br>'.'Address :- '.$address; ?></td>
        	<td width="120"><strong>Issue ID :</strong></td><td width="175px"><?php echo $dataArray[0][csf('issue_number')]; ?></td>
            <td width="125"><strong>Issue Date:</strong></td><td width="175px"><?php echo change_date_format($dataArray[0][csf('issue_date')]); ?></td>
        </tr>
        <tr>
            <td><strong>Issue Basis :</strong></td> <td width="175px"><?php echo $grey_issue_basis[$dataArray[0][csf('issue_basis')]]; ?></td>
            <td><strong>Issue Purpose:</strong></td><td width="175px"><?php echo $yarn_issue_purpose[$dataArray[0][csf('issue_purpose')]]; ?></td>
        </tr>
        <tr>
            <td><strong>Dyeing Source:</strong></td><td width="175px"><?php echo $knitting_source[$dataArray[0][csf('knit_dye_source')]]; ?></td>
            <td><strong>F. Booking No:</strong></td><td width="175px">
				<?php 
				if($dataArray[0][csf('issue_basis')]==1)
				{
					if(($dataArray[0][csf('issue_purpose')]==8 || $dataArray[0][csf('issue_purpose')]==3 || $dataArray[0][csf('issue_purpose')]==26 || $dataArray[0][csf('issue_purpose')]==29 || $dataArray[0][csf('issue_purpose')]==30 || $dataArray[0][csf('issue_purpose')]==31) && $dataArray[0][csf('issue_basis')]==1) 
					{
						echo $booking_non_order_arr[$dataArray[0][csf('booking_id')]]; 
					}
					else 
					{
						echo $booking_arr[$dataArray[0][csf('booking_id')]];
					}
				}
				else
				{
					echo $dataArray[0][csf('booking_id')];
				}
				?></td>
        </tr>
        <tr>
            <td><strong>Challan No:</strong></td><td width="175px"><?php echo $dataArray[0][csf('challan_no')]; ?></td>
            <td><strong>Batch Number:</strong></td><td width="175px"><?php echo $batch_arr[$dataArray[0][csf('batch_no')]]; ?></td>
            <td><strong>Buyer Name:</strong></td><td width="175px"><?php echo $buyer_library[$dataArray[0][csf('buyer_id')]]; ?></td>
        </tr>
        <tr>
            <td>
            <?php
				if($db_type==0)
				{
					$po_id=return_field_value("group_concat(b.po_breakdown_id) as po_id","inv_grey_fabric_issue_dtls a, order_wise_pro_details b","a.id=b.dtls_id and a.mst_id='$data[1]' and b.entry_form=16 and b.trans_type=2 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0","po_id");
				}
				else
				{
					$po_id=return_field_value("LISTAGG(b.po_breakdown_id, ',') WITHIN GROUP (ORDER BY b.po_breakdown_id) as po_id","inv_grey_fabric_issue_dtls a, order_wise_pro_details b","a.id=b.dtls_id and a.mst_id='$data[1]' and b.entry_form=16 and b.trans_type=2 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0","po_id");
				}
				
				$po_exp=array_unique(explode(',',$po_id));
				$po_no=''; $job='';
				foreach($po_exp as $row)
				{
					if($po_no=='') $po_no=$po_arr[$row]; else $po_no.=', '.$po_arr[$row];
					if($job=='') $job=$job_arr[$row]; else $job.=','.$job_arr[$row];
				}
				
				$job=implode(",",array_unique(explode(',',$job)));
			?>
            <strong>Job No:</strong></td>
            <td width="175px" colspan="3"><?php echo $job;//$job_arr[$dataArray[0][csf('order_id')]]; ?></td>
            <td><strong>Style Ref.:</strong></td><td width="175px"><?php echo $dataArray[0][csf('style_ref')]; ?></td>
        </tr>
        <tr>
        	 <td><strong>Order No:</strong></td><td colspan="5"><?php echo $po_no;//$po_arr[$dataArray[0][csf('order_id')]]; ?></td>
        </tr>
        <tr>
            <td><strong>Bar Code:</strong></td><td colspan="3" id="barcode_img_id"></td>
        </tr>
    </table>
    <div style="width:100%;">
    <table align="right" cellspacing="0" width="930"  border="1" rules="all" class="rpt_table" >
        <thead bgcolor="#dddddd" align="center">
            <th width="20">SL</th>
            <th width="50">Prog. No</th>
            <th width="130">Item Des.</th>
            <th width="50">Stich Length</th>
            <th width="40">GSM</th>
            <th width="40">Fin. Dia</th>
            <th width="30">M/C Dia</th>
            <th width="70">Color</th>
            <th width="40">Roll</th>
            <th width="70">Issue Qty</th> 
            <th width="40">UOM</th>
            <th width="50">Count</th>
            <th width="40">Supplier</th>
            <th width="50">Yarn Lot</th>
            <th width="30">Rack</th>
            <th width="30">Shelf</th>
            <th width="80">Store</th> 
            <th>Remarks</th> 
        </thead>
        <tbody> 
		<?php
		$color_arr = return_library_array( "select id, color_name from lib_color",'id','color_name');
		$product_array=array();
		$product_sql = sql_select("select id, supplier_id,item_category_id,product_name_details,lot,gsm,dia_width,color,brand,unit_of_measure from product_details_master where item_category_id in(1,13)");
		foreach($product_sql as $row)
		{
			$product_array[$row[csf("id")]]['product_name_details']=$row[csf("product_name_details")];
			$product_array[$row[csf("id")]]['gsm']=$row[csf("gsm")];
			$product_array[$row[csf("id")]]['dia_width']=$row[csf("dia_width")];
			$product_array[$row[csf("id")]]['brand']=$row[csf("brand")];
			$product_array[$row[csf("id")]]['color']=$row[csf("color")];
			$product_array[$row[csf("id")]]['uom']=$row[csf("unit_of_measure")];
			
			if($row[csf("item_category_id")]==1)
			{
				$product_array[$row[csf("lot")].'l']['lot']=$row[csf("brand")];
				$product_array[$row[csf("lot")]]['supp']=$supplier_short_library[$row[csf("supplier_id")]];
			}
		}
		
		$sql_dtls = "select b.id, b.program_no, b.prod_id, a.booking_id, b.issue_qnty, b.no_of_roll, b.yarn_lot, b.yarn_count, b.store_name, b.color_id, b.rack, b.self, b.stitch_length,b.remarks from inv_issue_master a, inv_grey_fabric_issue_dtls b where a.id=b.mst_id and a.entry_form=16 and a.id=$data[1] and b.status_active=1 and b.is_deleted=0";
		//echo $sql;
		$sql_result= sql_select($sql_dtls);
		$i=1; $all_program_no='';
		foreach($sql_result as $row)
		{
			if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
			if($row[csf('program_no')]!='')
			{
				if ($all_program_no=='') $all_program_no=$row[csf('program_no')]; else $all_program_no.=', '.$row[csf('program_no')];
			}	
					
			$roll_qty_sum+=$row[csf('no_of_roll')];
			$issue_qnty_sum+=$row[csf('issue_qnty')];
			
			$item_des=explode(',',$product_array[$row[csf("prod_id")]]['product_name_details']);
			//print_r ($item_des);
			if($item_des[0]!='' && $item_des[1]!='')
			{
				$item_name_details=$item_des[0].', '.$item_des[1];
			}
			else
			{
				$item_name_details='';
			}
			
			$yarn_count=$row[csf("yarn_count")];
			$count_id=explode(',',$yarn_count);
			$count_val='';
			foreach ($count_id as $val)
			{
				if($count_val=='') $count_val=$yarn_count_arr[$val]; else $count_val.=", ".$yarn_count_arr[$val];
			}
			
			$yarn_lot=$row[csf("yarn_lot")];
			$yarn_lot_id=explode(',',$yarn_lot);
			$yarn_lot_supp='';
			foreach ($yarn_lot_id as $val)
			{
				if($yarn_lot_supp=='') $yarn_lot_supp=$product_array[$val]['supp']; else $yarn_lot_supp.=", ".$product_array[$val]['supp'];
			}
			
			?>
			<tr bgcolor="<?php echo $bgcolor; ?>">
				<td align="center"><?php echo $i; ?></td>
				<td><p><?php if($row[csf("program_no")]==0) echo "&nbsp;"; else echo $row[csf("program_no")]; ?></p></td>
				<td><p><?php echo $item_name_details; ?></p></td>
				<td><p><?php echo $row[csf("stitch_length")]; ?></p></td>
				<td align="center"><p><?php echo $product_array[$row[csf("prod_id")]]['gsm']; ?></p></td>
				<td align="center"><p><?php echo $product_array[$row[csf("prod_id")]]['dia_width']; ?></p></td>
				<td align="center"><p><?php //echo $row[csf("booking_id")]; ?></p></td>
				<td><p><?php echo $color_arr[$row[csf("color_id")]]; ?></p></td>
				<td align="right"><?php echo $row[csf("no_of_roll")]; ?></td>
				<td align="right"><?php echo number_format($row[csf("issue_qnty")],2); ?></td>
				<td align="center"><p><?php echo $unit_of_measurement[$product_array[$row[csf("prod_id")]]['uom']]; ?></p></td>
				<td align="center"><p><?php echo $count_val; ?></p></td>
				<td><p><?php echo $yarn_lot_supp; ?></p></td>
				<td><p><?php echo $row[csf("yarn_lot")]; ?></p></td>
				<td align="center"><p><?php echo $row[csf("rack")]; ?></p></td>
				<td align="center"><p><?php echo $row[csf("self")]; ?></p></td>
				<td><p><?php echo $store_library[$row[csf("store_name")]]; ?></p></td>
				<td><p><?php echo $row[csf("remarks")]; ?></p></td>
			</tr>
			<?php $i++; 
			} ?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="8" align="right"><strong>Total :</strong></td>
				<td align="right"><?php echo $roll_qty_sum; ?></td>
				<td align="right"><?php echo number_format($issue_qnty_sum,2); ?></td>
				<td align="right" colspan="8"><?php //echo $req_qny_edit_sum; ?></td>
			</tr>                           
		</tfoot>
	</table>
    <br>
    <br>&nbsp;
    <!--================================================================-->
    <?php 
    if($dataArray[0][csf('issue_basis')]==3)
    {
    ?>
        <table width="850" border="1" rules="all" cellpadding="0" cellspacing="0" class="rpt_table">
            <thead>
                <tr>
                    <th colspan="7" align="center">Requisition Details</th>
                </tr>
                <tr>
                    <th width="40">SL</th>
                    <th width="100">Requisition No</th>
                    <th width="110">Lot No</th>
                    <th width="220">Yarn Description</th>
                    <th width="110">Brand</th>
                    <th width="90">Requisition Qty</th>
                    <th>Remarks</th>
                </tr>
            </thead>
            <?php
                $i=1; $tot_reqsn_qnty=0;
                $product_details_array=array();
                $sql_prod="select id, supplier_id, lot, current_stock, yarn_comp_type1st, yarn_comp_percent1st, yarn_comp_type2nd, yarn_comp_percent2nd, yarn_count_id, yarn_type, color, brand from product_details_master where item_category_id=13 and company_id='$data[0]' and status_active=1 and is_deleted=0";
                $result_prod = sql_select($sql_prod);
                
                foreach($result_prod as $row)
                {
                    $compos='';
                    if($row[csf('yarn_comp_percent2nd')]!=0)
                    {
                        $compos=$composition[$row[csf('yarn_comp_type1st')]]." ".$row[csf('yarn_comp_percent1st')]."%"." ".$composition[$row[csf('yarn_comp_type2nd')]]." ".$row[csf('yarn_comp_percent2nd')]."%";
                    }
                    else
                    {
                        $compos=$composition[$row[csf('yarn_comp_type1st')]]." ".$row[csf('yarn_comp_percent1st')]."%"." ".$composition[$row[csf('yarn_comp_type2nd')]];
                    }
                    $product_details_array[$row[csf('id')]]['count']=$count_arr[$row[csf('yarn_count_id')]];
                    $product_details_array[$row[csf('id')]]['comp']=$compos;
                    $product_details_array[$row[csf('id')]]['type']=$yarn_type[$row[csf('yarn_type')]];
                    $product_details_array[$row[csf('id')]]['lot']=$row[csf('lot')];
                    $product_details_array[$row[csf('id')]]['brand']=$brand_arr[$row[csf('brand')]];
                }	
                            
                $sql_knit="select knit_id, requisition_no, prod_id, sum(yarn_qnty) as yarn_qnty from ppl_yarn_requisition_entry where knit_id in ($all_program_no) and status_active=1 and is_deleted=0 group by requisition_no, prod_id, knit_id";
                $nameArray=sql_select( $sql_knit );
                foreach ($nameArray as $selectResult)
                {
                    ?>
                    <tr>
                        <td width="40" align="center"><?php echo $i; ?></td>
                        <td width="100" align="center">&nbsp;<?php echo $selectResult[csf('requisition_no')]; ?></td>
                        <td width="110" align="center">&nbsp;<?php echo $product_details_array[$selectResult[csf('prod_id')]]['lot']; ?></td>
                        <td width="220">&nbsp;<?php echo $product_details_array[$selectResult[csf('prod_id')]]['count']." ".$product_details_array[$selectResult[csf('prod_id')]]['comp']." ".$product_details_array[$selectResult[csf('prod_id')]]['type']; ?></td>
                        <td width="110" align="center">&nbsp;<?php echo $brand_arr[$product_details_array[$selectResult[csf('prod_id')]]['brand']]; ?></td>
                        <td width="90" align="right"><?php echo number_format($selectResult[csf('yarn_qnty')],2); ?>&nbsp;</td>
                        <td>&nbsp;<?php //echo $selectResult[csf('requisition_no')]; ?></td>	
                    </tr>
                    <?php
                    $tot_reqsn_qnty+=$selectResult[csf('yarn_qnty')];
                    $i++;
                }
            ?>
            <tfoot>
                <th colspan="5" align="right"><b>Total</b></th>
                <th align="right"><?php echo number_format($tot_reqsn_qnty,2); ?>&nbsp;</th>
                <th>&nbsp;</th>
            </tfoot>
        </table>
        <?php
        if($data[5]!=1)
        {
            $z=1; $k=1;
            $colorArray=sql_select("select a.id, a.mst_id, a.knitting_source, a.knitting_party, a.program_date,a. color_range, a.stitch_length, a.machine_dia, a.machine_gg, a.program_qnty, a.remarks, b.knit_id, c.booking_no, c.body_part_id, c.fabric_desc, c.gsm_weight, c.dia from ppl_planning_info_entry_dtls a, ppl_yarn_requisition_entry b, ppl_planning_info_entry_mst c where a.mst_id= c.id and a.id=b.knit_id and b.knit_id in ($all_program_no) and a.status_active=1 and a.is_deleted=0 and b.is_deleted=0 and b.is_deleted=0");
            $booking_no="";
            foreach ($colorArray as $book)
            {
                if ($booking_no=="") $booking_no=$book[csf('booking_no')]; else $booking_no.=','.$book[csf('booking_no')];
            }
            $booking_count=count(array_unique(explode(',',$booking_no)));
            ?> 
            
            <table width="710" cellpadding="0" cellspacing="0" border="1" rules="all" style="margin-top:20px;" class="rpt_table">
                <thead>
                    <th width="40">SL</th>
                    <th width="250">Fabrication</th>
                    <th width="120">Color</th>
                    <th width="120">GGSM OR S/L</th>
                    <th>FGSM</th>
                 </thead>
                 <tbody>
                    <tr>
                        <td width="40" align="center"><?php echo $z; ?></td>
                        <td width="250" align="center"><?php echo $body_part[$colorArray[0][csf('body_part_id')]].', '.$colorArray[0][csf('fabric_desc')]; ?></td>
                        <td width="120" align="center"><?php echo $color_range[$colorArray[0][csf('color_range')]]; ?></td>
                        <td width="120" align="center"><?php echo $colorArray[0][csf('stitch_length')]; ?></td>
                        <td align="center"><?php echo $colorArray[0][csf('gsm_weight')]; ?></td>
                    </tr>
                </tbody>
            </table>
            <table style="margin-top:20px;" width="650" border="1" rules="all" cellpadding="0" cellspacing="0" class="rpt_table">
                <thead>
                    <th width="40">SL</th>
                    <th width="50">Prog. No</th>
                    <th width="110">Finish Dia</th>
                    <th width="170">Machine Dia & Gauge</th>
                    <th width="110">Program Qty</th>
                    <th>Remarks</th>
                </thead>
                <tbody>
                    <tr>
                        <td width="40" align="center"><?php echo $k; ?></td>
                        <td width="50" align="center">&nbsp;<?php echo $colorArray[0][csf('knit_id')]; ?></td>
                        <td width="110" align="center"><?php echo $colorArray[0][csf('dia')]; ?></td>
                        <td width="170" align="center"><?php echo $colorArray[0][csf('machine_dia')]."X".$colorArray[0][csf('machine_gg')]; ?></td>
                        <td width="110" align="right"><?php echo number_format($colorArray[0][csf('program_qnty')],2); ?>&nbsp;</td>
                        <td><?php echo $colorArray[0][csf('remarks')]; ?></td>
                    </tr>
                    <?php
                    $tot_prog_qty+=$colorArray[0][csf('program_qnty')];
                    ?>
                    <tr>
                        <td colspan="4" align="right"><strong>Total : </strong></td>
                        <td align="right"><?php echo number_format($tot_prog_qty,2); ?></td>
                        <td>&nbsp;</td>
                    </tr>
                </tbody>
            </table> 
            <table style="margin-top:20px;" width="500" border="1" rules="all" cellpadding="0" cellspacing="0" class="rpt_table">
                <thead>
                    <tr><th colspan="4" align="center">Comments (Booking No: <?php if ($booking_count>1) echo "Multiple Booking."; else echo $colorArray[0][csf('booking_no')]; ?>)</th></tr>
                    <tr>
                        <th>Req. Qty</th>
                        <th>Cuml. Issue Qty</th>
                        <th>Balance Qty</th>
                        <th>Remarks</th>
                    </tr>
                </thead>
                 <?php
                     if($db_type==0)
                     {
                         $reqs_no= " group_concat(b.requisition_no)";
                     }
                     else if($db_type==2)
                     {
                         $reqs_no="LISTAGG(b.requisition_no, ',') WITHIN GROUP (ORDER BY b.requisition_no)";
                     }
                    $all_booking_sql=sql_select("select $reqs_no as requisition_no from ppl_planning_info_entry_dtls a, ppl_yarn_requisition_entry b, ppl_planning_info_entry_mst c where a.mst_id= c.id and a.id=b.knit_id and c.booking_no='".$colorArray[0][csf('booking_no')]."' and a.status_active=1 and a.is_deleted=0 and b.is_deleted=0 and b.is_deleted=0 group by c.booking_no");
                    $all_requ_no=$all_booking_sql[0][csf('requisition_no')];
                    
                    if( $dataArray[0][csf('issue_purpose')]==8 )
                    {
                        $sql = "select a.id, sum(b.grey_fabric) as fabric_qty from wo_non_ord_samp_booking_mst a, wo_non_ord_samp_booking_dtls b where a.booking_no=b.booking_no and a.item_category=2 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.booking_no='".$colorArray[0][csf('booking_no')]."' group by a.id"; 
                    }
                    else if( $dataArray[0][csf('issue_purpose')]==2)
                    {					
                        $sql = "select a.id, sum(b.yarn_wo_qty) as fabric_qty from wo_yarn_dyeing_mst a, wo_yarn_dyeing_dtls b where a.id=b.mst_id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.booking_no='".$colorArray[0][csf('booking_no')]."' group by a.id"; 
                    }
                    else
                    {
                        $sql = "select a.id, sum(b.grey_fab_qnty) as fabric_qty from wo_booking_mst a,  wo_booking_dtls b where a.booking_no=b.booking_no and a.item_category=2 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.booking_no='".$colorArray[0][csf('booking_no')]."' group by a.id"; 
                    }
                    //echo $sql;
                    $result = sql_select($sql);
                    
                    $total_issue_qty=return_field_value("sum(a.cons_quantity) as issue_qty"," inv_transaction a, inv_issue_master b","b.id=a.mst_id and a.requisition_no in ($all_program_no) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.transaction_type=2 and b.item_category=13","issue_qty");		 
                    $total_return_qty=return_field_value("sum(a.cons_quantity) as return_qty"," inv_transaction a, inv_receive_master b","b.id=a.mst_id and b.booking_id in ($all_program_no) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.transaction_type=4 and b.item_category=13","return_qty");		 							
                    if ($booking_count==1)
                    {
                     ?>
                     <tbody>
                         <tr>
                             <td align="center">
                                 <?php echo number_format($result[0][csf('fabric_qty')],3); ?>
                             </td>
                             <td align="center">
                                 <?php $cumulative_qty=$total_issue_qty-$total_return_qty; echo number_format($cumulative_qty,3); ?>
                             </td>
                             <td align="center">
                                 <?php $balance_qty=$result[0][csf('fabric_qty')]-$cumulative_qty; echo number_format($balance_qty,3);?>
                             </td>
                             <td align="center">
                                 <?php if ($result[0][csf('fabric_qty')]>$cumulative_qty) echo "Less"; else if ($result[0][csf('fabric_qty')]<$cumulative_qty) echo "Over"; else echo "";?>
                             </td>
                         </tr>
                     </tbody>
                 </table>
            <?php
                }
            }
        }
        else if($dataArray[0][csf('issue_basis')]==1)
        {
			if($dataArray[0][csf('issue_purpose')]==8 )
			{
				$sql = "select a.id, a.booking_no, sum(b.grey_fabric) as fabric_qty from wo_non_ord_samp_booking_mst a, wo_non_ord_samp_booking_dtls b where a.booking_no=b.booking_no and a.item_category=2 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.id='".$dataArray[0][csf('booking_id')]."' group by a.id, a.booking_no"; 
			}
			else if( $dataArray[0][csf('issue_purpose')]==2)
			{					
				$sql = "select a.id, a.ydw_no as booking_no, sum(b.yarn_wo_qty) as fabric_qty from wo_yarn_dyeing_mst a, wo_yarn_dyeing_dtls b where a.id=b.mst_id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.id='".$dataArray[0][csf('booking_id')]."' group by a.id, a.ydw_no"; 
			}
			else
			{
				$sql = "select a.id, a.booking_no, sum(b.grey_fab_qnty) as fabric_qty from wo_booking_mst a,  wo_booking_dtls b where a.booking_no=b.booking_no and a.item_category=2 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.id='".$dataArray[0][csf('booking_id')]."' group by a.id, a.booking_no"; 
			}
			$result = sql_select($sql);
			
			$total_issue_qty=return_field_value("sum(a.cons_quantity) as total_issue_qty"," inv_transaction a, inv_issue_master b","b.id=a.mst_id and b.booking_id='".$dataArray[0][csf('booking_id')]."' and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.transaction_type=2 and b.item_category=13","total_issue_qty");		 
			$total_return_qty=return_field_value("sum(a.cons_quantity) as total_issue_qty"," inv_transaction a, inv_receive_master b","b.id=a.mst_id and b.booking_id='".$dataArray[0][csf('booking_id')]."' and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.transaction_type=4 and b.item_category=13","total_issue_qty");		 
		 ?>  
             <table style="margin-top:20px;" width="500" border="1" rules="all" cellpadding="0" cellspacing="0" class="rpt_table">
                <thead>
                    <tr><th colspan="4" align="center">Comments (Booking : <?php echo $result[0][csf('booking_no')]; ?>)</th></tr>
                    <tr>
                        <th>Req. Qty</th>
                        <th>Cuml. Issue Qty</th>
                        <th>Balance Qty</th>
                        <th>Remarks</th>
                    </tr>
                </thead>
                
                 <tbody>
                     <tr>
                         <td align="center">
                             <?php echo number_format($result[0][csf('fabric_qty')],3); ?>
                         </td>
                         <td align="center">
                             <?php $cumulative_qty=$total_issue_qty-$total_return_qty; echo number_format($cumulative_qty,3); ?>
                         </td>
                         <td align="center">
                             <?php $balance_qty=$result[0][csf('fabric_qty')]-$cumulative_qty; echo number_format($balance_qty,3);?>
                         </td>
                         <td align="center">
                             <?php if($result[0][csf('fabric_qty')]>$cumulative_qty) echo "Less"; else if ($result[0][csf('fabric_qty')]<$cumulative_qty) echo "Over"; else echo ""; ?>
                         </td>
                     </tr>
                </tbody>
            </table>
        <?php
        }
        echo signature_table(17, $data[0], "900px");
        ?>
      </div>
   </div>          
  <script type="text/javascript" src="../../../js/jquery.js"></script>
     <script type="text/javascript" src="../../../js/jquerybarcode.js"></script>
     <script>

	function generateBarcode( valuess )
	{
		var value = valuess;//$("#barcodeValue").val();
	 // alert(value)
		var btype = 'code39';//$("input[name=btype]:checked").val();
		var renderer ='bmp';// $("input[name=renderer]:checked").val();
		 
		var settings = {
		  output:renderer,
		  bgColor: '#FFFFFF',
		  color: '#000000',
		  barWidth: 1,
		  barHeight: 30,
		  moduleSize:5,
		  posX: 10,
		  posY: 20,
		  addQuietZone: 1
		};
		$("#barcode_img_id").html('11');
		 value = {code:value, rect: false};
		
		$("#barcode_img_id").show().barcode(value, btype, settings);
	} 
	generateBarcode('<?php echo $data[2]; ?>');
	</script>
	<?php
    exit();
}
?>