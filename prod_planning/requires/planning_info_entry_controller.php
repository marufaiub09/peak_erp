<?php
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_REQUEST);
$permission=$_SESSION['page_permission'];

include('../../includes/common.php');

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

$buyer_arr=return_library_array( "select id, short_name from lib_buyer",'id','short_name');
$company_arr=return_library_array( "select id, company_short_name from lib_company",'id','company_short_name');
$supllier_arr=return_library_array( "select id, supplier_name from lib_supplier",'id','supplier_name');
$machine_arr=return_library_array( "select id, machine_no from lib_machine_name",'id','machine_no');
//$color_library=return_library_array( "select id,color_name from lib_color", "id", "color_name"  );
$yarn_count_details=return_library_array( "select id,yarn_count from lib_yarn_count", "id", "yarn_count"  );

if($db_type==0) $select_field="group"; 
else if($db_type==2) $select_field="wm";
else $select_field="";//defined Later

if ($action=="load_drop_down_buyer")
{
	echo create_drop_down( "cbo_buyer_name", 150, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) $buyer_cond order by buyer_name","id,buyer_name", 1, "-- All Buyer --", $selected, "" ,0); 
	exit();
}

if($action=="load_drop_down_knitting_party")
{
	$data=explode("**",$data);
	if($data[0]==1)
	{
		echo create_drop_down( "cbo_knitting_party", 177, "select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name","id,company_name",1, "--Select Knit Party--", $data[1], "","" );
	}
	else if($data[0]==3)
	{	
		if($data[2]==1) $selected_id=$data[1]; else $selected_id=0;
		echo create_drop_down( "cbo_knitting_party", 177, "select a.id, a.supplier_name from lib_supplier a, lib_supplier_party_type b where a.id=b.supplier_id and b.party_type=20 and a.status_active=1 and a.is_deleted=0 group by a.id, a.supplier_name order by a.supplier_name","id,supplier_name", 1, "--Select Knit Party--", $selected_id, "" );
	}
	else
	{
		echo create_drop_down( "cbo_knitting_party", 177, $blank_array,"",1, "--Select Knit Party--", 0, "" );
	}
	exit();
}


if($action=="style_ref_search_popup")
{
	echo load_html_head_contents("Style Reference Info", "../../", 1, 1,'','','');
	extract($_REQUEST);
	?>
     
	<script>
		
		var selected_id = new Array; var selected_name = new Array;
		
		function check_all_data()
		{
			var tbl_row_count = document.getElementById( 'tbl_list_search' ).rows.length;
			tbl_row_count = tbl_row_count - 1;

			for( var i = 1; i <= tbl_row_count; i++ )
			{
				$('#tr_'+i).trigger('click'); 
			}
		}
		
		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
		function js_set_value( str ) {
			
			if (str!="") str=str.split("_");
			 
			toggle( document.getElementById( 'tr_' + str[0] ), '#FFFFCC' );
			 
			if( jQuery.inArray( str[1], selected_id ) == -1 ) {
				selected_id.push( str[1] );
				selected_name.push( str[2] );
				
			}
			else {
				for( var i = 0; i < selected_id.length; i++ ) {
					if( selected_id[i] == str[1] ) break;
				}
				selected_id.splice( i, 1 );
				selected_name.splice( i, 1 );
			}
			var id = ''; var name = '';
			for( var i = 0; i < selected_id.length; i++ ) {
				id += selected_id[i] + ',';
				name += selected_name[i] + '*';
			}
			
			id = id.substr( 0, id.length - 1 );
			name = name.substr( 0, name.length - 1 );
			
			$('#hide_job_id').val( id );
			$('#hide_style_ref').val( name );
		}
	
    </script>

</head>

<body>
<div align="center">
	<form name="styleRef_form" id="styleRef_form">
		<fieldset style="width:580px;">
            <table width="570" cellspacing="0" cellpadding="0" border="1" rules="all" align="center" class="rpt_table" id="tbl_list">
            	<thead>
                    <th>Buyer</th>
                    <th>Search By</th>
                    <th id="search_by_td_up" width="170">Please Enter Style Ref.</th>
                    <th><input type="reset" name="button" class="formbutton" value="Reset"  style="width:100px;"></th> 
                    <input type="hidden" name="hide_style_ref" id="hide_style_ref" value="" />
                    <input type="hidden" name="hide_job_id" id="hide_job_id" value="" />
                </thead>
                <tbody>
                	<tr>
                        <td align="center">
                        	 <?php 
								echo create_drop_down( "cbo_buyer_name", 140, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company=$companyID $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name","id,buyer_name",1, "-- All Buyer--",0,"",0 );
							?>
                        </td>                 
                        <td align="center">	
                    	<?php
                       		$search_by_arr=array(1=>"Style Ref",2=>"Job No");
							$dd="change_search_event(this.value, '0*0', '0*0', '../../') ";							
							echo create_drop_down( "cbo_search_by", 130, $search_by_arr,"",0, "--Select--", "",$dd,0 );
						?>
                        </td>     
                        <td align="center" id="search_by_td">				
                            <input type="text" style="width:130px" class="text_boxes" name="txt_search_common" id="txt_search_common" />	
                        </td> 	
                        <td align="center">
                        	<input type="button" name="button" class="formbutton" value="Show" onClick="show_list_view ('<?php echo $companyID; ?>'+'**'+document.getElementById('cbo_buyer_name').value+'**'+document.getElementById('cbo_search_by').value+'**'+document.getElementById('txt_search_common').value, 'create_style_ref_search_list_view', 'search_div', 'planning_info_entry_controller', 'setFilterGrid(\'tbl_list_search\',-1)');" style="width:100px;" />
                    </td>
                    </tr>
            	</tbody>
           	</table>
            <div style="margin-top:15px" id="search_div"></div>
		</fieldset>
	</form>
</div>
</body>           
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
	exit(); 
}

if($action=="create_style_ref_search_list_view")
{
	$data=explode('**',$data);
	$company_id=$data[0];
	//if($data[1]==0) $buyer_name="%%"; else $buyer_name=$data[1];
	
	if($data[1]==0)
	{
		if ($_SESSION['logic_erp']["data_level_secured"]==1)
		{
			if($_SESSION['logic_erp']["buyer_id"]!="") $buyer_id_cond=" and buyer_name in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_id_cond="";
		}
		else
		{
			$buyer_id_cond="";
		}
	}
	else
	{
		$buyer_id_cond=" and buyer_name=$data[1]";//.str_replace("'","",$cbo_buyer_name)
	}
	
	$search_by=$data[2];
	$search_string="%".trim($data[3])."%";

	if($search_by==1) $search_field="style_ref_no"; else $search_field="job_no";
	
	if($db_type==0) $year_field="YEAR(insert_date) as year,"; 
	else if($db_type==2) $year_field="to_char(insert_date,'YYYY') as year,";
	else $year_field="";//defined Later

	$arr=array (0=>$company_arr,1=>$buyer_arr);
	$sql= "select id, $year_field job_no, job_no_prefix_num, company_name, buyer_name, style_ref_no from wo_po_details_master where status_active=1 and is_deleted=0 and company_name=$company_id and $search_field like '$search_string' $buyer_id_cond order by job_no";
		
	echo create_list_view("tbl_list_search", "Company,Buyer Name,Year,Job No,Style Ref. No", "120,120,60,70","600","240",0, $sql, "js_set_value", "id,style_ref_no", "", 1, "company_name,buyer_name,0,0,0", $arr , "company_name,buyer_name,year,job_no_prefix_num,style_ref_no", "",'','0,0,0,0,0','',1) ;
	
   exit(); 
} 

if($action=="order_no_search_popup")
{
	echo load_html_head_contents("Order No Info", "../../", 1, 1,'','','');
	extract($_REQUEST);
	?>
     
	<script>
		
		var selected_id = new Array; var selected_name = new Array;
		
		function check_all_data()
		{
			var tbl_row_count = document.getElementById( 'tbl_list_search' ).rows.length;
			tbl_row_count = tbl_row_count - 1;

			for( var i = 1; i <= tbl_row_count; i++ )
			{
				$('#tr_'+i).trigger('click'); 
			}
		}
		
		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
		function js_set_value( str ) {
			
			if (str!="") str=str.split("_");
			 
			toggle( document.getElementById( 'tr_' + str[0] ), '#FFFFCC' );
			 
			if( jQuery.inArray( str[1], selected_id ) == -1 ) {
				selected_id.push( str[1] );
				selected_name.push( str[2] );
				
			}
			else {
				for( var i = 0; i < selected_id.length; i++ ) {
					if( selected_id[i] == str[1] ) break;
				}
				selected_id.splice( i, 1 );
				selected_name.splice( i, 1 );
			}
			var id = ''; var name = '';
			for( var i = 0; i < selected_id.length; i++ ) {
				id += selected_id[i] + ',';
				name += selected_name[i] + '*';
			}
			
			id = id.substr( 0, id.length - 1 );
			name = name.substr( 0, name.length - 1 );
			
			$('#hide_order_id').val( id );
			$('#hide_order_no').val( name );
		}
	
    </script>

</head>

<body>
<div align="center">
	<form name="styleRef_form" id="styleRef_form">
		<fieldset style="width:780px;">
            <table width="770" cellspacing="0" cellpadding="0" border="1" rules="all" align="center" class="rpt_table" id="tbl_list">
            	<thead>
                    <th>Buyer</th>
                    <th>Search By</th>
                    <th id="search_by_td_up" width="170">Please Enter Order No</th>
                    <th>Shipment Date</th>
                    <th><input type="reset" name="button" class="formbutton" value="Reset"  style="width:100px;"></th> 
                    <input type="hidden" name="hide_order_no" id="hide_order_no" value="" />
                    <input type="hidden" name="hide_order_id" id="hide_order_id" value="" />
                </thead>
                <tbody>
                	<tr>
                        <td align="center">
                        	 <?php 
								echo create_drop_down( "cbo_buyer_name", 140, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company=$companyID $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name","id,buyer_name",1, "-- All Buyer--",0,"",0 );
							?>
                        </td>                 
                        <td align="center">	
                    	<?php
                       		$search_by_arr=array(1=>"Order No",2=>"Style Ref",3=>"Job No");
							$dd="change_search_event(this.value, '0*0*0', '0*0*0', '../../') ";							
							echo create_drop_down( "cbo_search_by", 110, $search_by_arr,"",0, "--Select--", "",$dd,0 );
						?>
                        </td>     
                        <td align="center" id="search_by_td">				
                            <input type="text" style="width:130px" class="text_boxes" name="txt_search_common" id="txt_search_common" />	
                        </td> 
                        <td align="center">
                            <input type="text" name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px" readonly>To
                            <input type="text" name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px" readonly>
                        </td>	
                        <td align="center">
                        	<input type="button" name="button" class="formbutton" value="Show" onClick="show_list_view ('<?php echo $companyID; ?>'+'**'+document.getElementById('cbo_buyer_name').value+'**'+document.getElementById('cbo_search_by').value+'**'+document.getElementById('txt_search_common').value+'**'+document.getElementById('txt_date_from').value+'**'+document.getElementById('txt_date_to').value, 'create_order_no_search_list_view', 'search_div', 'planning_info_entry_controller', 'setFilterGrid(\'tbl_list_search\',-1)');" style="width:100px;" />
                    	</td>
                    </tr>
                    <tr>
                        <td colspan="5" height="20" valign="middle"><?php echo load_month_buttons(1); ?></td>
                    </tr>
            	</tbody>
           	</table>
            <div style="margin-top:15px" id="search_div"></div>
		</fieldset>
	</form>
</div>
</body>           
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
	exit(); 
}

if($action=="create_order_no_search_list_view")
{
	$data=explode('**',$data);
	$company_id=$data[0];
	
	if($data[1]==0)
	{
		if ($_SESSION['logic_erp']["data_level_secured"]==1)
		{
			if($_SESSION['logic_erp']["buyer_id"]!="") $buyer_id_cond=" and a.buyer_name in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_id_cond="";
		}
		else
		{
			$buyer_id_cond="";
		}
	}
	else
	{
		$buyer_id_cond=" and a.buyer_name=$data[1]";
	}
	
	$search_by=$data[2];
	$search_string="%".trim($data[3])."%";

	if($search_by==1) 
		$search_field="b.po_number"; 
	else if($search_by==2) 
		$search_field="a.style_ref_no"; 	
	else 
		$search_field="a.job_no";
		
	$start_date =trim($data[4]);
	$end_date =trim($data[5]);	
	
	if($start_date!="" && $end_date!="")
	{
		if($db_type==0)
		{
			$date_cond="and b.pub_shipment_date between '".change_date_format($start_date,"yyyy-mm-dd", "-")."' and '".change_date_format($end_date,"yyyy-mm-dd", "-")."'";
		}
		else
		{
			$date_cond="and b.pub_shipment_date between '".change_date_format($start_date,'','',1)."' and '".change_date_format($end_date,'','',1)."'";	
		}
	}
	else
	{
		$date_cond="";
	}
	
	if($db_type==0) $year_field="YEAR(a.insert_date) as year,"; 
	else if($db_type==2) $year_field="to_char(a.insert_date,'YYYY') as year,";
	else $year_field="";//defined Later
	
	$arr=array(0=>$company_arr,1=>$buyer_arr);
		
	$sql= "select b.id, $year_field a.job_no, a.job_no_prefix_num, a.company_name, a.buyer_name, a.style_ref_no, b.po_number, b.pub_shipment_date from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.company_name=$company_id and $search_field like '$search_string' $buyer_id_cond $date_cond order by b.id, b.pub_shipment_date";
		
	echo create_list_view("tbl_list_search", "Company,Buyer Name,Year,Job No,Style Ref. No, Po No, Shipment Date", "100,120,50,60,130,110","760","220",0, $sql , "js_set_value", "id,po_number", "", 1, "company_name,buyer_name,0,0,0,0,0", $arr , "company_name,buyer_name,year,job_no_prefix_num,style_ref_no,po_number,pub_shipment_date", "",'','0,0,0,0,0,0,3','',1) ;
	
   exit(); 
}


if($action=="booking_item_details")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	$company_name=str_replace("'","",$cbo_company_name);
	$planning_status=str_replace("'","",$cbo_planning_status);
	//if(str_replace("'","",$cbo_buyer_name)==0) $buyer_name="%%"; else $buyer_name=str_replace("'","",$cbo_buyer_name);
	if(str_replace("'","",$cbo_buyer_name)==0)
	{
		if ($_SESSION['logic_erp']["data_level_secured"]==1)
		{
			if($_SESSION['logic_erp']["buyer_id"]!="") $buyer_id_cond=" and a.buyer_id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_id_cond="";
		}
		else
		{
			$buyer_id_cond="";
		}
	}
	else
	{
		$buyer_id_cond=" and a.buyer_id=$cbo_buyer_name";
	}
	
	if(str_replace("'","",$hide_job_id)=="")
	{
		$job_no_cond="";
	}
	else
	{
		$job_no_cond="and c.id in(".str_replace("'","",$hide_job_id).")";
	}
	
	if(str_replace("'","",trim($txt_order_no))=="")
	{
		$po_id_cond="";
	}
	else
	{
		if(str_replace("'","",$hide_order_id)!="")
		{
			$po_id=str_replace("'","",$hide_order_id);
		}
		else
		{
			$po_number=trim(str_replace("'","",$txt_order_no))."%";
			if($db_type==0)
			{
				$po_id=return_field_value("group_concat(id) as po_id","wo_po_break_down","po_number like '$po_number' and status_active=1 and is_deleted=0","po_id");
			}
			else
			{
				$po_id=return_field_value("LISTAGG(id, ',') WITHIN GROUP (ORDER BY id) as po_id","wo_po_break_down","po_number like '$po_number' and status_active=1 and is_deleted=0","po_id");	
			}
			if($po_id=="") $po_id=0;
		}
		$po_id_cond="and b.po_break_down_id in(".$po_id.")";
	}
	
	$po_array=return_library_array( "select id, po_number from wo_po_break_down",'id','po_number');
	
	$pre_cost_array=array();
	if($db_type==0)
	{
		$costing_sql=sql_select("select id, body_part_id, color_type_id, width_dia_type, gsm_weight, concat_ws(', ',construction,composition) as fab_desc, lib_yarn_count_deter_id from wo_pre_cost_fabric_cost_dtls");//previous name lib_yarn_count_determination_id
	}
	else
	{
		$costing_sql=sql_select("select id, body_part_id, color_type_id, width_dia_type, gsm_weight, construction || ',' || composition as fab_desc, lib_yarn_count_deter_id from wo_pre_cost_fabric_cost_dtls");	
	}
	foreach($costing_sql as $row)
	{
		$costing_per_id_library[$row[csf('id')]]['body_part']=$row[csf('body_part_id')];
		$costing_per_id_library[$row[csf('id')]]['color_type']=$row[csf('color_type_id')];
		$costing_per_id_library[$row[csf('id')]]['width_dia_type']=$row[csf('width_dia_type')];
		$costing_per_id_library[$row[csf('id')]]['gsm']=$row[csf('gsm_weight')]; 
		$costing_per_id_library[$row[csf('id')]]['desc']=$row[csf('fab_desc')]; 
		$costing_per_id_library[$row[csf('id')]]['determination_id']=$row[csf('lib_yarn_count_deter_id')]; 
	}
	
	$tna_array=array();
	$tna_sql=sql_select("select id, po_number_id, actual_start_date, actual_finish_date from tna_process_mst where task_category=25 and task_number=1 and is_deleted=0 and status_active=1");
	foreach($tna_sql as $row)
	{
		$tna_array[$row[csf('po_number_id')]]['start_d']=$row[csf('actual_start_date')];
		$tna_array[$row[csf('po_number_id')]]['finish_d']=$row[csf('actual_finish_date')];
	}
	
	$yarn_desc_array=array();
	$sql="select id, lot, yarn_comp_type1st, yarn_comp_percent1st, yarn_comp_type2nd, yarn_comp_percent2nd, yarn_count_id, yarn_type from product_details_master where item_category_id=1";
	$result = sql_select($sql);
	foreach($result as $row)
	{
		$compostion='';
		if($row[csf('yarn_comp_percent2nd')]!=0)
		{
			$compostion=$composition[$row[csf('yarn_comp_type1st')]]." ".$row[csf('yarn_comp_percent1st')]."%"." ".$composition[$row[csf('yarn_comp_type2nd')]]." ".$row[csf('yarn_comp_percent2nd')]."%";
		}
		else
		{
			$compostion=$composition[$row[csf('yarn_comp_type1st')]]." ".$row[csf('yarn_comp_percent1st')]."%"." ".$composition[$row[csf('yarn_comp_type2nd')]];
		}

		$yarn_desc=$row[csf('lot')]." ".$yarn_count_details[$row[csf('yarn_count_id')]]." ".$compostion." ".$yarn_type[$row[csf('yarn_type')]];
		$yarn_desc_array[$row[csf('id')]]=$yarn_desc;
	}
	
	$booking_item_array=array();
	if($db_type==0)
	{
		$booking_item_array=return_library_array( "select a.booking_no, group_concat(distinct(b.item_id)) as prod_id from inv_material_allocation_mst a,inv_material_allocation_dtls b where a.id=b.mst_id and b.item_category=1 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.booking_no",'booking_no','prod_id');
	}
	else
	{
		$booking_item_array=return_library_array( "select a.booking_no, LISTAGG(b.item_id, ',') WITHIN GROUP (ORDER BY b.item_id) as prod_id from inv_material_allocation_mst a,inv_material_allocation_dtls b where a.id=b.mst_id and b.item_category=1 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.booking_no",'booking_no','prod_id');
	}
	
	$program_data_array=array();
	if($db_type==0)
	{
		$sql_plan="select mst_id, booking_no, po_id, body_part_id, fabric_desc, gsm_weight, dia, group_concat(distinct(dtls_id)) as prog_no, sum(program_qnty) as program_qnty, min(id) as id from ppl_planning_entry_plan_dtls where status_active=1 and is_deleted=0 group by mst_id, booking_no, po_id, body_part_id, fabric_desc, gsm_weight, dia";
	}
	else
	{
		$sql_plan="select mst_id, booking_no, po_id, body_part_id, fabric_desc, gsm_weight, dia, LISTAGG(dtls_id, ',') WITHIN GROUP (ORDER BY dtls_id) as prog_no, sum(program_qnty) as program_qnty, min(id) as id from ppl_planning_entry_plan_dtls where status_active=1 and is_deleted=0 group by mst_id, booking_no, po_id, body_part_id, fabric_desc, gsm_weight, dia";
	}
	
	$res_plan=sql_select($sql_plan);
	foreach($res_plan as $rowPlan)
	{
		$program_data_array[$rowPlan[csf('booking_no')]][$rowPlan[csf('po_id')]][$rowPlan[csf('body_part_id')]][$rowPlan[csf('fabric_desc')]][$rowPlan[csf('gsm_weight')]][$rowPlan[csf('dia')]]['mst_id']=$rowPlan[csf('mst_id')];
		$program_data_array[$rowPlan[csf('booking_no')]][$rowPlan[csf('po_id')]][$rowPlan[csf('body_part_id')]][$rowPlan[csf('fabric_desc')]][$rowPlan[csf('gsm_weight')]][$rowPlan[csf('dia')]]['prog_no']=$rowPlan[csf('prog_no')];
		$program_data_array[$rowPlan[csf('booking_no')]][$rowPlan[csf('po_id')]][$rowPlan[csf('body_part_id')]][$rowPlan[csf('fabric_desc')]][$rowPlan[csf('gsm_weight')]][$rowPlan[csf('dia')]]['program_qnty']=$rowPlan[csf('program_qnty')];
		$program_data_array[$rowPlan[csf('booking_no')]][$rowPlan[csf('po_id')]][$rowPlan[csf('body_part_id')]][$rowPlan[csf('fabric_desc')]][$rowPlan[csf('gsm_weight')]][$rowPlan[csf('dia')]]['id']=$rowPlan[csf('id')];
	}
	
	if($db_type==0)
	{
		$sql="select a.id, a.company_id, a.item_category, a.fabric_source, a.booking_type, a.is_short, a.booking_no, a.booking_date, a.job_no, a.buyer_id, a.is_approved, b.construction, b.copmposition, b.pre_cost_fabric_cost_dtls_id, b.po_break_down_id, b.gsm_weight, b.dia_width, sum(b.grey_fab_qnty) as qnty, c.style_ref_no from wo_booking_mst a, wo_booking_dtls b, wo_po_details_master c where a.booking_no=b.booking_no and b.job_no=c.job_no and a.company_id=$company_name and a.item_category=2 and a.fabric_source=1 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and b.grey_fab_qnty>0 $job_no_cond $buyer_id_cond $po_id_cond group by a.booking_no, b.po_break_down_id, b.pre_cost_fabric_cost_dtls_id, b.dia_width order by cast(b.dia_width as unsigned),a.booking_no";// and a.buyer_id like '$buyer_name'
	}
	else
	{
		$sql="select a.id, a.company_id, a.item_category, a.fabric_source, a.booking_type, a.is_short, a.booking_no, a.booking_date, a.job_no, a.buyer_id, a.is_approved, b.construction, b.copmposition, b.pre_cost_fabric_cost_dtls_id, b.po_break_down_id, b.gsm_weight, b.dia_width, sum(b.grey_fab_qnty) as qnty, c.style_ref_no from wo_booking_mst a, wo_booking_dtls b, wo_po_details_master c where a.booking_no=b.booking_no and b.job_no=c.job_no and a.company_id=$company_name and a.item_category=2 and a.fabric_source=1 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and b.grey_fab_qnty>0 $job_no_cond $buyer_id_cond $po_id_cond group by a.id, a.company_id, a.fabric_source, a.booking_type, a.is_short, a.booking_no, a.booking_date, a.job_no, a.buyer_id, a.is_approved, a.item_category, b.construction, b.copmposition, b.pre_cost_fabric_cost_dtls_id, b.po_break_down_id, b.gsm_weight, b.dia_width, c.style_ref_no order by b.dia_width,a.booking_no";	
	}
	//echo $sql;
	
	?>
    <form name="palnningEntry_2" id="palnningEntry_2">
        <fieldset style="width:2000px;">
        <legend>Fabric Description Details</legend>	
            <table cellspacing="0" cellpadding="0" border="1" rules="all" width="1980" class="rpt_table" >
                <thead>
                    <th width="40">SL</th>
                    <th width="70">Plan Id</th>
                    <th width="70">Prog. No</th>
                    <th width="115">Booking No</th>
                    <th width="80">Booking Date</th>
                    <th width="80">Start Date</th>
                    <th width="80">T.O.D</th>
                    <th width="70">Buyer</th>
                    <th width="110">Order No</th>
                    <th width="70"><?php echo $company_arr[$company_name]; ?></th>
                    <th width="120">Style</th>
                    <th width="110">Body Part</th>
                    <th width="100">Color Type</th>
                    <th width="130">Fabric Desc.</th>
                    <th width="70">Fabric Gsm</th>
                    <th width="80">Fabric Dia</th>
                    <th width="80">Width/Dia Type</th>
                    <th width="90">Booking Qnty</th>
                    <th width="110">Prog. Qnty</th>
                    <th width="90">Balance Prog. Qnty</th>
                    <th>Desc.Of Yarn</th>
                </thead>
            </table>
            <div style="width:1997px; overflow-y:scroll; max-height:330px;" id="buyer_list_view" align="center">
                <table cellspacing="0" cellpadding="0" border="1" rules="all" width="1980" class="rpt_table" id="tbl_list_search">
                    <tbody>
                        <?php 
                            $i=1; $k=1; $z=1; $dia_array=array();
                            $nameArray=sql_select( $sql );
                            foreach ($nameArray as $row)
                            {
							    $plan_id='';
								$gsm=$costing_per_id_library[$row[csf('pre_cost_fabric_cost_dtls_id')]]['gsm'];
								$dia=$row[csf('dia_width')]; 
								$desc=$costing_per_id_library[$row[csf('pre_cost_fabric_cost_dtls_id')]]['desc'];
								$determination_id=$costing_per_id_library[$row[csf('pre_cost_fabric_cost_dtls_id')]]['determination_id'];
								
								/*if($db_type==0)
								{
									$sql_plan="select mst_id, group_concat(distinct(dtls_id)) as prog_no, sum(program_qnty) as program_qnty, min(id) as id from ppl_planning_entry_plan_dtls where booking_no='".$row[csf('booking_no')]."' and po_id='".$row[csf('po_break_down_id')]."' and body_part_id ='".$costing_per_id_library[$row[csf('pre_cost_fabric_cost_dtls_id')]]['body_part']."' and fabric_desc ='".$desc."' and gsm_weight ='".$gsm."' and dia ='".$row[csf('dia_width')]."' and status_active=1 and is_deleted=0 group by mst_id";
								}
								else
								{
									$sql_plan="select mst_id, LISTAGG(dtls_id, ',') WITHIN GROUP (ORDER BY dtls_id) as prog_no, sum(program_qnty) as program_qnty, min(id) as id from ppl_planning_entry_plan_dtls where booking_no='".$row[csf('booking_no')]."' and po_id='".$row[csf('po_break_down_id')]."' and body_part_id ='".$costing_per_id_library[$row[csf('pre_cost_fabric_cost_dtls_id')]]['body_part']."' and fabric_desc ='".$desc."' and gsm_weight ='".$gsm."' and dia ='".$row[csf('dia_width')]."' and status_active=1 and is_deleted=0 group by mst_id";
								}
								$result_plan=sql_select($sql_plan); 
								
								$update_id=$result_plan[0][csf('id')];
								$program_qnty=$result_plan[0][csf('program_qnty')];
								$plan_id=$result_plan[0][csf('mst_id')];
								$prog_no=$result_plan[0][csf('prog_no')];*/
								
								$update_id=$program_data_array[$row[csf('booking_no')]][$row[csf('po_break_down_id')]][$costing_per_id_library[$row[csf('pre_cost_fabric_cost_dtls_id')]]['body_part']][$desc][$gsm][$row[csf('dia_width')]]['id'];
								$program_qnty=$program_data_array[$row[csf('booking_no')]][$row[csf('po_break_down_id')]][$costing_per_id_library[$row[csf('pre_cost_fabric_cost_dtls_id')]]['body_part']][$desc][$gsm][$row[csf('dia_width')]]['program_qnty'];
								$plan_id=$program_data_array[$row[csf('booking_no')]][$row[csf('po_break_down_id')]][$costing_per_id_library[$row[csf('pre_cost_fabric_cost_dtls_id')]]['body_part']][$desc][$gsm][$row[csf('dia_width')]]['mst_id'];
								$prog_no=$program_data_array[$row[csf('booking_no')]][$row[csf('po_break_down_id')]][$costing_per_id_library[$row[csf('pre_cost_fabric_cost_dtls_id')]]['body_part']][$desc][$gsm][$row[csf('dia_width')]]['prog_no'];
								
								$prog_no=implode(",",array_unique(explode(",",$prog_no)));
								$balance_qnty=$row[csf('qnty')]-$program_qnty;
								
								$start_date=$tna_array[$row[csf('po_break_down_id')]]['start_d'];//date("Y-m-d");
								$end_date=$tna_array[$row[csf('po_break_down_id')]]['finish_d'];//date("Y-m-d");
								
								if(($planning_status==2 && $balance_qnty<=0) || ($planning_status==1 && $balance_qnty>0))
								{
									if ($z%2==0)  
										$bgcolor="#E9F3FF";
									else
										$bgcolor="#FFFFFF";
								
									if(!in_array($row[csf('dia_width')], $dia_array))
									{
										if ($k!=1)
										{
										?>
											<tr bgcolor="#CCCCCC" id="tr_<?php echo $i; ?>">
												<td colspan="17" align="right"><b>Sub Total</b></td>
												<td align="right"><b><?php echo number_format($total_dia_qnty,2,'.',''); ?></b></td>
												<td align="right"><b><?php echo number_format($total_program_qnty,2,'.',''); ?></b></td>
												<td align="right"><b><?php echo number_format($total_balance,2,'.',''); ?></b></td>
												<td>&nbsp;<input type="hidden" name="check[]" id="check_<?php echo $i; ?>" value="0" /></td>
											</tr>
										<?php
											$total_dia_qnty = 0;
											$total_program_qnty = 0;
											$total_balance = 0;
											$i++;
										}
										
									?>
										<tr bgcolor="#EFEFEF" id="tr_<?php echo $i; ?>">
											<td colspan="21">
                                                <b>Dia/Width:- <?php echo $row[csf('dia_width')]; ?></b>
                                                <input type="hidden" name="check[]" id="check_<?php echo $i; ?>" value="0" />
                                            </td>
										</tr>
									<?php
										$dia_array[]=$row[csf('dia_width')];
										$k++;
										$i++;
									}
									
									/*if($plan_id=="")
									{
										$reqsn_found_or_not=0;	
									}
									else
									{
										$reqsn_id=return_field_value("a.id as reqsn_id","ppl_planning_info_entry_dtls a,ppl_yarn_requisition_entry b","a.id=b.knit_id and a.mst_id='".$plan_id."' and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0","reqsn_id");
										
										if($reqsn_id=="") $reqsn_found_or_not=0; else $reqsn_found_or_not=1; 
									}*/
									
									$reqsn_found_or_not=0; $yarn_desc='';
									/*if($db_type==0)
									{
										$prod_id=return_field_value("group_concat(distinct(b.item_id)) as prod_id","inv_material_allocation_mst a,inv_material_allocation_dtls b","a.id=b.mst_id and a.booking_no='".$row[csf('booking_no')]."' and b.item_category=1 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.booking_no","prod_id");
									}
									else
									{
										$prod_id=return_field_value("LISTAGG(b.item_id, ',') WITHIN GROUP (ORDER BY b.item_id) as prod_id","inv_material_allocation_mst a,inv_material_allocation_dtls b","a.id=b.mst_id and a.booking_no='".$row[csf('booking_no')]."' and b.item_category=1 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.booking_no","prod_id");
									}*/
									$prod_id=$booking_item_array[$row[csf('booking_no')]];
									
									$prod_id=array_unique(explode(",",$prod_id));
									foreach($prod_id as $value)
									{
										if($yarn_desc=='') $yarn_desc=$yarn_desc_array[$value]; else $yarn_desc.=",<br>".$yarn_desc_array[$value];
									}
									
									if($row[csf('booking_type')]==4) $booking_type=3; else $booking_type=$row[csf('is_short')];

									?>
									<tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none;cursor:pointer" onClick="selected_row('<?php echo $i; ?>')" id="tr_<?php echo $i; ?>"> 
										<td width="40"><?php echo $z; ?></td>
                                        <td width="70" id="plan_id_<?php echo $i; ?>"><p><?php echo $plan_id; ?>&nbsp;</p></td>
                                        <td width="70" id="prog_no_<?php echo $i; ?>"><p><?php echo $prog_no; ?>&nbsp;</p></td>
										<td width="115" id="booking_no_<?php echo $i; ?>"><p><?php echo "<a href='##' style='color:#000' onclick=\"generate_worder_report('$booking_type','".$row[csf('booking_no')]."', '".$row[csf('company_id')]."', '".$row[csf('po_break_down_id')]."', '".$row[csf('item_category')]."', '".$row[csf('fabric_source')]."', '".$row[csf('job_no')]."','".$row[csf('is_approved')]."')\">".$row[csf('booking_no')]."</a>"; ?></p></td>
										<td width="80" align="center"><?php echo change_date_format($row[csf('booking_date')]); ?></td>
										<td width="80" align="center" id="start_date_<?php echo $i; ?>"><?php if($start_date!="") echo change_date_format($start_date); ?></td>
										<td width="80" align="center" id="end_date_<?php echo $i; ?>"><?php if($end_date!="") echo change_date_format($end_date); ?></td>
										<td width="70"><p><?php echo $buyer_arr[$row[csf('buyer_id')]]; ?></p></td>
										<td width="110"><p><?php echo $po_array[$row[csf('po_break_down_id')]]; ?></p></td>
										<td width="70" id="po_id_<?php echo $i; ?>"><?php echo $row[csf('po_break_down_id')]; ?></td>
										<td width="120"><p><?php echo $row[csf('style_ref_no')]; ?></p></td>
										<td width="110"><p><?php echo $body_part[$costing_per_id_library[$row[csf('pre_cost_fabric_cost_dtls_id')]]['body_part']]; ?></p></td>
                                        <td width="100"><p><?php echo $color_type[$costing_per_id_library[$row[csf('pre_cost_fabric_cost_dtls_id')]]['color_type']]; ?></p></td>
										<td width="130" id="desc_<?php echo $i; ?>"><p><?php echo $desc; ?></p></td>
										<td width="70" id="gsm_weight_<?php echo $i; ?>"><p><?php echo $gsm; ?></p></td>
										<td width="80" id="dia_width_<?php echo $i; ?>"><p><?php echo $row[csf('dia_width')]; ?></p></td>
										<td width="80"><?php echo $fabric_typee[$costing_per_id_library[$row[csf('pre_cost_fabric_cost_dtls_id')]]['width_dia_type']]; ?></td>
										<td align="right" id="booking_qnty_<?php echo $i; ?>" width="90"><?php echo number_format($row[csf('qnty')],2,'.',''); ?></td>
										<td align="right" width="110"><?php if($program_qnty>0) echo number_format($program_qnty,2,'.',''); ?></td>
										<td align="right" width="90"><?php echo number_format($balance_qnty,2,'.',''); ?></td>
										<td id="yarn_desc_<?php echo $i; ?>"><p><?php echo $yarn_desc; ?>&nbsp;</p></td>
										
										<input type="hidden" name="buyer_id[]" id="buyer_id_<?php echo $i; ?>" value="<?php echo $row[csf('buyer_id')]; ?>" />
										<input type="hidden" name="body_part_id[]" id="body_part_id_<?php echo $i; ?>" value="<?php echo $costing_per_id_library[$row[csf('pre_cost_fabric_cost_dtls_id')]]['body_part']; ?>" />
                                        <input type="hidden" name="color_type_id[]" id="color_type_id_<?php echo $i; ?>" value="<?php echo $costing_per_id_library[$row[csf('pre_cost_fabric_cost_dtls_id')]]['color_type']; ?>" />
                                        <input type="hidden" name="determination_id[]" id="determination_id_<?php echo $i; ?>" value="<?php echo $determination_id; ?>" />
                                        <?php
											if($costing_per_id_library[$row[csf('pre_cost_fabric_cost_dtls_id')]]['width_dia_type']=="")
												$width_dia_type=0;
											else
												$width_dia_type=$costing_per_id_library[$row[csf('pre_cost_fabric_cost_dtls_id')]]['width_dia_type'];
										?>
                                        <input type="hidden" name="fabric_typee[]" id="fabric_typee_<?php echo $i; ?>" value="<?php echo $width_dia_type; ?>" />
										<input type="hidden" name="pre_cost_id[]" id="pre_cost_id_<?php echo $i; ?>" value="<?php echo $row[csf('pre_cost_fabric_cost_dtls_id')]; ?>" />
										<input type="hidden" name="updateId[]" id="updateId_<?php echo $i; ?>" value="<?php echo $update_id; ?>" /><!-- Not Used -->
                                        <input type="hidden" name="reqsn_found_or_not[]" id="reqsn_found_or_not_<?php echo $i; ?>" value="<?php echo $reqsn_found_or_not; ?>" />
                                        <input type="hidden" name="check[]" id="check_<?php echo $i; ?>" value="1" />
									</tr>
									<?php
									
									$total_dia_qnty+=$row[csf('qnty')];
									$total_program_qnty+=$program_qnty;
									$total_balance+=$balance_qnty;
									
									$total_qnty+=$row[csf('qnty')];
									$grand_total_program_qnty+=$program_qnty;
									$grand_total_balance+=$balance_qnty;
									
									$i++;
									$z++;
								}
                            }
                            
                            if($i>1)
                            {
                            ?>
                                <tr bgcolor="#CCCCCC" id="tr_<?php echo $i; ?>">
                                    <td colspan="17" align="right"><b>Sub Total</b></td>
                                    <td align="right"><b><?php echo number_format($total_dia_qnty,2,'.',''); ?></b></td>
                                    <td align="right"><b><?php echo number_format($total_program_qnty,2,'.',''); ?></b></td>
                                    <td align="right"><b><?php echo number_format($total_balance,2,'.',''); ?></b></td>
                                    <td>&nbsp;<input type="hidden" name="check[]" id="check_<?php echo $i; ?>" value="0" /></td>
                                </tr>
                            <?php
                            }
                        ?>
                    </tbody>
                    <tfoot>
                        <th colspan="17" align="right">Grand Total</th>
                        <th align="right"><?php echo number_format($total_qnty,2,'.',''); ?></th>
                        <th align="right"><?php echo number_format($grand_total_program_qnty,2,'.',''); ?></th>
                        <th align="right"><?php echo number_format($grand_total_balance,2,'.',''); ?></th>
                        <th><input type="hidden" name="company_id" id="company_id" value="<?php echo $company_name; ?>" /></th>
                    </tfoot>
                </table>
            </div>
        </fieldset>
    </form>         
<?php
	exit();	
}

if($action=="prog_qnty_popup")
{
	echo load_html_head_contents("Program Qnty Info", "../../", 1, 1,'','','');
	extract($_REQUEST);
	$current_date=date("d-m-Y");
	
	if($start_date!="") echo $start_date=change_date_format($start_date);
	if($end_date!="") echo $end_date=change_date_format($end_date);
	
	?>
     
	<script>
		var permission='<?php echo $permission; ?>';
		
		function openpage_machine()
		{
			/*if(form_validation('txt_machine_dia','Machine Dia')==false )
			{
				return;
			}*/
			
			var save_string=$('#save_data').val();
			var txt_machine_dia=$('#txt_machine_dia').val();
			var update_dtls_id=$('#update_dtls_id').val();
			
			var page_link='planning_info_entry_controller.php?action=machine_info_popup&save_string='+save_string+'&companyID='+<?php echo $companyID; ?>+'&txt_machine_dia='+txt_machine_dia+'&update_dtls_id='+update_dtls_id;
			var title='Machine Info';
			
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=840px,height=300px,center=1,resize=1,scrolling=0','../');
			emailwindow.onclose=function()
			{
				var theform=this.contentDoc.forms[0];
				var hidden_machine_no=this.contentDoc.getElementById("hidden_machine_no").value;
				var hidden_machine_id=this.contentDoc.getElementById("hidden_machine_id").value;
				var save_string=this.contentDoc.getElementById("save_string").value;
				var hidden_machine_capacity=this.contentDoc.getElementById("hidden_machine_capacity").value;
				var hidden_distribute_qnty=this.contentDoc.getElementById("hidden_distribute_qnty").value;
				var hidden_min_date=this.contentDoc.getElementById("hidden_min_date").value;
				var hidden_max_date=this.contentDoc.getElementById("hidden_max_date").value;
				
				$('#txt_machine_no').val(hidden_machine_no);
				$('#machine_id').val(hidden_machine_id);	
				$('#save_data').val(save_string);
				$('#txt_machine_capacity').val(hidden_machine_capacity);
				$('#txt_distribution_qnty').val(hidden_distribute_qnty);
				$('#txt_start_date').val(hidden_min_date);
				$('#txt_end_date').val(hidden_max_date);
				
				//var days_req=hidden_distribute_qnty*1/hidden_machine_capacity*1;
				//$('#txt_days_req').val(days_req.toFixed(2));
				days_req();
			}
		}
		
		function days_req()
		{
			txt_start_date=$('#txt_start_date').val();
			txt_end_date=$('#txt_end_date').val();
			
			if(txt_start_date!="" && txt_end_date!="")
			{
				var days_req=date_diff('d',txt_start_date,txt_end_date);
				$('#txt_days_req').val(days_req+1);
			}
			else
			{
				$('#txt_days_req').val('');
			}
		}
		
		function openpage_color()
		{
			var hidden_color_id=$('#hidden_color_id').val();
			var page_link='planning_info_entry_controller.php?action=color_info_popup&companyID='+<?php echo $companyID; ?>+'&po_id='+'<?php echo $po_id; ?>'+'&pre_cost_id='+'<?php echo $pre_cost_id; ?>'+'&booking_no='+'<?php echo $booking_no; ?>'+'&dia='+'<?php echo $dia; ?>'+'&hidden_color_id='+hidden_color_id;
			var title='Color Info';
			
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=430px,height=300px,center=1,resize=1,scrolling=0','../');
			emailwindow.onclose=function()
			{
				var theform=this.contentDoc.forms[0];
				var hidden_color_no=this.contentDoc.getElementById("txt_selected").value;
				var hidden_color_id=this.contentDoc.getElementById("txt_selected_id").value;
				
				$('#txt_color').val(hidden_color_no);
				$('#hidden_color_id').val(hidden_color_id);
			}
		}
		
		function fnc_program_entry(operation)
		{
			//cbo_knitting_source*cbo_knitting_party*Knitting Source*Knitting Party*	
			if(form_validation('txt_machine_dia*txt_machine_gg*txt_program_qnty','Machine Dia*Machine GG*Program Qnty')==false )
			{
				return;
			}
			
			var data="action=save_update_delete&operation="+operation+get_submitted_data_string('cbo_knitting_source*cbo_knitting_party*txt_color*txt_machine_dia*txt_machine_gg*txt_program_qnty*txt_stitch_length*txt_spandex_stitch_length*txt_draft_ratio*machine_id*txt_machine_capacity*txt_distribution_qnty*cbo_knitting_status*txt_start_date*txt_end_date*txt_program_date*cbo_feeder*txt_remarks*save_data*updateId*update_dtls_id*cbo_color_range*cbo_dia_width_type*hidden_color_id*txt_fabric_dia*hidden_no_of_feeder_data',"../../")+'&companyID='+<?php echo $companyID; ?>+'&gsm='+'<?php echo $gsm; ?>'+'&dia='+'<?php echo $dia; ?>'+'&desc='+'<?php echo $desc; ?>'+'&start_date='+'<?php echo $start_date; ?>'+'&end_date='+'<?php echo $end_date; ?>'+'&determination_id='+<?php echo $determination_id; ?>+'&booking_no='+'<?php echo $booking_no; ?>'+'&data='+<?php echo $data; ?>+'&body_part_id='+<?php echo $body_part_id; ?>+'&color_type_id='+<?php echo $color_type_id; ?>+'&fabric_typee='+<?php echo $fabric_type; ?>+'&tot_booking_qnty='+<?php echo $booking_qnty; ?>+'&buyer_id='+<?php echo $buyer_id; ?>;
			
			freeze_window(operation);
			
			http.open("POST","planning_info_entry_controller.php",true);
			http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			http.send(data);
			http.onreadystatechange=fnc_program_entry_Reply_info;
		}
	
	function fnc_program_entry_Reply_info()
	{
		
		if(http.readyState == 4) 
		{
			//release_freezing();return;//alert(http.responseText);
			var reponse=trim(http.responseText).split('**');	
				
			show_msg(reponse[0]);
			
			if((reponse[0]==0 || reponse[0]==1 || reponse[0]==2))
			{
				reset_form('programQnty_1','','','txt_start_date,<?php echo $start_date; ?>*txt_end_date,<?php echo $end_date;?>*txt_program_date,<?php echo $current_date;?>','','');
				$('#updateId').val(reponse[1]);
				show_list_view(reponse[1], 'planning_info_details', 'list_view', 'planning_info_entry_controller', '' ) ;
				set_button_status(0, permission, 'fnc_program_entry',1);	
			}
			release_freezing();	
		}
	}
	
	function active_inactive()
	{
		var knitting_source=document.getElementById('cbo_knitting_source').value;
		
		reset_form('','','txt_machine_no*machine_id*txt_machine_capacity*txt_distribution_qnty*txt_days_req','txt_start_date,<?php echo $start_date; ?>*txt_end_date,<?php echo $end_date; ?>*txt_program_date,<?php echo $current_date; ?>','','');
		
		if(knitting_source==1)
		{
			document.getElementById('txt_machine_no').disabled=false;
		}
		else
		{
			document.getElementById('txt_machine_no').disabled=true;
		}
	}
	
	function openpage_feeder()
	{
		var no_of_feeder_data=$('#hidden_no_of_feeder_data').val();
		var color_type_id=<?php echo $color_type_id; ?>;
		
		if(!(color_type_id==2 || color_type_id==3 || color_type_id==4))
		{
			alert("Only for Stripe");
			return;
		}
		
		var page_link='planning_info_entry_controller.php?action=feeder_info_popup&no_of_feeder_data='+no_of_feeder_data+'&pre_cost_id='+'<?php echo $pre_cost_id; ?>';
		var title='Stripe Measurement Info';
		
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=640px,height=300px,center=1,resize=1,scrolling=0','../');
		emailwindow.onclose=function()
		{
			var theform=this.contentDoc.forms[0];
			var hidden_no_of_feeder_data=this.contentDoc.getElementById("hidden_no_of_feeder_data").value;
			
			$('#hidden_no_of_feeder_data').val(hidden_no_of_feeder_data);
		}
	}

    </script>

</head>

<body>
<div align="center">
	<?php echo load_freeze_divs ("../../",$permission,1); ?>
	<form name="programQnty_1" id="programQnty_1">
		<fieldset style="width:900px;">
        	<table border="1" cellpadding="0" cellspacing="0" class="rpt_table" rules="all" width="890" align="center">
                <thead>
                    <th width="160">Fabric Description</th>
                    <th width="60">GSM</th>
                    <th width="60">Dia</th>
                    <th width="90">Booking Qnty</th>
                    <th width="80">Start date</th>
                    <th width="80">Plan finish date</th>
                    <th>Description Of Yarn</th>
                </thead>
                <tr bgcolor="#FFFFFF">
                    <td><p><?php echo $desc; ?></p></td>
                    <td><?php echo $gsm; ?>&nbsp;</td>
                    <td><?php echo $dia; ?>&nbsp;</td>
                    <td align="right"><?php echo number_format($booking_qnty,2); ?></td>
                    <td align="center"><?php echo $start_date; ?>&nbsp;</td>
                    <td align="center"><?php echo $end_date; ?>&nbsp;</td>
                    <td><p><?php echo $desc_of_yarn; ?>&nbsp;</p></td>
                </tr>
            </table>
        </fieldset> 
        <fieldset style="width:900px; margin-top:10px">
            <legend>New Entry</legend>
            <table width="900" align="center" border="0">
                <tr>
                    <td>Knitting Source</td>
                    <td>
						<?php
							echo create_drop_down("cbo_knitting_source",152,$knitting_source,"", 1, "-- Select --", 0,"active_inactive();load_drop_down( 'planning_info_entry_controller', this.value+'**'+$companyID, 'load_drop_down_knitting_party','knitting_party');",0,'1,3');
                        ?>
                    </td>
                    <td>Knitting Party</td>
                    <td id="knitting_party">
						<?php
                        	echo create_drop_down( "cbo_knitting_party", 177, $blank_array,"",1, "--Select Knit Party--", 1, "" );
                        ?>
                    </td>
                    <td>Color</td>
                    <td>
                    	<input type="text" name="txt_color" id="txt_color" class="text_boxes" style="width:140px;" placeholder="Browse" onClick="openpage_color();" readonly/>
                        <input type="hidden" name="hidden_color_id" id="hidden_color_id" readonly/>
                    </td>
                </tr>
                <tr>
                	<td>Color Range</td>                                              
                    <td> 
                        <?php
                        	echo create_drop_down( "cbo_color_range", 152, $color_range,"",1, "-- Select --", 0, "" );
                        ?>
                    </td>
                    <td class="must_entry_caption">Machine Dia</td>
                    <td>
                        <input type="text" name="txt_machine_dia" id="txt_machine_dia" class="text_boxes_numeric" style="width:60px;" maxlength="3" title="Maximum 3 Character"/>
                         <?php
                        	echo create_drop_down( "cbo_dia_width_type", 100, $fabric_typee,"",1, "-- Select --", $fabric_type, "" );
                        ?>
                    </td>
                    <td class="must_entry_caption">Machine GG</td>
                    <td>
                        <input type="text" name="txt_machine_gg" id="txt_machine_gg" class="text_boxes" style="width:140px;"/>
                    </td>
                </tr>
                <tr>
                	<td>Finish Fabric Dia</td>
                    <td>
                        <input type="text" name="txt_fabric_dia" id="txt_fabric_dia" class="text_boxes" style="width:140px;"/>
                    </td>
                	<td class="must_entry_caption">Program Qnty</td>
                    <td>
                        <input type="text" name="txt_program_qnty" id="txt_program_qnty" class="text_boxes_numeric" style="width:165px;"/>
                    </td>
                    <td>Program Date</td>                                              
                   	<td> 
                        <input type="text" name="txt_program_date" id="txt_program_date" class="datepicker" style="width:140px" value="<?php echo $current_date; ?>" readonly>
                	</td>
                </tr>
                <tr>
                	<td>Stitch Length</td>
                    <td>
                        <input type="text" name="txt_stitch_length" id="txt_stitch_length" class="text_boxes" style="width:140px;"/>
                    </td>
                    <td>Spandex Stitch Length</td>
                    <td>
                        <input type="text" name="txt_spandex_stitch_length" id="txt_spandex_stitch_length" class="text_boxes" style="width:165px;"/>
                    </td>
                    <td>Draft Ratio</td>
                    <td>
                        <input type="text" name="txt_draft_ratio" id="txt_draft_ratio" class="text_boxes_numeric" style="width:140px;"/>
                    </td>
                </tr>
                <tr>
                    <td>Machine No</td>
                    <td>
                        <input type="text" name="txt_machine_no" id="txt_machine_no" class="text_boxes" placeholder="Double Click For Search" style="width:140px;" onDblClick="openpage_machine();" disabled="disabled" readonly/>
                        <input type="hidden" name="machine_id" id="machine_id" class="text_boxes" readonly/>
                    </td>
                    <td>Machine Capacity</td>
                    <td>
                        <input type="text" name="txt_machine_capacity" id="txt_machine_capacity" placeholder="Display" class="text_boxes_numeric" style="width:165px;" disabled="disabled"/>
                    </td>
                    <td>Distribution Qnty</td>
                    <td>
                        <input type="text" name="txt_distribution_qnty" id="txt_distribution_qnty" placeholder="Display" class="text_boxes_numeric" style="width:65px;" disabled="disabled"/>
                        <input type="text" name="txt_days_req" id="txt_days_req" placeholder="Days Req." class="text_boxes_numeric" style="width:60px;" disabled="disabled"/>
                    </td>
                </tr>
                <tr>
                	<td>Start Date</td>                                              
                   	<td> 
                        <input type="text" name="txt_start_date" id="txt_start_date" class="datepicker" style="width:140px" value="<?php echo $start_date; ?>" readonly >
                	</td>
                    <td>End Date</td>                                              
                   	<td> 
                        <input type="text" name="txt_end_date" id="txt_end_date" class="datepicker" style="width:165px" value="<?php echo $end_date; ?>" readonly>
                	</td>
                    <td>Status</td>
                    <td>
                        <?php
                        	echo create_drop_down( "cbo_knitting_status", 152, $knitting_program_status,"",1, "--Select Status--", 0, "" );
                        ?>
                    </td>
                </tr>
                 <tr>
                    <td>Feeder</td>                                              
                    <td> 
                        <?php
							$feeder=array(1=>"Full Feeder",2=>"Half Feeder");
                        	echo create_drop_down( "cbo_feeder", 152, $feeder,"",1, "--Select Feeder--", 0, "" );
                        ?>
                    </td>
                    <td colspan="2">
                    	<input type="button" name="feeder" class="formbuttonplasminus" value="No Of Feeder" onClick="openpage_feeder();" style="width:100px" />
                        <input type="hidden" name="hidden_no_of_feeder_data" id="hidden_no_of_feeder_data" class="text_boxes">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Program No.</b>
                        <input type="text" name="txt_program_no" id="txt_program_no" class="text_boxes" placeholder="Display" disabled style="width:90px">
                    </td>
                    <td>Remarks</td>                                              
                    <td> 
                        <input type="text" name="txt_remarks" id="txt_remarks" class="text_boxes" style="width:140px">
                    </td>
                </tr>
                <tr>
                    <td colspan="4" align="right" class="button_container">
						<?php 
							echo load_submit_buttons($permission, "fnc_program_entry", 0,0,"reset_form('programQnty_1','','','txt_start_date,$start_date*txt_end_date,$end_date*txt_program_date,$current_date','','updateId*txt_color');",1);
                        ?>
                     </td>
                     <td colspan="2" align="left" valign="top" class="button_container">   
                        <input type="button" name="close" class="formbutton" value="Close" id="main_close" onClick="parent.emailwindow.hide();" style="width:100px;"/>
                        <input type="hidden" name="save_data" id="save_data" class="text_boxes">
                        <input type="hidden" name="updateId" id="updateId" class="text_boxes" value="<?php echo str_replace("'",'',$plan_id); ?>">
                        <input type="hidden" name="update_dtls_id" id="update_dtls_id" class="text_boxes">
                    </td>	  
                </tr>
             </table>
		</fieldset>
        <div id="list_view" style="margin-top:10px">
        	<?php
			if(str_replace("'",'',$plan_id)!="")
			{
			?>
				<script>
					show_list_view('<?php echo str_replace("'",'',$plan_id); ?>', 'planning_info_details', 'list_view', 'planning_info_entry_controller', '' ) ;
                </script>
            <?php
			}
			?>
        </div>
	</form>
</div>
</body>           
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
	exit(); 
}

if($action=="planning_info_details")
{
	$sql="select id, knitting_source, knitting_party, color_range, machine_dia, machine_gg, program_qnty, stitch_length, spandex_stitch_length, draft_ratio, status, program_date from ppl_planning_info_entry_dtls where mst_id=$data and status_active = '1' and is_deleted = '0'";
?>
	<table cellspacing="0" cellpadding="0" border="1" rules="all" width="900" class="rpt_table">
		<thead>
			<th width="90">Knitting Source</th>
			<th width="100">Knitting Company</th>
			<th width="90">Color Range</th>
			<th width="70">Machine Dia</th>
			<th width="70">Machine GG</th>
			<th width="80">Program Qnty</th>
			<th width="75">Stitch Length</th>
			<th width="80">Span. Stitch Length</th>
			<th width="70">Draft Ratio</th>
            <th width="75">Program Date</th>
			<th>Status</th>
		</thead>
	</table>
	<div style="width:900px; max-height:140px; overflow-y:scroll" id="list_container_batch" align="left">	 
		<table cellspacing="0" cellpadding="0" border="1" rules="all" width="882" class="rpt_table" id="tbl_list_search">  
		<?php
			$i=1;
			$result=sql_select($sql);
			foreach ($result as $row)
			{  
				if ($i%2==0)  
					$bgcolor="#E9F3FF";
				else
					$bgcolor="#FFFFFF";	 
				if($row[csf('knitting_source')]==1)
					$knit_party=$company_arr[$row[csf('knitting_party')]]; 
				else
					$knit_party=$supllier_arr[$row[csf('knitting_party')]];
			?>
				<tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" onClick="get_php_form_data(<?php echo $row[csf('id')]; ?>,'populate_data_from_planning_info', 'planning_info_entry_controller' );"> 
					<td width="90"><p><?php echo $knitting_source[$row[csf('knitting_source')]]; ?></p></td>
					<td width="100"><p><?php echo $knit_party; ?></p></td>             
					<td width="90"><p><?php echo $color_range[$row[csf('color_range')]]; ?></p></td>
					<td width="70"><p><?php echo $row[csf('machine_dia')]; ?></p></td>
					<td width="70"><?php echo $row[csf('machine_gg')]; ?></td>
					<td width="80" align="right"><?php echo number_format($row[csf('program_qnty')],2); ?></td>
					<td width="75"><p><?php echo $row[csf('stitch_length')]; ?></p></td>
					<td width="80"><p><?php echo $row[csf('spandex_stitch_length')]; ?></p></td>
					<td width="70" align="right"><?php echo number_format($row[csf('draft_ratio')],2); ?></td>
                    <td width="75" align="right"><?php echo change_date_format($row[csf('program_date')]); ?></td>
					<td><p><?php echo $knitting_program_status[$row[csf('status')]]; ?></p></td>
				</tr>
			<?php
			$i++;
			}
			?>
		</table>
	</div>
<?php
}

if($action=="populate_data_from_planning_info")
{
	$color_library=return_library_array( "select id,color_name from lib_color", "id", "color_name"  );
	$sql="select id, knitting_source, knitting_party, color_id, color_range, machine_dia, width_dia_type, machine_gg, fabric_dia, program_qnty, stitch_length, spandex_stitch_length, draft_ratio, machine_id, machine_capacity, distribution_qnty, status, start_date, end_date, program_date, feeder, remarks, save_data, no_fo_feeder_data from ppl_planning_info_entry_dtls where id=$data";
	$data_array=sql_select($sql);
	foreach ($data_array as $row)
	{ 
		echo "document.getElementById('cbo_knitting_source').value 			= '".$row[csf("knitting_source")]."';\n";
		
		echo "load_drop_down('planning_info_entry_controller', ".$row[csf("knitting_source")]."+'**'+".$row[csf("knitting_party")]."+'**1', 'load_drop_down_knitting_party','knitting_party');\n";
		
		$color='';
		$color_id=explode(",",$row[csf("color_id")]);
		foreach($color_id as $val)
		{
			if($color=="") $color=$color_library[$val]; else $color.=",".$color_library[$val];
		}
		
		echo "document.getElementById('knitting_party').value 				= '".$row[csf("knitting_party")]."';\n";
		echo "document.getElementById('txt_color').value 					= '".$color."';\n";
		echo "document.getElementById('hidden_color_id').value 				= '".$row[csf("color_id")]."';\n";
		echo "document.getElementById('cbo_color_range').value 				= '".$row[csf("color_range")]."';\n";
		echo "document.getElementById('txt_machine_dia').value 				= '".$row[csf("machine_dia")]."';\n";
		echo "document.getElementById('cbo_dia_width_type').value 			= '".$row[csf("width_dia_type")]."';\n";
		echo "document.getElementById('txt_machine_gg').value 				= '".$row[csf("machine_gg")]."';\n";
		echo "document.getElementById('txt_fabric_dia').value 				= '".$row[csf("fabric_dia")]."';\n";
		echo "document.getElementById('txt_program_qnty').value 			= '".$row[csf("program_qnty")]."';\n";
		echo "document.getElementById('txt_stitch_length').value 			= '".$row[csf("stitch_length")]."';\n";
		echo "document.getElementById('txt_spandex_stitch_length').value 	= '".$row[csf("spandex_stitch_length")]."';\n";
		echo "document.getElementById('txt_draft_ratio').value 				= '".$row[csf("draft_ratio")]."';\n";
		
		echo "active_inactive();\n";
		
		echo "document.getElementById('machine_id').value 					= '".$row[csf("machine_id")]."';\n";
		$machine_no='';
		$machine_id=explode(",",$row[csf("machine_id")]);
		foreach($machine_id as $val)
		{
			if($machine_no=='') $machine_no=$machine_arr[$val]; else $machine_no.=",".$machine_arr[$val];
		}
		
		echo "document.getElementById('txt_machine_no').value 				= '".$machine_no."';\n";
		echo "document.getElementById('txt_machine_capacity').value 		= '".$row[csf("machine_capacity")]."';\n";
		echo "document.getElementById('txt_distribution_qnty').value 		= '".$row[csf("distribution_qnty")]."';\n";

		//$days_req=$row[csf("distribution_qnty")]/$row[csf("machine_capacity")];
		//if($days_req<0) $days_req=''; else $days_req=number_format($days_req,2,'.','');
		
		//echo "document.getElementById('txt_days_req').value 				= '".$days_req."';\n";

		echo "document.getElementById('cbo_knitting_status').value 			= '".$row[csf("status")]."';\n";
		echo "document.getElementById('txt_start_date').value 				= '".change_date_format($row[csf("start_date")])."';\n";
		echo "document.getElementById('txt_end_date').value 				= '".change_date_format($row[csf("end_date")])."';\n";
		echo "document.getElementById('txt_program_date').value 			= '".change_date_format($row[csf("program_date")])."';\n";
		echo "document.getElementById('cbo_feeder').value 					= '".$row[csf("feeder")]."';\n";
		echo "document.getElementById('txt_remarks').value 					= '".$row[csf("remarks")]."';\n";
		echo "document.getElementById('save_data').value 					= '".$row[csf("save_data")]."';\n";
		echo "document.getElementById('hidden_no_of_feeder_data').value 	= '".$row[csf("no_fo_feeder_data")]."';\n";
		echo "document.getElementById('update_dtls_id').value 				= '".$row[csf("id")]."';\n";
		echo "document.getElementById('txt_program_no').value 				= '".$row[csf("id")]."';\n";
		echo "days_req();\n";
		echo "set_button_status(1, '".$_SESSION['page_permission']."', 'fnc_program_entry',1);\n";  
		exit();
	}
}


if($action=="machine_info_popup")
{
	echo load_html_head_contents("Machine Info", "../../", 1, 1,'','','');
	extract($_REQUEST); 
	?>
    
    <script>
	
		$(document).ready(function(e) {
            setFilterGrid('tbl_list_search',-1);
        });
		
		function calculate_qnty(tr_id)
		{
			var distribution_qnty=$('#txt_distribution_qnty_'+tr_id).val()*1;
			if(distribution_qnty>0)
			{
				$('#search' + tr_id).css('background-color','yellow');
			}
			else
			{
				$('#search' + tr_id).css('background-color','#FFFFCC');
			}
			
			calculate_total_qnty('txt_distribution_qnty_','txt_total_distribution_qnty');
		}
		
		function calculate_total_qnty(field_id,total_field_id)
		{
			var tot_row=$("#tbl_list_search tbody tr").length-1;

			var ddd={ dec_type:2, comma:0, currency:''}
			
			math_operation( total_field_id, field_id, "+", tot_row,ddd );

		}
		
		function fnc_close()
		{
			var save_string=''; var allMachineId=''; var allMachineNo=''; var tot_capacity=''; var tot_distribution_qnty=''; var min_date=''; var max_date='';
			var tot_row=$("#tbl_list_search tbody tr").length-1;
			
			for(var i=1; i<=tot_row; i++)
			{
				var machineId=$('#txt_individual_id'+i).val();
				var machineNo=$('#txt_individual'+i).val();
				var capacity=$('#txt_capacity_'+i).val();
				var distributionQnty=$('#txt_distribution_qnty_'+i).val();
				var noOfDays=$('#txt_noOfDays_'+i).val();
				var startDate=$('#txt_startDate_'+i).val();
				var endDate=$('#txt_endDate_'+i).val();
				
				if(distributionQnty*1>0)
				{
					if(save_string=="")
					{
						save_string=machineId+"_"+machineNo+"_"+capacity+"_"+distributionQnty+"_"+noOfDays+"_"+startDate+"_"+endDate;
						allMachineId=machineId;
						allMachineNo=machineNo;
					}
					else
					{
						save_string+=","+machineId+"_"+machineNo+"_"+capacity+"_"+distributionQnty+"_"+noOfDays+"_"+startDate+"_"+endDate;
						allMachineId+=","+machineId;
						allMachineNo+=","+machineNo;
					}
					
					if(min_date=='')
					{
						min_date=startDate;
					}
					
					if(date_compare(min_date, startDate )==false)
					{
						min_date=startDate;
					}
					
					if(date_compare(min_date, endDate )==false)
					{
						min_date=endDate;
					}
					
					if(max_date=='')
					{
						max_date=startDate;
					}
					
					if(date_compare(max_date, startDate)==true)
					{
						max_date=startDate;
					}
					
					if(date_compare(max_date, endDate)==true)
					{
						max_date=endDate;
					}
					
					tot_capacity=tot_capacity*1+capacity*1;
					tot_distribution_qnty=tot_distribution_qnty*1+distributionQnty*1;
				}
			}
			
			$('#hidden_machine_id').val(allMachineId);	
			$('#hidden_machine_no').val(allMachineNo);	
			$('#save_string').val( save_string );
			$('#hidden_machine_capacity').val( tot_capacity );
			$('#hidden_distribute_qnty').val( tot_distribution_qnty );
			$('#hidden_min_date').val( min_date );
			$('#hidden_max_date').val( max_date );
			
			parent.emailwindow.hide();
		}
		
		function fn_add_date_field(row_no)
		{
			var distribute_qnty=$('#txt_distribution_qnty_'+row_no).val()*1;
			
			if(distribute_qnty==0 || distribute_qnty<0) 
			{
				alert("Please Insert Distribution Qnty First.");
				$('#txt_startDate_'+row_no).val('');
				$('#txt_distribution_qnty_'+row_no).focus();
				return;
			}

			if($('#txt_startDate_'+row_no).val()!="")
			{
				var days_req=$('#txt_noOfDays_'+row_no).val();
				
				days_req=Math.ceil(days_req);
				if(days_req>0)
				{
					days_req=days_req-1;
					$("#txt_endDate_"+row_no).val(add_days($('#txt_startDate_'+row_no).val(),days_req));
				}
				
				var txt_startDate=$('#txt_startDate_'+row_no).val();
				var txt_endDate=$('#txt_endDate_'+row_no).val();
				var machine_id=$('#txt_individual_id'+row_no).val();
				
				var data=machine_id+"**"+txt_startDate+"**"+txt_endDate+"**"+'<?php echo $update_dtls_id; ?>';
				var response=return_global_ajax_value( data, 'date_duplication_check', '', 'planning_info_entry_controller');
				var response=response.split("_");
				//alert(response);return;
				if(response[0]!=0)
				{
					alert("Date Overlaping for this machine. Dates Are ("+response[1]+").");
					$('#txt_startDate_'+row_no).val('');
					$('#txt_endDate_'+row_no).val('');
					return;
				}
			}
		}
		
		function calculate_noOfDays(row_no)
		{
			var distribute_qnty=$('#txt_distribution_qnty_'+row_no).val();
			var machine_capacity=$('#txt_capacity_'+row_no).val();
			
			var days_req=distribute_qnty*1/machine_capacity*1;
			$('#txt_noOfDays_'+row_no).val(days_req.toFixed(2));
			
			if(distribute_qnty*1>0)
			{
				fn_add_date_field(row_no);
			}
			else
			{
				$('#txt_noOfDays_'+row_no).val('');
				$('#txt_startDate_'+row_no).val('');
				$('#txt_endDate_'+row_no).val('');
			}
		}
		
    </script>

</head>

<body>
<div style="width:830px;">
	<form name="searchwofrm"  id="searchwofrm">
		<fieldset style="width:820px; margin-top:10px; margin-left:5px">
        	<input type="hidden" name="save_string" id="save_string" class="text_boxes" value="">
         	<input type="hidden" name="hidden_machine_id" id="hidden_machine_id" class="text_boxes" value=""> 
            <input type="hidden" name="hidden_machine_no" id="hidden_machine_no" class="text_boxes" value="">  
        	<input type="hidden" name="hidden_machine_capacity" id="hidden_machine_capacity" class="text_boxes" value="">   
            <input type="hidden" name="hidden_distribute_qnty" id="hidden_distribute_qnty" class="text_boxes" value="">  
            <input type="hidden" name="hidden_min_date" id="hidden_min_date" class="text_boxes" value="">   
			<input type="hidden" name="hidden_max_date" id="hidden_max_date" class="text_boxes" value="">      
            <table cellspacing="0" cellpadding="0" border="1" rules="all" width="800" class="rpt_table" >
                <thead>
                    <th width="40">SL</th>
                    <th width="80">Floor No</th> 
                    <th width="60">Machine No</th>               
                    <th width="60">Dia</th>
                    <th width="60">GG</th>
                    <th width="80">Group</th>
                    <th width="90">Capacity</th>
                    <th width="90">Distribution Qnty</th> 
                    <th width="60">No. Of Days</th> 
                    <th width="80">Start Date</th> 
                    <th>End Date</th> 
                </thead>
            </table>
            <div style="width:818px; overflow-y:scroll; max-height:220px;" id="buyer_list_view">
                <table cellspacing="0" cellpadding="0" border="1" rules="all" width="800" class="rpt_table" id="tbl_list_search">
                    <tbody>
                    <?php
                        $qnty_array=array();
                        $save_string=explode(",",$save_string);
        
                        for($i=0;$i<count($save_string);$i++)
                        {
                            $machine_wise_data=explode("_",$save_string[$i]);
                            $machine_id=$machine_wise_data[0];
                            $capacity=$machine_wise_data[2];
                            $distribution_qnty=$machine_wise_data[3];
                            $noOfDays=$machine_wise_data[4];
                            $startDate=$machine_wise_data[5];
                            $endDate=$machine_wise_data[6];
                            
                            $qnty_array[$machine_id]['capacity']=$capacity;
                            $qnty_array[$machine_id]['distribution']=$distribution_qnty;
                            $qnty_array[$machine_id]['noOfDays']=$noOfDays;
                            $qnty_array[$machine_id]['startDate']=$startDate;
                            $qnty_array[$machine_id]['endDate']=$endDate;
                        }
						
                        $floor_arr=return_library_array( "select id, floor_name from lib_prod_floor",'id','floor_name');
						
                        $sql="select id, machine_no, dia_width, gauge, machine_group, prod_capacity, floor_id from lib_machine_name where company_id=$companyID and category_id=1 and status_active=1 and is_deleted=0 order by machine_no";// and dia_width='$txt_machine_dia'
                        $result = sql_select($sql);
                        
                        $i=1; $tot_capacity=0; $tot_distribution_qnty=0;
                        foreach($result as $row)
                        {
                            if ($i%2==0)  
                                $bgcolor="#E9F3FF";
                            else
                                $bgcolor="#FFFFFF";
                            
                            $capacity=$qnty_array[$row[csf('id')]]['capacity'];
                            if($capacity=="")
                            {
                                $capacity=$row[csf('prod_capacity')];
                            }
                            
                            $distribution_qnty=$qnty_array[$row[csf('id')]]['distribution'];
                            
                            if($distribution_qnty>0) $bgcolor="yellow"; else $bgcolor=$bgcolor;
                            
                            $noOfDays=$qnty_array[$row[csf('id')]]['noOfDays'];
                            $startDate=$qnty_array[$row[csf('id')]]['startDate'];
                            $endDate=$qnty_array[$row[csf('id')]]['endDate'];
                            
                            $tot_capacity+=$capacity; 
                            $tot_distribution_qnty+=$distribution_qnty;
                            
                            ?>
                            <tr valign="middle" bgcolor="<?php echo $bgcolor; ?>" id="search<?php echo $i;?>"> 
                                <td width="40" align="center"><?php echo $i; ?>
                                    <input type="hidden" name="txt_individual_id" id="txt_individual_id<?php echo $i ?>" value="<?php echo $row[csf('id')]; ?>"/> 
                                    <input type="hidden" name="txt_individual" id="txt_individual<?php echo $i ?>" value="<?php echo $row[csf('machine_no')]; ?>"/> 
                                </td>	
                                <td width="80"><p><?php echo $floor_arr[$row[csf('floor_id')]]; ?></p></td>     
                                <td width="60"><p><?php echo $row[csf('machine_no')]; ?></p></td>               
                                <td width="60" align="center"><p><?php echo $row[csf('dia_width')]; ?></p></td>
                                <td width="60" align="center"><p><?php echo $row[csf('gauge')]; ?></p></td>
                                <td width="80" align="center"><p><?php echo $row[csf('machine_group')]; ?></p></td>
                                <td width="90" align="center">
                                     <input type="text" name="txt_capacity[]" id="txt_capacity_<?php echo $i; ?>" class="text_boxes_numeric" style="width:75px" value="<?php echo $capacity; ?>" onKeyUp="calculate_total_qnty('txt_capacity_','txt_total_capacity');calculate_noOfDays(<?php echo $i; ?>);"/>
                                </td>                    
                                <td align="center" width="90">
                                    <input type="text" name="txt_distribution_qnty[]" id="txt_distribution_qnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:75px" value="<?php echo $distribution_qnty; ?>" onKeyUp="calculate_qnty(<?php echo $i; ?>);calculate_noOfDays(<?php echo $i; ?>);"/>
                                </td>
                                <td align="center" width="60">
                                    <input type="text" name="txt_noOfDays[]" id="txt_noOfDays_<?php echo $i; ?>" class="text_boxes_numeric" style="width:45px" value="<?php echo $noOfDays; ?>" onKeyUp="calculate_noOfDays(<?php echo $i; ?>);" disabled="disabled"/>
                                </td> 
                                <td align="center" width="80">
                                    <input type="text" name="txt_startDate[]" id="txt_startDate_<?php echo $i; ?>" class="datepicker" style="width:67px" value="<?php echo $startDate; ?>" onChange="fn_add_date_field(<?php echo $i; ?>);"/>
                                </td> 
                                <td align="center">
                                    <input type="text" name="txt_endDate[]" id="txt_endDate_<?php echo $i; ?>" class="datepicker" style="width:67px" value="<?php echo $endDate; ?>" disabled="disabled"/>
                                </td>
                            </tr>
                        <?php
                        $i++;
                        }
                    ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="6" align="right"><b>Total</b></th>
                            <th align="center"><input type="text" name="txt_total_capacity" id="txt_total_capacity" class="text_boxes_numeric" style="width:75px" readonly disabled="disabled" value="<?php echo $tot_capacity; ?>"/></th>
                            <th align="center"><input type="text" name="txt_total_distribution_qnty" id="txt_total_distribution_qnty" class="text_boxes_numeric" style="width:75px" readonly disabled="disabled" value="<?php echo $tot_distribution_qnty; ?>"/></th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <table width="700" id="tbl_close">
                 <tr>
                    <td align="center" >
                        <input type="button" name="close" class="formbutton" value="Close" id="main_close" onClick="fnc_close();" style="width:100px" />
                    </td>
                </tr>
            </table>
		</fieldset>
	</form>
</div>
</body>           
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
	exit();
}

if($action=="date_duplication_check")
{
	$data=explode("**",$data);
	$machine_id=$data[0];
	if($db_type==0)
	{
		$startDate=change_date_format(trim($data[1]),"yyyy-mm-dd","");
		$endDate=change_date_format(trim($data[2]),"yyyy-mm-dd","");
	}
	else
	{
		$startDate=change_date_format(trim($data[1]),'','',1);
		$endDate=change_date_format(trim($data[2]),'','',1);	
	}
	$update_dtls_id=$data[3];
	
	if($update_dtls_id=="")
	{
		$sql="select distribution_date, sum(days_complete) as days_complete from ppl_entry_machine_datewise where machine_id='$machine_id' and distribution_date between '$startDate' and '$endDate' group by distribution_date";
	}
	else
	{
		$sql="select distribution_date, sum(days_complete) as days_complete from ppl_entry_machine_datewise where machine_id='$machine_id' and distribution_date between '$startDate' and '$endDate' and dtls_id<>$update_dtls_id group by distribution_date";
	}
	//echo $sql;die;
	$data_array=sql_select($sql);
	$data='';
	if(count($data_array)>0)
	{
		foreach($data_array as $row)
		{
			if($row[csf('days_complete')] >= 1)
			{
				if($data=='') $data=change_date_format($row[csf('distribution_date')]); else $data.=",".change_date_format($row[csf('distribution_date')]);
			}
		}
		
		if($data=='') echo "0_"; else echo "1"."_".$data;
	}
	else
	{
		echo "0_";
	}
	
	exit();	
}

if($action=="color_info_popup")
{
	echo load_html_head_contents("Color Info", "../../", 1, 1,'','','');
	extract($_REQUEST);
	?>
    
    <script>
	
		$(document).ready(function(e) {
            setFilterGrid('tbl_list_search',-1);
			set_all();
        });
		
		 var selected_id = new Array, selected_name = new Array();
		
		function check_all_data()
		{
			var tbl_row_count = $('#tbl_list_search tbody tr').length;
			tbl_row_count = tbl_row_count - 1;
			
			for( var i = 1; i <= tbl_row_count; i++ ) {
				js_set_value( i );
			}
		}
		
		function toggle( x, origColor ) 
		{
			var newColor = 'yellow';
			if ( x.style ) 
			{
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
		function set_all()
		{
			var old=document.getElementById('txt_color_row_id').value;
			if(old!="")
			{   
				old=old.split(",");
				for(var i=0; i<old.length; i++)
				{  
					js_set_value( old[i] ) 
				}
			}
		}

		function js_set_value( str) 
		{
			toggle( document.getElementById( 'search' + str ), '#FFFFCC' );
			 
			if( jQuery.inArray( $('#txt_individual_id' + str).val(), selected_id ) == -1 ) {
				selected_id.push( $('#txt_individual_id' + str).val() );
				selected_name.push( $('#txt_individual' + str).val() );
				
			}
			else
			{
				for( var i = 0; i < selected_id.length; i++ )
				{
					if( selected_id[i] == $('#txt_individual_id' + str).val() ) break;
				}
				selected_id.splice( i, 1 );
				selected_name.splice( i, 1 );
			}
			var id =''; var name = '';
			for( var i = 0; i < selected_id.length; i++ ) {
				id += selected_id[i] + ',';
				name += selected_name[i] + ',';
			}
			id = id.substr( 0, id.length - 1 );
			name = name.substr( 0, name.length - 1 );
			
			$('#txt_selected_id').val( id );
			$('#txt_selected').val( name );
		}
    </script>

</head>

<body>
<div align="center" style="width:390px;">
	<form name="searchwofrm"  id="searchwofrm">
		<fieldset style="width:380px; margin-top:10px; margin-left:20px">
            <div>
                <table cellspacing="0" cellpadding="0" border="1" rules="all" width="360" class="rpt_table" >
                    <thead>
                        <th width="40">SL</th>
                        <th width="160">Color</th>               
                        <th>Qnty</th> 
                        <input type="hidden" name="txt_selected_id" id="txt_selected_id" value="" />
                    	<input type="hidden" name="txt_selected"  id="txt_selected" value="" />  
                    </thead>
                </table>
                <div style="width:360px; overflow-y:scroll; max-height:230px;" id="buyer_list_view" align="center">
                    <table cellspacing="0" cellpadding="0" border="1" rules="all" width="342" class="rpt_table" id="tbl_list_search">
                    	<tbody>
						<?php 
							$hidden_color_id=explode(",",$hidden_color_id);
							$pre_cost_id=explode(",",$pre_cost_id);
							$pre_cost_id=implode(",",array_unique($pre_cost_id));
							$color_library=return_library_array( "select id,color_name from lib_color", "id", "color_name"  );
							
							$sql="select b.fabric_color_id, sum(b.grey_fab_qnty) as qnty from wo_booking_mst a, wo_booking_dtls b where a.booking_no=b.booking_no and a.company_id=$companyID and a.item_category=2 and a.booking_no='$booking_no' and b.dia_width='$dia' and b.po_break_down_id in ($po_id) and b.pre_cost_fabric_cost_dtls_id in ($pre_cost_id) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by b.fabric_color_id";
                            $result = sql_select($sql);
							
							$i=1; $tot_qnty=0;
                            foreach($result as $row)
                            {
                                if ($i%2==0)  
                                    $bgcolor="#E9F3FF";
                                else
                                    $bgcolor="#FFFFFF";
									
                                $tot_qnty+=$row[csf('qnty')];
								
								if(in_array($row[csf('fabric_color_id')],$hidden_color_id)) 
								{
									if($color_row_id=="") $color_row_id=$i; else $color_row_id.=",".$i;
								}
											
                                ?>
                                <tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" id="search<?php echo $i;?>" onClick="js_set_value(<?php echo $i; ?>)"> 
                                    <td width="40" align="center"><?php echo $i; ?>
                                     	<input type="hidden" name="txt_individual_id" id="txt_individual_id<?php echo $i; ?>" value="<?php echo $row[csf('fabric_color_id')]; ?>"/>	
                         				<input type="hidden" name="txt_individual" id="txt_individual<?php echo $i; ?>" value="<?php echo $color_library[$row[csf('fabric_color_id')]]; ?>"/>
                                    </td>	
                                    <td width="160"><p><?php echo $color_library[$row[csf('fabric_color_id')]]; ?></p></td>               
                                    <td align="right"><?php echo number_format($row[csf('qnty')],2); ?></td>
                                </tr>
                            <?php
                            $i++;
                            }
                        ?>
                        	<input type="hidden" name="txt_color_row_id" id="txt_color_row_id" value="<?php echo $color_row_id; ?>"/>
                    	</tbody>
                    	<tfoot>
                        	<tr>
                                <th colspan="2" align="right"><b>Total</b></th>
                                <th align="right"><?php echo number_format($tot_qnty,2); ?></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <div style="width:100%; margin-left:10px; margin-top:5px"> 
                <div style="width:43%; float:left" align="left">
                    <input type="checkbox" name="check_all" id="check_all" onClick="check_all_data();" /> Check / Uncheck All
                </div>
                <div style="width:57%; float:left" align="left">
                    <input type="button" name="close" onClick="parent.emailwindow.hide();" class="formbutton" value="Close" style="width:100px" />
                </div>
            </div>
		</fieldset>
	</form>
</div>
</body>           
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
	exit();
}

if($action=="feeder_info_popup")
{
	echo load_html_head_contents("Machine Info", "../../", 1, 1,'','','');
	extract($_REQUEST); 
	?>
    
    <script>
	
		function fnc_close()
		{
			var save_string='';
			var tot_row=$("#tbl_list_search tbody tr").length;
			
			for(var i=1; i<=tot_row; i++)
			{
				var txtPreCostId=$('#txtPreCostId_'+i).val();
				var txtColorId=$('#txtColorId_'+i).val();
				var txtStripeColorId=$('#txtStripeColorId_'+i).val();
				var txtNoOfFeeder=$('#txtNoOfFeeder_'+i).val();
				
				if(save_string=="")
				{
					save_string=txtPreCostId+"_"+txtColorId+"_"+txtStripeColorId+"_"+txtNoOfFeeder;
				}
				else
				{
					save_string+=","+txtPreCostId+"_"+txtColorId+"_"+txtStripeColorId+"_"+txtNoOfFeeder;
				}
				
			}
			
			$('#hidden_no_of_feeder_data').val( save_string );
				
			parent.emailwindow.hide();
		}
		
		function calculate_total()
		{
			var tot_row=$("#tbl_list_search tbody tr").length;
			
			var ddd={ dec_type:6, comma:0, currency:''}
					
			math_operation( "txtTotFeeder", "txtNoOfFeeder_", "+", tot_row,ddd );
		}
		
    </script>

</head>

<body>
<div style="width:630px;">
	<form name="searchwofrm"  id="searchwofrm">
		<fieldset style="width:620px; margin-top:10px; margin-left:5px">
        	<input type="hidden" name="hidden_no_of_feeder_data" id="hidden_no_of_feeder_data" class="text_boxes" value="">
             <table cellspacing="0" cellpadding="0" border="1" rules="all" width="600" class="rpt_table" >
                <thead>
                    <th width="40">SL</th>
                    <th width="140">Color</th> 
                    <th width="130">Stripe Color</th> 
                    <th width="90">Measurement</th>               
                    <th width="70">UOM</th>
                    <th>No Of Feeder</th> 
                </thead>
            </table>
            <div style="width:618px; overflow-y:scroll; max-height:230px;" id="buyer_list_view">
                <table cellspacing="0" cellpadding="0" border="1" rules="all" width="600" class="rpt_table" id="tbl_list_search">
                	<tbody>
                    <?php
                        $noOfFeeder_array=array();
                        $no_of_feeder_data=explode(",",$no_of_feeder_data);
        				$pre_cost_id=explode(",",$pre_cost_id);
						$pre_cost_id=implode(",",array_unique($pre_cost_id));
						
						$color_library=return_library_array( "select id,color_name from lib_color", "id", "color_name"  );
						
                        for($i=0;$i<count($no_of_feeder_data);$i++)
                        {
                            $color_wise_data=explode("_",$no_of_feeder_data[$i]);
                            $pre_cost_fabric_cost_dtls_id=$color_wise_data[0];
                            $color_id=$color_wise_data[1];
                            $stripe_color=$color_wise_data[2];
                            $no_of_feeder=$color_wise_data[3];
							
							$noOfFeeder_array[$pre_cost_fabric_cost_dtls_id][$color_id][$stripe_color]=$no_of_feeder;
                        }

                        $sql="select pre_cost_fabric_cost_dtls_id as pre_cost_id, color_number_id, stripe_color, measurement, uom from wo_pre_stripe_color where pre_cost_fabric_cost_dtls_id in($pre_cost_id) and status_active=1 and is_deleted=0";
                        $result = sql_select($sql);
                        
                        $i=1; $tot_feeder=0;
                        foreach($result as $row)
                        {
                            if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
                            
                            $no_of_feeder=$noOfFeeder_array[$row[csf('pre_cost_id')]][$row[csf('color_number_id')]][$row[csf('stripe_color')]];
							$tot_feeder+=$no_of_feeder;
							
                            ?>
                            <tr valign="middle" bgcolor="<?php echo $bgcolor; ?>" id="search<?php echo $i;?>"> 
                                <td width="40" align="center"><?php echo $i; ?>
                                    <input type="hidden" name="txtPreCostId[]" id="txtPreCostId_<?php echo $i ?>" value="<?php echo $row[csf('pre_cost_id')]; ?>"/>
                                    <input type="hidden" name="txtColorId[]" id="txtColorId_<?php echo $i ?>" value="<?php echo $row[csf('color_number_id')]; ?>"/>
                                    <input type="hidden" name="txtStripeColorId[]" id="txtStripeColorId_<?php echo $i ?>" value="<?php echo $row[csf('stripe_color')]; ?>"/> 
                                </td>	
                                <td width="140"><p><?php echo $color_library[$row[csf('color_number_id')]]; ?></p></td> 
                                <td width="130"><p><?php echo $color_library[$row[csf('stripe_color')]]; ?></p></td>     
                                <td width="90"><input type="text" name="txtMeasurement[]" id="txtMeasurement_<?php echo $i; ?>" class="text_boxes_numeric" style="width:80px" value="<?php echo $row[csf('measurement')]; ?>" disabled/></td>               
                                <td width="70" align="center"><p><?php echo $unit_of_measurement[$row[csf('uom')]]; ?></p></td>
                                <td align="center">
                                    <input type="text" name="txtNoOfFeeder[]" id="txtNoOfFeeder_<?php echo $i; ?>" class="text_boxes_numeric" style="width:90px" value="<?php echo $no_of_feeder; ?>" onKeyUp="calculate_total();"/>
                                </td>
                            </tr>
                        <?php
                        $i++;
                        }
                    ?>
                    </tbody>
                    <tfoot>
                    	<th colspan="5">Total</th>
                    	<th style="text-align:center"><input type="text" name="txtTotFeeder" id="txtTotFeeder" class="text_boxes_numeric" style="width:90px" value="<?php echo $tot_feeder; ?>" disabled/></th> 
                    </tfoot>
                </table>
            </div>
            <table width="600" id="tbl_close">
                 <tr>
                    <td align="center" >
                        <input type="button" name="close" class="formbutton" value="Close" id="main_close" onClick="fnc_close();" style="width:100px" />
                    </td>
                </tr>
            </table>
		</fieldset>
	</form>
</div>
</body>           
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
	exit();
}


if ($action=="save_update_delete")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	if ($operation==0)  // Insert Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
			$start_date = change_date_format( str_replace("'","",trim($start_date)),"yyyy-mm-dd","");
			$end_date = change_date_format( str_replace("'","",trim($end_date)),"yyyy-mm-dd","");
		}
	 	else
		{
			$start_date = change_date_format( str_replace("'","",trim($start_date)),'','',1);
			$end_date = change_date_format( str_replace("'","",trim($end_date)),'','',1);
		}
		
		$id='';
		
		if(str_replace("'",'',$updateId)=="")
		{
			$id=return_next_id( "id","ppl_planning_info_entry_mst", 1 ) ;
					 
			$field_array="id, company_id, buyer_id, booking_no, body_part_id, color_type_id, determination_id, fabric_desc, gsm_weight, dia, width_dia_type, inserted_by, insert_date";
			
			$data_array="(".$id.",".$companyID.",".$buyer_id.",'".$booking_no."',".$body_part_id.",".$color_type_id.",".$determination_id.",'".$desc."','".$gsm."','".$dia."',".$fabric_typee.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
			
			//echo "insert into ppl_planning_info_entry_mst (".$field_array.") values ".$data_array;die;
			/*$rID=sql_insert("ppl_planning_info_entry_mst",$field_array,$data_array,0);
			if($rID) $flag=1; else $flag=0;*/
		}
		else
		{
			$id=str_replace("'",'',$updateId);
			$flag=1;
		}
		//echo $flag;die;
		
		$dtls_id=return_next_id( "id","ppl_planning_info_entry_dtls", 1 ) ;
		$field_array_dtls="id, mst_id, knitting_source, knitting_party, color_id, color_range, machine_dia, width_dia_type, machine_gg, fabric_dia, program_qnty, stitch_length, spandex_stitch_length, draft_ratio,  machine_id, machine_capacity, distribution_qnty, status, start_date, end_date, program_date, feeder, remarks, save_data, no_fo_feeder_data, inserted_by, insert_date"; 
		
		$data_array_dtls="(".$dtls_id.",".$id.",".$cbo_knitting_source.",".$cbo_knitting_party.",".$hidden_color_id.",".$cbo_color_range.",".$txt_machine_dia.",".$cbo_dia_width_type.",".$txt_machine_gg.",".$txt_fabric_dia.",".$txt_program_qnty.",".$txt_stitch_length.",".$txt_spandex_stitch_length.",".$txt_draft_ratio.",".$machine_id.",".$txt_machine_capacity.",".$txt_distribution_qnty.",".$cbo_knitting_status.",".$txt_start_date.",".$txt_end_date.",".$txt_program_date.",".$cbo_feeder.",".$txt_remarks.",".$save_data.",".$hidden_no_of_feeder_data.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')"; 
		
		$plan_dtls_id=return_next_id( "id","ppl_planning_entry_plan_dtls", 1 ) ;
		$field_array_plan_dtls="id, mst_id, dtls_id, company_id, buyer_id, booking_no, po_id, start_date, finish_date, body_part_id, color_type_id, determination_id, fabric_desc, gsm_weight, dia, width_dia_type, color_id, yarn_desc, program_qnty, inserted_by, insert_date";
		
		$data=str_replace("'","",$data);
		if($data!="")
		{
			$data=explode("_",$data);
			for($i=0;$i<count($data);$i++)
			{
				$plan_data=explode("**",$data[$i]);	
				$booking_no=$plan_data[0];
				$start_date=$plan_data[1];
				$end_date=$plan_data[2];
				$po_id=$plan_data[3];
				$buyer_id=$plan_data[4];
				$body_part_id=$plan_data[5];
				$dia_width_type=$plan_data[6];
				$pre_cost_id=$plan_data[7];
				$desc=$plan_data[8];
				$gsm_weight=$plan_data[9];
				$dia_width=$plan_data[10];
				$determination_id=$plan_data[11];
				$booking_qnty=$plan_data[12];
				$color_type_id=$plan_data[13];
				
				$perc=($booking_qnty/$tot_booking_qnty)*100;
				$prog_qnty=($perc*str_replace("'",'',$txt_program_qnty))/100;
				
				if($data_array_plan_dtls!="") $data_array_plan_dtls.=",";
					
				$data_array_plan_dtls.="(".$plan_dtls_id.",".$id.",".$dtls_id.",".$companyID.",".$buyer_id.",'".$booking_no."',".$po_id.",'".$start_date."','".$end_date."',".$body_part_id.",".$color_type_id.",".$determination_id.",'".$desc."',".$gsm.",'".$dia."',".$dia_width_type.",0,".$pre_cost_id.",".$prog_qnty.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
				
				$plan_dtls_id=$plan_dtls_id+1;
			}
	
			/*if($data_array_plan_dtls!="")
			{
				//echo"insert into ppl_planning_entry_plan_dtls (".$field_array_plan_dtls.") Values ".$data_array_plan_dtls."";die;
				$rID3=sql_insert("ppl_planning_entry_plan_dtls",$field_array_plan_dtls,$data_array_plan_dtls,0);
				if($flag==1) 
				{
					if($rID3) $flag=1; else $flag=0; 
				}  
			}*/
		}
		
		$machine_dtls_id=return_next_id( "id","ppl_planning_info_machine_dtls", 1 ) ;
		$field_array_machine_dtls="id, mst_id, dtls_id, machine_id, dia, capacity, distribution_qnty, no_of_days, start_date, end_date, inserted_by, insert_date";
		
		$machine_dtls_datewise_id=return_next_id( "id","ppl_entry_machine_datewise", 1 ) ;
		$field_array_machine_dtls_datewise="id, mst_id, dtls_id, machine_id, distribution_date, fraction_date, days_complete, qnty";
		
		$save_data=str_replace("'","",$save_data);
		if($save_data!="")
		{
			$save_data=explode(",",$save_data);
			for($i=0;$i<count($save_data);$i++)
			{
				$machine_wise_data=explode("_",$save_data[$i]);	
				$machine_id=$machine_wise_data[0];
				$dia=$machine_wise_data[1];
				$capacity=$machine_wise_data[2];
				$qnty=$machine_wise_data[3];
				$noOfDays=$machine_wise_data[4];
				
				$dateWise_qnty=0; $bl_qnty=$qnty;
				
				if($machine_wise_data[5]!="") $startDate=date("Y-m-d",strtotime($machine_wise_data[5]));
				if($machine_wise_data[6]!="") $endDate=date("Y-m-d",strtotime($machine_wise_data[6]));
				
				if($startDate!="" && $endDate!="")
				{
					$sCurrentDate=date("Y-m-d",strtotime("-1 day", strtotime($startDate))); $days=$noOfDays; $fraction=0; $days_complete=0;
					while($sCurrentDate < $endDate)
					{ 
						$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate))); 
						if($days>=1)
						{
							$fraction=0;
							$days_complete=1;
							$dateWise_qnty=$capacity;
						}
						else 
						{
							$fraction=1;
							$days_complete=$days;
							$dateWise_qnty=$bl_qnty;
						}
						
						$days=$days-1;
						$bl_qnty=$bl_qnty-$capacity;
						
						if($db_type==0) $curr_date=$sCurrentDate; else $curr_date=change_date_format($sCurrentDate,'','',1);  
						
						if($data_array_machine_dtls_datewise!="") $data_array_machine_dtls_datewise.=",";
						$data_array_machine_dtls_datewise.="(".$machine_dtls_datewise_id.",".$id.",".$dtls_id.",'".$machine_id."','".$curr_date."','".$fraction."','".$days_complete."','".$dateWise_qnty."')"; 
						$machine_dtls_datewise_id=$machine_dtls_datewise_id+1;
					}
				}
				
				if($db_type==0)
				{
					$mstartDate=$startDate;
					$mendDate=$endDate;
				}
				else
				{
					$mstartDate=change_date_format($startDate,'','',1);
					$mendDate=change_date_format($endDate,'','',1);
				}
				
				if($data_array_machine_dtls!="") $data_array_machine_dtls.=",";
				$data_array_machine_dtls.="(".$machine_dtls_id.",".$id.",".$dtls_id.",'".$machine_id."','".$dia."','".$capacity."','".$qnty."','".$noOfDays."','".$mstartDate."','".$mendDate."',".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')"; 
				$machine_dtls_id=$machine_dtls_id+1;
			}
	
			/*if($data_array_machine_dtls!="")
			{
				//echo "10**insert into ppl_planning_info_machine_dtls (".$field_array_machine_dtls.") Values ".$data_array_machine_dtls."";die;
				$rID3=sql_insert("ppl_planning_info_machine_dtls",$field_array_machine_dtls,$data_array_machine_dtls,0);
				if($flag==1) 
				{
					if($rID3) $flag=1; else $flag=0; 
				}  
			}
			
			if($data_array_machine_dtls_datewise!="")
			{
				//echo "10**insert into ppl_entry_machine_datewise (".$field_array_machine_dtls_datewise.") Values ".$data_array_machine_dtls_datewise."";die;
				$rID4=sql_insert("ppl_entry_machine_datewise",$field_array_machine_dtls_datewise,$data_array_machine_dtls_datewise,0);
				if($flag==1) 
				{
					if($rID4) $flag=1; else $flag=0; 
				}  
			}*/
		}
		
		$feeder_dtls_id=return_next_id( "id","ppl_planning_feeder_dtls", 1 ) ;
		$field_array_feeder_dtls="id, mst_id, dtls_id, pre_cost_id, color_id, stripe_color_id, no_of_feeder, inserted_by, insert_date";
		
		$hidden_no_of_feeder_data=str_replace("'","",$hidden_no_of_feeder_data);
		if($hidden_no_of_feeder_data!="")
		{
			$hidden_no_of_feeder_data=explode(",",$hidden_no_of_feeder_data);
			for($i=0;$i<count($hidden_no_of_feeder_data);$i++)
			{
				$color_wise_data=explode("_",$hidden_no_of_feeder_data[$i]);	
				$pre_cost_id=$color_wise_data[0];
				$color_id=$color_wise_data[1];
				$stripe_color_id=$color_wise_data[2];
				$no_of_feeder=$color_wise_data[3];
				
				if($data_array_feeder_dtls!="") $data_array_feeder_dtls.=",";
					
				$data_array_feeder_dtls.="(".$feeder_dtls_id.",".$id.",".$dtls_id.",'".$pre_cost_id."','".$color_id."','".$stripe_color_id."','".$no_of_feeder."',".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')"; 
				
				$feeder_dtls_id=$feeder_dtls_id+1;
			}
	
			/*if($data_array_feeder_dtls!="")
			{
				//echo "10**insert into ppl_planning_feeder_dtls (".$field_array_feeder_dtls.") Values ".$data_array_feeder_dtls."";die;
				$rID5=sql_insert("ppl_planning_feeder_dtls",$field_array_feeder_dtls,$data_array_feeder_dtls,0);
				if($flag==1) 
				{
					if($rID5) $flag=1; else $flag=0; 
				}  
			}*/
		}
		
		if(str_replace("'",'',$updateId)=="")
		{
			$rID=sql_insert("ppl_planning_info_entry_mst",$field_array,$data_array,0);
			if($rID) $flag=1; else $flag=0;
		}
		else
		{
			$flag=1;
		}
		
		//echo "insert into pro_recipe_entry_dtls (".$field_array_dtls.") Values ".$data_array_dtls."";die;
		$rID2=sql_insert("ppl_planning_info_entry_dtls",$field_array_dtls,$data_array_dtls,1);
		if($flag==1) 
		{
			if($rID2) $flag=1; else $flag=0; 
		} 
		
		if($data!="")
		{
			if($data_array_plan_dtls!="")
			{
				//echo"insert into ppl_planning_entry_plan_dtls (".$field_array_plan_dtls.") Values ".$data_array_plan_dtls."";die;
				$rIDdtls=sql_insert("ppl_planning_entry_plan_dtls",$field_array_plan_dtls,$data_array_plan_dtls,0);
				if($flag==1) 
				{
					if($rIDdtls) $flag=1; else $flag=0; 
				}  
			}
		}
		
		if($save_data!="")
		{
			if($data_array_machine_dtls!="")
			{
				//echo "10**insert into ppl_planning_info_machine_dtls (".$field_array_machine_dtls.") Values ".$data_array_machine_dtls."";die;
				$rID3=sql_insert("ppl_planning_info_machine_dtls",$field_array_machine_dtls,$data_array_machine_dtls,0);
				if($flag==1) 
				{
					if($rID3) $flag=1; else $flag=0; 
				}  
			}
			
			if($data_array_machine_dtls_datewise!="")
			{
				//echo "10**insert into ppl_entry_machine_datewise (".$field_array_machine_dtls_datewise.") Values ".$data_array_machine_dtls_datewise."";die;
				$rID4=sql_insert("ppl_entry_machine_datewise",$field_array_machine_dtls_datewise,$data_array_machine_dtls_datewise,0);
				if($flag==1) 
				{
					if($rID4) $flag=1; else $flag=0; 
				}  
			}
		}
		
		if($hidden_no_of_feeder_data!="")
		{
			if($data_array_feeder_dtls!="")
			{
				//echo "10**insert into ppl_planning_feeder_dtls (".$field_array_feeder_dtls.") Values ".$data_array_feeder_dtls."";die;
				$rID5=sql_insert("ppl_planning_feeder_dtls",$field_array_feeder_dtls,$data_array_feeder_dtls,0);
				if($flag==1) 
				{
					if($rID5) $flag=1; else $flag=0; 
				}  
			} 
		}
		
		if($db_type==0)
		{
			if($flag==1)
			{
				mysql_query("COMMIT");  
				echo "0**".$id."**0";
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "5**0**0";
			}
		}
		else if($db_type==2 || $db_type==1 )
		{
			if($flag==1)
			{
				oci_commit($con);
				echo "0**".$id."**0";
			}
			else
			{
				oci_rollback($con);
				echo "5**0**0";
			}
		}
		disconnect($con);
		die;
	}
	else if ($operation==1)   // Update Here
	{ 
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
		$color_id=0;
		$field_array_update="knitting_source*knitting_party*color_id*color_range*machine_dia*width_dia_type*machine_gg*fabric_dia*program_qnty*stitch_length*spandex_stitch_length*draft_ratio*machine_id*machine_capacity*distribution_qnty*status*start_date*end_date*program_date*feeder*remarks*save_data*no_fo_feeder_data*updated_by*update_date";
		
		$data_array_update=$cbo_knitting_source."*".$cbo_knitting_party."*".$hidden_color_id."*".$cbo_color_range."*".$txt_machine_dia."*".$cbo_dia_width_type."*".$txt_machine_gg."*".$txt_fabric_dia."*".$txt_program_qnty."*".$txt_stitch_length."*".$txt_spandex_stitch_length."*".$txt_draft_ratio."*".$machine_id."*".$txt_machine_capacity."*".$txt_distribution_qnty."*".$cbo_knitting_status."*".$txt_start_date."*".$txt_end_date."*".$txt_program_date."*".$cbo_feeder."*".$txt_remarks."*".$save_data."*".$hidden_no_of_feeder_data."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
		
		/*$delete=execute_query( "delete from ppl_planning_entry_plan_dtls where dtls_id=$update_dtls_id",0);
		if($delete) $flag=1; else $flag=0;*/
		
		$plan_dtls_id=return_next_id( "id","ppl_planning_entry_plan_dtls", 1 ) ;
		
		$field_array_plan_dtls="id, mst_id, dtls_id, company_id, buyer_id, booking_no, po_id, start_date, finish_date, body_part_id, color_type_id, determination_id, fabric_desc, gsm_weight, dia, width_dia_type, color_id, yarn_desc, program_qnty, inserted_by, insert_date";
		
		$data=str_replace("'","",$data);
		if($data!="")
		{
			$data=explode("_",$data);
			for($i=0;$i<count($data);$i++)
			{
				$plan_data=explode("**",$data[$i]);	
				$booking_no=$plan_data[0];
				$start_date=$plan_data[1];
				$end_date=$plan_data[2];
				$po_id=$plan_data[3];
				$buyer_id=$plan_data[4];
				$body_part_id=$plan_data[5];
				$dia_width_type=$plan_data[6];
				$pre_cost_id=$plan_data[7];
				$desc=$plan_data[8];
				$gsm_weight=$plan_data[9];
				$dia_width=$plan_data[10];
				$determination_id=$plan_data[11];
				$booking_qnty=$plan_data[12];
				$color_type_id=$plan_data[13];
				
				$perc=($booking_qnty/$tot_booking_qnty)*100;
				$prog_qnty=($perc*str_replace("'",'',$txt_program_qnty))/100;
				
				if($data_array_plan_dtls!="") $data_array_plan_dtls.=",";
					
				$data_array_plan_dtls.="(".$plan_dtls_id.",".$updateId.",".$update_dtls_id.",".$companyID.",".$buyer_id.",'".$booking_no."',".$po_id.",'".$start_date."','".$end_date."',".$body_part_id.",".$color_type_id.",".$determination_id.",'".$desc."',".$gsm.",'".$dia."',".$dia_width_type.",0,".$pre_cost_id.",".$prog_qnty.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
				
				$plan_dtls_id=$plan_dtls_id+1;
			}
	
			/*if($data_array_plan_dtls!="")
			{
				//echo"insert into ppl_planning_entry_plan_dtls (".$field_array_plan_dtls.") Values ".$data_array_plan_dtls."";die;
				$rID2=sql_insert("ppl_planning_entry_plan_dtls",$field_array_plan_dtls,$data_array_plan_dtls,0);
				if($flag==1) 
				{
					if($rID2) $flag=1; else $flag=0; 
				}  
			}*/
		}
		
		/*$deletem=execute_query( "delete from ppl_planning_info_machine_dtls where dtls_id=$update_dtls_id",0);
		if($flag==1) 
		{
			if($deletem) $flag=1; else $flag=0; 
		}
		
		$delete_datewise=execute_query( "delete from ppl_entry_machine_datewise where dtls_id=$update_dtls_id",0);
		if($flag==1) 
		{
			if($delete_datewise) $flag=1; else $flag=0; 
		}*/
		
		$machine_dtls_id=return_next_id( "id","ppl_planning_info_machine_dtls", 1 ) ;
		/*$field_array_machine_dtls="id, mst_id, dtls_id, machine_id, dia, capacity, distribution_qnty, inserted_by, insert_date";
		
		$save_data=str_replace("'","",$save_data);
		if($save_data!="")
		{
			$save_data=explode(",",$save_data);
			for($i=0;$i<count($save_data);$i++)
			{
				$machine_wise_data=explode("_",$save_data[$i]);	
				$machine_id=$machine_wise_data[0];
				$dia=$machine_wise_data[1];
				$capacity=$machine_wise_data[2];
				$qnty=$machine_wise_data[3];
				
				if($data_array_machine_dtls!="") $data_array_machine_dtls.=",";
					
				$data_array_machine_dtls.="(".$machine_dtls_id.",".$updateId.",".$update_dtls_id.",'".$machine_id."','".$dia."','".$capacity."','".$qnty."',".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')"; 
				
				$machine_dtls_id=$machine_dtls_id+1;
			}
	
			if($data_array_machine_dtls!="")
			{
				//echo"insert into ppl_planning_info_machine_dtls (".$field_array_machine_dtls.") Values ".$data_array_machine_dtls."";die;
				$rID3=sql_insert("ppl_planning_info_machine_dtls",$field_array_machine_dtls,$data_array_machine_dtls,1);
				if($flag==1) 
				{
					if($rID3) $flag=1; else $flag=0; 
				} 
			}
		}
		*/
		$field_array_machine_dtls="id, mst_id, dtls_id, machine_id, dia, capacity, distribution_qnty, no_of_days, start_date, end_date, inserted_by, insert_date";
		
		$machine_dtls_datewise_id=return_next_id( "id","ppl_entry_machine_datewise", 1 ) ;
		$field_array_machine_dtls_datewise="id, mst_id, dtls_id, machine_id, distribution_date, fraction_date, days_complete, qnty";
		
		$save_data=str_replace("'","",$save_data);
		if($save_data!="")
		{
			$save_data=explode(",",$save_data);
			for($i=0;$i<count($save_data);$i++)
			{
				$machine_wise_data=explode("_",$save_data[$i]);	
				$machine_id=$machine_wise_data[0];
				$dia=$machine_wise_data[1];
				$capacity=$machine_wise_data[2];
				$qnty=$machine_wise_data[3];
				$noOfDays=$machine_wise_data[4];
				
				$dateWise_qnty=0; $bl_qnty=$qnty;
				
				if($machine_wise_data[5]!="") $startDate=date("Y-m-d",strtotime($machine_wise_data[5]));
				if($machine_wise_data[6]!="") $endDate=date("Y-m-d",strtotime($machine_wise_data[6]));
				
				if($startDate!="" && $endDate!="")
				{
					$sCurrentDate=date("Y-m-d",strtotime("-1 day", strtotime($startDate))); $days=$noOfDays; $fraction=0; $days_complete=0;
					
					while($sCurrentDate < $endDate)
					{  
						$sCurrentDate = date("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate))); 
						
						if($days>=1)
						{
							$fraction=0;
							$days_complete=1;
							$dateWise_qnty=$capacity;
						}
						else 
						{
							$fraction=1;
							$days_complete=$days;
							$dateWise_qnty=$bl_qnty;
						}

						$days=$days-1;
						$bl_qnty=$bl_qnty-$capacity;
						
						if($db_type==0) $curr_date=$sCurrentDate; else $curr_date=change_date_format($sCurrentDate,'','',1); 
						
						if($data_array_machine_dtls_datewise!="") $data_array_machine_dtls_datewise.=",";
						
						$data_array_machine_dtls_datewise.="(".$machine_dtls_datewise_id.",".$updateId.",".$update_dtls_id.",'".$machine_id."','".$curr_date."','".$fraction."','".$days_complete."','".$dateWise_qnty."')"; 
						$machine_dtls_datewise_id=$machine_dtls_datewise_id+1;
					}
				}
				
				if($db_type==0)
				{
					$mstartDate=$startDate;
					$mendDate=$endDate;
				}
				else
				{
					$mstartDate=change_date_format($startDate,'','',1);
					$mendDate=change_date_format($endDate,'','',1);
				}
				
				if($data_array_machine_dtls!="") $data_array_machine_dtls.=",";
				$data_array_machine_dtls.="(".$machine_dtls_id.",".$updateId.",".$update_dtls_id.",'".$machine_id."','".$dia."','".$capacity."','".$qnty."','".$noOfDays."','".$mstartDate."','".$mendDate."',".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')"; 
				$machine_dtls_id=$machine_dtls_id+1;
			}
	
			/*if($data_array_machine_dtls!="")
			{
				//echo"insert into ppl_planning_info_machine_dtls (".$field_array_machine_dtls.") Values ".$data_array_machine_dtls."";die;
				$rID3=sql_insert("ppl_planning_info_machine_dtls",$field_array_machine_dtls,$data_array_machine_dtls,0);
				if($flag==1) 
				{
					if($rID3) $flag=1; else $flag=0; 
				}  
			}
			
			if($data_array_machine_dtls_datewise!="")
			{
				//echo "10**insert into ppl_entry_machine_datewise (".$field_array_machine_dtls_datewise.") Values ".$data_array_machine_dtls_datewise."";die;
				$rID4=sql_insert("ppl_entry_machine_datewise",$field_array_machine_dtls_datewise,$data_array_machine_dtls_datewise,0);
				if($flag==1) 
				{
					if($rID4) $flag=1; else $flag=0; 
				} 
			}*/
			//echo "10**".$flag;die;
		}
		
		/*$delete_feeder=execute_query( "delete from ppl_planning_feeder_dtls where dtls_id=$update_dtls_id",0);
		if($flag==1) 
		{
			if($delete_feeder) $flag=1; else $flag=0; 
		}*/
		
		$feeder_dtls_id=return_next_id( "id","ppl_planning_feeder_dtls", 1 ) ;
		$field_array_feeder_dtls="id, mst_id, dtls_id, pre_cost_id, color_id, stripe_color_id, no_of_feeder, inserted_by, insert_date";
		
		$hidden_no_of_feeder_data=str_replace("'","",$hidden_no_of_feeder_data);
		if($hidden_no_of_feeder_data!="")
		{
			$hidden_no_of_feeder_data=explode(",",$hidden_no_of_feeder_data);
			for($i=0;$i<count($hidden_no_of_feeder_data);$i++)
			{
				$color_wise_data=explode("_",$hidden_no_of_feeder_data[$i]);	
				$pre_cost_id=$color_wise_data[0];
				$color_id=$color_wise_data[1];
				$stripe_color_id=$color_wise_data[2];
				$no_of_feeder=$color_wise_data[3];
				
				if($data_array_feeder_dtls!="") $data_array_feeder_dtls.=",";
					
				$data_array_feeder_dtls.="(".$feeder_dtls_id.",".$updateId.",".$update_dtls_id.",'".$pre_cost_id."','".$color_id."','".$stripe_color_id."','".$no_of_feeder."',".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')"; 
				
				$feeder_dtls_id=$feeder_dtls_id+1;
			}
	
			/*if($data_array_feeder_dtls!="")
			{
				//echo "10**insert into ppl_planning_feeder_dtls (".$field_array_feeder_dtls.") Values ".$data_array_feeder_dtls."";die;
				$rID5=sql_insert("ppl_planning_feeder_dtls",$field_array_feeder_dtls,$data_array_feeder_dtls,0);
				if($flag==1) 
				{
					if($rID5) $flag=1; else $flag=0; 
				}  
			}*/
		}
		
		//Query Execution Start
		$delete=execute_query( "delete from ppl_planning_entry_plan_dtls where dtls_id=$update_dtls_id",0);
		if($delete) $flag=1; else $flag=0;
		
		$rID=sql_update("ppl_planning_info_entry_dtls",$field_array_update,$data_array_update,"id",$update_dtls_id,1);
		if($flag==1) 
		{
			if($rID) $flag=1; else $flag=0; 
		} 
		
		if($data!="")
		{
			if($data_array_plan_dtls!="")
			{
				//echo"insert into ppl_planning_entry_plan_dtls (".$field_array_plan_dtls.") Values ".$data_array_plan_dtls."";die;
				$rID2=sql_insert("ppl_planning_entry_plan_dtls",$field_array_plan_dtls,$data_array_plan_dtls,0);
				if($flag==1) 
				{
					if($rID2) $flag=1; else $flag=0; 
				}  
			}
		}
		
		$deletem=execute_query( "delete from ppl_planning_info_machine_dtls where dtls_id=$update_dtls_id",0);
		if($flag==1) 
		{
			if($deletem) $flag=1; else $flag=0; 
		}
		
		$delete_datewise=execute_query( "delete from ppl_entry_machine_datewise where dtls_id=$update_dtls_id",0);
		if($flag==1) 
		{
			if($delete_datewise) $flag=1; else $flag=0; 
		}
		
		if($save_data!="")
		{
			if($data_array_machine_dtls!="")
			{
				//echo"insert into ppl_planning_info_machine_dtls (".$field_array_machine_dtls.") Values ".$data_array_machine_dtls."";die;
				$rID3=sql_insert("ppl_planning_info_machine_dtls",$field_array_machine_dtls,$data_array_machine_dtls,0);
				if($flag==1) 
				{
					if($rID3) $flag=1; else $flag=0; 
				}  
			}
			
			if($data_array_machine_dtls_datewise!="")
			{
				//echo "10**insert into ppl_entry_machine_datewise (".$field_array_machine_dtls_datewise.") Values ".$data_array_machine_dtls_datewise."";die;
				$rID4=sql_insert("ppl_entry_machine_datewise",$field_array_machine_dtls_datewise,$data_array_machine_dtls_datewise,0);
				if($flag==1) 
				{
					if($rID4) $flag=1; else $flag=0; 
				} 
			}
		}
		
		$delete_feeder=execute_query( "delete from ppl_planning_feeder_dtls where dtls_id=$update_dtls_id",0);
		if($flag==1) 
		{
			if($delete_feeder) $flag=1; else $flag=0; 
		}
		
		if($hidden_no_of_feeder_data!="")
		{
			if($data_array_feeder_dtls!="")
			{
				//echo "10**insert into ppl_planning_feeder_dtls (".$field_array_feeder_dtls.") Values ".$data_array_feeder_dtls."";die;
				$rID5=sql_insert("ppl_planning_feeder_dtls",$field_array_feeder_dtls,$data_array_feeder_dtls,0);
				if($flag==1) 
				{
					if($rID5) $flag=1; else $flag=0; 
				}  
			}
		}
		
		if($db_type==0)
		{
			if($flag==1)
			{
				mysql_query("COMMIT");  
				echo "1**".str_replace("'","",$updateId)."**0";
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "6**0**1";
			}
		}
		else if($db_type==2 || $db_type==1 )
		{
			if($flag==1)
			{
				oci_commit($con);  
				echo "1**".str_replace("'","",$updateId)."**0";
			}
			else
			{
				oci_rollback($con); 
				echo "6**0**1";
			}
		}
		disconnect($con);
		die;
	}
	else if ($operation==2)
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
		$field_array_update="status_active*is_deleted*updated_by*update_date";
		
		$data_array_update="0*1*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
		
		$rID=sql_update("ppl_planning_info_entry_dtls",$field_array_update,$data_array_update,"id",$update_dtls_id,0);
		if($rID) $flag=1; else $flag=0;
		
		$rID2=sql_update("ppl_planning_entry_plan_dtls",$field_array_update,$data_array_update,"dtls_id",$update_dtls_id,0);
		if($flag==1)
		{
			if($rID2) $flag=1; else $flag=0; 
		}
		
		$delete=execute_query( "delete from ppl_planning_info_machine_dtls where dtls_id=$update_dtls_id",0);
		if($flag==1) 
		{
			if($delete) $flag=1; else $flag=0; 
		}
		
		$delete_datewise=execute_query( "delete from ppl_entry_machine_datewise where dtls_id=$update_dtls_id",0);
		if($flag==1) 
		{
			if($delete_datewise) $flag=1; else $flag=0; 
		}
		
		$delete_feeder=execute_query( "delete from ppl_planning_feeder_dtls where dtls_id=$update_dtls_id",1);
		if($flag==1) 
		{
			if($delete_feeder) $flag=1; else $flag=0; 
		}
		
		if($db_type==0)
		{
			if($flag==1)
			{
				mysql_query("COMMIT");  
				echo "2**".str_replace("'","",$updateId)."**0";
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "7**0**1";
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
			if($flag==1)
			{
				oci_commit($con);
				echo "2**".str_replace("'","",$updateId)."**0";
			}
			else
			{
				oci_rollback($con);
				echo "7**0**1";
			}
		}
		disconnect($con);
		die;
	}
}

?>