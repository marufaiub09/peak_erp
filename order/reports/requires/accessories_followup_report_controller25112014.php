<?
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../../includes/common.php');

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

//--------------------------------------------------------------------------------------------------------------------
$company_library=return_library_array( "select id, company_name from lib_company", "id", "company_name"  );
$buyer_short_name_library=return_library_array( "select id, short_name from lib_buyer", "id", "short_name"  );
$costing_per_id_library=return_library_array( "select job_no, costing_per from wo_pre_cost_mst", "job_no", "costing_per");
$item_library=return_library_array( "select id, item_name from lib_item_group", "id", "item_name"  );
$color_name_library=return_library_array( "select id, color_name from lib_color", "id", "color_name"  );
$country_name_library=return_library_array( "select id, country_name from lib_country", "id", "country_name"  );
$order_arr=return_library_array( "select id, po_number from wo_po_break_down", "id", "po_number"  );

if($action=="load_drop_down_buyer")
{
	echo create_drop_down( "cbo_buyer_name", 160, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name","id,buyer_name", 1, "-- All Buyer --", $selected, "" );   	 
	exit();
}

$tmplte=explode("**",$data);

if ($tmplte[0]=="viewtemplate") $template=$tmplte[1]; else $template=$lib_report_template_array[$_SESSION['menu_id']]['0'];
if ($template=="") $template=1;


if($action=="report_generate")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	$company_name=str_replace("'","",$cbo_company_name);
	//if(str_replace("'","",$cbo_buyer_name)==0) $buyer_name="%%"; else $buyer_name=str_replace("'","",$cbo_buyer_name);
	if(str_replace("'","",$cbo_buyer_name)==0)
	{
		if ($_SESSION['logic_erp']["data_level_secured"]==1)
		{
			if($_SESSION['logic_erp']["buyer_id"]!="") $buyer_id_cond=" and a.buyer_name in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_id_cond="";
		}
		else
		{
			$buyer_id_cond="";
		}
	}
	else
	{
		$buyer_id_cond=" and a.buyer_name=$cbo_buyer_name";//.str_replace("'","",$cbo_buyer_name)
	}
	
	if(str_replace("'","",$cbo_item_group)=="")
	{
		$item_group_cond="";
	}
	else
	{
		$item_group_cond="and a.trim_group in(".str_replace("'","",$cbo_item_group).")";
	}
	
	//echo $item_group_cond;die;
	//print $company; $txt_pay_date=date("j-M-Y",strtotime($txt_pay_date));
	$date_cond='';
	if(str_replace("'","",$txt_date_from)!="" && str_replace("'","",$txt_date_to)!="")
	{
		$start_date=(str_replace("'","",$txt_date_from));
		$end_date=(str_replace("'","",$txt_date_to));
		/*if($db_type==0)
		{
			$start_date=change_date_format(str_replace("'","",$txt_date_from),"yyyy-mm-dd","");
			$end_date=change_date_format(str_replace("'","",$txt_date_to),"yyyy-mm-dd","");
		}
		else
		{
			$start_date=date("j-M-Y",strtotime(str_replace("'","",$txt_date_from)));
			$end_date=date("j-M-Y",strtotime(str_replace("'","",$txt_date_to)));
		}*/
		$date_cond=" and b.pub_shipment_date between '$start_date' and '$end_date'";
		//$year_cond_pre=" and to_char(d.insert_date,'mm-dd-yyyy')  between '$start_date' and '$end_date'";
		$year_cond_pre=" and d.insert_date between '".$start_date."' and '".$end_date."'";
	}
	if(str_replace("'","",$cbo_search_date)==2 )
	{
	$date_cond='';
	if(str_replace("'","",$txt_date_from)!="" && str_replace("'","",$txt_date_to)!="")
	{
		$start_date=(str_replace("'","",$txt_date_from));
		$end_date=(str_replace("'","",$txt_date_to));
		/*if($db_type==0)
		{
			$start_date=change_date_format(str_replace("'","",$txt_date_from),"yyyy-mm-dd","");
			$end_date=change_date_format(str_replace("'","",$txt_date_to),"yyyy-mm-dd","");
		}
		else
		{
			$start_date=date("j-M-Y",strtotime(str_replace("'","",$txt_date_from)));
			$end_date=date("j-M-Y",strtotime(str_replace("'","",$txt_date_to)));
		}*/	
		
		//$year_cond_pre=" and to_char(d.insert_date,'YYYY')  between '$start_date' and '$end_date'";
	}	
	}
	if(str_replace("'","",$cbo_search_date)==2)
	{
		if($db_type==0) $insert_year_cond=" YEAR(d.insert_date) as pre_date";
		if($db_type==2) $insert_year_cond=" d.insert_date as pre_date";
	}
	//condition add
	if($db_type==0)
	{
		if(str_replace("'","",$cbo_year)!=0) $year_cond=" and year(a.insert_date)=".str_replace("'","",$cbo_year).""; else $year_cond="";
	}
	else
	{
		
		if(str_replace("'","",$cbo_year)!=0) $year_cond=" and to_char(a.insert_date,'YYYY')=".str_replace("'","",$cbo_year).""; else $year_cond="";
		if(str_replace("'","",$cbo_year)!=0) $year_cond_p=" and to_char(d.insert_date,'YYYY')=".str_replace("'","",$cbo_year).""; else $year_cond_p="";
	}
	
	if (str_replace("'","",$txt_job_no)=="") $job_no_cond=""; else $job_no_cond=" and a.job_no_prefix_num in (".str_replace("'","",$txt_job_no).") ";
	if(str_replace("'","",$txt_style_ref)!="") $style_ref_cond=" and a.style_ref_no like '%".str_replace("'","",$txt_style_ref)."%'"; else $style_ref_cond="";
	
	
	$serch_by=str_replace("'","",$cbo_search_by);
  if(str_replace("'","",$cbo_search_by)==1)
  {
	
	$wo_qty_array=array();
	$wo_qty_summary_array=array();$conversion_factor_array=array();
	if($db_type==2)
	{
	$wo_sql="select min(a.booking_date) as booking_date ,b.job_no,LISTAGG(CAST(a.booking_no AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY a.booking_no) as booking_no, b.po_break_down_id, b.trim_group,b.pre_cost_fabric_cost_dtls_id, sum(b.wo_qnty) as wo_qnty,sum(b.amount) as amount,sum(b.rate) as rate from wo_booking_mst a, wo_booking_dtls b 
	where a.item_category=4 and a.booking_no=b.booking_no and a.is_deleted=0 and a.status_active=1 and b.status_active=1 and b.is_deleted=0 and a.company_id=$company_name group by b.po_break_down_id,b.trim_group,b.job_no,b.pre_cost_fabric_cost_dtls_id";
	}
	else if($db_type==0)
	{
	$wo_sql="select min(a.booking_date) as booking_date ,b.job_no,group_concat(a.booking_no) as booking_no, b.po_break_down_id, b.trim_group,b.pre_cost_fabric_cost_dtls_id,sum(b.wo_qnty) as wo_qnty,sum(b.amount) as amount,sum(b.rate) as rate from wo_booking_mst a, wo_booking_dtls b 
	where a.item_category=4 and a.booking_no=b.booking_no and a.is_deleted=0 and a.status_active=1 and b.status_active=1 and b.is_deleted=0 and a.company_id=$company_name group by b.po_break_down_id,b.trim_group,b.job_no,b.pre_cost_fabric_cost_dtls_id";
	}
	//echo $wo_sql;die;
	$dataArray=sql_select($wo_sql);
	foreach($dataArray as $row )
	{
		
		$wo_qty_array[$row[csf('po_break_down_id')]][$row[csf('trim_group')]]['booking_no']=$row[csf('booking_no')];
		$wo_qty_array[$row[csf('po_break_down_id')]][$row[csf('trim_group')]][$row[csf('pre_cost_fabric_cost_dtls_id')]]['wo_qnty']=$row[csf('wo_qnty')];
		$wo_qty_array[$row[csf('po_break_down_id')]][$row[csf('trim_group')]]['rate']=$row[csf('rate')];
		$wo_qty_summary_array[$row[csf('trim_group')]]['wo_qnty']=$row[csf('wo_qnty')];
		$wo_qty_array[$row[csf('po_break_down_id')]][$row[csf('trim_group')]]['amount']=$row[csf('amount')];
		$wo_qty_array[$row[csf('po_break_down_id')]][$row[csf('trim_group')]]['wo_date']=$row[csf('booking_date')];
		//var_dump($wo_qty_array);die;	3693=2
	}
	
	$conversion_factor=sql_select("select id ,conversion_factor from  lib_item_group  ");
	foreach($conversion_factor as $row_f)
	{
	$conversion_factor_array[$row_f[csf('id')]]['con_factor']=$row_f[csf('conversion_factor')];
	}
	//var_dump($conversion_factor_array);
	if($db_type==2)
	{
$trimsArray=sql_select("select  b.po_break_down_id,b.id,a.trim_group,a.insert_date as pre_date, a.cons_uom, a.apvl_req, a.brand_sup_ref, a.rate, b.cons,b.country_id,a.id as trim_dtls_id
	from wo_pre_cost_trim_cost_dtls a, wo_pre_cost_trim_co_cons_dtls b 
	where a.id=b.wo_pre_cost_trim_cost_dtls_id and a.job_no=b.job_no and a.status_active=1 and  a.is_deleted=0  $item_group_cond group by  b.po_break_down_id,a.trim_group,b.id,a.cons_uom, a.apvl_req, a.brand_sup_ref, a.rate,b.cons,b.country_id,a.insert_date,a.id 
	
	union
	select  a.po_break_down_id,a.id,a.trim_group,null,null,null,null,null,null,null,null from wo_booking_mst b,wo_booking_dtls a where b.booking_no=a.booking_no and a.job_no=b.job_no and b.item_from_precost=2 and b.booking_type=2  $item_group_cond group by a.po_break_down_id,a.id,a.trim_group
	union
	select b.po_breakdown_id,c.id as prod_id,a.item_group_id as trim_group,null,null,null,null,null,null,null,null from  inv_receive_master d,inv_trims_entry_dtls a ,order_wise_pro_details b,product_details_master c where d.id=a.mst_id and a.trans_id=b.trans_id and b.trans_type=1 and b.entry_form=24 and d.receive_basis in (1,4,6) and a.prod_id=c.id and c.id=b.prod_id and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by b.po_breakdown_id,a.item_group_id,c.id
	 ");
  	}
	else if($db_type==0)
	{
$trimsArray=sql_select("select  b.po_break_down_id,b.id,a.trim_group,a.insert_date as pre_date, a.cons_uom, a.apvl_req, a.brand_sup_ref, a.rate, b.cons,b.country_id,a.id as trim_dtls_id 
	from wo_pre_cost_trim_cost_dtls a, wo_pre_cost_trim_co_cons_dtls b 
	where a.id=b.wo_pre_cost_trim_cost_dtls_id and a.job_no=b.job_no and a.status_active=1 and  a.is_deleted=0  $item_group_cond group by  b.po_break_down_id,a.trim_group,b.id,a.cons_uom, a.apvl_req, a.brand_sup_ref, a.rate, b.cons,b.country_id,a.insert_date,a.id 
	
	union
	select  a.po_break_down_id,a.id,a.trim_group,'','','','','','','','' from wo_booking_mst b,wo_booking_dtls a where b.booking_no=a.booking_no and a.job_no=b.job_no and b.item_from_precost=2 and b.booking_type=2  $item_group_cond group by a.po_break_down_id,a.id,a.trim_group
	union
	select b.po_breakdown_id,c.id as prod_id,a.item_group_id as trim_group,'','','','','','','','' from  inv_receive_master d,inv_trims_entry_dtls a ,order_wise_pro_details b,product_details_master c where d.id=a.mst_id and a.trans_id=b.trans_id and b.trans_type=1 and b.entry_form=24 and d.receive_basis in (1,4,6) and a.prod_id=c.id and c.id=b.prod_id and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by b.po_breakdown_id,a.item_group_id,c.id
	 ");
  	}
	$reference_arr=array();
	foreach($trimsArray as $row)
	{
		
		$reference_arr[$row[csf('po_break_down_id')]][$row[csf('id')]]['id']=$row[csf('id')];
		$reference_arr[$row[csf('po_break_down_id')]][$row[csf('id')]]['po_break_down_id']=$row[csf('po_break_down_id')];
		$reference_arr[$row[csf('po_break_down_id')]][$row[csf('id')]]['trim_group']=$row[csf('trim_group')];
		$reference_arr[$row[csf('po_break_down_id')]][$row[csf('id')]]['pre_date']=$row[csf('pre_date')];
		$reference_arr[$row[csf('po_break_down_id')]][$row[csf('id')]]['cons_uom']=$row[csf('cons_uom')];
		$reference_arr[$row[csf('po_break_down_id')]][$row[csf('id')]]['apvl_req']=$row[csf('apvl_req')];
		$reference_arr[$row[csf('po_break_down_id')]][$row[csf('id')]]['brand_sup_ref']=$row[csf('brand_sup_ref')];
		$reference_arr[$row[csf('po_break_down_id')]][$row[csf('id')]]['rate']=$row[csf('rate')];
		$reference_arr[$row[csf('po_break_down_id')]][$row[csf('id')]]['cons']=$row[csf('cons')];
		$reference_arr[$row[csf('po_break_down_id')]][$row[csf('id')]]['country_id']=$row[csf('country_id')];
		//$reference_arr[$row[csf('po_break_down_id')]][$row[csf('id')]][$row[csf('trim_dtls_id')]]=$row[csf('trim_dtls_id')];
	} //var_dump($reference_arr);die;
	
	$app_sql=sql_select("select po_break_down_id,accessories_type_id,approval_status from wo_po_trims_approval_info");
	$app_status_arr=array();
	foreach($app_sql as $row)
	{
		$app_status_arr[$row[csf("po_break_down_id")]][$row[csf("accessories_type_id")]]=$row[csf("approval_status")];
	}
	
	//$inhouse_qnty=return_field_value("sum(a.cons_qnty)","inv_receive_master b, inv_trims_entry_dtls a","b.id=a.mst_id and a.item_group_id='$trim_id' and a.order_id='$order_id' and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and b.item_category=4");
	/*if($db_type==2)
	{
		$inhouse_qnty_sql=sql_select( " select a.item_group_id,LISTAGG(CAST(a.order_id AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY a.order_id) as order_id,sum(a.cons_qnty) as  cons_qnty from inv_receive_master b, inv_trims_entry_dtls a where b.id=a.mst_id and b.entry_form=24 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and b.item_category=4 group by  a.item_group_id");
	}
	else if($db_type==0)
	{
		$inhouse_qnty_sql=sql_select( " select a.item_group_id,group_concat(a.order_id) as order_id,sum(a.cons_qnty) as  cons_qnty from inv_receive_master b, inv_trims_entry_dtls a where b.id=a.mst_id and a.status_active=1 and b.entry_form=24 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and b.item_category=4 group by  a.item_group_id");
	}
	$inhouse_qnty_arr=array();
	foreach($inhouse_qnty_sql as $row)
	{
		$order_id_arr=explode(",",implode(",",array_unique(explode(",",$row[csf("order_id")]))));
		for($i=0;$i<=count($order_id_arr);$i++)
		{
			$inhouse_qnty_arr[$order_id_arr[$i]][$row[csf("item_group_id")]]=$row[csf("cons_qnty")];
		}
	}*/
	$receive_qty_array=array();
	$issue_qty_array=array();
	$receive_qty_data=sql_select("select b.po_breakdown_id, a.item_group_id,sum(b.quantity) as quantity   from  inv_receive_master c,product_details_master d,inv_trims_entry_dtls a , order_wise_pro_details b where a.mst_id=c.id and a.trans_id=b.trans_id and a.prod_id=d.id and b.trans_type=1 and b.entry_form=24 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by b.po_breakdown_id, a.item_group_id");
		foreach($receive_qty_data as $row)
		{
			$receive_qty_array[$row[csf('po_breakdown_id')]][$row[csf('item_group_id')]]['receive_qty']=$row[csf('quantity')];
			//$receive_qty_array[$row[csf('po_breakdown_id')]][$row[csf('item_group_id')]]['rate']=$row[csf('rate')];
			
		}
	$issue_qty_data=sql_select("select b.po_breakdown_id, a.item_group_id,sum(b.quantity) as quantity  from  inv_issue_master d,product_details_master p,inv_trims_issue_dtls a , order_wise_pro_details b where a.mst_id=d.id and a.prod_id=p.id and a.trans_id=b.trans_id and b.trans_type=2 and b.entry_form=25 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by b.po_breakdown_id, a.item_group_id");
		foreach($issue_qty_data as $row)
		{
			$issue_qty_array[$row[csf('po_breakdown_id')]][$row[csf('item_group_id')]]['issue_qty']=$row[csf('quantity')];
			//$issue_qty_array[$row[csf('po_breakdown_id')]][$row[csf('item_group_id')]]['rate']=$row[csf('rate')];
			//$receive_qty_array[$row[csf('po_breakdown_id')]][$row[csf('item_group_id')]][value]+=$row[csf('rate')];
		}
		
	if($db_type==2)
		{
		   $issue_qnty_sql=sql_select("select item_group_id,LISTAGG(CAST(order_id AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY order_id) as order_id,sum(issue_qnty) as issue_qnty from inv_trims_issue_dtls where status_active=1 and is_deleted=0 group by item_group_id");
		}
  	if($db_type==0)
		{
		   $issue_qnty_sql=sql_select("select item_group_id,group_concat(order_id,',') as order_id,sum(issue_qnty) as issue_qnty from inv_trims_issue_dtls where status_active=1 and is_deleted=0 group by item_group_id");
		}
	$issue_qnty_arr=array();
	foreach($issue_qnty_sql as $row)
	{
		$order_id_arr=explode(",",implode(",",array_unique(explode(",",$row[csf("order_id")]))));
		for($i=0;$i<=count($order_id_arr);$i++)
		{
			$issue_qnty_arr[$order_id_arr[$i]][$row[csf("item_group_id")]]=$row[csf("issue_qnty")];
		}
	}

	//var_dump($wo_qty_summary_array);
	
	if($template==1)
	{
		ob_start();
	?>
		<div style="width:2050px">
		<fieldset style="width:100%;">	
			<table width="2050">
				<tr class="form_caption">
					<td colspan="24" align="center">Accessories Followup Report</td>
				</tr>
				<tr class="form_caption">
					<td colspan="24" align="center"><? echo $company_library[$company_name]; ?></td>
				</tr>
			</table>
			<table class="rpt_table" width="2010" cellpadding="0" cellspacing="0" border="1" rules="all">
				<thead>
					<th width="30">SL</th>
					<th width="50">Buyer</th>
					<th width="100">Job No</th>
					<th width="100">Style Ref</th>
					<th width="90">Order No</th>
					<th width="80">Order Qnty</th>
					<th width="50">UOM</th>
					<th width="80">Qnty (Pcs)</th>
					<th width="80">Shipment Date</th>
					<th width="100">Trims Name</th>
					<th width="100">Brand/Sup Ref</th>
					<th width="60">Appr Req.</th>
					<th width="80">Approve Status</th>
					
                    <th width="100">Item Entry Date</th>
                    
					<th width="100">Req Qnty</th>
					<th width="100">Pre Costing Value</th>
					<th width="90">WO Qnty</th>
                    <th width="60">Trims UOM</th>
                    <th width="100">WO Value</th>
                    <th width="70">WO Delay Days</th>
					<th width="90">In-House Qnty</th>
					<th width="90">Receive Balance</th>
					<th width="90">Issue to Prod.</th>
					<th>Left Over/Balance</th>
				</thead>
			</table>
			<div style="width:2030px; max-height:400px; overflow-y:scroll" id="scroll_body">
				<table class="rpt_table" width="2010" cellpadding="0" cellspacing="0" border="1" rules="all" id="table_body">
				<?
				$i=1; $s=1; $total_order_qnty=0; $total_req_qnty=0; $total_order_qnty_in_pcs=0; $total_wo_qnty=0; $total_in_qnty=0; $item_array=array(); $uom_array=array(); 				               
				$today=date("Y-m-d");
				//echo $today;die;
				$item_app_array=array();
				if(str_replace("'","",$cbo_search_date)==1 )
				{
				$sql="select a.job_no_prefix_num, a.job_no, a.buyer_name, a.order_uom, a.style_ref_no, a.total_set_qnty as ratio,sum(distinct b.po_quantity) as po_quantity,
				 b.id,b.po_number, b.pub_shipment_date,  b.unit_price, b.po_total_price from wo_po_details_master a, wo_po_break_down b,wo_pre_cost_trim_cost_dtls d,wo_pre_cost_trim_co_cons_dtls c 
				 where a.job_no=d.job_no and d.id=c.wo_pre_cost_trim_cost_dtls_id and b.id=c.po_break_down_id and b.job_no_mst=c.job_no 
				  and a.job_no=b.job_no_mst and a.company_name='$company_name' 
				  and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and  b.is_deleted=0 and d.status_active=1 and d.is_deleted=0  $buyer_id_cond $date_cond $year_cond $job_no_cond $style_ref_cond  group by b.id, a.job_no, a.buyer_name, a.order_uom, a.style_ref_no,a.total_set_qnty,b.po_number,b.pub_shipment_date,b.unit_price, b.po_total_price,a.job_no_prefix_num order by b.id, b.pub_shipment_date"; //and a.buyer_name like '$buyer_name' and b.pub_shipment_date between '$start_date' and '$end_date'
				}
				else if(str_replace("'","",$cbo_search_date)==2)
				{
				$sql="select a.job_no_prefix_num, a.job_no,a.buyer_name, a.order_uom, a.style_ref_no, a.total_set_qnty as ratio, b.id, b.po_number, 
				b.pub_shipment_date, b.po_quantity as po_quantity , b.unit_price, b.po_total_price from wo_po_details_master a, wo_po_break_down b,wo_pre_cost_trim_cost_dtls d,wo_pre_cost_trim_co_cons_dtls c
				where  a.job_no=b.job_no_mst and a.job_no=d.job_no and d.id=c.wo_pre_cost_trim_cost_dtls_id and b.id=c.po_break_down_id and b.job_no_mst=c.job_no and 
				 a.company_name='$company_name' and a.status_active=1 and a.is_deleted=0  and b.status_active=1 and b.is_deleted=0  and d.is_deleted=0 and d.status_active=1 $buyer_id_cond $year_cond_pre $year_cond_p $date_cond $year_cond  $job_no_cond $style_ref_cond 
				  group by  b.id,a.job_no,b.po_quantity,a.job_no_prefix_num, a.buyer_name,a.order_uom, a.style_ref_no, a.total_set_qnty, b.po_number, b.pub_shipment_date, b.unit_price, b.po_total_price order by b.id, b.pub_shipment_date"; 
			
				}
				//echo $sql;
				$nameArray=sql_select($sql);
				$tot_rows=count($nameArray);
				$daysOnHand=0;
				foreach($nameArray as $row )
				{
					if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
					
					$dzn_qnty=0;
					if($costing_per_id_library[$row[csf('job_no')]]==1)
					{
						$dzn_qnty=12;
					}
					else if($costing_per_id_library[$row[csf('job_no')]]==3)
					{
						$dzn_qnty=12*2;
					}
					else if($costing_per_id_library[$row[csf('job_no')]]==4)
					{
						$dzn_qnty=12*3;
					}
					else if($costing_per_id_library[$row[csf('job_no')]]==5)
					{
						$dzn_qnty=12*4;
					}
					else
					{
						$dzn_qnty=1;
					}
					$dzn_qnty=$row[csf('ratio')]*$dzn_qnty;
					$k=1;
					$order_qnty_in_pcs=$row[csf('po_quantity')]*$row[csf('ratio')];
					$total_order_qnty+=$row[csf('po_quantity')];
					$total_order_qnty_in_pcs+=$order_qnty_in_pcs;
					$item_group_name=$row[csf('trim_group')];
					?>
					<tr bgcolor="<? echo $bgcolor;?>" onclick="change_color('tr_<? echo $s; ?>','<? echo $bgcolor;?>')" id="tr_<? echo $s; ?>">
						<td width="30"><p><? echo $i; ?>&nbsp;</p></td>
						<td width="50"><p><? echo $buyer_short_name_library[$row[csf('buyer_name')]]; ?>&nbsp;</p></td>
						<td width="100" align="center"><p><? echo $row[csf('job_no_prefix_num')]; ?>&nbsp;</p></td>
						<td width="100"><p><? echo $row[csf('style_ref_no')]; ?>&nbsp;</p></td>
						<td width="90"><p><a href='#report_details' onclick="generate_report('<? echo $company_name; ?>','<? echo $row[csf('job_no')]; ?>','preCostRpt');"><? echo $row[csf('po_number')]; ?></a>&nbsp;</p></td>
						<td width="80" align="right"><p><a href='#report_details' onclick="order_qty_popup('<? echo $company_name; ?>','<? echo $row[csf('job_no')]; ?>','<? echo $row[csf('id')]; ?>', '<? echo $row[csf('buyer_name')]; ?>' ,'order_qty_data');"><? echo number_format($row[csf('po_quantity')],0,'.',''); ?></a>&nbsp;</p></td>
						<td width="50" align="center"><p><? echo $unit_of_measurement[$row[csf('order_uom')]]; ?>&nbsp;</p></td>
						<td width="80" align="right"><p><? echo number_format($order_qnty_in_pcs,0,'.',''); ?>&nbsp;</p></td>
						<td width="80" align="center"><p><? echo change_date_format($row[csf('pub_shipment_date')]); ?>&nbsp;</p></td>
					<?
					/*$trimsArray=sql_select("select a.trim_group, a.cons_uom, a.apvl_req, a.brand_sup_ref, a.rate, b.cons from wo_pre_cost_trim_cost_dtls a, wo_pre_cost_trim_co_cons_dtls b where a.id=b.wo_pre_cost_trim_cost_dtls_id and b.po_break_down_id='".$row[csf('id')]."' and a.job_no='".$row[csf('job_no')]."' and a.status_active=1 and a.is_deleted=0 $item_group_cond");*/
					foreach($reference_arr[$row[csf('id')]] as $selectResult)
					{
						$req_qnty=0; $req_value=0; $wo_qty=0;
						//echo $selectResult[('cons')];
						    if($selectResult['country_id']==0)
							 {
								 $txt_country_cond="";
							 }
							 else
							 {
								 $txt_country_cond ="and c.country_id in (".$selectResult['country_id'].")";
							 }
							//echo $txt_country_cond;
							 $sql_po_qty=sql_select("select sum(c.order_quantity) as order_quantity,(sum(c.order_quantity)/a.total_set_qnty) as order_quantity_set  from wo_po_details_master a, wo_po_break_down b,wo_po_color_size_breakdown c where a.job_no=b.job_no_mst and a.job_no=c.job_no_mst and b.id=c.po_break_down_id and  b.id=".$row[csf('id')]."  $txt_country_cond  and a.status_active=1 and b.status_active=1 and c.status_active=1 group by b.id,a.total_set_qnty");
							 list($sql_po_qty_row)=$sql_po_qty;
							 $po_qty=$sql_po_qty_row[csf('order_quantity_set')];
						$req_qnty=($po_qty/$dzn_qnty)*$selectResult[('cons')];
						$total_req_qnty+=$req_qnty;
						//echo $total_req_qnty;
						$req_value=$req_qnty*$selectResult[('rate')];
						$total_pre_costing_value+=$req_value;
						
						if($selectResult[('apvl_req')]==1)
						{
							//$app_status=return_field_value("approval_status","wo_po_trims_approval_info","job_no_mst='".$row[csf('job_no')]."' and po_break_down_id='".$row[csf('id')]."' and accessories_type_id='".$selectResult[('trim_group')]."' and status_active=1 and is_deleted=0 and current_status=1");
							$app_status=$app_status_arr[$row[csf('id')]][$selectResult[('trim_group')]];
							
							$approved_status=$approval_status[$app_status];
							
							if(array_key_exists($selectResult[('trim_group')], $item_app_array)) 
							{
								$item_app_array[$selectResult[('trim_group')]]['all']+=1;
								
								if($app_status==3)
								{
									$item_app_array[$selectResult[('trim_group')]]['app']+=1;
								}
							}
							else
							{
								$item_app_array[$selectResult[('trim_group')]]['all']=1;
								
								if($app_status==3)
								{
									$item_app_array[$selectResult[('trim_group')]]['app']=1;
								}	
							}
						}
						else
						{
							$approved_status="";
						}
						
						if(!array_key_exists($selectResult[('trim_group')], $uom_array)) 
						{
							$uom_array[$selectResult[('trim_group')]]=$unit_of_measurement[$selectResult[('cons_uom')]];
						}
						
						 //echo $selectResult[('trim_group')];
						if($k==1)
						{
							
							if($wo_qty_array[$row[csf('id')]][$selectResult[('trim_group')]]['wo_qnty']>$req_qnty)
								{
								$color_wo="red";	
								}
								
								else if($req_qnty>$wo_qty_array[$row[csf('id')]][$selectResult[('trim_group')]]['wo_qnty'] )
								{
								$color_wo="yellow";		
								}
								
								else 
								{
								$color_wo="";	
								}
								$trim_id=$selectResult[('trim_group')];
								
								$order_id=$row[csf('id')];
								
								$booking_no_arr=array_unique(explode(',',$wo_qty_array[$order_id][$trim_id]['booking_no']));
								//$booking_no_arr_d=implode(',',$booking_no_arr);
								//print $order_id.'='.	$trim_id;// $wo_qty_array[$order_id][$trim_id]['booking_no'];
								$main_booking_no_large_data="";
								foreach($booking_no_arr as $booking_no1)
								{	
									//if($booking_no1>0)
									//{
									if($main_booking_no_large_data=="") $main_booking_no_large_data=$booking_no1; else $main_booking_no_large_data.=",".$booking_no1;
									//}
									//print($main_booking_no_large_data);
								}
								
								$conversion_factor_rate=$conversion_factor_array[$trim_id]['con_factor'];
								
								if($db_type==2)
								{
											//$wo_qty_array[$row[csf('id')]][$selectResult[('trim_group')]]['wo_qnty']
										//$pre_cost_insert_date=explode('',$row[csf('pre_date')]);
								if($wo_qty_array[$row[csf('id')]][$selectResult[('trim_group')]]['wo_qnty']==0 || $wo_qty_array[$row[csf('id')]][$selectResult[('trim_group')]]['wo_qnty']=='')
						
									{
									
									//$reference_arr[$row[csf('po_break_down_id')]][$row[csf('id')]]['id'];
									$pre_id=$selectResult[('id')];
									$pre_cost_date=$reference_arr[$order_id][$pre_id]['pre_date'];
									$pre_cost_insert_date=explode(' ',$pre_cost_date);
									
									$wo_day=$wo_qty_array[$order_id][$trim_id]['wo_date'];
									$tot=change_date_format($pre_cost_insert_date[0]); //change_date_format($row[csf('pre_date')],'','',1);
									//echo change_date_format($pre_cost_insert_date[0]);die;
									$wo_date= change_date_format($wo_day);
										
									$daysOnHand = datediff(d,$tot,$today);
									//echo $tot.'--'.$today.'=='.$daysOnHand.'Aziz';
										//$daysOnHand=(strtotime($today)-strtotime($tot))/(60*60*24);
										//echo $daysOnHand;	
									}
									else
									{
									$pre_id=$selectResult[('id')];
									$pre_cost_date=$reference_arr[$order_id][$pre_id]['pre_date'];
									$pre_cost_insert_date=explode(' ',$pre_cost_date);
									
									$wo_day=$wo_qty_array[$order_id][$trim_id]['wo_date'];
									$tot=change_date_format($pre_cost_insert_date[0]); //change_date_format($row[csf('pre_date')],'','',1);
									$wo_date= change_date_format($wo_day);	
									$daysOnHand = datediff("d",$tot,$wo_date);
								
									//echo $tot.'--'.$wo_date.'=='.$daysOnHand.'Muk';	
									}
								}
								else
								{
									/*$wo_day=$wo_qty_array[$order_id][$trim_id]['wo_date'];
									$tot=change_date_format($row[csf('pre_date')]);
									$wo_date= change_date_format($wo_day);	
									$daysOnHand = datediff("d",$wo_date,$tot);*/
									$trim_id=$selectResult[('trim_group')];
									$order_id=$row[csf('id')];
									if($wo_qty_array[$row[csf('id')]][$selectResult[('trim_group')]]['wo_qnty']==0 || $wo_qty_array[$row[csf('id')]][$selectResult[('trim_group')]]['wo_qnty']=='')
									{
									$pre_id=$selectResult[('id')];
									$pre_cost_date=$reference_arr[$order_id][$pre_id]['pre_date'];
									$pre_cost_insert_date=explode(' ',$pre_cost_date);
									$wo_day=$wo_qty_array[$order_id][$trim_id]['wo_date'];
									$tot=change_date_format($pre_cost_insert_date[0]); //change_date_format($row[csf('pre_date')],'','',1);
									$wo_date= change_date_format($wo_day);
									$daysOnHand = datediff(d,$tot,$today);
										//$daysOnHand=(strtotime($today)-strtotime($tot))/(60*60*24);
										//echo $daysOnHand;	
									}
									else
									{
									$pre_id=$selectResult[('id')];
									$pre_cost_date=$reference_arr[$order_id][$pre_id]['pre_date'];
									$pre_cost_insert_date=explode(' ',$pre_cost_date);
									$wo_day=$wo_qty_array[$order_id][$trim_id]['wo_date'];
									$tot=change_date_format($pre_cost_insert_date[0]); //change_date_format($row[csf('pre_date')],'','',1);
									$wo_date= change_date_format($wo_day);	
									$daysOnHand = datediff("d",$tot,$wo_date);	
									}	
								}
						?>
								<td width="100">
									<p>
										<? echo $item_library[$selectResult[('trim_group')]]; ?>
									&nbsp;</p>
								</td>
								<td width="100">
									<p>
										<? echo $selectResult[('brand_sup_ref')]; ?>
									&nbsp;</p>
								</td>
								<td width="60" align="center"><p><? if($selectResult[('apvl_req')]==1) echo "Yes"; else echo "&nbsp;"; ?>&nbsp;</p></td>
								<td width="80" align="center"><p><? echo $approved_status; ?>&nbsp;</p></td>
                                <td width="100" align="right"><p><? echo change_date_format($pre_cost_insert_date[0]);//echo change_date_format($row[csf('pre_date')],'','',1); ?>&nbsp;</p></td>
								<td width="100" align="right"><p><a href='#report_details' onclick="order_req_qty_popup('<? echo $company_name; ?>','<? echo $row[csf('job_no')]; ?>','<? echo $row[csf('id')]; ?>', '<? echo $row[csf('buyer_name')]; ?>','<? echo $selectResult[('rate')]; ?>','<? echo $selectResult[('trim_group')];?>' ,'<? echo $booking_no;?>','order_req_qty_data');"><? echo number_format($req_qnty,2,'.',''); ?></a>&nbsp;</p></td>
                                
								<td width="100" align="right"><p><? echo number_format($req_value,2); ?>&nbsp;</p></td>
								<td width="90" align="right" title="<? echo 'conversion_factor='.$conversion_factor_rate;?>" bgcolor="<? echo $color_wo;?>"><p><a href='#report_details' onclick="openmypage('<? echo $row[csf('id')]; ?>','<? echo $selectResult[('trim_group')]; ?>','<? echo $row[csf('job_no')]; ?>','<? echo $main_booking_no_large_data;?>','booking_info');">
								<? 
								//echo $selectResult[('trim_dtls_id')];
								echo number_format($wo_qty_array[$row[csf('id')]][$selectResult[('trim_group')]]['wo_qnty']*$conversion_factor_rate,2,'.','')
								?>
                                </a>&nbsp;</p></td>
                                <td width="60" align="center"><p><? echo $unit_of_measurement[$selectResult[('cons_uom')]]; ?>&nbsp;</p></td>
                                <td width="100" align="right" title="<? echo number_format($wo_qty_array[$order_id][$trim_id]['rate'],2,'.',''); ?>"><p><? echo number_format($wo_qty_array[$order_id][$trim_id]['amount'],2,'.',''); ?>&nbsp;</p></td>
                                <td width="70" align="right" title="<? //echo change_date_format($wo_day);?>"><p>
								<?  
								if($wo_qty_array[$row[csf('id')]][$selectResult[('trim_group')]]['wo_qnty']==0 || $wo_qty_array[$row[csf('id')]][$selectResult[('trim_group')]]['wo_qnty']!=0) echo $daysOnHand; 
								?>&nbsp;</p></td>
                                <?
									
									$inhouse_qnty=$receive_qty_array[$order_id][$trim_id]['receive_qty'];
									//$inhouse_qnty=$inhouse_qnty_arr[$order_id][$trim_id];
									$balance=$wo_qty_array[$row[csf('id')]][$selectResult[('trim_group')]]['wo_qnty']-$inhouse_qnty;
									$issue_qnty=$issue_qty_array[$order_id][$trim_id]['issue_qty'];
									$left_overqty=$inhouse_qnty-$issue_qnty;
								?>
								<td width="90" align="right"><p><a href='#report_details' onclick="openmypage_inhouse('<? echo $row[csf('id')]; ?>','<? echo $selectResult[('trim_group')]; ?>','booking_inhouse_info');"><? echo number_format($inhouse_qnty,2,'.',''); ?></a>&nbsp;</p></td>
								<td width="90" align="right"><p><? echo number_format($balance,2,'.',''); ?>&nbsp;</p></td>
								<td width="90" align="right"><p><a href='#report_details' onclick="openmypage_issue('<? echo $row[csf('id')]; ?>','<? echo $selectResult[('trim_group')]; ?>','booking_issue_info');"><? echo number_format($issue_qnty,2,'.',''); ?></a>&nbsp;</p></td>
								<td align="right"><p><? echo number_format($left_overqty,2,'.',''); ?>&nbsp;</p></td>
							</tr>
							<?
						}
						else
						{
							if($wo_qty_array[$row[csf('id')]][$selectResult[('trim_group')]]['wo_qnty']>$req_qnty)
								{
								$color_wo="red";	
								}
								
								else if($req_qnty>$wo_qty_array[$row[csf('id')]][$selectResult[('trim_group')]]['wo_qnty'])
								{
								$color_wo="yellow";		
								}
								
								else 
								{
								$color_wo="";	
								}
								$trim_id=$selectResult[('trim_group')];
								$order_id=$row[csf('id')];
								$booking_no_arr=array_unique(explode(',',$wo_qty_array[$order_id][$trim_id]['booking_no']));
								//$booking_no_arr_d=implode(',',$booking_no_arr);
								//print $order_id.'='.	$trim_id;// $wo_qty_array[$order_id][$trim_id]['booking_no'];
								$main_booking_no_large_data="";
								foreach($booking_no_arr as $booking_no1)
								{	
									if($main_booking_no_large_data=="") $main_booking_no_large_data=$booking_no1; else $main_booking_no_large_data.=",".$booking_no1;
								}
								if($db_type==2)
								{
								if($wo_qty_array[$row[csf('id')]][$selectResult[('trim_group')]]['wo_qnty']==0 || $wo_qty_array[$row[csf('id')]][$selectResult[('trim_group')]]['wo_qnty']=='')
						
									{
									$pre_id=$selectResult[('id')];
									$pre_cost_date=$reference_arr[$order_id][$pre_id]['pre_date'];
									$pre_cost_insert_date=explode(' ',$pre_cost_date);
							//echo change_date_format($pre_cost_insert_date[0])."HHHH";
									$wo_day=$wo_qty_array[$order_id][$trim_id]['wo_date'];
									//echo change_date_format($pre_cost_insert_date[0]).'tttt'; //change_date_format($row[csf('pre_date')],'','',1);
									//$tot=change_date_format($pre_cost_insert_date[0]);
									$wo_date= change_date_format($wo_day);

									$daysOnHand = datediff("d",$tot,$today);
									//echo $tot.'--'.$today.'=='.$daysOnHand.'BB';	
										//$daysOnHand=(strtotime($today)-strtotime($tot))/(60*60*24);
										//echo $daysOnHand;	
									}
									else
									{
									$pre_id=$selectResult[('id')];
									$pre_cost_date=$reference_arr[$order_id][$pre_id]['pre_date'];
									$pre_cost_insert_date=explode(' ',$pre_cost_date);
									
									$wo_day=$wo_qty_array[$order_id][$trim_id]['wo_date'];
									$tot=change_date_format($pre_cost_insert_date[0]); //change_date_format($row[csf('pre_date')],'','',1);
									$wo_date= change_date_format($wo_day);	
									$daysOnHand = datediff("d",$tot,$wo_date);
									//echo $tot.'--'.$wo_date.'=='.$daysOnHand.'CC';		
									}
								}
								else
								{
									
									
									if($wo_qty_array[$row[csf('id')]][$selectResult[('trim_group')]]['wo_qnty']==0 || $wo_qty_array[$row[csf('id')]][$selectResult[('trim_group')]]['wo_qnty']=='')
						
									{
									$pre_id=$selectResult[('id')];
									$pre_cost_date=$reference_arr[$order_id][$pre_id]['pre_date'];
									$pre_cost_insert_date=explode(' ',$pre_cost_date);
									$wo_day=$wo_qty_array[$order_id][$trim_id]['wo_date'];
									$tot=change_date_format($pre_cost_insert_date[0]); //change_date_format($row[csf('pre_date')],'','',1);
									$wo_date= change_date_format($wo_day);

									$daysOnHand = datediff("d",$tot,$today);
										//$daysOnHand=(strtotime($today)-strtotime($tot))/(60*60*24);
										//echo $daysOnHand;	
									}
									else
									{
									$pre_id=$selectResult[('id')];
									$pre_cost_date=$reference_arr[$order_id][$pre_id]['pre_date'];
									$pre_cost_insert_date=explode(' ',$pre_cost_date);
									$wo_day=$wo_qty_array[$order_id][$trim_id]['wo_date'];
									$tot=change_date_format($pre_cost_insert_date[0]); //change_date_format($row[csf('pre_date')],'','',1);
									$wo_date= change_date_format($wo_day);	
									$daysOnHand = datediff("d",$tot,$wo_date);	
									}	
								}
							
						?>
							<tr bgcolor="<? echo $bgcolor;?>" onclick="change_color('tr_<? echo $s; ?>','<? echo $bgcolor;?>')" id="tr_<? echo $s; ?>">
								<td width="30">
									<font style="display:none" color="<? echo $bgcolor; ?>">
										<p><? echo $i; ?>&nbsp;</p>
									</font>
								</td>
								<td width="50">
									<font style="display:none" color="<? echo $bgcolor; ?>">
										<p><? echo $buyer_short_name_library[$row[csf('buyer_name')]]; ?>&nbsp;</p>
									</font>
								</td>
								<td width="100">
									<font style="display:none" color="<? echo $bgcolor; ?>">
										<p><? echo $row[csf('job_no')]; ?>&nbsp;</p>
									</font>
								</td>
								<td width="100">
									<font style="display:none" color="<? echo $bgcolor; ?>">
										<p>
											<? echo $row[csf('style_ref_no')]; ?>
										&nbsp;</p>
									</font>
								</td>
								<td width="90">
									<font style="display:none" color="<? echo $bgcolor; ?>">
										<p>
											<? echo $row[csf('po_number')]; ?>
										&nbsp;</p>
									</font>
								</td>
								<td width="80" align="right">
									<font style="display:none" color="<? echo $bgcolor; ?>">
										<p><? //echo number_format($row[csf('po_quantity')],0,'.',''); ?>&nbsp;</p>
									</font>
								</td>
								<td width="50" align="center">
									<font style="display:none" color="<? echo $bgcolor; ?>">
										<p><? echo $unit_of_measurement[$row[csf('order_uom')]]; ?>&nbsp;</p>
									</font>
								</td>
								<td width="80" align="right">
									<font style="display:none" color="<? echo $bgcolor; ?>">
										<p><? //echo number_format($order_qnty_in_pcs,0,'.',''); ?>&nbsp;</p>
									</font>
								</td>
								<td width="80" align="center">
									<font style="display:none" color="<? echo $bgcolor; ?>">
										<p><? echo change_date_format($row[csf('pub_shipment_date')]); ?>&nbsp;</p>
									</font>
								</td>
								<td width="100">
									<p>
										<? echo $item_library[$selectResult[('trim_group')]]; ?>
									&nbsp;</p>
								</td>
								<td width="100">
									<p>
										<? echo $selectResult[('brand_sup_ref')]; ?>
									&nbsp;</p>
								</td>
								<td width="60" align="center"><p><? if($selectResult[('apvl_req')]==1) echo "Yes"; else echo "&nbsp;"; ?>&nbsp;</p></td>
								<td width="80" align="center"><p><? echo $approved_status; ?>&nbsp;</p></td>
								
                                <td width="100" align="right"><p><? echo change_date_format($pre_cost_insert_date[0]); ?>&nbsp;</p></td>
								<td width="100" align="right"><p><a href='#report_details' onclick="order_req_qty_popup('<? echo $company_name; ?>','<? echo $row[csf('job_no')]; ?>','<? echo $row[csf('id')]; ?>', '<? echo $row[csf('buyer_name')]; ?>','<? echo $selectResult[('rate')]; ?>','<? echo $selectResult[('trim_group')];?>','<? echo $booking_no;?>' ,'order_req_qty_data');"><? echo number_format($req_qnty,2,'.',''); ?></a>&nbsp;</p></td>
								<td width="100" align="right"><p><? echo number_format($req_value,2); ?>&nbsp;</p></td>
								<td width="90" align="right" bgcolor="<? echo $color_wo;?>"  title="<? echo 'conversion_factor='.$conversion_factor_rate;?>"><p><a href='#report_details' onclick="openmypage('<? echo $row[csf('id')]; ?>','<? echo $selectResult[('trim_group')]; ?>','<? echo $row[csf('job_no')]; ?>','<? echo $main_booking_no_large_data;?>','booking_info');">
								<? 
								//$wo_qty_array[$row[csf('id')]][$selectResult[('trim_group')]]['wo_qnty']*$conversion_factor_rate;
								echo number_format($wo_qty_array[$row[csf('id')]][$selectResult[('trim_group')]]['wo_qnty']*$conversion_factor_rate,2,'.','');
								?></a>&nbsp;</p></td>
                                <td width="60" align="center"><p><? echo $unit_of_measurement[$selectResult[('cons_uom')]]; ?>&nbsp;</p></td>
                                <td width="100" align="right" title="<? echo number_format($wo_qty_array[$order_id][$trim_id]['rate'],2,'.',''); ?>"><p><? echo  number_format($wo_qty_array[$order_id][$trim_id]['amount'],2,'.',''); ?>&nbsp;</p></td>
                                <td width="70" align="right" title="<? //echo change_date_format($wo_date);?>" ><p>
								<?  
								//echo $daysOnHand; $next_day=$daysOnHand;  
								if($wo_qty_array[$row[csf('id')]][$selectResult[('trim_group')]]['wo_qnty']==0 || $wo_qty_array[$row[csf('id')]][$selectResult[('trim_group')]]['wo_qnty']!=0) echo $daysOnHand; 
								//echo $wo_qty_array[$row[csf('id')]][$selectResult[('trim_group')]]['wo_qnty'];
								?>
                                &nbsp;</p></td>
                            
                                <?
									$trim_id=$selectResult[('trim_group')];
									$order_id=$row[csf('id')];
									//$inhouse_qnty=return_field_value("sum(a.cons_qnty)","inv_receive_master b, inv_trims_entry_dtls a","b.id=a.mst_id and a.item_group_id='$trim_id' and a.order_id='$order_id' and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and b.item_category=4");
									$inhouse_qnty=$receive_qty_array[$order_id][$trim_id]['receive_qty'];
									//$inhouse_qnty=$inhouse_qnty_arr[$order_id][$trim_id];
									$balance=$wo_qty_array[$row[csf('id')]][$selectResult[('trim_group')]]['wo_qnty']-$inhouse_qnty;
									
									//$issue_qnty=return_field_value("sum(issue_qnty)","inv_trims_issue_dtls","item_group_id='$trim_id' and order_id='$order_id' and status_active=1 and is_deleted=0");
									//$issue_qnty=$issue_qnty_arr[$order_id][$trim_id];
									$issue_qnty=$issue_qty_array[$order_id][$trim_id]['issue_qty'];
									$left_overqty=$inhouse_qnty-$issue_qnty;
								?>
                                <td width="90" align="right"><p><a href='#report_details' onclick="openmypage_inhouse('<? echo $row[csf('id')]; ?>','<? echo $selectResult[('trim_group')]; ?>','booking_inhouse_info');"><? echo number_format($inhouse_qnty,2,'.',''); ?></a>&nbsp;</p></td>
								<td width="90" align="right"><p><? echo number_format($balance,2,'.',''); ?>&nbsp;</p></td>
								<td width="90" align="right"><p><a href='#report_details' onclick="openmypage_issue('<? echo $row[csf('id')]; ?>','<? echo $selectResult[('trim_group')]; ?>','booking_issue_info');"><? echo number_format($issue_qnty,2,'.',''); ?></a>&nbsp;</p></td>
								<td align="right"><p><? echo number_format($left_overqty,2,'.',''); ?>&nbsp;</p></td>
							</tr>
						<?
						}
					$k++;
					$s++;	
                            $total_wo_qnty+=$wo_qty_array[$row[csf('id')]][$selectResult[('trim_group')]]['wo_qnty'];
							$total_wo_value+=$wo_qty_array[$order_id][$trim_id]['amount'];
                            $total_in_qnty+=$inhouse_qnty;
                            $rec_bal=$wo_qty_array[$row[csf('id')]][$selectResult[('trim_group')]]['wo_qnty']-$inhouse_qnty;
                            $total_rec_bal_qnty+=$balance;
                            
                            $total_issue_qnty+=$issue_qnty;
                            $total_leftover_qnty+=$left_overqty;
							
						$item_array[$selectResult[('trim_group')]]['req']+=$req_qnty;
						$item_array[$selectResult[('trim_group')]]['wo']+=$wo_qty_array[$row[csf('id')]][$selectResult[('trim_group')]]['wo_qnty'];
						$item_array[$selectResult[('trim_group')]]['in']+=$inhouse_qnty;
						$item_array[$selectResult[('trim_group')]]['issue']+=$issue_qnty;
						$item_array[$selectResult[('trim_group')]]['leftover']+=$left_overqty;
					}
					if(count($reference_arr[$row[csf('id')]])<1)
					{
					?>
							<td width="100">&nbsp;</td>
							<td width="100">&nbsp;</td>
							<td width="60" align="center">&nbsp;</td>
							<td width="80">&nbsp;</td>
							<td width="60" align="center">&nbsp;</td>
                            <td width="100" align="right">&nbsp;</td>
							<td width="100" align="right">&nbsp;</td>
							<td width="100" align="right">&nbsp;</td>
							<td width="90" align="right">&nbsp;</td>
                            <td width="70" align="right">&nbsp;</td>
                            <td width="90" align="right">&nbsp;</td>
							<td width="90" align="right">&nbsp;</td>
							<td width="90" align="right">&nbsp;</td>
							<td align="right">&nbsp;</td>
						</tr>
					<?
					} //$total_in_qnty+=$inhouse_qnty;
				$i++;
				}
				?>
				</table>
				<table class="rpt_table" width="2010" cellpadding="0" cellspacing="0" border="1" rules="all">
					<tfoot>
						<th width="30"></th>
						<th width="50"></th>
						<th width="100"></th>
						<th width="100"></th>
						<th width="90"></th>
						<th width="80" align="right" id="total_order_qnty"><? echo number_format($total_order_qnty,0); ?></th>
						<th width="50"></th>
						<th width="80" align="right" id="total_order_qnty_in_pcs"><? echo number_format($total_order_qnty_in_pcs,0); ?></th>
						<th width="80"></th>
						<th width="100"></th>
						<th width="100"></th>
						<th width="60"></th>
						<th width="80"></th>
						<th width="60"></th>
                        <th width="100"></th>
						<th width="100" align="right" id="value_req_qnty"><? //echo number_format($total_req_qnty,2); ?></th>
						<th width="100" align="right" id="value_pre_costing"><? echo number_format($total_pre_costing_value,2); ?></th>
						<th width="90" align="right" id="value_wo_qty"><? echo number_format($total_wo_qnty,2); ?></th>
                        <th width="100" align="right" id=""><? echo number_format($total_wo_value,2); ?></th>
                        <th width="70" align="right"><p><? //echo number_format($req_value,2,'.',''); ?>&nbsp;</p></th>
                        <th width="90" align="right" id="value_in_qty"><? echo number_format($total_in_qnty,2); ?></th>
						<th width="90" align="right" id="value_rec_qty"><? echo number_format($total_rec_bal_qnty,2); ?></th>
						<th width="90" align="right" id="value_issue_qty"><? echo number_format($total_issue_qnty,2); ?></th>
						<th align="right" id="value_leftover_qty"><? echo number_format($total_leftover_qnty,2); ?></th>
					</tfoot>
				</table>
				</div>
				<table>
					<tr><td height="15"></td></tr>
				</table>
				<u><b>Summary</b></u>
				<table class="rpt_table" width="1200" cellpadding="0" cellspacing="0" border="1" rules="all">
					<thead>
						<th width="30">SL</th>
						<th width="110">Item</th>
						<th width="60">UOM</th>
						<th width="80">Approved %</th>
						<th width="110">Req Qty</th>
						<th width="110">WO Qty</th>
						<th width="80">WO %</th>
						<th width="110">In-House Qty</th>
						<th width="80">In-House %</th>
						<th width="110">In-House Balance Qty</th>
						<th width="110">Issue Qty</th>
						<th width="80">Issue %</th>
						<th>Left Over</th>
					</thead>
					<?
					$z=1; $tot_req_qnty_summary=0;
					foreach($item_array as $key=>$value)
					{
						if($z%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
						//print_r ($value);
						$tot_req_qnty_summary+=$value['req'];
						$tot_wo_qnty_summary+=$value['wo'];
						$tot_in_qnty_summary+=$value['in'];
						$tot_issue_qnty_summary+=$value['issue'];
						$tot_leftover_qnty_summary+=$value['leftover'];
					?>
						<tr bgcolor="<? echo $bgcolor;?>" onclick="change_color('tr2_<? echo $z; ?>','<? echo $bgcolor;?>')" id="tr2_<? echo $z; ?>">
							<td width="30"><? echo $z; ?></td>
							<td width="110"><p><? echo $item_library[$key]; ?></p></td>
							<td width="60" align="center"><? echo $uom_array[$key]; ?></td>
							<td width="80" align="right"><? $app_perc=($item_app_array[$key]['app']*100)/$item_app_array[$key]['all']; echo number_format($app_perc,2); ?>&nbsp;</td>
							<td width="110" align="right"><? echo number_format($value['req'],2); ?>&nbsp;</td>
							<td width="110" align="right"><? echo number_format($value['wo'],2); ?>&nbsp;</td>
							<td width="80" align="right"><? $wo_per=$value['wo']/$value['req']*100; echo number_format($wo_per,2).'%'; ?>&nbsp;</td>
							<td width="110" align="right"><? echo number_format($value['in'],2); ?>&nbsp;</td>
							<td width="80" align="right"><? $in_per=$value['in']/$value['wo']*100; echo number_format($in_per,2).'%'; ?>&nbsp;</td>
							<td width="110" align="right"><? echo number_format($value['wo']-$value['in'],2); $in_house_bal+=($value['wo']-$value['in']); ?>&nbsp;</td>
							<td width="110" align="right"><? echo number_format($value['issue'],2); ?>&nbsp;</td>
							<td width="80" align="right"><? $wo_per=$value['issue']/$value['wo']*100; echo number_format($wo_per,2).'%'; ?>&nbsp;</td>
							<td align="right"><? echo number_format($value['leftover'],2); ?>&nbsp;</td>
						</tr>
					<?	
					$z++;
					}
					?>
					<tfoot>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<th>&nbsp;</th>
						<th align="right"><? echo number_format($tot_req_qnty_summary,2); ?>&nbsp;</th>
						<th align="right"><? echo number_format($tot_wo_qnty_summary,2); ?>&nbsp;</th>
						<th>&nbsp;</th>
						<th align="right"><? echo number_format($tot_in_qnty_summary,2); ?>&nbsp;</th>
						<th>&nbsp;</th>
						<th align="right"><? echo number_format($in_house_bal,2); ?>&nbsp;</th>
						<th align="right"><? echo number_format($tot_issue_qnty_summary,2); ?>&nbsp;</th>
						<th>&nbsp;</th>
						<th align="right"><? echo number_format($tot_leftover_qnty_summary,2); ?>&nbsp;</th>
					</tfoot>   	
				</table>
			</fieldset>
		</div>
	<?
	
	}
	}
	
//===========================================================================================================================================================

  if(str_replace("'","",$cbo_search_by)==2)
  {
	$trim_group_id_arr=array();
	if($db_type==2)
	{
 	$trims_sql="select b.po_break_down_id, a.id, a.trim_group, a.cons_uom, a.rate from wo_pre_cost_trim_cost_dtls a, wo_pre_cost_trim_co_cons_dtls b where a.id=b.wo_pre_cost_trim_cost_dtls_id and a.status_active=1 and a.is_deleted=0 
	union
			select a.po_break_down_id, a.id, a.trim_group, 0, 0 from wo_booking_mst b,wo_booking_dtls a where b.booking_no=a.booking_no and a.job_no=b.job_no and b.item_from_precost=2 and b.booking_type=2  and  b.status_active=1 and b.is_deleted=0 
	union
	select b.po_breakdown_id as po_break_down_id, a.id, a.item_group_id as trim_group,0,0 from  inv_receive_master d,inv_trims_entry_dtls a ,order_wise_pro_details b,product_details_master c where d.id=a.mst_id and a.trans_id=b.trans_id and b.trans_type=1 and b.entry_form=24 and d.receive_basis in (1,4,6) and a.prod_id=c.id and c.id=b.prod_id and a.status_active=1  and b.is_deleted=0 and b.status_active=1 and b.is_deleted=0 
	 ";
	}
	else if($db_type==0)
	{
	 $trims_sql="select b.po_break_down_id, a.id, a.trim_group, a.cons_uom, a.rate from wo_pre_cost_trim_cost_dtls a, wo_pre_cost_trim_co_cons_dtls b where a.id=b.wo_pre_cost_trim_cost_dtls_id  and a.status_active=1 and a.is_deleted=0 
	union
			select a.po_break_down_id, a.id, a.trim_group, 0, 0 from wo_booking_mst b,wo_booking_dtls a where b.booking_no=a.booking_no and a.job_no=b.job_no and b.item_from_precost=2 and b.booking_type=2   and  b.status_active=1 and b.is_deleted=0 
	union
	select b.po_breakdown_id as po_break_down_id, a.id, a.item_group_id as trim_group,0,0 from  inv_receive_master d,inv_trims_entry_dtls a ,order_wise_pro_details b,product_details_master c where d.id=a.mst_id and a.trans_id=b.trans_id and b.trans_type=1 and b.entry_form=24 and d.receive_basis in (1,4,6) and a.prod_id=c.id and c.id=b.prod_id and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and b.is_deleted=0 
	 ";
	}
	//echo $trims_sql;
	$trimsArray=sql_select($trims_sql);
	foreach($trimsArray as $trim_val)
	{
		//$trim_group_arr[$trim_val[csf("po_break_down_id")]]['id']=$trim_val[csf("id")];
		//$trim_group_arr[$trim_val[csf("po_break_down_id")]]['po_break_down_id']=$trim_val[csf("po_break_down_id")];
		$trim_group_id_arr[$trim_val[csf("po_break_down_id")]][$trim_val[csf("id")]]['trim']=$trim_val[csf("trim_group")];
		$trim_group_id_arr[$trim_val[csf("po_break_down_id")]][$trim_val[csf("id")]]["uom"]=$trim_val[csf("cons_uom")];
		
		//echo $trim_group_arr[$trim_val[csf("job_no")]][$trim_val[csf("trim_group")]];//$trim_val[csf("job_no")].'<br>'.$trim_val[csf("trim_group")];
	} //var_dump($trim_group_id_arr);die;
	//print_r($trim_uom_arr);
	$receive_qty_array=array();
	$issue_qty_array=array();
	$receive_qty_data=sql_select("select b.po_breakdown_id, a.item_group_id,sum(b.quantity) as quantity   from  inv_receive_master c,product_details_master d,inv_trims_entry_dtls a , order_wise_pro_details b where a.mst_id=c.id and a.trans_id=b.trans_id and a.prod_id=d.id and b.trans_type=1 and b.entry_form=24 and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by b.po_breakdown_id, a.item_group_id");
		foreach($receive_qty_data as $row)
		{
			$receive_qty_array[$row[csf('po_breakdown_id')]][$row[csf('item_group_id')]]['receive_qty']=$row[csf('quantity')];
			//$receive_qty_array[$row[csf('po_breakdown_id')]][$row[csf('item_group_id')]]['rate']=$row[csf('rate')];
		}
	$issue_qty_data=sql_select("select b.po_breakdown_id, a.item_group_id,sum(b.quantity) as quantity  from  inv_issue_master d,product_details_master p,inv_trims_issue_dtls a , order_wise_pro_details b where a.mst_id=d.id and a.prod_id=p.id and a.trans_id=b.trans_id and b.trans_type=2 and b.entry_form=25 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by b.po_breakdown_id, a.item_group_id");
		foreach($issue_qty_data as $row)
		{
			$issue_qty_array[$row[csf('po_breakdown_id')]][$row[csf('item_group_id')]]['issue_qty']=$row[csf('quantity')];
			//$issue_qty_array[$row[csf('po_breakdown_id')]][$row[csf('item_group_id')]]['rate']=$row[csf('rate')];
			//$receive_qty_array[$row[csf('po_breakdown_id')]][$row[csf('item_group_id')]][value]+=$row[csf('rate')];
		}
	//print_r($trim_group_arr);
	if($template==1)
	{
		ob_start();
	?>
		<div style="width:1780px">
		<fieldset style="width:100%;">	
			<table width="1250">
				<tr class="form_caption">
					<td colspan="12" align="center">Accessories Followup Report</td>
				</tr>
				<tr class="form_caption">
					<td colspan="12" align="center"><? echo $company_library[$company_name]; ?></td>
				</tr>
			</table>
			<table class="rpt_table" width="1250" cellpadding="0" cellspacing="0" border="1" rules="all">
				<thead>
					<th width="30">SL</th>
					<th width="100">Buyer Name</th>
					<th width="100">Job No</th>
					<th width="100">Style Ref</th>
					<th width="200">Order No</th>
					<th width="100">Order Qnty</th>
					<th width="100">Item Group</th>
					<th width="50">UOM</th>
					<th width="100">Rev. Qty</th>
					<th width="100">Issue Qty.</th>
                    <th width="100">Left Over</th>
					<th>Remarks</th>
				</thead>
			</table>
		<div style="width:1270px; max-height:400px; overflow-y:scroll" id="scroll_body">
				<table class="rpt_table" width="1250" cellpadding="0" cellspacing="0" border="1" rules="all" id="table_body">
				<?
				$i=1; $s=1; $total_po_qnty=0; $total_req_qnty=0; $total_order_qnty_in_pcs=0; $total_wo_qnty=0; $total_in_qnty=0; $item_array=array(); $uom_array=array(); $item_app_array=array();
				if($db_type==2)
				{
				$sql_style="select max(a.job_no_prefix_num) as job_no_prefix_num,a.job_no, max(a.buyer_name) as buyer_name, a.style_ref_no,listagg(cast((b.id ||'**'||b.po_number) as varchar(4000)),',') within group (order by null) as po_num, sum(b.po_quantity)as po_qty from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.company_name='$company_name' and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 $buyer_id_cond $date_cond $year_cond $job_no_cond $style_ref_cond group by  a.job_no,a.style_ref_no order by a.job_no"; //and a.buyer_name like '$buyer_name' and b.pub_shipment_date between '$start_date' and '$end_date'
				}
				else if($db_type==0)
				{
				$sql_style="select a.job_no_prefix_num, a.job_no, a.buyer_name, a.style_ref_no,group_concat(concat_ws('**',b.id,b.po_number)) as po_num, sum(b.po_quantity)as po_qty from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.company_name='$company_name' and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 $buyer_id_cond $date_cond $year_cond $job_no_cond $style_ref_cond group by a.job_no,a.style_ref_no order by a.buyer_name,a.job_no"; //and a.buyer_name like '$buyer_name' and b.pub_shipment_date between '$start_date' and '$end_date'
				}	
				$nameArray=sql_select($sql_style);
				$tot_rows=count($nameArray);
				foreach($nameArray as $row )
				{
					if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
					$po_num_all=explode(',',$row[csf('po_num')]);
					//print_r( $po_num_all);die;
					$po_num="";
					$po_dataArray=array();
					foreach( $po_num_all as $val_a)
					{
						$id_val=explode('**',$val_a);
						$po_id=$id_val[0];
						if($po_num=="") $po_num=$id_val[1]; else $po_num.=",".$id_val[1];
						
					}
					
					?>
					<tr bgcolor="<? echo $bgcolor;?>" onclick="change_color('tr_<? echo $s; ?>','<? echo $bgcolor;?>')" id="tr_<? echo $s; ?>">
						<td width="30"><? echo $i; ?></td>
						<td width="100"><? echo $buyer_short_name_library[$row[csf('buyer_name')]]; ?></td>
						<td width="100" align="center"><? echo $row[csf('job_no_prefix_num')]; ?></td>
						<td width="100"><p><? echo $row[csf('style_ref_no')]; ?></p></td>
						<td width="200"><p><? echo $po_num; ?></p></td>
						<td width="100" align="right"><? $total_po_qnty+=$row[csf('po_qty')]; echo $row[csf('po_qty')]; ?></td>  
                        <?
						$k=1;
						//print_r($trim_group_arr[$row[csf('job_no')]]);
						foreach($trim_group_id_arr as $val=>$key)
						{
							//echo $val['trim'];
							foreach($key as $id=>$name)
							{

							//echo $name['trim'].'<br>';
						   $rev_qty=0;
						   $iss_qty=0;
						   foreach( $po_num_all as $val_b)
							{
								$po_val=explode('**',$val_b);
								$po_no_id=$po_val[0];
								$rev_qty+=$receive_qty_array[$po_no_id][$name['trim']]['receive_qty'];
								//$inhouse_qnty=$receive_qty_array[$order_id][$trim_id]['receive_qty'];
								$iss_qty+=$issue_qty_array[$po_no_id][$name['trim']]['issue_qty'];
								
							} //echo $po_no_id;;
						//print_r( $val);
						 								
						if($k==1)
						{
								//print($item_library[$trim_uom_arr[$row[csf('job_no')]][$val['trim']]]);
						?>
						<td width="100" align="center"><? echo $item_library[$name['trim']];//$item_library[$val]; ?></td>
						<td width="50" align="center"><? echo $unit_of_measurement[$name['uom']];//$unit_of_measurement[$trim_uom_arr[$row[csf('job_no')]][$val]["uom"]];?></td>
                        <td width="100" align="right"><? echo number_format($rev_qty,2,'.',''); ?></td>
						<td width="100" align="right"><? echo number_format($iss_qty,2,'.','') ?></td>
						<td width="100" align="right"><? echo number_format(($rev_qty-$iss_qty),2,'.',''); ?></td>
						<td  align="center"></td>
                        </tr>
                        <?
						}
						else
						{
						?>
							<tr bgcolor="<? echo $bgcolor;?>" onclick="change_color('tr_<? echo $s; ?>','<? echo $bgcolor;?>')" id="tr_<? echo $s; ?>">
								<td width="30">
									<font style="display:none" color="<? echo $bgcolor; ?>">
										<? echo $i; ?>
									</font>
								</td>
								<td width="100">
									<font style="display:none" color="<? echo $bgcolor; ?>">
										<? echo $buyer_short_name_library[$row[csf('buyer_name')]]; ?>
									</font>
								</td>
								<td width="100">
									<font style="display:none" color="<? echo $bgcolor; ?>">
										<? echo $row[csf('job_no_prefix_num')]; ?>
									</font>
								</td>
								<td width="100">
									<font style="display:none" color="<? echo $bgcolor; ?>">
										<p>
											<? echo $row[csf('style_ref_no')]; ?>
										</p>
									</font>
								</td>
								<td width="200">
									<font style="display:none" color="<? echo $bgcolor; ?>">
										<p>
											<? echo $row[csf('po_number')]; ?>
										</p>
									</font>
								</td>
								<td width="100" align="right">
									<font style="display:none" color="<? echo $bgcolor; ?>">
										<? //echo number_format($row[csf('po_quantity')],0,'.',''); ?>
									</font>
								</td>
								
								<td width="100" align="center"><p><? echo  $item_library[$name['trim']]; ?></p></td>
								<td width="50" align="center"><? echo $unit_of_measurement[$name['uom']];//$unit_of_measurement[ $trim_uom_arr[$row[csf('job_no')]][$val]["uom"]]; ?></td>
                                <td width="100" align="right"><?  echo number_format($rev_qty,2,'.',''); ?></td>
								<td width="100" align="right"><? echo number_format($iss_qty,2,'.','') ?></td>
								<td width="100" align="right"><? echo number_format(($rev_qty-$iss_qty),2,'.',''); ?></td>
								<td align="right"></td>
							</tr>
						<?
						}
						 $k++;
					     $s++;	
                         $total_issue_qnty+=$iss_qty;
                         $total_receiv_qnty+=$rev_qty;
					}
				if(count($trim_group_id_arr)<1)
					{
					?>
						<td width="100" align="center"></td>
						<td width="50" align="center"></td>
                        <td width="100" align="right"></td>
						<td width="100" align="right"></td>
						<td width="100" align="right"></td>
						<td  align="center"></td>
                        </tr>
					<?
					} 
				$i++;
						}
				}
				?>
			</table>
            <table class="rpt_table" width="1250" cellpadding="0" cellspacing="0" border="1" rules="all">
					<tfoot>
                       <tr>
                            <th width="30"></th>
                            <th width="100"></th>
                            <th width="100" align="center"></th>
                            <th width="100"><p></p></th>
                            <th width="200"><p></p></th>
                            <th width="100" align="right" id="total_order_qnty"><? echo number_format($total_po_qnty,2); ?></th>
                            <th width="100" align="right" id="value_pre_costing"></th>
                            <th width="50" align="right" id=""></th>
                            <th width="100" align="right" id="value_rec_qty"><? echo number_format($total_receiv_qnty,2); ?></th>
                            <th width="100" align="right" id="value_issue_qty"><?  echo number_format($total_issue_qnty,2); ?></th>
                            <th width="100" align="right" id="value_leftover_qty"><? echo number_format(($total_receiv_qnty-$total_issue_qnty),2); ?></th>
                            <th align="right" id=""></th>
                           </tr>
					</tfoot>
				</table>
            </div>	
			</fieldset>
		</div>
	<?
	}
}
	foreach (glob("*.xls") as $filename) {
	//if( @filemtime($filename) < (time()-$seconds_old) )
	@unlink($filename);
	}
	//---------end------------//
	$name=time();
	$filename=$name.".xls";
	$create_new_doc = fopen($filename, 'w');	
	$is_created = fwrite($create_new_doc,ob_get_contents());
	echo "$total_data****$filename****$tot_rows";
	exit();	
}

if($action=="booking_info")
{
	echo load_html_head_contents("Job Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
	?>
	<!--<div style="width:880px" align="center"><input type="button" value="Print Preview" onClick="print_window()" style="width:100px"  class="formbutton"/></div>-->
	<fieldset style="width:870px; margin-left:3px">
		<div id="scroll_body" align="center">
        <table border="1" class="rpt_table" rules="all" width="850" cellpadding="0" cellspacing="0" align="center">
        <tr>
        <td align="center" colspan="8"><strong> WO  Summary</strong> </td>
         </tr>
        </table>
			<table border="1" class="rpt_table" rules="all" width="850" cellpadding="0" cellspacing="0" align="center">
				<thead>
                    <th width="30">Sl</th>
                    <th width="100">Wo No</th>
                    <th width="75">Wo Date</th>
                     <th width="100">Country</th>
                     <th width="200">Item Description</th>
                    <th width="80">Wo Qty</th>
                    <th width="60">UOM</th>
                    <th width="100">Supplier</th>
				</thead>
                <tbody>
                <?
				
					
					$conversion_factor_array=array();
					$supplier_arr=return_library_array( "select id, supplier_name from lib_supplier", "id", "supplier_name"  );
					$conversion_factor=sql_select("select id ,conversion_factor from  lib_item_group ");
					foreach($conversion_factor as $row_f)
					{
					$conversion_factor_array[$row_f[csf('id')]]['con_factor']=$row_f[csf('conversion_factor')];
					}
					
					$i=1;
					$country_arr_data=array();
					$sql_data=sql_select("select c.country_id,c.po_break_down_id,c.job_no_mst from wo_po_color_size_breakdown c  where c.po_break_down_id=$po_id and c.status_active=1 and c.is_deleted=0 group by c.country_id,c.po_break_down_id,c.job_no_mst  ");
					foreach($sql_data as $row_c)
					{
					$country_arr_data[$row_c[csf('po_break_down_id')]][$row_c[csf('job_no_mst')]]['country']=$row_c[csf('country_id')];
					} //var_dump($country_arr_data);
						
					$item_description_arr=array();
					$wo_sql_trim=sql_select("select b.id,b.item_color,b.job_no, b.po_break_down_id, b.description,b.brand_supplier,b.item_size from wo_booking_dtls a, wo_trim_book_con_dtls b where a.id=b.wo_trim_booking_dtls_id and a.is_deleted=0 and a.status_active=1 and a.job_no=b.job_no  group by b.id,b.po_break_down_id,b.job_no,b.description,b.brand_supplier,b.item_size,b.item_color");
					foreach($wo_sql_trim as $row_trim)
					{
					$item_description_arr[$row_trim[csf('po_break_down_id')]][$row_trim[csf('job_no')]]['job_no']=$row_trim[csf('job_no')];
					$item_description_arr[$row_trim[csf('po_break_down_id')]][$row_trim[csf('job_no')]]['description']=$row_trim[csf('description')];
					//$item_description_arr[$row_trim[csf('po_break_down_id')]][$row_trim[csf('job_no')]]['size']=$row_trim[csf('item_size')];
					$item_description_arr[$row_trim[csf('po_break_down_id')]][$row_trim[csf('job_no')]]['supplier']=$row_trim[csf('brand_supplier')];	
					//$item_description_arr[$row_trim[csf('po_break_down_id')]][$row_trim[csf('job_no')]]['color']=$row_trim[csf('item_color')];		
	
					} //var_dump($item_description_arr);
					
					$boking_cond="";
					$booking_no= explode(',',$book_num);
					foreach($booking_no as $book_row)
					{
						if($boking_cond=="") $boking_cond="and a.booking_no in('$book_row'"; else  $boking_cond .=",'$book_row'";
						
					} 
					if($boking_cond!="")$boking_cond.=")";
					//echo $book_row;
					 $wo_sql="select a.booking_no, a.booking_date, a.supplier_id,b.job_no,b.country_id_string, b.po_break_down_id,sum(b.wo_qnty) as wo_qnty,b.uom from wo_booking_mst a, wo_booking_dtls b 
					where  a.item_category=4 and a.booking_no=b.booking_no  and a.is_deleted=0 and a.status_active=1 
					and b.status_active=1 and b.is_deleted=0 and  b.job_no='$job_no' and b.trim_group=$item_name and b.po_break_down_id='$po_id' $boking_cond group by  b.po_break_down_id,b.job_no,
					a.booking_no, a.booking_date, a.supplier_id,b.uom,b.country_id_string";
					//echo $wo_sql;
					$dtlsArray=sql_select($wo_sql);
					
					foreach($dtlsArray as $row)
					{
						if ($i%2==0)  
							$bgcolor="#E9F3FF";
						else
							$bgcolor="#FFFFFF";	
							$item_group=$item_library[$item_name];
							//$job_no=$row[csf('job_no')];
							$item_descrp=$item_description_arr[$row[csf('po_break_down_id')]][$row[csf('job_no')]]['description'];
							//$item_size=$item_description_arr[$row[csf('po_break_down_id')]][$row[csf('job_no')]]['size'];
							$supplier=$item_description_arr[$row[csf('po_break_down_id')]][$row[csf('job_no')]]['supplier'];
							//$item_color=$item_description_arr[$row[csf('po_break_down_id')]][$row[csf('job_no')]]['color'];
							$item_des=$item_group.','.$item_descrp.','.$supplier;
							//$item_d2=$supplier;
							if($item_group!='' || $item_descrp!='' || $supplier!='')
							{
							$item_all=$item_group.','.$item_descrp.','.$supplier;	
							}
							else
							{
							$item_all='';	
							}
							
							$country_a=$country_arr_data[$row[csf('po_break_down_id')]][$row[csf('job_no')]]['country'];
							//echo $po_id.'-'.$job_no
							//echo $row[csf('po_break_down_id')];
							$conversion_factor_rate=$conversion_factor_array[$item_name]['con_factor'];
							//echo $conversion_factor_rate; $country_name_library[
							$country_arr_data=explode(',',$row[csf('country_id_string')]);
							$country_name_data="";
							foreach($country_arr_data as $country_row)
								{
									if($country_name_data=="") $country_name_data=$country_name_library[$country_row]; else $country_name_data.=",".$country_name_library[$country_row];
								}
						?>
						<tr bgcolor="<? echo  $bgcolor; ?>" onClick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor;?>')" id="tr_<? echo $i;?>">
							<td width="30"><p><? echo $i; ?></p></td>
                            <td width="100"><p><? echo $row[csf('booking_no')]; ?></p></td>
                            <td width="75"><p><? echo change_date_format($row[csf('booking_date')]); ?></p></td>
                             <td width="100"><p><? echo $country_name_data//$row[csf('country_id_string')]; ?></p></td>
                             <td width="200"><p><?  echo $item_all; ?></p></td>
                            <td width="80" align="right" title="<? echo 'conversion_factor='.$conversion_factor_rate; ?>"><p><? echo number_format($row[csf('wo_qnty')]*$conversion_factor_rate,2); ?></p></td>
                            <td width="60" align="center" ><p><? echo $unit_of_measurement[$row[csf('uom')]]; ?></p></td>
                            <td width="100"><p><? echo $supplier_arr[$row[csf('supplier_id')]]; ?></p></td>
                        </tr>
						<?
						$tot_qty+=$row[csf('wo_qnty')]*$conversion_factor_rate;
						$i++;
					}
				?>
                </tbody>
                <tfoot>
                	<tr class="tbl_bottom">
                   		 <td colspan="5" align="right">Total</td>
                    	<td  align="right"><? echo number_format($tot_qty,2); ?></td>
                        <td align="right">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </fieldset>
    <?
	exit();
}
disconnect($con);
?>
<?
if($action=="booking_inhouse_info")
{
	echo load_html_head_contents("Job Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
	?>
	<!--<div style="width:880px" align="center"><input type="button" value="Print Preview" onClick="print_window()" style="width:100px"  class="formbutton"/></div>-->
	<fieldset style="width:870px; margin-left:3px">
		<div id="scroll_body" align="center">
			<table border="1" class="rpt_table" rules="all" width="850" cellpadding="0" cellspacing="0" align="center">
				<thead>
                    <th width="30">Sl</th>
                    <th width="80">Prod. ID</th>
                    <th width="100">Recv. ID</th>
                    <th width="100">Chalan No</th>
                    <th width="100">Recv. Date</th>
                    <th width="80">Item Description.</th>
                    <th width="100">Recv. Qty.</th>
				</thead>
                <tbody>
                <?
					$supplier_arr=return_library_array( "select id, supplier_name from lib_supplier", "id", "supplier_name"  );
					$i=1;
					//$wo_sql="select a.item_group_id,a.prod_id,b.recv_number,b.receive_date,a.item_description,sum(a.cons_qnty) as cons_qnty  from inv_receive_master b, inv_trims_entry_dtls a where b.id=a.mst_id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and b.item_category=4 group by a.item_group_id,a.prod_id,b.recv_number,b.receive_date,a.item_description";
					
					//echo $receive_qty_data=("select b.po_breakdown_id,c.id as prod_id,c.item_description,d.recv_number,d.receive_date, a.item_group_id,sum(b.quantity) as quantity from  inv_receive_master d,inv_trims_entry_dtls a ,order_wise_pro_details b,product_details_master c where d.id=a.mst_id and a.trans_id=b.trans_id and b.trans_type=1 and a.item_group_id='$item_name' and b.po_breakdown_id=$po_id and b.entry_form=24 and a.prod_id=c.id and c.id=b.prod_id and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by b.po_breakdown_id,a.item_group_id,c.item_description,d.recv_number,d.receive_date, a.item_group_id,c.id");
					$receive_qty_data=("select a.id, c.po_breakdown_id,b.item_group_id,b.prod_id as prod_id,a.challan_no,b.item_description, a.recv_number, a.receive_date, SUM(c.quantity) as quantity
					from inv_receive_master a, inv_trims_entry_dtls b, order_wise_pro_details c 
					where a.id=b.mst_id  and a.entry_form=24 and  a.item_category=4  and b.id=c.dtls_id and c.trans_type=1 and  c.po_breakdown_id in($po_id)  and b.item_group_id='$item_name' and a.is_deleted=0 and a.status_active=1 and b.status_active=1 and b.is_deleted=0 group by  c.po_breakdown_id,b.item_group_id,b.prod_id,a.id,b.item_description, a.recv_number,a.challan_no, a.receive_date");

					$dtlsArray=sql_select($receive_qty_data);
					
					foreach($dtlsArray as $row)
					{
						if ($i%2==0)  
							$bgcolor="#E9F3FF";
						else
							$bgcolor="#FFFFFF";	
							
						?>
						<tr bgcolor="<? echo  $bgcolor; ?>" onClick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor;?>')" id="tr_<? echo $i;?>">
							<td width="30"><p><? echo $i; ?></p></td>
                            <td width="80"><p><? echo $row[csf('prod_id')]; ?></p></td>
                            <td width="100" align="center"><p><? echo $row[csf('recv_number')]; ?></p></td>
                            <td width="100" align="center"><p><? echo $row[csf('challan_no')]; ?></p></td>
                            <td width="100" align="center"><p><? echo  change_date_format($row[csf('receive_date')]); ?></p></td>
                            <td width="80" align="center"><p><? echo $row[csf('item_description')]; ?></p></td>
                            <td width="100" align="right"><p><? echo number_format($row[csf('quantity')],2); ?></p></td>
                        </tr>
						<?
						$tot_qty+=$row[csf('quantity')];
						$i++;
					}
				?>
                </tbody>
                <tfoot>
                	<tr class="tbl_bottom">
                    	<td colspan="5" align="right"></td>
                        <td align="right">Total</td>
                        <td><? echo number_format($tot_qty,2); ?></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </fieldset>
    <?
	exit();
}
disconnect($con);
?>

<?				
if($action=="booking_issue_info")
{
	echo load_html_head_contents("Job Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
	?>
<!--	<div style="width:880px" align="center"><input type="button" value="Print Preview" onClick="print_window()" style="width:100px"  class="formbutton"/></div>
-->	<fieldset style="width:870px; margin-left:3px">
		<div id="scroll_body" align="center">
			<table border="1" class="rpt_table" rules="all" width="850" cellpadding="0" cellspacing="0" align="center">
				<thead>
                    <th width="30">Sl</th>
                    <th width="80">Prod. ID</th>
                    <th width="100">Issue. ID</th>
                     <th width="100">Chalan No</th>
                     <th width="100">Issue. Date</th>
                    <th width="80">Item Description.</th>
                    <th width="100">Issue. Qty.</th>
				</thead>
                <tbody>
                <?
					$supplier_arr=return_library_array( "select id, supplier_name from lib_supplier", "id", "supplier_name"  );
					$i=1;
					//$wo_sql="select a.item_group_id,a.prod_id,b.recv_number,b.receive_date,a.item_description,sum(a.cons_qnty) as cons_qnty  from inv_receive_master b, inv_trims_entry_dtls a where b.id=a.mst_id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and b.item_category=4 group by a.item_group_id,a.prod_id,b.recv_number,b.receive_date,a.item_description";
					
				 $mrr_sql=("select a.id, a.issue_number,a.challan_no,b.prod_id, a.issue_date,b.item_description,SUM(c.quantity) as quantity
					from  inv_issue_master a,inv_trims_issue_dtls b, order_wise_pro_details c,product_details_master p 
					where a.id=b.mst_id  and a.entry_form=25 and p.id=b.prod_id and b.id=c.dtls_id and c.trans_type=2 and a.is_deleted=0 and a.status_active=1 and
					b.status_active=1 and b.is_deleted=0 and  c.po_breakdown_id in($po_id) and p.item_group_id='$item_name' group by c.po_breakdown_id,p.item_group_id,b.item_description,a.issue_number,a.id,a.issue_date,b.prod_id,a.challan_no ");					
					
					$dtlsArray=sql_select($mrr_sql);
					
					foreach($dtlsArray as $row)
					{
						if ($i%2==0)  
							$bgcolor="#E9F3FF";
						else
							$bgcolor="#FFFFFF";	
							
						?>
						<tr bgcolor="<? echo  $bgcolor; ?>" onClick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor;?>')" id="tr_<? echo $i;?>">
							<td width="30"><p><? echo $i; ?></p></td>
                            <td width="80" align="center"><p><? echo $row[csf('prod_id')]; ?></p></td>
                            <td width="100"><p><? echo $row[csf('issue_number')]; ?></p></td>
                            <td width="100" align="center"><p><? echo $row[csf('challan_no')]; ?></p></td>
                            <td width="100" align="center"><p><? echo  change_date_format($row[csf('issue_date')]); ?></p></td>
                            <td width="80" align="center"><p><? echo $row[csf('item_description')]; ?></p></td>
                            <td width="100" align="right"><p><? echo number_format($row[csf('quantity')],2); ?></p></td>
                        </tr>
						<?
						$tot_qty+=$row[csf('quantity')];
						$i++;
					}
				?>
                </tbody>
                <tfoot>
                	<tr class="tbl_bottom">
                    	<td colspan="5" align="right"></td>
                        <td align="right">Total</td>
                        <td><? echo number_format($tot_qty,2); ?></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </fieldset>
    <?
	exit();
}
disconnect($con);
?>
<?
if($action=="order_qty_data")
{
	echo load_html_head_contents("Job Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
	?>
<!--	<div style="width:780px" align="center"><input type="button" value="Print Preview" onClick="print_window()" style="width:100px"  class="formbutton"/></div>
-->	<fieldset style="width:770px; margin-left:3px">
		<div id="scroll_body" align="center">
			<table border="1" class="rpt_table" rules="all" width="750" cellpadding="0" cellspacing="0" align="center">
				<thead>
                    <th width="30">Sl</th>
                    <th width="80">Buyer Name</th>
                    <th width="100">Order No</th>
                     <th width="100">Country</th>
                    <th width="80">Order Qty.</th>
                   
				</thead>
                <tbody>
                <?
					$i=1;
					
				 $gmt_item_id=return_field_value("item_number_id", "wo_po_color_size_breakdown", "po_break_down_id='$po_id'");
				$country_id=return_field_value("country_id", "wo_po_color_size_breakdown", "po_break_down_id='$po_id'");
					 //echo $gmt_item_id;
					 $sql_po_qty=sql_select("select sum(c.order_quantity) as order_quantity,c.country_id  from wo_po_details_master a, wo_po_break_down b,wo_po_color_size_breakdown c where a.job_no=b.job_no_mst and a.job_no=c.job_no_mst and b.id=c.po_break_down_id and  b.id='".$po_id."' and c.item_number_id=' $gmt_item_id' and a.status_active=1 and b.status_active=1 and c.status_active=1 group by b.id,c.country_id ");
					list($sql_po_qty_row)=$sql_po_qty;
					$po_qty=$sql_po_qty_row[csf('order_quantity')];
					
					$fabriccostArray=sql_select("select costing_per_id, trims_cost, embel_cost, cm_cost, commission, common_oh, lab_test, inspection, freight, comm_cost from wo_pre_cost_dtls where job_no='".$job_no."' and status_active=1 and is_deleted=0");
                        
                       
					
					$sql=" select sum( c.order_quantity) as po_quantity ,c.country_id from wo_po_color_size_breakdown c  where c.po_break_down_id=$po_id and c.status_active=1 and c.is_deleted=0 group by c.country_id";					
					
					$dtlsArray=sql_select($sql);
					
					foreach($dtlsArray as $row)
					{
						if ($i%2==0)  
							$bgcolor="#E9F3FF";
						else
							$bgcolor="#FFFFFF";	
							
						?>
						<tr bgcolor="<? echo  $bgcolor; ?>" onClick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor;?>')" id="tr_<? echo $i;?>">
							<td width="30"><p><? echo $i; ?></p></td>
                            <td width="80" align="center"><p><? echo $buyer_short_name_library[$buyer]; ?></p></td>
                            <td width="100"><p><? echo $order_arr[$po_id]; ?></p></td>
                             <td width="100" align="center"><p><? echo $country_name_library[$row[csf('country_id')]]; ?></p></td>
                            <td width="80" align="right"><p><? echo number_format($po_qty,2); ?></p></td>
                           
                        </tr>
						<?
						$tot_qty+=$po_qty;
						$i++;
					}
				?>
                </tbody>
                <tfoot>
                	<tr class="tbl_bottom">
                    	<td colspan="3" align="right"></td>
                        <td align="right">Total</td>
                        <td><? echo number_format($tot_qty,2); ?></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </fieldset>
    <?
	exit();
}
disconnect($con);
?>
<?
if($action=="order_req_qty_data")
{
	echo load_html_head_contents("Job Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
	
	?>
<!--	<div style="width:680px" align="center"><input type="button" value="Print Preview" onClick="print_window()" style="width:100px"  class="formbutton"/></div>
-->	<fieldset style="width:670px; margin-left:3px">
		<div id="scroll_body" align="center">
			<table border="1" class="rpt_table" rules="all" width="650" cellpadding="0" cellspacing="0" align="center">
				<thead>
                    <th width="30">Sl</th>
                    <th width="80">Buyer Name</th>
                    <th width="100">Order No</th>
                     <th width="100">Item Description</th>
                     <th width="100">Country</th>
                    <th width="80">Req. Qty.</th>
                    <th width="">Req. Rate</th>
                   
				</thead>
                <tbody>
                <? 
				
					 $gmt_item_id=return_field_value("item_number_id", "wo_po_color_size_breakdown", "po_break_down_id='$po_id'");
					 $country_id=return_field_value("country_id", "wo_po_color_size_breakdown", "po_break_down_id='$po_id'");
					 //echo $gmt_item_id;
					 $sql_po_qty=sql_select("select sum(c.order_quantity) as order_quantity,c.country_id  from wo_po_details_master a, wo_po_break_down b,wo_po_color_size_breakdown c where a.job_no=b.job_no_mst and a.job_no=c.job_no_mst and b.id=c.po_break_down_id and  b.id='".$po_id."' and c.item_number_id=' $gmt_item_id' and a.status_active=1 and b.status_active=1 and c.status_active=1 group by b.id,c.country_id ");
					list($sql_po_qty_row)=$sql_po_qty;
					$po_qty=$sql_po_qty_row[csf('order_quantity')];
					
					
					
					$req_arr=array();
					$red_data=sql_select("select a.id,a.job_no,a.cons, a.po_break_down_id  from wo_pre_cost_trim_co_cons_dtls a , wo_pre_cost_trim_cost_dtls b where b.id=a.wo_pre_cost_trim_cost_dtls_id and b.trim_group=$item_group and a.job_no='$job_no' and a.po_break_down_id=$po_id ");
					foreach($red_data as $row_data)
					{
					$req_arr[$row_data[csf('po_break_down_id')]][$row_data[csf('job_no')]]['cons']=$row_data[csf('cons')];
					}
					
					$wo_sql_trim=sql_select("select b.id,b.job_no, b.po_break_down_id, b.description from wo_booking_dtls a, wo_trim_book_con_dtls b where a.id=b.wo_trim_booking_dtls_id and a.is_deleted=0 and a.status_active=1 and a.job_no=b.job_no  group by b.id,b.po_break_down_id,b.job_no,b.description ");
					foreach($wo_sql_trim as $row_trim)
					{
					$item_description_arr[$row_trim[csf('po_break_down_id')]][$row_trim[csf('job_no')]]['job_no']=$row_trim[csf('job_no')];
					$item_description_arr[$row_trim[csf('po_break_down_id')]][$row_trim[csf('job_no')]]['description']=$row_trim[csf('description')];
						
	
					}
						
				$fabriccostArray=sql_select("select costing_per_id, trims_cost, embel_cost, cm_cost, commission, common_oh, lab_test, inspection, freight, comm_cost from wo_pre_cost_dtls where job_no='".$job_no."' and status_active=1 and is_deleted=0");
                        
                        $dzn_qnty=0;
                        if($fabriccostArray[0][csf('costing_per_id')]==1)
                        {
                            $dzn_qnty=12;
                        }
                        else if($fabriccostArray[0][csf('costing_per_id')]==3)
                        {
                            $dzn_qnty=12*2;
                        }
                        else if($fabriccostArray[0][csf('costing_per_id')]==4)
                        {
                            $dzn_qnty=12*3;
                        }
                        else if($fabriccostArray[0][csf('costing_per_id')]==5)
                        {
                            $dzn_qnty=12*4;
                        }
                        else
                        {
                            $dzn_qnty=1;
                        }
						$costing_per=$fabriccostArray[0][csf('costing_per_id')];
					
					$i=1;
									
				 $sql=" select  sum(c.order_quantity) as po_quantity ,c.country_id as country_id from wo_po_color_size_breakdown c  
			where   c.job_no_mst='$job_no' and c.po_break_down_id=$po_id and c.status_active=1 and c.is_deleted=0 group by c.country_id ";
			 			
					$dtlsArray=sql_select($sql);						
					foreach($dtlsArray as $row)
					{
						if ($i%2==0)  
							$bgcolor="#E9F3FF";
						else
							$bgcolor="#FFFFFF";	
							//*$row[csf('rate')]
							 $cons=$req_arr[$po_id][$job_no]['cons'];
							  //echo $row[csf('po_quantity')];
								//echo $row[csf('po_quantity')]/$cons;
							//echo $req_rate=$data_array_req[0][csf('cons_dzn_gmts')];
							$req_qty=($row[csf('po_quantity')]/$dzn_qnty)*$cons;
							//$sql_lib_item_group_array[$selectResult[csf('trim_group')]][conversion_factor];
							
							//$req_qty=def_number_format(($cons*($po_qty/$dzn_qnty))/$sql_lib_item_group_array[$item_group][conversion_factor],5,"");

							//echo $boook_no;
							$descript=$item_description_arr[$po_id][$job_no]['description'];
							//echo $row[csf('po_quantity')]; $country_name_library
						?>
						<tr bgcolor="<? echo  $bgcolor; ?>" onClick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor;?>')" id="tr_<? echo $i;?>">
							<td width="30"><p><? echo $i; ?></p></td>
                            <td width="80" align="center"><p><? echo $buyer_short_name_library[$buyer]; ?></p></td>
                            <td width="100"><p><? echo $order_arr[$po_id]; ?></p></td>
                            <td width="100"><p><? echo $data_array_req[0][csf('description')]; //$descript; ?></p></td>
                            <td width="100" align="center"><p><? echo  $country_name_library[$row[csf('country_id')]]; ?></p></td>
                            <td width="80" align="right"><p><? echo number_format($req_qty,2); ?></p></td>
                            <td width="" align="right"><p><? echo number_format($rate,4); ?></p></td>
                           
                        </tr>
						<?
						$tot_qty+=$req_qty;
						$i++;
					}
				?>
                </tbody>
                <tfoot>
                	<tr class="tbl_bottom">
                    	<td  align="right"></td>
                    	<td colspan="4" align="right">Total</td>
                        <td align="right"><? echo number_format($tot_qty,2); ?> </td>
                        <td></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </fieldset>
    <?
	exit();
}
disconnect($con);
?>