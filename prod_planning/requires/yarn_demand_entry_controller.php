<?php
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
extract($_REQUEST);
$permission=$_SESSION['page_permission'];

include('../../includes/common.php');

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

$buyer_arr=return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
$company_arr=return_library_array( "select id, company_short_name from lib_company",'id','company_short_name');
$supllier_arr=return_library_array( "select id, supplier_name from lib_supplier",'id','supplier_name');
$color_library=return_library_array( "select id,color_name from lib_color", "id", "color_name"  );
$count_arr=return_library_array( "select id, yarn_count from lib_yarn_count",'id','yarn_count');
$brand_arr=return_library_array( "select id, brand_name from lib_brand",'id','brand_name');

$product_desc_array=array(); $product_details_array=array();
$sql="select id, supplier_id, lot, current_stock, yarn_comp_type1st, yarn_comp_percent1st, yarn_comp_type2nd, yarn_comp_percent2nd, yarn_count_id, yarn_type, color, brand from product_details_master where item_category_id=1 and status_active=1 and is_deleted=0";
$result = sql_select($sql);

foreach($result as $row)
{
	$compos=''; $desc='';
	if($row[csf('yarn_comp_percent2nd')]!=0)
	{
		$compos=$composition[$row[csf('yarn_comp_type1st')]]." ".$row[csf('yarn_comp_percent1st')]."%"." ".$composition[$row[csf('yarn_comp_type2nd')]]." ".$row[csf('yarn_comp_percent2nd')]."%";
	}
	else
	{
		$compos=$composition[$row[csf('yarn_comp_type1st')]]." ".$row[csf('yarn_comp_percent1st')]."%"." ".$composition[$row[csf('yarn_comp_type2nd')]];
	}
	
	$desc=$row[csf('lot')]." ".$count_arr[$row[csf('yarn_count_id')]]." ".$compos." ".$yarn_type[$row[csf('yarn_type')]]." ".$color_library[$row[csf('color')]];
	
	$product_desc_array[$row[csf('id')]]=$desc;
	
	$product_details_array[$row[csf('id')]]['count']=$count_arr[$row[csf('yarn_count_id')]];
	$product_details_array[$row[csf('id')]]['comp']=$compos;
	$product_details_array[$row[csf('id')]]['type']=$yarn_type[$row[csf('yarn_type')]];
	$product_details_array[$row[csf('id')]]['lot']=$row[csf('lot')];
	$product_details_array[$row[csf('id')]]['color']=$color_library[$row[csf('color')]];
	$product_details_array[$row[csf('id')]]['suppl']=$supllier_arr[$row[csf('supplier_id')]];
	$product_details_array[$row[csf('id')]]['brand']=$brand_arr[$row[csf('brand')]];
}

if ($action=="load_drop_down_location")
{
	$data=explode("_",$data);
	echo create_drop_down( "cbo_location", 152, "select id,location_name from lib_location where company_id='$data[0]' and status_active =1 and is_deleted=0 order by location_name","id,location_name", 1, "-- Select Location --", 0, "" );
	exit();
}

if($action=="yarn_reqsn_popup")
{
	echo load_html_head_contents("Requisition Info", "../../", 1, 1,'','','');
	extract($_REQUEST); 
	
	$data=explode("_",$data);
	$reqsn_no=$data[0]; $type=$data[1];

	if($type==1) 
	{
		$save_data=$data[2]; 
	}
	
	?>
     
	<script>
	
		function js_set_value(reqsn_no)
		{
			$('#reqsn_no').val(reqsn_no);
			show_list_view (reqsn_no+'_'+'1'+'_'+'<?php echo $save_data; ?>', 'yarn_reqsn_popup', 'search_div', 'yarn_demand_entry_controller', '');
		}
		
		function color_row(tr_id)
		{
			var txt_demand_qnty=$('#txt_demand_qnty_'+tr_id).val()*1;
			var reqsn_qnty=$('#txt_reqsn_qnty_'+tr_id).val()*1;

			if(txt_demand_qnty>reqsn_qnty)
			{
				alert("Demand Qnty Exceeds Requisition Qnty.");
				$('#txt_demand_qnty_'+tr_id).val('');
				$('#search' + tr_id).css('background-color','#FFFFCC');
			}
			else
			{
				if(txt_demand_qnty>0)
				{
					$('#search' + tr_id).css('background-color','yellow');
				}
				else
				{
					$('#search' + tr_id).css('background-color','#FFFFCC');
				}
			}
			var tot_row=$("#tbl_list_search tbody tr").length;

			var ddd={ dec_type:2, comma:0, currency:''}
			
			math_operation( "txt_total_demand_qnty", "txt_demand_qnty_", "+", tot_row,ddd );
		}
		
		function fnc_close()
		{
			var save_data=''; var tot_demand_qnty='';

			$("#tbl_list_search").find('tr').each(function()
			{
				var productId=$(this).find('input[name="productId[]"]').val();
				var reqsId=$(this).find('input[name="reqsId[]"]').val();
				var txt_demand_qnty=$(this).find('input[name="txt_demand_qnty[]"]').val();

				if(txt_demand_qnty*1>0)
				{
					if(save_data=="")
					{
						save_data=productId+"_"+reqsId+"_"+txt_demand_qnty;
					}
					else
					{
						save_data+=","+productId+"_"+reqsId+"_"+txt_demand_qnty;
					}
					
					tot_demand_qnty=tot_demand_qnty*1+txt_demand_qnty*1;
				}
			});
			
			if(tot_demand_qnty*1<=0) { $('#reqsn_no').val(''); }
			$('#save_data').val( save_data );
			$('#tot_demand_qnty').val(tot_demand_qnty);

			parent.emailwindow.hide();
		}
		
    </script>

</head>

<body>
<div align="center">
	<?php
	if($type!=1)
	{
	?>
	<form name="yarnDemandQnty_1" id="yarnDemandQnty_1">
        <fieldset style="width:760px; margin-top:10px">
        	<input type="hidden" name="reqsn_no" id="reqsn_no" class="text_boxes" value="<?php echo $txt_requisition_no; ?>">
            <input type="hidden" name="save_data" id="save_data" class="text_boxes" value="">
            <input type="hidden" name="tot_demand_qnty" id="tot_demand_qnty" class="text_boxes" value="">
        	<table class="rpt_table" width="600" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
                <thead>
                    <th>Buyer Name</th>
                    <th>Source</th>
                    <th>Machine Dia</th>
                    <th><input type="reset" name="reset" id="reset" value="Reset" style="width:100px" class="formbutton" /></th>
                </thead>
                <tbody>
                    <tr class="general">
                        <td>
                            <?php
                                echo create_drop_down( "cbo_buyer_name", 170, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$companyID' $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name","id,buyer_name", 1, "-- All Buyer --", $selected, "" ); 
                            ?> 
                        </td>
                        <td>
							<?php
                                $search_by_arr=array(1=>"Inside",3=>"Outside");
                                echo create_drop_down( "cbo_type", 130, $search_by_arr,"",0, "", "",'',0 );
                            ?>
                        </td>
                        <td>
                            <input type="text" style="width:130px;" class="text_boxes"  name="txt_search_common" id="txt_search_common" />
                        </td>
                        <td>
                             <input type="button" name="button2" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('txt_search_common').value+'**'+document.getElementById('cbo_buyer_name').value+'**'+<?php echo $companyID; ?>+'**'+document.getElementById('cbo_type').value, 'create_reqsn_search_list_view', 'search_div', 'yarn_demand_entry_controller', 'setFilterGrid(\'tbl_list_search\',-1);')" style="width:100px;" />
                        </td>
                    </tr>
                </tbody>
            </table> 
            <div id="search_div" style="margin-top:10px">
			<?php
            if($save_data!="")
            {
				$tot_reqs_qnty=0; $tot_demand_qnty=0; $i=1; $reqsn_array=array();
				$explSaveData = explode(",",$save_data); 
				for($z=0;$z<count($explSaveData);$z++)
				{
					$data_all=explode("_",$explSaveData[$z]);
					$prod_id=$data_all[0];
					$reqsn_id=$data_all[1];
					$demand_qnty=$data_all[2];
					
					$reqsn_array[$reqsn_id]=$demand_qnty;
				}
            ?>
            	<table width="100%" border="1" rules="all" class="rpt_table">
                    <thead>
                        <th width="30">Sl</th>
                        <th width="110">Supplier</th>
                        <th width="50">Count</th>
                        <th width="130">Composition</th>
                        <th width="70">Type</th>
                        <th width="80">Color</th>
                        <th width="70">Lot No</th>
                        <th width="105">Bl. Reqsn. Qnty</th>
                        <th>Demand Qnty</th>
                    </thead>
                    </thead>
                </table>
                <div style="width:100%; overflow-y:scroll; max-height:300px;" id="scroll_body" align="left">
                    <table class="rpt_table" rules="all" border="1" width="753" id="tbl_list_search">
                        <tbody>
                        <?php
                            $sql="select id, yarn_qnty, prod_id from ppl_yarn_requisition_entry where requisition_no=$txt_requisition_no and status_active=1 and is_deleted=0";
                            $result = sql_select($sql);
                            
                            foreach($result as $row)
                            {
                                if ($i%2==0)  
                                    $bgcolor="#E9F3FF";
                                else
                                    $bgcolor="#FFFFFF";
                                
								$demand_qnty=$reqsn_array[$row[csf('id')]];
								
								$prod_tot_demand_qnty=return_field_value("sum(yarn_demand_qnty)","ppl_yarn_demand_reqsn_dtls", "requisition_id='".$row[csf('id')]."' and status_active=1 and is_deleted=0");
								$bl_reqsn_qnty=$row[csf('yarn_qnty')]-$prod_tot_demand_qnty;
								
								if($demand_qnty>0) $bgcolor="yellow"; else $bgcolor=$bgcolor;
                                ?>
                                 <tr bgcolor="<?php echo $bgcolor; ?>" id="search<?php echo $i;?>">  
                                    <td width="30" align="center"><?php echo $i; ?></td>	
                                    <td width="110"><p><?php echo $product_details_array[$row[csf('prod_id')]]['suppl']; ?></p></td>               
                                    <td width="50"><p><?php echo $product_details_array[$row[csf('prod_id')]]['count']; ?></p></td>
                                    <td width="130"><p><?php echo $product_details_array[$row[csf('prod_id')]]['comp']; ?></p></td>  
                                    <td width="70"><p><?php echo $product_details_array[$row[csf('prod_id')]]['type']; ?></p></td>
                                    <td width="80"><p><?php echo $product_details_array[$row[csf('prod_id')]]['color']; ?></p></td>
                                    <td width="70"><p><?php echo $product_details_array[$row[csf('prod_id')]]['lot']; ?></p></td>                  
                                    <td width="105" align="right"><?php echo number_format($bl_reqsn_qnty,2); ?></td>
                                    <td align="center"><input type="text" name="txt_demand_qnty[]" id="txt_demand_qnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:80px" onKeyUp="color_row(<?php echo $i; ?>)" value="<?php if($demand_qnty>0) echo $demand_qnty; ?>"></td>
                                    <input type="hidden" name="txt_reqsn_qnty[]" id="txt_reqsn_qnty_<?php echo $i; ?>" value="<?php echo $row[csf('yarn_qnty')]; ?>" class="text_boxes">
                                    <input type="hidden" name="productId[]" id="productId_<?php echo $i; ?>" value="<?php echo $row[csf('prod_id')]; ?>" class="text_boxes">
                                    <input type="hidden" name="reqsId[]" id="reqsId_<?php echo $i; ?>" value="<?php echo $row[csf('id')]; ?>" class="text_boxes">
                                </tr>
                            <?php
                                //$tot_reqs_qnty+=$row[csf('yarn_qnty')];
								$tot_reqs_qnty+=$bl_reqsn_qnty;
								$tot_demand_qnty+=$demand_qnty;
                                $i++;
                            }
                        ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="7" align="right"><b>Total</b></th>
                                <th><?php echo number_format($tot_reqs_qnty,2); ?></th>
                                <th align="center">
                                    <input type="text" name="txt_total_demand_qnty" id="txt_total_demand_qnty" class="text_boxes_numeric" style="width:80px" readonly disabled="disabled" value="<?php echo $tot_demand_qnty; ?>"/>
                                </th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <table width="620">
                     <tr>
                        <td align="center" >
                            <input type="button" name="close" class="formbutton" value="Close" id="main_close" onClick="fnc_close();" style="width:100px" />
                        </td>
                    </tr>
                </table>
            <?php
			}
			?>
            </div>
	<?php
	}
	else
	{
	?>
     	<div align="center" style="width:740px;">
            <input type="hidden" name="hidden_prod_id" id="hidden_prod_id" class="text_boxes" value="">  
            <input type="hidden" name="hidden_data" id="hidden_data" class="text_boxes" value="">  
            <table width="100%" border="1" rules="all" class="rpt_table">
                <thead>
                    <th width="30">Sl</th>
                    <th width="100">Supplier</th>
                    <th width="50">Count</th>
                    <th width="130">Composition</th>
                    <th width="70">Type</th>
                    <th width="80">Color</th>
                    <th width="70">Lot No</th>
                    <th width="80">Bl. Reqsn. Qnty</th>
                    <th>Demand Qnty</th>
                </thead>
                </thead>
            </table>
            <div style="width:100%; overflow-y:scroll; max-height:300px;" id="scroll_body" align="left">
                <table class="rpt_table" rules="all" border="1" width="723" id="tbl_list_search">
                	<tbody>
					<?php
                        $tot_reqs_qnty=0; $i=1;
                        $sql="select id, yarn_qnty, prod_id from ppl_yarn_requisition_entry where requisition_no=$reqsn_no and status_active=1 and is_deleted=0";
                        $result = sql_select($sql);
                        
                        foreach($result as $row)
                        {
                            if ($i%2==0)  
                                $bgcolor="#E9F3FF";
                            else
                                $bgcolor="#FFFFFF";
								
							$prod_tot_demand_qnty=return_field_value("sum(yarn_demand_qnty)","ppl_yarn_demand_reqsn_dtls", "requisition_id='".$row[csf('id')]."' and status_active=1 and is_deleted=0");
							$bl_reqsn_qnty=$row[csf('yarn_qnty')]-$prod_tot_demand_qnty;
                            
                            ?>
                             <tr bgcolor="<?php echo $bgcolor; ?>" id="search<?php echo $i;?>">  
                                <td width="30" align="center"><?php echo $i; ?></td>	
                                <td width="100"><p><?php echo $product_details_array[$row[csf('prod_id')]]['suppl']; ?></p></td>               
                                <td width="50"><p><?php echo $product_details_array[$row[csf('prod_id')]]['count']; ?></p></td>
                                <td width="130"><p><?php echo $product_details_array[$row[csf('prod_id')]]['comp']; ?></p></td>  
                                <td width="70"><p><?php echo $product_details_array[$row[csf('prod_id')]]['type']; ?></p></td>
                                <td width="80"><p><?php echo $product_details_array[$row[csf('prod_id')]]['color']; ?></p></td>
                                <td width="70"><p><?php echo $product_details_array[$row[csf('prod_id')]]['lot']; ?></p></td>                  
                                <td width="80" align="right"><?php echo number_format($bl_reqsn_qnty,2); ?></td>
                                <td align="center"><input type="text" name="txt_demand_qnty[]" id="txt_demand_qnty_<?php echo $i; ?>" class="text_boxes_numeric" style="width:80px" onKeyUp="color_row(<?php echo $i; ?>)" value="<?php if($demand_qnty>0) echo $demand_qnty; ?>"></td>
                                <input type="hidden" name="txt_reqsn_qnty[]" id="txt_reqsn_qnty_<?php echo $i; ?>" value="<?php echo $row[csf('yarn_qnty')]; ?>" class="text_boxes">
                                <input type="hidden" name="productId[]" id="productId_<?php echo $i; ?>" value="<?php echo $row[csf('prod_id')]; ?>" class="text_boxes">
                                <input type="hidden" name="reqsId[]" id="reqsId_<?php echo $i; ?>" value="<?php echo $row[csf('id')]; ?>" class="text_boxes">
                            </tr>
                        <?php
                            //$tot_reqs_qnty+=$row[csf('yarn_qnty')];
							$tot_reqs_qnty+=$bl_reqsn_qnty;
                            $i++;
                        }
                    ?>
                	</tbody>
                    <tfoot>
                        <tr>
                            <th colspan="7" align="right"><b>Total</b></th>
                            <th><?php echo number_format($tot_reqs_qnty,2); ?></th>
                            <th align="center">
                            	<input type="text" name="txt_total_demand_qnty" id="txt_total_demand_qnty" class="text_boxes_numeric" style="width:80px" readonly disabled="disabled" value="<?php echo $tot_demand_qnty; ?>"/>
                            </th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <table width="620">
				 <tr>
					<td align="center" >
						<input type="button" name="close" class="formbutton" value="Close" id="main_close" onClick="fnc_close();" style="width:100px" />
					</td>
				</tr>
			</table>
        </div> 
    <?php
	}
	if($type!=1)
	{
	?>
		</fieldset>
	</form>
    <?php
	}
	?>
</div>
</body>           
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
	exit(); 
}

if($action=="create_reqsn_search_list_view")
{ 
	$data = explode("**",$data);
	$search_string="%".trim($data[0])."%";
	$company_id=$data[2];
	$type=$data[3];
	
	//if($data[1]==0) $buyer_name="%%"; else $buyer_name=$data[1];
	if($data[1]==0)
	{
		if ($_SESSION['logic_erp']["data_level_secured"]==1)
		{
			if($_SESSION['logic_erp']["buyer_id"]!="") $buyer_id_cond=" and buyer_id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_id_cond="";
		}
		else
		{
			$buyer_id_cond="";
		}
	}
	else
	{
		$buyer_id_cond=" and buyer_id=$data[1]";//.str_replace("'","",$cbo_buyer_name)
	}
	
	$po_array=array();
	$costing_sql=sql_select("select a.job_no, a.style_ref_no, b.id, b.po_number from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.company_name=$company_id");
	foreach($costing_sql as $row)
	{
		$po_array[$row[csf('id')]]['no']=$row[csf('po_number')];
		$po_array[$row[csf('id')]]['style_ref']=$row[csf('style_ref_no')]; 
	}
	if($db_type==0)
	{
		$plan_details_array=return_library_array( "select dtls_id, group_concat(distinct(po_id)) as po_id from ppl_planning_entry_plan_dtls where company_id=$company_id group by dtls_id", "dtls_id", "po_id"  );
	}
	else
	{
		$plan_details_array=return_library_array( "select dtls_id, ISTAGG(po_id, ',') WITHIN GROUP (ORDER BY po_id) as po_id from ppl_planning_entry_plan_dtls where company_id=$company_id group by dtls_id", "dtls_id", "po_id"  );	
	}
	
	?>
	<fieldset style="width:720px; margin-top:10px">
		<table cellspacing="0" cellpadding="0" border="1" rules="all" width="715" class="rpt_table" >
			<thead>
				<th width="40">SL</th>
                <th width="70">Reqsn. No</th>
				<th width="110">Buyer</th>
				<th width="120">Order No</th>
				<th width="100"><?php echo $company_arr[str_replace("'","",$company_id)]; ?></th>
				<th width="120">Style</th>
                <th>Reqsn. Qnty</th>
			</thead>
		</table>
		<div style="width:715px; overflow-y:scroll; max-height:270px;" id="buyer_list_view" align="center">
			<table cellspacing="0" cellpadding="0" border="1" rules="all" width="697" class="rpt_table" id="tbl_list_search">
				<tbody>
					<?php 
						$i=1;
						$sql="select a.company_id, a.buyer_id, b.id, c.requisition_no, sum(c.yarn_qnty) as reqs_qnty from ppl_planning_info_entry_mst a, ppl_planning_info_entry_dtls b, ppl_yarn_requisition_entry c where a.company_id=$company_id and a.id=b.mst_id and b.knitting_source=$type and b.machine_dia like '$search_string' and b.id=c.knit_id and a.is_deleted=0 and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and c.is_deleted=0 and c.status_active=1 $buyer_id_cond group by a.company_id, a.buyer_id, b.id, c.requisition_no order by c.requisition_no"; //and a.buyer_id like '$buyer_name'							
						$nameArray=sql_select( $sql );
						foreach ($nameArray as $row)
						{
							if ($i%2==0)  
								$bgcolor="#E9F3FF";
							else
								$bgcolor="#FFFFFF"; 
							
							$po_id=explode(",",$plan_details_array[$row[csf('id')]]);
							$po_no=''; $style_ref='';
							
							foreach($po_id as $val)
							{
								if($po_no=='') $po_no=$po_array[$val]['no']; else $po_no.=",".$po_array[$val]['no'];
								if($style_ref=='') $style_ref=$po_array[$val]['style_ref'];
							}
								
							?>
							<tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" id="search<?php echo $i;?>" onClick="js_set_value(<?php echo $row[csf('requisition_no')]; ?>)"> 
                                <td width="40" align="center"><?php echo $i; ?></td>
								<td width="70" align="center"><?php echo $row[csf('requisition_no')]; ?></td>
								<td width="110"><p><?php echo $buyer_arr[$row[csf('buyer_id')]]; ?></p></td>
								<td width="120"><p><?php echo $po_no; ?></p></td>
								<td width="100"><p><?php echo $plan_details_array[$row[csf('id')]]; ?></p></td>
								<td width="120"><p><?php echo  $style_ref; ?></p></td>
								<td align="right"><?php echo number_format($row[csf('reqs_qnty')],2); ?></td>
							</tr>
							<?php
							$i++;
						}
						?>
				</tbody>
			</table>
		</div>
	</fieldset>      
	<?php
	exit();
}


if ($action=="save_update_delete")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	if ($operation==0)  // Insert Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
		$req_qnty=return_field_value("sum(yarn_qnty)","ppl_yarn_requisition_entry", "requisition_no=$txt_requisition_no and status_active=1 and is_deleted=0");
		$tot_demand_qnty=return_field_value("sum(demand_qnty)","ppl_yarn_demand_entry_dtls", "requisition_no=$txt_requisition_no and status_active=1 and is_deleted=0")+str_replace("'","",$txt_demand_qnty);
		
		if($tot_demand_qnty>$req_qnty)
		{
			echo "17**0**0**0**0";
			exit();
		}
		
		$demand_num=''; $demand_update_id=''; $flag=1;
		
		if(str_replace("'","",$update_id)=="")
		{
			if($db_type==0) $year_cond="YEAR(insert_date)"; 
			else if($db_type==2) $year_cond="to_char(insert_date,'YYYY')";
			else $year_cond="";//defined Later
			
			$new_demand_system_id=explode("*",return_mrr_number( str_replace("'","",$cbo_company_id), '', 'YDE', date("Y",time()), 5, "select demand_prefix, demand_prefix_number from ppl_yarn_demand_entry_mst where company_id=$cbo_company_id and $year_cond=".date('Y',time())." order by id desc ", "demand_prefix", "demand_prefix_number" ));
		 
			$id=return_next_id( "id", "ppl_yarn_demand_entry_mst", 1 ) ;
					 
			$field_array="id, demand_prefix, demand_prefix_number, demand_system_no, company_id, location_id, demand_date, inserted_by, insert_date";
			
			$data_array="(".$id.",'".$new_demand_system_id[1]."',".$new_demand_system_id[2].",'".$new_demand_system_id[0]."',".$cbo_company_id.",".$cbo_location.",".$txt_demand_date.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
			
			//echo "insert into ppl_yarn_demand_entry_mst (".$field_array.") values ".$data_array;die;
			/*$rID=sql_insert("ppl_yarn_demand_entry_mst",$field_array,$data_array,0);
			if($rID) $flag=1; else $flag=0;*/
			
			$demand_num=$new_demand_system_id[0];
			$demand_update_id=$id;
		}
		else
		{
			$field_array_update="location_id*demand_date*updated_by*update_date";
			
			$data_array_update=$cbo_location."*".$txt_demand_date."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
			
			/*$rID=sql_update("ppl_yarn_demand_entry_mst",$field_array_update,$data_array_update,"id",$update_id,0);
			if($rID) $flag=1; else $flag=0;*/ 
			
			$demand_num=str_replace("'","",$txt_demand_no);
			$demand_update_id=str_replace("'","",$update_id);
		}
		
		$id_dtls=return_next_id( "id","ppl_yarn_demand_entry_dtls", 1 ) ;
		
		$field_array_dtls="id, mst_id, requisition_no, demand_qnty, save_string, inserted_by, insert_date"; 
	    $data_array_dtls="(".$id_dtls.",".$demand_update_id.",".$txt_requisition_no.",".$txt_demand_qnty.",".$save_data.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
		//echo "insert into ppl_yarn_demand_entry_dtls (".$field_array_dtls.") Values ".$data_array_dtls."";die;
		/*$rID2=sql_insert("ppl_yarn_demand_entry_dtls",$field_array_dtls,$data_array_dtls,0);
		if($flag==1)
		{
			if($rID2) $flag=1; else $flag=0; 
		}*/
		
		$id_dtls_item=return_next_id( "id","ppl_yarn_demand_reqsn_dtls", 1 ) ;
		$field_array_dtls_item="id, mst_id, dtls_id, requisition_no, requisition_id, prod_id, yarn_demand_qnty, inserted_by, insert_date"; 
		$save_string=explode(",",str_replace("'","",$save_data));
		for($i=0;$i<count($save_string);$i++)
		{
			$data=explode("_",$save_string[$i]);  
			$productId=$data[0];
			$reqsnId=$data[1];
			$demandQnty=$data[2];
			
			if ($i!=0) $data_array_dtls_item.=",";
			$data_array_dtls_item.="(".$id_dtls_item.",".$demand_update_id.",".$id_dtls.",".$txt_requisition_no.",".$reqsnId.",".$productId.",".$demandQnty.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')"; 
			
			$id_dtls_item=$id_dtls_item+1;
		}
		
		if(str_replace("'","",$update_id)=="")
		{
			$rID=sql_insert("ppl_yarn_demand_entry_mst",$field_array,$data_array,0);
			if($rID) $flag=1; else $flag=0;
		}
		else
		{
			$rID=sql_update("ppl_yarn_demand_entry_mst",$field_array_update,$data_array_update,"id",$update_id,0);
			if($rID) $flag=1; else $flag=0;
		}
		$rID2=sql_insert("ppl_yarn_demand_entry_dtls",$field_array_dtls,$data_array_dtls,0);
		if($flag==1)
		{
			if($rID2) $flag=1; else $flag=0; 
		}
		//echo "insert into ppl_yarn_demand_reqsn_dtls (".$field_array_dtls_item.") Values ".$data_array_dtls_item."";die;
		$rID3=sql_insert("ppl_yarn_demand_reqsn_dtls",$field_array_dtls_item,$data_array_dtls_item,1);
		if($flag==1)
		{
			if($rID3) $flag=1; else $flag=0; 
		}
		
		if($db_type==0)
		{
			if($flag==1)
			{
				mysql_query("COMMIT");  
				echo "0**".$demand_num."**".$demand_update_id."**0**1";
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "5**".$demand_numd."**0**0**0";
			}
		}
		else if($db_type==2 || $db_type==1 )
		{
			if($flag==1)
			{
				oci_commit($con);   
				echo "0**".$demand_num."**".$demand_update_id."**0**1";
			}
			else
			{
				oci_rollback($con);
				echo "5**".$demand_numd."**0**0**0";
			}
		}
		disconnect($con);
		die;
	}
	else if ($operation==1)   // Update Here
	{ 
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
		$req_qnty=return_field_value("sum(yarn_qnty)","ppl_yarn_requisition_entry", "requisition_no=$txt_requisition_no and status_active=1 and is_deleted=0");
		$tot_demand_qnty=return_field_value("sum(demand_qnty)","ppl_yarn_demand_entry_dtls", "requisition_no=$txt_requisition_no and id<>$update_dtls_id and status_active=1 and is_deleted=0")+str_replace("'","",$txt_demand_qnty);
		
		if($tot_demand_qnty>$req_qnty)
		{
			echo "17**0**0**1**1";
			exit();
		}
		
		$field_array_update="location_id*demand_date*updated_by*update_date";
			
		$data_array_update=$cbo_location."*".$txt_demand_date."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
		
		/*$rID=sql_update("ppl_yarn_demand_entry_mst",$field_array_update,$data_array_update,"id",$update_id,0);
		if($rID) $flag=1; else $flag=0; */
		
		$field_array_update_dtls="requisition_no*demand_qnty*save_string*updated_by*update_date";
			
		$data_array_update_dtls=$txt_requisition_no."*".$txt_demand_qnty."*".$save_data."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
		
		/*$rID2=sql_update("ppl_yarn_demand_entry_dtls",$field_array_update_dtls,$data_array_update_dtls,"id",$update_dtls_id,0);
		if($flag==1)
		{
			if($rID2) $flag=1; else $flag=0; 
		}
		
		$delete_item_dtls=execute_query( "delete from ppl_yarn_demand_reqsn_dtls where dtls_id=$update_dtls_id",0);
		if($flag==1) 
		{
			if($delete_item_dtls) $flag=1; else $flag=0; 
		} */
		
		$id_dtls_item=return_next_id( "id","ppl_yarn_demand_reqsn_dtls", 1 ) ;
		$field_array_dtls_item="id, mst_id, dtls_id, requisition_no, requisition_id, prod_id, yarn_demand_qnty, inserted_by, insert_date"; 
		$save_string=explode(",",str_replace("'","",$save_data));
		for($i=0;$i<count($save_string);$i++)
		{
			$data=explode("_",$save_string[$i]);  
			$productId=$data[0];
			$reqsnId=$data[1];
			$demandQnty=$data[2];
			
			if ($i!=0) $data_array_dtls_item.=",";
			
			$data_array_dtls_item.="(".$id_dtls_item.",".$update_id.",".$update_dtls_id.",".$txt_requisition_no.",".$reqsnId.",".$productId.",".$demandQnty.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')"; 
			
			$id_dtls_item=$id_dtls_item+1;
		}
		
		$rID=sql_update("ppl_yarn_demand_entry_mst",$field_array_update,$data_array_update,"id",$update_id,0);
		if($rID) $flag=1; else $flag=0; 
		
		$rID2=sql_update("ppl_yarn_demand_entry_dtls",$field_array_update_dtls,$data_array_update_dtls,"id",$update_dtls_id,0);
		if($flag==1)
		{
			if($rID2) $flag=1; else $flag=0; 
		}
		
		$delete_item_dtls=execute_query( "delete from ppl_yarn_demand_reqsn_dtls where dtls_id=$update_dtls_id",0);
		if($flag==1) 
		{
			if($delete_item_dtls) $flag=1; else $flag=0; 
		} 
		
		//echo "insert into ppl_yarn_demand_reqsn_dtls (".$field_array_dtls_item.") Values ".$data_array_dtls_item."";die;
		$rID3=sql_insert("ppl_yarn_demand_reqsn_dtls",$field_array_dtls_item,$data_array_dtls_item,1);
		if($flag==1)
		{
			if($rID3) $flag=1; else $flag=0; 
		}
		
		$demand_num=str_replace("'","",$txt_demand_no);
		$demand_update_id=str_replace("'","",$update_id);
		
		if($db_type==0)
		{
			if($flag==1)
			{
				mysql_query("COMMIT");  
				echo "1**".$demand_num."**".$demand_update_id."**0**1";
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "6**0**1**1**1";
			}
		}
		else if($db_type==2 || $db_type==1 )
		{
			if($flag==1)
			{
				oci_commit($con);   
				echo "1**".$demand_num."**".$demand_update_id."**0**1";
			}
			else
			{
				oci_rollback($con); 
				echo "6**0**1**1**1";
			}
		}
		disconnect($con);
		die;
	}
	else if ($operation==2)   // Update Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
		$field_array_status="updated_by*update_date*status_active*is_deleted";
		$data_array_status=$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'*0*1";

		$rID=sql_update("ppl_yarn_demand_entry_mst",$field_array_status,$data_array_status,"id",$update_id,0);
		$rID2=sql_update("ppl_yarn_demand_entry_dtls",$field_array_status,$data_array_status,"id",$update_dtls_id,0);
		$rID3=sql_update("ppl_yarn_demand_reqsn_dtls",$field_array_status,$data_array_status,"id",$update_dtls_id,1);

		if($db_type==0)
		{
			if($rID && $rID2 && $rID3)
			{
				mysql_query("COMMIT");  
				echo "2**0**0**0**0";
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "7**0**0**1**1";
			}
		}
		else if($db_type==2 || $db_type==1 )
		{
			if($rID && $rID2 && $rID3)
			{
				oci_commit($con); 
				echo "2**0**0**0**0";
			}
			else
			{
				oci_rollback($con);
				echo "7**0**0**1**1";
			}
		}
		
		disconnect($con);
		die;
	}
}

if ($action=="systemId_popup")
{
	echo load_html_head_contents("System ID Info", "../../", 1, 1,'','','');
	extract($_REQUEST);
?>
	<script>
		function js_set_value(id)
		{
			$('#hidden_sys_id').val(id);
			parent.emailwindow.hide();
		}
    </script>
</head>

<body>
<div align="center" style="width:640px;">
    <form name="searchsystemidfrm"  id="searchsystemidfrm">
        <fieldset style="width:630px;">
        <legend>Enter search words</legend>
            <table cellpadding="0" cellspacing="0" width="550" class="rpt_table">
                <thead>
                    <th>Search By</th>
                    <th width="250" id="search_by_td_up">Enter System Id</th>
                    <th>
                        <input type="reset" name="reset" id="reset" value="Reset" style="width:100px;" class="formbutton" />
                        <input type="hidden" name="txt_company_id" id="txt_company_id" class="text_boxes" value="<?php echo $cbo_company_id; ?>">
                        <input type="hidden" name="hidden_sys_id" id="hidden_sys_id" class="text_boxes" value="">
                    </th>
                </thead>
                <tr class="general">
                    <td>
						<?php
							$search_by_arr=array(1=>"System ID",2=>"Requisition No.");
							$dd="change_search_event(this.value, '0*0', '0*0', '../../') ";
							echo create_drop_down( "cbo_search_by", 150, $search_by_arr,"",0, "--Select--", "",$dd,0 );
                        ?>
                    </td>
                    <td id="search_by_td">
                        <input type="text" style="width:130px;" class="text_boxes"  name="txt_search_common" id="txt_search_common" />
                    </td>
                    <td>
                        <input type="button" name="button2" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('txt_search_common').value+'_'+document.getElementById('cbo_search_by').value+'_'+document.getElementById('txt_company_id').value, 'create_demand_search_list_view', 'search_div', 'yarn_demand_entry_controller', 'setFilterGrid(\'tbl_list_search\',-1);')" style="width:100px;" />
                    </td>
                </tr>
            </table>
           <div style="width:100%; margin-top:10px; margin-left:3px;" id="search_div" align="left"></div>
    	</fieldset>
    </form>
</div>
</body>
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}

if($action=="create_demand_search_list_view")
{
	$data = explode("_",$data);
	$search_string=trim($data[0]);
	$search_by=$data[1];
	$company_id =$data[2];

	if(trim($data[0])!="")
	{
		if($search_by==1)
			$search_field_cond="and a.demand_system_no like '%".$search_string."%'";
		else
			$search_field_cond="and b.requisition_no like '$search_string'";
	}
	else
	{
		$search_field_cond="";
	}
	
	$location_arr=return_library_array( "select id, location_name from lib_location",'id','location_name');
	
	if($db_type==0) $year_field="YEAR(a.insert_date) as year,"; 
	else if($db_type==2) $year_field="to_char(a.insert_date,'YYYY') as year,";
	else $year_field="";//defined Later
	
	if($db_type==0)
	{
		$sql="select a.id, a.demand_system_no, $year_field a.demand_prefix_number, a.company_id, a.location_id, a.demand_date, group_concat(distinct(b.requisition_no)) as reqsn_no from ppl_yarn_demand_entry_mst a, ppl_yarn_demand_entry_dtls b where a.id=b.mst_id and a.company_id='$company_id' and a.status_active = '1' and a.is_deleted = '0' and b.status_active = '1' and b.is_deleted = '0' $search_field_cond group by a.id, a.demand_system_no, a.demand_prefix_number, a.company_id,a.location_id,a.demand_date,a.insert_date";
	}
	else
	{
		$sql="select a.id, a.demand_system_no, $year_field a.demand_prefix_number, a.company_id, a.location_id, a.demand_date, LISTAGG(b.requisition_no, ',') WITHIN GROUP (ORDER BY b.requisition_no) as reqsn_no from ppl_yarn_demand_entry_mst a, ppl_yarn_demand_entry_dtls b where a.id=b.mst_id and a.company_id='$company_id' and a.status_active = '1' and a.is_deleted = '0' and b.status_active = '1' and b.is_deleted = '0' $search_field_cond group by a.id, a.demand_system_no, a.demand_prefix_number, a.company_id,a.location_id,a.demand_date,a.insert_date";	
	}
	$arr=array(2=>$company_arr,3=>$location_arr);
	echo  create_list_view("tbl_list_search", "Demand No,Year,Company,Location,Demand Date,Requisition No", "70,70,80,120,80","610","200",0, $sql, "js_set_value", "id", "", 1, "0,0,company_id,location_id,0,0", $arr, "demand_prefix_number,year,company_id,location_id,demand_date,reqsn_no", "",'','0,0,0,0,3,0');
	
	exit();
}

if($action=='populate_data_from_demand_update')
{
	$data_array=sql_select("select id, demand_system_no, company_id, location_id, demand_date from ppl_yarn_demand_entry_mst where id='$data'");
	foreach ($data_array as $row)
	{ 
		echo "document.getElementById('txt_demand_no').value 				= '".$row[csf("demand_system_no")]."';\n";
		echo "document.getElementById('cbo_company_id').value 				= '".$row[csf("company_id")]."';\n";
		echo "$('#cbo_company_id').attr('disabled','true')".";\n";
		//echo "load_drop_down('requires/yarn_demand_entry_controller', ".$row[csf("company_id")].", 'load_drop_down_location', 'location_td' );\n";
		echo "document.getElementById('cbo_location').value 				= '".$row[csf("location_id")]."';\n";
		echo "document.getElementById('txt_demand_date').value 				= '".change_date_format($row[csf("demand_date")])."';\n";
		echo "document.getElementById('update_id').value 					= '".$row[csf("id")]."';\n";
		
		echo "set_button_status(0, '".$_SESSION['page_permission']."', 'fnc_yarn_demand_entry',1,1);\n";  
		exit();
	}
}

if($action=="show_demand_listview")
{
	$data=explode("**",$data);
	
	$po_array=array();
	$costing_sql=sql_select("select a.job_no, a.buyer_name, b.id, b.po_number from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.company_name=$data[1]");
	foreach($costing_sql as $row)
	{
		$po_array[$row[csf('id')]]['no']=$row[csf('po_number')];
		$po_array[$row[csf('id')]]['buyer']=$buyer_arr[$row[csf('buyer_name')]]; 
	}
?>
    <table cellspacing="0" cellpadding="0" border="1" rules="all" width="650" class="rpt_table" >
        <thead>
            <th width="40">SL</th>
            <th width="100">Requisition No</th>
            <th width="110">Buyer</th>
            <th width="130">PO No</th>
            <th width="110"><?php echo $company_arr[$data[1]]; ?></th>
            <th>Demand Qnty</th>
        </thead>
    </table>
    <div style="width:650px; overflow-y:scroll; max-height:240px;" id="buyer_list_view" align="center">
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="632" class="rpt_table" id="tbl_list_search" >
        <?php
            $i=1; $tot_demand_qnty=0;
			$sql="select id, requisition_no, demand_qnty from ppl_yarn_demand_entry_dtls where mst_id='$data[0]' and status_active=1 and is_deleted=0";
            $nameArray=sql_select( $sql );
            foreach ($nameArray as $selectResult)
            {
                if ($i%2==0)  
                    $bgcolor="#E9F3FF";
                else
                    $bgcolor="#FFFFFF";
				
				if($db_type==0)
				{
					$po_id=return_field_value("group_concat(distinct(c.po_id)) as po_id","ppl_yarn_requisition_entry a, ppl_planning_info_entry_dtls b, ppl_planning_entry_plan_dtls c","a.requisition_no=".$selectResult[csf('requisition_no')]." and a.knit_id=b.id and b.id=c.dtls_id","po_id");
				}
				else
				{
					$po_id=return_field_value("LISTAGG(c.po_id, ',') WITHIN GROUP (ORDER BY c.po_id) as po_id","ppl_yarn_requisition_entry a, ppl_planning_info_entry_dtls b, ppl_planning_entry_plan_dtls c","a.requisition_no=".$selectResult[csf('requisition_no')]." and a.knit_id=b.id and b.id=c.dtls_id","po_id");	
				}
				
				$po_id=array_unique(explode(",",$po_id));
				$all_po_id=$po_id;
				$po_no=''; $buyer='';
				
				foreach($all_po_id as $val)
				{
					if($po_no=='') $po_no=$po_array[$val]['no']; else $po_no.=",".$po_array[$val]['no'];
					if($buyer=='') $buyer=$po_array[$val]['buyer'];
				}
				
                ?>
                <tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" id="search<?php echo $i;?>" onClick="get_php_form_data(<?php echo $selectResult[csf('id')]; ?>, 'populate_data_from_demand_dtls', 'requires/yarn_demand_entry_controller' );"> 
                    <td width="40" align="center"><?php echo $i; ?></td>	
                    <td width="100"><p><?php echo $selectResult[csf('requisition_no')]; ?></p></td>
                    <td width="110"><p><?php echo $buyer; ?></p></td>
                    <td width="130"><p><?php echo $po_no; ?></p></td>
                    <td width="110"><p><?php echo implode(",",$po_id); ?></p></td>	
                    <td align="right"><?php echo number_format($selectResult[csf('demand_qnty')],2); ?></td>	
                </tr>
                <?php
				
				$tot_demand_qnty+=$selectResult[csf('demand_qnty')];
				
                $i++;
            }
        ?>
        	<tfoot>
            	<th colspan="5">Total</th>
                <th><?php echo number_format($tot_demand_qnty,2); ?></th>
            </tfoot>
        </table>
    </div>
   
	<?php
	exit();
}

if($action=='populate_data_from_demand_dtls')
{
	$data_array=sql_select("select id, requisition_no, demand_qnty, save_string from ppl_yarn_demand_entry_dtls where id='$data'");
	foreach ($data_array as $row)
	{ 
		echo "document.getElementById('txt_requisition_no').value 			= '".$row[csf("requisition_no")]."';\n";
		echo "document.getElementById('txt_demand_qnty').value 				= '".$row[csf("demand_qnty")]."';\n";
		echo "document.getElementById('save_data').value 					= '".$row[csf("save_string")]."';\n";
		echo "document.getElementById('update_dtls_id').value 				= '".$row[csf("id")]."';\n";
		
		echo "set_button_status(1, '".$_SESSION['page_permission']."', 'fnc_yarn_demand_entry',1);\n";  
		exit();
	}
}

if($action=="print") 
{
	echo load_html_head_contents("Demand Print", "../", 1, 1,'','','');
	extract($_REQUEST);
	$data=explode("**",$data);
	
	$po_array=array();
	$costing_sql=sql_select("select a.job_no, a.buyer_name, b.id, b.po_number from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.company_name=$data[1]");
	foreach($costing_sql as $row)
	{
		$po_array[$row[csf('id')]]['no']=$row[csf('po_number')];
		$po_array[$row[csf('id')]]['buyer']=$buyer_arr[$row[csf('buyer_name')]]; 
	}
	$company_library=return_library_array( "select id,company_name from lib_company", "id", "company_name");
	$location_arr=return_library_array( "select id, location_name from lib_location",'id','location_name');
	$country_arr=return_library_array( "select id,country_name from lib_country",'id','country_name');
	$supplier_arr=return_library_array( "select id,supplier_name from lib_supplier",'id','supplier_name');
	?> 
    <div style="margin-left:20px">    
        <table width="950" style="margin-top:10px">
            <tr>
                <td align="center" style="font-size:16px;">
                  <?php      
                        echo $company_library[$data[1]];
                  ?>
                </td>
            </tr>
            <tr>
                <td align="center" style="font-size:14px">  
                <?php
                $nameArray=sql_select( "select plot_no,level_no,road_no,block_no,country_id,province,city,zip_code,email,website from lib_company where id=$data[1]"); 
                foreach ($nameArray as $result)
                { 
                ?>
                    Plot No: <?php echo $result['plot_no']; ?> 
                    Level No: <?php echo $result['level_no']?>
                    Road No: <?php echo $result['road_no']; ?> 
                    Block No: <?php echo $result['block_no'];?> 
                    City No: <?php echo $result['city'];?> 
                    Zip Code: <?php echo $result['zip_code']; ?> 
                    Province No: <?php echo $result['province'];?> 
                    Country: <?php echo $country_arr[$result['country_id']]; ?><br> 
                    Email Address: <?php echo $result['email'];?> 
                    Website No: <?php echo $result['website'];
                }
                ?>   
               </td> 
            </tr>
            <tr>
                <td height="10"></td>
            </tr>
            <tr>
                <td width="100%" align="center" style="font-size:14px;"><b><u>Daily Yarn Demand</u></b></td>
            </tr>
        </table>
        <?php $dataArray=sql_select("select location_id, demand_date, demand_system_no from ppl_yarn_demand_entry_mst where id=$data[0]"); ?>
        <table width="750" style="margin-top:10px">
        	<tr>
                <td><b>Demand No:&nbsp;&nbsp;&nbsp;&nbsp;</b><?php echo $dataArray[0][csf('demand_system_no')]; ?></td>
                <td><b>Demand Date:&nbsp;&nbsp;&nbsp;&nbsp;</b><?php echo change_date_format($dataArray[0][csf('demand_date')]); ?></td>
                <td><b>Location:&nbsp;&nbsp;&nbsp;&nbsp;</b><?php echo $location_arr[$dataArray[0][csf('location_id')]]; ?></td>
            </tr>
        </table>
        
        <table style="margin-top:10px;" width="950" border="1" rules="all" cellpadding="0" cellspacing="0" class="rpt_table">
            <thead>
                <th width="30">SL</th>
                <th width="100">Buyer Name</th>
                <th width="100">Order No</th>
                <th width="80">Knitting Com.<?php //echo $company_arr[$data[1]]; ?></th>
                <th width="80">Program No</th>
                <th width="50">Reqsn. No</th>
                <th width="50">Count</th>
                <th width="50">Brand</th>
                <th width="50">Lot</th>
                <th width="110">Composition</th>
                <th width="70">Type</th>
                <th width="70">Color</th>
                <th>Demand Qnty</th>
            </thead>
            <?php
				$kintting_arr=array();
				$kintting_source_data=sql_select("select a.requisition_no, a.knit_id, b.knitting_source, b.knitting_party from ppl_yarn_requisition_entry a, ppl_planning_info_entry_dtls b where a.knit_id=b.id");
				foreach($kintting_source_data as $row)
				{
					$kintting_arr[$row[csf('requisition_no')]]['source']=$row[csf('knitting_source')];
					$kintting_arr[$row[csf('requisition_no')]]['party']=$row[csf('knitting_party')];
					$kintting_arr[$row[csf('requisition_no')]]['prog_no']=$row[csf('knit_id')];
				}
				
				if($db_type==0)
				{
					$program_po_arr=return_library_array( "select dtls_id, group_concat(po_id) as po_id from ppl_planning_entry_plan_dtls where status_active=1 and is_deleted=0 group by dtls_id",'dtls_id','po_id');
				}
				else
				{
					$program_po_arr=return_library_array( "select dtls_id, LISTAGG(po_id, ',') WITHIN GROUP (ORDER BY po_id) as po_id from ppl_planning_entry_plan_dtls where status_active=1 and is_deleted=0 group by dtls_id",'dtls_id','po_id');
				}
				
				$i=1; 
				$sql="select a.requisition_no, b.prod_id, sum(b.yarn_demand_qnty) as yarn_demand_qnty from ppl_yarn_demand_entry_dtls a, ppl_yarn_demand_reqsn_dtls b where a.id=b.dtls_id and a.mst_id='$data[0]' and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by a.requisition_no, b.prod_id order by a.requisition_no";
				$nameArray=sql_select( $sql );
				foreach ($nameArray as $selectResult)
				{
					/*if($db_type==0)
					{
						$program_no=return_field_value("group_concat(distinct(b.id)) as program_no","ppl_yarn_requisition_entry a, ppl_planning_info_entry_dtls b","a.requisition_no=".$selectResult[csf('requisition_no')]." and a.knit_id=b.id","program_no");
						
						$po_id=return_field_value("group_concat(distinct(c.po_id)) as po_id","ppl_yarn_requisition_entry a, ppl_planning_info_entry_dtls b, ppl_planning_entry_plan_dtls c","a.requisition_no=".$selectResult[csf('requisition_no')]." and a.knit_id=b.id and b.id=c.dtls_id","po_id");
					}
					else
					{
						$program_no=return_field_value("LISTAGG(b.id, ',') WITHIN GROUP (ORDER BY b.id) as program_no","ppl_yarn_requisition_entry a, ppl_planning_info_entry_dtls b","a.requisition_no=".$selectResult[csf('requisition_no')]." and a.knit_id=b.id","program_no");
						$program_no=implode(",",array_unique(explode(",",$program_no)));
						
						$po_id=return_field_value("LISTAGG(c.po_id, ',') WITHIN GROUP (ORDER BY c.po_id) as po_id","ppl_yarn_requisition_entry a, ppl_planning_info_entry_dtls b, ppl_planning_entry_plan_dtls c","a.requisition_no=".$selectResult[csf('requisition_no')]." and a.knit_id=b.id and b.id=c.dtls_id","po_id");
						$po_id=implode(",",array_unique(explode(",",$po_id)));
					}
					
					$kintting_source=return_field_value("b.knitting_source","ppl_yarn_requisition_entry a, ppl_planning_info_entry_dtls b","a.requisition_no=".$selectResult[csf('requisition_no')]." and a.knit_id=b.id","knitting_source");
					$kintting_com=return_field_value("b.knitting_party","ppl_yarn_requisition_entry a, ppl_planning_info_entry_dtls b","a.requisition_no=".$selectResult[csf('requisition_no')]." and a.knit_id=b.id","knitting_party");*/
					
					$kintting_source=$kintting_arr[$selectResult[csf('requisition_no')]]['source'];
					$kintting_com=$kintting_arr[$selectResult[csf('requisition_no')]]['party'];
					$program_no=implode(",",array_unique(explode(",",$kintting_arr[$selectResult[csf('requisition_no')]]['prog_no'])));
					$po_id=$program_po_arr[$program_no];
					$all_po_id=array_unique(explode(",",$po_id));
					$po_no=''; $buyer='';
					foreach($all_po_id as $val)
					{
						if($po_no=='') $po_no=$po_array[$val]['no']; else $po_no.=",".$po_array[$val]['no'];
						if($buyer=='') $buyer=$po_array[$val]['buyer'];
					}
					
					?>
                    <tr>
                        <td width="30" align="center"><?php echo $i; ?></td>
                        <td width="100"><p><?php echo $buyer; ?></p></td>
                        <td width="100"><p><?php echo $po_no; ?></p></td>
                        <td width="80">
                            <p>
                            	<?php 
									if ($kintting_source==1) echo $company_library[$kintting_com]; 
									else if ($kintting_source==3) echo $supplier_arr[$kintting_com];
									else echo "&nbsp;";
								?>
                            </p>
                        </td>
                        <td width="80">&nbsp;&nbsp;<?php echo $program_no; ?></td>
                        <td width="60">&nbsp;&nbsp;<?php echo $selectResult[csf('requisition_no')]; ?></td>
                        <td width="50">&nbsp;<?php echo $product_details_array[$selectResult[csf('prod_id')]]['count']; ?></td>
                        <td width="50"><p><?php echo $product_details_array[$selectResult[csf('prod_id')]]['brand']; ?>&nbsp;</p></td>
                        <td width="50"><p><?php echo $product_details_array[$selectResult[csf('prod_id')]]['lot']; ?></p></td>
                        <td width="110"><p><?php echo $product_details_array[$selectResult[csf('prod_id')]]['comp']; ?></p></td>
                        <td width="70"><p><?php echo $product_details_array[$selectResult[csf('prod_id')]]['type']; ?></p></td>
                        <td width="70"><p><?php echo $product_details_array[$selectResult[csf('prod_id')]]['color']; ?></p></td>
                        <td align="right"><?php echo number_format($selectResult[csf('yarn_demand_qnty')],2); ?></td>	
                    </tr>
					<?php
					$tot_demand_qnty+=$selectResult[csf('yarn_demand_qnty')];
					$i++;
				}
			?>
            <tfoot>
                <th colspan="12" align="right"><b>Total</b></th>
                <th align="right"><?php echo number_format($tot_demand_qnty,2); ?></th>
            </tfoot>
        </table>
        <br>
		 <?php
            echo signature_table(40, $data[1], "950px");
         ?>
    </div>
<?php
exit();
}
?>