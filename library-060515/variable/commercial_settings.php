<?
/*-------------------------------------------- Comments

Purpose			: 	This form will create Order Tracking Variable Settings
					Select company and select Variable List that onchange will change content
					
Functionality	:	Must fill Company, Variable List

JS Functions	:

Created by		:	Bilas 
Creation date 	: 	12-10-2012
Updated by 		: 		
Update date		: 		   

QC Performed BY	:		

QC Date			:	

Comments		:

*/

 

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;
//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Order Tracking Variable Settings", "../../", 1, 1,$unicode,'','');

if ($_SESSION['logic_erp']["data_level_secured"]==1) 
{
	if ($_SESSION['logic_erp']["buyer_id"]!=0 && $_SESSION['logic_erp']["buyer_id"]!="") $buyer_name=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_name="";
	if ($_SESSION['logic_erp']["company_id"]!=0 && $_SESSION['logic_erp']["company_id"]!="") $company_name="and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_name="";
}
else
{
	$buyer_name="";
	$company_name="";
}
?>
 
<script>
if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../../logout.php";  
var permission='<? echo $permission; ?>';


function fnc_variable_settings_commercial( operation )
{
	
		
	if (document.getElementById('cbo_variable_list').value*1==5)
	{
		
		if ( form_validation('cbo_company_name*txt_capacity_value*cbo_currency_id','Company Name*Capacity Value*Currency')==0 )
		{
			return;
		}
		else
		{					
				nocache = Math.random();
				var capacity_in_value	= escape(document.getElementById('capacity_in_value').innerHTML);
				var currency	= escape(document.getElementById('currency').innerHTML);
					
				//eval(get_submitted_variables('cbo_company_name*cbo_variable_list*txt_capacity_value*cbo_currency_id*update_id'));
				var data="action=save_update_delete&operation="+operation+'&capacity_in_value='+capacity_in_value+'&currency='+currency+get_submitted_data_string('cbo_company_name*cbo_variable_list*txt_capacity_value*cbo_currency_id*update_id',"../../");
				freeze_window(operation);
				http.open("POST","requires/commercial_settings_controller.php", true);
				http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				http.send(data);
				http.onreadystatechange = fnc_variable_settings_commercial_reponse;
		}
	
	} // end cbo_variable_list type 5
	
	
	if (document.getElementById('cbo_variable_list').value*1==6)
	{
		
		if ( form_validation('cbo_company_name*txt_max_btb_limit','Company Name*Max BTB Limit')==0 )
		{
			return;
		}
		else
		{					
				nocache = Math.random();
					
				var max_btb_limit	= escape(document.getElementById('max_btb_limit').innerHTML);
				eval(get_submitted_variables('cbo_company_name*cbo_variable_list*txt_max_btb_limit*update_id'));
				var data="action=save_update_delete&operation="+operation+"&max_btb_limit="+max_btb_limit+get_submitted_data_string('cbo_company_name*cbo_variable_list*txt_max_btb_limit*update_id',"../../");
				freeze_window(operation);
				http.open("POST","requires/commercial_settings_controller.php", true);
				http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				http.send(data);
				http.onreadystatechange = fnc_variable_settings_commercial_reponse;
		}
	
	} // end cbo_variable_list type 6
	
	if (document.getElementById('cbo_variable_list').value*1==7)
	{
		
		if ( form_validation('cbo_company_name*txt_max_pc_limit','Company Name*Max PC Limit')==0 )
		{
			return;
		}
		else
		{					
				nocache = Math.random();
					
				var max_pc_limit	= escape(document.getElementById('max_pc_limit').innerHTML);
				eval(get_submitted_variables('cbo_company_name*cbo_variable_list*txt_max_pc_limit*update_id'));
				var data="action=save_update_delete&operation="+operation+"&max_pc_limit="+max_pc_limit+get_submitted_data_string('cbo_company_name*cbo_variable_list*txt_max_pc_limit*update_id',"../../");
				freeze_window(operation);
				http.open("POST","requires/commercial_settings_controller.php", true);
				http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				http.send(data);
				http.onreadystatechange = fnc_variable_settings_commercial_reponse;
		}
	
	} // end cbo_variable_list type 7
	
	if (document.getElementById('cbo_variable_list').value*1==17)
	{
		if ( form_validation('cbo_company_name*cbo_cost_heads*cbo_cost_heads_status','Company Name*Cost Head*Status')==0 )
		{
			return;
		}
		else
		{					
												
				nocache = Math.random();
				
				//eval(get_submitted_variables('cbo_company_name*cbo_variable_list*cbo_cost_heads*cbo_cost_heads_status*update_id'));
				var data="action=save_update_delete&operation="+operation+get_submitted_data_string('cbo_company_name*cbo_variable_list*cbo_cost_heads*cbo_cost_heads_status*update_id',"../../");
				freeze_window(operation);
				http.open("POST","requires/commercial_settings_controller.php", true);
				http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				http.send(data);
				http.onreadystatechange = fnc_variable_settings_commercial_reponse;
		}
	
	} // end cbo_variable_list type 18
	
	
	
}	

function fnc_variable_settings_commercial_reponse()
{
	if(http.readyState == 4) 
	{
		//alert(http.responseText);
		var reponse=trim(http.responseText).split('**');
		if (reponse[0].length>2) reponse[0]=10;
		show_msg(reponse[0]);
		document.getElementById('update_id').value  = reponse[2];
		set_button_status(0, permission, 'fnc_variable_settings_commercial',1);
		reset_form('commercialvariablesettings_1','variable_settings_container','','','','cbo_company_name');
		release_freezing();
	}
}	


</script>

</head>

<body  onLoad="set_hotkey()">
	<div align="center" style="width:100%;">
         
				<? echo load_freeze_divs ("../../",$permission);  ?>
               
	<fieldset style="width:850px;">
		<legend>Commercial Variable Settings</legend>
		<form name="commercialvariablesettings_1" id="commercialvariablesettings_1" autocomplete="off">	
      			<table  width="750" cellspacing="2" cellpadding="0" border="0">
            		<tr>
                		<td width="200" align="left" class="must_entry_caption">Company Name</td>
                        <td width="250">
                           			<? 
										echo create_drop_down( "cbo_company_name", 250, "select company_name,id from lib_company where is_deleted=0  and status_active=1 $company_name order by company_name",'id,company_name', 1, '--- Select Company ---', 0, "show_list_view(document.getElementById('cbo_variable_list').value+'_'+this.value,'on_change_data','variable_settings_container','../variable/requires/commercial_settings_controller','')" );
									?>
                        </td>
                		<td width="200" align="center">Variable List</td>
                        <td width="250">
                            		<? 
										echo create_drop_down( "cbo_variable_list", 250, $commercial_module,'', '1', '---- Select ----', '',"show_list_view(this.value+'_'+document.getElementById('cbo_company_name').value,'on_change_data','variable_settings_container','../variable/requires/commercial_settings_controller','')",''); //data, action, div, path, extra_func 
									?>
                        </td>
            		</tr>
        		</table>
            <div style="width:895px; float:left; min-height:40px; margin:auto" align="center" id="variable_settings_container">
            </div>
		</form>	
	</fieldset>
    </div>
    <script src="../../includes/functions_bottom.js" type="text/javascript"></script> 
 </body>
    

