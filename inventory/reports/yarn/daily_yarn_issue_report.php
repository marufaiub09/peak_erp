﻿<?php
/*-------------------------------------------- Comments
Purpose			: 	This form will create Daily Yarn Issue Report
				
Functionality	:	
JS Functions	:
Created by		:	Fuad Shahriar
Creation date 	: 	21-11-2013
Updated by 		: 		
Update date		: 		   
QC Performed BY	:		
QC Date			:	
Comments		:
*/

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../../../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;
//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Daily Yarn Issue Report","../../../", 1, 1, $unicode,1,1); 

?>	

<script>
var permission='<?php echo $permission; ?>';
if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../logout.php";

function generate_report()
{
	if( form_validation('cbo_company_name*txt_date_from*txt_date_to','Company Name*From Date*To Date')==false )
	{
		return;
	}
	
	var zero_val='';
	var r=confirm("Press \"OK\" to open without Rate & value column\nPress \"Cancel\"  to open with Rate & value column");
	if (r==true)
	{
		zero_val="1";
	}
	else
	{
		zero_val="0";
	} 
	
	var report_title=$( "div.form_caption" ).html();
	var data="action=report_generate"+get_submitted_data_string('cbo_company_name*cbo_yarn_type*cbo_yarn_count*txt_lot_no*txt_date_from*txt_date_to*cbo_issue_purpose',"../../../")+'&report_title='+report_title+"&zero_val="+zero_val;//*txt_job_no
	//alert (data);
	freeze_window(3);
	http.open("POST","requires/daily_yarn_issue_report_controller.php",true);
	http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	http.send(data);
	http.onreadystatechange = generate_report_reponse;  
}

function generate_report_reponse()
{	
	if(http.readyState == 4) 
	{	 
 		var reponse=trim(http.responseText).split("####");
		$("#report_container2").html(reponse[0]);  
		document.getElementById('report_container').innerHTML='<a href="requires/'+reponse[1]+'" style="text-decoration:none"><input type="button" value="Excel Preview" name="excel" id="excel" class="formbutton" style="width:100px"/></a>&nbsp;&nbsp;<input type="button" onclick="new_window()" value="Print Preview" name="Print" class="formbutton" style="width:100px"/>';
		
		show_msg('3');
		release_freezing();
	}
} 
 

function new_window()
{
	document.getElementById('scroll_body').style.overflow="auto";
	document.getElementById('scroll_body').style.maxHeight="none";
	
	var w = window.open("Surprise", "#");
	var d = w.document.open();
	d.write ('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN""http://www.w3.org/TR/html4/strict.dtd">'+
'<html><head><title></title><link rel="stylesheet" href="../../../css/style_common.css" type="text/css" media="print" /></head><body>'+document.getElementById('report_container2').innerHTML+'</body</html>');
	d.close(); 

	document.getElementById('scroll_body').style.overflowY="scroll";
	document.getElementById('scroll_body').style.maxHeight="380px";
}

function openmypage_job()
{
	if( form_validation('cbo_company_name','Company Name')==false )
	{
		return;
	}
	var data=document.getElementById('cbo_company_name').value;
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe','requires/daily_yarn_issue_report_controller.php?action=job_no_popup&data='+data,'Job No Popup', 'width=700px,height=420px,center=1,resize=0','../../')
	
	emailwindow.onclose=function()
	{
		var theemail=this.contentDoc.getElementById("job_no_id");
		var theemailv=this.contentDoc.getElementById("job_no_val");
		var response=theemail.value.split('_');
		if (theemail.value!="")
		{
			freeze_window(5);
			document.getElementById("txt_job_id").value=theemail.value;
			document.getElementById("txt_job_no").value=theemailv.value;
			release_freezing();
		}
	}
}

</script>
</head>

<body onLoad="set_hotkey()">
<div style="width:100%;" align="left">
	<?php echo load_freeze_divs ("../../../",$permission); ?><br />    		 
    <form name="dailyYarnIssueReport_1" id="dailyYarnIssueReport_1" autocomplete="off" > 
        <div style="width:100%;" align="center">
            <fieldset style="width:960px;">
            <legend>Search Panel</legend> 
                <table class="rpt_table" width="950" cellpadding="0" cellspacing="0" border="1" rules="all">
                    <thead>
                        <tr> 	 	
                            <th width="140" class="must_entry_caption">Company</th>                                
                            <th width="120">Yarn Type</th>
                            <th width="100">Count</th>
                            <th width="100">Lot</th>
                            <th width="100">Issue Purpose</th>
<!--                            <th width="80">Job No</th>
-->                            <th class="must_entry_caption">Issue Date</th>
                            <th width="70"><input type="reset" name="res" id="res" value="Reset" style="width:70px" class="formbutton" onClick="reset_form('dailyYarnIssueReport_1','report_container*report_container2','','','','');" /></th>
                        </tr>
                    </thead>
                    <tr align="center">
                        <td>
                            <?php 
                               echo create_drop_down( "cbo_company_name", 140, "select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name","id,company_name", 1, "-- Select Company --", $selected, "" );
                            ?>                            
                        </td>
                        <td> 
                            <?php
                                echo create_drop_down( "cbo_yarn_type", 120, $yarn_type,"", 0, "--Select--", 0, "",0 );
                            ?>
                        </td>
                        <td>
                            <?php
                                echo create_drop_down("cbo_yarn_count",100,"select id,yarn_count from lib_yarn_count where is_deleted=0 and status_active=1 order by yarn_count","id,yarn_count",0, "-- Select --", $selected, "");
                            ?>
                        </td>
                        <td>
                            <input type="text" id="txt_lot_no" name="txt_lot_no" class="text_boxes" style="width:80px" value="" />
                        </td>
                        <td>
                            <?php
                                //echo create_drop_down("cbo_issue_purpose",100,$yarn_issue_purpose,"",1, "-- Select --", $selected, "");
                                echo create_drop_down( "cbo_issue_purpose", 130, $yarn_issue_purpose,"", 1, "--Select Purpose--",$selected,"","","","","","9,10,11,13,14,27,28" );
                            ?>
                        </td>
                        <!-- <td>
                            <input type="text" id="txt_job_no" name="txt_job_no" class="text_boxes" style="width:80px" onDblClick="openmypage_job();" placeholder="Browse" />
                            <input type="hidden" id="txt_job_id" name="txt_job_id" class="text_boxes" style="width:60px" />
                        </td>--> 
                       <td align="center">
                             <input type="text" name="txt_date_from" id="txt_date_from" value="" class="datepicker" style="width:70px" placeholder="From Date"/>
                             To
                             <input type="text" name="txt_date_to" id="txt_date_to" value="" class="datepicker" style="width:70px" placeholder="To Date"/>
                        </td>
                        <td>
                            <input type="button" name="search" id="search" value="Show" onClick="generate_report()" style="width:70px" class="formbutton" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="8" align="center" width="95%"><?php echo load_month_buttons(1); ?></td>
                    </tr>
                </table> 
            </fieldset> 
            
            <div id="report_container" align="center"></div>
            <div id="report_container2"></div>   
            
        </div>
    </form>    
</div>    
</body>  
<script>
	set_multiselect('cbo_yarn_type*cbo_yarn_count','0*0','0*0','','0*0');
</script>
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script> 
</html>
