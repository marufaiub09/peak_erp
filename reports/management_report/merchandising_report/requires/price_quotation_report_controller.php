<?php
/*-------------------------------------------- Comments -----------------------
Purpose			: 	This Form Will Create Woven Garments Price Quotation Entry.
Functionality	:	
JS Functions	:
Created by		:	Bilas 
Creation date 	: 	27-01-2013
Updated by 		: 		
Update date		: 		   
QC Performed BY	:		
QC Date			:	
Comments		:
*/
error_reporting('0');
session_start();
include('../../../../includes/common.php');

extract($_REQUEST);
 
$pc_time= add_time(date("H:i:s",time()),360);  
$pc_date = date("Y-m-d",strtotime(add_time(date("H:i:s",time()),360)));

/*if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }

if ($_SESSION['logic_erp']["data_level_secured"]==1)
{
	if($_SESSION['logic_erp']["buyer_id"]!=0) $buyer_cond=" and id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_cond="";
	if($_SESSION['logic_erp']["company_id"]!=0) $company_cond=" and id in (".$_SESSION['logic_erp']["company_id"].")"; else $company_cond="";
}
else
{
	$buyer_cond="";	$company_cond="";
}*/
$permission=$_SESSION['page_permission'];

//---------------------------------------------------- Start

if ($action=="load_drop_down_buyer")
{
	

	echo create_drop_down( "cbo_buyer_name", 160, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' $buyer_cond and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (1,3,21,90)) order by buyer_name","id,buyer_name", 1, "-- Select Buyer --", $selected, "" );     	 
	exit();
}

if ($action=="load_search_text")
{
	if($data==1)
	{
		$search_txt = "Enter Job Number";
	}
	else if($data==2)
	{
		$search_txt = "Enter Style Ref No";
	}
	else if($data==3)
	{
		$search_txt = "Enter Order No";
	}
	
	echo $search_txt;
	exit();
}



if ($action=="report_generate")
{
	 
		if(str_replace("'","",$cbo_company_name)==0 || str_replace("'","",$cbo_company_name)=="") $company_name="";else $company_name = "and a.company_id=$cbo_company_name";
		//if(str_replace("'","",$cbo_buyer_name)==0 || str_replace("'","",$cbo_buyer_name)=="") $buyer_name=""; else $buyer_name = "and a.buyer_id=$cbo_buyer_name"; 
		
		if(str_replace("'","",$cbo_buyer_name)==0)
		{
			if ($_SESSION['logic_erp']["data_level_secured"]==1)
			{
				if($_SESSION['logic_erp']["buyer_id"]!="") $buyer_id_cond=" and a.buyer_id in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_cond="";
			}
			else
			{
				$buyer_id_cond="";
			}
		}
		else
		{
			$buyer_id_cond=" and a.buyer_id=$cbo_buyer_name";//.str_replace("'","",$cbo_buyer_name)
		}
		if(str_replace("'","",$txt_search_text)!="") $search_text = "and a.style_ref like '%".str_replace("'","",$txt_search_text)."%'";
		if(str_replace("'","",trim($txt_date_from))=="" || str_replace("'","",trim($txt_date_to))=="") $txt_date="";
		else $txt_date=" and est_ship_date between $txt_date_from and $txt_date_to";
		
 		$buyer_arr=return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
		$comp_arr=return_library_array( "select id, company_short_name from lib_company",'id','company_short_name');
		$image_arr=return_library_array( "select master_tble_id, image_location from common_photo_library where form_name='quotation_entry' group by master_tble_id",'master_tble_id','image_location');
		//print_r($image_arr);
		$sql= "select a.id,company_id, buyer_id, agent, costing_per_id, style_desc,season,product_code, pord_dept, offer_qnty, order_uom, est_ship_date, approved,remarks, fabric_cost, trims_cost, embel_cost,wash_cost, cm_cost, commission,(comm_cost+lab_test+inspection+freight+common_oh+currier_pre_cost+certificate_pre_cost) as othercost, final_cost_dzn,total_cost, final_cost_pcs, a1st_quoted_price,asking_quoted_price, confirm_price, revised_price, margin_dzn from  wo_price_quotation a,  wo_price_quotation_costing_mst b where a.id=b.quotation_id $company_name $buyer_id_cond $search_text $txt_date"; 
		$master_sql = sql_select($sql) or die(mysql_error());	
 		//echo $sql;
		ob_start();	
			
 		?>
        	
            <fieldset>
				<div style="width:1700px" align="left">	
						<table width="1680" border="1" rules="all"  class="rpt_table" > 
							<thead>
                            	<tr> 
                                    <th width="20">SL</th>
                                    <th width="40">ID</th> 
                                    <th width="40">Comp.</th>                                    
                                    <th width="50">Buyer</th>
                                    <th width="50">Agent</th>
                                    <th width="80">Style Desc </th> 
                                    <th width="80">Season </th> 
                                    <th width="60">Image</th>
                                    <th width="50">Prod. Dept</th>
                                    <th width="70">Offered Qnty</th>
                                    <th width="35">UOM</th>
                                    <th width="70">Est. Ship Date</th>
                                    <th width="70">Status</th>
                                    <th width="65">Fabric Cost /Dzn</th>
                                    <th width="65">Trims Cost /Dzn</th>
                                    <th width="65">Embel. Cost /Dzn</th>
                                    <th width="65">Gmts Wash. Cost /Dzn</th>
                                    <th width="65">CM Cost /Dzn</th>
                                    
                                    <th width="65">Other Cost /Dzn</th>
                                    <th width="65">Total Cost /Dzn</th>
                                    <th width="65">Cost/Pcs</th>
                                    <th width="65">Quot/Pcs</th>
                                    <th width="65">Asking Price/Pcs</th>
                                    <th width="65">Conf. Price /Pcs</th>
                                    <th width="65">Margin /Pcs</th> 
                                    <th width="65">Comm.</th>
                                    <th width="">Remarks</th>                                    
                                </tr>                            	
						</thead>
 			         </table>
					<div id="report_div" style="width:1700px; max-height:550px; overflow-y:scroll" >	
					<table id="report_tbl" width="1680" border="1"  class="rpt_table" rules="all">
					<?php	
						$k=0; 
						foreach($master_sql as $row )  // Master queery 
						{										 
 								 	
							if ($k%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
							$k++;
							if($row[csf('confirm_price')]>0)
							{
							$sl_bg_color="#66CC00";	
							}
							else
							{
                                                            $sl_bg_color=$bgcolor;	
							}
							 
							?>
								<tr bgcolor="<?php echo $bgcolor;?>" onclick="change_color('tr3_<?php echo $k; ?>','<?php echo $bgcolor;?>')" id="tr3_<?php echo $k; ?>" >
									<td width="20" bgcolor="<?php echo $sl_bg_color;  ?>"> <?php echo $k; ?> </td>
                                    <td width="40" align="left" ><p><?php echo $row[csf('id')]; ?></p></td>	
									<td width="40" align="left" ><p><?php echo $comp_arr[$row[csf('company_id')]]; ?></p></td>	
									<td width="50" align="left"><p><?php echo $buyer_arr[$row[csf('buyer_id')]]; ?></p></td>
									<td width="50" align="left"><p><?php echo $buyer_arr[$row[csf('agent')]]; ?></p></td>
									<td width="80" align="left"><p><?php echo trim($row[csf('style_desc')]); ?></p></td> 
                                     <td width="80" align="left"><p><?php echo trim($row[csf('season')]); ?></td>                               
									<td width="60" align="center" ><a href="#" onClick="file_uploader ( '../../../', <?php echo $row[csf('id')]; ?>,'', 'quotation_entry', 0 ,1,2)" title="Click To View Large" ><img height="20" width="25" src="../../../<?php echo $image_arr[$row[csf('id')]]; ?>" /></a></td>
                                    
                                    <td width="50" align="left"><p><?php echo $row[csf('product_code')]." ". $product_dept[$row[csf('pord_dept')]]; ?></p></td>
                                    <td width="70" align="right"><?php echo number_format($row[csf('offer_qnty')]); ?></td>
									<td width="35" align="left"><?php echo $unit_of_measurement[$row[csf('order_uom')]]; ?></td> 
                                    <td width="70" align="left"><?php echo change_date_format($row[csf('est_ship_date')]); ?></td> 
                                    <td width="70" align="left"><?php if($row[csf('confirm_price')]==0) echo "Under Process"; else echo "Confirm"; ?></td> 
                                    <td width="65" align="right"><?php echo number_format($row[csf('fabric_cost')],2); ?></td> 
                                    <td width="65" align="right"><?php echo number_format($row[csf('trims_cost')],2); ?></td> 
                                    <td width="65" align="right"><?php echo number_format($row[csf('embel_cost')],2); ?></td>
                                    <td width="65" align="right"><?php echo number_format($row[csf('wash_cost')],2); ?></td>  
                                    <td width="65" align="right"><?php echo number_format($row[csf('cm_cost')],2); ?></td>
                                     
                                    <td width="65" align="right"><?php echo number_format($row[csf('othercost')],2); ?></td>
                                    <td width="65" align="right"><?php echo number_format($row[csf('total_cost')],2); ?></td> 
                                    <td width="65" align="right"><?php echo number_format($row[csf('final_cost_pcs')],2); ?></td>
                                    <?php if($row[csf('revised_price')] > 0) $row[csf('1st_quoted_price')]=$row[csf('revised_price')];  ?>
                                    <td width="65" align="right" ><?php echo number_format($row[csf('1st_quoted_price')],2); ?></td>
                                    <td width="65" align="right" ><?php echo number_format($row[csf('asking_quoted_price')],2); ?></td>  
                                    <td width="65" align="right" ><?php echo number_format($row[csf('confirm_price')],2); ?></td> 
                                    <?php 
										if($row[csf("costing_per_id")]==1){$order_price_per_dzn=12;}
										else if($row[csf("costing_per_id")]==2){$order_price_per_dzn=1;}
										else if($row[csf("costing_per_id")]==3){$order_price_per_dzn=24;}
										else if($row[csf("costing_per_id")]==4){$order_price_per_dzn=36;}
										else if($row[csf("costing_per_id")]==5){$order_price_per_dzn=48;}
										
										$row[csf('margin_dzn')] = $row[csf('margin_dzn')]/$order_price_per_dzn;
									?>
                                    <td width="65" align="right"<?php if($row[csf('margin_dzn')]<0) echo"bgcolor='#FF0000'";?>><?php echo number_format($row[csf('margin_dzn')],2); ?></td>
                                    <td width="65" align="right"><?php echo number_format($row[csf('commission')],2); ?></td>
                                    <td width="" align="right"><p><?php echo $row[csf('remarks')]; ?></p></td>    
   								</tr>
                          <?php 		
						  	
							$offer_qnty += $row[csf('offer_qnty')];
							$fabric_cost += number_format($row[csf('fabric_cost')],2); 
							$trims_cost += number_format($row[csf('trims_cost')],2); 
							$embel_cost += number_format($row[csf('embel_cost')],2); 
							$wash_cost += number_format($row[csf('wash_cost')],2); 
							$cm_cost += number_format($row[csf('cm_cost')],2); 
							
							$othercost += number_format($row[csf('othercost')],2); 
							$final_cost_dzn += number_format($row[csf('total_cost')],2); 
 							$final_cost_pcs += number_format($row[csf('final_cost_pcs')],2);
 							$st_quoted_price += number_format($row[csf('a1st_quoted_price')],2); 
							$asking_price += number_format($row[csf('asking_quoted_price')],2);
							$confirm_price += number_format($row[csf('confirm_price')],2); 
							$margin_dzn += number_format($row[csf('margin_dzn')],2);
							$commisioncost += number_format($row[csf('commission')],2); 
							
                       	 }// Master table query
                        ?>
                  </table>
                  <table width="1680" border="1" rules="all"  class="rpt_table" > 
                  <tfoot>
                  <tr> 
                                    <th width="20"></th>
                                     <th width="40"></th>  
                                    <th width="40"></th>                                    
                                    <th width="50"></th>
                                    <th width="50"></th>
                                    <th width="80"> </th> 
                                    <th width="80"> </th> 
                                    <th width="60"></th>
                                    <th width="50"></th>
                                    <th width="70"><?php echo number_format($offer_qnty);?></th>
                                    <th width="35"></th>
                                    <th width="70"></th>
                                    <th width="70"></th>
                                    <th width="65"><?php echo number_format($fabric_cost,2);?></th>
                                    <th width="65"><?php echo number_format($trims_cost,2);?></th>
                                    <th width="65"><?php echo number_format($embel_cost,2);?></th>
                                    <th width="65"><?php echo number_format($wash_cost,2);?></th>
                                    <th width="65"><?php echo number_format($cm_cost,2);?></th>
                                    
                                    <th width="65"><?php echo number_format($othercost,2);?></th>
                                    <th width="65"><?php echo number_format($final_cost_dzn,2);?></th>
                                    <th width="65"><?php echo number_format($final_cost_pcs,2);?></th>
                                    <th width="65"><?php echo number_format($st_quoted_price,2);?></th>
                                    <th width="65"><?php echo number_format($asking_price,2);?></th>
                                    <th width="65"><?php echo number_format($confirm_price,2);?></th>
                                    <th width="65"><?php echo number_format($margin_dzn,2);?></th> 
                                    <th width="65"><?php echo number_format($commisioncost,2);?></th>
                                    <th width=""></th>                                    
                                </tr>     
                            <!--<tr>
                                <th colspan="8" align="right"><b>Total</b></th>
                                <th align="right"><b><? echo number_format($offer_qnty);?></b></th>
                                <th colspan="3"></th>
                                <th align="right"><b><? echo number_format($fabric_cost,2);?></b></th>
                                <th align="right"><b><? echo number_format($trims_cost,2);?></b></th>
                                <th align="right"><b><? echo number_format($embel_cost,2);?></b></th>
                                <th align="right"><b><? echo number_format($wash_cost,2);?></b></th>
                                <th align="right"><b><? echo number_format($cm_cost,2);?></b></th>
                               
                                <th align="right"><b><? echo number_format($othercost,2);?></b></th>
                                <th align="right"><b><? echo number_format($final_cost_dzn,2);?></b></th>
                                <th align="right"><b><? echo number_format($final_cost_pcs,2);?></b></th>
                                <th align="right"><b><? echo number_format($st_quoted_price,2);?></b></th>
                                <th align="right"><b><? echo number_format($asking_price,2);?></b></th>
                                <th align="right"><b><? echo number_format($confirm_price,2);?></b></th>
                                <th align="right"><b><? echo number_format($margin_dzn,2);?></b></th> 
                                <th align="right"><b><? echo number_format($commisioncost,2);?></b></th> 
                                <th align="right"><b></b></th>                                                                
                            </tr>-->
                        </tfoot> 
                        </table>   
                  </div>						
              </div>
 			</fieldset>	
        
		<?php
		
		$html = ob_get_contents();
		ob_clean();
		 
		foreach (glob($_SESSION['logic_erp']['user_id']."*") as $filename) {
		@unlink($filename);
		}
		
		$name=time();
		$filename=$_SESSION['logic_erp']['user_id']."_".$name.".xls";
		$create_new_doc = fopen($filename, 'w');	
		$is_created = fwrite($create_new_doc,$html);
		echo "$html****$filename";
		exit();	
			
}



exit();


?>
