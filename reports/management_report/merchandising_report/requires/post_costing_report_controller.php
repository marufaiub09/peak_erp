<?php

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../../../includes/common.php');

$_SESSION['page_permission']=$permission;
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
//--------------------------------------------------------------------------------------------------------------------

$user_name = $_SESSION['logic_erp']["user_id"];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

if ($action=="load_drop_down_buyer")
{
	echo create_drop_down( "cbo_buyer_name", 150, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name","id,buyer_name", 1, "-- All Buyer --", $selected, "" );     	 
	exit();
}

if($action=="order_no_search_popup")
{
	echo load_html_head_contents("Order No Info", "../../../../", 1, 1,'','','');
	extract($_REQUEST);
	?>
     
	<script>
		
		var selected_id = new Array; var selected_name = new Array;
		
		function check_all_data()
		{
			var tbl_row_count = document.getElementById( 'tbl_list_search' ).rows.length;
			tbl_row_count = tbl_row_count - 1;

			for( var i = 1; i <= tbl_row_count; i++ )
			{
				$('#tr_'+i).trigger('click'); 
			}
		}
		
		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
		function js_set_value( str ) {
			
			if (str!="") str=str.split("_");
			 
			toggle( document.getElementById( 'tr_' + str[0] ), '#FFFFCC' );
			 
			if( jQuery.inArray( str[1], selected_id ) == -1 ) {
				selected_id.push( str[1] );
				selected_name.push( str[2] );
				
			}
			else {
				for( var i = 0; i < selected_id.length; i++ ) {
					if( selected_id[i] == str[1] ) break;
				}
				selected_id.splice( i, 1 );
				selected_name.splice( i, 1 );
			}
			var id = ''; var name = '';
			for( var i = 0; i < selected_id.length; i++ ) {
				id += selected_id[i] + ',';
				name += selected_name[i] + '*';
			}
			
			id = id.substr( 0, id.length - 1 );
			name = name.substr( 0, name.length - 1 );
			
			$('#hide_order_id').val( id );
			$('#hide_order_no').val( name );
		}
	
    </script>

</head>

<body>
<div align="center">
	<form name="styleRef_form" id="styleRef_form">
		<fieldset style="width:780px;">
            <table width="770" cellspacing="0" cellpadding="0" border="1" rules="all" align="center" class="rpt_table" id="tbl_list">
            	<thead>
                    <th>Buyer</th>
                    <th>Search By</th>
                    <th id="search_by_td_up" width="170">Please Enter Order No</th>
                    <th>Shipment Date</th>
                    <th><input type="reset" name="button" class="formbutton" value="Reset"  style="width:100px;"></th> 
                    <input type="hidden" name="hide_order_no" id="hide_order_no" value="" />
                    <input type="hidden" name="hide_order_id" id="hide_order_id" value="" />
                </thead>
                <tbody>
                	<tr>
                        <td align="center">
                        	 <?php 
								echo create_drop_down( "cbo_buyer_name", 140, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company=$companyID $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name","id,buyer_name",1, "-- All Buyer--",0,"",0 );
							?>
                        </td>                 
                        <td align="center">	
                    	<?php
                       		$search_by_arr=array(1=>"Order No",2=>"Style Ref",3=>"Job No");
							$dd="change_search_event(this.value, '0*0*0', '0*0*0', '../../') ";							
							echo create_drop_down( "cbo_search_by", 110, $search_by_arr,"",0, "--Select--", "",$dd,0 );
						?>
                        </td>     
                        <td align="center" id="search_by_td">				
                            <input type="text" style="width:130px" class="text_boxes" name="txt_search_common" id="txt_search_common" />	
                        </td> 
                        <td align="center">
                            <input type="text" name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px" readonly>To
                            <input type="text" name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px" readonly>
                        </td>	
                        <td align="center">
                        	<input type="button" name="button" class="formbutton" value="Show" onClick="show_list_view ('<?php echo $companyID; ?>'+'**'+document.getElementById('cbo_buyer_name').value+'**'+document.getElementById('cbo_search_by').value+'**'+document.getElementById('txt_search_common').value+'**'+document.getElementById('txt_date_from').value+'**'+document.getElementById('txt_date_to').value, 'create_order_no_search_list_view', 'search_div', 'post_costing_report_controller', 'setFilterGrid(\'tbl_list_search\',-1)');" style="width:100px;" />
                    	</td>
                    </tr>
                    <tr>
                        <td colspan="5" height="20" valign="middle"><?php echo load_month_buttons(1); ?></td>
                    </tr>
            	</tbody>
           	</table>
            <div style="margin-top:15px" id="search_div"></div>
		</fieldset>
	</form>
</div>
</body>           
<script src="../../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
	exit(); 
}

if($action=="create_order_no_search_list_view")
{
	$data=explode('**',$data);
	$company_id=$data[0];

	if($data[1]==0)
	{
		if ($_SESSION['logic_erp']["data_level_secured"]==1)
		{
			if($_SESSION['logic_erp']["buyer_id"]!="") $buyer_id_cond=" and a.buyer_name in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_id_cond="";
		}
		else
		{
			$buyer_id_cond="";
		}
	}
	else
	{
		$buyer_id_cond=" and a.buyer_name=$data[1]";
	}
	
	$search_by=$data[2];
	$search_string="%".trim($data[3])."%";

	if($search_by==1) 
		$search_field="b.po_number"; 
	else if($search_by==2) 
		$search_field="a.style_ref_no"; 	
	else 
		$search_field="a.job_no";
		
	$start_date =$data[4];
	$end_date =$data[5];	
	
	if($start_date!="" && $end_date!="")
	{
		if($db_type==0)
		{
			$date_cond="and b.pub_shipment_date between '".change_date_format(trim($start_date),"yyyy-mm-dd")."' and '".change_date_format(trim($end_date),"yyyy-mm-dd")."'";
		}
		else
		{
			$date_cond="and b.pub_shipment_date between '".change_date_format(trim($start_date),'','',1)."' and '".change_date_format(trim($end_date),'','',1)."'";	
		}
	}
	else
	{
		$date_cond="";
	}
	
	$company_short_arr=return_library_array( "select id, company_short_name from lib_company",'id','company_short_name');
	$buyer_arr=return_library_array( "select id, short_name from lib_buyer", "id", "short_name"  );
	$arr=array (0=>$company_short_arr,1=>$buyer_arr);
	
	if($db_type==0) $year_field="YEAR(a.insert_date) as year"; 
	else if($db_type==2) $year_field="to_char(a.insert_date,'YYYY') as year";
	else $year_field="";//defined Later
	
	$sql= "select b.id, a.job_no, $year_field, a.job_no_prefix_num, a.company_name, a.buyer_name, a.style_ref_no, b.po_number, b.pub_shipment_date from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.company_name=$company_id and $search_field like '$search_string' $buyer_id_cond $date_cond order by b.id, b.pub_shipment_date";
		
	echo create_list_view("tbl_list_search", "Company,Buyer Name,Year,Job No,Style Ref. No, Po No, Shipment Date", "70,70,50,70,150,180","760","220",0, $sql , "js_set_value", "id,po_number","",1,"company_name,buyer_name,0,0,0,0,0",$arr,"company_name,buyer_name,year,job_no_prefix_num,style_ref_no,po_number,pub_shipment_date","",'','0,0,0,0,0,0,3','',1) ;
   exit(); 
}

if($action=="report_generate")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	$company_arr=return_library_array( "select id, company_name from lib_company",'id','company_name');
	$buyer_arr=return_library_array( "select id, short_name from lib_buyer", "id", "short_name"  );

	$company_name=str_replace("'","",$cbo_company_name);
	
	if(str_replace("'","",$cbo_buyer_name)==0)
	{
		if ($_SESSION['logic_erp']["data_level_secured"]==1)
		{
			if($_SESSION['logic_erp']["buyer_id"]!="") $buyer_id_cond=" and a.buyer_name in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_id_cond="";
		}
		else
		{
			$buyer_id_cond="";
		}
	}
	else
	{
		$buyer_id_cond=" and a.buyer_name=$cbo_buyer_name";//.str_replace("'","",$cbo_buyer_name)
	}
	
	$txt_job_no=str_replace("'","",$txt_job_no);
	if(trim($txt_job_no)!="") $job_no=trim($txt_job_no); else $job_no="%%";
	
	$cbo_year=str_replace("'","",$cbo_year);
	if(trim($cbo_year)!=0) 
	{
		if($db_type==0) $year_cond=" and YEAR(a.insert_date)=$cbo_year";
		else if($db_type==2) $year_cond=" and to_char(a.insert_date,'YYYY')=$cbo_year";
		else $year_cond="";
	}
	else $year_cond="";
	
	$date_cond='';
	if(str_replace("'","",$txt_date_from)!="" && str_replace("'","",$txt_date_to)!="")
	{
		$date_cond=" and b.pub_shipment_date between $txt_date_from and $txt_date_to";
	}
	
	$po_id_cond="";
	if(trim(str_replace("'","",$txt_order_no))!="")
	{
		if(str_replace("'","",$hide_order_id)!="")
		{
			$po_id_cond=" and b.id in(".str_replace("'","",$hide_order_id).")";
		}
		else
		{
			$po_id_cond=" and b.po_number like '".trim(str_replace("'","",$txt_order_no))."%'";
		}
	}
	
	$shipping_status_cond='';
	if(str_replace("'","",$shipping_status)!=0)
	{
		$shipping_status_cond=" and b.shiping_status=$shipping_status";
	}
	
	$exchange_rate=76; $po_ids_array=array();
	
	$ex_factory_arr=return_library_array( "select po_break_down_id, sum(ex_factory_qnty) as qnty from pro_ex_factory_mst where status_active=1 and is_deleted=0 group by po_break_down_id", "po_break_down_id", "qnty");	
		
	if($db_type==0) $year_field="YEAR(a.insert_date) as year"; 
	else if($db_type==2) $year_field="to_char(a.insert_date,'YYYY') as year";
	else $year_field="";//defined Later
		
	$sql="select a.job_no_prefix_num, a.job_no, $year_field, a.company_name, a.buyer_name, a.style_ref_no, a.order_uom, a.gmts_item_id, a.total_set_qnty as ratio, b.id, b.po_number, b.pub_shipment_date, b.po_quantity, b.plan_cut, b.unit_price, b.po_total_price, b.shiping_status from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.company_name='$company_name' and a.job_no_prefix_num like '$job_no' and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 $date_cond $buyer_id_cond $po_id_cond $shipping_status_cond $year_cond order by b.pub_shipment_date, a.job_no_prefix_num, b.id";
	//echo $sql; die;
	$result=sql_select($sql);
	ob_start();
	?>
    <fieldset>
    	<table width="2080">
            <tr class="form_caption">
                <td colspan="20" align="center">Post Costing Report</td>
            </tr>
        </table>
        <table style="margin-top:10px" id="table_header_1" class="rpt_table" width="3170" cellpadding="0" cellspacing="0" border="1" rules="all">
            <thead>
                <th width="40">SL</th>
                <th width="70">Buyer</th>
                <th width="60">Job Year</th>
                <th width="70">Job No</th>
                <th width="100">PO No</th>
                <th width="110">Style Name</th>
                <th width="120">Garments Item</th>
                <th width="90">PO Quantity</th>
                <th width="50">UOM</th>
                <th width="70">Unit Price</th>
                <th width="110">PO Value</th>
                <th width="80">Shipment Date</th>
                <th width="100">Shipping Status</th>
                <th width="100">Cost Source</th>
                <th width="100">PO/Ex-Factory Qnty</th>
                <th width="110">PO/Ex-Factory Value</th>
                <th width="110">Yarn Cost</th>
                <th width="110">Knitting Cost</th>
                <th width="110">Dye & Fin Cost</th>
                <!--<th width="110">Yarn Dyeing Cost</th>-->
                <th width="110">AOP Cost</th>
                <th width="110">Trims Cost</th>
                <th width="100">Embellishment Cost</th>
                <th width="100">Wash Cost</th>
                <th width="100">Commission Cost</th>
                <th width="100">Commercial Cost</th>
                <th width="100">Freight Cost</th>
                <th width="100">Testing Cost</th>
                <th width="100">Inspection Cost</th>
                <th width="100">Courier Cost</th>
                <th width="100">CM Cost</th>
                <th width="100">Toal Cost</th>
                <th width="100">Margin</th>
                <th>% to Ex-Factory Value</th>
            </thead>
        </table>
    	<div style="width:3192px; max-height:400px; overflow-y:scroll" id="scroll_body">
            <table class="rpt_table" width="3170" cellpadding="0" cellspacing="0" border="1" rules="all" id="table_body">
            <?php
				$fabriccostArray=array(); $yarncostArray=array(); $trimsCostArray=array(); $prodcostArray=array(); $actualCostArray=array(); $actualTrimsCostArray=array(); 
				$subconCostArray=array(); $embellCostArray=array(); $washCostArray=array(); $aopCostArray=array(); $yarnTrimsCostArray=array(); 
				$yarncostDataArray=sql_select("select job_no, sum(amount) as amount from wo_pre_cost_fab_yarn_cost_dtls where status_active=1 and is_deleted=0 group by job_no");
				foreach($yarncostDataArray as $yarnRow)
				{
				   $yarncostArray[$yarnRow[csf('job_no')]]=$yarnRow[csf('amount')];
				}
				
				$fabriccostDataArray=sql_select("select job_no, costing_per_id, embel_cost, wash_cost, cm_cost, commission, currier_pre_cost, lab_test, inspection, freight, comm_cost from wo_pre_cost_dtls where status_active=1 and is_deleted=0");
				foreach($fabriccostDataArray as $fabRow)
				{
					 $fabriccostArray[$fabRow[csf('job_no')]]['costing_per_id']=$fabRow[csf('costing_per_id')];
					 $fabriccostArray[$fabRow[csf('job_no')]]['embel_cost']=$fabRow[csf('embel_cost')];
					 $fabriccostArray[$fabRow[csf('job_no')]]['wash_cost']=$fabRow[csf('wash_cost')];
					 $fabriccostArray[$fabRow[csf('job_no')]]['cm_cost']=$fabRow[csf('cm_cost')];
					 $fabriccostArray[$fabRow[csf('job_no')]]['commission']=$fabRow[csf('commission')];
					 $fabriccostArray[$fabRow[csf('job_no')]]['currier_pre_cost']=$fabRow[csf('currier_pre_cost')];
					 $fabriccostArray[$fabRow[csf('job_no')]]['lab_test']=$fabRow[csf('lab_test')];
					 $fabriccostArray[$fabRow[csf('job_no')]]['inspection']=$fabRow[csf('inspection')];
					 $fabriccostArray[$fabRow[csf('job_no')]]['freight']=$fabRow[csf('freight')];
					 $fabriccostArray[$fabRow[csf('job_no')]]['comm_cost']=$fabRow[csf('comm_cost')];
				}
				
				$trimscostDataArray=sql_select("select b.po_break_down_id, sum(b.cons*a.rate) as total from wo_pre_cost_trim_cost_dtls a, wo_pre_cost_trim_co_cons_dtls b where a.id=b.wo_pre_cost_trim_cost_dtls_id and a.status_active=1 and a.is_deleted=0 group by b.po_break_down_id");
				foreach($trimscostDataArray as $trimsRow)
				{
					 $trimsCostArray[$trimsRow[csf('po_break_down_id')]]=$trimsRow[csf('total')];
				}
				 
				$prodcostDataArray=sql_select("select job_no, 
									  sum(CASE WHEN cons_process=1 THEN amount END) AS knit_charge,
									  sum(CASE WHEN cons_process=30 THEN amount END) AS yarn_dye_charge,
									  sum(CASE WHEN cons_process=35 THEN amount END) AS aop_charge,
									  sum(CASE WHEN cons_process not in(1,2,30,35) THEN amount END) AS dye_finish_charge
									  from wo_pre_cost_fab_conv_cost_dtls where status_active=1 and is_deleted=0 group by job_no");
				foreach($prodcostDataArray as $prodRow)
				{
					$prodcostArray[$prodRow[csf('job_no')]]['knit_charge']=$prodRow[csf('knit_charge')];
					$prodcostArray[$prodRow[csf('job_no')]]['yarn_dye_charge']=$prodRow[csf('yarn_dye_charge')];
					$prodcostArray[$prodRow[csf('job_no')]]['aop_charge']=$prodRow[csf('aop_charge')];
					$prodcostArray[$prodRow[csf('job_no')]]['dye_finish_charge']=$prodRow[csf('dye_finish_charge')];
				}	
				
				$actualCostDataArray=sql_select("select cost_head,po_id,sum(amount_usd) as amount_usd from wo_actual_cost_entry where company_id=$company_name and status_active=1 and is_deleted=0 group by cost_head,po_id");
				foreach($actualCostDataArray as $actualRow)
				{
				   $actualCostArray[$actualRow[csf('cost_head')]][$actualRow[csf('po_id')]]=$actualRow[csf('amount_usd')];
				}
				
				$subconInBillDataArray=sql_select("select b.order_id, 
									  sum(CASE WHEN a.process_id=2 THEN b.amount END) AS knit_bill,
									  sum(CASE WHEN a.process_id=4 THEN b.amount END) AS dye_finish_bill
									  from subcon_inbound_bill_mst a, subcon_inbound_bill_dtls b where a.id=b.mst_id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.party_source=1 and a.process_id in(2,4) group by b.order_id");
				foreach($subconInBillDataArray as $subRow)
				{
					$subconCostArray[$subRow[csf('order_id')]]['knit_bill']=$subRow[csf('knit_bill')];
					$subconCostArray[$subRow[csf('order_id')]]['dye_finish_bill']=$subRow[csf('dye_finish_bill')];
				}	
				
				$subconOutBillDataArray=sql_select("select b.order_id, 
									  sum(CASE WHEN a.process_id=2 THEN b.amount END) AS knit_bill,
									  sum(CASE WHEN a.process_id=4 THEN b.amount END) AS dye_finish_bill
									  from subcon_outbound_bill_mst a, subcon_outbound_bill_dtls b where a.id=b.mst_id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.process_id in(2,4) group by b.order_id");
				foreach($subconOutBillDataArray as $subRow)
				{
					$subconCostArray[$subRow[csf('order_id')]]['knit_bill']+=$subRow[csf('knit_bill')];
					$subconCostArray[$subRow[csf('order_id')]]['dye_finish_bill']+=$subRow[csf('dye_finish_bill')];
				}
				
				$embell_type_arr=return_library_array( "select id, emb_name from wo_pre_cost_embe_cost_dtls", "id", "emb_name");	
				
				$bookingDataArray=sql_select("select a.booking_type, a.item_category, a.currency_id, a.exchange_rate, b.po_break_down_id, b.process, b.amount, b.pre_cost_fabric_cost_dtls_id from wo_booking_mst a, wo_booking_dtls b where a.booking_no=b.booking_no and a.company_id=$company_name and a.item_category in(4,12,25) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0");
				foreach($bookingDataArray as $woRow)
				{
					$amount=0; $trimsAmnt=0;
					if($woRow[csf('currency_id')]==1) { $amount=$woRow[csf('amount')]/$exchange_rate; } else { $amount=$woRow[csf('amount')]; }
					
					if($woRow[csf('item_category')]==25 && ($woRow[csf('booking_type')]==3 || $woRow[csf('booking_type')]==6)) 
					{ 
						if($embell_type_arr[$woRow[csf('pre_cost_fabric_cost_dtls_id')]]==3)
						{
							$washCostArray[$woRow[csf('po_break_down_id')]]+=$amount; 
						}
						else
						{
							$embellCostArray[$woRow[csf('po_break_down_id')]]+=$amount; 
						}
					}
					else if($woRow[csf('item_category')]==12 && $woRow[csf('process')]==35 && ($woRow[csf('booking_type')]==3 || $woRow[csf('booking_type')]==6)) 
					{ 
						$aopCostArray[$woRow[csf('po_break_down_id')]]+=$amount; 
					}
					else if($woRow[csf('item_category')]==4)
					{
						if($woRow[csf('currency_id')]==1) { $trimsAmnt=$woRow[csf('amount')]/$woRow[csf('exchange_rate')]; } else { $trimsAmnt=$woRow[csf('amount')]; }
						$actualTrimsCostArray[$woRow[csf('po_break_down_id')]]+=$trimsAmnt; 
					}
				}	
				
				$avg_rate_array=return_library_array( "select id, avg_rate_per_unit from product_details_master where item_category_id=1", "id", "avg_rate_per_unit"  );
				$receive_array=array();
				$sql_receive="select prod_id, sum(order_qnty) as qty, sum(order_amount) as amnt from inv_transaction where transaction_type=1 and item_category=1 and status_active=1 and is_deleted=0 group by prod_id";
				$resultReceive = sql_select($sql_receive);
				foreach($resultReceive as $invRow)
				{
					$avg_rate=$invRow[csf('amnt')]/$invRow[csf('qty')];
					$receive_array[$invRow[csf('prod_id')]]=$avg_rate;
				}
				
				$yarnTrimsDataArray=sql_select("select b.po_breakdown_id, b.prod_id, a.item_category, 
					sum(CASE WHEN a.transaction_type=2 and b.entry_form ='3' and b.trans_type=2 and b.issue_purpose!=2 THEN b.quantity ELSE 0 END) AS yarn_iss_qty,
					sum(CASE WHEN a.transaction_type=4 and b.entry_form ='9' and b.trans_type=4 THEN b.quantity ELSE 0 END) AS yarn_iss_return_qty,
					sum(CASE WHEN a.transaction_type=5 and b.entry_form ='11' and b.trans_type=5 THEN b.quantity ELSE 0 END) AS trans_in_qty_yarn,
					sum(CASE WHEN a.transaction_type=6 and b.entry_form ='11' and b.trans_type=6 THEN b.quantity ELSE 0 END) AS trans_out_qty_yarn,
					sum(CASE WHEN a.transaction_type=2 and b.entry_form ='25' and b.trans_type=2 THEN a.cons_rate*b.quantity ELSE 0 END) AS trims_issue_amnt
					from inv_transaction a, order_wise_pro_details b where a.id=b.trans_id and a.item_category in(1,4) and a.transaction_type in(2,4,5,6) and a.status_active=1 and a.is_deleted=0 and b.is_deleted=0 and b.status_active=1 group by b.po_breakdown_id, b.prod_id, a.item_category");
				foreach($yarnTrimsDataArray as $invRow)
				{
					if($invRow[csf('item_category')]==1)
					{
						$iss_qty=$invRow[csf('yarn_iss_qty')]+$invRow[csf('trans_in_qty_yarn')]-$invRow[csf('yarn_iss_return_qty')]-$invRow[csf('trans_out_qty_yarn')];
						$rate='';
						if($receive_array[$invRow[csf('prod_id')]]>0)
						{
							$rate=$receive_array[$invRow[csf('prod_id')]];
						}
						else
						{
							$rate=$avg_rate_array[$invRow[csf('prod_id')]]/$exchange_rate;
						}
						
						$iss_amnt=$iss_qty*$rate;
						$yarnTrimsCostArray[$invRow[csf('po_breakdown_id')]][1]+=$iss_amnt;
					}
					else
					{
						$yarnTrimsCostArray[$invRow[csf('po_breakdown_id')]][4]+=$invRow[csf('trims_issue_amnt')];
					}
				}
				
				$i=1; $tot_po_qnty=0; $tot_po_value=0; $tot_ex_factory_qnty=0; $tot_ex_factory_val=0; $tot_yarn_cost_mkt=0; $tot_knit_cost_mkt=0; $tot_dye_finish_cost_mkt=0; $tot_yarn_dye_cost_mkt=0; $tot_aop_cost_mkt=0; $tot_trims_cost_mkt=0; $tot_embell_cost_mkt=0; $tot_wash_cost_mkt=0; $tot_commission_cost_mkt=0; $tot_comm_cost_mkt=0; $tot_freight_cost_mkt=0; $tot_test_cost_mkt=0; $tot_inspection_cost_mkt=0; $tot_currier_cost_mkt=0; $tot_cm_cost_mkt=0; $tot_mkt_all_cost=0; $tot_mkt_margin=0; $tot_yarn_cost_actual=0; $tot_knit_cost_actual=0; $tot_dye_finish_cost_actual=0; $tot_yarn_dye_cost_actual=0; $tot_aop_cost_actual=0; $tot_trims_cost_actual=0; $tot_embell_cost_actual=0; $tot_wash_cost_actual=0; $tot_commission_cost_actual=0; $tot_comm_cost_actual=0; $tot_freight_cost_actual=0; $tot_test_cost_actual=0; $tot_inspection_cost_actual=0; $tot_currier_cost_actual=0; $tot_cm_cost_actual=0; $tot_actual_all_cost=0; $tot_actual_margin=0;
				
				$buyer_name_array=array();

				$mkt_po_val_array = array(); $mkt_yarn_array = array(); $mkt_knit_array = array(); $mkt_dy_fin_array = array(); $mkt_yarn_dy_array = array();
				$mkt_aop_array = array(); $mkt_trims_array = array(); $mkt_emb_array = array(); $mkt_wash_array=array(); $mkt_commn_array=array(); $mkt_commercial_array=array();
				$mkt_freight_array = array(); $mkt_test_array = array(); $mkt_ins_array = array(); $mkt_courier_array = array(); $mkt_cm_array = array();
				$mkt_total_array = array(); $mkt_margin_array = array();
				
				$ex_factory_val_array= array(); $yarn_cost_array= array(); $knit_cost_array= array(); $dye_cost_array= array(); $yarn_dyeing_cost_array= array();
				$aop_n_others_cost_array= array(); $trims_cost_array= array(); $enbellishment_cost_array= array(); $wash_cost_array= array(); $commission_cost_array= array(); 
				$commercial_cost_array= array(); $freight_cost_array= array(); $testing_cost_array= array(); $inspection_cost_array= array(); $courier_cost_array= array();
				$cm_cost_array= array(); $total_cost_array= array(); $margin_array= array(); $ex_factory_array=array(); $actual_cm_amnt=array();
				
				foreach($result as $row)
				{
					if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
					
					$po_ids_array[]=$row[csf('id')];
					$gmts_item='';
					$gmts_item_id=explode(",",$row[csf('gmts_item_id')]);
					foreach($gmts_item_id as $item_id)
					{
						if($gmts_item=="") $gmts_item=$garments_item[$item_id]; else $gmts_item.=",".$garments_item[$item_id];
					}
					
					$order_qnty_in_pcs=$row[csf('po_quantity')]*$row[csf('ratio')];
					$plan_cut_qnty=$row[csf('plan_cut')]*$row[csf('ratio')];
					$unit_price=$row[csf('unit_price')]/$row[csf('ratio')];
					$po_value=$order_qnty_in_pcs*$unit_price;
					
					$tot_po_qnty+=$order_qnty_in_pcs; 
					$tot_po_value+=$po_value;
					
					$ex_factory_qty=$ex_factory_arr[$row[csf('id')]];
					$ex_factory_value=$ex_factory_qty*$unit_price;
					
					$tot_ex_factory_qnty+=$ex_factory_qty; 
					$tot_ex_factory_val+=$ex_factory_value; 
					
					$dzn_qnty=0;
					$costing_per_id=$fabriccostArray[$row[csf('job_no')]]['costing_per_id'];
					if($costing_per_id==1)
					{
						$dzn_qnty=12;
					}
					else if($costing_per_id==3)
					{
						$dzn_qnty=12*2;
					}
					else if($costing_per_id==4)
					{
						$dzn_qnty=12*3;
					}
					else if($costing_per_id==5)
					{
						$dzn_qnty=12*4;
					}
					else
					{
						$dzn_qnty=1;
					}
					
					$dzn_qnty=$dzn_qnty*$row[csf('ratio')];
					$yarn_cost_mkt=($plan_cut_qnty/$dzn_qnty)*$yarncostArray[$row[csf('job_no')]];
					$knit_cost_mkt=($order_qnty_in_pcs/$dzn_qnty)*$prodcostArray[$row[csf('job_no')]]['knit_charge'];
					$dye_finish_cost_mkt=($order_qnty_in_pcs/$dzn_qnty)*$prodcostArray[$row[csf('job_no')]]['dye_finish_charge'];
					$yarn_dye_cost_mkt=($order_qnty_in_pcs/$dzn_qnty)*$prodcostArray[$row[csf('job_no')]]['yarn_dye_charge'];
					$aop_cost_mkt=($order_qnty_in_pcs/$dzn_qnty)*$prodcostArray[$row[csf('job_no')]]['aop_charge'];
					$trims_cost_mkt=($order_qnty_in_pcs/$dzn_qnty)*$trimsCostArray[$row[csf('id')]];
					
					$embell_cost_mkt=($order_qnty_in_pcs/$dzn_qnty)*$fabriccostArray[$row[csf('job_no')]]['embel_cost'];
					$wash_cost_mkt=($order_qnty_in_pcs/$dzn_qnty)*$fabriccostArray[$row[csf('job_no')]]['wash_cost'];
					$commission_cost_mkt=($order_qnty_in_pcs/$dzn_qnty)*$fabriccostArray[$row[csf('job_no')]]['commission'];
					$comm_cost_mkt=($order_qnty_in_pcs/$dzn_qnty)*$fabriccostArray[$row[csf('job_no')]]['comm_cost'];
					$freight_cost_mkt=($order_qnty_in_pcs/$dzn_qnty)*$fabriccostArray[$row[csf('job_no')]]['freight'];
					$test_cost_mkt=($order_qnty_in_pcs/$dzn_qnty)*$fabriccostArray[$row[csf('job_no')]]['lab_test'];
					$inspection_cost_mkt=($order_qnty_in_pcs/$dzn_qnty)*$fabriccostArray[$row[csf('job_no')]]['inspection'];
					$currier_cost_mkt=($order_qnty_in_pcs/$dzn_qnty)*$fabriccostArray[$row[csf('job_no')]]['currier_pre_cost'];
					$cm_cost_mkt=($order_qnty_in_pcs/$dzn_qnty)*$fabriccostArray[$row[csf('job_no')]]['cm_cost'];
					
					$mkt_all_cost=$yarn_cost_mkt+$knit_cost_mkt+$dye_finish_cost_mkt+$yarn_dye_cost_mkt+$aop_cost_mkt+$trims_cost_mkt+$embell_cost_mkt+$wash_cost_mkt+$commission_cost_mkt+$comm_cost_mkt+$freight_cost_mkt+$test_cost_mkt+$inspection_cost_mkt+$currier_cost_mkt+$cm_cost_mkt;
					
					$mkt_margin=$po_value-$mkt_all_cost;
					$mkt_margin_perc=($mkt_margin/$po_value)*100;
					
					//$yarn_cost_actual=($order_qnty_in_pcs/$dzn_qnty)*$yarncostArray[$row[csf('job_no')]];
					//$knit_cost_actual=($order_qnty_in_pcs/$dzn_qnty)*$prodcostArray[$row[csf('job_no')]]['knit_charge'];
					//$dye_finish_cost_actual=($order_qnty_in_pcs/$dzn_qnty)*$prodcostArray[$row[csf('job_no')]]['dye_finish_charge'];
					//$yarn_dye_cost_actual=($order_qnty_in_pcs/$dzn_qnty)*$prodcostArray[$row[csf('job_no')]]['yarn_dye_charge'];
					//$aop_cost_actual=($order_qnty_in_pcs/$dzn_qnty)*$prodcostArray[$row[csf('job_no')]]['aop_charge'];
					//$trims_cost_actual=($order_qnty_in_pcs/$dzn_qnty)*$trimsCostArray[$row[csf('id')]];
					
					$yarn_cost_actual=$yarnTrimsCostArray[$row[csf('id')]][1];
					//$trims_cost_actual=$yarnTrimsCostArray[$row[csf('id')]][4]/$exchange_rate;
					$trims_cost_actual=$actualTrimsCostArray[$row[csf('id')]];
					$yarn_dye_cost_actual=0;
					$aop_cost_actual=$aopCostArray[$row[csf('id')]];
					$embell_cost_actual=$embellCostArray[$row[csf('id')]];
					$wash_cost_actual=$washCostArray[$row[csf('id')]];
					$knit_cost_actual=$subconCostArray[$row[csf('id')]]['knit_bill']/$exchange_rate;
					$dye_finish_cost_actual=$subconCostArray[$row[csf('id')]]['dye_finish_bill']/$exchange_rate;
					$commission_cost_actual=($ex_factory_qty/$dzn_qnty)*$fabriccostArray[$row[csf('job_no')]]['commission'];
					$comm_cost_actual=$actualCostArray[6][$row[csf('id')]];
					$freight_cost_actual=$actualCostArray[2][$row[csf('id')]];
					$test_cost_actual=$actualCostArray[1][$row[csf('id')]];
					$inspection_cost_actual=$actualCostArray[3][$row[csf('id')]];
					$currier_cost_actual=$actualCostArray[4][$row[csf('id')]];
					$cm_cost_actual=$actualCostArray[5][$row[csf('id')]];
					
					$actual_all_cost=$yarn_cost_actual+$knit_cost_actual+$dye_finish_cost_actual+$yarn_dye_cost_actual+$aop_cost_actual+$trims_cost_actual+$embell_cost_actual+$wash_cost_actual+$commission_cost_actual+$comm_cost_actual+$freight_cost_actual+$test_cost_actual+$inspection_cost_actual+$currier_cost_actual+$cm_cost_actual;
					
					$actual_margin=$ex_factory_value-$actual_all_cost;
					$actual_margin_perc=($actual_margin/$ex_factory_value)*100;
					
					$tot_yarn_cost_mkt+=$yarn_cost_mkt; 
					$tot_knit_cost_mkt+=$knit_cost_mkt; 
					$tot_dye_finish_cost_mkt+=$dye_finish_cost_mkt; 
					$tot_yarn_dye_cost_mkt+=$yarn_dye_cost_mkt; 
					$tot_aop_cost_mkt+=$aop_cost_mkt; 
					$tot_trims_cost_mkt+=$trims_cost_mkt; 
					$tot_embell_cost_mkt+=$embell_cost_mkt; 
					$tot_wash_cost_mkt+=$wash_cost_mkt; 
					$tot_commission_cost_mkt+=$commission_cost_mkt; 
					$tot_comm_cost_mkt+=$comm_cost_mkt; 
					$tot_freight_cost_mkt+=$freight_cost_mkt; 
					$tot_test_cost_mkt+=$test_cost_mkt; 
					$tot_inspection_cost_mkt+=$inspection_cost_mkt; 
					$tot_currier_cost_mkt+=$currier_cost_mkt; 
					$tot_cm_cost_mkt+=$cm_cost_mkt; 
					$tot_mkt_all_cost+=$mkt_all_cost; 
					$tot_mkt_margin+=$mkt_margin; 
					$tot_yarn_cost_actual+=$yarn_cost_actual; 
					$tot_knit_cost_actual+=$knit_cost_actual; 
					$tot_dye_finish_cost_actual+=$dye_finish_cost_actual;
					$tot_yarn_dye_cost_actual+=$yarn_dye_cost_actual; 
					$tot_aop_cost_actual+=$aop_cost_actual; 
					$tot_trims_cost_actual+=$trims_cost_actual; 
					$tot_embell_cost_actual+=$embell_cost_actual;
					$tot_wash_cost_actual+=$wash_cost_actual; 
					$tot_commission_cost_actual+=$commission_cost_actual; 
					$tot_comm_cost_actual+=$comm_cost_actual; 
					$tot_freight_cost_actual+=$freight_cost_actual; 
					$tot_test_cost_actual+=$test_cost_actual; 
					$tot_inspection_cost_actual+=$inspection_cost_actual; 
					$tot_currier_cost_actual+=$currier_cost_actual; 
					$tot_cm_cost_actual+=$cm_cost_actual; 
					$tot_actual_all_cost+=$actual_all_cost; 
					$tot_actual_margin+=$actual_margin;
					
					$buyer_name_array[$row[csf('buyer_name')]]=$buyer_arr[$row[csf('buyer_name')]];
					$mkt_po_val_array[$row[csf('buyer_name')]]+=$po_value;
					$mkt_yarn_array[$row[csf('buyer_name')]]+=$yarn_cost_mkt+$yarn_dye_cost_mkt;
					$mkt_knit_array[$row[csf('buyer_name')]]+=$knit_cost_mkt;
					$mkt_dy_fin_array[$row[csf('buyer_name')]]+=$dye_finish_cost_mkt;
					//$mkt_yarn_dy_array[$row[csf('buyer_name')]]+=$yarn_dye_cost_mkt;
					$mkt_aop_array[$row[csf('buyer_name')]]+=$aop_cost_mkt;
					$mkt_trims_array[$row[csf('buyer_name')]]+=$trims_cost_mkt;
					$mkt_emb_array[$row[csf('buyer_name')]]+=$embell_cost_mkt;
					$mkt_wash_array[$row[csf('buyer_name')]]+=$wash_cost_mkt;
					$mkt_commn_array[$row[csf('buyer_name')]]+=$commission_cost_mkt;
					$mkt_commercial_array[$row[csf('buyer_name')]]+=$comm_cost_mkt;
					$mkt_freight_array[$row[csf('buyer_name')]]+=$freight_cost_mkt;
					$mkt_test_array[$row[csf('buyer_name')]]+=$test_cost_mkt;
					$mkt_ins_array[$row[csf('buyer_name')]]+=$inspection_cost_mkt;
					$mkt_courier_array[$row[csf('buyer_name')]]+=$currier_cost_mkt;
					$mkt_cm_array[$row[csf('buyer_name')]]+=$cm_cost_mkt;
					$mkt_total_array[$row[csf('buyer_name')]]+=$mkt_all_cost;
					$mkt_margin_array[$row[csf('buyer_name')]]+=$mkt_margin;
					
					$ex_factory_val_array[$row[csf('buyer_name')]]+=$ex_factory_value;
					$yarn_cost_array[$row[csf('buyer_name')]]+=$yarn_cost_actual;
					$knit_cost_array[$row[csf('buyer_name')]]+=$knit_cost_actual;
					$dye_cost_array[$row[csf('buyer_name')]]+=$dye_finish_cost_actual;
					//$yarn_dyeing_cost_array[$row[csf('buyer_name')]]+=$yarn_dye_cost_actual;
					$aop_n_others_cost_array[$row[csf('buyer_name')]]+=$aop_cost_actual;
					$trims_cost_array[$row[csf('buyer_name')]]+=$trims_cost_actual;
					$enbellishment_cost_array[$row[csf('buyer_name')]]+=$embell_cost_actual;
					$wash_cost_array[$row[csf('buyer_name')]]+=$wash_cost_actual;
					$commission_cost_array[$row[csf('buyer_name')]]+=$commission_cost_actual;
					$commercial_cost_array[$row[csf('buyer_name')]]+=$comm_cost_actual;
					$freight_cost_array[$row[csf('buyer_name')]]+=$freight_cost_actual;
					$testing_cost_array[$row[csf('buyer_name')]]+=$test_cost_actual;
					$inspection_cost_array[$row[csf('buyer_name')]]+=$inspection_cost_actual;
					$courier_cost_array[$row[csf('buyer_name')]]+=$currier_cost_actual;
					$cm_cost_array[$row[csf('buyer_name')]]+=$cm_cost_actual;
					$total_cost_array[$row[csf('buyer_name')]]+=$actual_all_cost;
					$margin_array[$row[csf('buyer_name')]]+=$actual_margin;
					
				?>
                	<tr bgcolor="<?php echo $bgcolor;?>" onClick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor;?>')" id="tr_<?php echo $i; ?>">
                        <td width="40" rowspan="4"><?php echo $i; ?></td>
                        <td width="70" rowspan="4"><?php echo $buyer_arr[$row[csf('buyer_name')]]; ?></td>
                        <td width="60" align="center" rowspan="4"><?php echo $row[csf('year')]; ?></td>
                        <td width="70" align="center" rowspan="4"><?php echo $row[csf('job_no_prefix_num')]; ?></td>
                        <td width="100" rowspan="4" style="word-break:break-all;"><?php echo $row[csf('po_number')]; ?></td>
                        <td width="110" rowspan="4" style="word-break:break-all;"><?php echo $row[csf('style_ref_no')]; ?></td>
                        <td width="120" rowspan="4" style="word-break:break-all;"><?php echo $gmts_item; ?></td>
                        <td width="88" align="right" style="padding-right:2px" rowspan="4"><?php echo $order_qnty_in_pcs; ?></td>
                        <td width="50" align="center" rowspan="4"><?php echo $unit_of_measurement[$row[csf('order_uom')]]; ?></td>
                        <td width="68" align="right" style="padding-right:2px" rowspan="4"><?php echo number_format($unit_price,2,'.',''); ?></td>
                        <td width="108" align="right" style="padding-right:2px" rowspan="4"><?php echo number_format($po_value,2,'.',''); ?></td>
                        <td width="80" align="center" rowspan="4"><?php echo change_date_format($row[csf('pub_shipment_date')]); ?></td>
                        <td width="100" rowspan="4" style="word-break:break-all;"><?php echo $shipment_status[$row[csf('shiping_status')]]; ?></td>
                        <td width="100" align="left"><b>Pre Costing</b></td>
                        <td width="98" align="right" style="padding-right:2px"><?php echo number_format($order_qnty_in_pcs,0,'.',''); ?></td>
                        <td width="108" align="right" style="padding-right:2px"><?php echo number_format($po_value,2,'.',''); ?></td>
                        <td width="108" style="padding-right:2px" align="right"><?php echo number_format($yarn_cost_mkt+$yarn_dye_cost_mkt,2,'.',''); ?></td>
                        <td width="108" style="padding-right:2px" align="right"><?php echo number_format($knit_cost_mkt,2,'.',''); ?></td>
                        <td width="108" style="padding-right:2px" align="right"><?php echo number_format($dye_finish_cost_mkt,2,'.',''); ?></td>
                        <!--<td width="108" style="padding-right:2px" align="right"><?phpecho number_format($yarn_dye_cost_mkt,2,'.',''); ?></td>-->
                        <td width="108" style="padding-right:2px" align="right"><?php echo number_format($aop_cost_mkt,2,'.',''); ?></td>
                        <td width="108" style="padding-right:2px" align="right"><?php echo number_format($trims_cost_mkt,2,'.',''); ?></td>
                        <td width="98" style="padding-right:2px" align="right"><?php echo number_format($embell_cost_mkt,2,'.',''); ?></td>
                        <td width="98" style="padding-right:2px" align="right"><?php echo number_format($wash_cost_mkt,2,'.',''); ?></td>
                        <td width="98" style="padding-right:2px" align="right"><?php echo number_format($commission_cost_mkt,2,'.',''); ?></td>
                        <td width="98" style="padding-right:2px" align="right"><?php echo number_format($comm_cost_mkt,2,'.',''); ?></td>
                        <td width="98" style="padding-right:2px" align="right"><?php echo number_format($freight_cost_mkt,2,'.',''); ?></td>
                        <td width="98" style="padding-right:2px" align="right"><?php echo number_format($test_cost_mkt,2,'.',''); ?></td>
                        <td width="98" style="padding-right:2px" align="right"><?php echo number_format($inspection_cost_mkt,2,'.',''); ?></td>
                        <td width="98" style="padding-right:2px" align="right"><?php echo number_format($currier_cost_mkt,2,'.',''); ?></td>
                        <td width="98" style="padding-right:2px" align="right"><?php echo number_format($cm_cost_mkt,2,'.',''); ?></td>
                        <td width="98" style="padding-right:2px" align="right"><?php echo number_format($mkt_all_cost,2,'.',''); ?></td>
                        <td width="98" style="padding-right:2px" align="right"><?php echo number_format($mkt_margin,2,'.',''); ?></td>
                        <td style="padding-right:2px" align="right"><?php echo number_format($mkt_margin_perc,2,'.',''); ?></td>
                    </tr>
                    <tr bgcolor="#F5F5F5" onClick="change_color('tr_a<?php echo $i; ?>','#F5F5F5')" id="tr_a<?php echo $i; ?>">
                    	<td width="100" align="left"><b>Actual</b></td>
                        <td width="98" align="right" style="padding-right:2px"><?php echo number_format($ex_factory_qty,0,'.',''); ?></td>
                        <td width="108" align="right" style="padding-right:2px"><?php echo number_format($ex_factory_value,2,'.',''); ?></td>
                        <td width="108" style="padding-right:2px" align="right"><?php echo number_format($yarn_cost_actual,2,'.',''); ?></td>
                        <td width="108" style="padding-right:2px" align="right"><?php echo number_format($knit_cost_actual,2,'.',''); ?></td>
                        <td width="108" style="padding-right:2px" align="right"><?php echo number_format($dye_finish_cost_actual,2,'.',''); ?></td>
                        <!--<td width="108" style="padding-right:2px" align="right"><?phpecho number_format($yarn_dye_cost_actual,2,'.',''); ?></td>-->
                        <td width="108" style="padding-right:2px" align="right"><?php echo number_format($aop_cost_actual,2,'.',''); ?></td>
                        <td width="108" style="padding-right:2px" align="right"><?php echo number_format($trims_cost_actual,2,'.',''); ?></td>
                        <td width="98" style="padding-right:2px" align="right"><?php echo number_format($embell_cost_actual,2,'.',''); ?></td>
                        <td width="98" style="padding-right:2px" align="right"><?php echo number_format($wash_cost_actual,2,'.',''); ?></td>
                        <td width="98" style="padding-right:2px" align="right"><?php echo number_format($commission_cost_actual,2,'.',''); ?></td>
                        <td width="98" style="padding-right:2px" align="right"><?php echo number_format($comm_cost_actual,2,'.',''); ?></td>
                        <td width="98" style="padding-right:2px" align="right"><?php echo number_format($freight_cost_actual,2,'.',''); ?></td>
                        <td width="98" style="padding-right:2px" align="right"><?php echo number_format($test_cost_actual,2,'.',''); ?></td>
                        <td width="98" style="padding-right:2px" align="right"><?php echo number_format($inspection_cost_actual,2,'.',''); ?></td>
                        <td width="98" style="padding-right:2px" align="right"><?php echo number_format($currier_cost_actual,2,'.',''); ?></td>
                        <td width="98" style="padding-right:2px" align="right"><?php echo number_format($cm_cost_actual,2,'.',''); ?></td>
                        <td width="98" style="padding-right:2px" align="right"><?php echo number_format($actual_all_cost,2,'.',''); ?></td>
                        <td width="98" style="padding-right:2px" align="right"><?php echo number_format($actual_margin,2,'.',''); ?></td>
                        <td style="padding-right:2px" align="right"><?php echo number_format($actual_margin_perc,2,'.',''); ?></td>
                    </tr>
                    <tr bgcolor="#E9E9E9" onClick="change_color('tr_v<?php echo $i; ?>','#E9E9E9')" id="tr_v<?php echo $i; ?>">
                    	<td width="100" align="left"><b>Variance</b></td>
                    	<td width="98" align="right" style="padding-right:2px">
							<?php 
								$variance_qnty=$order_qnty_in_pcs-$ex_factory_qty;
								$variance_qnty_per=($variance_qnty/$order_qnty_in_pcs)*100; 
								echo number_format($variance_qnty,0,'.',''); 
							?>
                        </td>
                    	<td width="108" align="right" style="padding-right:2px">
                        	<?php 
								$variance_po_value=$po_value-$ex_factory_value;
								$variance_po_value_per=($variance_po_value/$po_value)*100; 
								echo number_format($variance_po_value,2,'.',''); 
							?>
                        </td>
                        <td width="108" style="padding-right:2px" align="right">
                        	<?php 
								$variance_yarn_cost=($yarn_cost_mkt+$yarn_dye_cost_mkt)-$yarn_cost_actual;
								$variance_yarn_cost_per=($variance_yarn_cost/($yarn_cost_mkt+$yarn_dye_cost_mkt))*100; 
								echo number_format($variance_yarn_cost,2,'.',''); 
							?>
                        </td>
                        <td width="108" style="padding-right:2px" align="right">
                        	<?php 
								$variance_kint_cost=$knit_cost_mkt-$knit_cost_actual;
								$variance_knit_cost_per=($variance_kint_cost/$knit_cost_mkt)*100; 
								echo number_format($variance_kint_cost,2,'.',''); 
							?>
                        </td>
                        <td width="108" style="padding-right:2px" align="right">
                        	<?php 
								$variance_dye_finish_cost=$dye_finish_cost_mkt-$dye_finish_cost_actual;
								$variance_dye_finish_cost_per=($variance_dye_finish_cost/$dye_finish_cost_mkt)*100; 
								echo number_format($variance_dye_finish_cost,2,'.','');
							?>
                        </td>
                        <!--<td width="108" style="padding-right:2px" align="right">
                        	<?php$variance_yarn_dye_cost=$yarn_dye_cost_mkt-$yarn_dye_cost_actual;
								$variance_yarn_dye_cost_per=($variance_yarn_dye_cost/$yarn_dye_cost_mkt)*100; 
								echo number_format($variance_yarn_dye_cost,2,'.',''); 
							?>
                        </td>-->
                        <td width="108" style="padding-right:2px" align="right">
                        	<?php 
								$variance_aop_cost=$aop_cost_mkt-$aop_cost_actual;
								$variance_aop_cost_per=($variance_aop_cost/$aop_cost_mkt)*100; 
								echo number_format($variance_aop_cost,2,'.',''); 
							?>
                        </td>
                        <td width="108" style="padding-right:2px" align="right">
                        	<?php 
								$variance_trims_cost=$trims_cost_mkt-$trims_cost_actual;
								$variance_trims_cost_per=($variance_trims_cost/$trims_cost_mkt)*100; 
								echo number_format($variance_trims_cost,2,'.','');
							?>
                        </td>
                        <td width="98" style="padding-right:2px" align="right">
                        	<?php 
								$variance_embell_cost=$embell_cost_mkt-$embell_cost_actual;
								$variance_embell_cost_per=($variance_embell_cost/$embell_cost_mkt)*100; 
								echo number_format($variance_embell_cost,2,'.',''); 
							?>
                        </td>
                        <td width="98" style="padding-right:2px" align="right">
                        	<?php 
								$variance_wash_cost=$wash_cost_mkt-$wash_cost_actual;
								$variance_wash_cost_per=($variance_wash_cost/$wash_cost_mkt)*100; 
								echo number_format($variance_wash_cost,2,'.',''); 
							?>
                        </td>
                        <td width="98" style="padding-right:2px" align="right">
                        	<?php 
								$variance_commission_cost=$commission_cost_mkt-$commission_cost_actual;
								$variance_commission_cost_per=($variance_commission_cost/$commission_cost_mkt)*100; 
								echo number_format($variance_commission_cost,2,'.',''); 
							?>
                        </td>
                        <td width="98" style="padding-right:2px" align="right">
                        	<?php 
								$variance_comm_cost=$comm_cost_mkt-$comm_cost_actual;
								$variance_comm_cost_per=($variance_comm_cost/$comm_cost_mkt)*100; 
								echo number_format($variance_comm_cost,2,'.',''); 
							?>
                        </td>
                        <td width="98" style="padding-right:2px" align="right">
                        	<?php 
								$variance_freight_cost=$freight_cost_mkt-$freight_cost_actual;
								$variance_freight_cost_per=($variance_freight_cost/$freight_cost_mkt)*100; 
								echo number_format($variance_freight_cost,2,'.',''); 
							?>
                        </td>
                        <td width="98" style="padding-right:2px" align="right">
                        	<?php 
								$variance_test_cost=$test_cost_mkt-$test_cost_actual;
								$variance_test_cost_per=($variance_test_cost/$test_cost_mkt)*100; 
								echo number_format($variance_test_cost,2,'.','');
							?>
                        </td>
                        <td width="98" style="padding-right:2px" align="right">
                        	<?php 
								$variance_inspection_cost=$inspection_cost_mkt-$inspection_cost_actual;
								$variance_inspection_cost_per=($variance_inspection_cost/$inspection_cost_mkt)*100; 
								echo number_format($variance_inspection_cost,2,'.','');
							?>
                        </td>
                        <td width="98" style="padding-right:2px" align="right">
                        	<?php 
								$variance_currier_cost=$currier_cost_mkt-$currier_cost_actual;
								$variance_currier_cost_per=($variance_currier_cost/$currier_cost_mkt)*100; 
								echo number_format($variance_currier_cost,2,'.','');
							?>
                        </td>
                        <td width="98" style="padding-right:2px" align="right">
                        	<?php 
								$variance_cm_cost=$cm_cost_mkt-$cm_cost_actual;
								$variance_cm_cost_per=($variance_cm_cost/$cm_cost_mkt)*100; 
								echo number_format($variance_cm_cost,2,'.',''); 
							?>
                        </td>
                        <td width="98" style="padding-right:2px" align="right">
                        	<?php 
								$variance_all_cost=$mkt_all_cost-$actual_all_cost;
								$variance_all_cost_per=($variance_all_cost/$mkt_all_cost)*100; 
								echo number_format($variance_all_cost,2,'.','');
							?>
                        </td>
                        <td width="98" style="padding-right:2px" align="right">
                        	<?php 
								$variance_margin_cost=$mkt_margin-$actual_margin;
								$variance_margin_cost_per=($variance_margin_cost/$mkt_margin)*100; 
								echo number_format($variance_margin_cost,2,'.','');
							?>
                        </td>
                        <td style="padding-right:2px" align="right">
                        	<?php 
								$variance_per_cost=$mkt_margin_perc-$actual_margin_perc;
								$variance_per_cost_per=($variance_per_cost/$mkt_margin_perc)*100; 
								echo number_format($variance_per_cost,2,'.','');
							?>
                        </td>
                    </tr>
                    <tr bgcolor="#DFDFDF" onClick="change_color('tr_vp<?php echo $i; ?>','#DFDFDF')" id="tr_vp<?php echo $i; ?>">
                    	<td width="100" align="left"><b>Variance (%)</b></td>
                    	<td width="98" align="right" style="padding-right:2px"><?php echo number_format($variance_qnty_per,2,'.',''); ?></td>
                    	<td width="108" align="right" style="padding-right:2px"><?php echo number_format($variance_po_value_per,2,'.',''); ?></td>
                        <td width="108" style="padding-right:2px" align="right"><?php echo number_format($variance_yarn_cost_per,2,'.',''); ?></td>
                        <td width="108" style="padding-right:2px" align="right"><?php echo number_format($variance_knit_cost_per,2,'.',''); ?></td>
                        <td width="108" style="padding-right:2px" align="right"><?php echo number_format($variance_dye_finish_cost_per,2,'.',''); ?></td>
                        <!--<td width="108" style="padding-right:2px" align="right"><?phpecho number_format($variance_yarn_dye_cost_per,2,'.',''); ?></td>-->
                        <td width="108" style="padding-right:2px" align="right"><?php echo number_format($variance_aop_cost_per,2,'.',''); ?></td>
                        <td width="108" style="padding-right:2px" align="right"><?php echo number_format($variance_trims_cost_per,2,'.',''); ?></td>
                        <td width="98" style="padding-right:2px" align="right"><?php echo number_format($variance_embell_cost_per,2,'.',''); ?></td>
                        <td width="98" style="padding-right:2px" align="right"><?php echo number_format($variance_wash_cost_per,2,'.',''); ?></td>
                        <td width="98" style="padding-right:2px" align="right"><?php echo number_format($variance_commission_cost_per,2,'.',''); ?></td>
                        <td width="98" style="padding-right:2px" align="right"><?php echo number_format($variance_comm_cost_per,2,'.',''); ?></td>
                        <td width="98" style="padding-right:2px" align="right"><?php echo number_format($variance_freight_cost_per,2,'.',''); ?></td>
                        <td width="98" style="padding-right:2px" align="right"><?php echo number_format($variance_test_cost_per,2,'.',''); ?></td>
                        <td width="98" style="padding-right:2px" align="right"><?php echo number_format($variance_inspection_cost_per,2,'.',''); ?></td>
                        <td width="98" style="padding-right:2px" align="right"><?php echo number_format($variance_currier_cost_per,2,'.',''); ?></td>
                        <td width="98" style="padding-right:2px" align="right"><?php echo number_format($variance_cm_cost_per,2,'.',''); ?></td>
                        <td width="98" style="padding-right:2px" align="right"><?php echo number_format($variance_all_cost_per,2,'.',''); ?></td>
                        <td width="98" style="padding-right:2px" align="right"><?php echo number_format($variance_margin_cost_per,2,'.',''); ?></td>
                        <td style="padding-right:2px" align="right"><?php echo number_format($variance_per_cost_per,2,'.',''); ?></td>
                    </tr>
				<?php
					$i++;
				}
				
				$tot_mkt_margin_perc=($tot_mkt_margin/$tot_po_value)*100;
				$tot_actual_margin_perc=($tot_actual_margin/$tot_ex_factory_val)*100;
				
				?>
            </table>
		</div>
        <table class="rpt_table" width="3170" cellpadding="0" cellspacing="0" border="1" rules="all">
            <tr bgcolor="#CCDDEE">
                <td colspan="7" style="padding-right:4px" align="right">Pre Costing Total</td>
                <td width="88" style="padding-right:2px" align="right"><?php echo number_format($tot_po_qnty,0,'.',''); ?></td>
                <td width="50">&nbsp;</td>
                <td width="70">&nbsp;</td>
                <td width="108" style="padding-right:2px" align="right"><?php echo number_format($tot_po_value,2,'.',''); ?></td>
                <td width="80">&nbsp;</td>
                <td width="100">&nbsp;</td>
                <td width="100">&nbsp;</td>
                <td width="98" style="padding-right:2px" align="right"><?php echo number_format($tot_po_qnty,0,'.',''); ?></td>
                <td width="108" style="padding-right:2px" align="right"><?php echo number_format($tot_po_value,2,'.',''); ?></td>
                <td width="108" style="padding-right:2px" align="right"><?php echo number_format($tot_yarn_cost_mkt+$tot_yarn_dye_cost_mkt,2,'.',''); ?></td>
                <td width="108" style="padding-right:2px" align="right"><?php echo number_format($tot_knit_cost_mkt,2,'.',''); ?></td>
                <td width="108" style="padding-right:2px" align="right"><?php echo number_format($tot_dye_finish_cost_mkt,2,'.',''); ?></td>
                <!--<td width="108" style="padding-right:2px" align="right"><?phpecho number_format($tot_yarn_dye_cost_mkt,2,'.',''); ?></td>-->
                <td width="108" style="padding-right:2px" align="right"><?php echo number_format($tot_aop_cost_mkt,2,'.',''); ?></td>
                <td width="108" style="padding-right:2px" align="right"><?php echo number_format($tot_trims_cost_mkt,2,'.',''); ?></td>
                <td width="98" style="padding-right:2px" align="right"><?php echo number_format($tot_embell_cost_mkt,2,'.',''); ?></td>
                <td width="98" style="padding-right:2px" align="right"><?php echo number_format($tot_wash_cost_mkt,2,'.',''); ?></td>
                <td width="98" style="padding-right:2px" align="right"><?php echo number_format($tot_commission_cost_mkt,2,'.',''); ?></td>
                <td width="98" style="padding-right:2px" align="right"><?php echo number_format($tot_comm_cost_mkt,2,'.',''); ?></td>
                <td width="98" style="padding-right:2px" align="right"><?php echo number_format($tot_freight_cost_mkt,2,'.',''); ?></td>
                <td width="98" style="padding-right:2px" align="right"><?php echo number_format($tot_test_cost_mkt,2,'.',''); ?></td>
                <td width="98" style="padding-right:2px" align="right"><?php echo number_format($tot_inspection_cost_mkt,2,'.',''); ?></td>
                <td width="98" style="padding-right:2px" align="right"><?php echo number_format($tot_currier_cost_mkt,2,'.',''); ?></td>
                <td width="98" style="padding-right:2px" align="right"><?php echo number_format($tot_cm_cost_mkt,2,'.',''); ?></td>
                <td width="98" style="padding-right:2px" align="right"><?php echo number_format($tot_mkt_all_cost,2,'.',''); ?></td>
                <td width="98" style="padding-right:2px" align="right"><?php echo number_format($tot_mkt_margin,2,'.',''); ?></td>
                <td width="93" style="padding-right:2px" align="right"><?php echo number_format($tot_mkt_margin_perc,2,'.',''); ?></td>
            </tr>
            <tr bgcolor="#CCCCFF">
                <td colspan="7" style="padding-right:4px" align="right">Actual Total</td>
                <td width="88" style="padding-right:2px" align="right">&nbsp;</td>
                <td width="50">&nbsp;</td>
                <td width="70">&nbsp;</td>
                <td width="108" style="padding-right:2px" align="right">&nbsp;</td>
                <td width="80">&nbsp;</td>
                <td width="100">&nbsp;</td>
                <td width="100">&nbsp;</td>
                <td width="98" align="right" style="padding-right:2px"><?php echo number_format($tot_ex_factory_qnty,0,'.',''); ?></td>
                <td width="108" align="right" style="padding-right:2px"><?php echo number_format($tot_ex_factory_val,2,'.',''); ?></td>
                <td width="108" style="padding-right:2px" align="right"><?php echo number_format($tot_yarn_cost_actual,2,'.',''); ?></td>
                <td width="108" style="padding-right:2px" align="right"><?php echo number_format($tot_knit_cost_actual,2,'.',''); ?></td>
                <td width="108" style="padding-right:2px" align="right"><?php echo number_format($tot_dye_finish_cost_actual,2,'.',''); ?></td>
                <!--<td width="108" style="padding-right:2px" align="right"><?phpecho number_format($tot_yarn_dye_cost_actual,2,'.',''); ?></td>-->
                <td width="108" style="padding-right:2px" align="right"><?php echo number_format($tot_aop_cost_actual,2,'.',''); ?></td>
                <td width="108" align="right" style="padding-right:2px"><?php echo number_format($tot_trims_cost_actual,2,'.',''); ?></td>
                <td width="98" style="padding-right:2px" align="right"><?php echo number_format($tot_embell_cost_actual,2,'.',''); ?></td>
                <td width="98" style="padding-right:2px" align="right"><?php echo number_format($tot_wash_cost_actual,2,'.',''); ?></td>
                <td width="98" style="padding-right:2px" align="right"><?php echo number_format($tot_commission_cost_actual,2,'.',''); ?></td>
                <td width="98" style="padding-right:2px" align="right"><?php echo number_format($tot_comm_cost_actual,2,'.',''); ?></td>
                <td width="98" style="padding-right:2px" align="right"><?php echo number_format($tot_freight_cost_actual,2,'.',''); ?></td>
                <td width="98" style="padding-right:2px" align="right"><?php echo number_format($tot_test_cost_actual,2,'.',''); ?></td>
                <td width="98" style="padding-right:2px" align="right"><?php echo number_format($tot_inspection_cost_actual,2,'.',''); ?></td>
                <td width="98" style="padding-right:2px" align="right"><?php echo number_format($tot_currier_cost_actual,2,'.',''); ?></td>
                <td width="98" style="padding-right:2px" align="right"><?php echo number_format($tot_cm_cost_actual,2,'.',''); ?></td>
                <td width="98" style="padding-right:2px" align="right"><?php echo number_format($tot_actual_all_cost,2,'.',''); ?></td>
                <td width="98" style="padding-right:2px" align="right"><?php echo number_format($tot_actual_margin,2,'.',''); ?></td>
                <td width="93" style="padding-right:2px" align="right"><?php echo number_format($tot_actual_margin_perc,2,'.',''); ?></td>
            </tr>
            <tr bgcolor="#FFEEFF">
                <td colspan="7" style="padding-right:4px" align="right">Variance Total</td>
                <td width="88" style="padding-right:2px" align="right">&nbsp;</td>
                <td width="50">&nbsp;</td>
                <td width="70">&nbsp;</td>
                <td width="108" style="padding-right:2px" align="right">&nbsp;</td>
                <td width="80">&nbsp;</td>
                <td width="100">&nbsp;</td>
                <td width="100">&nbsp;</td>
                <td width="98" align="right" style="padding-right:2px">
					<?php 
                        $tot_variance_qnty=$tot_po_qnty-$tot_ex_factory_qnty;
                        $tot_variance_qnty_per=($tot_variance_qnty/$tot_po_qnty)*100; 
                        echo number_format($tot_variance_qnty,0,'.',''); 
                    ?>
                </td>
                <td width="108" align="right" style="padding-right:2px">
                    <?php 
                        $tot_variance_po_value=$tot_po_value-$tot_ex_factory_val;
                        $tot_variance_po_value_per=($tot_variance_po_value/$tot_po_value)*100; 
                        echo number_format($tot_variance_po_value,2,'.',''); 
                    ?>
                </td>
                <td width="108" style="padding-right:2px" align="right">
                    <?php 
                        $tot_variance_yarn_cost=($tot_yarn_cost_mkt+$tot_yarn_dye_cost_mkt)-$tot_yarn_cost_actual;
                        $tot_variance_yarn_cost_per=($tot_variance_yarn_cost/($tot_yarn_cost_mkt+$tot_yarn_dye_cost_mkt))*100; 
                        echo number_format($tot_variance_yarn_cost,2,'.',''); 
                    ?>
                </td>
                <td width="108" style="padding-right:2px" align="right">
                    <?php 
                        $tot_variance_kint_cost=$tot_knit_cost_mkt-$tot_knit_cost_actual;
                        $tot_variance_knit_cost_per=($tot_variance_kint_cost/$tot_knit_cost_mkt)*100; 
                        echo number_format($tot_variance_kint_cost,2,'.',''); 
                    ?>
                </td>
                <td width="108" style="padding-right:2px" align="right">
                    <?php 
                        $tot_variance_dye_finish_cost=$dye_finish_cost_mkt-$dye_finish_cost_actual;
                        $tot_variance_dye_finish_cost_per=($tot_variance_dye_finish_cost/$dye_finish_cost_mkt)*100; 
                        echo number_format($tot_variance_dye_finish_cost,2,'.','');
                    ?>
                </td>
                <!--<td width="108" style="padding-right:2px" align="right">
                    <?php//$tot_variance_yarn_dye_cost=$tot_yarn_dye_cost_mkt-$tot_yarn_dye_cost_actual;
                       // $tot_variance_yarn_dye_cost_per=($tot_variance_yarn_dye_cost/$tot_yarn_dye_cost_mkt)*100; 
                       // echo number_format($tot_variance_yarn_dye_cost,2,'.',''); 
                    ?>
                </td>-->
                <td width="108" style="padding-right:2px" align="right">
                    <?php 
                        $tot_variance_aop_cost=$tot_aop_cost_mkt-$tot_aop_cost_actual;
                        $tot_variance_aop_cost_per=($tot_variance_aop_cost/$tot_aop_cost_mkt)*100; 
                        echo number_format($tot_variance_aop_cost,2,'.',''); 
                    ?>
                </td>
                <td width="108" style="padding-right:2px" align="right">
                    <?php 
                        $tot_variance_trims_cost=$tot_trims_cost_mkt-$tot_trims_cost_actual;
                        $tot_variance_trims_cost_per=($tot_variance_trims_cost/$tot_trims_cost_mkt)*100; 
                        echo number_format($tot_variance_trims_cost,2,'.','');
                    ?>
                </td>
                <td width="98" style="padding-right:2px" align="right">
                    <?php 
                        $tot_variance_embell_cost=$tot_embell_cost_mkt-$tot_embell_cost_actual;
                        $tot_variance_embell_cost_per=($tot_variance_embell_cost/$tot_embell_cost_mkt)*100; 
                        echo number_format($tot_variance_embell_cost,2,'.',''); 
                    ?>
                </td>
                <td width="98" style="padding-right:2px" align="right">
                    <?php 
                        $tot_variance_wash_cost=$tot_wash_cost_mkt-$tot_wash_cost_actual;
                        $tot_variance_wash_cost_per=($tot_variance_wash_cost/$tot_wash_cost_mkt)*100; 
                        echo number_format($tot_variance_wash_cost,2,'.',''); 
                    ?>
                </td>
                <td width="98" style="padding-right:2px" align="right">
                    <?php 
                        $tot_variance_commission_cost=$tot_commission_cost_mkt-$tot_commission_cost_actual;
                        $tot_variance_commission_cost_per=($tot_variance_commission_cost/$tot_commission_cost_mkt)*100; 
                        echo number_format($tot_variance_commission_cost,2,'.',''); 
                    ?>
                </td>
                <td width="98" style="padding-right:2px" align="right">
                    <?php 
                        $tot_variance_comm_cost=$tot_comm_cost_mkt-$tot_comm_cost_actual;
                        $tot_variance_comm_cost_per=($tot_variance_comm_cost/$tot_comm_cost_mkt)*100; 
                        echo number_format($tot_variance_comm_cost,2,'.',''); 
                    ?>
                </td>
                <td width="98" style="padding-right:2px" align="right">
                    <?php 
                        $tot_variance_freight_cost=$tot_freight_cost_mkt-$tot_freight_cost_actual;
                        $tot_variance_freight_cost_per=($tot_variance_freight_cost/$tot_freight_cost_mkt)*100; 
                        echo number_format($tot_variance_freight_cost,2,'.',''); 
                    ?>
                </td>
                <td width="98" style="padding-right:2px" align="right">
                    <?php 
                        $tot_variance_test_cost=$tot_test_cost_mkt-$tot_test_cost_actual;
                        $tot_variance_test_cost_per=($tot_variance_test_cost/$tot_test_cost_mkt)*100; 
                        echo number_format($tot_variance_test_cost,2,'.','');
                    ?>
                </td>
                <td width="98" style="padding-right:2px" align="right">
                    <?php 
                        $tot_variance_inspection_cost=$tot_inspection_cost_mkt-$tot_inspection_cost_actual;
                        $tot_variance_inspection_cost_per=($tot_variance_inspection_cost/$tot_inspection_cost_mkt)*100; 
                        echo number_format($tot_variance_inspection_cost,2,'.','');
                    ?>
                </td>
                <td width="98" style="padding-right:2px" align="right">
                    <?php 
                        $tot_variance_currier_cost=$tot_currier_cost_mkt-$tot_currier_cost_actual;
                        $tot_variance_currier_cost_per=($tot_variance_currier_cost/$tot_currier_cost_mkt)*100; 
                        echo number_format($tot_variance_currier_cost,2,'.','');
                    ?>
                </td>
                <td width="98" style="padding-right:2px" align="right">
                    <?php 
                        $tot_variance_cm_cost=$tot_cm_cost_mkt-$tot_cm_cost_actual;
                        $tot_variance_cm_cost_per=($tot_variance_cm_cost/$tot_cm_cost_mkt)*100; 
                        echo number_format($tot_variance_cm_cost,2,'.',''); 
                    ?>
                </td>
                <td width="98" style="padding-right:2px" align="right">
                    <?php 
                        $tot_variance_all_cost=$tot_mkt_all_cost-$tot_actual_all_cost;
                        $tot_variance_all_cost_per=($tot_variance_all_cost/$tot_mkt_all_cost)*100; 
                        echo number_format($variance_all_cost,2,'.','');
                    ?>
                </td>
                <td width="98" style="padding-right:2px" align="right">
                    <?php 
                        $tot_variance_margin_cost=$tot_mkt_margin-$tot_actual_margin;
                        $tot_variance_margin_cost_per=($tot_variance_margin_cost/$tot_mkt_margin)*100; 
                        echo number_format($tot_variance_margin_cost,2,'.','');
                    ?>
                </td>
                <td style="padding-right:2px" align="right">
                    <?php 
                        $tot_variance_per_cost=$tot_mkt_margin_perc-$tot_actual_margin_perc;
                        $tot_variance_per_cost_per=($tot_variance_per_cost/$tot_mkt_margin_perc)*100; 
                        echo number_format($tot_variance_per_cost,2,'.','');
                    ?>
                </td>
            </tr>
            <tr bgcolor="#CCCCEE">
                <td colspan="7" style="padding-right:4px" align="right">Variance (%) Total</td>
                <td width="88" style="padding-right:2px" align="right">&nbsp;</td>
                <td width="50">&nbsp;</td>
                <td width="70">&nbsp;</td>
                <td width="108" style="padding-right:2px" align="right">&nbsp;</td>
                <td width="80">&nbsp;</td>
                <td width="100">&nbsp;</td>
                <td width="100">&nbsp;</td>
                <td width="98" align="right" style="padding-right:2px"><?php echo number_format($tot_variance_qnty_per,0,'.',''); ?></td>
                <td width="108" align="right" style="padding-right:2px"><?php echo number_format($tot_variance_po_value_per,2,'.',''); ?></td>
                <td width="108" style="padding-right:2px" align="right"><?php echo number_format($tot_variance_yarn_cost_per,2,'.',''); ?></td>
                <td width="108" style="padding-right:2px" align="right"><?php echo number_format($tot_variance_knit_cost_per,2,'.',''); ?></td>
                <td width="108" style="padding-right:2px" align="right"><?php echo number_format($tot_variance_dye_finish_cost_per ,2,'.',''); ?></td>
                <!--<td width="108" style="padding-right:2px" align="right"><?phpecho number_format($tot_variance_yarn_dye_cost_per,2,'.',''); ?></td>-->
                <td width="108" style="padding-right:2px" align="right"><?php echo number_format($tot_variance_aop_cost_per,2,'.',''); ?></td>
                <td width="108" style="padding-right:2px" align="right"><?php echo number_format($tot_variance_trims_cost_per,2,'.',''); ?></td>
                <td width="98" style="padding-right:2px" align="right"><?php echo number_format($tot_variance_embell_cost_per,2,'.',''); ?></td>
                <td width="98" style="padding-right:2px" align="right"><?php echo number_format($tot_variance_wash_cost_per,2,'.',''); ?></td>
                <td width="98" style="padding-right:2px" align="right"><?php echo number_format($tot_variance_commission_cost_per,2,'.',''); ?></td>
                <td width="98" style="padding-right:2px" align="right"><?php echo number_format($tot_variance_comm_cost_per,2,'.',''); ?></td>
                <td width="98" style="padding-right:2px" align="right"><?php echo number_format($tot_variance_freight_cost_per,2,'.',''); ?></td>
                <td width="98" style="padding-right:2px" align="right"><?php echo number_format($tot_variance_test_cost_per,2,'.',''); ?></td>
                <td width="98" style="padding-right:2px" align="right"><?php echo number_format($tot_variance_inspection_cost_per,2,'.',''); ?></td>
                <td width="98" style="padding-right:2px" align="right"><?php echo number_format($tot_variance_currier_cost_per,2,'.',''); ?></td>
                <td width="98" style="padding-right:2px" align="right"><?php echo number_format($tot_variance_cm_cost_per,2,'.',''); ?></td>
                <td width="98" style="padding-right:2px" align="right"><?php echo number_format($tot_variance_all_cost_per,2,'.',''); ?></td>
                <td width="98" style="padding-right:2px" align="right"><?php echo number_format($tot_variance_margin_cost_per,2,'.',''); ?></td>
                <td width="104" style="padding-right:2px" align="right"><?php echo number_format($tot_variance_per_cost_per,2,'.',''); ?></td>
            </tr>
        </table>
        <br />
        <table width="3170">
            <tr>
                <td width="760" valign="top">
                    <div align="center" style="width:450px" id="div_summary"><input type="button" value="Print Preview" class="formbutton" onClick="new_window2('company_div','summary_full')" /></div>
                    <br /><?php $po_ids=implode(',',$po_ids_array); ?>
                    <div id="summary_full"> <font color="#FF0000" style="display:none">*Yarn Dyeing Charge included with actual Yarn Cost</font>
                        <div align="center" id="company_div" style="visibility:hidden; font-size:24px;width:700px"><b><?php echo $company_arr[$company_name]; ?></b></div>
                        <table width="760" cellpadding="0" cellspacing="0" border="1" class="rpt_table" rules="all">
                            <thead>
                            <tr>
                                <th colspan="7">Summary</th>
                            </tr>
                            <tr>
                                <th width="30">SL</th>
                                <th width="180">Particulars</th>
                                <th width="130">Pre Costing</th>
                                <th width="80">%</th>
                                <th width="130">At Actual</th>
                                <th width="80">%</th>
                                <th>Variance</th>
                            </tr> 
							<?php
								$bgcolor1='#E9F3FF';
								$bgcolor2='#FFFFFF';
							?>
                            </thead>
                            <tr bgcolor="<?php echo $bgcolor1; ?>" onClick="change_color('trtd_1','<?php echo $bgcolor1; ?>')" id="trtd_1">
                                <td align="center">1</td>
                                <td>PO/Shipment Value</td>
                                <td align="right"><?php echo number_format($tot_po_value,2); ?></td>
                                <td align="right">&nbsp;</td>
                                <td align="right"><?php echo number_format($tot_ex_factory_val,2); ?></td>
                                <td align="right">&nbsp;</td>
                                <td align="right"><?php echo number_format($tot_ex_factory_val-$tot_po_value,2); ?></td>
                            </tr>
                            <tr bgcolor="<?php echo $bgcolor2; ?>" onClick="change_color('trtd_2','<?php echo $bgcolor2; ?>')" id="trtd_2">
                                <td colspan="7"><b>Cost</b></td>
                            </tr>
                            <tr bgcolor="<?php echo $bgcolor1; ?>" onClick="change_color('trtd_3','<?php echo $bgcolor2; ?>')" id="trtd_3">
                                <td align="center">2</td>
                                <td>Yarn Cost+Yarn Dyeing Cost</td>
                                <td align="right"><?php echo number_format($tot_yarn_cost_mkt+$tot_yarn_dye_cost_mkt,2); ?></td>
                                <td align="right"><?php echo number_format((($tot_yarn_cost_mkt+$tot_yarn_dye_cost_mkt)/$tot_po_value)*100,2); ?></td>
                                <td align="right"><a href="#report_details" onClick="openmypage('<?php echo $po_ids; ?>','yarn_cost','Yarn Cost Details')"><?php echo number_format($tot_yarn_cost_actual,2); ?></a></td>
                                <td align="right"><?php echo number_format(($tot_yarn_cost_actual/$tot_ex_factory_val)*100,2); ?></td>
                                <td align="right"><?php echo number_format(($tot_yarn_cost_mkt+$tot_yarn_dye_cost_mkt)-$tot_yarn_cost_actual,2); ?></td>
                            </tr>
                            <tr bgcolor="<?php echo $bgcolor2; ?>"  onclick="change_color('trtd_4','<?php echo $bgcolor2; ?>')" id="trtd_4">
                                <td align="center">3</td>
                                <td>Knitting Cost</td>
                                <td align="right"><?php echo number_format($tot_knit_cost_mkt,2); ?></td>
                                <td align="right"><?php echo number_format(($tot_knit_cost_mkt/$tot_po_value)*100,2); ?></td>
                                
                                <td align="right"><a href="#report_details" onClick="openmypage('<?php echo $po_ids; ?>','knitting_cost','Knitting Cost Details')"><?php echo number_format($tot_knit_cost_actual,2); ?></a></td>
                                
                                <td align="right"><?php echo number_format(($tot_knit_cost_actual/$tot_ex_factory_val)*100,2); ?></td>
                                <td align="right"><?php echo number_format($tot_knit_cost_mkt-$tot_knit_cost_actual,2); ?></td>
                            </tr>
                            <tr bgcolor="<?php echo $bgcolor1; ?>"  onclick="change_color('trtd_5','<?php echo $bgcolor1; ?>')" id="trtd_5">
                                <td align="center">4</td>
                                <td>Dye & Fin Cost</td>
                                <td align="right"><?php echo number_format($tot_dye_finish_cost_mkt,2); ?></td>
                                <td align="right"><?php echo number_format(($tot_dye_finish_cost_mkt/$tot_po_value)*100,2); ?></td>
                                
                                <td align="right"><a href="#report_details" onClick="openmypage('<?php echo $po_ids; ?>','dye_fin_cost','Dye & Fin Cost Details')"><?php echo number_format($tot_dye_finish_cost_actual,2); ?></a></td>
                                
                                <td align="right"><?php echo number_format(($tot_dye_finish_cost_actual/$tot_ex_factory_val)*100,2); ?></td>
                                <td align="right"><?php echo number_format($tot_dye_finish_cost_mkt-$tot_dye_finish_cost_actual,2); ?></td>
                            </tr>
                             <!--<tr bgcolor="<?phpecho $bgcolor2; ?>"  onclick="change_color('trtd_6','<?php echo $bgcolor2; ?>')" id="trtd_6">
                                <td align="center">5</td>
                                <td>Yarn Dyeing Cost</td>
                                <td align="right"><?phpecho number_format($tot_yarn_dye_cost_mkt,2); ?></td>
                                <td align="right"><?phpecho number_format(($tot_yarn_dye_cost_mkt/$tot_po_value)*100,2); ?></td>
                                <td align="right"><font color="#FF0000">*</font><?php//echo number_format($tot_yarn_dye_cost_actual,2); ?></td>
                                <td align="right">&nbsp;<?php//echo number_format(($tot_yarn_dye_cost_actual/$tot_ex_factory_val)*100,2); ?></td>
                                <td align="right">&nbsp;<?php//echo number_format($tot_yarn_dye_cost_mkt-$tot_yarn_dye_cost_actual,2); ?></td>
                            </tr>-->
                            <tr bgcolor="<?php echo $bgcolor2; ?>"  onclick="change_color('trtd_7','<?php echo $bgcolor1; ?>')" id="trtd_7">
                                <td align="center">5</td>
                                <td>AOP & Others Cost</td>
                                <td align="right"><?php echo number_format($tot_aop_cost_mkt,2); ?></td>
                                <td align="right"><?php echo number_format(($tot_aop_cost_mkt/$tot_po_value)*100,2); ?></td>
                                <td align="right"><?php echo number_format($tot_aop_cost_actual,2); ?></td>
                                <td align="right"><?php echo number_format(($tot_aop_cost_actual/$tot_ex_factory_val)*100,2); ?></td>
                                <td align="right"><?php echo number_format($tot_aop_cost_mkt-$tot_aop_cost_actual,2); ?></td>
                            </tr>
                            <tr bgcolor="<?php echo $bgcolor1; ?>" onClick="change_color('trtd_8','<?php echo $bgcolor2; ?>')" id="trtd_8">
                                <td align="center">6</td>
                                <td>Trims Cost</td>
                                <td align="right"><?php echo number_format($tot_trims_cost_mkt,2); ?></td>
                                <td align="right"><?php echo number_format(($tot_trims_cost_mkt/$tot_po_value)*100,2); ?></td>
                                <td align="right"><?php echo number_format($tot_trims_cost_actual,2); ?></td>
                                <td align="right"><?php echo number_format(($tot_trims_cost_actual/$tot_ex_factory_val)*100,2); ?></td>
                                <td align="right"><?php echo number_format($tot_trims_cost_mkt-$tot_trims_cost_actual,2); ?></td>
                            </tr> 
                            <tr bgcolor="<?php echo $bgcolor2; ?>" onClick="change_color('trtd_9','<?php echo $bgcolor1; ?>')" id="trtd_9">
                                <td align="center">7</td>
                                <td>Embellishment Cost</td>
                                <td align="right"><?php echo number_format($tot_embell_cost_mkt,2); ?></td>
                                <td align="right"><?php echo number_format(($tot_embell_cost_mkt/$tot_po_value)*100,2); ?></td>
                                <td align="right"><?php echo number_format($tot_embell_cost_actual,2); ?></td>
                                <td align="right"><?php echo number_format(($tot_embell_cost_actual/$tot_ex_factory_val)*100,2); ?></td>
                                <td align="right"><?php echo number_format($tot_embell_cost_mkt-$tot_embell_cost_actual,2); ?></td>
                            </tr>
                            <tr bgcolor="<?php echo $bgcolor1; ?>" onClick="change_color('trtd_10','<?php echo $bgcolor2; ?>')" id="trtd_10">
                                <td align="center">8</td>
                                <td>Wash Cost</td>
                                <td align="right"><?php echo number_format($tot_wash_cost_mkt,2); ?></td>
                                <td align="right"><?php echo number_format(($tot_wash_cost_mkt/$tot_po_value)*100,2); ?></td>
                                <td align="right"><?php echo number_format($tot_wash_cost_actual,2); ?></td>
                                <td align="right"><?php echo number_format(($tot_wash_cost_actual/$tot_ex_factory_val)*100,2); ?></td>
                                <td align="right"><?php echo number_format($tot_wash_cost_mkt-$tot_wash_cost_actual,2); ?></td>
                            </tr>
                            <tr bgcolor="<?php echo $bgcolor2; ?>"  onclick="change_color('trtd_11','<?php echo $bgcolor1; ?>')" id="trtd_11">
                                <td align="center">9</td>
                                <td>Commission Cost</td>
                                <td align="right"><?php echo number_format($tot_commission_cost_mkt,2); ?></td>
                                <td align="right"><?php echo number_format(($tot_commission_cost_mkt/$tot_po_value)*100,2); ?></td>
                                <td align="right"><?php echo number_format($tot_commission_cost_actual,2); ?></td>
                                <td align="right"><?php echo number_format(($tot_commission_cost_actual/$tot_ex_factory_val)*100,2); ?></td>
                                <td align="right"><?php echo number_format($tot_commission_cost_mkt-$tot_commission_cost_actual,2); ?></td>
                            </tr>
                            <tr bgcolor="<?php echo $bgcolor1; ?>" onClick="change_color('trtd_12','<?php echo $bgcolor2; ?>')" id="trtd_12">
                                <td align="center">10</td>
                                <td>Commercial Cost</td>
                                <td align="right"><?php echo number_format($tot_comm_cost_mkt,2); ?></td>
                                <td align="right"><?php echo number_format(($tot_comm_cost_mkt/$tot_po_value)*100,2); ?></td>
                                <td align="right"><?php echo number_format($tot_comm_cost_actual,2); ?></td>
                                <td align="right"><?php echo number_format(($tot_comm_cost_actual/$tot_ex_factory_val)*100,2); ?></td>
                                <td align="right"><?php echo number_format($tot_comm_cost_mkt-$tot_comm_cost_actual,2); ?></td>
                            </tr>
                            <tr bgcolor="<?php echo $bgcolor2; ?>" onClick="change_color('trtd_13','<?php echo $bgcolor1; ?>')" id="trtd_13">
                                <td align="center">11</td>
                                <td>Freight Cost</td>
                                <td align="right"><?php echo number_format($tot_freight_cost_mkt,2); ?></td>
                                <td align="right"><?php echo number_format(($tot_freight_cost_mkt/$tot_po_value)*100,2); ?></td>
                                <td align="right"><?php echo number_format($tot_freight_cost_actual,2); ?></td>
                                <td align="right"><?php echo number_format(($tot_freight_cost_actual/$tot_ex_factory_val)*100,2); ?></td>
                                <td align="right"><?php echo number_format($tot_freight_cost_mkt-$tot_freight_cost_actual,2); ?></td>
                            </tr>
                            <tr bgcolor="<?php echo $bgcolor1; ?>"  onclick="change_color('trtd_14','<?php echo $bgcolor2; ?>')" id="trtd_14">
                                <td align="center">12</td>
                                <td>Testing Cost</td>
                                <td align="right"><?php echo number_format($tot_test_cost_mkt,2); ?></td>
                                <td align="right"><?php echo number_format(($tot_test_cost_mkt/$tot_po_value)*100,2); ?></td>
                                <td align="right"><?php echo number_format($tot_test_cost_actual,2); ?></td>
                                <td align="right"><?php echo number_format(($tot_test_cost_actual/$tot_ex_factory_val)*100,2); ?></td>
                                <td align="right"><?php echo number_format($tot_test_cost_mkt-$tot_test_cost_actual,2); ?></td>
                            </tr>
                            <tr bgcolor="<?php echo $bgcolor2; ?>" onClick="change_color('trtd_15','<?php echo $bgcolor1; ?>')" id="trtd_15">
                                <td align="center">13</td>
                                <td>Inspection Cost</td>
                                <td align="right"><?php echo number_format($tot_inspection_cost_mkt,2); ?></td>
                                <td align="right"><?php echo number_format(($tot_inspection_cost_mkt/$tot_po_value)*100,2); ?></td>
                                <td align="right"><?php echo number_format($tot_inspection_cost_actual,2); ?></td>
                                <td align="right"><?php echo number_format(($tot_inspection_cost_actual/$tot_ex_factory_val)*100,2); ?></td>
                                <td align="right"><?php echo number_format($tot_inspection_cost_mkt-$tot_inspection_cost_actual,2); ?></td>
                            </tr>
                            <tr bgcolor="<?php echo $bgcolor1; ?>" onClick="change_color('trtd_16','<?php echo $bgcolor2; ?>')" id="trtd_16">
                                <td align="center">14</td>
                                <td>Courier Cost</td>
                                <td align="right"><?php echo number_format($tot_currier_cost_mkt,2); ?></td>
                                <td align="right"><?php echo number_format(($tot_currier_cost_mkt/$tot_po_value)*100,2); ?></td>
                                <td align="right"><?php echo number_format($tot_currier_cost_actual,2); ?></td>
                                <td align="right"><?php echo number_format(($tot_currier_cost_actual/$tot_ex_factory_val)*100,2); ?></td>
                                <td align="right"><?php echo number_format($tot_currier_cost_mkt-$tot_currier_cost_actual,2); ?></td>
                            </tr>
                            <tr bgcolor="<?php echo $bgcolor2; ?>" onClick="change_color('trtd_17','<?php echo $bgcolor1; ?>')" id="trtd_17">
                                <td align="center">15</td>
                                <td>CM</td>
                                <td align="right"><?php echo number_format($tot_cm_cost_mkt,2); ?></td>
                                <td align="right"><?php echo number_format(($tot_cm_cost_mkt/$tot_po_value)*100,2); ?></td>
                                <td align="right"><?php echo number_format($tot_cm_cost_actual,2); ?></td>
                                <td align="right"><?php echo number_format(($tot_cm_cost_actual/$tot_ex_factory_val)*100,2); ?></td>
                                <td align="right"><?php echo number_format($tot_cm_cost_mkt-$tot_cm_cost_actual,2); ?></td>
                            </tr>
                            <tr bgcolor="<?php echo $bgcolor1; ?>" onClick="change_color('trtd_18','<?php echo $bgcolor2; ?>')" id="trtd_18">
                                <td align="center">16</td>
                                <td>Total Cost</td>
                                <td align="right"><?php echo number_format($tot_mkt_all_cost,2); ?></td>
                                <td align="right"><?php echo number_format(($tot_mkt_all_cost/$tot_po_value)*100,2); ?></td>
                                <td align="right"><?php echo number_format($tot_actual_all_cost,2); ?></td>
                                <td align="right"><?php echo number_format(($tot_actual_all_cost/$tot_ex_factory_val)*100,2); ?></td>
                                <td align="right"><?php echo number_format($tot_mkt_all_cost-$tot_actual_all_cost,2); ?></td>
                            </tr>
                            <tr bgcolor="<?php echo $bgcolor2; ?>" onClick="change_color('trtd_19','<?php echo $bgcolor1; ?>')" id="trtd_19">
                                <td align="center">17</td>
                                <td>Margin/Loss</td>
                                <td align="right"><?php echo number_format($tot_mkt_margin,2); ?></td>
                                <td align="right"><?php echo number_format(($tot_mkt_margin/$tot_po_value)*100,2); ?></td>
                                <td align="right"><?php echo number_format($tot_actual_margin,2); ?></td>
                                <td align="right"><?php echo number_format(($tot_actual_margin/$tot_ex_factory_val)*100,2); ?></td>
                                <td align="right"><?php echo number_format($tot_mkt_margin-$tot_actual_margin,2); ?></td>
                            </tr>
                        </table>
                    </div>
                </td>
                <td width="525" align="center" valign="top">
                    <div align="center" style="width:500px; height:53px" id="graph">&nbsp;</div>
                    <fieldset style="text-align:center; width:450px" > 
                    	<legend>Chart</legend>
                    </fieldset>
                </td>   
                <td width="" valign="top">
                    <div align="center" style="width:600px" id="div_buyer"><input type="button" value="Print Preview" class="formbutton" onClick="new_window2('company_div_b','summary_buyer')" /></div>
                    <br />
                    <div id="summary_buyer">
                        <div align="center" id="company_div_b" style="visibility:hidden; font-size:24px;width:1105px"><b><?php echo $company_arr[$company_name]; ?></b></div>
                        <table width="100%" cellpadding="0" cellspacing="0" border="1" class="rpt_table" rules="all">
                            <thead>
                                 <tr>
                                    <th colspan="21">Buyer Level Summary</th>
                                </tr>
                                <tr>
                                    <th width="35">SL</th>
                                    <th width="70">Buyer name</th>
                                    <th width="110">Cost Source</th>
                                    <th width="110">Ex-Factory Value</th>
                                    <th width="110">Yarn cost</th>
                                    <th width="110">Knitting Cost</th>
                                    <th width="110">Dye & Fin Cost</th>
                                    <!--<th width="110">Yarn Dyeing Cost</th>-->
                                    <th width="110">AOP & Others Cost</th>
                                    <th width="110">Trims Cost</th>
                                    <th width="110">Embellishment Cost</th>
                                    <th width="110">Wash Cost</th>
                                    <th width="110">Commission Cost</th>
                                    <th width="100">Commercial Cost</th>
                                    <th width="110">Freight Cost</th>
                                    <th width="100">Testing Cost</th>
                                    <th width="100">Inspection Cost</th>
                                    <th width="100">Courier Cost</th>
                                    <th width="110">CM Cost</th>
                                    <th width="110">Toal Cost</th>
                                    <th width="110">Margin</th>
                                    <th width="90">Margin %</th>
                                </tr>
                            </thead>
                            <?php
                                $j=1;
                                foreach($buyer_name_array as $key=>$value)
                                {
                                    if ($j%2==0)  
                                        $bgcolor="#E9F3FF";
                                    else
                                        $bgcolor="#FFFFFF";	
                                ?>
                                    <tr bgcolor="<?php echo $bgcolor; ?>" onClick="change_color('trTD_<?php echo $j; ?>','<?php echo $bgcolor; ?>')" id="trTD_<?php echo $j; ?>">
                                        <td rowspan="4"><?php echo $j ;?></td>
                                        <td rowspan="4"><?php echo $value ;?></td>
                                        <td><b>Pre Costing</b></td>
                                        <td align="right"><?php echo number_format($mkt_po_val_array[$key],2); $tot_mkt_po_val+=$mkt_po_val_array[$key];  ?></td>
                                        <td align="right"><?php echo number_format($mkt_yarn_array[$key],2); $tot_mkt_yarn_cost+=$mkt_yarn_array[$key]; ?></td>
                                        <td align="right"><?php echo number_format($mkt_knit_array[$key],2); $tot_mkt_knit_cost+=$mkt_knit_array[$key]; ?></td>
                                        <td align="right"><?php echo number_format($mkt_dy_fin_array[$key],2); $tot_mkt_dy_fin_cost+=$mkt_dy_fin_array[$key]; ?></td>
                                        <!--<td align="right"><?phpecho number_format($mkt_yarn_dy_array[$key],2); $tot_mkt_yarn_dy_cost+=$mkt_yarn_dy_array[$key]; ?></td>-->
                                        <td align="right"><?php echo number_format($mkt_aop_array[$key],2); $tot_mkt_aop_cost+=$mkt_aop_array[$key]; ?></td>
                                        <td align="right"><?php echo number_format($mkt_trims_array[$key],2); $tot_mkt_trims_cost+=$mkt_trims_array[$key]; ?></td>
                                        <td align="right"><?php echo number_format($mkt_emb_array[$key],2); $tot_mkt_emb_cost+=$mkt_emb_array[$key]; ?></td>
                                        <td align="right"><?php echo number_format($mkt_wash_array[$key],2); $tot_mkt_wash_cost+=$mkt_wash_array[$key]; ?></td>
                                        <td align="right"><?php echo number_format($mkt_commn_array[$key],2); $tot_mkt_commn_cost+=$mkt_commn_array[$key]; ?></td>
                                        <td align="right"><?php echo number_format($mkt_commercial_array[$key],2); $tot_mkt_commercial_cost+=$mkt_commercial_array[$key]; ?></td>
                                        <td align="right"><?php echo number_format($mkt_freight_array[$key],2); $tot_mkt_freight_cost+=$mkt_freight_array[$key]; ?></td>
                                        <td align="right"><?php echo number_format($mkt_test_array[$key],2); $tot_mkt_test_cost+=$mkt_test_array[$key]; ?></td>
                                        <td align="right"><?php echo number_format($mkt_ins_array[$key],2); $tot_mkt_ins_cost+=$mkt_ins_array[$key]; ?></td>
                                        <td align="right"><?php echo number_format($mkt_courier_array[$key],2); $tot_mkt_courier_cost+=$mkt_courier_array[$key]; ?></td>
                                        <td align="right"><?php echo number_format($mkt_cm_array[$key],2); $tot_mkt_cm_cost+=$mkt_cm_array[$key]; ?></td>
                                        <td align="right"><?php echo number_format($mkt_total_array[$key],2); $tot_mkt_total_cost+=$mkt_total_array[$key]; ?></td>
                                        <td align="right"><?php echo number_format($mkt_margin_array[$key],2); $tot_mkt_margin_cost+=$mkt_margin_array[$key]; ?></td>
                                        <td align="right"><?php $mkt_margin_perc=($mkt_margin_array[$key]/$mkt_po_val_array[$key])*100; echo number_format($mkt_margin_perc,2); $tot_mkt_margin_perc_cost+=$mkt_margin_perc; ?></td> 
                                    </tr>
                                    <tr bgcolor="<?php echo $bgcolor; ?>" onClick="change_color('trTD2_<?php echo $j; ?>','<?php echo $bgcolor; ?>')" id="trTD2_<?php echo $j; ?>">
                                        <td><b>Actual</b></td>
                                        <td align="right"><?php echo number_format($ex_factory_val_array[$key],2); $tot_buyer_ex_factory_val+=$ex_factory_val_array[$key];  ?></td>
                                        <td align="right"><?php echo number_format($yarn_cost_array[$key],2); $tot_buyer_yarn_cost+=$yarn_cost_array[$key]; ?></td>
                                        <td align="right"><?php echo number_format($knit_cost_array[$key],2); $tot_buyer_knit_cost+=$knit_cost_array[$key]; ?></td>
                                        <td align="right"><?php echo number_format($dye_cost_array[$key],2); $tot_buyer_dye_cost+=$dye_cost_array[$key]; ?></td>
                                        <!--<td align="right"><?phpecho number_format($yarn_dyeing_cost_array[$key],2); $tot_buyer_yarn_dyeing_cost+=$yarn_dyeing_cost_array[$key];?></td>-->
                                        <td align="right"><?php echo number_format($aop_n_others_cost_array[$key],2); $tot_buyer_aop_n_others_cost+=$aop_n_others_cost_array[$key]; ?></td>
                                        <td align="right"><?php echo number_format($trims_cost_array[$key],2); $tot_buyer_trims_cost+=$trims_cost_array[$key]; ?></td>
                                        <td align="right"><?php echo number_format($enbellishment_cost_array[$key],2); $tot_buyer_embell_cost+=$enbellishment_cost_array[$key]; ?></td>
                                        <td align="right"><?php echo number_format($wash_cost_array[$key],2); $tot_buyer_wash_cost+=$wash_cost_array[$key]; ?></td>
                                        <td align="right"><?php echo number_format($commission_cost_array[$key],2); $tot_buyer_commi_cost+=$commission_cost_array[$key]; ?></td>
                                        <td align="right"><?php echo number_format($commercial_cost_array[$key],2); $tot_buyer_commercial_cost+=$commercial_cost_array[$key]; ?></td>
                                        <td align="right"><?php echo number_format($freight_cost_array[$key],2); $tot_buyer_freight_cost+=$freight_cost_array[$key]; ?></td>
                                        <td align="right"><?php echo number_format($testing_cost_array[$key],2); $tot_buyer_testing_cost+=$testing_cost_array[$key]; ?></td>
                                        <td align="right"><?php echo number_format($inspection_cost_array[$key],2); $tot_buyer_inspection_cost+=$inspection_cost_array[$key]; ?></td>
                                        <td align="right"><?php echo number_format($courier_cost_array[$key],2); $tot_buyer_courier_cost+=$courier_cost_array[$key]; ?></td>
                                        <td align="right"><?php echo number_format($cm_cost_array[$key],2); $tot_buyer_cm_cost+=$cm_cost_array[$key]; ?></td>
                                        <td align="right"><?php echo number_format($total_cost_array[$key],2); $tot_buyer_total_cost+=$total_cost_array[$key]; ?></td>
                                        <td align="right"><?php echo number_format($margin_array[$key],2); $tot_buyer_margin_cost+=$margin_array[$key]; ?></td>
                                        <td align="right"><?php $margin_perc=($margin_array[$key]/$ex_factory_val_array[$key])*100; echo number_format($margin_perc,2); $tot_buyer_margin_perc_cost+=$margin_perc; ?></td> 
                                    </tr>
                                    <tr bgcolor="<?php echo $bgcolor; ?>" onClick="change_color('trTD3_<?php echo $j; ?>','<?php echo $bgcolor; ?>')" id="trTD3_<?php echo $j; ?>">
                                        <td><b>Variance</b></td>
                                        <td align="right">
                                        <?php
                                            $ex_var= $mkt_po_val_array[$key]-$ex_factory_val_array[$key];
                                            echo number_format($ex_var,2); 
                                        ?>
                                        </td>
                                        <td align="right">
                                        <?php
                                            $yarn_var= $mkt_yarn_array[$key]-$yarn_cost_array[$key];
                                            echo number_format($yarn_var,2);
                                        ?>
                                        </td>
                                        <td align="right">
                                        <?php
                                            $knit_var= $mkt_knit_array[$key]-$knit_cost_array[$key];
                                            echo number_format($knit_var,2); 
                                        ?>
                                        </td>
                                        <td align="right">
                                        <?php
                                            $dy_var= $mkt_dy_fin_array[$key]-$dye_cost_array[$key];
                                            echo number_format($dy_var,2); 
                                        ?>
                                        </td>
                                        <!--<td align="right"><?php$yarn_dy_var= $mkt_yarn_dy_array[$key]-$yarn_dyeing_cost_array[$key];echo number_format($yarn_dy_var,2);?></td>-->
                                        <td align="right">
                                        <?php
                                            $aop_var= $mkt_aop_array[$key]-$aop_n_others_cost_array[$key];
                                            echo number_format($aop_var,2); 
                                        ?>
                                        </td>
                                        <td align="right">
                                        <?php
                                            $trims_var= $mkt_trims_array[$key]-$trims_cost_array[$key];
                                            echo number_format($trims_var,2); 
                                        ?>
                                        </td>
                                        <td align="right">
                                        <?php
                                            $emb_var= $mkt_emb_array[$key]-$enbellishment_cost_array[$key];
                                            echo number_format($emb_var,2); 
                                        ?>
                                        </td>
                                        <td align="right">
                                        <?php
                                            $wash_var= $mkt_wash_array[$key]-$wash_cost_array[$key];
                                            echo number_format($wash_var,2); 
                                        ?>
                                        </td>
                                        <td align="right">
                                        <?php
                                            $com_var= $mkt_commn_array[$key]-$commission_cost_array[$key];
                                            echo number_format($com_var,2); 
                                        ?>
                                        </td>
                                        <td align="right">
                                        <?php
                                            $commer_var= $mkt_commercial_array[$key]-$commercial_cost_array[$key];
                                            echo number_format($commer_var,2); 
                                        ?>
                                        </td>
                                        <td align="right">
                                        <?php
                                            $fr_var= $mkt_freight_array[$key]-$freight_cost_array[$key];
                                            echo number_format($fr_var,2); 
                                        ?>
                                        </td>
                                        <td align="right">
                                        <?php
                                            $test_var= $mkt_test_array[$key]-$testing_cost_array[$key];
                                            echo number_format($test_var,2); 
                                        ?>
                                        </td>
                                        <td align="right">
                                        <?php
                                            $ins_var= $mkt_ins_array[$key]-$inspection_cost_array[$key];
                                            echo number_format($ins_var,2); 
                                        ?>
                                        </td>
                                        <td align="right">
                                        <?php
                                            $cour_var= $mkt_courier_array[$key]-$courier_cost_array[$key];
                                            echo number_format($cour_var,2); 
                                        ?>
                                        </td>
                                        <td align="right">
                                        <?php
                                            $cm_var= $mkt_cm_array[$key]-$cm_cost_array[$key];
                                            echo number_format($cm_var,2); 
                                        ?>
                                        </td>
                                        <td align="right">
                                        <?php
                                            $tot_var= $mkt_total_array[$key]-$total_cost_array[$key];
                                            echo number_format($tot_var,2); 
                                        ?>
                                        </td>
                                        <td align="right">
                                        <?php
                                            $mar_var= $mkt_margin_array[$key]-$margin_array[$key];
                                            echo number_format($mar_var,2); 
                                        ?>
                                        </td>
                                        <td align="right">
                                        <?php
                                            $margin_perc_to= $mkt_margin_perc-$margin_perc;
                                            echo number_format($margin_perc_to,2); 
                                        ?>
                                        </td>
                                    </tr>
                                    <tr bgcolor="<?php echo $bgcolor; ?>" onClick="change_color('trTD4_<?php echo $j; ?>','<?php echo $bgcolor; ?>')" id="trTD4_<?php echo $j; ?>">
                                        <td><b>Variance (%)</b></td>
                                        <td align="right"><?php echo number_format(($ex_var/$mkt_po_val_array[$key]*100),2); ?></td>
                                        <td align="right"><?php echo number_format(($yarn_var/$mkt_yarn_array[$key]*100),2); ?></td>
                                        <td align="right"><?php echo number_format(($knit_var/$mkt_knit_array[$key]*100),2); ?></td>
                                        <td align="right"><?php echo number_format(($dy_var/$mkt_dy_fin_array[$key]*100),2); ?></td>
                                       <!-- <td align="right"><?phpecho number_format(($yarn_dy_var/$mkt_yarn_dy_array[$key]*100),2); ?></td>-->
                                        <td align="right"><?php echo number_format(($aop_var/$mkt_aop_array[$key]*100),2); ?></td>
                                        <td align="right"><?php echo number_format(($trims_var/$mkt_trims_array[$key]*100),2); ?></td>
                                        <td align="right"><?php echo number_format(($emb_var/$mkt_emb_array[$key]*100),2); ?></td>
                                        <td align="right"><?php echo number_format(($wash_var/$mkt_wash_array[$key]*100),2); ?></td>
                                        <td align="right"><?php echo number_format(($com_var/$mkt_commn_array[$key]*100),2); ?></td>
                                        <td align="right"><?php echo number_format(($commer_var/$mkt_commercial_array[$key]*100),2); ?></td>
                                        <td align="right"><?php echo number_format(($fr_var/$mkt_freight_array[$key]*100),2); ?></td>
                                        <td align="right"><?php echo number_format(($test_var/$mkt_test_array[$key]*100),2); ?></td>
                                        <td align="right"><?php echo number_format(($ins_var/$mkt_ins_array[$key]*100),2); ?></td>
                                        <td align="right"><?php echo number_format(($cour_var/$mkt_courier_array[$key]*100),2); ?></td>
                                        <td align="right"><?php echo number_format(($cm_var/$mkt_cm_array[$key]*100),2); ?></td>
                                        <td align="right"><?php echo number_format(($tot_var/$mkt_total_array[$key]*100),2); ?></td>
                                        <td align="right"><?php echo number_format(($mar_var/$mkt_margin_array[$key]*100),2); ?></td>
                                        <td align="right"><?php echo number_format(($margin_perc_to/$mkt_margin_perc*100),2); ?></td> 
                                    </tr>
                           	<?php
                                $j++;
                                }
								
								$bgcolor5='#CCDDEE';
								$bgcolor6='#CCCCFF';
								$bgcolor7='#FFEEFF';
                            ?>
                            <tr bgcolor="<?php echo $bgcolor5; ?>" onClick="change_color('trTD5_<?php echo $j; ?>','<?php echo $bgcolor5; ?>')" id="trTD5_<?php echo $j; ?>">
                                <th colspan="3">Pre Costing</th>
                                <th align="right"><?php echo number_format($tot_mkt_po_val,2); ?></th>
                                <th align="right"><?php echo number_format($tot_mkt_yarn_cost,2); ?></th>
                                <th align="right"><?php echo number_format($tot_mkt_knit_cost,2); ?></th>
                                <th align="right"><?php echo number_format($tot_mkt_dy_fin_cost,2); ?></th>
                                <!--<th align="right"><?phpecho number_format($tot_mkt_yarn_dy_cost,2); ?></th>-->
                                <th align="right"><?php echo number_format($tot_mkt_aop_cost,2); ?></th>
                                <th align="right"><?php echo number_format($tot_mkt_trims_cost,2); ?></th>
                                <th align="right"><?php echo number_format($tot_mkt_emb_cost,2); ?></th>
                                <th align="right"><?php echo number_format($tot_mkt_wash_cost,2); ?></th>
                                <th align="right"><?php echo number_format($tot_mkt_commn_cost,2); ?></th>
                                <th align="right"><?php echo number_format($tot_mkt_commercial_cost,2); ?></th>
                                <th align="right"><?php echo number_format($tot_mkt_freight_cost,2); ?></th>
                                <th align="right"><?php echo number_format($tot_mkt_test_cost,2); ?></th>
                                <th align="right"><?php echo number_format($tot_mkt_ins_cost,2); ?></th>
                                <th align="right"><?php echo number_format($tot_mkt_courier_cost,2); ?></th>
                                <th align="right"><?php echo number_format($tot_mkt_cm_cost,2); ?></th>
                                <th align="right"><?php echo number_format($tot_mkt_total_cost,2); ?></th>
                                <th align="right"><?php echo number_format($tot_mkt_margin_cost,2); ?></th>
                                <th align="right"><?php $mm=$tot_mkt_margin_cost/$tot_mkt_po_val*100; echo number_format($mm,2); ?></th>   
                            </tr>
                            <tr bgcolor="<?php echo $bgcolor6; ?>" onClick="change_color('trTD6_<?php echo $j; ?>','<?php echo $bgcolor6; ?>')" id="trTD6_<?php echo $j; ?>">
                                <th colspan="3">Actual</th>
                                <th align="right"><?php echo number_format($tot_buyer_ex_factory_val,2); ?></th>
                                <th align="right"><?php echo number_format($tot_buyer_yarn_cost,2); ?></th>
                                <th align="right"><?php echo number_format($tot_buyer_knit_cost,2); ?></th>
                                <th align="right"><?php echo number_format($tot_buyer_dye_cost,2); ?></th>
                                <!--<th align="right"><?phpecho number_format($tot_buyer_yarn_dyeing_cost,2); ?></th>-->
                                <th align="right"><?php echo number_format($tot_buyer_aop_n_others_cost,2); ?></th>
                                <th align="right"><?php echo number_format($tot_buyer_trims_cost,2); ?></th>
                                <th align="right"><?php echo number_format($tot_buyer_embell_cost,2); ?></th>
                                <th align="right"><?php echo number_format($tot_buyer_wash_cost,2); ?></th>
                                <th align="right"><?php echo number_format($tot_buyer_commi_cost,2); ?></th>
                                <th align="right"><?php echo number_format($tot_buyer_commercial_cost,2); ?></th>
                                <th align="right"><?php echo number_format($tot_buyer_freight_cost,2); ?></th>
                                <th align="right"><?php echo number_format($tot_buyer_testing_cost,2); ?></th>
                                <th align="right"><?php echo number_format($tot_buyer_inspection_cost,2); ?></th>
                                <th align="right"><?php echo number_format($tot_buyer_courier_cost,2); ?></th>
                                <th align="right"><?php echo number_format($tot_buyer_cm_cost,2); ?></th>
                                <th align="right"><?php echo number_format($tot_buyer_total_cost,2); ?></th>
                                <th align="right"><?php echo number_format($tot_buyer_margin_cost,2); ?></th>
                                <th align="right"><?php $pp=$tot_buyer_margin_cost/$tot_buyer_ex_factory_val*100; echo number_format($pp,2); ?></th>   
                            </tr>
                            <tr bgcolor="<?php echo $bgcolor7; ?>" onClick="change_color('trTD7_<?php echo $j; ?>','<?php echo $bgcolor7; ?>')" id="trTD7_<?php echo $j; ?>">
                                <th colspan="3">Variance</th>
                                <th align="right"><?php $evar=$tot_mkt_po_val-$tot_buyer_ex_factory_val;  echo number_format($evar,2); ?></th>
                                <th align="right"><?php $yvar=$tot_mkt_yarn_cost-$tot_buyer_yarn_cost;  echo number_format($yvar,2); ?></th>
                                <th align="right"><?php $kvar=$tot_mkt_knit_cost-$tot_buyer_knit_cost;  echo number_format($kvar,2); ?></th>
                                <th align="right"><?php $dfvar=$tot_mkt_dy_fin_cost-$tot_buyer_dye_cost;  echo number_format($dfvar,2); ?></th>
                                <!--<th align="right"><?php$ydvar=$tot_mkt_yarn_dy_cost-$tot_buyer_yarn_dyeing_cost;  echo number_format($ydvar,2); ?></th>-->
                                <th align="right"><?php $aopvar=$tot_mkt_aop_cost-$tot_buyer_aop_n_others_cost;  echo number_format($aopvar,2); ?></th>
                                <th align="right"><?php $trimvar=$tot_mkt_trims_cost-$tot_buyer_trims_cost;  echo number_format($trimvar,2); ?></th>
                                <th align="right"><?php $embvar=$tot_mkt_emb_cost-$tot_buyer_embell_cost;  echo number_format($embvar,2); ?></th>
                                <th align="right"><?php $washvar=$tot_mkt_wash_cost-$tot_buyer_wash_cost;  echo number_format($washvar,2); ?></th>
                                <th align="right"><?php $comvar=$tot_mkt_commn_cost-$tot_buyer_commi_cost;  echo number_format($comvar,2); ?></th>
                                <th align="right"><?php $commercialvar=$tot_mkt_commercial_cost-$tot_buyer_commercial_cost;  echo number_format($commercialvar,2); ?></th>
                                <th align="right"><?php $fvar=$tot_mkt_freight_cost-$tot_buyer_freight_cost;  echo number_format($fvar,2); ?></th>
                                <th align="right"><?php $tvar=$tot_mkt_test_cost-$tot_buyer_testing_cost;  echo number_format($tvar,2); ?></th>
                                <th align="right"><?php $ivar=$tot_mkt_ins_cost-$tot_buyer_inspection_cost;  echo number_format($ivar,2); ?></th>
                                <th align="right"><?php $courvar=$tot_mkt_courier_cost-$tot_buyer_courier_cost;  echo number_format($courvar,2); ?></th>
                                <th align="right"><?php $cmvar=$tot_mkt_cm_cost-$tot_buyer_cm_cost;  echo number_format($cmvar,2); ?></th>
                                <th align="right"><?php $totvar=$tot_mkt_total_cost-$tot_buyer_total_cost;  echo number_format($totvar,2); ?></th>
                                <th align="right"><?php $mvar=$tot_mkt_margin_cost-$tot_buyer_margin_cost;  echo number_format($mvar,2); ?></th>
                                <th align="right"><?php $mpvar=$mm-$pp;  echo number_format($mpvar,2); ?></th>   
                            </tr>
                            <tr bgcolor="<?php echo $bgcolor6; ?>" onClick="change_color('trTD8_<?php echo $j; ?>','<?php echo $bgcolor6; ?>')" id="trTD8_<?php echo $j; ?>">
                                <th colspan="3">Variance (%)</th>
                                <th align="right"><?php echo number_format(($evar/$tot_mkt_po_val*100),2); ?></th>
                                <th align="right"><?php echo number_format(($yvar/$tot_mkt_yarn_cost*100),2); ?></th>
                                <th align="right"><?php echo number_format(($kvar/$tot_mkt_knit_cost*100),2); ?></th>
                                <th align="right"><?php echo number_format(($dfvar/$tot_mkt_dy_fin_cost*100),2); ?></th>
                                <!--<th align="right"><?phpecho number_format(($ydvar/$tot_mkt_yarn_dy_cost*100),2); ?></th>-->
                                <th align="right"><?php echo number_format(($aopvar/$tot_mkt_aop_cost*100),2); ?></th>
                                <th align="right"><?php echo number_format(($trimvar/$tot_mkt_trims_cost*100),2); ?></th>
                                <th align="right"><?php echo number_format(($embvar/$tot_mkt_emb_cost*100),2); ?></th>
                                <th align="right"><?php echo number_format(($washvar/$tot_mkt_wash_cost*100),2); ?></th>
                                <th align="right"><?php echo number_format(($comvar/$tot_mkt_commn_cost*100),2); ?></th>
                                <th align="right"><?php echo number_format(($commercialvar/$tot_mkt_commercial_cost*100),2); ?></th>
                                <th align="right"><?php echo number_format(($fvar/$tot_mkt_freight_cost*100),2); ?></th>
                                <th align="right"><?php echo number_format(($tvar/$tot_mkt_test_cost*100),2); ?></th>
                                <th align="right"><?php echo number_format(($ivar/$tot_mkt_ins_cost*100),2); ?></th>
                                <th align="right"><?php echo number_format(($courvar/$tot_mkt_courier_cost*100),2); ?></th>
                                <th align="right"><?php echo number_format(($cmvar/$tot_mkt_cm_cost*100),2); ?></th>
                                <th align="right"><?php echo number_format(($totvar/$tot_mkt_total_cost*100),2); ?></th>
                                <th align="right"><?php echo number_format(($mvar/$tot_mkt_margin_cost*100),2); ?></th>
                                <th align="right"><?php echo number_format(($mpvar/$mm*100),2); ?></th>   
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
	</fieldset>
<?php
	foreach (glob("../../../../ext_resource/tmp_report/$user_name*.xls") as $filename) 
	{
		if( @filemtime($filename) < (time()-$seconds_old) )
		@unlink($filename);
	}
	//---------end------------//
	$html=ob_get_contents();
	$name=time();
	$filename="../../../../ext_resource/tmp_report/".$user_name."_".$name.".xls";
	$create_new_doc = fopen($filename, 'w');
	$is_created = fwrite($create_new_doc,$html);
	$filename="../../../ext_resource/tmp_report/".$user_name."_".$name.".xls";
	echo "$total_data****$filename";
	exit();
}

if($action=="yarn_cost")
{
	echo load_html_head_contents("Report Info","../../../../", 1, 1, $unicode,'','');
	extract($_REQUEST);
	
	$exchange_rate=76;
	$costing_per_arr=return_library_array( "select job_no, costing_per_id from wo_pre_cost_dtls where status_active=1 and is_deleted=0", "job_no", "costing_per_id");
	$avg_rate_array=return_library_array( "select id, avg_rate_per_unit from product_details_master where item_category_id=1", "id", "avg_rate_per_unit"  );
	$yarn_count_details=return_library_array( "select id, yarn_count from lib_yarn_count", "id", "yarn_count");
	$buyer_arr=return_library_array( "select id, short_name from lib_buyer", "id", "short_name"  );

	$dataArrayYarn=array();
	$yarn_sql="select job_no, count_id, copm_one_id, percent_one, copm_two_id, percent_two, type_id, sum(cons_qnty) as qnty, sum(amount) as amount from wo_pre_cost_fab_yarn_cost_dtls where status_active=1 and is_deleted=0 group by job_no, count_id, copm_one_id, percent_one, copm_two_id, percent_two, type_id";
	$resultYarn=sql_select($yarn_sql);
	foreach($resultYarn as $yarnRow)
	{
		$dataArrayYarn[$yarnRow[csf('job_no')]].=$yarnRow[csf('count_id')]."**".$yarnRow[csf('copm_one_id')]."**".$yarnRow[csf('percent_one')]."**".$yarnRow[csf('copm_two_id')]."**".$yarnRow[csf('percent_two')]."**".$yarnRow[csf('type_id')]."**".$yarnRow[csf('qnty')]."**".$yarnRow[csf('amount')].",";
	}
	
	$receive_array=array();
	$sql_receive="select prod_id, sum(order_qnty) as qty, sum(order_amount) as amnt from inv_transaction where transaction_type=1 and item_category=1 and status_active=1 and is_deleted=0 group by prod_id";
	$resultReceive = sql_select($sql_receive);
	foreach($resultReceive as $invRow)
	{
		$avg_rate=$invRow[csf('amnt')]/$invRow[csf('qty')];
		$receive_array[$invRow[csf('prod_id')]]=$avg_rate;
	}
	
	$yarnIssDataArray=sql_select("select po_breakdown_id, prod_id,
					sum(CASE WHEN entry_form ='3' and trans_type=2 and issue_purpose!=2 THEN quantity ELSE 0 END) AS yarn_iss_qty,
					sum(CASE WHEN entry_form ='9' and trans_type=4 THEN quantity ELSE 0 END) AS yarn_iss_return_qty,
					sum(CASE WHEN entry_form ='11' and trans_type=5 THEN quantity ELSE 0 END) AS trans_in_qty_yarn,
					sum(CASE WHEN entry_form ='11' and trans_type=6 THEN quantity ELSE 0 END) AS trans_out_qty_yarn
					from order_wise_pro_details where trans_type in(2,4,5,6) and status_active=1 and is_deleted=0 and po_breakdown_id in($po_id) group by po_breakdown_id,prod_id");
	foreach($yarnIssDataArray as $invRow)
	{
		$iss_qty=$invRow[csf('yarn_iss_qty')]+$invRow[csf('trans_in_qty_yarn')]-$invRow[csf('yarn_iss_return_qty')]-$invRow[csf('trans_out_qty_yarn')];
		$dataArrayYarnIssue[$invRow[csf('po_breakdown_id')]][1]+=$iss_qty;
		$rate='';
		if($receive_array[$invRow[csf('prod_id')]]>0)
		{
			$rate=$receive_array[$invRow[csf('prod_id')]];
		}
		else
		{
			$rate=$avg_rate_array[$invRow[csf('prod_id')]]/$exchange_rate;
		}
		$dataArrayYarnIssue[$invRow[csf('po_breakdown_id')]][2]+=$iss_qty*$rate;
	}

?>
	<style>
		hr
		{
			color: #676767;
			background-color: #676767;
			height: 1px;
		}
	</style> 
    <div>
        <fieldset style="width:1033px;">
        	<table class="rpt_table" width="1030" cellpadding="0" cellspacing="0" border="1" rules="all">
            	<thead>
                	<th width="40">SL</th>
                    <th width="60">Buyer</th>
                    <th width="80">PO No</th>
                    <th width="50">Year</th>
                    <th width="60">Job No</th>
                    <th width="90">Order Qty</th>
                    <th width="70">Count</th>
                    <th width="100">Composition</th>
                    <th width="70">Type</th>
                    <th width="90">Required<br/><font style="font-size:9px; font-weight:100">(As Per Pre-Cost)</font></th>
                    <th width="100">Cost ($)</th>
                    <th width="90">Issued</th>
                    <th>Cost ($)</th>
                </thead>
            </table>	
            <div style="width:1030px; max-height:310px; overflow-y:scroll" id="scroll_body">
                <table class="rpt_table" width="1010" cellpadding="0" cellspacing="0" border="1" rules="all">
                	<?php
					if($db_type==0) $year_field="YEAR(a.insert_date) as year"; 
					else if($db_type==2) $year_field="to_char(a.insert_date,'YYYY') as year";
					else $year_field="";//defined Later
						
					$sql="select a.job_no_prefix_num, a.job_no, $year_field, a.company_name, a.buyer_name, a.style_ref_no, a.order_uom, a.gmts_item_id, a.total_set_qnty as ratio, b.id, b.po_number, b.pub_shipment_date, b.po_quantity, b.plan_cut, b.unit_price, b.po_total_price, b.shiping_status from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.id in($po_id) order by b.pub_shipment_date, a.job_no_prefix_num, b.id";
					$result=sql_select($sql);
					$i=1; $tot_po_qnty=0; $tot_mkt_required=0; $tot_required_cost=0; $tot_yarn_iss_qty=0; $tot_yarn_iss_cost=0;
					foreach($result as $row)
					{
						if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";

						$dzn_qnty=0; $job_mkt_required=0; $yarn_issued=0; $yarn_data_array=array(); $job_mkt_required_cost=0;
						$costing_per_id=$costing_per_arr[$row[csf('job_no')]];
						if($costing_per_id==1)
						{
							$dzn_qnty=12;
						}
						else if($costing_per_id==3)
						{
							$dzn_qnty=12*2;
						}
						else if($costing_per_id==4)
						{
							$dzn_qnty=12*3;
						}
						else if($costing_per_id==5)
						{
							$dzn_qnty=12*4;
						}
						else
						{
							$dzn_qnty=1;
						}
						
						$dzn_qnty=$dzn_qnty*$row[csf('ratio')];
						$order_qnty_in_pcs=$row[csf('po_quantity')]*$row[csf('ratio')];
						$plan_cut_qnty=$row[csf('plan_cut')]*$row[csf('ratio')];
						$tot_po_qnty+=$order_qnty_in_pcs;
						
						$dataYarn=explode(",",substr($dataArrayYarn[$row[csf('job_no')]],0,-1));
						foreach($dataYarn as $yarnRow)
						{
							$yarnRow=explode("**",$yarnRow);
							$count_id=$yarnRow[0];
							$copm_one_id=$yarnRow[1];
							$percent_one=$yarnRow[2];
							$copm_two_id=$yarnRow[3];
							$percent_two=$yarnRow[4];
							$type_id=$yarnRow[5];
							$qnty=$yarnRow[6];
							$amnt=$yarnRow[7];
							
							$mkt_required=$plan_cut_qnty*($qnty/$dzn_qnty);
							$mkt_required_cost=$plan_cut_qnty*($amnt/$dzn_qnty);
							$job_mkt_required+=$mkt_required;
							$job_mkt_required_cost+=$mkt_required_cost;
							
							$yarn_data_array['count'][]=$yarn_count_details[$count_id];
							$yarn_data_array['type'][]=$yarn_type[$type_id];
							
							if($percent_two!=0)
							{
								$compos=$composition[$copm_one_id]." ".$percent_one." %"." ".$composition[$copm_two_id]." ".$percent_two." %";
							}
							else
							{
								$compos=$composition[$copm_one_id]." ".$percent_one." %"." ".$composition[$copm_two_id];
							}

							$yarn_data_array['comp'][]=$compos;
						}
						
						$yarn_iss_qty=$dataArrayYarnIssue[$row[csf('id')]][1];
						$yarn_iss_cost=$dataArrayYarnIssue[$row[csf('id')]][2];
						
					?>
						<tr bgcolor="<?php echo $bgcolor; ?>">
							<td width="40"><?php echo $i; ?></td>
							<td width="60" style="word-break:break-all;"><?php echo $buyer_arr[$row[csf('buyer_name')]]; ?></td>
                            <td width="80" style="word-break:break-all;"><?php echo $row[csf('po_number')]; ?></td>
                            <td width="50" align="center"><?php echo $row[csf('year')]; ?></td>
                            <td width="60" align="center"><?php echo $row[csf('job_no_prefix_num')]; ?></td>
                            <td width="90" align="right"><?php echo $order_qnty_in_pcs; ?></td>
                            <td width="70">
								<?php 
                                    $d=1;
                                    foreach($yarn_data_array['count'] as $yarn_count_value)
                                    {
                                        if($d!=1)
                                        {
                                            echo "<hr/>";
                                        }
                                        echo $yarn_count_value;
                                        $d++;
                                    }
                                ?>
                            </td>
                            <td width="100">
                                <div style="word-wrap:break-word; width:100px">
                                    <?php 
                                         $d=1;
                                         foreach($yarn_data_array['comp'] as $yarn_composition_value)
                                         {
                                            if($d!=1)
                                            {
                                                echo "<hr/>";
                                            }
                                            echo $yarn_composition_value;
                                            $d++;
                                         }
                                    ?>
                                </div>
                            </td>
                            <td width="70">
                                <p>
                                    <?php 
                                         $d=1;
                                         foreach($yarn_data_array['type'] as $yarn_type_value)
                                         {
                                            if($d!=1)
                                            {
                                               echo "<hr/>";
                                            }
                                            
                                            echo $yarn_type_value; 
                                            $d++;
                                         }
                                    ?>
                                </p>
                            </td>
							<td width="90" align="right"><?php echo number_format($job_mkt_required,2,'.',''); ?></td>
                            <td width="100" align="right"><?php echo number_format($job_mkt_required_cost,2,'.',''); ?></td>
                            <td width="90" align="right"><?php echo number_format($yarn_iss_qty,2,'.',''); ?></td>
                            <td align="right" style="padding-right:2px"><?php echo number_format($yarn_iss_cost,2,'.',''); ?></td>
						</tr>
					<?php
						$i++;
						$tot_mkt_required+=$job_mkt_required; 
						$tot_required_cost+=$job_mkt_required_cost;
						$tot_yarn_iss_qty+=$yarn_iss_qty; 
						$tot_yarn_iss_cost+=$yarn_iss_cost; 
					}
					?>
                	<tfoot>
                        <th colspan="5">Total</th>
                        <th><?php echo $tot_po_qnty; ?></th>
                        <th colspan="3">&nbsp;</th>
                        <th><?php echo number_format($tot_mkt_required,2,'.',''); ?></th>
                        <th><?php echo number_format($tot_required_cost,2,'.',''); ?></th>
                        <th><?php echo number_format($tot_yarn_iss_qty,2,'.',''); ?></th>
                        <th><?php echo number_format($tot_yarn_iss_cost,2,'.',''); ?></th>
                    </tfoot>    
                </table>
            </div>
        </fieldset>
    </div>
<?php
	exit();
}

if($action=="knitting_cost")
{
	echo load_html_head_contents("Report Info","../../../../", 1, 1, $unicode,'','');
	extract($_REQUEST);
	
	$exchange_rate=76;
	$costing_per_arr=return_library_array( "select job_no, costing_per_id from wo_pre_cost_dtls where status_active=1 and is_deleted=0", "job_no", "costing_per_id");
	$buyer_arr=return_library_array( "select id, short_name from lib_buyer", "id", "short_name"  );
	
	$subconInBillDataArray=sql_select("select b.order_id, sum(b.amount) AS knit_bill, sum(b.delivery_qty) as qty from subcon_inbound_bill_mst a, subcon_inbound_bill_dtls b where a.id=b.mst_id and b.order_id in($po_id) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.party_source=1 and a.process_id=2 group by b.order_id");
	foreach($subconInBillDataArray as $subRow)
	{
		$subconCostArray[$subRow[csf('order_id')]]['amnt']=$subRow[csf('knit_bill')];
		$subconCostArray[$subRow[csf('order_id')]]['qty']=$subRow[csf('qty')];
	}	
	
	$subconOutBillDataArray=sql_select("select b.order_id, sum(b.amount) AS knit_bill, sum(b.receive_qty) as qty from subcon_outbound_bill_mst a, subcon_outbound_bill_dtls b where a.id=b.mst_id and a.process_id=2 and b.order_id in($po_id) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by b.order_id");
	foreach($subconOutBillDataArray as $subRow)
	{
		$subconCostArray[$subRow[csf('order_id')]]['amnt']+=$subRow[csf('knit_bill')];
		$subconCostArray[$subRow[csf('order_id')]]['qty']+=$subRow[csf('qty')];
	}
	
	$bookingArray=return_library_array( "select b.po_break_down_id, sum(b.grey_fab_qnty) as qnty from wo_booking_mst a, wo_booking_dtls b where a.booking_no=b.booking_no and b.po_break_down_id in($po_id) and a.item_category in(2,13) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by b.po_break_down_id", "po_break_down_id", "qnty");
	
	$greyProdArray=return_library_array( "select po_breakdown_id, sum(quantity) as qnty from order_wise_pro_details where po_breakdown_id in($po_id) and entry_form=2 and status_active=1 and is_deleted=0 group by po_breakdown_id", "po_breakdown_id", "qnty");
	
	$knitCostArray=return_library_array( "select job_no, sum(amount) AS knit_charge from wo_pre_cost_fab_conv_cost_dtls where cons_process=1 and status_active=1 and is_deleted=0 group by job_no", "job_no", "knit_charge");
?>
	<script>
    function print_window()
	{
		document.getElementById('scroll_body').style.overflow="auto";
		document.getElementById('scroll_body').style.maxHeight="none";
		
		var w = window.open("Surprise", "#");
		var d = w.document.open();
		d.write ('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN""http://www.w3.org/TR/html4/strict.dtd">'+
	'<html><head><link rel="stylesheet" href="../../../../css/style_common.css" type="text/css" /><title></title></head><body>'+document.getElementById('main_body').innerHTML+'</body</html>');
	
		d.close();
		document.getElementById('scroll_body').style.overflowY="scroll";
		document.getElementById('scroll_body').style.maxHeight="290px";
	}
	function openmypage_bill(po_id,type,tittle)
	{
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'post_costing_report_controller.php?po_id='+po_id+'&action='+type, tittle, 'width=560px, height=350px, center=1, resize=0, scrolling=0', '../../../');
	}
    </script>
    <input type="button" value="Print Preview" onClick="print_window()" style="width:100px"  class="formbutton"/>
    <div id="main_body" style="width:1050px;">
    
        <fieldset style="width:1050px;">
        	<table class="rpt_table" width="1030" cellpadding="0" cellspacing="0" border="1" rules="all">
            	<thead>
                	<th width="40">SL</th>
                    <th width="60">Buyer</th>
                    <th width="80">PO No</th>
                    <th width="50">Year</th>
                    <th width="60">Job No</th>
                    <th width="80">Style Name</th>
                    <th width="110">Gmts Item</th>
                    <th width="90">Order Qty</th>
                    <th width="80">Booking Qty</th>
                    <th width="80">Grey Prod.</th>
                    <th width="90">Knitting Cost</th>
                    <th width="80">Fabric Bill Qty</th>
                    <th>Bill Amount (USD)</th>
                </thead>
            </table>	
            <div style="width:1050px; max-height:290px; overflow-y:scroll" id="scroll_body">
                <table class="rpt_table" width="1030" cellpadding="0" cellspacing="0" border="1" rules="all">
                	<?php
					if($db_type==0) $year_field="YEAR(a.insert_date) as year"; 
					else if($db_type==2) $year_field="to_char(a.insert_date,'YYYY') as year";
					else $year_field="";//defined Later
						
					$sql="select a.job_no_prefix_num, a.job_no, $year_field, a.company_name, a.buyer_name, a.style_ref_no, a.order_uom, a.gmts_item_id, a.total_set_qnty as ratio, b.id, b.po_number, b.pub_shipment_date, b.po_quantity, b.unit_price, b.po_total_price, b.shiping_status from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.id in($po_id) order by b.pub_shipment_date";
					$result=sql_select($sql);
					$i=1; $tot_po_qnty=0; $tot_booking_qnty=0; $tot_greyProd_qnty=0; $tot_knitCost=0; $tot_knitbill=0; $tot_knitQty=0;
					foreach($result as $row)
					{
						if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
						$gmts_item='';
						$gmts_item_id=explode(",",$row[csf('gmts_item_id')]);
						foreach($gmts_item_id as $item_id)
						{
							if($gmts_item=="") $gmts_item=$garments_item[$item_id]; else $gmts_item.=",".$garments_item[$item_id];
						}
						
						$dzn_qnty=0;
						$costing_per_id=$costing_per_arr[$row[csf('job_no')]];
						if($costing_per_id==1)
						{
							$dzn_qnty=12;
						}
						else if($costing_per_id==3)
						{
							$dzn_qnty=12*2;
						}
						else if($costing_per_id==4)
						{
							$dzn_qnty=12*3;
						}
						else if($costing_per_id==5)
						{
							$dzn_qnty=12*4;
						}
						else
						{
							$dzn_qnty=1;
						}
						
						$dzn_qnty=$dzn_qnty*$row[csf('ratio')];
						$order_qnty_in_pcs=$row[csf('po_quantity')]*$row[csf('ratio')];
						$tot_po_qnty+=$order_qnty_in_pcs;
						$bookingQty=$bookingArray[$row[csf('id')]];
						$tot_booking_qnty+=$bookingQty;
						$greyProdQty=$greyProdArray[$row[csf('id')]];
						$tot_greyProd_qnty+=$greyProdQty;
						$knitCost=($order_qnty_in_pcs/$dzn_qnty)*$knitCostArray[$row[csf('job_no')]];
						$tot_knitCost+=$knitCost;
						$knitQty=$subconCostArray[$row[csf('id')]]['qty'];
						$knitbill=$subconCostArray[$row[csf('id')]]['amnt']/$exchange_rate;
						$tot_knitQty+=$knitQty;
						$tot_knitbill+=$knitbill;
						
					?>
						<tr bgcolor="<?php echo $bgcolor; ?>">
							<td width="40"><?php echo $i; ?></td>
							<td width="60" style="word-break:break-all;"><?php echo $buyer_arr[$row[csf('buyer_name')]]; ?></td>
                            <td width="80" style="word-break:break-all;"><?php echo $row[csf('po_number')]; ?></td>
                            <td width="50" align="center"><?php echo $row[csf('year')]; ?></td>
                            <td width="60" align="center"><?php echo $row[csf('job_no_prefix_num')]; ?></td>
                            <td width="80" style="word-break:break-all;"><?php echo $row[csf('style_ref_no')]; ?></td>
                            <td width="110" style="word-break:break-all;"><?php echo $gmts_item; ?></td>
                            <td width="90" align="right"><?php echo $order_qnty_in_pcs; ?></td>
							<td width="80" align="right"><?php echo number_format($bookingQty,2,'.',''); ?></td>
							<td width="80" align="right"><?php echo number_format($greyProdQty,2,'.',''); ?></td>
                            <td width="90" align="right"><?php echo number_format($knitCost,2,'.',''); ?></td>
							<td width="80" align="right"><?php echo number_format($knitQty,2,'.',''); ?></td>
							<td align="right"><a href="##" onClick="openmypage_bill('<?php echo $row[csf('id')]; ?>','knitting_bill','Knitting bill Details')"><?php echo number_format($knitbill,2,'.',''); ?></a></td>
						</tr>
					<?php
					$i++;
					}
					?>
                	<tfoot>
                        <th colspan="7">Total</th>
                        <th><?php echo $tot_po_qnty; ?></th>
                        <th><?php echo number_format($tot_booking_qnty,2,'.',''); ?></th>
                        <th><?php echo number_format($tot_greyProd_qnty,2,'.',''); ?></th>
                        <th><?php echo number_format($tot_knitCost,2,'.',''); ?></th>
                        <th><?php echo number_format($tot_knitQty,2,'.',''); ?></th>
                        <th><?php echo number_format($tot_knitbill,2,'.',''); ?></th>
                    </tfoot>    
                </table>
            </div>
        </fieldset>
    </div>
<?php
	exit();
}

if($action=="knitting_bill")
{
	echo load_html_head_contents("Report Info","../../../../", 1, 1, $unicode,'','');
	extract($_REQUEST);
	$exchange_rate=76;
	$costing_per_arr=return_library_array( "select job_no, costing_per_id from wo_pre_cost_dtls where status_active=1 and is_deleted=0", "job_no", "costing_per_id");
	
	$subconInBillDataArray=sql_select("select a.id as mst_id, a.bill_no, b.order_id, b.delivery_qty as qty, b.amount AS knit_bill, b.currency_id  from subcon_inbound_bill_mst a, subcon_inbound_bill_dtls b where a.id=b.mst_id and b.order_id in($po_id) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.party_source=1 and a.process_id=2");
	/*foreach($subconInBillDataArray as $subRow)
	{
		$subconCostArray[$subRow[csf('order_id')]]['amnt']=$subRow[csf('knit_bill')];
		$subconCostArray[$subRow[csf('order_id')]]['qty']=$subRow[csf('qty')];
	}*/	
	
	$subconOutBillDataArray=sql_select("select a.id as mst_id, a.bill_no, b.order_id, b.receive_qty as qty, b.amount AS knit_bill, b.currency_id from subcon_outbound_bill_mst a, subcon_outbound_bill_dtls b where a.id=b.mst_id and a.process_id=2 and b.order_id in($po_id) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 ");
	/*foreach($subconOutBillDataArray as $subRow)
	{
		$subconCostArray[$subRow[csf('order_id')]]['amnt']+=$subRow[csf('knit_bill')];
		$subconCostArray[$subRow[csf('order_id')]]['qty']+=$subRow[csf('qty')];
	}*/
	
	
	$bookingArray=return_library_array( "select b.po_break_down_id, sum(b.grey_fab_qnty) as qnty from wo_booking_mst a, wo_booking_dtls b where a.booking_no=b.booking_no and b.po_break_down_id in($po_id) and a.item_category in(2,13) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by b.po_break_down_id", "po_break_down_id", "qnty");
	
	$greyProdArray=return_library_array( "select po_breakdown_id, sum(quantity) as qnty from order_wise_pro_details where po_breakdown_id in($po_id) and entry_form=2 and status_active=1 and is_deleted=0 group by po_breakdown_id", "po_breakdown_id", "qnty");
	
	$knitCostArray=return_library_array( "select job_no, sum(amount) AS knit_charge from wo_pre_cost_fab_conv_cost_dtls where cons_process=1 and status_active=1 and is_deleted=0 group by job_no", "job_no", "knit_charge");
?>
	<script>
    function print_window()
	{
		document.getElementById('scroll_body').style.overflow="auto";
		document.getElementById('scroll_body').style.maxHeight="none";
		document.getElementById('scroll_body2').style.overflow="auto";
		document.getElementById('scroll_body2').style.maxHeight="none";
		
		var w = window.open("Surprise", "#");
		var d = w.document.open();
		d.write ('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN""http://www.w3.org/TR/html4/strict.dtd">'+
	'<html><head><link rel="stylesheet" href="../../../../css/style_common.css" type="text/css"/><title></title></head><body>'+document.getElementById('main_body').innerHTML+'</body</html>');
	
		d.close();
		document.getElementById('scroll_body').style.overflowY="scroll";
		document.getElementById('scroll_body').style.maxHeight="290px";
		document.getElementById('scroll_body2').style.overflowY="scroll";
		document.getElementById('scroll_body2').style.maxHeight="290px";
	}
    </script>
    <input type="button" value="Print Preview" onClick="print_window()" style="width:100px"  class="formbutton"/>
    <div id="main_body" style="width:470px;">
    
        <fieldset style="width:470px;">
        <legend>In Bound Bill</legend>
        	<table class="rpt_table" width="450" cellpadding="0" cellspacing="0" border="1" rules="all">
            	<thead>
                	<th width="50">SL</th>
                    <th width="150">Bill No</th>
                    <th width="100">Bill Quantity</th>
                    <th>Bill Amount (USD)</th>
                </thead>
            </table>	
            <div style="width:470px; max-height:290px; overflow-y:scroll" id="scroll_body">
                <table class="rpt_table" width="450" cellpadding="0" cellspacing="0" border="1" rules="all">
                	<?php
					$tot_bill_qnty=$tot_bill_amt=0;
					foreach($subconInBillDataArray as $row)
					{
						if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
						$knitbill=$row[csf('knit_bill')]/$exchange_rate;
						?>
						<tr bgcolor="<?php echo $bgcolor; ?>">
							<td width="50"><?php echo $i; ?></td>
							<td width="150" style="word-break:break-all;"><?php echo $row[csf('bill_no')]; ?></td>
                            <td width="100" align="right"><?php echo number_format($row[csf('qty')],0,' ',' '); $tot_bill_qnty+=$row[csf('qty')]; ?></td>
							<td align="right"><?php echo number_format($knitbill,2,'.',''); $tot_bill_amt+=$knitbill; ?></td>
						</tr>
						<?php
                        $i++;
					}
					?>
                	<tfoot>
                        <th colspan="2">Total</th>
                        <th><?php echo number_format($tot_bill_qnty,0,' ',' '); ?></th>
                        <th><?php echo number_format($tot_bill_amt,2,'.',''); ?></th>
                    </tfoot>    
                </table>
            </div>
            <br>
        <legend>Out Bound Bill</legend>
        	<table class="rpt_table" width="450" cellpadding="0" cellspacing="0" border="1" rules="all">
            	<thead>
                	<th width="50">SL</th>
                    <th width="150">Bill No</th>
                    <th width="100">Bill Quantity</th>
                    <th>Bill Amount (USD)</th>
                </thead>
            </table>	
            <div style="width:470px; max-height:290px; overflow-y:scroll" id="scroll_body2">
                <table class="rpt_table" width="450" cellpadding="0" cellspacing="0" border="1" rules="all">
                	<?php
					$tot_bill_qnty=$tot_bill_amt=0;
					foreach($subconOutBillDataArray as $row)
					{
						if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
						$knitbill=$row[csf('knit_bill')]/$exchange_rate;	
						?>
						<tr bgcolor="<?php echo $bgcolor; ?>">
							<td width="50"><?php echo $i; ?></td>
							<td width="150" style="word-break:break-all;"><?php echo $row[csf('bill_no')]; ?></td>
                            <td width="100" align="right"><?php echo number_format($row[csf('qty')],0,' ',' '); $tot_bill_qnty+=$row[csf('qty')]; ?></td>
							<td align="right"><?php echo number_format($knitbill,2,'.',''); $tot_bill_amt+=$knitbill; ?></td>
						</tr>
						<?php
                        $i++;
					}
					?>
                	<tfoot>
                        <th colspan="2">Total</th>
                        <th><?php echo number_format($tot_bill_qnty,0,' ',' '); ?></th>
                        <th><?php echo number_format($tot_bill_amt,2,'.',''); ?></th>
                    </tfoot>    
                </table>
            </div>
        </fieldset>
    </div>
<?php
	exit();
}

if($action=="dye_fin_cost")
{
	echo load_html_head_contents("Report Info","../../../../", 1, 1, $unicode,'','');
	extract($_REQUEST);
	
	$exchange_rate=76;
	$costing_per_arr=return_library_array( "select job_no, costing_per_id from wo_pre_cost_dtls where status_active=1 and is_deleted=0", "job_no", "costing_per_id");
	$buyer_arr=return_library_array( "select id, short_name from lib_buyer", "id", "short_name"  );
	
	$subconInBillDataArray=sql_select("select b.order_id, sum(b.amount) AS knit_bill, sum(b.delivery_qty) as qty from subcon_inbound_bill_mst a, subcon_inbound_bill_dtls b where a.id=b.mst_id and b.order_id in($po_id) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.party_source=1 and a.process_id=4 group by b.order_id");
	foreach($subconInBillDataArray as $subRow)
	{
		$subconCostArray[$subRow[csf('order_id')]]['amnt']=$subRow[csf('knit_bill')];
		$subconCostArray[$subRow[csf('order_id')]]['qty']=$subRow[csf('qty')];
	}	
	
	$subconOutBillDataArray=sql_select("select b.order_id, sum(b.amount) AS knit_bill, sum(b.receive_qty) as qty from subcon_outbound_bill_mst a, subcon_outbound_bill_dtls b where a.id=b.mst_id and a.process_id=4 and b.order_id in($po_id) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by b.order_id");
	foreach($subconOutBillDataArray as $subRow)
	{
		$subconCostArray[$subRow[csf('order_id')]]['amnt']+=$subRow[csf('knit_bill')];
		$subconCostArray[$subRow[csf('order_id')]]['qty']+=$subRow[csf('qty')];
	}
	
	$bookingArray=return_library_array( "select b.po_break_down_id, sum(b.fin_fab_qnty) as qnty from wo_booking_mst a, wo_booking_dtls b where a.booking_no=b.booking_no and b.po_break_down_id in($po_id) and a.item_category in(2,13) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by b.po_break_down_id", "po_break_down_id", "qnty");
	
	$finProdArray=return_library_array( "select po_breakdown_id, sum(quantity) as qnty from order_wise_pro_details where po_breakdown_id in($po_id) and entry_form=7 and status_active=1 and is_deleted=0 group by po_breakdown_id", "po_breakdown_id", "qnty");
	
	$finCostArray=return_library_array( "select job_no, sum(amount) AS dye_fin_charge from wo_pre_cost_fab_conv_cost_dtls where cons_process not in(1,2,30,35) and status_active=1 and is_deleted=0 group by job_no", "job_no", "dye_fin_charge");

?>
    <div>
        <fieldset style="width:1033px;">
        	<table class="rpt_table" width="1030" cellpadding="0" cellspacing="0" border="1" rules="all">
            	<thead>
                	<th width="40">SL</th>
                    <th width="60">Buyer</th>
                    <th width="80">PO No</th>
                    <th width="50">Year</th>
                    <th width="60">Job No</th>
                    <th width="80">Style Name</th>
                    <th width="110">Gmts Item</th>
                    <th width="90">Order Qty</th>
                    <th width="80">Booking Qty</th>
                    <th width="80">Finish Prod.</th>
                    <th width="90">Dye & Fin Cost</th>
                    <th width="80">Fabric Bill Qty</th>
                    <th>Bill Amount (USD)</th>
                </thead>
            </table>	
            <div style="width:1030px; max-height:290px; overflow-y:scroll" id="scroll_body">
                <table class="rpt_table" width="1010" cellpadding="0" cellspacing="0" border="1" rules="all">
                	<?php
					if($db_type==0) $year_field="YEAR(a.insert_date) as year"; 
					else if($db_type==2) $year_field="to_char(a.insert_date,'YYYY') as year";
					else $year_field="";//defined Later
						
					$sql="select a.job_no_prefix_num, a.job_no, $year_field, a.company_name, a.buyer_name, a.style_ref_no, a.order_uom, a.gmts_item_id, a.total_set_qnty as ratio, b.id, b.po_number, b.pub_shipment_date, b.po_quantity, b.unit_price, b.po_total_price, b.shiping_status from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and b.id in($po_id) order by b.pub_shipment_date";
					$result=sql_select($sql);
					$i=1; $tot_po_qnty=0; $tot_booking_qnty=0; $tot_finProd_qnty=0; $tot_finCost=0; $tot_finbill=0; $tot_knitQty=0;
					foreach($result as $row)
					{
						if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
						$gmts_item='';
						$gmts_item_id=explode(",",$row[csf('gmts_item_id')]);
						foreach($gmts_item_id as $item_id)
						{
							if($gmts_item=="") $gmts_item=$garments_item[$item_id]; else $gmts_item.=",".$garments_item[$item_id];
						}
						
						$dzn_qnty=0;
						$costing_per_id=$costing_per_arr[$row[csf('job_no')]];
						if($costing_per_id==1)
						{
							$dzn_qnty=12;
						}
						else if($costing_per_id==3)
						{
							$dzn_qnty=12*2;
						}
						else if($costing_per_id==4)
						{
							$dzn_qnty=12*3;
						}
						else if($costing_per_id==5)
						{
							$dzn_qnty=12*4;
						}
						else
						{
							$dzn_qnty=1;
						}
						
						$dzn_qnty=$dzn_qnty*$row[csf('ratio')];
						$order_qnty_in_pcs=$row[csf('po_quantity')]*$row[csf('ratio')];
						$tot_po_qnty+=$order_qnty_in_pcs;
						$bookingQty=$bookingArray[$row[csf('id')]];
						$tot_booking_qnty+=$bookingQty;
						$finProdQty=$finProdArray[$row[csf('id')]];
						$tot_finProd_qnty+=$finProdQty;
						$finCost=($order_qnty_in_pcs/$dzn_qnty)*$finCostArray[$row[csf('job_no')]];
						$tot_finCost+=$finCost;
						$finQty=$subconCostArray[$row[csf('id')]]['qty'];
						$finbill=$subconCostArray[$row[csf('id')]]['amnt']/$exchange_rate;
						$tot_finQty+=$finQty;
						$tot_finbill+=$finbill;
						
					?>
						<tr bgcolor="<?php echo $bgcolor; ?>">
							<td width="40"><?php echo $i; ?></td>
							<td width="60" style="word-break:break-all;"><?php echo $buyer_arr[$row[csf('buyer_name')]]; ?></td>
                            <td width="80" style="word-break:break-all;"><?php echo $row[csf('po_number')]; ?></td>
                            <td width="50" align="center"><?php echo $row[csf('year')]; ?></td>
                            <td width="60" align="center"><?php echo $row[csf('job_no_prefix_num')]; ?></td>
                            <td width="80" style="word-break:break-all;"><?php echo $row[csf('style_ref_no')]; ?></td>
                            <td width="110" style="word-break:break-all;"><?php echo $gmts_item; ?></td>
                            <td width="90" align="right"><?php echo $order_qnty_in_pcs; ?></td>
							<td width="80" align="right"><?php echo number_format($bookingQty,2,'.',''); ?></td>
							<td width="80" align="right"><?php echo number_format($finProdQty,2,'.',''); ?></td>
                            <td width="90" align="right"><?php echo number_format($finCost,2,'.',''); ?></td>
							<td width="80" align="right"><?php echo number_format($finQty,2,'.',''); ?></td>
							<td align="right"><?php echo number_format($finbill,2,'.',''); ?></td>
						</tr>
					<?php
					$i++;
					}
					?>
                	<tfoot>
                        <th colspan="7">Total</th>
                        <th><?php echo $tot_po_qnty; ?></th>
                        <th><?php echo number_format($tot_booking_qnty,2,'.',''); ?></th>
                        <th><?php echo number_format($tot_finProd_qnty,2,'.',''); ?></th>
                        <th><?php echo number_format($tot_finCost,2,'.',''); ?></th>
                        <th><?php echo number_format($tot_finQty,2,'.',''); ?></th>
                        <th><?php echo number_format($tot_finbill,2,'.',''); ?></th>
                    </tfoot>    
                </table>
            </div>
        </fieldset>
    </div>
<?php
	exit();
}

disconnect($con);
?>