<?php
/*--- ----------------------------------------- Comments
Purpose			: 	This form will create Out-Side Finishing Bill Entry						
Functionality	:	
JS Functions	:
Created by		:	Sohel 
Creation date 	: 	04-08-2013
Updated by 		: 		
Update date		: 	
Oracle Convert 	:	Kausar		
Convert date	: 	03-06-2014	   
QC Performed BY	:		
QC Date			:	
Comments		:
*/
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;
//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Out-Side Finishing Bill Entry", "../", 1,1, $unicode,1,'');
?>
<script>
	if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../logout.php"; 
	var permission='<?php echo $permission; ?>';

	var selected_id = new Array(); var selected_currency_id = new Array();
	function check_all_data()
	{
		var tbl_row_count = document.getElementById( 'list_view_issue' ).rows.length;
		tbl_row_count = tbl_row_count - 1;
		
		for( var i = 1; i <= tbl_row_count; i++ ) 
		{
			eval($('#tr_'+i).attr("onclick"));  
		}
	}

	function toggle( x, origColor )
	{
		//alert (x);
		var newColor = 'yellow';
		if ( x.style )
		{
			x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
		}
	}

	function js_set_value(id)
	{
		var str=id.split("_");
		toggle( document.getElementById( 'tr_' + str[0] ), '#FFFFFF' );
		if( jQuery.inArray(  str[0] , selected_id ) == -1) {
			
			selected_id.push( str[0] );
		}
		else {
			for( var i = 0; i < selected_id.length; i++ ) {
			if( selected_id[i] == str[0]  ) break;
		}
			selected_id.splice( i, 1 );
		}
		var id = ''; var currency = '';
		for( var i = 0; i < selected_id.length; i++ ) {
			id += selected_id[i] + ',';
		}
		id = id.substr( 0, id.length - 1 );
		
		$('#selected_id').val( id );
	}

	function fnc_outside_finishing_bill_entry( operation )
	{
		if( form_validation('cbo_company_id*txt_bill_date*cbo_supplier_company*cbo_bill_for*txtreceivedate_1*txtchallenno_1','Company Name*Bill Date*supplier company*bill for*receive date*challen no')==false)
		{
			return;
		}
			
		var tot_row=$('#outside_finishingbill_table tr').length;
		var data1="action=save_update_delete&operation="+operation+"&tot_row="+tot_row+get_submitted_data_string('txt_bill_no*cbo_company_id*cbo_location_name*txt_bill_date*cbo_supplier_company*cbo_bill_for*update_id',"../");
		//alert (data1);
		var data2='';
		for(var i=1; i<=tot_row; i++)
		{
			data2+=get_submitted_data_string('txtreceivedate_'+i+'*txtchallenno_'+i+'*ordernoid_'+i+'*txtstylename_'+i+'*txtpartyname_'+i+'*txtnumberroll_'+i+'*itemid_'+i+'*textwonumid_'+i+'*txtfabqnty_'+i+'*cbouom_'+i+'*txtrate_'+i+'*txtamount_'+i+'*txtremarks_'+i+'*reciveid_'+i+'*updateiddtls_'+i+'*delete_id',"../");
		}
		var data=data1+data2;
		//alert (data);
		freeze_window(operation);
		http.open("POST","requires/outside_finishing_bill_entry_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = fnc_outside_finishing_bill_entry_response;
	}

	function fnc_outside_finishing_bill_entry_response()
	{
		if(http.readyState == 4) 
		{
			//alert (http.responseText);//return;
			var response=trim(http.responseText).split('**');
			//if (response[0].length>2) reponse[0]=10;
			show_msg(response[0]);
			document.getElementById('update_id').value = response[1];
			document.getElementById('txt_bill_no').value = response[2];
			set_button_status(1, permission, 'fnc_outside_finishing_bill_entry',1);
			release_freezing();
		}
	}

	function openmypage_wonum()
	{ 
		if ( form_validation('txtreceivedate_1*txtchallenno_1','Receive Date*Challen No')==false )
		{
			return;
		}
		var data=document.getElementById('cbo_company_id').value+"_"+document.getElementById('cbo_supplier_company').value;
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe','requires/outside_finishing_bill_entry_controller.php?action=wonum_popup&data='+data,'Wo Popup', 'width=950px,height=400px,center=1,resize=1,scrolling=0','')
		
		emailwindow.onclose=function()
		{
			var theemail=this.contentDoc.getElementById("hidd_item_id") 
			var response=theemail.value.split('_');
			if (theemail.value!="")
			{
				freeze_window(5);
				//alert (response[0]);
				var tot_row=$('#outside_finishingbill_table tr').length;
				for(var k=1; k<=tot_row; k++)
				{
					document.getElementById('textwonumid_'+k).value=response[0];
					document.getElementById('textwonum_'+k).value=response[1];
					document.getElementById('txtrate_'+k).value=response[2];
				}
				exchenge_rate_val(response[2])
				release_freezing();
			}
		}
	}

	function exchenge_rate_val(rate)
	{
		var tot_row=$('#outside_finishingbill_table tr').length;
		var amount_total=0;
		for(var k=1; k<=tot_row; k++)
		{
			amount_total=(document.getElementById('txtfabqnty_'+k).value*1)*(rate*1);
			document.getElementById('txtamount_'+k).value=amount_total;
		}
		math_operation( "txt_tot_qnty", "txtfabqnty_", "+", tot_row );
		math_operation( "txt_tot_amount", "txtamount_", "+", tot_row );
	}

	function openmypage_outside_bill()
	{ 
		var data=document.getElementById('cbo_company_id').value+'_'+document.getElementById('cbo_supplier_company').value;
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe','requires/outside_finishing_bill_entry_controller.php?data='+data+'&action=outside_bill_popup','Outside Bill Popup', 'width=700px,height=400px,center=1,resize=1,scrolling=0','')
		
		emailwindow.onclose=function()
		{
			var theemail=this.contentDoc.getElementById("outside_bill_id") 
			if (theemail.value!="")
			{
				freeze_window(5);
				get_php_form_data( theemail.value, "load_php_data_to_form_outside_bill", "requires/outside_finishing_bill_entry_controller" );
				window_close(theemail.value);
				show_list_view(document.getElementById('cbo_company_id').value+'_'+document.getElementById('cbo_supplier_company').value+'_'+document.getElementById('update_id').value,'outside_finishing_info_list','outside_finishing_info_list','requires/outside_finishing_bill_entry_controller','set_all();');
	 
				set_button_status(1, permission, 'fnc_outside_finishing_bill_entry',1);
				set_all_onclick();
				release_freezing();
			}
		}
	}

	function set_all()
	{
		var old=document.getElementById('issue_id_all').value;
		if(old!="")
		{   
			old=old.split(",");
			for(var i=0; i<old.length; i++)
			{   
				js_set_value( old[i]+"_"+document.getElementById('currid'+old[i]).value ) 
			}
		}
	}


	function window_close( frm )
	{
		if ( !frm ) var frm='';
		 
		if ($('#update_id').val()!=frm)
			var issue_id=document.getElementById('issue_id_all').value;
		else
			var issue_id='';
		var data=document.getElementById('selected_id').value+"_"+issue_id+"_"+frm;
		//alert (data);
		var list_view_orders = return_global_ajax_value( data, 'load_php_dtls_form', '', 'requires/outside_finishing_bill_entry_controller');
		if(list_view_orders!='')
		{
			$("#outside_finishingbill_table tr").remove();
			$("#outside_finishingbill_table").append(list_view_orders);
		}
		var tot_row=$('#outside_finishingbill_table tr').length;
		math_operation( "txt_tot_qnty", "txtfabqnty_", "+", tot_row );
		math_operation( "txt_tot_amount", "txtamount_", "+", tot_row );
		set_all_onclick();
	}
	
	function amount_caculation(id)
	{
		var tot_amount='';
		tot_amount=(document.getElementById('txtfabqnty_'+id).value*1)*(document.getElementById('txtrate_'+id).value*1);
		document.getElementById('txtamount_'+id).value=tot_amount;
		math_operation( "txt_tot_amount", "txtamount_", "+", id );
	}

</script>
</head>
<body onLoad="set_hotkey()">
   <div align="center" style="width:100%;">
   <?php echo load_freeze_divs ("../",$permission);  ?>
    <form name="outfinishingbill_1" id="outfinishingbill_1"  autocomplete="off"  >
    <fieldset style="width:810px;">
    <legend>Finishing Bill Info </legend>
        <table cellpadding="0" cellspacing="2" width="100%">
            <tr>
                <td align="right" colspan="3"><strong>Bill No </strong></td>
                <td width="140" align="justify">
                    <input type="hidden" name="selected_id" id="selected_id" />
                    <input type="hidden" name="update_id" id="update_id" />
                    <input type="text" name="txt_bill_no" id="txt_bill_no" class="text_boxes" style="width:140px" placeholder="Double Click to Search" onDblClick="openmypage_outside_bill();" readonly tabindex="1" >
                </td>
             </tr>
             <tr>
                <td width="120" class="must_entry_caption">Company Name</td>
                <td width="150">
					<?php 
						echo create_drop_down( "cbo_company_id",150,"select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name","id,company_name", 1, "--Select Company--", $selected, "load_drop_down( 'requires/outside_finishing_bill_entry_controller', this.value, 'load_drop_down_location', 'location_td');load_drop_down( 'requires/outside_finishing_bill_entry_controller', this.value, 'load_drop_down_supplier_name', 'supplier_td');","","","","","",2);	
                    ?>
                </td>
                <td width="120">Location Name</td>                                              
                <td width="150" id="location_td">
					<?php 
						echo create_drop_down( "cbo_location_name", 150, $blank_array,"", 1, "--Select Location--", $selected,"","","","","","",3);
                    ?>
                </td>
                <td width="90" class="must_entry_caption">Bill Date</td>                                              
                <td width="140">
                    <input class="datepicker" type="text" style="width:140px" name="txt_bill_date" id="txt_bill_date" tabindex="4" />
                </td>
            </tr> 
            <tr>
                <td class="must_entry_caption">Supplier Name</td>
                <td id="supplier_td">
					<?php
						echo create_drop_down( "cbo_supplier_company", 150, $blank_array,"", 1, "-- Select supplier --", $selected, "",0,"","","","",5);
					?> 	
                </td>
                <td class="must_entry_caption">Bill For</td>
                <td>
					<?php
						echo create_drop_down( "cbo_bill_for", 150, $bill_for,"", 1, "-- Select Party --", $selected, "",0,"","","","",7);
                    ?> 
                </td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
        </fieldset>
        &nbsp;
        <fieldset style="width:880px;">
    	<legend>Finishing Bill Details </legend>
        <table  style="border:none; width:850px;" cellpadding="0" cellspacing="1" border="0" id="">
            <thead class="form_table_header">
                <th width="80" class="must_entry_caption">Receive Date </th>
                <th width="70" class="must_entry_caption">Challan No.</th>
                <th width="60">Order No.</th>
                <th width="80">Style</th>
                <th width="60">Buyer</th>
                <th width="40">N.O Roll</th>
                <th width="100">Fabric Des.</th>
                <th width="60">WO Num</th>
                <th width="60">Fabric Qty</th>
                <th width="50" class="must_entry_caption">UOM</th>
                <th width="40">Rate</th>
                <th width="60">Amount</th>
                <th>Remarks</th>
            </thead>
            <tbody id="outside_finishingbill_table">
                <tr align="center">				
                    <td>
                        <input type="hidden" name="updateiddtls_1" id="updateiddtls_1" style="width:70px">
                        <input type="date" name="txtreceivedate_1" id="txtreceivedate_1"  class="text_boxes" style="width:80px" readonly />									
                    </td>
                    <td>
                        <input type="text" name="txtchallenno_1" id="txtchallenno_1"  class="text_boxes" style="width:70px" readonly />							 
                    </td>
                    <td>
                        <input type="hidden" name="ordernoid_1" id="ordernoid_1" value="" style="width:60px">
                        <input type="text" name="txtorderno_1" id="txtorderno_1"  class="text_boxes" style="width:60px" readonly />										
                    </td>
                    <td>
                        <input type="text" name="txtstylename_1" id="txtstylename_1"  class="text_boxes" style="width:80px;" />
                    </td>
                    <td>
                        <input type="text" name="txtpartyname_1" id="txtpartyname_1"  class="text_boxes" style="width:60px" />								
                    </td>
                    <td>			
                        <input type="text" name="txtnumberroll_1" id="txtnumberroll_1" class="text_boxes" style="width:40px" readonly />							
                    </td>  
                    <td>
                        <input type="text" name="textfebricdesc_1" id="textfebricdesc_1"  class="text_boxes" style="width:100px" readonly/>
                    </td>
                    <td>
                        <input type="text" name="textwonum_1" id="textwonum_1"  class="text_boxes" style="width:60px" placeholder="Browse" onDblClick="openmypage_wonum();" readonly/>
                    </td>
                    <td>
                        <input type="text" name="txtfabqnty_1" id="txtfabqnty_1"  class="text_boxes_numeric" style="width:60px" readonly />
                    </td>
                    <td>
						<?php echo create_drop_down( "cbouom_1", 50, $unit_of_measurement,"", 0, "--Select UOM--",12,"",1,"" );?>
                    </td>
                    <td>
                        <input type="text" name="txtrate_1" id="txtrate_1"  class="text_boxes_numeric" style="width:40px" readonly />
                    </td>

                    <td>
                        <input type="text" name="txtamount_1" id="txtamount_1" class="text_boxes_numeric" style="width:60px"  readonly />
                    </td>
                    <td>
                        <input type="text" name="txtremarks_1" id="txtremarks_1"  class="text_boxes" style="width:80px" />
                    </td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                	<td width="80px">&nbsp;</td>
                    <td width="70px">&nbsp;</td>
                    <td width="60px">&nbsp;</td>
                    <td width="80px">&nbsp;</td>
                    <td width="60px">&nbsp;</td>
                    <td width="40px">&nbsp;</td>
                    <td width="100px" align="right"></td>
                     <td width="60px" align="right">Total Qty:</td>
                    <td width="60px">
                    	<input type="text" name="txt_tot_qnty" id="txt_tot_qnty"  class="text_boxes_numeric" style="width:60px" readonly disabled />
                    </td>
                    <td width="50px">&nbsp;</td>
                    <td width="40px" align="right">Total:</td>
                    <td width="60px">
                    	<input type="text" name="txt_tot_amount" id="txt_tot_amount"  class="text_boxes_numeric" style="width:60px" readonly  disabled/>
                    </td>
                    <td width="80px">&nbsp;</td>
                </tr>                
                <tr>
                    <td colspan="13" height="15" align="center"> </td>
                </tr>
                <tr>
                    <td colspan="13" align="center" class="button_container">
						<?php 
							echo load_submit_buttons($permission,"fnc_outside_finishing_bill_entry",0,0,"reset_form('outfinishingbill_1','outside_finishing_info_list','','','$(\'#bill_issue_table tr:not(:first)\').remove();')",1);
                        ?> 
                    </td>
                </tr>  
            </tfoot>                                                             
        </table>
        </fieldset> 
        <br>
        <div id="outside_finishing_info_list"></div>  
        </form>
         <div style="width:250px; margin-top:13px; margin-left:0px; float:left;">
        <div id="wonum_list_view"></div>
        </div>
     </div>         
</body>
<script src="../includes/functions_bottom.js" type="text/javascript"></script>
</html>
		
			