<?php
/*--- ----------------------------------------- Comments
Purpose			: 					
Functionality	:	
JS Functions	:
Created by		:	Kausar
Creation date 	: 	22-09-2013
Updated by 		: 		
Update date		: 		   
QC Performed BY	:		
QC Date			:
Comments		:
*/
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;
//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Production Scanning", "../", 1,1, $unicode,1,'');
?>
<script>
if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../logout.php"; 
var permission='<?php echo $permission; ?>';
var test=0;

<?php

	$data_array=sql_select("SELECT a.job_no,a.buyer_name,b.po_number,a.style_ref_no,a.company_name,b.id as bid,c.id as cid,c.size_number_id,c.color_number_id,c.item_number_id FROM wo_po_details_master a, wo_po_break_down b, wo_po_color_size_breakdown c WHERE a.job_no=b.job_no_mst and b.id=c.po_break_down_id and b.shiping_status<>3");
	$po_details_array=array();
	$po_id=array();
	foreach($data_array as $row)
	{
		$po_details_array[$row[csf("cid")]]['job_no']=$row[csf("job_no")];
		$po_details_array[$row[csf("cid")]]['buyer_name']=$row[csf("buyer_name")];
		$po_details_array[$row[csf("cid")]]['company_name']=$row[csf("company_name")];
		$po_details_array[$row[csf("cid")]]['style_ref_no']=$row[csf("style_ref_no")];
		$po_details_array[$row[csf("cid")]]['poid']=$row[csf("bid")];
		$po_details_array[$row[csf("cid")]]['size_number_id']=$row[csf("size_number_id")];
		$po_details_array[$row[csf("cid")]]['color_number_id']=$row[csf("color_number_id")];
		$po_details_array[$row[csf("cid")]]['item_number_id']=$row[csf("item_number_id")];
		$po_details_array[$row[csf("cid")]]['po_number']=$row[csf("po_number")];
		
		$po_id[]=$row[csf("cid")];
		$job_no[]=$row[csf("job_no")];
	}
	$jspo_details_array= json_encode($po_details_array);
	echo "var po_details_array = ". $jspo_details_array . ";\n";
	$po_ids=implode(",",$po_id);
	$job_nos="'".implode("','",$job_no)."'";
	
	$data_array=sql_select("select mst_id,production_type,color_size_break_down_id,production_qnty from  pro_garments_production_dtls where color_size_break_down_id in ($po_ids) ");
	$production_arr=array();
	$prod_arr=array();
	foreach($data_array as $row)
	{
		$production_arr[$row[csf("mst_id")]][$row[csf("color_size_break_down_id")]]=$row[csf("production_qnty")];
		$prod_arr[]=$row[csf("mst_id")];
	}
	$jsproduction_arr= json_encode($production_arr); 
	echo "var production_arr = ". $jsproduction_arr . ";\n"; 
	$prod_arrs=implode(",",$prod_arr);
	
	$data_array=sql_select("select a.id,body_part_id,operation_id,oparetion_type_id from ppl_gsd_entry_dtls a,ppl_gsd_entry_mst b where b.id=a.mst_id and po_job_no in ( $job_nos )");
	$gsd_arr=array();
	foreach($data_array as $row)
	{
		$gsd_arr[$row[csf("id")]]['body_part_id']=$row[csf("body_part_id")];
		$gsd_arr[$row[csf("id")]]['operation_id']=$row[csf("operation_id")];
	}
	$jsgsd_arr= json_encode($gsd_arr); 
	echo "var gsd_arr = ". $jsgsd_arr . ";\n"; 
	
	//echo "select a.op_code,a.bundle_dtls,a.bundle_mst,a.prod_mst,a.style,a.gsd_mst,a.gsd_dtls,b.pro_gmts_pro_id from  pro_operation_bar_code a, pro_bundle_mst b where a.id=b.bundle_mst and  a.prod_mst in ($po_ids) ";
	$data_array=sql_select("select a.op_code,a.bundle_dtls,a.bundle_mst,a.prod_mst,a.style,a.gsd_mst,a.gsd_dtls,b.pro_gmts_pro_id from  pro_operation_bar_code a, pro_bundle_mst b where b.id=a.bundle_mst and  a.prod_mst in ($po_ids) ");
	$operation_barcode=array(); 
	foreach($data_array as $row)
	{
		$operation_barcode[$row[csf("op_code")]]['bundle_dtls']=$row[csf("bundle_dtls")];
		$operation_barcode[$row[csf("op_code")]]['bundle_mst']=$row[csf("bundle_mst")];
		$operation_barcode[$row[csf("op_code")]]['prod_mst']=$row[csf("prod_mst")];
		$operation_barcode[$row[csf("op_code")]]['style']=$row[csf("style")];
		$operation_barcode[$row[csf("op_code")]]['gsd_mst']=$row[csf("gsd_mst")];
		$operation_barcode[$row[csf("op_code")]]['gsd_dtls']=$row[csf("gsd_dtls")];
		$operation_barcode[$row[csf("op_code")]]['pro_gmts_pro_id']=$row[csf("pro_gmts_pro_id")];
		//$bnd_mst_arr[]=$row[csf("bundle_mst")];
	}
	$operation_barcode= json_encode($operation_barcode);
	echo "var operation_barcode = ". $operation_barcode . ";\n";
	//$bnd_mst=implode(",",$bnd_mst_arr);
	 
?>

	 
	function openmypage_workercode()
	{ 
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe','requires/production_scanning_controller.php?action=worker_code_popup','Worker Code Popup', 'width=850px,height=350px,center=1,resize=1,scrolling=0','')
		
		emailwindow.onclose=function()
		{
			var theemail=this.contentDoc.getElementById("worker_id");
			var response=theemail.value.split('__');
			
			if (theemail.value!="")
			{
				freeze_window(5);
				//document.getElementById("update_id").value=response[0];
				document.getElementById("txt_worker_code").value=response[1];
				document.getElementById("txt_worker_name").value=response[2];
				//create_row();
				//get_php_form_data( response[0], "load_php_dtls_form", "requires/production_scanning_controller" );
				test=15;
				release_freezing();
			}
		}
	}

	function openmypage_operationname()
	{ 
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe','requires/production_scanning_controller.php?action=operation_body_popup','Operation Name & Body Part Popup', 'width=850px,height=350px,center=1,resize=1,scrolling=0','')
	
	
	}

	function fnc_production_scanning( operation )
	{
	/*	if( form_validation('txt_worker_code*txt_bar_code_num*txtbundlenum_1','Worker Code*Bar Code Number*Bundle Number')==false)
		{
			return; 
		}
	*/		var num_row =$('#scanning_tbl tbody tr').length; 
			
			var data1="action=save_update_delete&operation="+operation+"&num_row="+num_row+get_submitted_data_string('txt_worker_code*update_id',"../");
			var data2='';
			var i=1;
			for(var i=1; i<=num_row; i++)
			{
				data2+=get_submitted_data_string('operationbodyId_'+i+'*bundlenum_'+i+'*samperoperation_'+i+'*barcodeid_'+i,"../",i);
				//data2+=get_submitted_data_string('txtslnum_'+i+'*txtbarcode_'+i+'*txtopernamebody_'+i+'*txtbundlenum_'+i+'*txtprodqnty_'+i+'*txtcolorsize_'+i+'*txtsamperoperation_'+i+'*txtordnum_'+i+'*txtstyleref_'+i+'*txtgmtitem_'+i+'*txtbuyer_'+i+'*txtcompany_'+i,"../");
			}
			//alert (data2);return;
			var data=data1+data2;
			freeze_window(operation);
			http.open("POST","requires/production_scanning_controller.php",true);
			http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			http.send(data);
			http.onreadystatechange = fnc_production_scanning_response;
	
	}

	function fnc_production_scanning_response()
	{
		if(http.readyState == 4) 
		{
			var response=trim(http.responseText).split('**');
			//if (response[0].length>2) reponse[0]=10;
			show_msg(response[0]);
			document.getElementById('update_id').value = response[1];
			set_button_status(1, permission, 'fnc_production_scanning',1);
			release_freezing();
		}
	}
//document.onkeydown = create_row;
/*
jQuery(".text_boxes_barcode").keypress(function(e) {
	
	var c = String.fromCharCode(e.which);
 	var evt = (e) ? e : window.event;
    var key = (evt.keyCode) ? evt.keyCode : evt.which;
	if(key != null) key = parseInt(key, 10);
	var allowed = '1234567890.'; // ~ replace of Hash(#)
		if (isUserFriendlyChar(key)) return true
		else if (key != 8 && key !=0 && allowed.indexOf(c) < 0) 
			return false;
		else if (!numeric_valid( $(this).attr('id'), 0))
			return false;
	  
   
});
*/
	function create_row( )
	{
		
		var num_row =$('#scanning_tbl tbody tr').length; 
		var bar_code = document.getElementById('txt_bar_code_num').value;
		//alert(operation_barcode['13130000099041']['gsd_dtls'])
		if(!operation_barcode[bar_code]['gsd_dtls']){ return; }
		
		//document.getElementById('txt_bar_code_num').value='';
		var bar_code_id = document.getElementById('bar_code_id').value;
		var gsd=gsd_arr[operation_barcode[bar_code]['gsd_dtls']]['body_part_id']+"  "+gsd_arr[operation_barcode[bar_code]['gsd_dtls']]['operation_id'];
		var bundle_num=bar_code.substr(0,11);
		var prod_qnty=production_arr[operation_barcode[bar_code]['pro_gmts_pro_id']][operation_barcode[bar_code]['prod_mst']]
		var color=po_details_array[operation_barcode[bar_code]['prod_mst']]['color_number_id'];
		var size=po_details_array[operation_barcode[bar_code]['prod_mst']]['size_number_id'];
		var order=po_details_array[operation_barcode[bar_code]['prod_mst']]['po_number'];
		var buyer_name=po_details_array[operation_barcode[bar_code]['prod_mst']]['buyer_name'];
		var company_name=po_details_array[operation_barcode[bar_code]['prod_mst']]['company_name'];
		var style_ref_no=po_details_array[operation_barcode[bar_code]['prod_mst']]['style_ref_no'];
		var num_rowl=(num_row*1)+1;
		
		var new_row ='<tr><td width="50"><input type="text" name="txtslnum_'+num_rowl+'" id="txtslnum_'+num_rowl+'" value="'+num_rowl+'" class="text_boxes" style="width:42px;" readonly /></td><td width="100"><input type="text" name="txtbarcode_'+num_rowl+'" id="txtbarcode_'+num_rowl+'" class="text_boxes" style="width:90px;" value="'+bar_code+'"  readonly /><input type="hidden" id="barcodeid_'+num_rowl+'" /></td><td width="150"><input type="text" name="txtopernamebody_'+num_rowl+'" id="txtopernamebody_'+num_rowl+'" value="'+gsd+'" class="text_boxes" style="width:140px;" readonly /><input type="hidden" name="operationbodyId_'+num_rowl+'" id="operationbodyId_'+num_rowl+'" /></td><td width="100"><input type="text" name="txtbundlenum_'+num_rowl+'" value="'+bundle_num+'" id="txtbundlenum_'+num_rowl+'" class="text_boxes" style="width:90px;" readonly /><input type="hidden" name="bundlenum_'+num_rowl+'" id="bundlenum_'+num_rowl+'" /></td><td width="80"><input type="text" name="txtprodqnty_'+num_rowl+'" id="txtprodqnty_'+num_rowl+'" value="'+bundle_num+'" class="text_boxes" style="width:70px;" readonly /></td><td width="100"><input type="text" name="txtcolorsize_'+num_rowl+'" id="txtcolorsize_'+num_rowl+'" value="'+bundle_num+'" class="text_boxes" style="width:90px;" readonly /></td><td width="70"><input type="text" name="txtsamperoperation_'+num_rowl+'" value="'+bundle_num+'" id="txtsamperoperation_'+num_rowl+'" class="text_boxes" style="width:60px;" readonly /><input type="hidden" name="samperoperation_'+num_rowl+'" id="samperoperation_'+num_rowl+'" /></td><td width="100"><input type="text" name="txtordnum_'+num_rowl+'" id="txtordnum_'+num_rowl+'" value="'+bundle_num+'" class="text_boxes" style="width:90px;" readonly /></td><td width="100"><input type="text" name="txtstyleref_'+num_rowl+'" id="txtstyleref_'+num_rowl+'" value="'+bundle_num+'" class="text_boxes" style="width:90px;" readonly /></td><td width="100"><input type="text" name="txtgmtitem_'+num_rowl+'" value="'+bundle_num+'" id="txtgmtitem_'+num_rowl+'" class="text_boxes" style="width:90px;" readonly /></td><td width="100"><input type="text" name="txtbuyer_'+num_rowl+'" value="'+bundle_num+'" id="txtbuyer_'+num_rowl+'" class="text_boxes" style="width:90px;" readonly /></td><td><input type="text" name="txtcompany_'+num_rowl+'" value="'+bundle_num+'" id="txtcompany_'+num_rowl+'" class="text_boxes"  style="width:100px;" readonly /></td></tr>'; 
		
		//var new_row =new_row+'<tr><td width="50"><input type="text" name="txtslnum_'+num_row+'" id="txtslnum_'+num_row+'" value="'+num_row+'" class="text_boxes" style="width:42px;" readonly /></td><td width="100"><input type="text" name="txtbarcode_'+num_row+'" id="txtbarcode_'+num_row+'" class="text_boxes" style="width:90px;" value="'+bar_code+'"  readonly /><input type="hidden" id="barcodeid_'+num_row+'" /></td><td width="150"><input type="text" name="txtopernamebody_'+num_row+'" id="txtopernamebody_'+num_row+'" value="'+gsd+'" class="text_boxes" style="width:140px;" readonly /><input type="hidden" name="operationbodyId_'+num_row+'" id="operationbodyId_'+num_row+'" /></td><td width="100"><input type="text" name="txtbundlenum_'+num_row+'" value="'+bundle_num+'" id="txtbundlenum_'+num_row+'" class="text_boxes" style="width:90px;" readonly /><input type="hidden" name="bundlenum_'+num_row+'" id="bundlenum_'+num_row+'" /></td><td width="80"><input type="text" name="txtprodqnty_'+num_row+'" id="txtprodqnty_'+num_row+'" value="'+bundle_num+'" class="text_boxes" style="width:70px;" readonly /></td><td width="100"><input type="text" name="txtcolorsize_'+num_row+'" id="txtcolorsize_'+num_row+'" value="'+bundle_num+'" class="text_boxes" style="width:90px;" readonly /></td><td width="70"><input type="text" name="txtsamperoperation_'+num_row+'" value="'+bundle_num+'" id="txtsamperoperation_'+num_row+'" class="text_boxes" style="width:60px;" readonly /><input type="hidden" name="samperoperation_'+num_row+'" id="samperoperation_'+num_row+'" /></td><td width="100"><input type="text" name="txtordnum_'+num_row+'" id="txtordnum_'+num_row+'" value="'+bundle_num+'" class="text_boxes" style="width:90px;" readonly /></td><td width="100"><input type="text" name="txtstyleref_'+num_row+'" id="txtstyleref_'+num_row+'" value="'+bundle_num+'" class="text_boxes" style="width:90px;" readonly /></td><td width="100"><input type="text" name="txtgmtitem_'+num_row+'" value="'+bundle_num+'" id="txtgmtitem_'+num_row+'" class="text_boxes" style="width:90px;" readonly /></td><td width="100"><input type="text" name="txtbuyer_'+num_row+'" value="'+bundle_num+'" id="txtbuyer_'+num_row+'" class="text_boxes" style="width:90px;" readonly /></td><td><input type="text" name="txtcompany_'+num_row+'" value="'+bundle_num+'" id="txtcompany_'+num_row+'" class="text_boxes"  style="width:100px;" readonly /></td></tr>';  
		  
		//$('#scanning_tbl tbody tr:first').remove();
		 
		
		var old=$('#scanning_tbl tbody').html();
	//	$('#scanning_tbl tbody').html('');
		$('#scanning_tbl tbody').html(new_row+old);
		 
	}
	
$('#txt_bar_code_num').live('keydown', function(e) {
    
    if (e.keyCode === 13) {
        e.preventDefault();
        create_row();
    }
});

</script>
</head>

        
<body onLoad="set_hotkey()">
 <div align="center" style="width:100%;">
   <?php echo load_freeze_divs ("../",$permission); ?>
    <form name="prodscanning_1" id="prodscanning_1"  autocomplete="off"  >
	<fieldset style="width:880px;">
    <legend>Production Scanning</legend>
        <table cellpadding="0" cellspacing="2" width="100%">
        	<tr>
            	<td width="110px">Worker Code</td>
            	<td>
                	<input type="hidden" id="update_id" />
                	<input type="text" name="txt_worker_code" id="txt_worker_code" class="text_boxes" placeholder="Browse" style="width:140px;" onDblClick="openmypage_workercode();"  />
				</td>
            	<td width="110px">Worker Name</td>
            	<td>
                	<input type="text" name="txt_worker_name" id="txt_worker_name" class="text_boxes" style="width:140px;" readonly />
				</td>
            	<td width="110px">Designation</td>
            	<td>
                	<input type="text" name="txt_designation" id="txt_designation" class="text_boxes" style="width:140px;"  readonly />
				</td>
			</tr>
        	<tr>
            	<td width="110px">Line Number</td>
            	<td>
                	<input type="text" name="txt_line_num" id="txt_line_num" class="text_boxes" style="width:140px;"  readonly />
				</td>
            	<td width="110px">Location</td>
            	<td>
					<?php
						echo create_drop_down( "cbo_location_id",150,"select id,location_name from lib_location where status_active =1 and is_deleted=0 order by location_name","id,location_name", 1, "-- Select Location --", $selected,"load_drop_down( 'requires/production_scanning_controller', this.value, 'load_drop_down_floor', 'floor_td' );",0);
                    ?>
				</td>
            	<td width="110px">Floor Name</td>
            	<td id="floor_td">
					<?php
						echo create_drop_down( "cbo_floor_id",150,$blank_array,"", 1, "-- Select Floor --", 1, "","","","","","","");	
                    ?>
				</td>
			</tr>
            <tr>
            	<td colspan="6">&nbsp;</td>
            </tr>
            <tr>
                <td width="140" align="left"><strong>Bar Code Number</strong></td>
                <td colspan="3">
                    <input type="text" name="txt_bar_code_num" id="txt_bar_code_num" class="text_boxes" style="width:170px;" onBlur="create_row();"/>
                    <input type="hidden" id="bar_code_id" />
                </td>
            </tr>
            <tr>
            	<td colspan="6">&nbsp;</td>
            </tr>
        </table>
        </fieldset>
		<br>
        <fieldset style="width:1240px;">
         
        <table cellpadding="0" cellspacing="2" width="1220" class="rpt_table" id="scanning_tbl_top">
        	<thead>
            	<th width="50" align="center">SL. No</th>
                <th width="100" align="center">Bar Code</th>
                <th width="150" align="center">Operation Name & Body Part</th>
                <th width="100" align="center">Bundle Num</th>
                <th width="80" align="center">Prod. Qnty</th>
                <th width="100" align="center">Color & Size</th>
                <th width="70" align="center">SAM Per Operation</th>
                <th width="80" align="center">Ord. Num</th>
                <th width="100" align="center">Style Ref.</th>
                <th width="100" align="center">Gmt. Item</th>
                <th width="100" align="center">Buyer</th>
                <th align="center" width="100" >Company</th>
                <th align="center" ></th>
            </thead>
         </table>
         <div style="width:1220px; max-height:250px; min-height:150px; overflow:auto" align="left">
         <table cellpadding="0" cellspacing="2" width="1200"  border="0" id="scanning_tbl">
            <tbody>
            	<tr>
                	<td width="50" align="center">
                    	<input type="text" name="txtslnum_1" id="txtslnum_1" class="text_boxes" style="width:42px;" readonly />
                    </td>
                	<td width="100" align="center">
                    	<input type="text" name="txtbarcode_1" id="txtbarcode_1" class="text_boxes" style="width:90px;" readonly />
                        <input type="hidden" id="barcodeid_1"/>
                    </td>
                	<td width="150" align="center">
                    	<input type="text" name="txtopernamebody_1" id="txtopernamebody_1" class="text_boxes" style="width:140px;" readonly />
                        <input type="hidden" name="operationbodyId_1" id="operationbodyId_1" />
                    </td>
                	<td width="100" align="center">
                    	<input type="text" name="txtbundlenum_1" id="txtbundlenum_1" class="text_boxes" style="width:90px;" readonly />
                        <input type="hidden" name="bundlenum_1" id="bundlenum_1" />
                    </td>
                	<td width="80" align="center">
                    	<input type="text" name="txtprodqnty_1" id="txtprodqnty_1" class="text_boxes" style="width:70px;" readonly />
                    </td>
                	<td width="100" align="center">
                    	<input type="text" name="txtcolorsize_1" id="txtcolorsize_1" class="text_boxes" style="width:90px;" readonly />
                    </td>
                	<td width="70" align="center">
                    	<input type="text" name="txtsamperoperation_1" id="txtsamperoperation_1" class="text_boxes" style="width:60px;" readonly />
                        <input type="hidden" name="samperoperation_1" id="samperoperation_1" />
                    </td>
                	<td width="80" align="center">
                    	<input type="text" name="txtordnum_1" id="txtordnum_1" class="text_boxes" style="width:70px;" readonly />
                    </td>
                	<td width="100" align="center">
                    	<input type="text" name="txtstyleref_1" id="txtstyleref_1" class="text_boxes" style="width:90px;" readonly />
                    </td>
                	<td width="100" align="center">
                    	<input type="text" name="txtgmtitem_1" id="txtgmtitem_1" class="text_boxes" style="width:90px;" readonly />
                    </td>
                	<td width="100" align="center">
                    	<input type="text" name="txtbuyer_1" id="txtbuyer_1" class="text_boxes" style="width:90px;" readonly />
                    </td>
                	<td align="center"  width="100" >
                    	<input type="text" name="txtcompany_1" id="txtcompany_1" class="text_boxes" style="width:80px;" readonly />
                    </td>
                    <td align="right" >
                    	<input type="button" value=" - " class="formbutton" style="width:35px" />
                    </td>
                </tr>
            </tbody>
        </table>
        </div>
 	    <br>
        <table  style="border:none; width:920px;" cellpadding="0" cellspacing="1" border="0" id="">
            <tr>
                <td align="center" class="button_container">
                    <?php 
                        echo load_submit_buttons($permission,"fnc_production_scanning",0,0,"reset_form('prodscanning_1','','','','$(\'#scanning_tbl tr:not(:first)\').remove();')",1);
                    ?>
                </td>
            </tr>  
        </table>
    </fieldset>
    </form>
    </div>
</body>
<script src="../includes/functions_bottom.js" type="text/javascript"></script>
</html>
