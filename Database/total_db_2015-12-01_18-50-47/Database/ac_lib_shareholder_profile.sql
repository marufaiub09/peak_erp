-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ac_lib_shareholder_profile`
--

DROP TABLE IF EXISTS `ac_lib_shareholder_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ac_lib_shareholder_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_number` varchar(50) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `bo_account_id` varchar(50) DEFAULT NULL,
  `father_name` varchar(100) DEFAULT NULL,
  `mother_name` varchar(100) DEFAULT NULL,
  `profession` varchar(50) DEFAULT NULL,
  `organization` varchar(50) DEFAULT NULL,
  `designation` varchar(50) DEFAULT NULL,
  `national_id` varchar(30) DEFAULT NULL,
  `tin` varchar(50) DEFAULT NULL,
  `vat` varchar(50) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `present_plot_no` varchar(20) DEFAULT NULL,
  `present_level_no` varchar(20) DEFAULT NULL,
  `present_road_no` varchar(20) DEFAULT NULL,
  `present_block` varchar(20) DEFAULT NULL,
  `present_country` varchar(50) DEFAULT NULL,
  `present_province` varchar(50) DEFAULT NULL,
  `present_city` varchar(50) DEFAULT NULL,
  `present_zip_code` varchar(20) DEFAULT NULL,
  `permanent_plot_no` varchar(20) DEFAULT NULL,
  `permanent_level_no` varchar(20) DEFAULT NULL,
  `permanent_road_no` varchar(20) DEFAULT NULL,
  `permanent_block` varchar(20) DEFAULT NULL,
  `permanent_country` varchar(50) DEFAULT NULL,
  `permanent_province` varchar(50) DEFAULT NULL,
  `permanent_city` varchar(50) DEFAULT NULL,
  `permanent_zip_code` varchar(20) DEFAULT NULL,
  `inserted_by` int(3) DEFAULT NULL,
  `insert_date` datetime DEFAULT NULL,
  `updated_by` int(3) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `status_active` tinyint(1) DEFAULT '1' COMMENT '0=No; 1=Yes',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '0=No; 1=Yes',
  PRIMARY KEY (`id`),
  KEY `id_number` (`id_number`),
  KEY `name` (`name`),
  KEY `bo_account_id` (`bo_account_id`),
  KEY `status_active` (`status_active`),
  KEY `is_deleted` (`is_deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ac_lib_shareholder_profile`
--

LOCK TABLES `ac_lib_shareholder_profile` WRITE;
/*!40000 ALTER TABLE `ac_lib_shareholder_profile` DISABLE KEYS */;
/*!40000 ALTER TABLE `ac_lib_shareholder_profile` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:38
