
<?php
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

include('../../includes/common.php');

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

 //-------------------START ----------------------------------------
$company_arr=return_library_array( "select id, company_name from  lib_company",'id','company_name');
$location_arr=return_library_array( "select id, location_name from  lib_location",'id','location_name');
$buyer_array = return_library_array("select id, short_name from  lib_buyer","id","short_name");
$brand_details=return_library_array( "select id, brand_name from lib_brand", "id", "brand_name"  );
$yarn_count_details=return_library_array( "select id,yarn_count from lib_yarn_count", "id", "yarn_count"  );
	

if ($action=="load_drop_down_location")
{
	echo create_drop_down( "cbo_location_id", 120, "select id,location_name from lib_location where company_id='$data' and status_active =1 and is_deleted=0 order by location_name","id,location_name", 1, "-- Select Location --", 0, "" );
	exit();
}

if($action=="load_drop_down_buyer")
{
	echo create_drop_down( "cbo_buyer_id", 120, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name","id,buyer_name", 1, "-- Select Buyer --", $selected, "" );  
	exit();
}

$composition_arr=array();
$construction_arr=array();
$sql_deter="select a.id, a.construction, b.copmposition_id, b.percent from lib_yarn_count_determina_mst a, lib_yarn_count_determina_dtls b where a.id=b.mst_id";
$data_array=sql_select($sql_deter);
if(count($data_array)>0)
{
/*foreach( $data_array as $row )
{
	if(array_key_exists($row[csf('id')],$composition_arr))
	{
		$composition_arr[$row[csf('id')]]=$composition_arr[$row[csf('id')]].$composition[$row[csf('copmposition_id')]]." ".$row[csf('percent')]."% ";
	}
	else
	{
		$composition_arr[$row[csf('id')]]=$row[csf('construction')].", ".$composition[$row[csf('copmposition_id')]]." ".$row[csf('percent')]."% ";
	}
}*/
	foreach( $data_array as $row )
	{
		if(array_key_exists($row[csf('id')],$construction_arr))
		{
			$construction_arr[$row[csf('id')]]=$construction_arr[$row[csf('id')]];
		}
		else
		{
			$construction_arr[$row[csf('id')]]=$row[csf('construction')];
		}
		if(array_key_exists($row[csf('id')],$composition_arr))
		{
			$composition_arr[$row[csf('id')]]=$composition_arr[$row[csf('id')]];
		}
		else
		{
			$composition_arr[$row[csf('id')]]=$composition[$row[csf('copmposition_id')]]." ".$row[csf('percent')]."%";
		}
	}
}



if($action=='list_generate')
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process ));
	$permission=$_SESSION['page_permission'];
	$cbo_company_id=str_replace("'","",$cbo_company_id);
	$cbo_location_id=str_replace("'","",$cbo_location_id);
	$cbo_buyer_id=str_replace("'","",$cbo_buyer_id);
	$txt_prog_no=str_replace("'","",$txt_prog_no);
	$cbo_year=str_replace("'","",$cbo_year);
	$txt_job_no=str_replace("'","",$txt_job_no);
	$txt_ord_no=str_replace("'","",$txt_ord_no);
	$txt_date_from=str_replace("'","",$txt_date_from);
	$txt_date_to=str_replace("'","",$txt_date_to);
	$cbo_status=str_replace("'","",$cbo_status);
	$update_mst_id=str_replace("'","",$update_mst_id);
	//echo $txt_prog_no;die;
	
	if($txt_prog_no!="")
	{
		$program_cond="and a.booking_id='$txt_prog_no' and a.receive_basis=2";
	}
	else
	{
		$program_cond="";
	}
	if($txt_job_no!="")
	{
		if($cbo_year!="") $cbo_year="and year(a.insert_date)='$cbo_year'"; else $cbo_year="";
		$job_no_po_id=return_field_value("group_concat(distinct b.id) as po_id","wo_po_details_master a,  wo_po_break_down b","a.job_no=b.job_no_mst and a.job_no_prefix_num='$txt_job_no' and a.status_active=1 $cbo_year","po_id");
	}
	//echo $job_no_po_id;die;
	if($txt_ord_no!="")
	{
	$po_id=return_field_value("group_concat(id) as po_id","wo_po_break_down","po_number like '%$txt_ord_no' and status_active=1","po_id");
	}
	//echo $txt_date_from;
	if($cbo_location_id!=0) $location_cond="and a.location_id=$cbo_location_id"; else $location_cond=""; 
	if($cbo_buyer_id!=0) $buyer_cond="and a.buyer_id=$cbo_buyer_id"; else $buyer_cond="";
	if(trim($job_no_po_id)!="") $job_cond="and c.po_breakdown_id in(".trim($job_no_po_id).")"; else $job_cond="";
	if(trim($po_id)!="") $order_cond="and c.po_breakdown_id in(".$po_id.")"; else $order_cond="";
	if($txt_date_from!="" && $txt_date_to!="") $date_cond="and a.receive_date between '$txt_date_from' and '$txt_date_to'"; else $date_cond="";
	
	
	$sql="select 
				a.id,
				a.recv_number,
				a.receive_date,
				a.booking_id as prog_no,
				a.buyer_id,
				a.receive_basis,
				a.knitting_source,
				b.febric_description_id as determination_id,
				b.gsm,
				b.width as dia,
				b.prod_id,
				c.po_breakdown_id,
				c.quantity as quantity
			from
				inv_receive_master a,  pro_grey_prod_entry_dtls b, order_wise_pro_details c
			where
				a.id=b.mst_id and b.id=c.dtls_id and a.entry_form=2 and c.entry_form=2 and a.status_active=1 and a.is_deleted=0 and c.status_active=1 and c.is_deleted=0 and c.trans_type=1 and a.company_id=$cbo_company_id and c.po_breakdown_id!=0 $location_cond $program_cond $buyer_cond $job_cond $order_cond $date_cond order by a.id";
		
	//echo $sql;die;
	$result=sql_select($sql);
	$product_id=0;$po_break_id=0;$detarminate_id=0;$result_arr=array();
	foreach($result as $row)
	{
		if($product_id==0) $product_id=$row[csf("prod_id")]; else $product_id=$product_id.",".$row[csf("prod_id")];
		if($po_break_id==0) $po_break_id=$row[csf("po_breakdown_id")]; else $po_break_id=$po_break_id.",".$row[csf("po_breakdown_id")];
		if($detarmination_id==0) $detarmination_id=$row[csf("detarmination_id")]; else $detarmination_id=$detarmination_id.",".$row[csf("detarmination_id")];
		
		$result_arr[$row[csf("id")]][$row[csf("prod_id")]][$row[csf("po_breakdown_id")]]['prod_id']= $row[csf("prod_id")];
		$result_arr[$row[csf("id")]][$row[csf("prod_id")]][$row[csf("po_breakdown_id")]]['po_breakdown_id']= $row[csf("po_breakdown_id")];
		$result_arr[$row[csf("id")]][$row[csf("prod_id")]][$row[csf("po_breakdown_id")]]['receive_id']= $row[csf("id")];
		$result_arr[$row[csf("id")]][$row[csf("prod_id")]][$row[csf("po_breakdown_id")]]['recv_number']= $row[csf("recv_number")];
		$result_arr[$row[csf("id")]][$row[csf("prod_id")]][$row[csf("po_breakdown_id")]]['receive_date']= $row[csf("receive_date")];
		$result_arr[$row[csf("id")]][$row[csf("prod_id")]][$row[csf("po_breakdown_id")]]['receive_basis']= $row[csf("receive_basis")];
		$result_arr[$row[csf("id")]][$row[csf("prod_id")]][$row[csf("po_breakdown_id")]]['knitting_source']= $row[csf("knitting_source")];
		$result_arr[$row[csf("id")]][$row[csf("prod_id")]][$row[csf("po_breakdown_id")]]['prog_no']= $row[csf("prog_no")];
		$result_arr[$row[csf("id")]][$row[csf("prod_id")]][$row[csf("po_breakdown_id")]]['buyer_id']= $row[csf("buyer_id")];
		$result_arr[$row[csf("id")]][$row[csf("prod_id")]][$row[csf("po_breakdown_id")]]['detarmination_id']= $row[csf("determination_id")];
		$result_arr[$row[csf("id")]][$row[csf("prod_id")]][$row[csf("po_breakdown_id")]]['gsm']= $row[csf("gsm")];
		$result_arr[$row[csf("id")]][$row[csf("prod_id")]][$row[csf("po_breakdown_id")]]['dia_width']= $row[csf("dia")];
		$result_arr[$row[csf("id")]][$row[csf("prod_id")]][$row[csf("po_breakdown_id")]]['current_stock'] += $row[csf("quantity")];
		
	}
	//echo $sql;
	//print_r($result_arr);
					
	
	/*echo "select 
					b.id as po_id,
					b.po_number,
					a.job_no,
					year(a.insert_date) as job_year
				from
					wo_po_details_master a, wo_po_break_down b
				where
					a.job_no=b.job_no_mst  and b.id in($po_break_id)";*/
	
	$job_po_arr=array();
	$sql_job=sql_select("select 
					b.id as po_id,
					b.po_number,
					a.job_no,
					a.job_no_prefix_num,
					year(a.insert_date) as job_year
				from
					wo_po_details_master a, wo_po_break_down b
				where
					a.job_no=b.job_no_mst  and b.id in($po_break_id)");
	
	foreach($sql_job as $row)
	{
		$job_po_arr[$row[csf("po_id")]]["po_id"]=$row[csf("po_id")];
		$job_po_arr[$row[csf("po_id")]]["job_no"]=$row[csf("job_no_prefix_num")];
		$job_po_arr[$row[csf("po_id")]]["po_number"]=$row[csf("po_number")];
		$job_po_arr[$row[csf("po_id")]]["job_year"]=$row[csf("job_year")];
	}
	
	$update_row_check=array();
	if($update_mst_id!="")
	{
		$sql_update=sql_select("select id,grey_sys_id,product_id,job_no,order_id,current_delivery,roll from pro_grey_prod_delivery_dtls where mst_id=$update_mst_id");
		foreach($sql_update as $row)
		{
			$update_row_check[$row[csf("grey_sys_id")]."*".$row[csf("product_id")]."*".$row[csf("order_id")]]["current_delivery"] =$row[csf("current_delivery")];
			$update_row_check[$row[csf("grey_sys_id")]."*".$row[csf("product_id")]."*".$row[csf("order_id")]]["id"] =$row[csf("id")];
			$update_row_check[$row[csf("grey_sys_id")]."*".$row[csf("product_id")]."*".$row[csf("order_id")]]["roll"] =$row[csf("roll")];
		}
	}
	
	//var_dump($update_row_check);
	ob_start();
	?>

<div style="width:1420px;" id="">
	<div id="report_print" style="width:1310px;">
    <table width="1400" class="rpt_table" id="tbl_header" cellpadding="0" cellspacing="1" rules="all">
    	<thead>
        	<th width="30">Sl</th>
            <th width="120">System Id</th>
            <th width="90">Progm/ Booking No</th>
            <th width="75">Knitting Source</th>
            <th width="70">Prd. date</th>
            <th width="50">Prod. Id</th>
            <th width="40">Year</th>
            <th width="60">Job No</th>
            <th width="50">Buyer</th>
            <th width="90">Order No</th>
            <th width="110">Construction </th>
            <th width="110">Composition</th>
            <th width="40">GSM</th>
            <th width="40">Dia</th>
            <th width="90">Prod. qty</th>
            <th width="90">Total Delivery</th>
            <th width="90">Balance</th>
            <th width="75" >Current Delv.</th>
            <th >Roll</th>
        </thead>
    </table>
    <div style="width:1420px; overflow-y:scroll; max-height:200px;font-size:12px; overflow-x:hidden;" id="scroll_body">
        <form name="delivery_details" id="delivery_details" autocomplete="off" > 
    <table width="1400" class="rpt_table" id="table_body" cellpadding="0" cellspacing="1" rules="all">
    	<tbody>
        <?php
		//var_dump($update_mst_id);
		$i=1;
		$current_row_array=array();
		foreach($result_arr as $recev_key=>$value)
		{
			foreach($value as $prod_key=>$val)
			{
				foreach($val as $po_key=>$row)
				{
					if ($i%2==0)
					$bgcolor="#E9F3FF";
					else
					$bgcolor="#FFFFFF";
					$tot_delivery=return_field_value("sum(b.current_delivery) as current_delivery","pro_grey_prod_delivery_dtls b","b.grey_sys_id='".$row["receive_id"]."' and b.product_id='".$row["prod_id"]."' and b.order_id='".$row["po_breakdown_id"]."'","current_delivery");
					
					$index_pk=$row[csf("receive_id")]."*".$row[csf("prod_id")]."*".$row["po_breakdown_id"];
					if($update_mst_id=="")
					{
						if($cbo_status==1)
						{
							//echo $tot_delivery;die;
							if($row["current_stock"]>$tot_delivery)
							{
								//$row["current_stock"];die;
								?>
								<tr bgcolor="<?php echo $bgcolor; ?>"<?php if($update_row_check[$index_pk]["id"]){?> style="background-color:#FF6;" <?php }?>>
									<td width="30" align="center"><p><?php echo $i; ?>
									<input type="hidden" id="hidesysid_<?php echo $i;?>" name="hidesysid_<?php echo $i;?>" value="<?php echo $row["receive_id"];?>"  />&nbsp;
									</p></td>
									<td width="120"><p><input type="hidden" id="hidesysnum_<?php echo $i;?>" name="hidesysnum_<?php echo $i;?>" value="<?php echo $row["recv_number"];?>"  />
									<?php
									 echo $row["recv_number"]; 
									?>&nbsp;
									</p></td>
									<td width="90" align="center"><p>
									<input type="hidden" id="hideprogrum_<?php echo $i;?>" name="hideprogrum_<?php echo $i;?>" value="<?php echo $row["recv_number"];?>"  />
									<?php if($row["prog_no"]==0) echo "Independent"; else echo $row["prog_no"]; ?>&nbsp;
									</p></td>
									<td width="75" align="center"><p>
									<?php
									  if($row["knitting_source"]==1)  echo "In-House"; if($row["knitting_source"]==3) echo "Sub-Contract"; 
									?>&nbsp;
									</p></td>
									<td width="70" align="center"><p><?php  if($row["receive_date"]!='0000-00-00')  echo change_date_format($row["receive_date"]); else echo ""; ?>&nbsp;</p></td>
									<td width="50" align="center"><p>
									<input type="hidden" id="hideprodid_<?php echo $i;?>" name="hideprodid_<?php echo $i;?>" value="<?php echo $row["prod_id"];?>"  />
									<?php echo $row["prod_id"]; ?>&nbsp;
									</p></td>
									<td width="40" align="center"><p><?php echo $job_po_arr[$po_key]["job_year"]; ?>&nbsp;</p></td>
									<td width="60" align="center"><p>
									<input type="hidden" id="hidejob_<?php echo $i;?>" name="hidejob_<?php echo $i;?>" value="<?php echo $job_po_arr[$po_key]["job_no"];?>"  />
									<?php echo $job_po_arr[$po_key]["job_no"]; ?>&nbsp;
									</p></td>
									<td width="50"><p><?php echo $buyer_array[$row["buyer_id"]]; ?>&nbsp;</p></td>
									<td width="90"><p>
									<input type="hidden" id="hideorder_<?php echo $i;?>" name="hideorder_<?php echo $i;?>" value="<?php echo $row["po_breakdown_id"];?>"  />
									<?php echo $job_po_arr[$po_key]["po_number"]; ?>&nbsp;
									</p></td>
									<td width="110"><p>
									 <input type="hidden" id="hideconstruction_<?php echo $i;?>" name="hideconstruction_<?php echo $i;?>" value="<?php echo $construction_arr[$row["detarmination_id"]];?>"  />
									<?php echo $construction_arr[$row["detarmination_id"]]; ?>&nbsp;
									</p></td>
									<td width="110"><p>
									<input type="hidden" id="hidecomposition_<?php echo $i;?>" name="hidecomposition_<?php echo $i;?>" value="<?php echo $composition_arr[$row["detarmination_id"]];?>"  />
									<?php echo $composition_arr[$row["detarmination_id"]]; ?>&nbsp;
									</p></td>
									<td width="40" align="center"><p>
									 <input type="hidden" id="hidegsm_<?php echo $i;?>" name="hidegsm_<?php echo $i;?>" value="<?php echo $row["gsm"]; ?>"  />
									<?php echo $row["gsm"]; ?>&nbsp;
									</p></td>
									<td width="40" align="center"><p>
									 <input type="hidden" id="hidedia_<?php echo $i;?>" name="hidedia_<?php echo $i;?>" value="<?php echo $row["dia_width"]; ?>"  />
									<?php echo $row["dia_width"]; ?>&nbsp;
									</p></td>
									<td width="90" align="right" ><p><?php echo number_format($row["current_stock"],2); $total_stock+=$row["current_stock"]; ?>&nbsp;</p></td>
									<td width="90" align="right" ><p>
									<?php
									
									if($update_row_check[$index_pk]["id"])
									{
										$tot_delivery=$tot_delivery-$update_row_check[$index_pk]["current_delivery"]; 
									}
									else
									{
										$tot_delivery=$tot_delivery;
									} 
									echo number_format($tot_delivery,2);
									$gt_tot_delivery+=$tot_delivery;
									//echo $job_po_arr[$po_key]["po_id"];
									?>&nbsp;</p>
									</td>
									<td width="90"align="right" id="totalqtyTd_<?php echo $i; ?>"><p><?php $balance=($row["current_stock"]-$tot_delivery); echo number_format($balance,2); $total_balance+=$balance; ?>&nbsp;</p></td>
									<td width="75">
									<p><input type="hidden" id="hiddendtlsid_<?php echo $i;?>" name="hiddendtlsid_<?php echo $i;?>"  style="width:60px;" value="<?php if($update_row_check[$index_pk]["id"]) echo $update_row_check[$index_pk]["id"]; else echo " "; ?>" />
										<input type="text" id="txtcurrentdelivery_<?php echo $i;?>" name="txtcurrentdelivery_<?php echo $i;?>" class="text_boxes_numeric" style="width:60px;" value="<?php if($update_row_check[$index_pk]["id"]) echo $update_row_check[$index_pk]["current_delivery"]; ?>" onKeyUp="check_qty()"  />
									&nbsp;</p></td>
									<td ><p>
										<input type="text" id="txtroll_<?php echo $i;?>" name="txtroll_<?php echo $i;?>" class="text_boxes_numeric" style="width:50px;" value="<?php if($update_row_check[$index_pk]["id"]) echo $update_row_check[$index_pk]["roll"]; ?>"  />
									&nbsp;</p></td>
								</tr>
								<?php
								$i++;
							}
						}
						
						else
						{
							if($row["current_stock"]<=$tot_delivery)
							{
								?>
								<tr bgcolor="<?php echo $bgcolor; ?>"<?php if($update_row_check[$index_pk]["id"]){?> style="background-color:#FF6;" <?php }?>>
									<td width="30" align="center"><p><?php echo $i; ?>
									<input type="hidden" id="hidesysid_<?php echo $i;?>" name="hidesysid_<?php echo $i;?>" value="<?php echo $row["receive_id"];?>"  />&nbsp;
									</p></td>
									<td width="120"><p><input type="hidden" id="hidesysnum_<?php echo $i;?>" name="hidesysnum_<?php echo $i;?>" value="<?php echo $row["recv_number"];?>"  />
									<?php
									 echo $row["recv_number"]; 
									?>&nbsp;
									</p></td>
									<td width="90" align="center"><p>
									<input type="hidden" id="hideprogrum_<?php echo $i;?>" name="hideprogrum_<?php echo $i;?>" value="<?php echo $row["recv_number"];?>"  />
									<?php if($row["prog_no"]==0) echo "Independent"; else echo $row["prog_no"]; ?>&nbsp;
									</p></td>
									<td width="75" align="center"><p>
									<?php
									  if($row["knitting_source"]==1)  echo "In-House"; if($row["knitting_source"]==3) echo "Sub-Contract"; 
									?>&nbsp;
									</p></td>
									<td width="70" align="center"><p><?php  if($row["receive_date"]!='0000-00-00')  echo change_date_format($row["receive_date"]); else echo ""; ?>&nbsp;</p></td>
									<td width="50" align="center"><p>
									<input type="hidden" id="hideprodid_<?php echo $i;?>" name="hideprodid_<?php echo $i;?>" value="<?php echo $row["prod_id"];?>"  />
									<?php echo $row["prod_id"]; ?>&nbsp;
									</p></td>
									<td width="40" align="center"><p><?php echo $job_po_arr[$po_key]["job_year"]; ?>&nbsp;</p></td>
									<td width="60" align="center"><p>
									<input type="hidden" id="hidejob_<?php echo $i;?>" name="hidejob_<?php echo $i;?>" value="<?php echo $job_po_arr[$po_key]["job_no"];?>"  />
									<?php echo $job_po_arr[$po_key]["job_no"]; ?>&nbsp;
									</p></td>
									<td width="50"><p><?php echo $buyer_array[$row["buyer_id"]]; ?>&nbsp;</p></td>
									<td width="90"><p>
									<input type="hidden" id="hideorder_<?php echo $i;?>" name="hideorder_<?php echo $i;?>" value="<?php echo $row["po_breakdown_id"];?>"  />
									<?php echo $job_po_arr[$po_key]["po_number"]; ?>&nbsp;
									</p></td>
									<td width="110"><p>
									 <input type="hidden" id="hideconstruction_<?php echo $i;?>" name="hideconstruction_<?php echo $i;?>" value="<?php echo $construction_arr[$row["detarmination_id"]];?>"  />
									<?php echo $construction_arr[$row["detarmination_id"]]; ?>&nbsp;
									</p></td>
									<td width="110"><p>
									<input type="hidden" id="hidecomposition_<?php echo $i;?>" name="hidecomposition_<?php echo $i;?>" value="<?php echo $composition_arr[$row["detarmination_id"]];?>"  />
									<?php echo $composition_arr[$row["detarmination_id"]]; ?>&nbsp;
									</p></td>
									<td width="40" align="center"><p>
									 <input type="hidden" id="hidegsm_<?php echo $i;?>" name="hidegsm_<?php echo $i;?>" value="<?php echo $row["gsm"]; ?>"  />
									<?php echo $row["gsm"]; ?>&nbsp;
									</p></td>
									<td width="40" align="center"><p>
									 <input type="hidden" id="hidedia_<?php echo $i;?>" name="hidedia_<?php echo $i;?>" value="<?php echo $row["dia_width"]; ?>"  />
									<?php echo $row["dia_width"]; ?>&nbsp;
									</p></td>
									<td width="90" align="right" ><p><?php echo number_format($row["current_stock"],2); $total_stock+=$row["current_stock"]; ?>&nbsp;</p></td>
									<td width="90" align="right" ><p>
									<?php
									
									if($update_row_check[$index_pk]["id"])
									{
										$tot_delivery=$tot_delivery-$update_row_check[$index_pk]["current_delivery"]; 
									}
									else
									{
										$tot_delivery=$tot_delivery;
									} 
									echo number_format($tot_delivery,2);
									$gt_tot_delivery+=$tot_delivery;
									//echo $job_po_arr[$po_key]["po_id"];
									?>&nbsp;</p>
									</td>
									<td width="90"align="right" id="totalqtyTd_<?php echo $i; ?>"><p><?php $balance=($row["current_stock"]-$tot_delivery); echo number_format($balance,2); $total_balance+=$balance; ?>&nbsp;</p></td>
									<td width="75">
									<p><input type="hidden" id="hiddendtlsid_<?php echo $i;?>" name="hiddendtlsid_<?php echo $i;?>"  style="width:60px;" value="<?php if($update_row_check[$index_pk]["id"]) echo $update_row_check[$index_pk]["id"]; else echo " "; ?>" />
										<input type="text" id="txtcurrentdelivery_<?php echo $i;?>" name="txtcurrentdelivery_<?php echo $i;?>" class="text_boxes_numeric" style="width:60px;" value="<?php if($update_row_check[$index_pk]["id"]) echo $update_row_check[$index_pk]["current_delivery"]; ?>" onKeyUp="check_qty()"  />
									&nbsp;</p></td>
									<td ><p>
										<input type="text" id="txtroll_<?php echo $i;?>" name="txtroll_<?php echo $i;?>" class="text_boxes_numeric" style="width:50px;" value="<?php if($update_row_check[$index_pk]["id"]) echo $update_row_check[$index_pk]["roll"]; ?>"  />
									&nbsp;</p></td>
								</tr>
								<?php
								$i++;
							}
						}
					}
					else
					{
						
						?>
						<tr bgcolor="<?php echo $bgcolor; ?>"<?php if($update_row_check[$index_pk]["id"]){?> style="background-color:#FF6;" <?php }?>>
                            <td width="30" align="center"><p><?php echo $i; ?>
                            <input type="hidden" id="hidesysid_<?php echo $i;?>" name="hidesysid_<?php echo $i;?>" value="<?php echo $row["receive_id"];?>"  />&nbsp;
                            </p></td>
                            <td width="120"><p><input type="hidden" id="hidesysnum_<?php echo $i;?>" name="hidesysnum_<?php echo $i;?>" value="<?php echo $row["recv_number"];?>"  />
                            <?php
                            echo $row["recv_number"]; 
                            ?>&nbsp;
                            </p></td>
                            <td width="90" align="center"><p>
                            <input type="hidden" id="hideprogrum_<?php echo $i;?>" name="hideprogrum_<?php echo $i;?>" value="<?php echo $row["recv_number"];?>"  />
                            <?php if($row["prog_no"]==0) echo "Independent"; else echo $row["prog_no"]; ?>&nbsp;
                            </p></td>
                            <td width="75" align="center"><p>
                            <?php
                            if($row["knitting_source"]==1)  echo "In-House"; if($row["knitting_source"]==3) echo "Sub-Contract"; 
                            ?>&nbsp;
                            </p></td>
                            <td width="70" align="center"><p><?php  if($row["receive_date"]!='0000-00-00')  echo change_date_format($row["receive_date"]); else echo ""; ?>&nbsp;</p></td>
                            <td width="50" align="center"><p>
                            <input type="hidden" id="hideprodid_<?php echo $i;?>" name="hideprodid_<?php echo $i;?>" value="<?php echo $row["prod_id"];?>"  />
                            <?php echo $row["prod_id"]; ?>&nbsp;
                            </p></td>
                            <td width="40" align="center"><p><?php echo $job_po_arr[$po_key]["job_year"]; ?>&nbsp;</p></td>
                            <td width="60" align="center"><p>
                            <input type="hidden" id="hidejob_<?php echo $i;?>" name="hidejob_<?php echo $i;?>" value="<?php echo $job_po_arr[$po_key]["job_no"];?>"  />
                            <?php echo $job_po_arr[$po_key]["job_no"]; ?>&nbsp;
                            </p></td>
                            <td width="50"><p><?php echo $buyer_array[$row["buyer_id"]]; ?>&nbsp;</p></td>
                            <td width="90"><p>
                            <input type="hidden" id="hideorder_<?php echo $i;?>" name="hideorder_<?php echo $i;?>" value="<?php echo $row["po_breakdown_id"];?>"  />
                            <?php echo $job_po_arr[$po_key]["po_number"]; ?>&nbsp;
                            </p></td>
                            <td width="110"><p>
                            <input type="hidden" id="hideconstruction_<?php echo $i;?>" name="hideconstruction_<?php echo $i;?>" value="<?php echo $construction_arr[$row["detarmination_id"]];?>"  />
                            <?php echo $construction_arr[$row["detarmination_id"]]; ?>&nbsp;
                            </p></td>
                            <td width="110"><p>
                            <input type="hidden" id="hidecomposition_<?php echo $i;?>" name="hidecomposition_<?php echo $i;?>" value="<?php echo $composition_arr[$row["detarmination_id"]];?>"  />
                            <?php echo $composition_arr[$row["detarmination_id"]]; ?>&nbsp;
                            </p></td>
                            <td width="40" align="center"><p>
                            <input type="hidden" id="hidegsm_<?php echo $i;?>" name="hidegsm_<?php echo $i;?>" value="<?php echo $row["gsm"]; ?>"  />
                            <?php echo $row["gsm"]; ?>&nbsp;
                            </p></td>
                            <td width="40" align="center"><p>
                            <input type="hidden" id="hidedia_<?php echo $i;?>" name="hidedia_<?php echo $i;?>" value="<?php echo $row["dia_width"]; ?>"  />
                            <?php echo $row["dia_width"]; ?>&nbsp;
                            </p></td>
                            <td width="90" align="right" ><p><?php echo number_format($row["current_stock"],2); $total_stock+=$row["current_stock"]; ?>&nbsp;</p></td>
                            <td width="90" align="right" ><p>
                            <?php
                            
                            if($update_row_check[$index_pk]["id"])
                            {
                            $tot_delivery=$tot_delivery-$update_row_check[$index_pk]["current_delivery"]; 
                            }
                            else
                            {
                            $tot_delivery=$tot_delivery;
                            } 
                            echo number_format($tot_delivery,2);
                            $gt_tot_delivery+=$tot_delivery;
                            //echo $job_po_arr[$po_key]["po_id"];
                            ?>&nbsp;</p>
                            </td>
                            <td width="90"align="right" id="totalqtyTd_<?php echo $i; ?>"><p><?php $balance=($row["current_stock"]-$tot_delivery); echo number_format($balance,2); $total_balance+=$balance; ?>&nbsp;</p></td>
                            <td width="75">
                            <p><input type="hidden" id="hiddendtlsid_<?php echo $i;?>" name="hiddendtlsid_<?php echo $i;?>"  style="width:60px;" value="<?php if($update_row_check[$index_pk]["id"]) echo $update_row_check[$index_pk]["id"]; else echo " "; ?>" />
                            <input type="text" id="txtcurrentdelivery_<?php echo $i;?>" name="txtcurrentdelivery_<?php echo $i;?>" class="text_boxes_numeric" style="width:60px;" value="<?php if($update_row_check[$index_pk]["id"]) echo $update_row_check[$index_pk]["current_delivery"]; ?>" onKeyUp="check_qty()"  />
                            &nbsp;</p></td>
                            <td ><p>
                            <input type="text" id="txtroll_<?php echo $i;?>" name="txtroll_<?php echo $i;?>" class="text_boxes_numeric" style="width:50px;" value="<?php if($update_row_check[$index_pk]["id"]) echo $update_row_check[$index_pk]["roll"]; ?>"  />
                            &nbsp;</p></td>
						</tr>
						<?php
						$i++;
					}

					
				}
			}
			
		}
		?>
        </tbody>
	</table>
    </div>
    <table width="1400" class="rpt_table" id="tbl_footer" cellpadding="0" cellspacing="1" rules="all">
    	<tfoot>
        	<th colspan="14" align="right">Total:</th>
            <th width="90"><?php echo number_format($total_stock,2); ?></th>
            <th width="90"><?php echo number_format($gt_tot_delivery,2); ?></th>
            <th width="90"><?php echo number_format($total_balance,2); ?></th>
            <th width="75"></th>
            <th width="62"></th>
        </tfoot>
    </table>
    
    </div>
    <table width="1315" class="rpt_table" id="tbl_foot" cellpadding="0" cellspacing="1" rules="all">
    	<tr>
        	<td colspan="18" height="30" valign="middle" align="center" class="button_container">
				<?php 
                	echo load_submit_buttons( $permission, "fnc_prod_delivery",0,1 ,"",1) ; 
                ?>
            </td>
        </tr>
    </table>
    </form>
</div>
<!--<script src="../includes/functions_bottom.js" type="text/javascript"></script>-->
	<?php
foreach (glob("$user_id*.xls") as $filename) 
	{
		if( @filemtime($filename) < (time()-$seconds_old) )
		@unlink($filename);
	}
	//---------end------------//
	$name=time();
	$filename=$user_id."_".$name.".xls";
	$create_new_doc = fopen($filename, 'w');
	$is_created = fwrite($create_new_doc,ob_get_contents());
	$filename=$user_id."_".$name.".xls";
	echo "$total_data####$filename####$update_mst_id";
	
	exit();	
}







if ($action=="save_update_delete")
{
		
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process ));
	//echo $cbo_buyer_id;die;
	if($operation==0) // Insert Here----------------------------------------------------------
	{
		$con = connect();
		if($db_type==0)	{ mysql_query("BEGIN"); }
		//table lock here 	 
		//if( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0"; die;}
				
		//yarn issue master table entry here START---------------------------------------//	
 		if( str_replace("'","",$update_mst_id) == "" ) //new insert cbo_ready_to_approved
		{
			$id=return_next_id("id", " pro_grey_prod_delivery_mst", 1);		
			$new_mrr_number=explode("*",return_mrr_number( str_replace("'","",$cbo_company_id), '', 'GDS', date("Y",time()), 5, "select sys_number_prefix,sys_number_prefix_num from pro_grey_prod_delivery_mst where company_id=$cbo_company_id and YEAR(insert_date)=".date('Y',time())." order by id DESC", "sys_number_prefix", "sys_number_prefix_num" ));
		
		
			$field_array="id,sys_number_prefix,sys_number_prefix_num,sys_number,delevery_date,company_id,location_id,buyer_id,inserted_by,insert_date";
			$data_array="(".$id.",'".$new_mrr_number[1]."','".$new_mrr_number[2]."','".$new_mrr_number[0]."',".$txt_delevery_date.",".$cbo_company_id.",".$cbo_location_id.",".$cbo_buyer_id.",'".$_SESSION['logic_erp']['user_id']."','".$pc_date_time."')";
			$rID=sql_insert(" pro_grey_prod_delivery_mst",$field_array,$data_array,1); 
		}

			
		$field_array_dtls="id,mst_id,grey_sys_id,grey_sys_number,product_id,job_no,order_id,construction,composition,gsm,dia,current_delivery,roll,inserted_by,insert_date";
		$dtls_id=return_next_id("id", "pro_grey_prod_delivery_dtls", 1);
		$ref_dtls_id=return_next_id("id", "pro_grey_prod_delivery_dtls", 1);	
		$data_array="";
		$k=1;
		for($i=1;$i<=$total_row;$i++)
		{
			$sys_id="hidesysid_".$i;
			$hidesysnum="hidesysnum_".$i;
			$hideprodid="hideprodid_".$i;
			$hidejob="hidejob_".$i;
			$hideorder="hideorder_".$i;
			$hideconstruc="hideconstruction_".$i;
			$hidecomposit="hidecomposition_".$i;
			$hidegsm="hidegsm_".$i;
			$hidedia="hidedia_".$i;
			$txtcurrentdelivery="txtcurrentdelivery_".$i;
			$txt_roll="txtroll_".$i;
			//echo $$txtcurrentdelivery;die;
			if(str_replace("'","",$$txtcurrentdelivery)!="")
			{
				if($k!=1)$dtls_id=$dtls_id+1;
				if ($k!=1) $data_array_dtls .=",";
				$data_array_dtls	.="(".$dtls_id.",".$id.",".$$sys_id.",".$$hidesysnum.",".$$hideprodid.",".$$hidejob.",".$$hideorder.",".$$hideconstruc.",".$$hidecomposit.",".$$hidegsm.",".$$hidedia.",".$$txtcurrentdelivery.",".$$txt_roll.",'".$_SESSION['logic_erp']['user_id']."','".$pc_date_time."')";
				$k++;
			}
		}
		//echo $field_array_dtls."*".$data_array_dtls;die;
		$rID2=sql_insert("pro_grey_prod_delivery_dtls",$field_array_dtls,$data_array_dtls,1);
		
		
		if($db_type==0)
		{
			if($rID && $rID2 )
			{
				mysql_query("COMMIT");
				echo "0**".str_replace("'",'',$id)."**".str_replace("'",'',$new_mrr_number[0])."**".str_replace("'",'',$ref_dtls_id);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'",'',$id);
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
			echo "0**".$entry_id."**".str_replace("'",'',$id);
		}
		disconnect($con);
		//die;
		
	}
	else if($operation==1) // Update Here----------------------------------------------------------
	{
		$con = connect();
		if($db_type==0)	{ mysql_query("BEGIN"); }
		if( str_replace("'","",$update_mst_id) != "") 
		{
			$field_array_mst="delevery_date*company_id*location_id*buyer_id*updated_by*update_date*status_active*is_deleted";
			$data_array_mst="".$txt_delevery_date."*".$cbo_company_id."*".$cbo_location_id."*".$cbo_buyer_id."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'*1*0";
			$rID=sql_update("pro_grey_prod_delivery_mst",$field_array_mst,$data_array_mst,"id",$update_mst_id,1);
		}
		if( str_replace("'","",$update_mst_id) != "") 
		{
			$rID3=1;
			$id_arr=array();
			$data_array_dtls=array();
			$data_array_dtls_in="";
			$field_array_dtls="current_delivery*roll";
			$dtls_id=return_next_id("id", "pro_grey_prod_delivery_dtls", 1);
			$ref_dtls_id=return_next_id("id", "pro_grey_prod_delivery_dtls", 1);	
			$mst_id=str_replace("'",'',$update_mst_id);
			$field_array_dtls_in="id,mst_id,grey_sys_id,grey_sys_number,product_id,job_no,order_id,construction,composition,gsm,dia,current_delivery,roll,inserted_by,insert_date";
			$coma=0;
			for($i=1; $i<=$total_row; $i++)
			{
				$sys_id="hidesysid_".$i;
				$hidesysnum="hidesysnum_".$i;
				$hideprodid="hideprodid_".$i;
				$hidejob="hidejob_".$i;
				$hideorder="hideorder_".$i;
				$hideconstruc="hideconstruction_".$i;
				$hidecomposit="hidecomposition_".$i;
				$hidegsm="hidegsm_".$i;
				$hidedia="hidedia_".$i;
				$txtcurrentdelivery="txtcurrentdelivery_".$i;
				$update_id_dtls="hiddendtlsid_".$i;
				$txt_roll="txtroll_".$i;
				
				if(str_replace("'",'',$$update_id_dtls)!="")
				{
					$id_arr[]=str_replace("'",'',$$update_id_dtls);
					$data_array_dtls[str_replace("'",'',$$update_id_dtls)] =explode(",",("".$$txtcurrentdelivery.",".$$txt_roll.""));
				}
				else
				{
					if(str_replace("'","",$$txtcurrentdelivery)!="")
					{
						if ($coma!=0) $data_array_dtls_in .=",";
						if ($coma!=0) $dtls_id=$dtls_id+1;
						$data_array_dtls_in	.="(".$dtls_id.",".$mst_id.",".$$sys_id.",".$$hidesysnum.",".$$hideprodid.",".$$hidejob.",".$$hideorder.",".$$hideconstruc.",".$$hidecomposit.",".$$hidegsm.",".$$hidedia.",".$$txtcurrentdelivery.",".$$txt_roll.",'".$_SESSION['logic_erp']['user_id']."','".$pc_date_time."')";
						$coma++;
					}
					
				}
			}
			
			
			$rID2=execute_query(bulk_update_sql_statement("pro_grey_prod_delivery_dtls","id", $field_array_dtls,$data_array_dtls,$id_arr));
			if($data_array_dtls_in!="")
			{
				$rID3=sql_insert("pro_grey_prod_delivery_dtls",$field_array_dtls_in,$data_array_dtls_in,1);
			}
		}
		
		
		
		if($db_type==0)
		{
			if($rID && $rID2 && $rID3)
			{
				mysql_query("COMMIT");
				echo "1**".str_replace("'",'',$mst_id)."**".str_replace("'",'',$ref_dtls_id);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'",'',$mst_id);
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
			echo "0**".$mst_id."**".str_replace("'",'',$ref_dtls_id);
		}
		disconnect($con);
		die;
	
		}
	

	exit();	
}


if($action=="delevery_search")
{

	echo load_html_head_contents("Export Information Entry Form", "../../", 1, 1,'','1','');
	extract($_REQUEST);
	?>
     
	<script>
	
		function js_set_value(data)
		{
			$('#hidden_tbl_id').val(data);
			parent.emailwindow.hide();
		}
	
    </script>

</head>

<body>
<div align="center" style="width:940px;">
	<form name="searchexportinformationfrm"  id="searchexportinformationfrm">
		<fieldset style="width:830px;">
		<legend>Enter search words</legend>           
            <table cellpadding="0" cellspacing="0" width="810" class="rpt_table" border="1" rules="all" align="center">
                <thead>
                    <th width="130" class="must_entry_caption">Company</th>
                    <th  width="120">Location</th>
                    <th width="120">Buyer</th>
                    <th width="110">Delivery Date from</th>
                    <th width="110">Delivery Date To</th>
                    <th width="110">System Id</th>
                    <th>
                        <input type="reset" name="reset" id="reset" value="Reset" style="width:100px" class="formbutton" />
                    </th>
                </thead>
                <tr class="general">
                    <td>
                        <?php
                        	echo create_drop_down( "cbo_company_id", 130, "select id,company_name from lib_company comp where status_active=1 and is_deleted=0 $company_cond order by company_name","id,company_name", 1, "--Select Company--", str_replace("'","",$company) , "load_drop_down( 'grey_feb_delivery_entry_controller', this.value, 'load_drop_down_location', 'location_td' );load_drop_down( 'grey_feb_delivery_entry_controller', this.value, 'load_drop_down_buyer', 'buyer_td' );" );
                        ?>                        
                    </td>
                    <td id="location_td">
                            <?php
								$blank_array="select id,location_name from lib_location where company_id='".str_replace("'","",$company)."' and status_active =1 and is_deleted=0 order by location_name"; 
                                echo create_drop_down( "cbo_location_id",120,$blank_array,"id,location_name", 1, "--Select Location--", $selected, "","","","","","",2);
                            ?>
                        </td>
                        <td id="buyer_td">
                            <?php 
								$blank_array="select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='".str_replace("'","",$company)."' and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name";
                                echo create_drop_down( "cbo_buyer_id",120,$blank_array,"id,buyer_name", 1, "--Select Buyer--", $selected, "","","","","","",2);
                            ?>
                        </td>
                        <td>
                        	<input type="date" name="txt_date_from" id="txt_date_from" class="datepicker" style="width:90px;" readonly/> 
                        </td>
                        <td>
                        	<input type="date" name="txt_date_to" id="txt_date_to" class="datepicker" style="width:90px;" readonly/>
                        </td>
                        <td>
                        	<input type="text" name="txt_sys_id" id="txt_sys_id" class="text_boxes" style="width:100px;"/>
                        </td>                   
                     <td>
                        <input type="button" id="search_button" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_company_id').value+'**'+document.getElementById('cbo_location_id').value+'**'+document.getElementById('cbo_buyer_id').value+'**'+document.getElementById('txt_date_from').value+'**'+document.getElementById('txt_date_to').value+'**'+document.getElementById('txt_sys_id').value, 'delivery_search_list_view', 'search_div', 'grey_feb_delivery_entry_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:100px;" />
                     </td>
                </tr>
                <tr>
                	<td colspan="7" align="center" valign="bottom"><?php echo load_month_buttons(1);  ?></td>
                </tr>
           </table>
            <div style="width:100%; margin-top:10px" id="search_div" align="left"></div>
		</fieldset>
	</form>
</div>
</body>           
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
<script>
$("#cbo_location_id").val(0);
$("#cbo_buyer_id").val(0);
</script>
</html>
<?php
	exit(); 
 
}

if ($action=="delivery_search_list_view")
{
	$data=explode("**",$data);
	$cbo_company_name=str_replace("'","",$data[0]);
	$cbo_location_name=str_replace("'","",$data[1]);
	$cbo_buyer_name=str_replace("'","",$data[2]);
	$txt_date_from=str_replace("'","",$data[3]);
	$txt_date_to=str_replace("'","",$data[4]);
	$sys_id=str_replace("'","",$data[5]);
	
	//echo $cbo_company_name."**".$cbo_location_name;
	
	
	if($cbo_company_name!=0) {$cbo_company_name="and a.company_id='$cbo_company_name'";} else {echo "Please select the company";die;}
	if($cbo_location_name!=0) $cbo_location_name="and a.location_id='$cbo_location_name'"; else $cbo_location_name="";
	if($txt_date_from!="" && $txt_date_to !="") $date_cond="and a.delevery_date between '$txt_date_from' and '$txt_date_to'"; else $date_cond="";
	if($sys_id!="") $sys_cond="and a.sys_number_prefix_num like '$sys_id'"; else $sys_cond="";
	
	$sql="select a.id,a.sys_number_prefix_num,year(a.insert_date) as sys_year,a.sys_number,a.delevery_date,a.company_id,a.location_id,b.grey_sys_number,sum(b.current_delivery) as current_delivery from  pro_grey_prod_delivery_mst a, pro_grey_prod_delivery_dtls b where a.id=b.mst_id and a.status_active=1 and a.is_deleted=0  $cbo_company_name $cbo_location_name $date_cond $sys_cond group by a.id";
	//echo $sql;
	$arr=array(2=>$company_arr,3=>$location_arr);
	echo  create_list_view("list_view", "Year,Delivery Sys.Num,Company Name,Location Name,Delivery Date,Delivery Qty","100,80,150,140,110,130","800","260",0, $sql , "js_set_value", "id,company_id,location_id,sys_number", "", 1, "0,0,company_id,location_id,0,0", $arr, "sys_year,sys_number_prefix_num,company_id,location_id,delevery_date,current_delivery", "",'','0,0,0,0,3,2') ;	
	echo '<input type="hidden" id="hidden_tbl_id">';
}


if($action=="populate_master_from_data")
{
	$sql=sql_select("select id,delevery_date,company_id,location_id,buyer_id from  pro_grey_prod_delivery_mst where id=$data");
	foreach($sql as $row)
	{
		echo "document.getElementById('txt_delevery_date').value 			= '".change_date_format($row[csf("delevery_date")])."';\n";
		echo "document.getElementById('cbo_company_id').value 				= '".$row[csf("company_id")]."';\n";
		echo "document.getElementById('cbo_location_id').value 				= '".$row[csf("location_id")]."';\n";
		echo "document.getElementById('cbo_buyer_id').value 				= '".$row[csf("buyer_id")]."';\n";
		echo "document.getElementById('update_mst_id').value 				= '".$row[csf("id")]."';\n";

	}
}


if($action=="delivery_challan_print")
{
	extract($_REQUEST);	
	
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	$datas=explode('_',$data);
	$program_ids = str_replace("'","",$datas[0]);
	$company = str_replace("'","",$datas[1]);
	$from_date = str_replace("'","",$datas[2]);
	$to_date = str_replace("'","",$datas[3]);
	$product_ids = str_replace("'","",$datas[4]);
	$order_ids = str_replace("'","",$datas[5]);
	$location= str_replace("'","",$datas[6]);
	$buyer = str_replace("'","",$datas[7]);
	$update_mst_id = str_replace("'","",$datas[8]);
	$delivery_date = str_replace("'","",$datas[9]);
	$Challan_no = str_replace("'","",$datas[10]);
	//echo $Challan_no;die;
	$company_details=return_library_array( "select id,company_name from lib_company", "id", "company_name");
	$country_arr=return_library_array( "select id,country_name from lib_country", "id", "country_name");
	$count_arr=return_library_array( "select id, yarn_count from lib_yarn_count",'id','yarn_count');
	$brand_arr=return_library_array( "select id, brand_name from lib_brand",'id','brand_name');
	$poNumber_arr=return_library_array( "select id, po_number from wo_po_break_down",'id','po_number');
	
	$machine_details=array();
	$machine_data=sql_select("select id, machine_no, dia_width from lib_machine_name");
	foreach($machine_data as $row)
	{
		$machine_details[$row[csf('id')]]['no']=$row[csf('machine_no')];
		$machine_details[$row[csf('id')]]['dia']=$row[csf('dia_width')];
	}
	
	$po_array=array();
	$po_data=sql_select("select a.job_no, a.job_no_prefix_num, a.style_ref_no, b.id, b.po_number as po_number from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.company_name=$company group by b.po_number");
	foreach($po_data as $row)
	{
		$po_array[$row[csf('id')]]['no']=$row[csf('po_number')];
		$po_array[$row[csf('id')]]['job_no']=$row[csf('job_no')];
		$po_array[$row[csf('id')]]['style_ref_no']=$row[csf('style_ref_no')];
	}
	
	$composition_arr=array();
	$sql_deter="select a.id, a.construction, b.copmposition_id, b.percent from lib_yarn_count_determina_mst a, lib_yarn_count_determina_dtls b where a.id=b.mst_id";
	$data_array=sql_select($sql_deter);
	if(count($data_array)>0)
	{
		foreach( $data_array as $row )
		{
			if(array_key_exists($row[csf('id')],$composition_arr))
			{
				$composition_arr[$row[csf('id')]]=$composition_arr[$row[csf('id')]]." ".$composition[$row[csf('copmposition_id')]]." ".$row[csf('percent')]."%";
			}
			else
			{
				$composition_arr[$row[csf('id')]]=$row[csf('construction')].", ".$composition[$row[csf('copmposition_id')]]." ".$row[csf('percent')]."%";
			}
		}
	}
	
	$knit_plan_arr=array();
	$plan_data=sql_select("select id, color_range, stitch_length from ppl_planning_info_entry_dtls");
	foreach($plan_data as $row)
	{
		$knit_plan_arr[$row[csf('id')]]['cr']=$row[csf('color_range')];
		$knit_plan_arr[$row[csf('id')]]['sl']=$row[csf('stitch_length')]; 
	}
	
	$update_row_check=array();
	if($update_mst_id!="")
	{
		$sql_update=sql_select("select id,grey_sys_id,product_id,job_no,order_id,current_delivery,roll from pro_grey_prod_delivery_dtls where mst_id=$update_mst_id");
		foreach($sql_update as $row)
		{
			$update_row_check[$row[csf("grey_sys_id")]."*".$row[csf("product_id")]."*".$row[csf("order_id")]]["current_delivery"] =$row[csf("current_delivery")];
			$update_row_check[$row[csf("grey_sys_id")]."*".$row[csf("product_id")]."*".$row[csf("order_id")]]["id"] =$row[csf("id")];
			$update_row_check[$row[csf("grey_sys_id")]."*".$row[csf("product_id")]."*".$row[csf("order_id")]]["roll"] =$row[csf("roll")];
		}
	}	
	
	//var_dump($update_row_check);die;
	
	?>
	<div style="width:1360px;">
		<table width="1350" cellspacing="0" align="center" border="0">
			<tr>
				<td colspan="17" align="center" style="font-size:x-large"><strong><?php echo $company_details[$company]; ?></strong></td>
			</tr>
			<tr>
				<td colspan="17" align="center" style="font-size:18px"><strong><u>Delivery Challan</u></strong></center></td>
			</tr>
			<tr>
				<td colspan="17" align="center" style="font-size:16px"><strong><u>Knitting Section</u></strong></center></td>
			</tr>
        </table> 
        <br>
		<table width="1350" cellspacing="0" align="center" border="0">
			<tr>
				<td   style="font-size:16px; font-weight:bold;" width="110">Challan No</td>
                <td colspan="16"  >:&nbsp;<?php echo $Challan_no; ?></td>
			</tr>
            <tr>
				<td style="font-size:16px; font-weight:bold;" width="110">Delivery Date </td>
                <td colspan="16" >:&nbsp;<?php echo $delivery_date; ?></td>
			</tr>
        </table> 
    </div>
    <div style="width:100%;">
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="1350" class="rpt_table" >
            <thead>
                <tr>
                    <th width="30" >SL</th>
                    <th width="130" >Order No</th>
                    <th width="60" >Buyer</th>
                    <th width="50" >System. ID</th>
                    <th width="50" >Prog. ID</th>
                    <th width="100" >Booking No</th>
                    <th width="80" >Yarn Count</th>
                    <th width="90" >Yarn Brand</th>
                    <th width="60" >Lot No</th>
                    <th width="100" >Color</th>
                    <th width="240">Fabric Type</th>
                    <th width="50" >Stich</th>
                    <th width="60" >Fin GSM</th>
                    <th width="40" >Fab. Dia</th>
                    <th width="40" >M/C Dia</th>
                    <th width="70" >Total Roll</th>
                    <th  width="100">Total Qty</th>
                </tr>
            </thead>
        </table>
    <div style="width:1350px">
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="1350" class="rpt_table" >
          
	<?php
		if($from_date!="" && $to_date!="") $date_con="and a.receive_date between '".change_date_format($from_date,'yyyy-mm-dd')."' and '".change_date_format($to_date,'yyyy-mm-dd')."'"; else $date_con="";
		if($location!=0) $location_con="and a.location_id=$location"; else $location_con="";
		if($buyer!=0) $buyer_con="and a.buyer_id=$buyer"; else $buyer_con="";
		$sql="select a.id, a.recv_number_prefix_num, a.recv_number, a.receive_basis, a.knitting_source, a.knitting_company, a.receive_date, a.booking_id, a.booking_no, a.buyer_id, a.remarks, b.prod_id, b.febric_description_id, b.gsm, b.width,  b.yarn_lot, b.yarn_count, b.brand_id, b.machine_no_id, c.po_breakdown_id, c.quantity  as outqntyshift  
		from 
			inv_receive_master a, pro_grey_prod_entry_dtls b, order_wise_pro_details c 
		where 
			a.id=b.mst_id and b.id=c.dtls_id and a.entry_form=2 and a.status_active=1 and a.is_deleted=0 and c.trans_type=1 and a.company_id=$company  and a.id in ($program_ids) and b.prod_id in ($product_ids) and c.po_breakdown_id in($order_ids) $date_con $location_con $buyer_con  group by a.id,b.prod_id,c.po_breakdown_id order by a.id";	
	//echo $sql;
	

	$nameArray=sql_select( $sql); $i=1; $tot_roll=0; $tot_qty=0;
	foreach($nameArray as $row)
	{
		if ($i%2==0)  
			$bgcolor="#E9F3FF";
		else
			$bgcolor="#FFFFFF";
			
		$count='';
		$yarn_count=explode(",",$row[csf('yarn_count')]);
		foreach($yarn_count as $count_id)
		{
			if($count=='') $count=$yarn_count_details[$count_id]; else $count.=",".$yarn_count_details[$count_id];
		}
		
		$reqsn_no=""; $stich_length=""; $color="";
		if($row[csf('receive_basis')]==2)
		{
			$stich_length=$knit_plan_arr[$row[csf('booking_id')]]['sl']; 
			$color=$color_range[$knit_plan_arr[$row[csf('booking_id')]]['cr']];
		}
		if($update_row_check[$row[csf("id")]."*".$row[csf("prod_id")]."*".$row[csf("po_breakdown_id")]]["current_delivery"]>0)
		{
			?>
			<tr bgcolor="<?php echo $bgcolor; ?>">
                <td width="30"><div style="word-wrap:break-word; width:30px"><?php echo $i; ?></div></td>
                <td width="130"><div style="word-wrap:break-word; width:127px"><?php echo $poNumber_arr[$row[csf('po_breakdown_id')]];?></div></td>
                <td width="60"><div style="word-wrap:break-word; width:60px"><?php echo $buyer_array[$row[csf('buyer_id')]]; ?></div></td>
                <td width="50"><div style="word-wrap:break-word; width:50px"><?php echo $row[csf('recv_number_prefix_num')]; ?></div></td>
                <td width="50"><div style="word-wrap:break-word; width:50px"><?php echo $row[csf('booking_id')]; ?></div></td>
                <td width="100"><div style="word-wrap:break-word; width:100px"><?php echo $row[csf('booking_no')]; ?></div></td>
                <td width="80"><div style="word-wrap:break-word; width:79px"><?php echo $count; ?></div></td>
                <td width="90"><div style="word-wrap:break-word; width:89px"><?php echo $brand_details[$row[csf('brand_id')]];?></div></td>
                <td width="60"><div style="word-wrap:break-word; width:59px"><?php echo $row[csf('yarn_lot')]; ?></p></td>
                <td width="100"><div style="word-wrap:break-word; width:96px"><?php echo $color; ?></div></td>
                <td width="240"><div style="word-wrap:break-word; width:234px"><?php echo $composition_arr[$row[csf('febric_description_id')]];; ?></div></td>
                <td width="50"><div style="word-wrap:break-word; width:49px"><?php echo $stich_length; ?></p></td>
                <td width="60"><div style="word-wrap:break-word; width:60px"><?php  echo $row[csf('gsm')]; ?></div></td>
                <td width="40"><div style="word-wrap:break-word; width:39px"><?php  echo $row[csf('width')];?></p></td>
                <td width="40"><div style="word-wrap:break-word; width:39px"><?php echo $machine_details[$row[csf('machine_no_id')]]['dia']; ?></div></td>
                <td width="70" align="right"><div style="word-wrap:break-word; width:69px"><?php echo number_format($update_row_check[$row[csf("id")]."*".$row[csf("prod_id")]."*".$row[csf("po_breakdown_id")]]["roll"],2); $tot_roll+=$update_row_check[$row[csf("id")]."*".$row[csf("prod_id")]."*".$row[csf("po_breakdown_id")]]["roll"]; ?></div></td>
                <td  align="right" width="100"><div style="word-wrap:break-word; width:98px"><?php echo number_format($update_row_check[$row[csf("id")]."*".$row[csf("prod_id")]."*".$row[csf("po_breakdown_id")]]["current_delivery"],2); $tot_qty+=$update_row_check[$row[csf("id")]."*".$row[csf("prod_id")]."*".$row[csf("po_breakdown_id")]]["current_delivery"]; ?></div></td>
			</tr>
			<?php
			$i++;
		}
	}
	?>
        	<tr> 
                <td align="right" colspan="15" ><strong>Total:</strong></td>
                <td align="right"><?php echo number_format($tot_roll,2,'.',''); ?>&nbsp;</td>
                <td align="right" ><?php echo number_format($tot_qty,2,'.',''); ?>&nbsp;</td>
			</tr>
            <tr>
                <td colspan="2" align="left"><b>Remarks: <?php //echo number_to_words($format_total_amount,$uom_unit,$uom_gm); ?></b></td>
                <td colspan="15" >&nbsp;</td>
            </tr>
		</table>
        <br>
		 <?php
            echo signature_table(44, $company, "1340px");
         ?>
	</div>
	</div>
	<?php
    exit();
}

?>
