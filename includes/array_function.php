<?php
//******************************************************************************************** please DO NOT CHANGE BELOW ARRAYS
$blank_array=array();    // for blank Drop Down or empty drop down
$mod_permission_type=array(0=>"Selective Permission",1=>"Full Permission",2=>"No Permission");
$form_permission_type=array(1=>"Permitted",2=>"Not Permitted");
$row_status=array(1=>"Active",2=>"InActive",3=>"Cancelled"); 
$knitting_program_status=array(1=>"Waiting",2=>"Running",3=>"Stop"); 
$attach_detach_array=array(1=>"Attach",0=>"Detach");
$planning_status=array(1=>"Pending",2=>"Planning Done",3=>"Requisition Done",4=>"Demand Done");
$yes_no=array(1=>"Yes",2=>"No"); //2= Deleted,3= Locked
$months = array(1=>'January',2=>'February',3=>'March',4=>'April',5=>'May',6=>'June',7=>'July',8=>'August',9=>'September',10=>'October',11=>'November',12=>'December');
$year = array(2010=>'2010',2011=>'2011',2012=>'2012',2013=>'2013',2014=>'2014',2015=>'2015',2016=>'2016',2017=>'2017',2018=>'2018',2019=>'2019',2020=>'2020',2021=>'2021');
$string_search_type=array(1=>"Exact",2=>"Starts with",3=>"Ends with",4=>"Contents");
$dec_place = array(1=>'2',2=>'2',3=>'8',4=>'2',5=>'4',6=>'0',7=>'2');
//1=qnty(kg),2=qnty(yds),3=Rate,4=Amount(local Currency/Taka), 5=Foreign Currency, 6=Gmts qnty, 7=%(percentage)

$all_cal_day=array(1=>"Saturday",2=>"Sunday",3=>"Monday",4=>"Tuesday",5=>"Wednesday",6=>"Thursday",7=>"Friday");

//******************************************************************************************** please DO NOT CHANGE UPPER ARRAYS
// common for All Module //
$currency=array(1=>"Taka",2=>"USD",3=>"EURO");
// Library Module //
$user_type=array(1=>"General User",2=>"Admin User",3=>"Demo User");
$sample_type=array(2=>"PP",3=>"FIT",4=>"Size Set",5=>"Others"); 
$trim_type=array(1=>"Sewing",2=>"Packing/Finishing");
$production_module=array(1=>"Production Update Areas",2=>"Excess Cutting Slab",3=>"Fabric in Roll Level",4=>"Fabric in Machine Level",13=>"Batch Maintained",15=>"Auto Fabric Store Update"); 
$production_update_areas=array(1=>"Gross Quantity Level",2=>"Color Level",3=>"Color & Size Level");
//$item_category=array(1=>"Yarn",2=>"Knit Fabrics",3=>"Woven Fabrics",4=>"Accessories",5=>"Chemicals",6=>"Dyes",7=>"Auxilary Chemicals",8=>"Spare Parts",9=>"Machinaries",10=>"Other Capital Items",11=>"Stationaries",12=>"Services",13=>'Grey Fabric(Knit)',14=>'Grey Fabric(woven)');

$entry_form=array(1=>"Yarn Receive",2=>"Grey Receive",3=>"Yarn Issue",4=>"Chemical Receive",5=>"Chemical Issue",6=>"Dye Production Update",7=>"Finish Fabric Production Entry",8=>'Yarn Receive Return',9=>'Yarn Issue Return',10=>'Yarn Transfer Entry',11=>'Yarn Order To Order Transfer Entry',12=>'Grey Fabric Transfer Entry',13=>'Grey Fabric Order To Order Transfer Entry',14=>'Finish Fabric Transfer Entry',15=>'Finish Fabric Order To Order Transfer Entry',16=>'Knit Grey Fabric Issue',17=>'Woven Finish Fabric Receive',18=>'Knit Finish Fabric Issue',19=>'Woven Finish Fabric Issue',20=>'General Item Receive',21=>'General Item Issue',22=>'Knit Grey Fabric Receive',23=>'Woven Grey Fabric Receive',24=>'Trims Receive',25=>'Trims Issue');

$entry_form_for_roll=array(1=>"grey_productin_entry", 2=>"batch_creation", 3=>"Dye Production Update",4=>"finish_fabric_receive",5=>"Woven Finish Fabric Receive",16=>"Knit Grey Fabric Issue",18=>'Knit Finish Fabric Issue',19=>'Woven Finish Fabric Issue',22=>'Knit Grey Fabric Receive',23=>'Woven Grey Fabric Receive');


$wages_rate_var_for=array(1=>"Garments Cutting",2=>"Garments Finishing");

$production_resource=array(1=>"Single Needle",2=>"Flat Lock",3=>"Over Lock",4=>"Button Hole",5=>"Button Stich",6=>"Snap Button",7=>"Eylet Hole",8=>"Kansai",9=>"Feed Of the Arm",10=>"Rib Scissoring",40=>"Helper",41=>"QuaLity Inspector");

$machine_category=array(1=>"Knitting",2=>"Dyeing",3=>"Printing",4=>"Finishing",5=>"Embroidery",6=>"Washing",7=>"Cutting",8=>"Sewing");
$depreciation_method=array(1=>"Straight-line",2=>"Reducing Balance");

$item_transfer_criteria=array(1=>"Company To Company",2=>"Store To Store",3=>"Style To Style",4=>"Order To Order",5=>"Item To Item");

$party_type=array(
1=>"Buyer",
2=>"Subcontract",
3=>"Buyer/Subcontract",
4=>"Notifying Party",
5=>"Consignee",
6=>"Notifying/Consignee",
20=>"Buying Agent",
21=>"Buyer/Buying Agent",
22=>"Export LC Applicant",
23=>"LC Applicant/Buying Agent",
30=>"Developing Buyer",
90=>"Buyer/Supplier");

$party_type_supplier=array(
1 =>"Supplier",
2 =>"Yarn Supplier",
3 =>"Dyes & Chemical Supplier",
4 =>"Trims Supplier",
5 =>"Accessories Supplier",
6 =>"Machineries Supplier",
7 =>"Spare Parts/Consumables",
8 =>"Stationery Supplier",
9 =>"Fabric Supplier",
20 =>"Knit Subcontract",
21 =>"Dyeing/Finishing Subcontract",
22 =>"Garments Subcontract",
23 =>"Embellishment Subcontract",
24 =>"Fabric Washing Subcontract",
25 =>"AOP Subcontract",
30 =>"C & F Agent",
31 =>"Clearing Agent",
32 =>"Forwarding Agent",
35 =>"Transport Supplier",
36 =>"Labor Contractor",
37 =>"Civil Contractor",
38 =>"Interior",
39 =>"Other Contractor",
40 =>"Indentor",
41 =>"Inspection",
90 =>"Buyer/Supplier");
///Add By Maruf from other array function
$report_signeture_list = array(0=>"-- Select Report --",1=>"Fabric Booking",2=>"Trims Booking",3=>"PI Wise Yarn Receive",4=>"Short Fabric Booking",5=>"Sample Fabric Booking -With order",6=>"Sample Fabric Booking -Without order",7=>"Yarn Receive Return",8=>"Dyes And Chemical Receive",9=>"Dyes And Chemical Issue",10=>"Dye/Chem Receive Return",11=>"General Item Receive",12=>"General Item Issue",13=>"General Item Receive Return",14=>"General Item Issue Return",15=>"Dyes And Chemical Issue Requisition",16=>"Knit Grey Fabric Receive",17=>"Knit Grey Fabric Issue",18=>"Grey Fabric Transfer Entry",19=>"Grey Fabric Order To Order Transfer Entry",20=>"Woven Finish Fabric Receive",21=>"Knit Finish Fabric Issue",22=>"Woven Finish Fabric Issue",23=>"Finish Fabric Transfer Entry",24=>"Finish Fabric Order To Order Transfer Entry",25=>"Purchase Requisition",26=>"Embellishment Issue Entry",27=>"Embellishment Receive Entry",28=>"Sewing Input",29=>"Sewing Output",30=>"Iron entry",31=>"Packing And Finishing",32=>"Ex-Factory",33=>"Gate In Entry",34=>"Gate Out Entry",35=>"Trims Receive Entry",36=>"Trims Issue",37=>"Yarn Issue Return",38=>"Yarn Transfer Entry",39=>"Yarn Order To Order Transfer Entry",40=>"Daily Yarn Demand",41=>"Knitting Plan Report",42=>"Yarn Work Order",43=>"Yarn Dyeing Work Order",44=>"Knitting Delivery Challan",45=>"SubCon Fabric Finishing Entry",46=>"SubCon Delivery Challan",47=>"SubCon Knitting Bill Issue",48=>"SubCon Dyeing And Finishing Bill Issue",49=>"Yarn Issue",50=>"SubCon Cutting Bill Issue",51=>"SubCon Material Return Challan",52=>"Batch Creation",53=>"Fabric Service Booking",54=>"Cutting Delivary To Input",55=>"Stationary Work Order",56=>"SubCon Batch Creation",57=>"Embellishment Work Order",58=>"Cut and Lay Entry",59=>"Dyes Chemical Work Order",60=>"Spare Parts Work Order",61=>"Subcon Material Issue",62=>"Recipe Entry",63=>"Garments Delivery",64=>"SubCon Knitting Delivery Challan",65=>"Yarn Receive",66=>"Knit Finish Fabric Receive",67=>"Finish Fabric Production Entry",68=>"Finish Fabric Delivery to Store",69=>"Quotation Evaluation",70=>"Grey Fabric Delivery to store roll wise",71=>"Grey Fabric Receive Roll By Batch",72=>"Grey Roll Issue to Sub Contact ",73=>"AOP Roll Receive",74=>"Finish Fabric Roll Receive By Cutting", 77=>"Sample Ex-factory",78=>"Scrap Out Challan",79=>"Service Booking For AOP",80=>"Lab Test Work Order",81=>"Service Booking For Knitting",82=>"Service Booking For Dyeing",83=>"Knit Finish Fabric Receive Return"); 

$item_category=array(1=>"Yarn",2=>"Knit Finish Fabrics",3=>"Woven Fabrics",4=>"Accessories",5=>"Chemicals",6=>"Dyes",7=>"Auxilary Chemicals",8=>"Spare Parts",9=>"Spare Parts & Machinaries",10=>"Other Capital Items",11=>"Stationaries",12=>"Services - Fabric",13=>'Grey Fabric(Knit)',14=>'Grey Fabric(woven)',15=>'Electical',16=>'Maintenance',17=>'Medical',18=>'ICT',19=>'Print & Publication',20=>'Utilities & Lubricants',21=>'Construction Materials',22=>'Printing Chemicals & Dyes',23=>'Dyes Chemicals & Auxilary Chemicals',24=>'Services - Yarn Dyeing ',25=>'Services - Embellishment',28=>'Cut Panel',30=>'Garments');


$tna_task_name = array(						 				
						1	=> "Order Placement Date",			
						2	=> "Order Evaluation",			
						3	=> "Acceptance To Be Given",			
						4	=> "Internal Communication To Be Done",			
						5	=> "SC/LC Received",
						7	=> "Fit Sample Submit",
						8	=> "PP Sample Submit",
						9	=> "Labdip Submit", 			
						10	=> "Labdip Approval",			
						11	=> "Trims Approval",			
						12	=> "PP Sample Approval",
						13	=> "Fit Sample Approval",
						14	=> "Size Set Submission",
						15	=> "Size Set Approval",
						16	=> "Production Sample Submission",
						17	=> "Production Sample Approval",
						19	=> "Embellishment Submission",			
						20	=> "Embellishment Approval",
						21	=> "Tag Sample Submission",
						22	=> "Tag Sample Approval",
						23	=> "Photo Sample Submission",
						24	=> "Photo Sample Approval",
						25	=> "Trims Submission",
						26	=> "Packing  Sample Submission",
						27	=> "Packing  Sample Approval",
						28	=> "Final  Sample Submission",
						29	=> "Final  Sample Approval",
						30	=> "Sample Fabric Booking To Be Issued",			
						31	=> "Fabric Booking To Be Issued",			
						32	=> "Trims Booking To Be Issued",			
						33	=> "Fabric Service Work Order To Be Issued",
						34	=> "Woven Fabric Work Order To Be Issued",			
						40	=> "Fabric Test To Be Done",			
						41	=> "Garments Test To Be Done",
						45	=> "Yarn purchase requisition",
						46	=> "Yarn purchase order",
						47	=> "Yarn Receive",
						48	=> "Yarn Allocating",		
						50	=> "Yarn Issue To Be Done",			
						51	=> "Yarn Send for Dyeing",			
						52	=> "Dyed Yarn Receive",			
						60	=> "Gray Fabric Production To Be Done",			
						61	=> "Dyeing Production To Be Done",			
						62	=> "Fabric Send for AOP",			
						63	=> "AOP Receive",			
						64	=> "Finish Fabric Production To Be Done",			
						70	=> "Sewing Trims To Be In-house",			
						71	=> "Finishing Trims To Be In-house",			
						72	=> "Gray fabric to be in-house",			
						73	=> "Finished fabric to be in-house",	
						74	=> "Finish Fabric Issue to Cut",		
						80	=> "PP Meeting To Be Conducted",			
						81	=> "Trail cut to be done",			
						82	=> "Trail production to be submitted",			
						83	=> "Trail production approval to be received",			
						84	=> "Cutting To Be Done",			
						85	=> "Print/Emb To Be Done",			
						86	=> "Sewing To Be Done",			
						87	=> "Iron To Be Done",			
						88	=> "Garments Finishing To Be Done",	
						89	=> "Garments sent for Wash",	
						90	=> "Garments Receive from Wash",	
						100	=> "Inspection Schedule To Be Offered",			
						101	=> "Inspection To Be Done",			
						110	=> "Ex-Factory To Be Done",			
						120	=> "Document to be submited",			
						121	=> "Proceeds to be realized",
						122	=> "Sewing Input To Be Done");

$subcon_variable=array(1=>"Dyeing & Finishing Bill Qty",2=>"Knitting Fabric From Yarn Count Det.",3=>"Bill Rate");  
///End Add By Maruf from other array function
$tna_task_catagory=array(
1 =>"General",
5 =>"Sample Approval",
6 =>"Lab Dip Approval",
7 =>"Trims Approval",
8 =>"Embellishment Approval",
9 =>"Test Approval",
15 =>"Purchase",
20 =>"Material Receive",
25 =>"Fabric Production",
26 =>"Garments Production",
30 =>"Inspection",
35 =>"Export");


$supplier_nature=array(
1=>" Goods",
2 =>"Service",
3 =>"Both"
);
$fabric_typee=array(1=>"Open Width",2=>"Tubular",3=>"Niddle Open");	
$process_type=array(1=>"Main Process",2=>"Additional Process");	
$account_type=array(1=>"CD A/C", 2=>"STD A/C", 3=>"OD A/C", 4=>"CC A/C", 5=>"BTB Margin A/C", 6=>"ERQ A/C", 7=>"Imp. LC Margin A/C", 8=>"BG Margin A/C", 9=>"ECC A/C", 10=>"PC A/C");
$core_business=array(1=>"Manufacturing",2=>"Trading",3=>"Service",4=>"Educational",5=>"Social Welfare");
$company_nature=array(1=>"Private Ltd",2=>"Public Ltd",3=>"Sole Tradership",4=>"Partnership");
$loan_type=array(0=>"Percent",1=>"Fixed");
$commercial_module=array(5=>"Garments Export Capacity",6=>"Max BTB Limit",7=>"Max PC Limit",17=>"Possible Heads For BTB");
$cost_heads=array(0=>"--Select--","Knitting Charge"=>"Knitting Charge","Fabric Dyeing Charge"=>"Fabric Dyeing Charge","Yarn Dyeing Charge"=>"Yarn Dyeing Charge","All Over Print Charge"=>"All Over Print Charge","Dyed Yarn Knit Charge"=>"Dyed Yarn Knit Charge","Stantering Charge"=>"Stantering Charge","Brush Peach Charge"=>"Brush Peach Charge","Washing Charge"=>"Washing Charge","Printing"=>"Printing","Embroidery"=>"Embroidery","Washing"=>"Washing");
$rate_for=array(1=>"Fabric Dyeing",2=>"Fabric Finishing");
$cal_parameter=array(1=>"Sewing Thread",2=>"Carton",3=>"Carton Stiker");
$cm_cost_predefined_method=array(1=>"SMV*CPM+ (SMV*CPM)* Efficiency Wastage%",2=>"(SMV*CPM/ Efficiency %)+(SMV*CPM/ Efficiency %)",3=>"{(MCE/WD)/NFM)*MPL)}/[{(PHL)*WH}]*Costing Per/Exchange Rate");

// Library Module ends //



// Merchandising
$approval_status=array( 1=>"Submitted",2=>"Rejected",3=>"Approved",4=>"Cancelled");
$order_status=array(1=>"Confirmed",2=>"Projected");
$region=array(1=>"Asia",2=>"Africa",3=>"Australia",4=>"Antarctica",5=>"Europe",6=>"North America ",7=>"South America");
$packing=array(1=>"SCSS",2=>"ACSS",3=>"SCAS",4=>"ACAS");
//$ship_mode=array(1=>"Air",2=>"Sea");
$product_dept=array(1=>"Menz",2=>"Ladies",3=>"Teen Age-Girls",4=>"Teen Age-Boys",5=>"Kids",6=>"Infant",7=>"Unisex");
//$pord_dept=array(1=>"Menz",2=>"Ladies",3=>"Teen Age-Girls",4=>"Teen Age-Boys",5=>"Kids",6=>"Infant",7=>"Intimates"); 

$product_category=array(1=>"Garments",2=>"Intimates",3=>"Sweater",4=>"Socks");
$garments_item=array(
1=>"T-Shirt-Long Sleeve",
2=>"T-Shirt-Short Sleeve",
3=>"Polo Shirt-Long Sleeve",
4=>"Polo Shirt-Short Sleeve",
5=>"Tang Top",
6=>"T-Shirt 3/4 ARM",
7=>"Hoodies",
10=>"Raglans",
14=>"Blazer",
15=>"Jacket",
16=>"Night Wear",
20=>"Full Pant",
21=>"Short Pant",
22=>"Trouser",
23=>"Payjama",
24=>"Romper Short Sleeve",
25=>"Romper Long Sleeve",
26=>"Romper Sleeveless",
27=>"Romper",
28=>"Legging",
29=>"Three Quater",
40=>"Singlet",
41=>"Teens Singlet",
42=>"Boxer",
43=>"Stripe Boxer",
44=>"Teens Boxer",
45=>"Jersy Boxer",
46=>"Panty",
47=>"Slip Brief",
48=>"Classic Brief",
49=>"Short Brief",
50=>"Mini Brief",
51=>"Bikini",
52=>"Lingerie",
53=>"Bikers",
54=>"Underwear",
60=>"Plain Socks",
61=>"Rib Socks",
62=>"Jacuard/Patern Socks",
63=>"Heavy Gauge Socks",
64=>"Sports Socks",
65=>" Terry Socks",
66=>"Tight Socks"
);
$composition=array(
1=>"Cotton",
2=>"Spandex",
3=>"Viscos",
4=>"Polyster",
5=>"Organic",
6=>"BCI",
7=>"Modal",
8=>"Conventional",
9=>"ECRU Melange",
10=>"Elastane",
11=>"Carded Flo Fair",
12=>"Linen",
13=>"Slub"
);

/*$unit_of_measurement=array(
"PIECES"=>array(
01=>"Pcs", 
02=>"Dzn",
03=>"Grs",
04=>"GG"
),
"WEIGHT"=>array(
10=>"Mg",
11=>"Gm",
12=>"Kg",
13=>"Quintal",
14=>"Ton"
),
"LENGTH"=>array(
20=>"Km",
21=>"Hm",
22=>"Dm",
23=>"Mtr",
24=>"Dcm",
25=>"CM",
26=>"MM",
27=>"Yds",
28=>"Feet",
29=>"Inch"
),
"LIQUID"=>array(
40=>"Ltr",
41=>"Ml"
),
"OTHERS"=>array(
50=>"Roll",
51=>"Coil",
52=>"Cone",
53=>"Bag",
54=>"Box",
55=>"Drum",
56=>"Bottle",
57=>"Pkt",
58=>"Set"
)
);*/
$unit_of_measurement=array(
1=>"Pcs", 
2=>"Dzn",
3=>"Grs",
4=>"GG",
10=>"Mg",
11=>"Gm",
12=>"Kg",
13=>"Quintal",
14=>"Ton",
15=>"Lbs",
20=>"Km",
21=>"Hm",
22=>"Dm",
23=>"Mtr",
24=>"Dcm",
25=>"CM",
26=>"MM",
27=>"Yds",
28=>"Feet",
29=>"Inch",
40=>"Ltr",
41=>"Ml",
50=>"Roll",
51=>"Coil",
52=>"Cone",
53=>"Bag",
54=>"Box",
55=>"Drum",
56=>"Bottle",
57=>"Pack",
58=>"Set"
);
$pord_dept=array(1=>"Menz",2=>"Ladies",3=>"Teen Age-Girls",4=>"Teen Age-Boys",5=>"Kids",6=>"Infant",7=>"Intimates"); 

//merchandise variable settings
$order_tracking_module=array(12=>"Sales Year started",14=>"TNA Integrated",15=>"Pre Costing : Profit Calculative",18=>"Process Loss Method",19=>"Consumtion Basis",20=>"Copy Quotation",21=>"Conversion From Chart",22=>"CM Cost Predefined Method"); 
$process_loss_method=array(1=>"Markup Method",2=>"Margin method");
$consumtion_basis=array(1=>"Cad Basis",2=>"Measurement Basis",3=>"Marker Basis");
//$wo_category = array(2=>"Knit Fabrics",3=>"Woven Fabrics",4=>"Accessories",13=>'Grey Fabric(Knitt)',14=>'Grey Fabric(woven)',12=>"Services");
$gmts_nature= array(1=>"Knit Garments",2=>"Woven Garments",3=>"Sweater");
$incoterm=array(1=>"FOB",2=>"CFR",3=>"CIF",4=>"FCA",5=>"CPT",6=>"EXW",7=>"FAS",8=>"CIP",9=>"DAF",10=>"DES",11=>"DEQ",12=>"DDU",13=>"DDP");
$fabric_source=array(1=>"Production",2=>"Purchase",3=>"Buyer Supplied");
$color_range=array(1=>"Dark Color",2=>"Light Color",3=>"Black Color",4=>"White Color",5=>"Average Color");
$costing_per=array(1=>"For 1 Dzn",2=>"For 1 Pcs",3=>"For 2 Dzn",4=>"For 3 Dzn",5=>"For 4 Dzn");
$delay_for=array(1=>"Sample Approval Delay",2=>"Lab Dip Approval Delay",3=>"Trims Approval Delay",4=>"Yarn In-House Delay",5=>"Knitting Delay",6=>"Dyeing Delay",7=>"Fabric In-House Delay",8=>"Trims In-House Delay",9=>"Print/Emb Delay",10=>"Line Insufficient",11=>"Worker Insufficient",12=>"Bulk Prod. Approval Delay",13=>"Traget Falilure",14=>"Inspection Fail",15=>"Production Problem");
//$body_part=array(1=>"Main Fabric",2=>"Collar",3=>"Culf",4=>"Rib",5=>"Hood",6=>"Pocketing",7=>"Bottom Rib",8=>"Sleeve",9=>"Back Part",10=>"Front Part");
$body_part=array(1=>"Main Fabric Top",2=>"Collar",3=>"Culf",4=>"Rib",6=>"Hood",7=>"Pocketing",8=>"Bottom Rib",9=>"Sleeve",10=>"Back Part",11=>"Front Part",12=>"Facing Fabric",13=>"Binding",14=>"Body part-1",15=>"Body part-2",16=>"Body part-3",17=>"Body part-4",18=>"Shoulder",19=>"Hood lining",20=>" Main Fabric Bottom",21=>"Pocketing Bottom",22=>"Bottom Rib Bottom",23=>"Waist belt insert",24=>"Waist belt",40=>"Placket",41=>"Neck Tape",42=>"Piping",43=>"Pocket binding",44=>"Neck band",45=>"Neck insert",46=>"Drawstring",47=>"Contrast insert",48=>"Mesh insert",49=>"Tricot mesh",50=>"Woven insert",51=>"Double layer",52=>"Side slit",53=>"Yoke",54=>"Applique fabrics",55=>"Neck Rib",56=>"Inner Neck",57=>"Sleeve Layer",58=>"Half Moon",59=>"Neck",60=>"Back Moon");
 
$color_type=array(1=>"Solid",2=>"Stripe (Y/D)",3=>"Cross Over (Y/D)",4=>"Check (Y/D)",5=>"AOP",20=>"Florecent",25=>"Reactive");

$dyeing_sub_process = array(1=>"Demineralizing",5=>"Pretreatment",10=>"Nutural",12=>"Enzym",15=>"Dyestuff",20=>"Dyeing Bath",30=>"After Treatment");
$dose_base = array(1=>"Gram Per Liter Liquor",2=>"% on Batch Weight");
//$conversion_cost_head_array=array(1=>"Knitting",2=>"Weaving",30=>"Yarn Dyeing",31=>"Fabric Dyeing",32=>"Tube Opening",33=>"Heat Setting",34=>"Stiching Back To Tube",35=>"All Over Printing",36=>"Stripe Printing",37=>"Cross Over Printing",60=>"Scouring",61=>"Color Dosing",62=>"Neutralization",63=>"Squeezing",64=>"Washing",65=>"Stentering",66=>"Compacting",67=>"Peach Finish",68=>"Brush",69=>"Peach+Brush",70=>"Heat+Peach",71=>"Peach+Brush+Heat",72=>"UV Prot",73=>"Odour Finish",74=>"Teflon Coating",75=>"Cool Touch",76=>"MM",77=>"Easy Care Finish",78=>"Water Repellent",79=>"Flame Resistant",80=>"Hydrophilics",81=>"Antistatic",82=>"Enzyme",83=>"Silicon", 84=>"Softener", 85=>"Brightener",86=>"Fixing/Binding Agent",87=>"Leveling Agent",101=>"Dyes & Chemical Cost");
$conversion_cost_head_array=array(
1=>"Knitting",
2=>"Weaving",
3=>"Collar and Cuff Knitting",
4=>"Feeder Stripe Knitting",
30=>"Yarn Dyeing",
31=>"Fabric Dyeing",
32=>"Tube Opening",
33=>"Heat Setting",
34=>"Stiching Back To Tube",
35=>"All Over Printing",
36=>"Stripe Printing",
37=>"Cross Over Printing",
60=>"Scouring",
61=>"Color Dosing",
62=>"Neutralization",
63=>"Squeezing",
64=>"Washing",
65=>"Stentering",
66=>"Compacting",
67=>"Peach Finish",
68=>"Brush",
69=>"Peach+Brush",
70=>"Heat+Peach",
71=>"Peach+Brush+Heat",
72=>"UV Prot",
73=>"Odour Finish",
74=>"Teflon Coating",
75=>"Cool Touch",
76=>"MM",
77=>"Easy Care Finish",
78=>"Water Repellent",
79=>"Flame Resistant",
80=>"Hydrophilics",
81=>"Antistatic",
82=>"Enzyme",
83=>"Silicon", 
84=>"Softener", 
85=>"Brightener",
86=>"Fixing/Binding Agent",
87=>"Leveling Agent",
88=>"Sueding",
101=>"Dyes & Chemical Cost",
120=>"Cutting",
121=>"Gmts. Printing",
122=>"Gmt. Embroidery",
123=>"Gmts. Washing",
124=>"Sewing");

$conversion_cost_type=array(1=>"Knitting",10=>"Yarn Dyeing",11=>"Dyeing",12=>"AOP",13=>"Wash",20=>"Finishing",21=>"Chemical Finish",22=>"Special Finishing",40=>"Dyes & Chemical "); 
$emblishment_name_array=array(1=>"Printing",2=>"Embroidery",3=>"Wash",4=>"Special Works",5=>"Others");
$cost_heads_for_btb=array(1=>"Knitting",4=>"Feeder Stripe Knitting",30=>"Yarn Dyeing",31=>"Fabric Dyeing",35=>"All Over Printing",64=>"Washing Charge",65=>"Stentering",68=>"Brush",101=>"Printing",102=>"Embroidery",103=>"Wash");//101 means 1, 102 means 2, 103 means 3

$emblishment_print_type=array(1=>"Rubber",2=>"Glitter",3=>"Flock",4=>"Puff",5=>"High D",6=>"Foil",7=>"Rubber+Foil",8=>"Rubber+Silver",9=>"Pigment",10=>"Rubber+Pearl",11=>"Rubber+Sugar",12=>"Transfer / Sel",13=>"Crack");
$emblishment_embroy_type=array(1=>"Applique",2=>"Plain",3=>"Sequence");
$emblishment_wash_type=array(1=>"Normal",2=>"Pigment",3=>"Acid",4=>"PP Spray/Dz",5=>"Enzyme",6=>"Enzyme+Silicon",7=>"Grinding",8=>"Cold Dry",9=>"Try Dry");
$emblishment_spwork_type=array(1=>"Stone",2=>"Bow",3=>"Ribbon",4=>"Beeds",5=>"H/Press");
$commission_particulars=array(1=>"Foreign",2=>"Local");
$commission_base_array=array(1=>" in Percentage",2=>"Per Pcs",3=>"Per Dzn");
$camarcial_items=array(1=>"LC Cost ",2=>"Port & Clearing",3=>"Transportation",4=>"All Togather");
$size_color_sensitive=array(1=>"As per Gmts. Color",2=>"Size Sensitive",3=>"Contrast Color",4=>"Color & Size Sensitive");
$shipment_status = array(0=>"ALL",1=>"Full Pending",2=>"Partial Shipment",3=>"Full Shipment");
$pay_mode=array(1=>"Credit",2=>"Import",3=>"In House",4=>"Cash");

//---------------------------------------------------------------------Start production Module Array------------------------------------------------------//
$production_type=array(1=>"cutting",2=>"printing",3=>"printreceived",4=>"sweingin",5=>"sewingout",6=>"finish_input",7=>"iron_output",8=>"garments_finish_entry");
$batch_for=array(1=>"Fabric Dyeing",2=>"Yarn Dyeing");
$batch_against=array(1=>"Buyer Order",2=>"Re-Dyeing",3=>"Sample",4=>"External",5=>"Without Booking");
$inspection_status=array(0=>"--Select--", 1=>"Passed",2=>"Re- Check",3=>"Failed");
$inspection_cause=array(0=>"--Select--", 1=>"Major",2=>"Minor");
//---------------------------------------------------------------------Start production Module Array END --------------------------------------------------//



//---------------------------------------------------------------------Start Commercial Module Array------------------------------------------------------//
$source=array(1=>"Abroad",2=>"EPZ",3=>"Non-EPZ");
$pi_basis=array(1=>"Work Order Based",2=>"Independent");
$wo_basis=array(1=>"Requisition Based",2=>"Independent",3=>"Buyer PO");
$lc_basis=array(1=>"PI Basis",2=>"Independent");
$convertible_to_lc=array(1=>"LC/SC",2=>"No",3=>"Finance");
$pay_term=array(1=>"At Sight",2=>"Usance",3=>"Cash In Advance",4=>"Open Account");
$shipment_mode=array(1=>"Sea",2=>"Air",3=>"Road",4=>"Train",5=>"Sea/Air",6=>"Road/Air"); 
$contract_source=array(1=>"Foreign",2=>"Inland");
 
$yarn_type=array(1=>"Carded",2=>"Combed",3=>"Compact",4=>"Polyster",5=>"CVC",6=>"PC",7=>"Melange",8=>"Micro Poly",9=>"Rottor",10=>"Slub",11=>"Spandex",12=>"Viscose",13=>"Modal Cotton",14=>"BCI",15=>"Modal",16=>"Semi Combed",17=>"Special",18=>"Cotton Linen",19=>"Pima",20=>"Su-Pima",21=>"Lurex",22=>"PV",23=>"Tencel",24=>"Excel/Linen");
$service_type= array(1=>"Knitting",2=>"Collar and Cuff Knitting",3=>"Feeder Stripe Knitting",10=>"Yarn Dyeing",11=>"Fabric Dyeing",12=>"All Over Printing",20=>"Scouring",21=>"Brushing",22=>"Sueding",23=>"Washing",24=>"Stentering",25=>"Compacting",40=>"Cutting",41=>"Gmts. Printing",42=>"Gmt. Embroidery",43=>"Gmts. Washing",44=>"Sewing");
//$service_type= array(1=>"AOP",2=>"Yarn Dyeing",3=>"Gmt. Print",4=>"Gmt. Embroidery",5=>"Gmt. Wash",6=>"Scouring",7=>"Brushing",8=>"Sueding",9=>"Knitting",10=>"Dyeing",11=>"Collar and Cuff Knitting",12=>"Feeder Stripe Knitting",13=>"Stripe Print Charge",20=>"Others");
//$export_finance_loan_type=array(1=>"Packing Credit",2=>"Export Cash Credit");
 
$lc_type = array(
	1	=> "BTB LC",
	2	=> "Margin LC",
	3	=> "Fund Building" 
);


$source_pay_term = array(
	1	=> "01 Import LC - At sight",
	2	=> "02 Import LC - Usance",
	3	=> "03 BTB Inland - At sight",
	4	=> "04 BTB Inland - Usance",
	5	=> "05 BTB Foreign LC - At sight",
	6	=> "06 BTB Foreign LC - Usance",
	7	=> "10 Import LC EPZ - Usance",
	8	=> "11 BTB LC EPZ - At sight",
	9	=> "12 BTB LC EPZ - Usance",
	10	=> "99 Import From Inland to EPZ" 
);

$maturity_from = array(
	1	=> "Acceptance Date",
	2	=> "Shipment Date",
	3	=> "Negotiation Date",
	4	=> "B/L Date" 
);
 $credit_to_be_advised=array(1=>"Teletransmission",2=>"Airmail",3=>"Courier",4=>"Airmail/Courier",5=>"Telex",6=>"SWIFT");
 $increase_decrease=array(1=>"Increase",2=>"Decrease");
 
$export_item_category=array(1=>"Knit Garments",2=>"Woven Garments",3=>"Sweater Garments",4=>"Leather Garments",10=>"Knit Fabric ",11=>"Woven Fabric", 20=>"Knitting", 21=>"Weaving", 22=>"Dyeing & Finishing", 22=>"All Over Printing", 24=>"Fabric Washing", 30=>"Cutting", 31=>"Sewing", 35=>"Gmts Printing", 36=>"Gmts Embroidery", 37=>"Gmts Washing", 40=>"Yarn", 45=>"Trims", 50=>"Chemical", 51=>"Dyes", 55=>"Food Item", 60=>"Medicine", 65=>"Transportation", 66=>"C & F"); 

$commercial_head = array(1=>"Negotiation Loan/Liability", 5=>"BTB Margin/DFC A/C", 6=>"ERQ A/C", 10=>"CD Account", 11=>"STD A/C", 15=>"CC Account", 16=>"OD A/C20", 20=>"Packing Credit", 21=>"Bi-Salam/PC", 22=>"Export Cash Credit", 30=>"EDF A/C", 31=>"PAD", 32=>"LTR", 33=>"FTT/TR", 34=>"LIM", 35=>"Term Loan", 40=>"IFDBC Liability", 45=>"Bank Charge", 46=>"SWIFT Charge", 47=>"Postage Charge", 48=>"Handling Charge", 49=>"Source Tax", 50=>"Excise Duty", 51=>"Foreign Collection Charge", 60=>"Other Charge", 61=>"Foreign Commision", 62=>"Local  Commision", 63=>"Penalty on Doc Descrepency", 64=>"Penalty on Goods Descrepency", 65=>"FDBC Commision", 70=>"Interest", 71=>"Import Margin A/C", 75=>"Discount A/C", 76=>"Advance A/C", 80=>"HPSM", 81=>"Sundry A/C", 82=>"MDA Special", 83=>"MDA UR");

$acceptance_time=array(1=>"After Goods Receive",2=>"Before Goods Receive");
$document_status=array(1=>"Original",2=>"Copy");
$submited_to=array(1=>"Lien Bank",2=>"Buyer");
$submission_type=array(1=>"Collection",2=>"Negotiation");
//--------------------------------------------------------------------End Commercial Module Array--------------------------------------//

//--------------------------------------------------------------- Start Accounts Module Array ------------------------------------------------//

$accounts_main_group=array(1=>"OWNERS EQUITY",
						   2=>"NON-CURRENT LIABILITIES",
						   3=>"CURRENT LIABILITIES",
						   4=>"NON-CURRENT ASSETS",
						   5=>"CURRENT ASSETS",
						   6=>"REVENUE",
						   7=>"COST OF GOOD SOLD",
						   8=>"OPERATING EXPENSES",
						   9=>"FINANCIAL EXPENSES",
						   10=>"NON-OPERATING INCOME & EXPENSE",
						   11=>"EXTRA ORDINARY ITEMS",
						   12=>"TAX EXPENSE");

$accounts_statement_type=array(1=>"Balance Sheet",
							   2=>"Income Statement");//Profit & Loss 

$accounts_account_type=array( 1=>"Credit",
							  2=>"Debit");

$accounts_cash_flow_group=array(1=>"Operating Activities",
								2=>"Investing Activities",
								3=>"Financing Activities",
								4=>"Cash & Cash Equivalents");
								
								/* OLD
								1=>"Financing Activities",
									2=>"Operating Activities",
									3=>"Investing Activities",
									4=>"Cash & Cash Equivalents",
									5=>"Operating Activities");
									*/

$accounts_journal_type=array(
						   1=>"Opening/Closing Journal",
						   2=>"Credit Purchase Journal",
						   3=>"Credit Sales Journal",
						   4=>"Cash withdrawn Journal",
						   5=>"Cash Deposit Journal",
						   6=>"Cash Receive Journal",
						   7=>"Cheque Deposit Journal",
						   8=>"Cash Payment Journal",
						   9=>"Export Realization Journal",
						   10=>"Bank Payment Journal",
						   11=>"Adjustment Journal",
						   12=>"Provisional Journal",
						   13=>"Reverse Journal",
						   14=>"Rectifying Journal",
						   15=>"General Journal");	
						   
$control_accounts = array(1=>"AP",
						  2=>"AR",
						  3=>"Import Payable",
						  4=>"Export Receivable",
						  5=>"Advance Paid",
						  6=>"Advance Received",
						  7=>"Export Negotiation Liability",
						  8=>"Other Trade Finance",
						  9=>"Tax at source from Suppliers' Bill",
						  10=>"Tax at source from Sales Bill",
						  11=>"VAT at source from Suppliers' Bill",
						  12=>"VAT at source from Sales Bill",
						  13=>"Security at source from Suppliers' Bill",
						  14=>"Security at source from Sales Bill",
						  15=>"Tax at source from Employees' Salary",
						  16=>"Discount Allowed",
						  17=>"Discount Received",
						  18=>"Write-off Assets",
						  19=>"Write-off Liability",
						  20=>"Other Subsidiary");		

  $account_nature = array(1=>"Cash",
						  2=>"Bank",
						  3=>"OD/CC",
						  4=>"Foreign Sales",
						  5=>"Local Sales",
						  6=>"Project Sales",
						  7=>"Purchase",
						  8=>"Project Cost",
						  9=>"Interest",
						  10=>"Bank Charges",
						  11=>"Currency Exchange Gain/Loss - Export",
						  12=>"Currency Exchange Gain/Loss - Import",
						  13=>"Project Common Cost",
						  14=>"Depreciation, Amortization & Depletion");	
						  
	$instrument_type = array(1=>"Bearer Cheque",
						  2=>"Crossed Cheque",
						  3=>"Pay Order",
						  4=>"TT",
						  5=>"DD",
						  6=>"Special Crossed Cheque",
						  7=>"Deposit Slip");					  
 
 	$ac_loan_type = array(
						1=>"PAD",
						2=>"LTR",
						3=>"LIM",
						10=>"Packing Credit",
						11=>"ECC",
						20=>"Term Loan",
						50=>"Project Loan");
 
  $ratio_category  = array(
						1=>"Liquidity",
						2=>"Activity",
						3=>"Leverage",
						4=>"Profitability",
						5=>"Market");
 
//------------------------------------------------------ End Accounts Module Array -------------------------------------------------------------//

//---------------all day---------------------------------------sohel -------------------------------------------------------------//


//--------------------------TNA_task----------------------------------------
 
 $general_task=array(1=>"Order Placement Date",2=>"Order Evaluation",3=>"Acceptance to be given",4=>"Internal communication to be done");
 $test_approval_task=array(1=>"Fabric test to be done",2=>"Garments test to be done");
 $purchase_task=array(1=>"Fabric booking to be issued",2=>"Trims booking to be issued",3=>"Fabric service work order to be issued");
 $material_receive_task=array(1=>"Gray fabric to be in-house",2=>"Finished fabric to be in-house",3=>"Sewing trims to be in-house",4=>"Finishing trims to be in-house");
 $fabric_production_task=array(1=>"Gray fabric production to be done",2=>"Dyeing production to be done",3=>"Finish fabric production to be done");
 $garments_production_task=array(1=>"PP meeting to be conducted",2=>"Trail cut to be done",3=>"Trail production to be submitted",4=>"Trail production approval to be received",5=>"PCD to be end",6=>"Print/Emb TOD  to be end",7=>"Sewing  to be end",8=>"Garments finishing to be done");
 $inspection_task=array(1=>"Inspection schedule to be offered",2=>"Inspection to be done");
 $export_task=array(1=>"Ex-Factory to be done",2=>"Document to be submited",3=>"Proceeds to be realized");
 $lapdip_task_name=array(1=>"Submission",2=>"Target Approval");
 $embelishment_approval_task=$emblishment_name_array;//array(1=>"Local",2=>"Imported");
 
 $category_wise_task_array=array(1=>"general_task",5=>"sample_approval_task",6=>"lapdip_task_name",7=>"trims_approval_task",8=>"embelishment_approval_task",9=>"test_approval_task",15=>"purchase_task",20=>"material_receive_task",25=>"fabric_production_task",26=>"garments_production_task",30=>"inspection_task",35=>"export_task");
  
$material_source=array(1=>"Local",2=>"Imported");
$test_approval_task=array(1=>"Fabric Approval",2=>"Garments Approval");
$inspection_task=array(1=>"Inspection Offered",2=>"Inspection Completed");
$knit_fabric_production_task=array(1=>"Knitting",2=>"Dyeing & Finishing"); 
$woven_fabric_production_task=array(1=>"Weaving",2=>"Dyeing",3=>"Finishing"); 
$material_purchase_task=array(1=>"Fabric Booking",2=>"Trims Booking",3=>"Embellishment Booking");

$knitting_source =array(1=>"In-house",2=>"In-bound Subcontract",3=>"Out-bound Subcontract");
$time_source =array(1=>"AM",2=>"PM");

//--------------------------- Start Inventory ------------- 04_03_2013  --------------------

//Yarn Receive Basis
$receive_basis_arr=array(1=>"PI Based",2=>"WO Based",3=>"In-Bound Subcontract",4=>"Independent",5=>"Batch Based",6=>"Opening Balance",7=>"Requisition",8=>"Recipe Based",9=>"Production");
//Yarn Issue Entry
$yarn_issue_purpose=array(1=>"Knitting",2=>"Yarn Dyeing",3=>"Sales",4=>"Sample With Order",5=>"Loan",6=>"Sample-material", 7=>"Yarn Test", 8=>"Sample Without Order", 9=>"Sewing Production", 10=>"Fabric Test",11=>"Fabric Dyeing");
//Inventory Variable List. Created by sohel
$inventory_module=array(8=>"ILE/Landed Cost Standard",9=>"Hide Opening Stock Flag",10=>"Item Rate Optional",11=>"Item QC",16=>"User given item code",17=>"Book Keeping Method",18=>"Allocated Quantity");
//Transaction Type
$transaction_type=array(1=>"Receive",2=>"Issue",3=>"Receive Return",4=>"Issue Return",5=>"Item Transfer Receive",6=>"Item Transfer Issue");
$issue_basis=array(1=>"Booking",2=>"Independent",3=>"Requisition");
$store_method=array(1=>"FIFO",2=>"LIFO");
$general_issue_purpose=array(1=>"Damage",2=>"Pilferage",3=>"Stolen",4=>"Unknown");
//-------------------------- End Inventory -------------------------------------------------
 

//------------------------- Start Sub. Bill ------------------ 09_03_2013 ------------------
$rate_type=array(1=>"External",2=>"Internal");	
$is_deleted=array(0=>"No",1=>"Yes");
$production_process=array(1=>"Cutting",2=>"Knitting",3=>"Dyeing",4=>"Finishing",5=>"Sewing",6=>"Fabric Printing",7=>"Washing",8=>"Printing",9=>"Embroidery");

$bill_for=array(1=>"Order",2=>"Sample with order",3=>"Sample without order");

$instrument_payment=array(1=>"Cash",2=>"Cheque",3=>"Pay Order",4=>"LC",5=>"Non-Cash");

$adjustment_type=array(1=>"Discount",2=>"Bad Debts",3=>"Write Off",5=>"Others");	

//------------------------- End Sub. Bill --------------------------------------------------

?>