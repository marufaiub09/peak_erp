<?php
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;
//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Item Group Info", "../../", 1, 1,$unicode,'','');
?>
<script>
	if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../../logout.php";
	var permission='<?php echo $permission; ?>';
			
	function fnc_item_group( operation )
	{
		if( $('#hide_group_code').val() == 1 && $('#txt_group_code').val() == "" ) 
		{	
			var bgcolor='-moz-linear-gradient(bottom, rgb(254,151,174) 0%, rgb(255,255,255) 10%, rgb(254,151,174) 96%)';					
			$('#messagebox_main', window.parent.document).fadeTo(100,1,function(){  //start fading the messagebox
				$(this).html('Please Insert Group Code').removeClass('messagebox').addClass('messagebox_error').fadeOut(2500);
			});	
			$('#txt_group_code').focus();
			 document.getElementById('txt_group_code').style.backgroundImage=bgcolor;
			return;	
		}
		
		if (form_validation('cbo_item_category*txt_item_name*cbo_order_uom*cbo_cons_uom*txt_conversion_factor*cbo_fancy_item','Item Category*Item Group*Order UOM*Cons UOM*Conversion Factor*Fancy Item')==false)
		{
			return;
		}
		
		if(document.getElementById('txt_conversion_factor').value==0)
		{      
			 var bgcolor='-moz-linear-gradient(bottom, rgb(254,151,174) 0%, rgb(255,255,255) 10%, rgb(254,151,174) 96%)';
			 $('#messagebox_main', window.parent.document).fadeTo(100,1,function() //start fading the messagebox
			 { 
				$(this).html('Value must be Greater than zero').removeClass('messagebox').addClass('messagebox_error').fadeOut(2500);
			 });
			 document.getElementById('txt_conversion_factor').focus();
			 document.getElementById('txt_conversion_factor').style.backgroundImage=bgcolor;
			 return;
		}
		if(document.getElementById('cbo_order_uom').value==document.getElementById('cbo_cons_uom').value &&  document.getElementById('txt_conversion_factor').value>1 )
		{      
			 var bgcolor='-moz-linear-gradient(bottom, rgb(254,151,174) 0%, rgb(255,255,255) 10%, rgb(254,151,174) 96%)';
			 $('#messagebox_main', window.parent.document).fadeTo(100,1,function() //start fading the messagebox
			 { 
				$(this).html('Value must be one').removeClass('messagebox').addClass('messagebox_error').fadeOut(2500);
			 });
			 document.getElementById('txt_conversion_factor').focus();
			 document.getElementById('txt_conversion_factor').style.backgroundImage=bgcolor;
			 return;
		}
		else
		{
			eval(get_submitted_variables('cbo_item_category*txt_group_code*txt_item_name*cbo_trim_type*cbo_order_uom*cbo_cons_uom*txt_conversion_factor*cbo_fancy_item*cbo_status*cbo_cal_parameter*update_id'));
			var data="action=save_update_delete&operation="+operation+get_submitted_data_string('cbo_item_category*txt_group_code*txt_item_name*cbo_trim_type*cbo_order_uom*cbo_cons_uom*txt_conversion_factor*cbo_fancy_item*cbo_status*cbo_cal_parameter*update_id',"../../");
			 
			freeze_window(operation);
			 
			http.open("POST","requires/item_group_controller.php",true);
			http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			http.send(data);
			http.onreadystatechange = fnc_item_group_reponse;
		}
	}
	
	function fnc_item_group_reponse()
	{
		if(http.readyState == 4) 
		{
			var reponse=trim(http.responseText).split('**');
			if (reponse[0].length>2) reponse[0]=10;
			show_msg(reponse[0]);
			show_list_view(reponse[1],'item_group_list_view','item_group_list_view','../item_details/requires/item_group_controller','setFilterGrid("list_view",-1)');
			reset_form('itemgroup_1','','','cbo_fancy_item,2*txt_conversion_factor,1','','');
			disable_enable_fields('txt_group_code');
			
			set_button_status(0, permission, 'fnc_item_group',1);
			release_freezing();
		}
	}
	
	function set_con_factor_value()
	{
		var order_uom=document.getElementById('cbo_order_uom').value;
		var cons_uom=document.getElementById('cbo_cons_uom').value;
		if( cons_uom*1 == order_uom*1)
		{
			document.getElementById('txt_conversion_factor').value=1;
		}
		else
		{
			document.getElementById('txt_conversion_factor').value="";
		}
	}
	
	function item_category_add(id,type)
	{
		get_php_form_data(id,type,'requires/item_group_controller');
	}

	function trim_fancy_disable(category)
	{
		if (category==4)
		{
			$('#cbo_trim_type').val('');
			//$('#cbo_trim_type').attr('disabled','disabled');
			$('#cbo_fancy_item').val('2');
			//$('#cbo_fancy_item').attr('disabled','disabled');
		}
		else
		{
			$('#cbo_trim_type').removeAttr('disabled','disabled');
			$('#cbo_fancy_item').removeAttr('disabled','disabled');
		}
	}
	
function open_rate_popup(id)
{ 
		if(id=="")
		{
		 	alert("Save Data First");
			return;
		}
		var  cbo_item_category= document.getElementById('cbo_item_category').value;
		
		var page_link="requires/item_group_controller.php?action=open_rate_popup_view&mst_id="+trim(id)+"&cbo_item_category="+trim(cbo_item_category);
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, "Rate Pop Up", 'width=400px,height=300px,center=1,resize=1,scrolling=0','../')
		emailwindow.onclose=function()
		{
		}		
}

</script>
</head>
<body onLoad="set_hotkey()">
<?php echo load_freeze_divs ("../../",$permission);  ?>
<div align="center">
	<fieldset style="width:750px;">
		<legend>Item Group Info</legend>
		<form name="itemgroup_1" id="itemgroup_1">	
			<table cellpadding="0" cellspacing="2" >
				<tr>
					<td colspan="2" valign="top">
						<table  cellpadding="0" cellspacing="2" width="100%">
			 				<tr>
								<td width="140" class="must_entry_caption">Item Category</td>
								<td width="170"> 
                                <?php
									echo create_drop_down( "cbo_item_category", 155,$item_category,"", '1', '---- Select ----', 0, "item_category_add(this.value,7);trim_fancy_disable(this.value);","","4,5,6,7,8,9,10,11,14,15,16,17,18,19,20,21,22" );
						        ?>
				   				</td>
                                <td width="130">Group Code</td>
								<td id="group_code" width="170"> 
                                    <Input name="txt_group_code" ID="txt_group_code"   style="width:145px" value="" class="text_boxes" autocomplete="off" maxlength="25" title="Maximum 25 Character">
				   				</td>
								<td width="130" class="must_entry_caption">Item Group</td>
								<td width="170"> <!-- Calander-->
                                    <Input name="txt_item_name" ID="txt_item_name"   style="width:137px" value="" class="text_boxes" autocomplete="off" maxlength="50" title="Maximum 50 Character">
								</td>
								
							</tr>	 
							<tr>
                            	<td width="122">Trims Type</td>
								<td width="170"> 
                                <?php
									echo create_drop_down( "cbo_trim_type", 155, $trim_type,"", "1", "---- Select ----", 0, "" );
						        ?>
								</td>
								<td width="130"	class="must_entry_caption">Order UOM</td>
								<td width="170"> 
									<?php 
                                      echo create_drop_down( "cbo_order_uom", 155, $unit_of_measurement,"", "", "", 1, "set_con_factor_value()","" );
                                    ?> 
								</td>
								<td width="130" class="must_entry_caption">Cons UOM</td>
								<td width="170">
									<?php 
										echo create_drop_down( "cbo_cons_uom", 150, $unit_of_measurement,"", "", "", 1, "set_con_factor_value()","" );
                                    ?>
								</td>
							</tr> 
							<tr id="fancy_item_tr_id" >
                            	<td width="130">Conv. Factor </td>
								<td width="170">
									<input name="txt_conversion_factor" id="txt_conversion_factor"   style="width:145px" class="text_boxes_numeric" value="1">
								</td>
								<td width="130" class="must_entry_caption" >Fancy Item</td>
								<td width="170" > 
                                        <?php
							            echo create_drop_down( "cbo_fancy_item", 155, $yes_no,"", "", "", 2, "" );
						                ?>
								</td>
								<td >Cal Parameter </td>
								<td >
									<?php
                                       echo create_drop_down( "cbo_cal_parameter", 150, $cal_parameter,"", 1, "--Select--", 0, "" );
                                    ?>
								</td>
							</tr>
							<tr>
                            <td >Status </td>
								<td >
									<?php
                                       echo create_drop_down( "cbo_status", 155, $row_status,"", "", "", 1, "" );
                                    ?>
								</td>
								<td colspan="4" align="center">&nbsp;
                                <input type="button" id="rate_pop_up" class="image_uploader" style="width:95px;" value="Rate Pop Up" onClick="open_rate_popup(document.getElementById('update_id').value)" /> 						
									 <input type="hidden" name="update_id" id="update_id" >
                                     <input type="hidden" name="hide_group_code" id="hide_group_code">	
								</td>					
							</tr>
			    		</table>
					</td>
			  	</tr>
				<tr>
				  <td width="670"></td>
				  <td width="157"></td>
			  	</tr>
				<tr><td colspan="6"></td></tr>
				<tr>
				  <td colspan="4" align="center" class="button_container">
						<?php 
						$dd="disable_enable_fields( 'txt_group_code',0)";
						echo load_submit_buttons( $permission, "fnc_item_group", 0,0 ,"reset_form('itemgroup_1','','','cbo_fancy_item,2*txt_conversion_factor,1',$dd)",1);
						?>	
					</td>				
				</tr>
			</table>
		</form>	
	</fieldset>	
	<div style="width:100%; float:left; margin:auto" align="center">
		<fieldset style="width:900px; margin-top:20px">
			<legend>List View</legend>
				<div style="width:900px; margin-top:10px" id="item_group_list_view" align="left">
                             <?php
							$arr=array (0=>$item_category,3=>$trim_type,4=>$unit_of_measurement,5=>$unit_of_measurement,7=>$cal_parameter);
	echo  create_list_view ( "list_view", "Item Catagory,Item Group Code,Item Group Name,Item Type,Order UOM,Cons. UOM,Conv. Factor,Cal Parameter", "150,100,200,80,50,50,50,80","900","220",0, "select id,item_category,item_group_code,item_name,trim_type,order_uom,trim_uom,conversion_factor,cal_parameter from  lib_item_group where is_deleted=0", "get_php_form_data", "id", "'load_php_data_to_form'", 1, "item_category,0,0,trim_type,order_uom,trim_uom,0,cal_parameter", $arr , "item_category,item_group_code,item_name,trim_type,order_uom,trim_uom,conversion_factor,cal_parameter", "../item_details/requires/item_group_controller", 'setFilterGrid("list_view",-1);','0,0,0,0,0,0,2' ) ;
							 ?>
                </div>
		</fieldset>	
	</div>
</div>
</body>
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
