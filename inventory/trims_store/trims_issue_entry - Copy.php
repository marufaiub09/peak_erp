<?php
/*-------------------------------------------- Comments
Purpose			: 	This form will create Trims Issue Entry 
Functionality	:	
JS Functions	:
Created by		:	Fuad Shahriar 
Creation date 	: 	22/09/2013
Updated by 		: 	Kausar (Creating Print Report)		
Update date		: 	12-01-2014	   
QC Performed BY	:		
QC Date			:	
Comments		:
*/

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;
//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Trims Issue Entry", "../../", 1, 1,'','1',''); 
?>
<script>

	if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../../logout.php"; 
	var permission='<?php echo $permission; ?>';

	
	function openmypage_ItemDescription()
	{
		var cbo_company_id = $('#cbo_company_id').val();
		var all_po_id = $('#all_po_id').val();
		
		if (form_validation('cbo_company_id','Company')==false)
		{
			return;
		}
		else
		{ 	
			var title = 'Item Description Info';	
			var page_link = 'requires/trims_issue_entry_controller.php?cbo_company_id='+cbo_company_id+'&all_po_id='+all_po_id+'&action=itemDescription_popup';
			  
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=820px,height=400px,center=1,resize=1,scrolling=0','../');
			
			emailwindow.onclose=function()
			{
				var theform=this.contentDoc.forms[0]//("search_order_frm"); //Access the form inside the modal window
				var prod_id=this.contentDoc.getElementById("hidden_prod_id").value;	 //Knit Id for Kintting Plan
				var all_data=this.contentDoc.getElementById("hidden_data").value; //Access form field with id="emailfield"
				
				var data=all_data.split("**");

				$('#hidden_prod_id').val(prod_id);
				$('#cbo_item_group').val(data[0]);
				$('#txt_item_description').val(data[1]);	
				$('#txt_gmts_color').val(data[2]);
				$('#gmts_color_id').val(data[3]);
				$('#txt_gmts_size').val(data[4]);
				$('#gmts_size_id').val(data[5]);		
				$('#txt_brand_supref').val(data[6]);
				$('#cbo_uom').val(data[7]);
			}
		}
	}
	
	function openmypage_po()
	{
		var cbo_company_id = $('#cbo_company_id').val();
		var save_data = $('#save_data').val();
		var all_po_id = $('#all_po_id').val();

		if (form_validation('cbo_company_id','Company')==false)
		{
			return;
		}
			
		var title = 'PO Info';	
		var page_link = 'requires/trims_issue_entry_controller.php?cbo_company_id='+cbo_company_id+'&save_data='+save_data+'&all_po_id='+all_po_id+'&action=po_popup';
		  
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=650px,height=370px,center=1,resize=1,scrolling=0','../');
		emailwindow.onclose=function()
		{
			var theform=this.contentDoc.forms[0]//("search_order_frm"); //Access the form inside the modal window
			var save_string=this.contentDoc.getElementById("save_string").value;	 //Access form field with id="emailfield"
			var tot_trims_issue_qnty=this.contentDoc.getElementById("tot_trims_qnty").value; //Access form field with id="emailfield"
			var all_po_id=this.contentDoc.getElementById("all_po_id").value; //Access form field with id="emailfield"
			var all_po_no=this.contentDoc.getElementById("all_po_no").value; //Access form field with id="emailfield"
			var buyer_name=this.contentDoc.getElementById("buyer_name").value; //Access form field with id="emailfield"

			$('#save_data').val(save_string);
			$('#txt_issue_qnty').val(tot_trims_issue_qnty);
			$('#all_po_id').val(all_po_id);
			$('#txt_buyer_order').val(all_po_no);
			$('#txt_buyer_name').val(buyer_name);
			show_list_view(all_po_id,'create_itemDesc_search_list_view','list_fabric_desc_container','requires/trims_issue_entry_controller','');
			get_php_form_data(all_po_id+"**"+$('#hidden_prod_id').val(), 'get_trim_cum_info', 'requires/trims_issue_entry_controller' );
		}
	}
	
	
	function fnc_trims_issue(operation)
	{
		if(operation==4)
		{
			 var report_title=$( "div.form_caption" ).html();
			 print_report( $('#cbo_company_id').val()+'*'+$('#update_id').val()+'*'+report_title, "trims_issue_entry_print", "requires/trims_issue_entry_controller" ) 
			 return;
		}
		else if(operation==0 || operation==1 || operation==2)
		{
			if(operation==2)
			{
				show_msg('13');
				return;
			}
			
			if( form_validation('cbo_company_id*txt_issue_date*txt_issue_chal_no*cbo_store_name*txt_item_description*txt_buyer_order*txt_issue_qnty','Company*Issue Date*Challan No*Store Name**Item Description*Buyer Order*Issue Qnty')==false )
			{
				return;
			}
			
			var data="action=save_update_delete&operation="+operation+get_submitted_data_string('txt_system_id*cbo_company_id*txt_issue_date*txt_issue_chal_no*cbo_store_name*cbo_item_group*cbo_uom*txt_item_description*hidden_prod_id*gmts_color_id*gmts_size_id*txt_brand_supref*txt_buyer_order*txt_issue_qnty*hidden_issue_qnty*update_id*all_po_id*update_dtls_id*update_trans_id*save_data*previous_prod_id',"../../");
			
			freeze_window(operation);
			
			http.open("POST","requires/trims_issue_entry_controller.php",true);
			http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			http.send(data);
			http.onreadystatechange =fnc_trims_issue_Reply_info;
		}
	}
	
	function fnc_trims_issue_Reply_info()
	{
		
		if(http.readyState == 4) 
		{
			 //alert(http.responseText);return;
			var reponse=trim(http.responseText).split('**');	
				
			show_msg(reponse[0]);
			
			if((reponse[0]==0 || reponse[0]==1))
			{
				document.getElementById('update_id').value = reponse[1];
				document.getElementById('txt_system_id').value = reponse[2];
				$('#cbo_company_id').attr('disabled','disabled');
				
				reset_form('trimsissue_1','','','','','update_id*txt_system_id*cbo_company_id*txt_issue_date*txt_issue_chal_no*cbo_store_name');
				
				show_list_view(reponse[1],'show_trims_listview','div_details_list_view','requires/trims_issue_entry_controller','');
			}

			set_button_status(reponse[3], permission, 'fnc_trims_issue',1,1);	
			release_freezing();	
		}
	}
	
	function openmypage_systemId()
	{
		var cbo_company_id = $('#cbo_company_id').val();
		
		if (form_validation('cbo_company_id','Company')==false)
		{
			return;
		}
		else
		{ 	
			var page_link='requires/trims_issue_entry_controller.php?cbo_company_id='+cbo_company_id+'&action=trims_issue_popup_search';
			var title='Trims Issue Form';
			
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=800px,height=390px,center=1,resize=1,scrolling=0','../');
			emailwindow.onclose=function()
			{
				var theform=this.contentDoc.forms[0];
				var trims_issue_id=this.contentDoc.getElementById("hidden_issue_id").value;

				if(trims_issue_id!="")
				{
					freeze_window(5);
					reset_form('trimsissue_1','div_details_list_view','','','','cbo_company_id');
					get_php_form_data(trims_issue_id, "populate_data_from_trims_issue", "requires/trims_issue_entry_controller" );
					
					show_list_view(trims_issue_id,'show_trims_listview','div_details_list_view','requires/trims_issue_entry_controller','');
					set_button_status(0, permission, 'fnc_trims_issue',1,1);
					release_freezing();
				}
							 
			}
		}
	}
	
	function calculate()
	{
		//amount and book currency calculate--------------//
		var currency_id 	= $("#cbo_currency_id").val();
		var quantity 		= $("#txt_receive_qnty").val();
		var exchangeRate 	= $("#txt_exchange_rate").val();
		var rate			= $('#txt_rate').val();	 
		var ile_cost 		= $("#txt_ile").val();
		var amount 			= quantity*1*(rate*1+ile_cost*1); 
		var bookCurrency 	= (rate*1+ile_cost*1)*exchangeRate*1*quantity*1;
		$("#txt_amount").val(number_format_common(amount,"","",currency_id));
		$("#txt_book_currency").val(number_format_common(bookCurrency,"","",1));
	}
	
	function openmypage_goodsPlacement()
	{
		var update_dtls_id = $('#update_dtls_id').val();
		
		if (form_validation('update_dtls_id','Save First')==false)
		{
			alert('Please Save First.');
			return;
		}
		else
		{ 	
			var page_link='requires/trims_issue_entry_controller.php?update_dtls_id='+update_dtls_id+'&action=goods_placement_popup';
			var title='Goods Placement Entry Form';
			
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=800px,height=390px,center=1,resize=1,scrolling=0','../');
			
		}
	}
	
</script>
<body onLoad="set_hotkey()">
<div style="width:100%;" align="left">
	<?php echo load_freeze_divs ("../../",$permission);  ?><br />    		 
    <form name="trimsissue_1" id="trimsissue_1" autocomplete="off" >
    <div style="width:100%;">   
        <fieldset style="width:680px;">
        <legend>Trims Issue Entry</legend>
        <br>
        	<fieldset style="width:680px;">
                <table width="670" cellspacing="2" cellpadding="2" border="0" id="tbl_master" align="center">
                    <tr>
                        <td colspan="3" align="right"><strong>Issue No</strong></td>
                        <td colspan="3" align="left">
                        	 <input type="hidden" name="update_id" id="update_id" />
                            <input type="text" name="txt_system_id" id="txt_system_id" class="text_boxes" style="width:150px;" placeholder="Double click to search" onDblClick="openmypage_systemId();" readonly />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="80" class="must_entry_caption"> Company </td>
                        <td>
                            <?php 
                                echo create_drop_down( "cbo_company_id", 140, "select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name","id,company_name", 1, "--Select Company--", 0, "load_drop_down( 'requires/trims_issue_entry_controller',this.value, 'load_drop_down_store', 'store_td' );" );
                            ?>
                        </td>
                        <td width="80" class="must_entry_caption"> Issue Date </td>
                        <td width="100">
                            <input class="datepicker" type="date" style="width:90px" name="txt_issue_date" id="txt_issue_date"/>
                        </td>
                        <td width="100" class="must_entry_caption">Issue Challan No </td>
                        <td width="140">
                            <input type="text" name="txt_issue_chal_no" id="txt_issue_chal_no" class="text_boxes" style="width:130px" >
                        </td>
                    </tr> 
                    <tr>
                        <td class="must_entry_caption">Store Name </td>
                        <td id="store_td">
                            <?php
                                echo create_drop_down( "cbo_store_name", 140, $blank_array,"",1, "--Select store--", 1, "" );
                            ?>
                        </td> 
                    </tr>
                </table>
            </fieldset>
            <br>
            <table width="670" cellspacing="2" cellpadding="2" border="0" id="tbl_dtls" align="center">
                <tr>
                    <td width="65%" valign="top">
                        <fieldset>
                        <legend>New Entry</legend>
                            <table cellpadding="0" cellspacing="2" width="100%">
                                <tr>
                                    <td class="must_entry_caption">Buyer Order</td>
                                    <td>
                                        <input type="text" name="txt_buyer_order" id="txt_buyer_order" class="text_boxes" style="width:135px;" onClick="openmypage_po()" placeholder="Single Click" readonly/>	
                                    </td>
                                    <td width="100">UOM</td>
                                    <td>
                                        <?php
                                            echo create_drop_down( "cbo_uom", 100, $unit_of_measurement,"", 1, "-- Select UOM --", '0', "",1 );
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="100">Item Group</td>
                                    <td>
                                    <?php
                                        echo create_drop_down( "cbo_item_group", 145, "select id,item_name from lib_item_group where item_category=4 and status_active and is_deleted=0 order by item_name", "id,item_name", 1, "-- Select --", 0,  "",1 );
                                    ?>	
                                    </td>
                                    <td>Brand/Sup Ref</td>
                                    <td>
                                        <input type="text" name="txt_brand_supref" id="txt_brand_supref" class="text_boxes" style="width:90px" disabled/>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="must_entry_caption">Item Des.</td>
                                    <td>
                                        <input type="text" name="txt_item_description" id="txt_item_description" class="text_boxes" style="width:135px" placeholder="Double click to search" onDblClick="openmypage_ItemDescription();" readonly />
                                        <input type="hidden" name="hidden_prod_id" id="hidden_prod_id" disabled/>
                                    </td>
                                    <td>Gmts Size</td>   
                                    <td>
                                        <input type="text" name="txt_gmts_size" id="txt_gmts_size" class="text_boxes" style="width:90px;" disabled/>
                                        <input type="hidden" name="gmts_size_id" id="gmts_size_id" class="text_boxes" style="width:100px;" disabled/>
                                    </td>
                                </tr> 
                                <tr>
                                    <td>Gmts Color</td>
                                    <td>
                                        <input type="text" name="txt_gmts_color" id="txt_gmts_color" class="text_boxes" style="width:135px;" disabled/>
                                        <input type="hidden" name="gmts_color_id" id="gmts_color_id" class="text_boxes" style="width:100px;" disabled/>
                                    </td>
                                    <td class="must_entry_caption">Issue Qnty</td>
                                    <td>
                                        <input type="text" name="txt_issue_qnty" id="txt_issue_qnty" class="text_boxes_numeric" style="width:90px;" disabled/>	
                                    </td>
                                </tr>
                                <tr>
                                	<td align="right" colspan="3" style="display:none"><input type="button" class="formbuttonplasminus" style="width:150px" value="Goods Placement" onClick="openmypage_goodsPlacement();"></td> 
                                </tr> 
                             </table>
                        </fieldset>
					</td>
                    <td width="2%" valign="top"></td>
					<td width="33%" valign="top">
						<fieldset>
                        <legend>Display</legend>					
                            <table id="tbl_display_info"  cellpadding="0" cellspacing="1" width="100%" >				
                                <tr>
                                    <td>Recv. Qty</td>						
                                	<td>
                                    	<input type="text" name="txt_received_qnty" id="txt_received_qnty" class="text_boxes_numeric" placeholder="Display" style="width:80px" disabled />
                                    </td>
								</tr>
                                <tr>
                                    <td>Cumul. Issued</td>
                                    <td><input type="text" name="txt_cumulative_issued" id="txt_cumulative_issued" class="text_boxes_numeric" placeholder="Display" style="width:80px" disabled /></td>
                                </tr>
                                <tr>
                                    <td>Yet to Issue</td>
                                    <td><input type="text" name="txt_yet_to_issue" id="txt_yet_to_issue" class="text_boxes_numeric" placeholder="Display" style="width:80px" disabled /></td>
                                </tr>					
                               	<tr>
                                    <td>Buyer</td>						
                                    <td><input type="text" name="txt_buyer_name" id="txt_buyer_name" class="text_boxes" placeholder="Display" style="width:80px" disabled /></td>
                                </tr>								
                            </table>                  
                       </fieldset>	
              		</td>
				</tr>
                <tr>
                    <td align="center" colspan="3" class="button_container" width="100%">
                        <?php
                            echo load_submit_buttons($permission, "fnc_trims_issue", 0,1,"reset_form('trimsissue_1','div_details_list_view','','','disable_enable_fields(\'cbo_company_id\');')",1);
                        ?>
                        <input type="hidden" name="save_data" id="save_data" readonly>
                        <input type="hidden" name="update_dtls_id" id="update_dtls_id" readonly>
                        <input type="hidden" name="update_trans_id" id="update_trans_id" readonly>
                        <input type="hidden" name="previous_prod_id" id="previous_prod_id" readonly>
                        <input type="hidden" name="all_po_id" id="all_po_id" readonly>
                        <input type="hidden" name="hidden_issue_qnty" id="hidden_issue_qnty" readonly>
                    </td>
                </tr>
			</table>
		</fieldset>
	</div>
         <div style="width:880px;" id="div_details_list_view"></div>
         <div id="list_fabric_desc_container" style="max-height:500px; width:540px; overflow:auto; float:left; padding-top:5px; margin-top:5px; position:relative;"></div>
	</form>
</div>   
</body>
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>