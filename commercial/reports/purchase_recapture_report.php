<?php
/*-------------------------------------------- Comments -----------------------
Purpose			: 	This Form Will Create Purchase Recapture Report.
Functionality	:	
JS Functions	:
Created by		:	Jahid 
Creation date 	: 	06-11-2014
Updated by 		: 		
Update date		: 		   
QC Performed BY	:		
QC Date			:	
Comments		:
*/

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;

//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Purchase Recapture Report", "../../", 1, 1,'','','');
?>	

<script>

 	if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../../logout.php";  
 
	var permission = '<?php echo $permission; ?>';
	
	function generate_report()
	{
		if(form_validation('cbo_company_name*cbo_item_category_id*txt_date_from*txt_date_to','Company Name*Item Category*date from*date to')==false)
		{
			return;
		}
		
		var report_title=$( "div.form_caption" ).html();	
		var data="action=report_generate"+get_submitted_data_string('cbo_company_name*cbo_search_date*cbo_item_category_id*txt_date_from*txt_date_to',"../../")+'&report_title='+report_title;
		freeze_window(3);
		http.open("POST","requires/purchase_recapture_report_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = fn_report_generated_reponse;
	}
	
	function fn_report_generated_reponse()
	{
		if(http.readyState == 4) 
		{
			var response=trim(http.responseText).split("****");
			$('#report_container2').html(response[0]);
			document.getElementById('report_container').innerHTML='<a href="requires/'+response[1]+'" style="text-decoration:none"><input type="button" value="Excel Preview" name="excel" id="excel" class="formbutton" style="width:100px"/></a>&nbsp;&nbsp;<input type="button" onclick="new_window()" value="Print Preview" name="Print" class="formbutton" style="width:100px"/>';
			
			/*var tableFilters = { 
				col_0: "none", 
				col_operation: {
					id: ["value_total_opening_balance","value_total_purchase","value_total_inside_return","value_total_outside_return","value_total_rcv_loan","value_total_total_rcv","value_total_issue_inside","value_total_issue_outside","value_total_receive_return","value_total_issue_loan","value_total_total_delivery","value_total_stock_in_hand","value_total_alocatted","value_total_free_stock"],
					col: [8,9,10,11,12,13,14,15,16,17,18,19,20,21],
					operation: ["sum","sum","sum","sum","sum","sum","sum","sum","sum","sum","sum","sum","sum","sum","sum"],
					write_method: ["innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML"]
				}
			}*/
			
			setFilterGrid("table_body",-1);
			
			show_msg('3');
			release_freezing();
		}
	}
	
	function new_window()
	{
		document.getElementById('scroll_body').style.overflow="auto";
		document.getElementById('scroll_body').style.maxHeight="none";
		
		$("#table_body tr:first").hide();
		
		var w = window.open("Surprise", "#");
		var d = w.document.open();
		d.write ('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN""http://www.w3.org/TR/html4/strict.dtd">'+
	'<html><head><title></title><link rel="stylesheet" href="../../css/style_common.css" type="text/css" /></head><body>'+document.getElementById('report_container2').innerHTML+'</body</html>');// media="print"
		d.close();
		
		document.getElementById('scroll_body').style.overflowY="scroll";
		document.getElementById('scroll_body').style.maxHeight="300px";
		$("#table_body tr:first").show();

	}
	
	function show_qty_details(pi_id,category_id)
	{
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'requires/purchase_recapture_report_controller.php?pi_id='+pi_id+'&category_id='+category_id+'&action=receive_qnty', "Receive Wise Quantity", 'width=550px,height=330px,center=1,resize=0,scrolling=0','../');
	}
	
	
	function search_populate(str)
	{
		if(str==1)
		{
			//document.getElementById('search_by_th_up').innerHTML="PI Date";
			$('#search_by_th_up').removeClass().addClass('must_entry_caption').html("PI Date").attr('style','color:blue');
		}
		else if(str==2)
		{
			$('#search_by_th_up').removeClass().addClass('must_entry_caption').html("Purchase Requisition Date").attr('style','color:blue');
		
		}
		else if(str==3)
		{
			$('#search_by_th_up').removeClass().addClass('must_entry_caption').html("WO Date").attr('style','color:blue');
		
		}
	}	

</script>

</head>

<body onLoad="set_hotkey();">
<form id="PurchaseRecap_report">
    <div style="width:100%;" align="center">
        <?php echo load_freeze_divs ("../../",''); ?>
        <h3 align="left" id="accordion_h1" style="width:810px" class="accordion_h" onClick="accordion_menu( this.id,'content_search_panel', '')"> -Search Panel</h3>
        <div id="content_search_panel" > 
            <fieldset style="width:800px;">
                <table class="rpt_table" width="800" border="1" rules="all" cellpadding="1" cellspacing="2" align="center">
                	<thead>
                   		<tr>                    
                            <th class="must_entry_caption">Company Name</th>
                            <th>Date Type</th>
                            <th id="search_by_th_up" class="must_entry_caption">PI Date</th>
                            <th class="must_entry_caption">Item Category</th>
                            <th><input type="reset" id="reset_btn" class="formbutton" style="width:100px" value="Reset" onClick="reset_form('PurchaseRecap_report','report_container*report_container2','','','')" /></th>
                        </tr>
                     </thead>
                    <tbody>
                        <tr class="general">
                            <td> 
                                <?php
                                    echo create_drop_down( "cbo_company_name", 140, "select comp.id, comp.company_name from lib_company comp where comp.status_active =1 and comp.is_deleted=0 $company_cond order by comp.company_name","id,company_name", 1, "-- Select Company --", $selected, "" );
                                ?>
                            </td>
                            <td>
								<?php  
									$search_by = array(1=>'PI Date',2=>'Purchase Requisition Date',3=>'WO Date');
									$dd="search_populate(this.value)";
									echo create_drop_down( "cbo_search_date", 120, $search_by,"",1, "--Select--", $selected,$dd,0 );
                                ?>                            
                           	</td>
                            <td align="center">
                             <input type="text" name="txt_date_from" id="txt_date_from" value="" class="datepicker" style="width:75px" placeholder="From Date"/>
                             To
                             <input type="text" name="txt_date_to" id="txt_date_to" value="" class="datepicker" style="width:75px" placeholder="To Date"/>
                        	</td>
                            <td>
                                <?php
								//function create_drop_down( $field_id, $field_width, $query, $field_list, $show_select, $select_text_msg, $selected_index, $onchange_func, $is_disabled, $array_index, $fixed_options, $fixed_values, $not_show_array_index, $tab_index, $new_conn, $field_name )
								 echo create_drop_down( "cbo_item_category_id", 130, $item_category,'', 1, '-- Select --',0,"",0,"","","","13,14"); 
								 ?>
                            </td>
                           <td>
                                <input type="button" name="search" id="search" value="Show" onClick="generate_report()" style="width:100px" class="formbutton" />
                            </td>
                        </tr>
                        <tr>
                        	<td colspan="5" align="center" width="95%"><?php echo load_month_buttons(1); ?></td>
                        </tr>
                    </tbody>
                </table>
            </fieldset>
        </div>
    </div>
    
    <div style="margin-top:10px" id="report_container" align="center"></div>
    <div id="report_container2"></div>
 </form>    
</body>
<script src="../../includes/functions_bottom.js" type="text/javascript"></script> 
</html>
