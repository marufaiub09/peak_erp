<?php
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

include('../../../includes/common.php');

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

if ($action=="save_update_delete")
{
	
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	if ($operation==0)  // Insert Here
	{
		if (is_duplicate_field( "buyer_name", "lib_buyer", "buyer_name=$txt_buyer_name and is_deleted=0" ) == 1)
		{
			echo "11**0"; die;
		}
		else
		{
			$con = connect();
			if($db_type==0)
			{
				mysql_query("BEGIN");
			}
			
			$id=return_next_id( "id", "lib_buyer", 1 );
			$field_array="id, buyer_name, short_name, contact_person, exporters_reference, party_type, designation, tag_company, country_id, web_site, buyer_email, address_1, address_2, address_3, address_4,remark,supllier, credit_limit_days, credit_limit_amount,credit_limit_amount_currency, discount_method, securitye_deducted, vat_to_be_deducted, ait_to_be_deducted,sewing_effi_mkt_percent,sewing_effi_plaing_per,marketing_team_id,inserted_by, insert_date, status_active,is_deleted";
			$data_array="(".$id.",".$txt_buyer_name.",".$txt_short_name.",".$txt_contact_person.",".$txt_exporter_ref.",".$cbo_party_type.",".$txt_desination.",".$cbo_buyer_company.",".$cbo_country.",".$txt_web_site.",".$txt_buyer_email.",".$txt_address_1st.",".$txt_address_2nd.",".$txt_address_3rd.",".$txt_address_4th.",".$txt_remark.",".$cbo_buyer_supplier.",".$txt_credit_limit_days.",".$txt_credit_limit_amount.",".$cbo_credit_limit_amount_curr.",".$cbo_discount_method.",".$cbo_security_deducted.",".$cbo_vat_to_be_deducted.",".$cbo_ait_to_be_deducted.",".$txt_sewing_effi_mkt.",".$txt_sewing_effi_planing.",".$cbo_marketing_team.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."',".$cbo_status.",'0')";
			$rID=sql_insert("lib_buyer",$field_array,$data_array,0);
			
			//Insert Data in lib_buyer_party_type Table----------------------------------------
			$data_array="";
			$party_type=explode(',',str_replace("'","",$cbo_party_type));
			for($i=0; $i<count($party_type); $i++)
			{
				if($id_lib_buyer_party_type=="") $id_lib_buyer_party_type=return_next_id( "id", "lib_buyer_party_type", 1 ); else $id_lib_buyer_party_type=$id_lib_buyer_party_type+1;
				if($i==0) $add_comma=""; else $add_comma=",";
				$data_array.="$add_comma(".$id_lib_buyer_party_type.",".$id.",".$party_type[$i].")";
			}
			$field_array="id, buyer_id, party_type";
			$rID1=sql_insert("lib_buyer_party_type",$field_array,$data_array,0);
			//----------------------------------------------------------------------------------
			//Insert Data in  lib_buyer_tag_company Table----------------------------------------
			$data_array="";
			$tag_company=explode(',',str_replace("'","",$cbo_buyer_company));
			for($i=0; $i<count($tag_company); $i++)
			{
				if($id_lib_buyer_tag_company=="") $id_lib_buyer_tag_company=return_next_id( "id", "lib_buyer_tag_company", 1 ); else $id_lib_buyer_tag_company=$id_lib_buyer_tag_company+1;
				if($i==0) $add_comma=""; else $add_comma=",";
				$data_array.="$add_comma(".$id_lib_buyer_tag_company.",".$id.",".$tag_company[$i].")";
			}
			 $field_array="id, buyer_id, tag_company";
			 $rID2=sql_insert("lib_buyer_tag_company",$field_array,$data_array,1);
			
			//----------------------------------------------------------------------------------
			
			if($db_type==0)
			{
				if($rID  && $rID1  && $rID2 ){
					mysql_query("COMMIT");  
					echo "0**".$rID;
				}
				else{
					mysql_query("ROLLBACK"); 
					echo "10**".$rID;
				}
			}
			
			if($db_type==2 || $db_type==1 )
			{
				if($rID  && $rID1  && $rID2 )
					{
						oci_commit($con);  
						echo "0**".$rID;
					}
				else{
					oci_rollback($con);
					echo "10**".$rID;
					}
			}
			disconnect($con);
			die;
		}
	}
	
	else if ($operation==1)   // Update Here
	{
		if (is_duplicate_field( "buyer_name", "lib_buyer", "buyer_name=$txt_buyer_name  and id!=$update_id and is_deleted=0" ) == 1)
		{
			echo "11**0"; die;
		}
		else
		{
			$con = connect();
			if($db_type==0)
			{
				mysql_query("BEGIN");
			}
			$id=return_next_id( "id", "lib_buyer", 1 ) ;
			$field_array="buyer_name*short_name*contact_person*exporters_reference*party_type*designation*tag_company*country_id*web_site*buyer_email*address_1*address_2*address_3* address_4*remark*supllier*credit_limit_days*credit_limit_amount*credit_limit_amount_currency*discount_method*securitye_deducted*vat_to_be_deducted*ait_to_be_deducted*sewing_effi_mkt_percent*sewing_effi_plaing_per*marketing_team_id*updated_by*update_date *status_active*is_deleted";
			$data_array="".$txt_buyer_name."*".$txt_short_name."*".$txt_contact_person."*".$txt_exporter_ref."*".$cbo_party_type."*".$txt_desination."*".$cbo_buyer_company."*".$cbo_country."*".$txt_web_site."*".$txt_buyer_email."*".$txt_address_1st."*".$txt_address_2nd."*".$txt_address_3rd."*".$txt_address_4th."*".$txt_remark."*".$cbo_buyer_supplier."*".$txt_credit_limit_days."*".$txt_credit_limit_amount."*".$cbo_credit_limit_amount_curr."*".$cbo_discount_method."*".$cbo_security_deducted."*".$cbo_vat_to_be_deducted."*".$cbo_ait_to_be_deducted."*".$txt_sewing_effi_mkt."*".$txt_sewing_effi_planing."*".$cbo_marketing_team."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'*".$cbo_status."*'0'";
			$rID=sql_update("lib_buyer",$field_array,$data_array,"id","".$update_id."",0);
			
			//Insert Data in lib_buyer_party_type Table----------------------------------------
			$rID1=execute_query( "delete from lib_buyer_party_type where  buyer_id = $update_id",0);
			$data_array="";
			$party_type=explode(',',str_replace("'","",$cbo_party_type));
			for($i=0; $i<count($party_type); $i++)
			{
				if($id_lib_buyer_party_type=="") $id_lib_buyer_party_type=return_next_id( "id", "lib_buyer_party_type", 1 ); else $id_lib_buyer_party_type=$id_lib_buyer_party_type+1;
				if($i==0) $add_comma=""; else $add_comma=",";
				$data_array.="$add_comma(".$id_lib_buyer_party_type.",".$update_id.",".$party_type[$i].")";
			}
			$field_array="id, buyer_id, party_type";
			$rID2=sql_insert("lib_buyer_party_type",$field_array,$data_array,0);
			//------------------------------------------------------------------------------------
			
			//Insert Data in  lib_buyer_tag_company Table----------------------------------------
			$rID3=execute_query( "delete from lib_buyer_tag_company where  buyer_id = $update_id",0);
			$data_array="";
			$tag_company=explode(',',str_replace("'","",$cbo_buyer_company));
			for($i=0; $i<count($tag_company); $i++)
			{
				if($id_lib_buyer_tag_company=="") $id_lib_buyer_tag_company=return_next_id( "id", "lib_buyer_tag_company", 1 ); else $id_lib_buyer_tag_company=$id_lib_buyer_tag_company+1;
				if($i==0) $add_comma=""; else $add_comma=",";
				$data_array.="$add_comma(".$id_lib_buyer_tag_company.",".$update_id.",".$tag_company[$i].")";
			}
			$field_array="id, buyer_id, tag_company";
			$rID4=sql_insert("lib_buyer_tag_company",$field_array,$data_array,1);
		
			//----------------------------------------------------------------------------------
			
			if($db_type==0)
			{
				if($rID && $rID1 && $rID2 && $rID3 && $rID4)
				{
					mysql_query("COMMIT");  
					echo "1**".$rID;
				}
				else
				{
					mysql_query("ROLLBACK"); 
					echo "10**".$rID;
				}
			}
			if($db_type==2 || $db_type==1 )
			{
			  if($rID && $rID1 && $rID2 && $rID3 && $rID4)
				{
					oci_commit($con);   
					echo "1**".$rID;
				}
				else
				{
					oci_rollback($con); 
					echo "10**".$rID;
				}
			}
			disconnect($con);
			die;
		}
	}
	else if ($operation==2)   // Delete Here
	{
		
		//$s="select job_no from wo_po_details_master where buyer_name=$hidden_buyer_id  and status_active=1 and is_deleted=0";
		$marchandising_job=return_field_value("min(job_no) as job_no", "wo_po_details_master", "buyer_name=$hidden_buyer_id  and status_active=1 and is_deleted=0","job_no");
		$sub_contract_job=return_field_value("min(subcon_job) as job_no" , "subcon_ord_mst", "party_id=$hidden_buyer_id  and status_active=1 and is_deleted=0","job_no");
		$sample_develop=return_field_value("id as sam_dev_id", "sample_development_mst", "buyer_name=$hidden_buyer_id  and status_active=1 and is_deleted=0","sam_dev_id");
		$price_quotation=return_field_value("id as pq_id", "wo_price_quotation", "buyer_id=$hidden_buyer_id  and status_active=1 and is_deleted=0","pq_id");
		$quotation_inquiry=return_field_value("system_number as system_number", "wo_quotation_inquery", "buyer_id=$hidden_buyer_id  and status_active=1 and is_deleted=0","system_number");
		$buyer_quotation_final=return_field_value("id as quotation_final", "wo_pri_sim_mst", "buyer_id=$hidden_buyer_id  and status_active=1 and is_deleted=0","quotation_final");
		$sales_contract=return_field_value("contact_system_id as contact_system_id", "com_sales_contract", "buyer_name=$hidden_buyer_id  and status_active=1 and is_deleted=0","contact_system_id");
		$export_lc=return_field_value("export_lc_system_id 	 as export_lc_system_id", "com_export_lc", "buyer_name=$hidden_buyer_id  and status_active=1 and is_deleted=0","export_lc_system_id");
			
		if($marchandising_job!="" || $sub_contract_job!="" || $sample_develop!="" || $price_quotation!="" || $quotation_inquiry!="" || $buyer_quotation_final!="" || $sales_contract!="" || $export_lc!="")
		{
		echo "50**Some Entries Found For This Buyer, Deleting Not Allowed, \n Merchandising Job: ".$marchandising_job."\n Sub Contract Job: ".$sub_contract_job."\n Sample Develop ID: ".$sample_develop."\n Price Quotation ID: ".$price_quotation."\n Quotation Inquiry ID: ".$quotation_inquiry."\n Buyer Quotation: ".$buyer_quotation_final."\n Sales Contract: ".$sales_contract."\n Export LC: ".$export_lc;	
		
		 die;
		}

	/*	if (is_duplicate_field( "buyer", "lib_supplier", "buyer=$update_id and is_deleted=0" ) == 1)
		{
			echo "13**0"; die;
		}*/
		else
		{
			$con = connect();
			if($db_type==0)
			{
				mysql_query("BEGIN");
			}
			$field_array="updated_by*update_date*status_active*is_deleted";
			$data_array="".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'*0*1";
			$rID=sql_delete("lib_buyer",$field_array,$data_array,"id","".$update_id."",1);
		
			if($db_type==0)
			{
				if($rID )
				{
					mysql_query("COMMIT");  
					echo "2**".$rID;
				}
				else
				{
					mysql_query("ROLLBACK"); 
					echo "10**".$rID;
				}
			}
			if($db_type==2 || $db_type==1 )
			{
				if($rID )
					{
						oci_commit($con);  
						echo "2**".$rID;
					}
				else
					{
						oci_rollback($con); 
						echo "10**".$rID;
					}
			}
			disconnect($con);
			die;
		}
	}
}

if ($action=="show_buyer_list_view")
{
		$arr=array (2=>$party_type,7=>$currency,8=>$row_status);
		echo create_list_view ( "list_view", "Contact Name,Short Name,Sewing Effi Mkt. %,Contact Person,Designation,Credit Limit(Days),Credit Limit (Amount),Currency, Status", "150,100,150,100,120,100,100,70","1010","220",0, "select buyer_name,short_name,sewing_effi_mkt_percent,contact_person,designation,credit_limit_days,credit_limit_amount, 	credit_limit_amount_currency,status_active,id from lib_buyer where is_deleted=0", "get_php_form_data", "id", "'load_php_data_to_form'", 1, "0,0,0,0,0,0,0,credit_limit_amount_currency,status_active", $arr , "buyer_name,short_name,sewing_effi_mkt_percent,contact_person,designation,credit_limit_days,credit_limit_amount,credit_limit_amount_currency,status_active", "../contact_details/requires/buyer_info_controller",'setFilterGrid("list_view",-1);','0,0,1,0,0,1,1,0,0');
		
}

if ($action=="load_php_data_to_form")
{
	
	$nameArray=sql_select( "select id ,buyer_name, short_name, contact_person, exporters_reference, party_type, designation, tag_company, country_id, web_site, buyer_email, address_1, address_2, address_3, address_4,remark,supllier,credit_limit_days, credit_limit_amount, credit_limit_amount_currency, discount_method, securitye_deducted, vat_to_be_deducted, ait_to_be_deducted,sewing_effi_mkt_percent,sewing_effi_plaing_per,marketing_team_id,status_active from lib_buyer  where id='$data'");
	foreach ($nameArray as $inf)
	{
		echo "document.getElementById('txt_buyer_name').value = '".($inf[csf("buyer_name")])."';\n"; 
		echo "document.getElementById('hidden_buyer_id').value = '".($inf[csf("id")])."';\n"; 

		echo "document.getElementById('txt_short_name').value = '".($inf[csf("short_name")])."';\n";    
		echo "document.getElementById('txt_contact_person').value  = '".($inf[csf("contact_person")])."';\n"; 
		echo "document.getElementById('txt_exporter_ref').value  = '".($inf[csf("exporters_reference")])."';\n"; 
		echo "document.getElementById('cbo_party_type').value  = '".($inf[csf("party_type")])."';\n";  
		echo "document.getElementById('txt_desination').value = '".($inf[csf("designation")])."';\n";    
		echo "document.getElementById('cbo_buyer_company').value  = '".($inf[csf("tag_company")])."';\n";  
		echo "document.getElementById('cbo_country').value = '".($inf[csf("country_id")])."';\n";    
		echo "document.getElementById('txt_web_site').value  = '".($inf[csf("web_site")])."';\n"; 
		echo "document.getElementById('txt_buyer_email').value  = '".($inf[csf("buyer_email")])."';\n";  
		echo "document.getElementById('txt_address_1st').value  = '".($inf[csf("address_1")])."';\n";  
		echo "document.getElementById('txt_address_2nd').value  = '".($inf[csf("address_2")])."';\n";  
		echo "document.getElementById('txt_address_3rd').value  = '".($inf[csf("address_3")])."';\n";  
		echo "document.getElementById('txt_address_4th').value  = '".($inf[csf("address_4")])."';\n";  
		echo "document.getElementById('txt_remark').value  = '".($inf[csf("remark")])."';\n";  
		echo "document.getElementById('cbo_buyer_supplier').value  = '".($inf[csf("supllier")])."';\n";
		echo "document.getElementById('cbo_marketing_team').value  = '".($inf[csf("marketing_team_id")])."';\n";
		echo "document.getElementById('txt_credit_limit_days').value  = '".($inf[csf("credit_limit_days")])."';\n"; 
		echo "document.getElementById('txt_credit_limit_amount').value  = '".($inf[csf("credit_limit_amount")])."';\n"; 
		echo "document.getElementById('cbo_credit_limit_amount_curr').value  = '".($inf[csf("credit_limit_amount_currency")])."';\n"; 
		echo "document.getElementById('cbo_discount_method').value  = '".($inf[csf("discount_method")])."';\n"; 
		echo "document.getElementById('cbo_security_deducted').value  = '".($inf[csf("securitye_deducted")])."';\n"; 
		echo "document.getElementById('cbo_vat_to_be_deducted').value  = '".($inf[csf("vat_to_be_deducted")])."';\n"; 
		echo "document.getElementById('cbo_ait_to_be_deducted').value  = '".($inf[csf("ait_to_be_deducted")])."';\n";
		echo "document.getElementById('txt_sewing_effi_mkt').value  = '".($inf[csf("sewing_effi_mkt_percent")])."';\n";
		echo "document.getElementById('txt_sewing_effi_planing').value  = '".($inf[csf("sewing_effi_plaing_per")])."';\n";
		echo "document.getElementById('cbo_status').value  = '".($inf[csf("status_active")])."';\n"; 
		echo "document.getElementById('update_id').value  = '".($inf[csf("id")])."';\n"; 
		echo "set_button_status(1, '".$_SESSION['page_permission']."', 'fnc_buyer_info',1);\n"; 
		
		echo "set_multiselect('cbo_party_type*cbo_buyer_company','0*0','1','".($inf[csf("party_type")])."*".($inf[csf("tag_company")])."','__set_supplier_status__../contact_details/requires/buyer_info_controller*0');\n";  
 
	}
}

if ($action=="set_supplier_status")
{
	if($data=="") echo ""; 
	else
	{
		$data=explode(",",$data);
		if (in_array("90",$data))
			echo "$('#cbo_buyer_supplier').removeAttr('disabled');\n";
		else	
			echo "$('#cbo_buyer_supplier').attr('disabled','true');\n";
	}
}

?>