<?php
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
 
require_once('../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;
//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("TNA Process","../", 1, 1, $unicode,1,'');

?>
 
<script>
if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../../logout.php";  
		
var permission='<?php echo $permission; ?>';



var tableFilters = 
 {
	col_33: "none",
	col_operation: {
	id: ["total_order_qnty","total_order_qnty_in_pcs","value_tot_cm_cost","value_tot_cost","value_order","value_margin","value_tot_trims_cost","value_tot_embell_cost"],
	col: [9,11,25,26,29,30,31,32],
	operation: ["sum","sum","sum","sum","sum","sum","sum","sum"],
	write_method: ["innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML","innerHTML"]
	}
 }

function fnc_generate_report_main( operation,rpt_type)
{
	var task_name_ref=$('#txt_taks_name').val();
	var job_no=$('#txt_job_no').val();
	var order_no=$('#txt_order_no').val();
	var style_no=$('#txt_style_ref_no').val();
	if(task_name_ref!="")
	{
		if(rpt_type==1)
		{
			alert("Please Click On Task Wise Button");
			return;
		}
	}
	
	var action_type="";
	if(rpt_type==1)
	{
		if(job_no!="" || order_no!="" || style_no!="")
		{
			if ( form_validation('cbo_company_name','Company Name')==false )
			{
				return;
			}
		}
		else
		{
			if ( form_validation('cbo_company_name*txt_date_from*txt_date_to','Company Name*Date From*Date To')==false )
			{
				return;
			}
		}
		action_type="generate_tna_report";
	} 
	else
	{
		if ( form_validation('cbo_company_name*txt_taks_name','Company Name*Task')==false )
		{
			return;
		}
		action_type="generate_task_wise_report";
	}
	
	var data="action="+action_type+"&"+get_submitted_data_string('cbo_company_name*txt_taks_name*tna_task_id*cbo_buyer_name*cbo_team_member*txt_date_from*txt_date_to*txt_job_no*txt_order_no*txt_style_ref_no*cbo_search_type',"../"); 
	freeze_window(operation);
	http.open("POST","requires/tna_report_controller.php",true);
	http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	http.send(data);
	http.onreadystatechange = fnc_generate_report_main_reponse;
}

function fnc_generate_report_main_reponse()
{
	if(http.readyState == 4) 
	{
		var reponse=trim(http.responseText).split('****');
		//document.getElementById('report_container').innerHTML  = reponse[1];
		
		//alert(reponse[0]);
		//alert(reponse[1]);
		$("#report_container").html(reponse[0]);  
		document.getElementById('print_button').innerHTML='<a href="requires/'+reponse[1]+'" style="text-decoration:none"><input type="button" value="Excel Preview" name="excel" id="excel" class="formbutton" style="width:100px"/></a>&nbsp;&nbsp;<input type="button" onclick="new_window()" value="Print Preview" name="Print" class="formbutton" style="width:100px"/>';
		
		setFilterGrid("table_body",-1,tableFilters);
		
		release_freezing();
	}
}

function new_window()
{
	document.getElementById('scroll_body').style.overflow="auto";
	document.getElementById('scroll_body').style.maxHeight="none";
	
	$('#table_body tr:first').hide(); 
	
	var w = window.open("Surprise", "#");
	var d = w.document.open();
	d.write ('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN""http://www.w3.org/TR/html4/strict.dtd">'+
'<html><head><title></title><link rel="stylesheet" href="../css/style_common.css" type="text/css" media="print" /></head><body>'+document.getElementById('report_container').innerHTML+'</body</html>');
	d.close(); 

	document.getElementById('scroll_body').style.overflowY="scroll";
	document.getElementById('scroll_body').style.maxHeight="400px";
	
	$('#table_body tr:first').show();
	
}
function update_tna_process(type,id,po_id)
{ 

	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'requires/tna_report_controller.php?type='+type+'&mid='+id+'&po_id='+po_id+'&action=edit_update_tna'+'&permission='+permission, "TNA Update", 'width=640px,height=240px,center=1,resize=1,scrolling=0','')
	emailwindow.onclose=function()
	{
		var theform=this.contentDoc.forms[0];
		var theemail=this.contentDoc.getElementById("selected_booking");
		if (theemail.value!="")
		{
			freeze_window(5);
			//get_submitted_data_string('',"../"); 
			release_freezing();
		}
	}
}

function progress_comment_popup(job_no,po_id,template_id,tna_process_type)
{
	
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'requires/tna_report_controller.php?job_no='+job_no+'&po_id='+po_id+'&template_id='+template_id+'&tna_process_type='+tna_process_type+'&action=update_tna_progress_comment'+'&permission='+permission, "TNA Progress Comment", 'width=1040px,height=460px,center=1,resize=1,scrolling=0','')
	
	emailwindow.onclose=function()
	{
		var theform=this.contentDoc.forms[0];
		
		var theemail=this.contentDoc.getElementById("selected_booking");
		
		if (theemail.value!="")
		{
			freeze_window(5);
			
			release_freezing();
		}
	}
}


function openmypage_task()
{
	if( form_validation('cbo_company_name','Company Name')==false )
	{
		return;
	}
	var company = $("#cbo_company_name").val();	
	var tna_task = $("#txt_taks_name").val();
	var tna_task_id = $("#tna_task_id").val();
	var tna_task_id_no = $("#tna_task_id_no").val();
	var page_link='requires/tna_report_controller.php?action=task_surch&company='+company+'&tna_task='+tna_task+'&tna_task_id='+tna_task_id+'&tna_task_id_no='+tna_task_id_no;  
	var title="Search Task Popup";
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=420px,height=370px,center=1,resize=0,scrolling=0','')
	emailwindow.onclose=function()
	{
		var theform=this.contentDoc.forms[0]; 
		var style_id=this.contentDoc.getElementById("txt_selected_id").value; // product ID
		var style_des=this.contentDoc.getElementById("txt_selected").value; // product Description
		var style_des_no=this.contentDoc.getElementById("txt_selected_no").value; // product Description
		//alert(style_des_no);
		$("#txt_taks_name").val(style_des);
		$("#tna_task_id").val(style_id); 
		$("#tna_task_id_no").val(style_des_no);
		if(style_des!="")
		{
			$('#from_date_html').html('');
			$('#from_date_html').html('Tna From Date');
			$('#to_date_html').html('');
			$('#to_date_html').html('Tna To Date');
			
		}
		else
		{
			$('#from_date_html').html('');
			$('#from_date_html').html('Ship From Date');
			$('#to_date_html').html('');
			$('#to_date_html').html('Ship To Date');
		}
	}
}

function fn_change_caption(str)
{
	
	var tna_task_id = $("#tna_task_id").val();
	if(tna_task_id){alert('Task ID Selected.');return;}
	
	if(str==1)
	{
	$('#from_date_html').html('');
	$('#from_date_html').html('Ship From Date');
	$('#to_date_html').html('');
	$('#to_date_html').html('Ship To Date');
	}
	else
	{
	$('#from_date_html').html('');
	$('#from_date_html').html('Recv. From Date');
	$('#to_date_html').html('');
	$('#to_date_html').html('Recv. To Date');
	}
	
}

</script>
<body  onLoad="set_hotkey()">
<div align="center"> 
    <?php echo load_freeze_divs ("../");  ?>
	<fieldset style="width:1130px; text-align:left">
    	<table width="1130" class="rpt_table">
        	<tr>
                <td width="120" class="must_entry_caption" align="center"> Company Name</td>
                <td width="200" align="center">Task</td>
                <td width="120" align="center">Buyer Name</td>
                 <td width="120" align="center">Merchant</td>
                <td width="100" align="center">Search type</td>
                <td width="100" align="center" id="from_date_html">Ship From Date</td>
                <td width="100" align="center" id="to_date_html">Ship To Date</td>
                <td width="120" align="center">Job No</td>
                <td width="120" align="center">Order No</td>
                <td width="120" align="center">Style Ref. No</td>
                <!--<td><input type="text" name="txt_job_no" id="txt_job_no" autocomplete="off" class="text_boxes" onDblClick="openmypage('Order Info',1); return false"  style="width:100px" readonly value=""/></td>-->
                
                 
           </tr>
           <tr>
                <td>
                	<?php
					   echo create_drop_down( "cbo_company_name", 130, "select id,company_name from lib_company comp where status_active =1 and is_deleted=0 $company_cond order by company_name","id,company_name", 1, "-- Select Company --", $selected, " load_drop_down( 'requires/tna_report_controller', this.value, 'load_drop_down_buyer', 'buyer_td' ); " );
						?> 
                </td>
                <td align="center">
                        <input style="width:200px;"  name="txt_taks_name" id="txt_taks_name"  ondblclick="openmypage_task()"  class="text_boxes" placeholder="Browse" readonly/>   
                        <input type="hidden" name="tna_task_id" id="tna_task_id"/> <input type="hidden" name="tna_task_id_no" id="tna_task_id_no"/>               
                 </td>
                <td id="buyer_td">
						 <?php 
                            echo create_drop_down( "cbo_buyer_name", 130, $blank_array,"", 1, "-- Select Buyer --", $selected, "" );
                         ?>	
                 </td>
                <td id="team_td">
                                    
					<?php 
						echo create_drop_down( "cbo_team_member", 130, "select id,team_member_name from  lib_mkt_team_member_info where status_active =1 and is_deleted=0 order by team_member_name","id,team_member_name", 1, "-- Select Team Member --", $selected, "" );
                        //echo create_drop_down( "cbo_team_member", 172, $blank_array,"", 1, "- Select Team Member- ", $selected, "" );
                     ?>	
                                    
                </td>
                <td>
					<?php 
						$search_type=array(1=>'Ship Date',2=>'PO Recv. Date');
						echo create_drop_down( "cbo_search_type", 80, $search_type, "",0, "-- Select --", $selected, "fn_change_caption(this.value)" );
                     ?>	
                </td>
                <td><input type="text" name="txt_date_from" id="txt_date_from"  class="datepicker" style="width:78px"  value=""/></td>
                <td><input type="text" name="txt_date_to" id="txt_date_to" class="datepicker"  style="width:78px"  value=""/></td>
                <td><input type="text" name="txt_job_no" id="txt_job_no" autocomplete="off" class="text_boxes" style="width:100px" /></td>
                <td><input type="text" name="txt_order_no" id="txt_order_no" autocomplete="off" class="text_boxes" style="width:100px" ></td>
                <td><input type="text" name="txt_style_ref_no" id="txt_style_ref_no" autocomplete="off" class="text_boxes" style="width:100px" /></td>
                <!--<td><input type="text" name="txt_job_no" id="txt_job_no" autocomplete="off" class="text_boxes" onDblClick="openmypage('Order Info',1); return false"  style="width:100px" readonly value=""/></td>-->
                
                 
           </tr>
           
           <tr>
            	<td colspan="12" align="center" height="40" valign="middle">
                    <?php echo load_month_buttons(1); ?>
                </td>
            </tr>
           <tr>
           		<td colspan="12" height="40" valign="middle" align="center">
                	<input type="button" class="formbutton" style="width:200px" value="Generate Report" onClick="fnc_generate_report_main(3,1)" id="btn_report_generate"/>
                    <input type="button" class="formbutton" style="width:200px" value="Generate Task Wise" onClick="fnc_generate_report_main(3,2)" id="btn_task_report_generate" />
                </td>
           </tr>
           
        </table>
        <div id="print_button"></div>
        <div style="margin-top:5px" id="report_container"></div>
    </fieldset>
	 
</div>
</body>
<script src="../includes/functions_bottom.js" type="text/javascript"></script>
</html>
 
 
 

 
 

