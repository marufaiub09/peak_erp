<?php
/*-------------------------------------------- Comments
Purpose			: 	This form will create chemical & dyes receive return
				
Functionality	:	
JS Functions	:
Created by		:	Jahid 
Creation date 	: 	21-11-2013
Updated by 		: 	Kausar		
Update date		: 	10-12-2013 (Creating report)
QC Performed BY	:	
QC Date			:	
Comments		:
*/

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;

//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Chamical & Dyes Receive Info","../../", 1, 1, $unicode,1,1); 

?>	
<script>
var permission='<?php echo $permission; ?>';
if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../../logout.php";

function open_mrrpopup()
{
	if( form_validation('cbo_company_name','Company Name')==false )
	{
		return;
	}
	var company = $("#cbo_company_name").val();	
	var page_link='requires/chemical_dyes_receive_return_controller.php?action=mrr_popup&company='+company; 
	var title="Search MRR Popup";
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=950px,height=370px,center=1,resize=0,scrolling=0','../')
	emailwindow.onclose=function()
	{
		var theform=this.contentDoc.forms[0]; 
		var mrrNumber=this.contentDoc.getElementById("hidden_recv_number").value; // mrr number
  		// master part call here
		set_button_status(0, permission, 'fnc_general_receive_return_entry',1,1);
		get_php_form_data(mrrNumber, "populate_data_from_data", "requires/chemical_dyes_receive_return_controller");  		
 		$("#tbl_child").find('input,select').val('');
 	}
}
//txt_return_value
function fn_calculateAmount(qnty)
{
	var rate = $("#txt_return_rate").val();
	var rcvQnty = $("#txt_curr_stock").val();
	//var total_qty=$("#txt_cons_quantity").val();
	
	if(qnty=="" || rate=="" || rcvQnty*1 <qnty)
	{
		$('#txt_receive_qty').val(0);
		$('#txt_return_value').val(0);
		return;
	}
	
	else
	{		
		var amount = rate*qnty;
		$('#txt_return_value').val(number_format_common(amount,"","",1));
	}
}
//Save Update Delete
function fnc_general_receive_return_entry(operation)
{
	if(operation==4)
	 {
		var report_title=$( "div.form_caption" ).html();
		print_report( $('#cbo_company_name').val()+'*'+$('#txt_mrr_retrun_no').val()+'*'+report_title, "chemical_dyes_receive_return_print", "requires/chemical_dyes_receive_return_controller" ) 
		 return;
	 }
	else if(operation==0 || operation==1 || operation==2)
	{
		if( form_validation('cbo_company_name*cbo_return_to*txt_receive_date*txt_mrr_no*txt_challan_no*txt_store_name*cbo_item_category*txt_receive_qty*txt_return_value*txt_return_rate','Company Name*Return To*Return Date*Received ID*Challan No*txt_mrr_no*Store Name*Item Category*Retuned Qnty*Return Value*Rate')==false )
		{
			return;
		}	
		if($("#txt_receive_qty").val()*1>$("#txt_cons_quantity").val()*1)
		{
			alert("Return Quantity Can not be Greater Than Receive Stock.");
			return;
		}
		var dataString = "txt_mrr_retrun_no*cbo_company_name*cbo_return_to*txt_receive_date*txt_received_id*txt_mrr_no*txt_challan_no*txt_store_name*cbo_item_category*txt_item_group*txt_item_description*txt_receive_qty*txt_return_value*txt_return_rate*txt_curr_stock*txt_uom*category*store*uom*txt_prod_id*before_prod_id*update_id*transaction_id";
		var data="action=save_update_delete&operation="+operation+get_submitted_data_string(dataString,"../../");//alert(data);
		//freeze_window(operation);
	//alert(data); return;
		http.open("POST","requires/chemical_dyes_receive_return_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = fnc_general_receive_return_entry_reponse;
	}
}

function fnc_general_receive_return_entry_reponse()
{	
	if(http.readyState == 4) 
	{	 		
		var reponse=trim(http.responseText).split('**');
		//alert(reponse);
		//alert(reponse[1]);	
		if(reponse[0]==20)
		{
			alert(reponse[1]);
			return;
		} 
		show_msg(reponse[0]); 		
		if(reponse[0]==0)
		{
			$("#txt_mrr_retrun_no").val(reponse[1]);
 			$("#tbl_master :input").attr("disabled", true);
			disable_enable_fields( 'txt_mrr_retrun_no', 0, "", "" ); // disable false	
		}	
		//alert(reponse[1]);	
		show_list_view(reponse[1],'show_dtls_list_view','list_container_general','requires/chemical_dyes_receive_return_controller','');		
		set_button_status(0, permission, 'fnc_general_receive_return_entry',1,1);
		$("#tbl_child").find('input,select').val('');
		release_freezing();
	}
}

function open_returnpopup()
{
	if( form_validation('cbo_company_name','Company Name')==false )
	{
		return;
	}
	var company = $("#cbo_company_name").val();	
	var page_link='requires/chemical_dyes_receive_return_controller.php?action=return_number_popup&company='+company; 
	var title="Search Return Number Popup";
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=950px,height=370px,center=1,resize=0,scrolling=0','../')
	emailwindow.onclose=function()
	{
		var theform=this.contentDoc.forms[0];
		//alert(theform);return; 
		var returnNumber=this.contentDoc.getElementById("hidden_return_number").value; // mrr number
  		// master part call here
		get_php_form_data(returnNumber, "populate_master_from_data", "requires/chemical_dyes_receive_return_controller");  		
		show_list_view(returnNumber,'show_dtls_list_view','list_container_general','requires/chemical_dyes_receive_return_controller','');
		set_button_status(0, permission, 'fnc_general_receive_return_entry',1,1);
		$("#tbl_child").find('input,select').val('');
		//disable_enable_fields( 'txt_return_no', 0, "", "" ); // disable false
 	}
}

//form reset/refresh function here
function fnResetForm()
{
	$("#tbl_master").find('input,select').attr("disabled", false);	
	set_button_status(0, permission, 'fnc_general_receive_return_entry',1);
	reset_form('genralitem_receive_return_1','list_container_general*list_product_container','','','','');
}

function validate_product()
{
	var data=document.getElementById('cbo_company_id').value+"_"+document.getElementById('cbo_item_category_id').value+"_"+document.getElementById('req_id').value+"_"+document.getElementById('cbo_supplier_id').value+"_"+document.getElementById('hidden_requsition').value;
	var list_view_orders = return_global_ajax_value( data, 'validate_supplier_load_php_dtls_form', '', 'requires/quotation_evaluation_controller');

	if(list_view_orders==1)
	{
		alert("This supplier is exist for same item of this requisition.");
		$("#cbo_supplier_id").focus();
	}
}

</script>
</head>
<body onLoad="set_hotkey()">
<div style="width:100%;" align="center">
<?php echo load_freeze_divs ("../../",$permission);  ?><br />
<form name="genralitem_receive_return_1" id="genralitem_receive_return_1" autocomplete="off" > 
    <div style="width:1000px;">       
    <table width="880" cellpadding="0" cellspacing="2" align="left">
     	<tr>
        	<td width="80%" height="308" align="center" valign="top">   
            	<fieldset style="width:850px; float:left;">
                <legend>Dye/Chem  Receive Return</legend>
                <br />
                 	<fieldset style="width:850px;">
                    <input type="hidden" id="transaction_id" name="transaction_id" />                                       
                        <table  width="800" cellspacing="2" cellpadding="0" border="0" id="tbl_master">
                            <tr>
                           		<td colspan="6" align="center"><b>Return ID</b>
                                	<input type="text" name="txt_mrr_retrun_no" id="txt_mrr_retrun_no" class="text_boxes" style="width:150px" placeholder="Double Click To Search" onDblClick="open_returnpopup()" readonly />
                                    <!--<input type="text" id="hidden_mrr_id" name="hidden_mrr_id" value="" />-->
                                </td>
                           </tr>
                           <tr>
                               <td  width="120" class="must_entry_caption">Company Name </td>
                               <td width="160">
                                    <?php 
                                     echo create_drop_down( "cbo_company_name", 160, "select id,company_name from lib_company comp where status_active=1 and is_deleted=0 $company_cond order by company_name","id,company_name", 1, "-- Select --", $selected, "" );
                                    ?>
                               </td>
                               <td width="120" align="" class="must_entry_caption"> Return Date </td>
                               <td width="150">
                               <input type="text" name="txt_receive_date" id="txt_receive_date" class="datepicker" style="width:150px;" placeholder="Select Date" />
                               </td>
                               <td width="120" align="" class="must_entry_caption">Received ID</td>
                               <td width="150">
                               <input class="text_boxes"  type="text" name="txt_mrr_no" id="txt_mrr_no" style="width:150px;" placeholder="Double Click To Search" onDblClick="open_mrrpopup()" readonly />
                                    <input type="hidden" name="txt_received_id" id="txt_received_id" />
                              </td>
                            </tr>
                            <tr>
                                <td  width="120" align="" class="must_entry_caption"> Challan No</td>
                                <td width="150"><input type="text" name="txt_challan_no" id="txt_challan_no" class="text_boxes" style="width:150px" ></td>
                               <td width="120" align="" >Returned To</td>
                               <td width="150">
                                     <?php                                    
                                    echo create_drop_down( "cbo_return_to", 160, "select id,supplier_name from lib_supplier order by supplier_name","id,supplier_name", 1, "-- Select --", 0, "",1 );
                                ?>
                               </td>
                               <td width="" align=""></td>
                               <td width="">
                               </td>
                            </tr>
                          </table>
                    </fieldset>
                    <br />
                    <input type="hidden" id="txt_cons_quantity" name="txt_cons_quantity" value=""/>
                    <table cellpadding="0" cellspacing="1" width="96%" id="tbl_child">
                    <tr>
                    <td width="49%" valign="top">
                   	  <fieldset style="width:800px;">  
                        <legend>New Receive Return Item</legend>                                     
                        <table width="250" cellspacing="2" cellpadding="0" border="0" style="float:left"> 
                                <tr>    
                                        <td width="130" class="must_entry_caption">Store Name</td>
                                        <td width="" id="store_td"> 
                                         <input type="hidden" id="store" name="store" value="" />
                                        <input type="text" name="txt_store_name" id="txt_store_name" class="text_boxes" style="width:120px;" readonly/>        
                                        </td>
                                </tr>
                                <tr>    
                                        <td class="must_entry_caption">Item Category</td>
                                        <td id="">
                                        <input type="hidden" id="category" name="category" value="" />
										<?php 
                                        	echo create_drop_down( "cbo_item_category", 132, $item_category,"", 1, "-- Select Category --",0, "", 0,"5,6,7" );
                                        ?>
                                        </td>
                                </tr>
                                <tr>    
                                        <td>Item Group.</td>
                                        <td id="item_group_td">
                                         <input type="text" name="txt_item_group" id="txt_item_group" class="text_boxes" style="width:120px;" readonly/>
                                        </td>
                                </tr>
                            </table>
                        <table width="220" cellspacing="2" cellpadding="0" border="0" style="float:left">
                            	<tr>   
                                <td>Item Desc.</td><input type="hidden" name="txt_prod_id" id="txt_prod_id" readonly disabled />
                               <td><input name="txt_item_description" id="txt_item_description" class="text_boxes" type="text" style="width:120px;" placeholder="" readonly /></td>
                                </tr>  
                                <tr>    
                                        <td class="must_entry_caption">Returned Qnty.</td>
                                    	<td><input name="txt_receive_qty" id="txt_receive_qty" class="text_boxes_numeric" type="text" style="width:120px;" placeholder="Entry"   onKeyUp="fn_calculateAmount(this.value)" /></td> <!-- next time have work on it. onKeyUp="fn_calculateAmount(this.value)"-->
                                </tr>
                                <tr>    
                                        <td class="must_entry_caption">Rate</td>   
                                        <td ><input name="txt_return_rate" id="txt_return_rate" class="text_boxes_numeric" type="text" style="width:120px;" readonly/></td> 
                                </tr>
                                   
                        </table>
                            
                        <table width="280" cellspacing="2" cellpadding="0" border="0" style="float:left">
                        		<tr>    
                                        <td class="must_entry_caption">Return Value</td>   
                                        <td ><input name="txt_return_value" id="txt_return_value" class="text_boxes_numeric" type="text" style="width:120px;" readonly/></td> 
                                </tr>
                                <tr>                 
                                    <td width="150">MRR Stock</td>
                                    <td width="130"><input type="text" name="txt_curr_stock" id="txt_curr_stock" class="text_boxes_numeric" style="width:120px;" readonly disabled /></td>
                                </tr>
                                <tr> 
                                    <td>Cons. UOM</td>
                                  
									 <td width="150" id="uom_td"><input type="text" name="txt_uom" id="txt_uom" class="text_boxes" style="width:120px;" readonly disabled /></td>
									 <input type="hidden" id="uom" name="uom" value="" />
                                </tr>
                               
                            </table>                            
                    </fieldset>
                    </td>
                    </tr>
                </table>                
               	<table cellpadding="0" cellspacing="1" width="100%">
                	<tr> 
                       <td colspan="6" align="center"></td>				
                	</tr>
                	<tr>
                        <td align="center" colspan="6" valign="middle" class="button_container">
                             <input type="hidden" id="before_prod_id" name="before_prod_id" value="" />
                             <input type="hidden" id="update_id" name="update_id" value="" />
                              <!-- -->
							 <?php echo load_submit_buttons( $permission, "fnc_general_receive_return_entry", 0,1,"fnResetForm()",1);?>
                        </td>
                   </tr> 
                </table>                 
              	</fieldset>
         </td>
         </tr>
  			</table>
             <div id="list_product_container" style="max-height:auto; width:350px; overflow: hidden; padding-top:0px; margin-top:0px; position:relative;"></div>
		 <div style="clear:both"></div> 
         </br>
    <div  id="list_container_general" style="width:860px;float:left; margin-left:15px;" ></div>
    </div>
	</form>    
    </div>    
</body>  
<script src="../../includes/functions_bottom.js" type="text/javascript"></script> 
</html>