<?php

header('Content-type:text/html; charset=utf-8');
session_start();
include('../../../../includes/common.php');


$user_id=$_SESSION['logic_erp']['user_id'];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

//---------------------------------------------------- Start---------------------------------------------------------------------------
$color_library=return_library_array("select id,color_name from lib_color", "id", "color_name");
$size_library=return_library_array("select id,size_name from lib_size", "id", "size_name");
$company_library=return_library_array("select id,company_name from lib_company", "id", "company_name");
$buyer_arr=return_library_array("select id, short_name from lib_buyer",'id','short_name');
$trim_group= return_library_array("select id, item_name from lib_item_group",'id','item_name');
$costing_per_id_library=return_library_array( "select job_no, costing_per from wo_pre_cost_mst", "job_no", "costing_per");


if ($action=="load_drop_down_buyer")
{
	//$data=explode('_',$data);
	echo create_drop_down( "cbo_buyer_id", 150, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name","id,buyer_name", 1, "-- Select Buyer --", $selected, "",0);  
	exit();
}

if ($action=="style_popup")
{
	echo load_html_head_contents("Popup Info","../../../../", 1, 1, $unicode);
	extract($_REQUEST);
	$data=explode('_',$data);
	//print_r ($data); 
	?>
	 <script>
	var selected_id = new Array, selected_name = new Array(); selected_attach_id = new Array();
	 
	function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		function check_all_data()
		{
			var row_num=$('#list_view tr').length-1;
			for(var i=1;  i<=row_num;  i++)
			{
				$("#tr_"+i).click();
			}
			
		}
		
	function js_set_value(id)
	{
		var str=id.split("_");
		toggle( document.getElementById( 'tr_' + str[0] ), '#FFFFFF' );
		var strdt=str[2];
		str=str[1];
	
		if( jQuery.inArray(  str , selected_id ) == -1 ) {
			selected_id.push( str );
			selected_name.push( strdt );
		}
		else {
			for( var i = 0; i < selected_id.length; i++ ) {
				if( selected_id[i] == str  ) break;
			}
			selected_id.splice( i, 1 );
			selected_name.splice( i,1 );
		}
		var id = '';
		var ddd='';
		for( var i = 0; i < selected_id.length; i++ ) {
			id += selected_id[i] + ',';
			ddd += selected_name[i] + ',';
		}
		id = id.substr( 0, id.length - 1 );
		ddd = ddd.substr( 0, ddd.length - 1 );
		$('#txt_po_id').val( id );
		$('#txt_po_val').val( ddd );
	} 
		  
	</script>
	

 <input type="text" id="txt_po_id" />
 <input type="text" id="txt_po_val" />
     <?php
	if ($data[0]==0) $company_name=""; else $company_name="company_name='$data[0]'";
	if ($data[1]==0) $buyer_name=""; else $buyer_name=" and buyer_name='$data[1]'";
	if($db_type==0) $year_field="YEAR(insert_date) as year"; 
	else if($db_type==2) $year_field="to_char(insert_date,'YYYY') as year";
	else $year_field="";

	
	$sql ="select id,style_ref_no,job_no_prefix_num as job_prefix,$year_field from wo_po_details_master where $company_name $buyer_name"; 
	echo create_list_view("list_view", "Style Ref. No.,Job No,Year","200,100,100","450","310",0, $sql , "js_set_value", "id,style_ref_no", "", 1, "0", $arr, "style_ref_no,job_prefix,year", "","setFilterGrid('list_view',-1)","0","",1) ;	
	exit();	 
}

if ($action=="order_no_popup")
{
	echo load_html_head_contents("Popup Info","../../../../", 1, 1, $unicode);
	extract($_REQUEST);
	$data=explode('_',$data);
	//print ($data[1]);
	?>
	 <script>
	var selected_id = new Array, selected_name = new Array(); selected_attach_id = new Array();
	 
	function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		function check_all_data()
		{
			var row_num=$('#list_view tr').length-1;
			for(var i=1;  i<=row_num;  i++)
			{
				$("#tr_"+i).click();
			}
			
		}
		
	function js_set_value(id)
	{ //alert(id);
		var str=id.split("_");
		toggle( document.getElementById( 'tr_' + str[0] ), '#FFFFFF' );
		var strdt=str[2];
		str=str[1];
	
		if( jQuery.inArray(  str , selected_id ) == -1 ) {
			selected_id.push( str );
			selected_name.push( strdt );
		}
		else {
			for( var i = 0; i < selected_id.length; i++ ) {
				if( selected_id[i] == str  ) break;
			}
			selected_id.splice( i, 1 );
			selected_name.splice( i,1 );
		}
		var id = '';
		var ddd='';
		for( var i = 0; i < selected_id.length; i++ ) {
			id += selected_id[i] + ',';
			ddd += selected_name[i] + ',';
		}
		id = id.substr( 0, id.length - 1 );
		ddd = ddd.substr( 0, ddd.length - 1 );
		$('#txt_po_id').val( id );
		$('#txt_po_val').val( ddd );
	} 
		  
	</script>
      <input type="hidden" id="txt_po_id" />
     <input type="hidden" id="txt_po_val" />
 <?php
	if ($data[0]==0) $company_id=""; else $company_id=" and company_name=$data[0]";
	if ($data[1]==0) $buyer_id=""; else $buyer_id=" and buyer_name=$data[1]";
	if ($data[2]==0) $style=""; else $style=" and b.id in($data[2])";
	if($db_type==0) $year_field="YEAR(b.insert_date) as year"; 
	else if($db_type==2) $year_field="to_char(b.insert_date,'YYYY') as year";
	else $year_field="";

	$sql ="select distinct a.id,a.po_number,b.job_no_prefix_num as job_prefix,$year_field from wo_po_break_down a, wo_po_details_master b where a.job_no_mst=b.job_no $company_name  $buyer_name $style"; 
	echo create_list_view("list_view", "Order Number,Job No, Year","150,100,50","450","310",0, $sql , "js_set_value", "id,po_number", "", 1, "0", $arr, "po_number,job_prefix,year", "","setFilterGrid('list_view',-1)","0","",1) ;	
	exit();
}
// Style Wise Search.
if ($action=="report_generate_style")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	$cbo_company=str_replace("'","",$cbo_company_id);
	$cbo_buyer=str_replace("'","",$cbo_buyer_id);
	//$txt_style_id=str_replace("'","",$txt_style);
	$txt_style=str_replace("'","",$txt_style);
	
	//$txt_style=str_replace("'","",$txt_style);
	$txt_order_no=str_replace("'","",$txt_order_no);
	$txt_order_id=str_replace("'","",$txt_order_no_id);
	$cbo_search_date=str_replace("'","",$cbo_search_date);
	$date_from=str_replace("'","",$txt_date_from);
	$date_to=str_replace("'","",$txt_date_to);
	
	
	if($db_type==0) $group_field="group by po_id,trim_group,description order by  po_id,trim_group"; 
	else if($db_type==2) $group_field="group by  po_id,trim_group,buyer_name,job_no_prefix_num,job_no,style_ref_no,po_id,po_number,po_quantity,brand_sup_ref,req_qnty,cons_uom order by po_id,trim_group";

	if($db_type==0) $group_field2="group by d.id order by b.id"; 
	else if($db_type==2) $group_field2="group by d.id,a.job_no_prefix_num, a.job_no, a.company_name, b.pub_shipment_date, b.po_quantity,d.cons, e.costing_per, a.buyer_name, a.style_ref_no, b.id,b.po_number, c.trim_group, c.brand_sup_ref,b.plan_cut,cc.order_uom order by b.id";
	

	$company_library=return_library_array( "select id,company_name from lib_company", "id", "company_name"  );
	$buyer_arr = return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
	$trim_group= return_library_array( "select id, item_name from lib_item_group",'id','item_name');
	$order_arr = return_library_array( "select id, po_number from wo_po_break_down",'id','po_number');
	//print_r( $order_arr);die;
	ob_start();	
	?>
    <div>
    <fieldset style="width:1350px;">
        <table width="1350">
            <tr class="form_caption">
                <td colspan="16" align="center" style="border:none;font-size:16px; font-weight:bold"> <?php echo $report_title; ?></td>
            </tr>
            <tr class="form_caption">
                <td colspan="16" align="center"><?php echo $company_library[$cbo_company]; ?></td>
            </tr>
        </table>
         <table cellspacing="0" cellpadding="0" border="1" rules="all" width="1368" class="rpt_table" >
            <thead>
                <th width="30">SL </th>
                <th width="80">Buyer Name</th>
                <th width="100">Job No</th>
                <th width="80">Style</th>
                <th width="100">Order No</th>
                <th width="80">Order Qty</th>
                <th width="80">Order Qty(Dzn)</th>
                <th width="150">Item Group</th>
                <th width="40">UOM</th>
                <th width="100">Item Color</th>
                <th width="50">Item Size</th>
                <th width="80">Recv. Qty</th>
                <th width="80">Recv. Value</th>
                <th width="80">Issue Qty.</th>
                <th width="80">Left Over</th>
                <th width="40">Rate</th>
                <th>Left Over Val.</th>
            </thead>
        </table>
        <div style="width:1368px; overflow-y:scroll; max-height:350px;font-size:12px; " id="scroll_body">
        <table cellspacing="0" cellpadding="0" border="1" rules="all" width="1350px" class="rpt_table" id="tbl_issue_status" >
           <tbody>
		   <?php
				// if ($cbo_company==0) $cbo_company1=""; else $cbo_company1="  company_name=$cbo_company";
				if($db_type==0) $po_search="and FIND_IN_SET(a.po_number,'$txt_order_no')"; 
				else if($db_type==2) $po_search=" and a.po_number in ('".$txt_order_no."')";
				else $po_search="";
				if($db_type==0) $po_id_search="and FIND_IN_SET(a.id,$txt_order_id)"; 
				else if($db_type==2) $po_id_search=" and a.id in ('".$txt_order_id."')";
				else $po_id_search="";
				if($db_type==2)
				{
				if(str_replace("'","",$txt_order_no_id)!="") $order_cond=" and a.id in(".str_replace("'","",$txt_order_no_id).")";
				else if(str_replace("'","",$txt_order_no)!="") $order_cond=" and a.po_number in('".str_replace("'","",$txt_order_no)."')"; 
    			else $order_cond="";
				}
				
				else if($db_type==0)
				{
				if(str_replace("'","",$txt_order_no_id)!="") $order_cond=" and a.id in(".str_replace("'","",$txt_order_no_id).")";
				else if(str_replace("'","",$txt_order_no)!="") $order_cond=" and a.po_number in('".str_replace("'","",$txt_order_no)."')"; 
    			else $order_cond="";
				}
				if ($cbo_buyer==0) $buyer_id=""; else $buyer_id=" and b.buyer_name=$cbo_buyer";
				//if ($txt_order_no=="") $order_no=""; else $order_no=$po_search;
				//if($txt_order_id="") $order_cond=""; else  $order_cond="  and FIND_IN_SET(b.id,$txt_order_id)"; 
				//if ($txt_style_id=="") $style=""; else $style="  and  FIND_IN_SET( style_ref_no,'$txt_style_id' )";
				if(str_replace("'","",$txt_style)!="") $style=" and b.id in(".str_replace("'","",$txt_style).")"; else $style="";
				if($db_type==0) $pub_ship_date_from=change_date_format($date_from,'yyyy-mm-dd');
				if($db_type==2) $pub_ship_date_from=change_date_format($date_from,'','',1);
			
				if($db_type==0) $pub_ship_date_to=change_date_format($date_to,'yyyy-mm-dd');
				if($db_type==2) $pub_ship_date_to=change_date_format($date_to,'','',1);
				if( $date_from==0 && $date_to==0 ) $pub_date=""; else $pub_date= "  and a.pub_shipment_date between '".$pub_ship_date_from."' and '".$pub_ship_date_to."'";
				$i=1;
				$receive_qty_array=array();
				$issue_qty_array=array();
				$left_over=0;
				
				$sql_rowspan=sql_select("select
				
				b.job_no,
				p.item_size,
				p.item_color,
				b.style_ref_no,
				p.item_group_id,
				sum(o.quantity)  AS receive_qty
				from
						wo_po_break_down a,wo_po_details_master b, order_wise_pro_details o, product_details_master p
				where 
						a.job_no_mst=b.job_no and o.po_breakdown_id=a.id and o.entry_form=24  and o.prod_id=p.id 
						 and b.company_name=$cbo_company 
				 group by b.style_ref_no,p.item_group_id,p.item_size,
				p.item_color,b.job_no,p.unit_of_measure
				order by b.job_no"); 
				$item_group_arr=array();
				$item_data_arr=array();
				foreach($sql_rowspan as $r_row)
				{
					$item_group_arr[$r_row[csf('style_ref_no')]][$r_row[csf('item_group_id')]]['item_group']=$r_row[csf('item_group_id')];
					$item_data_arr[$r_row[csf('style_ref_no')]][$r_row[csf('item_group_id')]]['item_color']=$r_row[csf('item_color')];
					$item_data_arr[$r_row[csf('style_ref_no')]][$r_row[csf('item_group_id')]]['item_size']=$r_row[csf('item_size')];
					//$item_data_arr[$r_row[csf('style_ref_no')]][$r_row[csf('item_color')]][$r_row[csf('item_size')]]['item_group']=$r_row[csf('item_group_id')];
				} //var_dump($item_data_arr);die;
				
				
				$receive_qty_data=sql_select("select b.po_breakdown_id, a.item_group_id,sum(b.quantity) as quantity , avg(a.rate) as rate,a.order_uom,a.item_color,a.item_size  from  inv_receive_master c,product_details_master d,inv_trims_entry_dtls a , order_wise_pro_details b where a.mst_id=c.id and a.trans_id=b.trans_id and a.prod_id=d.id and b.trans_type=1 and b.entry_form=24 and c.company_id=$cbo_company and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by b.po_breakdown_id, a.item_group_id,a.item_color,a.item_size,a.order_uom");
				foreach($receive_qty_data as $row)
				{
					
					$receive_qty_array[$row[csf('po_breakdown_id')]][$row[csf('item_group_id')]]['rate']=$row[csf('rate')];
					$receive_qty_array[$row[csf('po_breakdown_id')]][$row[csf('item_group_id')]]['item_color']=$row[csf('item_color')];
					//$receive_qty_array[$row[csf('po_breakdown_id')]][$row[csf('item_group_id')]]['item_size']=$row[csf('item_size')];
					$receive_qty_array[$row[csf('po_breakdown_id')]][$row[csf('item_group_id')]]['item_group']=$row[csf('item_group_id')];
					$receive_qty_array[$row[csf('po_breakdown_id')]][$row[csf('item_group_id')]]['order_uom']=$row[csf('order_uom')];
					
				}
				
				$issue_qty_data=sql_select("select b.po_breakdown_id,a.item_color_id,a.item_size, a.item_group_id,sum(b.quantity) as quantity  from  inv_issue_master d,product_details_master p,inv_trims_issue_dtls a , order_wise_pro_details b where a.mst_id=d.id and a.prod_id=p.id and a.trans_id=b.trans_id and b.trans_type=2 and b.entry_form=25 and d.company_id=$cbo_company and a.status_active=1 and b.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by b.po_breakdown_id, a.item_group_id,a.item_color_id,a.item_size");
				foreach($issue_qty_data as $row)
				{
				 $issue_qty_array[$row[csf('po_breakdown_id')]][$row[csf('item_group_id')]][$row[csf('item_color_id')]][$row[csf('item_size')]]['issue_qty']=$row[csf('quantity')];
				}
				
				$job_order_arr=return_library_array("select id,job_no_mst from wo_po_break_down", "id", "job_no_mst");
				
				
				$sql_color_size=sql_select("select b.po_breakdown_id, a.item_group_id,sum(b.quantity) as quantity , avg(a.rate) as rate,a.order_uom,a.item_color,a.item_size  from  inv_receive_master c,product_details_master d,inv_trims_entry_dtls a , order_wise_pro_details b where a.mst_id=c.id and a.trans_id=b.trans_id and a.prod_id=d.id and b.trans_type=1 and b.entry_form=24 and c.company_id=$cbo_company and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 group by b.po_breakdown_id, a.item_group_id,a.item_color,a.item_size,a.order_uom"); 
				$order_color_size_arr=array();
				foreach($sql_color_size as $row)
				{
					$order_color_size_arr[$job_order_arr[$row[csf('po_breakdown_id')]]][$row[csf('item_group_id')]][$row[csf('item_color')]][$row[csf('item_size')]] +=$row[csf('quantity')];
				}
				
				//var_dump($order_color_size_arr);die;
				
				if($db_type==0) $concat_order_id="group_concat(distinct(o.po_breakdown_id)) as po_id"; 
				else if($db_type==2) $concat_order_id="LISTAGG(CAST(o.po_breakdown_id AS VARCHAR2(4000)),',') WITHIN GROUP ( ORDER BY o.po_breakdown_id) as po_id";
	
				$sql=sql_select("select $concat_order_id, b.job_no, sum(distinct a.po_quantity) as po_quantity, b.style_ref_no, b.buyer_name, sum(o.quantity)  AS receive_qty
				from
						wo_po_break_down a,wo_po_details_master b, order_wise_pro_details o
				where 
						a.job_no_mst=b.job_no and o.po_breakdown_id=a.id and o.entry_form=24  and b.company_name=$cbo_company  $buyer_id  $style $order_cond $pub_date 
				group by b.style_ref_no,b.job_no,b.buyer_name
				order by b.job_no");
				
				foreach($sql as $row)
				{
					
					foreach($order_color_size_arr[$row[csf('job_no')]] as $item_group_id=>$color_value)
					{
						$count_item_group[$row[csf('job_no')]]++;
						foreach($color_value as $item_color_id=>$size_value)
						{
							foreach($size_value as $size_id=>$val)
							{
								$count_item_size[$row[csf('job_no')]]++;
							}
						}
					}
				}
				//var_dump($count_item_size);die;
				//echo $sql;die;
				
				$i=1; $k=0;
				$total_left_value=0;
				$total_rec_value=0;$left_val=0;$total_left=0;
				$order_id_array=array();
				foreach ($sql as $selectResult)
				{
					//echo $count_item_size['FAL-14-01252']."jahid";die;
					//echo $count_item_size[$selectResult[csf("job_no")]]."jahid";die;
					if ($i%2==0)  
						$bgcolor="#E9F3FF";
					else
						$bgcolor="#FFFFFF";
						$order_number_arr='';
					$order_number=array_unique(explode(",",$selectResult[csf('po_id')]));
					foreach($order_number as $po_id)
					{	if($po_id>0)
						{
						if($order_number_arr=="") $order_number_arr=$order_arr[$po_id]; else $order_number_arr.=", ".$order_arr[$po_id];
						}
					}
					?> 
					<tr bgcolor="<?php echo $bgcolor; ?>"  onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
						<td width="30"> <?php echo $i;?> </td>
						<td width="80" align="center"><p><?php echo $buyer_arr[$selectResult[csf('buyer_name')]];?></p></td>
						<td width="100" align="center"><p> <?php echo $selectResult[csf('job_no')];?></p></td>
						<td width="80" align="center"><p><?php echo $selectResult[csf('style_ref_no')];?></p></td>
						<td width="100" align="center"><p> <?php echo $order_number_arr;?></p></td>
						
						<td width="80" align="right"> <?php echo number_format($selectResult[csf('po_quantity')]);?> &nbsp;</td>
						<td width="80"  align="right"><?php echo number_format($selectResult[csf('po_quantity')]/12,0); ?> &nbsp;</td>
                                <td width="150" valign="top"  align="left"><p><?php echo $item_group_id; ?></p></td>
                                        <td width="40" align="center"><p> <?php echo $unit_of_measurement[$selectResult[csf('order_uom')]];   ?></p></td>
                                        <td width="100" align="left"><p><?php echo $item_color_id; ?></p></td>
                                                <td width="50" align="left"><p><?php echo $size_id;  ?></p></td>
                                            
                                                <td width="80"  align="right"><?php  echo number_format($val,2); ?>   &nbsp;</td>
                                                <td width="80" title="<?php echo $val; ?>" align="right"> <?php $rec_value= $selectResult[csf('receive_qty')]*$receive_qty_array[$selectResult[csf('po_id')]][$selectResult[csf('trim_group')]]['rate'];  echo number_format($rec_value,2); ?> &nbsp;  </td>
                                                <td width="80" align="right">
                                                <a href='#report_details' onClick="openmypage_color_size_issue('<?php echo $order_number_id; ?>','<?php echo $selectResult[csf('trim_group')]; ?>','<?php echo $item_color; ?>','<?php echo $item_size; ?>','issue_color_size_popup');">
                                                <?php echo number_format($issue_qty,2); ?></a>
                                                </td>
                                                <td width="80" title="Left Over" align="right"><?php $left_over=$selectResult[csf('receive_qty')]-$issue_qty; echo number_format($left_over,2); ?> &nbsp;</td>
                                                <td width="40" title="Rate" align="right" ><?php echo number_format($rec_value/$selectResult[csf('receive_qty')],2); ?> &nbsp;</td>
                                                <td  title="Left Over Value" align="right"> <?php  $total_left=$left_over*$receive_qty_array[$selectResult[csf('po_id')]][$selectResult[csf('trim_group')]]['rate']; echo  number_format($total_left,2); $left_val+=$total_left;  ?> &nbsp; </td>
                                            </tr>
                                            <?php
					$i++;
			}
			?>
             <tr>
               <td colspan="11" align="right"><b> Total </b> </td>
               <td align="right"> <b><?php  echo number_format($total_rec_qty,2); ?> </b> &nbsp;</td>
               <td align="right"><b><?php echo  number_format($tot_receve_val,2);?> </b></td>
               <td align="right"><b><?php echo number_format($total_issue_qty,2) ?></b> </td>
               <td>&nbsp; </td>
               <td align="right"></td>
               <td align="right" > <b><?php echo number_format($left_val,2); ?> </b> &nbsp;</td>
</tr>        
          
        </tbody>
    </table>
    </div>
    
    </fieldset>
    </div>
<?php
 
    $html = ob_get_contents();
    ob_clean();
    //$new_link=create_delete_report_file( $html, 2, $delete, "../../../" );
    foreach (glob("*.xls") as $filename) {
    //if( @filemtime($filename) < (time()-$seconds_old) )
    @unlink($filename);
    }
    //---------end------------//
    $name=time();
    $filename=$user_id."_".$name.".xls";
    $create_new_doc = fopen($filename, 'w');	
    $is_created = fwrite($create_new_doc, $html);
    echo "$html**$filename"; 
    exit();
}

if($action=="receive_item_color_size_popup")
{
	echo load_html_head_contents("Receive Info", "../../../../", 1, 1,'','','');
	extract($_REQUEST);
	?>
<fieldset style="width:470px; margin-left:3px">
		<div id="scroll_body" align="center">
			<table border="1" class="rpt_table" rules="all" width="450" cellpadding="0" cellspacing="0" align="center">
				<thead>
                    <th width="30">Sl</th>
                    <th width="100">Receive ID</th>
                    <th width="75">Receive Date</th>
                    <th width="80">Recv. Qty</th>
				</thead>
                <tbody>
                <?php
					if ($itemcolor!=0) $itemcolor_cond=" and b.item_color=$itemcolor"; else $itemcolor_cond="";
					if ($item_size!=0) $item_size_cond=" and b.item_size=$item_size"; else $item_size_cond="";
					
					$i=1;
					
					$mrr_sql="select a.id, a.recv_number, a.receive_date, c.quantity as quantity
					from inv_receive_master a, inv_trims_entry_dtls b, order_wise_pro_details c 
					where a.id=b.mst_id  and a.entry_form=24 and b.id=c.dtls_id and c.trans_type=1 and   c.po_breakdown_id in($po_id)  and a.company_id='$companyID' and b.item_group_id='$item_group' $itemcolor_cond $item_size_cond and a.is_deleted=0 and a.status_active=1 and b.status_active=1 and b.is_deleted=0 group by    c.po_breakdown_id,b.item_group_id,c.quantity,a.recv_number,a.id,a.receive_date";
					
					//echo $mrr_sql;
					
					$dtlsArray=sql_select($mrr_sql);
					
					foreach($dtlsArray as $row)
					{
						if ($i%2==0)  
							$bgcolor="#E9F3FF";
						else
							$bgcolor="#FFFFFF";	
						?>
						<tr bgcolor="<?php echo  $bgcolor; ?>" onClick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor;?>')" id="tr_<?php echo $i;?>">
							<td width="30"><p><?php echo $i; ?></p></td>
                            <td width="100"><p><?php echo $row[csf('recv_number')]; ?></p></td>
                            <td width="75"><p><?php echo change_date_format($row[csf('receive_date')]); ?></p></td>
                           
                            <td width="80" align="right"><p><?php echo number_format($row[csf('quantity')],2); ?></p></td>
                        </tr>
						<?php
						$tot_qty+=$row[csf('quantity')];
						$i++;
					}
				?>
                </tbody>
                <tfoot>
                	<tr class="tbl_bottom">
                    	<td colspan="3" align="right">Total</td>
                        <td align="right"><?php echo number_format($tot_qty,2); ?>&nbsp;</td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </fieldset>
    <?php
	exit();
}
if($action=="issue_color_size_popup")
{
	echo load_html_head_contents("Issue Info", "../../../../", 1, 1,'','','');
	extract($_REQUEST);
	?>
	<fieldset style="width:470px; margin-left:3px">
		<div id="scroll_body" align="center">
			<table border="1" class="rpt_table" rules="all" width="450" cellpadding="0" cellspacing="0" align="center">
				<thead>
                    <th width="30">Sl</th>
                    <th width="100">Issue ID</th>
                    <th width="75">Issue Date</th>
                    <th width="80">Issue Qty</th>
				</thead>
                <tbody>
                <?php
					if ($itemcolor!=0) $itemcolor_cond=" and b.item_color_id=$itemcolor"; else $itemcolor_cond="";
					if ($item_size!=0) $item_size_cond=" and b.item_size=$item_size"; else $item_size_cond="";
					
					$i=1;
					
					$mrr_sql="select a.id, a.issue_number, a.issue_date,c.quantity as quantity
					from  inv_issue_master a,inv_trims_issue_dtls b, order_wise_pro_details c,product_details_master p 
					where a.id=b.mst_id  and a.entry_form=25 and p.id=b.prod_id and b.id=c.dtls_id and  c.trans_type=2 and a.is_deleted=0 and a.status_active=1 and b.status_active=1 and b.is_deleted=0 and  c.po_breakdown_id in($po_id)  and a.company_id='$companyID' and p.item_group_id='$item_group' $itemcolor_cond  $item_size_cond group by c.po_breakdown_id,p.item_group_id,c.quantity,a.issue_number,a.id,a.issue_date ";
					//echo $mrr_sql;
					$dtlsArray=sql_select($mrr_sql);
					
					foreach($dtlsArray as $row)
					{
						if ($i%2==0)  
							$bgcolor="#E9F3FF";
						else
							$bgcolor="#FFFFFF";	
						?>
						<tr bgcolor="<?php echo  $bgcolor; ?>" onClick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor;?>')" id="tr_<?php echo $i;?>">
							<td width="30"><p><?php echo $i; ?></p></td>
                            <td width="100"><p><?php echo $row[csf('issue_number')]; ?></p></td>
                            <td width="75"><p><?php echo change_date_format($row[csf('issue_date')]); ?></p></td>
                           
                            <td width="80" align="right"><p><?php echo number_format($row[csf('quantity')],2); ?></p></td>
                        </tr>
						<?php
						$tot_qty+=$row[csf('quantity')];
						$i++;
					}
				?>
                </tbody>
                <tfoot>
                	<tr class="tbl_bottom">
                    	<td colspan="3" align="right">Total</td>
                        <td align="right"><?php echo number_format($tot_qty,2); ?>&nbsp;</td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </fieldset>
    <?php
	exit();
}
?>