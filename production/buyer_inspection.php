<?php
/*-------------------------------------------- Comments
Purpose			: 	This form will create Knit Garments Order Entry
				
Functionality	:	
JS Functions	:
Created by		:	Bilas 
Creation date 	: 	20-02-2013
Updated by 		: 		
Update date		: 		   
QC Performed BY	:		
QC Date			:	
Comments		:
*/

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;

//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Cutting Info","../", 1, 1, $unicode,'','');

?>	
<script>
var permission='<?php echo $permission; ?>';
if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../logout.php";
 
function openmypage_order(page_link,title)
{
		page_link=page_link+get_submitted_data_string('cbo_company_name','../');
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=1050px,height=470px,center=1,resize=1,scrolling=0','')
		emailwindow.onclose=function()
		{
			var theform=this.contentDoc.forms[0];;
			var job=this.contentDoc.getElementById("selected_job");
			if (job.value!="")
			{
				freeze_window(5);
				document.getElementById('txt_job_no').value=job.value;
				get_php_form_data( job.value, "populate_order_data_from_search_popup", "requires/buyer_inspection_controller" );
				load_drop_down( 'requires/buyer_inspection_controller', job.value, 'load_drop_down_po_number', 'order_drop_down_td' )
		        show_list_view(job.value,'show_active_listview','inspection_production_list_view','requires/buyer_inspection_controller','');
				release_freezing();
			}
		}
	
}

function open_set_popup(unit_id)
{
			var txt_quotation_id=document.getElementById('txt_job_no').value;
			var set_breck_down=document.getElementById('set_breck_down').value;
			var tot_set_qnty=document.getElementById('tot_set_qnty').value;
			var page_link="requires/buyer_inspection_controller.php?txt_quotation_id="+trim(txt_quotation_id)+"&action=open_set_list_view&set_breck_down="+set_breck_down+"&tot_set_qnty="+tot_set_qnty;
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, "Set Details", 'width=400px,height=250px,center=1,resize=1,scrolling=0','../')
			emailwindow.onclose=function()
			{
				var set_breck_down=this.contentDoc.getElementById("set_breck_down") //Access form field with id="emailfield"
				var item_id=this.contentDoc.getElementById("item_id") //Access form field with id="emailfield"
				var tot_set_qnty=this.contentDoc.getElementById("tot_set_qnty") //Access form field with id="emailfield"
				document.getElementById('set_breck_down').value=set_breck_down.value;
				document.getElementById('item_id').value=item_id.value;
				document.getElementById('tot_set_qnty').value=tot_set_qnty.value;

			}		
}

 
function fnc_buyer_inspection_entry( operation )
{
	if (form_validation('txt_job_no*cbo_inspection_company*txt_inp_date*cbo_order_id*txt_inspection_qnty*cbo_inspection_status','Job No*Insp. Company*Inspection Date*PO number*Inspection Qnty*Inspection Status')==false)
	{
		return;   
	}	
	else
	{
		var insfaction=document.getElementById('cbo_inspection_status').value;
		if(insfaction!=1)
		{
			if (form_validation('cbo_cause','Inspection Cause')==false)
			{
				return;
			}
		}
		
		var data="action=save_update_delete&operation="+operation+get_submitted_data_string('txt_job_no*cbo_inspection_company*txt_inp_date*cbo_order_id*txt_inspection_qnty*cbo_inspection_status*cbo_cause*txt_comments*txt_mst_id',"../");
		freeze_window(operation);
		http.open("POST","requires/buyer_inspection_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = fnc_buyer_inspection_entry_reponse;
	}
}
	 
function fnc_buyer_inspection_entry_reponse()
{
	if(http.readyState == 4) 
	{
		var reponse=http.responseText.split('**');
		show_msg(trim(reponse[0]));
		set_button_status(0, permission, 'fnc_buyer_inspection_entry',1); 
		show_list_view(document.getElementById('txt_job_no').value,'show_active_listview','inspection_production_list_view','requires/buyer_inspection_controller','');
		reset_form('','','cbo_order_id*txt_po_quantity*txt_pub_shipment_date*txt_inspection_qnty*txt_cum_inspection_qnty*cbo_inspection_status*cbo_cause*txt_comments*txt_mst_id','','');
		release_freezing();
	}
}

</script>
</head>
<body onLoad="set_hotkey()">

    <div style="width:100%;" align="center">
        <div style="width:850px;" align="center">
             <?php echo load_freeze_divs ("../",$permission);  ?>
        </div>
         
        <fieldset style="width:950px"> 
        <legend>Production Module</legend>
        <form name="inspectionentry_1" id="inspectionentry_1" action=""  autocomplete="off">
                <fieldset>
                    <table width="100%">
                         <tr>
                         <td width="130" class="must_entry_caption">Job No</td>
                                <td width="170" >
									<input type="text" name="txt_job_no" id="txt_job_no" class="text_boxes" style="width:200px" placeholder="Double Click to Search" onDblClick="openmypage_order('requires/buyer_inspection_controller.php?action=order_search_popup','Order Search')"/>
                                 </td>
                               <td width="130" class="must_entry_caption">Company </td>
                                <td width="473" colspan="3">
                                	<?php
									echo create_drop_down( "cbo_company_name", 550, "select id,company_name from lib_company where is_deleted=0 and status_active=1 order by company_name","id,company_name", 1, "-- Select Company --", $selected,"", 1,0 );	
									?> 
									<input type="hidden" id="sewing_production_variable" />
									<input type="hidden" id="styleOrOrderWisw" />  
                                </td>
                          </tr>
                          <tr>  
                                <td width="130">Buyer Name</td>
                                <td width="170" id="buyer_td">
									<?php
                                        echo create_drop_down( "cbo_buyer_name", 210, "select id,buyer_name from lib_buyer where is_deleted=0 and status_active=1","id,buyer_name", 1, "-- Select Buyer --", $selected, "",1,0 );	
                                    ?> 
                                 </td>
                                <td width="130">Style No</td>
                                <td width="170">
                                    <input name="txt_style_no"   id="txt_style_no" class="text_boxes" style="width:200px " disabled readonly />
                                </td>  
                                 <td width="130">Style Des</td>
                                 <td width="170">
                                 	<input type="text" name="txt_style_des" id="txt_style_des" class="text_boxes" style="width:200px" disabled readonly />
                                 </td>
                          </tr>
                          <tr>  
                                 <td width="130">Order Qnty</td>
                                 <td width="170">
                                 	<input name="txt_order_qty" id="txt_order_qty" class="text_boxes_numeric"  style="width:200px" disabled readonly  />
                                 </td>
                                 
                                 <td width="130">Order UOM </td> 
                                 <td width="170">
                                 <?php 
                                 echo create_drop_down( "cbo_order_uom",60, $unit_of_measurement, "",0, "", 1, "change_caption_cost_dtls( this.value, 'change_caption_pcs' )",1,"1,58" );
                                 ?>
                                 <input type="button" id="set_button" class="image_uploader"  value="Item Details" onClick="open_set_popup(document.getElementById('cbo_order_uom').value)" />
                                 <input type="button" class="image_uploader"  value="Image" onClick="file_uploader ( '../', document.getElementById('txt_job_no').value,'', 'knit_order_entry', 0 ,1)" />
                                 <input type="hidden" id="set_breck_down" />     
                                 <input type="hidden" id="item_id" /> 
                                 <input type="hidden" id="tot_set_qnty" />    
                                 </td>
                                 <td width="130" class="must_entry_caption">Inp. Company</td>
                                 <td width="170" id="cutt_company_td">
									 <?php //echo "select distinct a.id, a.supplier_name from lib_supplier a,lib_supplier_party_type b  where a.id=b.supplier_id and  b.party_type=41 and a.status_active=1 and a.is_deleted=0 order by a.supplier_name";
										echo create_drop_down( "cbo_inspection_company", 210, "select distinct a.id, a.supplier_name from lib_supplier a,lib_supplier_party_type b  where a.id=b.supplier_id and  b.party_type=41 and a.status_active=1 and a.is_deleted=0 order by a.supplier_name","id,supplier_name", 1, "--- Select ---", $selected, "" );     	 
                                     ?> 
                                 </td>
                          </tr>
                          <tr> 
                             <td width="130" class="must_entry_caption">Inp. Date</td>
                             <td width="170">
                                <input type="text" name="txt_inp_date" id="txt_inp_date" class="datepicker" style="width:200px"   />
                             </td> 
                          </tr>
                    </table>
                    </fieldset>
                    <table><tr><td colspan="6" height="5"></td></tr></table><!----------------------this is blank----------------->
                    <table cellpadding="0" cellspacing="1" width="100%" class="rpt_table" rules="all">
                    <thead>
                    <th width="130" class="must_entry_caption">Order No</th>
                    <th width="100" class="must_entry_caption">PO Qty</th>
                    <th width="90">Ship. Date </th>
					<th width="80">Inspec. Qty</th>
                    <th width="105">Cuml. Insp. Qty</th>
                    <th width="105" class="must_entry_caption">Inspec. Status</th>
                    <th width="">Cause </th>
                     
                    </thead>
                     <tr>
                    <td id="order_drop_down_td">
                        <?php  echo create_drop_down( "cbo_order_id",205, $blank_array, 0, "", $selected, "" ); ?>
                         
                    </td>
                    <td >
                    <input name="txt_po_quantity" id="txt_po_quantity"  class="text_boxes_numeric" type="text"  style="width:100px"  disabled />
                    </td>
                    <td >
                    <input name="txt_pub_shipment_date" id="txt_pub_shipment_date" class="datepicker" type="text" value="" style="width:90px;" disabled  />
                    </td>
                     <td >
                    <input name="txt_inspection_qnty" id="txt_inspection_qnty"  class="text_boxes_numeric" type="text"  style="width:80px"  />
                    </td>
                    <td >
                    <input name="txt_cum_inspection_qnty" id="txt_cum_inspection_qnty"  class="text_boxes_numeric" type="text"  style="width:105px"  />
                    </td>
                    <td>
                        <?php  echo create_drop_down( "cbo_inspection_status",105, $inspection_status,"", 1, "-- Select --", $selected, "" ); //change_cause_validation( this.value ) ?>
                         
                    </td>
                    <td>
                        <?php  echo create_drop_down( "cbo_cause",100, $inspection_cause,"", 1, "-- Select --", $selected, "" ); ?>
                         
                    </td>
                    
                    </tr>
                    <tr>
                    <td align="right">
                       <b> Comments</b>
                    </td>
                    <td colspan="7" >
                    <input name="txt_comments" id="txt_comments"  class="text_boxes" type="text"  style="width:622px"/>
                    </td>
                   
                    </tr>
                    
                     <tr>
                            <td align="center" colspan="9" valign="middle" class="button_container">
								<?php
                                echo load_submit_buttons( $permission, "fnc_buyer_inspection_entry", 0,0 ,"reset_form('inspectionentry_1','inspection_production_list_view','','','')",1); 
                                ?>
                                <input type="hidden" name="txt_mst_id" id="txt_mst_id" readonly >	
                            </td>
                            				
                    </tr>
                </table>
                <div style="width:900px; margin-top:5px;"  id="inspection_production_list_view" align="center"></div>
          </form>
        </fieldset>
    </div>
</body> 
<script src="../includes/functions_bottom.js" type="text/javascript"></script>  
</html>