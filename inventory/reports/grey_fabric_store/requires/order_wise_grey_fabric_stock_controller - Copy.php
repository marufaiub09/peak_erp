<?php
header('Content-type:text/html; charset=utf-8');
session_start();
include('../../../../includes/common.php');

$user_id=$_SESSION['logic_erp']['user_id'];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

$company_arr=return_library_array( "select id, company_name from lib_company", "id", "company_name"  );
$supplier_arr=return_library_array( "select id, supplier_name from lib_supplier", "id", "supplier_name"  );
$store_arr=return_library_array( "select id, store_name from lib_store_location", "id", "store_name"  );
$buyer_arr=return_library_array( "select id, short_name from lib_buyer", "id", "short_name"  );
$count_arr=return_library_array( "select id, yarn_count from lib_yarn_count",'id','yarn_count');
$brand_arr=return_library_array( "select id, brand_name from lib_brand",'id','brand_name');
$color_arr=return_library_array( "select id,color_name from lib_color", "id", "color_name"  );
$other_party_arr=return_library_array( "select id,other_party_name from lib_other_party", "id", "other_party_name"  );
$machine_arr=return_library_array( "select id,dia_width from lib_machine_name", "id", "dia_width"  );


if($action=="load_drop_down_buyer")
{
	$data=explode("_",$data);
	if($data[1]==1) $party="1,3,21,90"; else $party="80";
	echo create_drop_down( "cbo_buyer_id", 150, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data[0]' $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in ($party)) order by buy.buyer_name","id,buyer_name", 1, "--Select Buyer--", $selected, "","" );
	exit();
}

if($action=="job_no_popup")
{
	echo load_html_head_contents("Job Info", "../../../../", 1, 1,'','','');
	extract($_REQUEST);
	?>
     
	<script>
		
		/*var selected_id = new Array; var selected_name = new Array;
		
		function check_all_data()
		{
			var tbl_row_count = document.getElementById( 'tbl_list_search' ).rows.length;
			tbl_row_count = tbl_row_count - 1;

			for( var i = 1; i <= tbl_row_count; i++ )
			{
				$('#tr_'+i).trigger('click'); 
			}
		}
		
		function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
			}
		}
		
		function js_set_value( str ) {
			
			if (str!="") str=str.split("_");
			 
			toggle( document.getElementById( 'tr_' + str[0] ), '#FFFFCC' );
			 
			if( jQuery.inArray( str[1], selected_id ) == -1 ) {
				selected_id.push( str[1] );
				selected_name.push( str[2] );
				
			}
			else {
				for( var i = 0; i < selected_id.length; i++ ) {
					if( selected_id[i] == str[1] ) break;
				}
				selected_id.splice( i, 1 );
				selected_name.splice( i, 1 );
			}
			var id = ''; var name = '';
			for( var i = 0; i < selected_id.length; i++ ) {
				id += selected_id[i] + ',';
				name += selected_name[i] + ',';
			}
			
			id = id.substr( 0, id.length - 1 );
			name = name.substr( 0, name.length - 1 );
			
			$('#hide_job_id').val( id );
			$('#hide_job_no').val( name );
		}*/
		function js_set_value(str)
		{
			var splitData = str.split("_");
			//alert (splitData[1]);
			$("#hide_job_id").val(splitData[0]); 
			$("#hide_job_no").val(splitData[1]); 
			parent.emailwindow.hide();
		}
    </script>
</head>
<body>
<div align="center">
	<form name="styleRef_form" id="styleRef_form">
		<fieldset style="width:580px;">
            <table width="570" cellspacing="0" cellpadding="0" border="1" rules="all" align="center" class="rpt_table" id="tbl_list">
            	<thead>
                    <th>Buyer</th>
                    <th>Search By</th>
                    <th id="search_by_td_up" width="170">Please Enter Job No</th>
                    <th><input type="reset" name="button" class="formbutton" value="Reset" style="width:100px;" onClick="reset_form('styleRef_form','search_div','','','','');"></th> 					<input type="hidden" name="hide_job_id" id="hide_job_id" value="" />
                    <input type="hidden" name="hide_job_no" id="hide_job_no" value="" />
                </thead>
                <tbody>
                	<tr>
                        <td align="center">
                        	 <?php 
								echo create_drop_down( "cbo_buyer_name", 140, "select buy.id, buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company=$companyID $buyer_cond and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90)) order by buy.buyer_name","id,buyer_name",1, "-- All Buyer--",$buyer_name,"",0 );
							?>
                        </td>                 
                        <td align="center">	
                    	<?php
                       		$search_by_arr=array(1=>"Job No",2=>"Style Ref");
							$dd="change_search_event(this.value, '0*0', '0*0', '../../') ";							
							echo create_drop_down( "cbo_search_by", 130, $search_by_arr,"",0, "--Select--", "",$dd,0 );
						?>
                        </td>     
                        <td align="center" id="search_by_td">				
                            <input type="text" style="width:130px" class="text_boxes" name="txt_search_common" id="txt_search_common" />	
                        </td> 	
                        <td align="center">
                        	<input type="button" name="button" class="formbutton" value="Show" onClick="show_list_view ('<?php echo $companyID; ?>'+'**'+document.getElementById('cbo_buyer_name').value+'**'+document.getElementById('cbo_search_by').value+'**'+document.getElementById('txt_search_common').value+'**'+'<?php echo $cbo_year_id; ?>'+'**'+'<?php echo $cbo_month_id; ?>', 'create_job_no_search_list_view', 'search_div', 'order_wise_grey_fabric_stock_controller', 'setFilterGrid(\'tbl_list_search\',-1)');" style="width:100px;" />
                    </td>
                    </tr>
            	</tbody>
           	</table>
            <div style="margin-top:15px" id="search_div"></div>
		</fieldset>
	</form>
</div>
</body>           
<script src="../../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
	exit(); 
}

if($action=="create_job_no_search_list_view")
{
	$data=explode('**',$data);
	$company_id=$data[0];
	$year_id=$data[4];
	$month_id=$data[5];
	//echo $month_id;
	
	$buyer_arr=return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
	$company_arr=return_library_array( "select id, company_name from lib_company",'id','company_name');
	
	if($data[1]==0)
	{
		if ($_SESSION['logic_erp']["data_level_secured"]==1)
		{
			if($_SESSION['logic_erp']["buyer_id"]!="") $buyer_id_cond=" and buyer_name in (".$_SESSION['logic_erp']["buyer_id"].")"; else $buyer_id_cond="";
		}
		else
		{
			$buyer_id_cond="";
		}
	}
	else
	{
		$buyer_id_cond=" and buyer_name=$data[1]";
	}
	
	$search_by=$data[2];
	$search_string="%".trim($data[3])."%";

	if($search_by==2) $search_field="style_ref_no"; else $search_field="job_no";
	//$year="year(insert_date)";
	if($year_id!=0) $year_cond=" and year(insert_date)=$year_id"; else $year_cond="";
	if($month_id!=0) $month_cond=" and month(insert_date)=$month_id"; else $month_cond="";
	
	$arr=array (0=>$company_arr,1=>$buyer_arr);
		
	$sql= "select id, job_no, job_no_prefix_num, company_name, buyer_name, style_ref_no, year(insert_date) as year from wo_po_details_master where status_active=1 and is_deleted=0 and company_name=$company_id and $search_field like '$search_string' $buyer_id_cond $year_cond $month_cond order by job_no";
		
	echo create_list_view("tbl_list_search", "Company,Buyer Name,Job No,Year,Style Ref. No", "120,130,80,60","600","240",0, $sql , "js_set_value", "id,job_no_prefix_num", "", 1, "company_name,buyer_name,0,0,0", $arr , "company_name,buyer_name,job_no_prefix_num,year,style_ref_no", "",'','0,0,0,0,0','') ;
   exit(); 
} 

if ($action=="order_no_popup")
{
	echo load_html_head_contents("Popup Info","../../../../", 1, 1, $unicode);
	extract($_REQUEST);
	$data=explode('_',$data);
	//print_r ($data); 
?>	
    <script>
	 
	function js_set_value(str)
	{
		var splitData = str.split("_");
		//alert (splitData[1]);
		$("#order_no_id").val(splitData[0]); 
		$("#order_no_val").val(splitData[1]); 
		parent.emailwindow.hide();
	}
		  
	</script>
     <input type="hidden" id="order_no_id" />
     <input type="hidden" id="order_no_val" />
 <?php
	if ($data[1]==0) $buyer_name=""; else $buyer_name=" and b.buyer_name=$data[1]";
	if ($data[2]=="") $order_no=""; else $order_no=" and a.po_number=$data[2]";
	$job_no=str_replace("'","",$txt_job_id);
	if ($data[2]=="") $job_no_cond=""; else $job_no_cond="  and FIND_IN_SET(b.job_no_prefix_num,'$data[2]')";
	
	$sql="select a.id, a.po_number, b.job_no_prefix_num, b.job_no, b.buyer_name, b.style_ref_no from wo_po_details_master b, wo_po_break_down a  where b.job_no=a.job_no_mst and b.company_name=$data[0] and b.is_deleted=0 $buyer_name $job_no_cond ORDER BY b.job_no";
	//echo $sql;
	$buyer=return_library_array( "select id,buyer_name from lib_buyer", "id", "buyer_name"  );
	$arr=array(1=>$buyer);
	
	echo  create_list_view("list_view", "Job No,Buyer,Style Ref.,Order No", "110,110,150,180","610","350",0, $sql, "js_set_value", "id,po_number", "", 1, "0,buyer_name,0,0,0", $arr , "job_no_prefix_num,buyer_name,style_ref_no,po_number", "order_wise_grey_fabric_stock_controller",'setFilterGrid("list_view",-1);','0,0,0,0,0','') ;
	disconnect($con);
	exit();
}

if($action=="report_generate")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process ));
	
	if(str_replace("'","",$cbo_buyer_id)!="" && str_replace("'","",$cbo_buyer_id)!=0) $buyer_id_cond=" and b.buyer_name=$cbo_buyer_id";
	$job_no=str_replace("'","",$txt_job_no);
	if ($job_no=="") $job_no_cond=""; else $job_no_cond=" and b.job_no_prefix_num in ($job_no) ";//and FIND_IN_SET(c.job_no_prefix_num,'$job_no')
	//if(str_replace("'","",$txt_job_id)!="" && str_replace("'","",$txt_job_id)!=0) $job_id_cond=" and b.id in ($job_no)";
	$year_id=str_replace("'","",$cbo_year);
	//$month_id=str_replace("'","",$cbo_month);

	if($year_id!=0) $year_cond=" and year(b.insert_date)=$year_id"; else $year_cond="";
	//if($month_id!=0) $month_cond=" and month(e.insert_date)=$month_id"; else $month_cond="";
	
	$order_no=str_replace("'","",$txt_order_id);
	if(str_replace("'","",$txt_order_id)!="" && str_replace("'","",$txt_order_id)!=0) $order_id_cond=" and a.id in ($order_no)";
	
	$date_from=str_replace("'","",$txt_date_from);
	//$date_to=str_replace("'","",$txt_date_to);
	if( $date_from=="") $receive_date=""; else $receive_date= " and e.receive_date <=".$txt_date_from."";

	$composition_arr=array();
	$sql_deter="select a.id, a.construction, b.copmposition_id, b.percent from lib_yarn_count_determina_mst a, lib_yarn_count_determina_dtls b where a.id=b.mst_id";
	$data_array=sql_select($sql_deter);
	if(count($data_array)>0)
	{
		foreach( $data_array as $row )
		{
			if(array_key_exists($row[csf('id')],$composition_arr))
			{
				$composition_arr[$row[csf('id')]]=$composition_arr[$row[csf('id')]]." ".$composition[$row[csf('copmposition_id')]]." ".$row[csf('percent')]."%";
			}
			else
			{
				$composition_arr[$row[csf('id')]]=$row[csf('construction')].", ".$composition[$row[csf('copmposition_id')]]." ".$row[csf('percent')]."%";
			}
		}
	}
	
	$transfer_in_arr=array(); 	
	$sql_transfer_in="select a.id, a.to_order_id, b.from_prod_id, b.to_rack, b.to_shelf, sum(b.transfer_qnty) as transfer_in_qnty, sum(b.roll) as transfer_in_roll, b.y_count, b.brand_id, b.yarn_lot from  inv_item_transfer_mst a, inv_item_transfer_dtls b where a.company_id=$cbo_company_id and a.id=b.mst_id and a.transfer_criteria=4 and a.item_category=13 group by a.to_order_id, b.from_prod_id, b.y_count, b.yarn_lot, b.to_rack, b.to_shelf";
	$data_transfer_in_array=sql_select($sql_transfer_in);
	if(count($data_transfer_in_array)>0)
	{
		foreach( $data_transfer_in_array as $row )
		{
			$transfer_in_arr[$row[csf('to_order_id')]][$row[csf('from_prod_id')]][$row[csf('y_count')]][$row[csf('yarn_lot')]][$row[csf('to_rack')]][$row[csf('to_shelf')]]['qty']=$row[csf('transfer_in_qnty')];
			$transfer_in_arr[$row[csf('to_order_id')]][$row[csf('from_prod_id')]][$row[csf('y_count')]][$row[csf('yarn_lot')]][$row[csf('to_rack')]][$row[csf('to_shelf')]]['roll']=$row[csf('transfer_in_roll')];
		}
	}
	//print_r($transfer_in_arr);die;
	$transfer_out_arr=array(); 	
	$sql_transfer_out="select a.id, a.from_order_id, b.from_prod_id, b.rack, b.shelf, sum(b.transfer_qnty) as transfer_out_qnty, sum(b.roll) as transfer_out_roll, b.y_count, b.brand_id, b.yarn_lot from  inv_item_transfer_mst a, inv_item_transfer_dtls b where a.company_id=$cbo_company_id and a.id=b.mst_id and a.transfer_criteria=4 and a.item_category=13 group by a.from_order_id, b.from_prod_id, b.y_count, b.yarn_lot, b.rack, b.shelf";
	$data_transfer_out_array=sql_select($sql_transfer_out);
	if(count($data_transfer_out_array)>0)
	{
		foreach( $data_transfer_out_array as $row )
		{
			$transfer_out_arr[$row[csf('from_order_id')]][$row[csf('from_prod_id')]][$row[csf('y_count')]][$row[csf('yarn_lot')]][$row[csf('rack')]][$row[csf('shelf')]]['qty']=$row[csf('transfer_out_qnty')];
			$transfer_out_arr[$row[csf('from_order_id')]][$row[csf('from_prod_id')]][$row[csf('y_count')]][$row[csf('yarn_lot')]][$row[csf('rack')]][$row[csf('shelf')]]['roll']=$row[csf('transfer_out_roll')];
		}
	}
	//var_dump($transfer_out_arr);
	
	ob_start();
	?>
    <fieldset>
        <table cellpadding="0" cellspacing="0" width="1800">
            <tr  class="form_caption" style="border:none;">
               <td align="center" width="100%" colspan="25" style="font-size:18px"><strong><?php echo $report_title; ?></strong></td>
            </tr>
            <tr  class="form_caption" style="border:none;">
               <td align="center" width="100%" colspan="25" style="font-size:16px"><strong><?php echo $company_arr[str_replace("'","",$cbo_company_id)]; ?></strong></td>
            </tr>
            <tr  class="form_caption" style="border:none;">
               <td align="center" width="100%" colspan="25" style="font-size:14px"><strong> <?php if($date_from!="") echo "Upto : ".change_date_format(str_replace("'","",$txt_date_from)) ;?></strong></td>
            </tr>
        </table>
        <table width="1800" cellpadding="0" cellspacing="0" border="1" rules="all" class="rpt_table" align="left">            	
            <thead>
            	<tr>
                	<th width="30" rowspan="2">SL</th>
                    <th colspan="7">Fabric Details</th>
                    <th colspan="3">Used Yarn Details</th>
                    <th width="100" rowspan="2">Booking/ Prog. No</th>
                    <th colspan="4">Receive Details</th>
                    <th colspan="4">Issue Details</th>
                    <th colspan="5">Stock Details</th>
            	</tr>
                <tr>
                	<th width="">Const. & Comp</th>
                    <th width="60">GSM</th>
                    <th width="60">F/Dia</th>
                    <th width="60">M/Dia</th>
                    <th width="60">Stich Length</th> 
                    <th width="80">Dyeing Color</th>
                    <th width="80">Color Type</th>
                    
                    <th width="60">Y. Count</th>
                    <th width="80">Y. Brand</th>
                    <th width="80">Y. Lot</th>
                    
                    <th width="80">Recv. Qty.</th>
                    <th width="80">Transf. In Qty.</th>
                    <th width="80">Total Recv.</th>
                    <th width="60">Recv. Roll</th>
                    
                    <th width="80">Issue Qty.</th>
                    <th width="80">Transf. Out Qty.</th>
                    <th width="80">Total Issue</th>
                    <th width="60">Issue Roll</th>
                    
                    <th width="80">Stock Qty.</th>
                    <th width="60">Roll Qty.</th>
                    <th width="50">Rack</th>
                    <th width="50">Shelf</th>
                    <th width="50">DOH</th>
                </tr>
            </thead>
        </table>
        <div style="width:1817px; overflow-y: scroll; max-height:380px;" id="scroll_body">
			<table width="1800" cellpadding="0" cellspacing="0" border="1" rules="all" class="rpt_table" align="left"> 
            <?php
			$sql_dtls="select distinct(a.po_number) as po_number, sum(a.po_quantity) as po_quantity, b.job_no, b.buyer_name, b.style_ref_no,
			sum(c.quantity) as quantity, c.po_breakdown_id, c.prod_id,
			d.febric_description_id, d.gsm, d.width, d.stitch_length, d.color_id, d.color_range_id, d.yarn_lot, d.yarn_count, d.brand_id, d.rack, d.self, d.machine_no_id, 
			sum(case when c.trans_type=1  then d.no_of_roll else 0 end) as rec_roll,
			e.booking_no
			from wo_po_break_down a, wo_po_details_master b, order_wise_pro_details c,  pro_grey_prod_entry_dtls d, inv_receive_master e
			where a.job_no_mst=b.job_no and a.status_active=1 and b.status_active=1 and b.is_deleted=0 
			and c.trans_type=1 and c.entry_form in (2,22) and c.trans_id!=0 and c.status_active=1 and c.is_deleted=0
			and c.po_breakdown_id=a.id 
			and c.dtls_id=d.id
			and e.id=d.mst_id and d.status_active=1 and d.is_deleted=0 AND e.company_id=$cbo_company_id
			and e.entry_form in (2,22) and e.item_category=13 and e.status_active=1 and e.is_deleted=0 $buyer_id_cond $job_no_cond $year_cond $order_id_cond $receive_date
			group by a.po_number, c.prod_id,  d.yarn_count, d.yarn_lot, d.rack, d.self
			";//and a.transaction_date  between '".change_date_format($from_date,'yyyy-mm-dd')."' and '".change_date_format($to_date,'yyyy-mm-dd')."'
			$i=1;
			$order_arr=array();
			$nameArray=sql_select( $sql_dtls );
			//echo $sql_dtls;die;
			foreach ($nameArray as $row)
			{
				if ($i%2==0)  
					$bgcolor="#E9F3FF";
				else
					$bgcolor="#FFFFFF";
				$prod_id=$row[csf("prod_id")];
				$order_id=$row[csf("po_breakdown_id")];
				$yarn_count=$row[csf("yarn_count")];
				$yarn_lot=$row[csf("yarn_lot")];
				$rack=$row[csf("rack")];
				$selfd=$row[csf("self")];
				
				if ($row[csf("prod_id")]!="" ) 
				{
					$issue_qty=return_field_value("sum(a.quantity ) as issue_qnty","order_wise_pro_details a, inv_grey_fabric_issue_dtls b ","a.dtls_id=b.id and a.prod_id=".$row[csf("prod_id")]."  and a.po_breakdown_id='".$row[csf("po_breakdown_id")]."' and b.yarn_count='".$row[csf("yarn_count")]."'  and b.yarn_lot='".$row[csf("yarn_lot")]."' and b.rack='".$row[csf("rack")]."' and b.self='$selfd' and  a.trans_type=2 and a.entry_form=16",'issue_qnty');// and  a.trans_type=2 and a.entry_form in (2,22)
					
					$issue_roll=return_field_value("sum(b.no_of_roll) as issue_roll","order_wise_pro_details a, inv_grey_fabric_issue_dtls b","a.dtls_id=b.id and a.prod_id=".$row[csf("prod_id")]."  and a.po_breakdown_id='".$row[csf("po_breakdown_id")]."' and b.yarn_count='".$row[csf("yarn_count")]."'  and b.yarn_lot='".$row[csf("yarn_lot")]."' and b.rack='".$row[csf("rack")]."' and b.self='$selfd' and  a.trans_type=2 and a.entry_form=16",'issue_roll');
				}
				
				$trnsfer_in_qty=$transfer_in_arr[$row[csf('po_breakdown_id')]][$row[csf('prod_id')]][$row[csf('yarn_count')]][$row[csf('yarn_lot')]][$row[csf('rack')]][$row[csf('self')]]['qty'];
				$trnsfer_in_roll=$transfer_in_arr[$row[csf('po_breakdown_id')]][$row[csf('prod_id')]][$row[csf('yarn_count')]][$row[csf('yarn_lot')]][$row[csf('rack')]][$row[csf('self')]]['roll'];
				$trnsfer_out_qty=$transfer_out_arr[$row[csf('po_breakdown_id')]][$row[csf('prod_id')]][$row[csf('yarn_count')]][$row[csf('yarn_lot')]][$row[csf('rack')]][$row[csf('self')]]['qty'];
				$trnsfer_out_roll=$transfer_out_arr[$row[csf('po_breakdown_id')]][$row[csf('prod_id')]][$row[csf('yarn_count')]][$row[csf('yarn_lot')]][$row[csf('rack')]][$row[csf('self')]]['roll'];

				if(!in_array($row[csf('po_number')],$order_arr))
				{
					if($i!=1)
					{
					?>
                        <tr class="tbl_bottom">
                            <td colspan="12" align="right"><b>Order Total</b></td>
                          	<td align="right"><?php echo number_format($tot_rec_qty,2,'.',''); ?>&nbsp;</td>
                            <td align="right"><?php echo number_format($tot_transfer_in_qty,2,'.',''); ?>&nbsp;</td>
                            <td align="right"><?php echo number_format($tot_rec_bal,2,'.',''); ?>&nbsp;</td>
                            <td align="right"><?php echo $tot_rec_roll; ?>&nbsp;</td>
                            <td align="right"><?php echo number_format($tot_issue_qty,2,'.',''); ?>&nbsp;</td>
                            <td align="right"><?php echo number_format($tot_transfer_out_qty,2,'.',''); ?>&nbsp;</td>
                            <td align="right"><?php echo number_format($tot_issue_bal,2,'.',''); ?>&nbsp;</td>
                            <td align="right"><?php echo $tot_issue_roll; ?>&nbsp;</td>
                            <td align="right"><?php echo number_format($tot_stock,2,'.',''); ?>&nbsp;</td>
                            <td align="right"><?php echo $tot_stock_roll_qty; ?>&nbsp;</td>
                            <td align="right">&nbsp;</td>
                            <td align="right">&nbsp;</td>
                            <td align="right">&nbsp;</td>
                        </tr>
                <?php
                        unset($tot_req_qty);
						unset($tot_rec_qty);
                        unset($tot_transfer_in_qty);
                        unset($tot_rec_bal);
						unset($tot_rec_roll);
						unset($tot_issue_qty);
						unset($tot_transfer_out_qty);
						unset($tot_issue_bal);
						unset($tot_issue_roll);
						unset($tot_stock);
						unset($tot_stock_roll_qty);
                    }	
                ?>
                    <tr><td colspan="25" style="font-size:14px" bgcolor="#CCCCAA">&nbsp;<b><?php echo "Order No: ".$row[csf('po_number')]."; Job No: ".$row[csf('job_no')]."; Style Ref: ".$row[csf('style_ref_no')]."; Buyer: ".$buyer_arr[$row[csf('buyer_name')]]."; RMG Qty: ".number_format($row[csf('po_quantity')],2); ?>&nbsp;</b></td></tr>
                <?php	
                    $order_arr[$i]=$row[csf('po_number')];
                }			
				?>
                <tr bgcolor="<?php echo $bgcolor;?>" onClick="change_color('tr<?php echo $i;?>','<?php echo $bgcolor;?>')" id="tr<?php echo $i;?>">
                    <td width="30"><?php echo $i; ?></td>
                    <td width=""><p><?php echo $composition_arr[$row[csf('febric_description_id')]]; ?></p></td>
                    <td width="60"><p><?php echo $row[csf('gsm')]; ?></p></td>
                    <td width="60"><p><?php echo $row[csf('width')]; ?></p></td>
                    <td width="60"><p><?php echo $machine_arr[$row[csf('machine_no_id')]]; ?></p></td>
                    <td width="60"><p><?php echo $row[csf('stitch_length')]; ?></p></td>
                    <td width="80"><p><?php echo $color_arr[$row[csf('color_id')]]; ?></p></td>
                    <td width="80"><p><?php echo $color_range[$row[csf('color_range_id')]]; ?></p></td>
                    <td width="60"><p><?php echo $count_arr[$row[csf('yarn_count')]]; ?></p></td> 
                    <td width="80"><p><?php echo $brand_arr[$row[csf('brand_id')]]; ?></p></td>
                    <td width="80"><p><?php echo $row[csf('yarn_lot')]; ?></p></td>
                    <td width="100"><p><?php echo $row[csf('booking_no')]; ?></p></td>
                    
                    <td width="80" align="right"><p><?php echo number_format($row[csf('quantity')],2); $tot_rec_qty+=$row[csf('quantity')]; ?>&nbsp;</p></td>
                    <td width="80" align="right"><p><?php echo number_format($trnsfer_in_qty,2); $tot_transfer_in_qty+=$trnsfer_in_qty; ?>&nbsp;</p></td>
                    <td width="80" align="right"><p><?php $rec_bal=$row[csf('quantity')]+$trnsfer_in_qty; echo number_format($rec_bal,2); $tot_rec_bal+=$rec_bal; ?>&nbsp;</p></td>
                    <td width="60" align="right"><p><?php $rec_roll=$row[csf('rec_roll')]+$trnsfer_in_roll; echo $rec_roll; $tot_rec_roll+=$rec_roll; ?>&nbsp;</p></td> 
                    
                    <td width="80" align="right"><p><?php echo number_format($issue_qty,2); $tot_issue_qty+=$issue_qty; ?>&nbsp;</p></td>
                    <td width="80" align="right"><p><?php echo number_format($trnsfer_out_qty,2); $tot_transfer_out_qty+=$trnsfer_out_qty; ?>&nbsp;</p></td>
                    <td width="80" align="right"><p><?php $issue_bal=$issue_qty+$trnsfer_out_qty; echo number_format($issue_bal,2); $tot_issue_bal+=$issue_bal; ?></p></td>
                    <td width="60" align="right"><p><?php $iss_roll=$issue_roll-$trnsfer_out_roll; echo $iss_roll; $tot_issue_roll+=$iss_roll; ?>&nbsp;</p></td>
                    
                    <td width="80" align="right"><p><?php $stock=$rec_bal-$issue_bal; echo number_format($stock,2); $tot_stock+=$stock; ?>&nbsp;</p></td>
                    <td width="60" align="right"><p><?php $stock_roll_qty=$rec_roll-$iss_roll; echo $stock_roll_qty; $tot_stock_roll_qty+=$stock_roll_qty; ?>&nbsp;</p></td>
                    <td width="50" align="center"><p><?php echo $row[csf('rack')]; ?></p></td>
                    <td width="50" align="center"><p><?php echo $row[csf('self')]; ?></p></td>
					<?php
						if($row[csf("prod_id")]!=0)
						{
                        	$returnRes=explode(",",return_field_value("concat(min(transaction_date),',',max(transaction_date))","inv_transaction","prod_id=".$row[csf("prod_id")]));
						}
						//echo $returnRes;die;
                        $daysOnHand = datediff("d",$returnRes[1],date("Y-m-d"));
                    ?>
                    <td width="50" align="center"><p><?php echo $daysOnHand; ?></p></td>
                </tr>
			<?php
				$i++;
				$grand_tot_rec_qty+=$row[csf('quantity')];
				$grand_tot_transfer_in_qty+=$trnsfer_in_qty;
				$grand_tot_rec_bal+=$rec_bal;
				$grand_tot_rec_roll+=$rec_roll;
				$grand_tot_issue_qty+=$issue_qty;
				$grand_tot_transfer_out_qty+=$trnsfer_out_qty;
				$grand_tot_issue_bal+=$issue_bal;
				$grand_tot_issue_roll+=$iss_roll;
				$grand_tot_stock+=$stock;
				$grand_tot_roll_qty+=$stock_roll_qty;
			}
			?>
            <tr class="tbl_bottom">
                <td colspan="12" align="right"><b>Order Total</b></td>
                <td align="right"><?php echo number_format($tot_rec_qty,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($tot_transfer_in_qty,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($tot_rec_bal,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo $tot_rec_roll; ?>&nbsp;</td>
                <td align="right"><?php echo number_format($tot_issue_qty,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($tot_transfer_out_qty,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo number_format($tot_issue_bal,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo $tot_issue_roll; ?>&nbsp;</td>
                <td align="right"><?php echo number_format($tot_stock,2,'.',''); ?>&nbsp;</td>
                <td align="right"><?php echo $tot_stock_roll_qty; ?>&nbsp;</td>
                <td align="right">&nbsp;</td>
                <td align="right">&nbsp;</td>
                <td align="right">&nbsp;</td>
            </tr>
        <tfoot>
            <tr >
                <th colspan="12" align="right"><b>Grand Total</b></th>
                <th align="right"><?php echo number_format($grand_tot_rec_qty,2,'.',''); ?>&nbsp;</th>
                <th align="right"><?php echo number_format($grand_tot_transfer_in_qty,2,'.',''); ?>&nbsp;</th>
                <th align="right"><?php echo number_format($grand_tot_rec_bal,2,'.',''); ?>&nbsp;</th>
                <th align="right"><?php echo $grand_tot_rec_roll; ?>&nbsp;</th>
                <th align="right"><?php echo number_format($grand_tot_issue_qty,2,'.',''); ?>&nbsp;</th>
                <th align="right"><?php echo number_format($grand_tot_transfer_out_qty,2,'.',''); ?>&nbsp;</th>
                <th align="right"><?php echo number_format($grand_tot_issue_bal,2,'.',''); ?>&nbsp;</th>
                <th align="right"><?php echo $grand_tot_issue_roll; ?>&nbsp;</th>
                <th align="right"><?php echo number_format($grand_tot_stock,2,'.',''); ?>&nbsp;</th>
                <th align="right"><?php echo $grand_tot_roll_qty; ?>&nbsp;</th>
                <th align="right">&nbsp;</th>
                <th align="right">&nbsp;</th>
                <th align="right">&nbsp;</th>
            </tr>
        </tfoot>
    </table>
    </div>
    </fieldset>
	<?php
    $html = ob_get_contents();
    ob_clean();
    //$new_link=create_delete_report_file( $html, 2, $delete, "../../../" );
    foreach (glob("*.xls") as $filename) {
    //if( @filemtime($filename) < (time()-$seconds_old) )
    @unlink($filename);
    }
    //---------end------------//
    $name=time();
    $filename=$user_id."_".$name.".xls";
    $create_new_doc = fopen($filename, 'w');	
    $is_created = fwrite($create_new_doc, $html);
    echo "$html####$filename"; 
    exit();
}
