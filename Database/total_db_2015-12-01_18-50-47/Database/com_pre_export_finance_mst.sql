-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `com_pre_export_finance_mst`
--

DROP TABLE IF EXISTS `com_pre_export_finance_mst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `com_pre_export_finance_mst` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `system_number_prefix` varchar(20) NOT NULL,
  `system_number_prefix_num` int(11) NOT NULL,
  `system_number` varchar(20) NOT NULL,
  `beneficiary_id` int(11) NOT NULL DEFAULT '0',
  `loan_date` date NOT NULL,
  `inserted_by` int(11) NOT NULL DEFAULT '0',
  `insert_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `update_date` datetime NOT NULL,
  `status_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `is_posted_account` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `com_pre_export_finance_mst`
--

LOCK TABLES `com_pre_export_finance_mst` WRITE;
/*!40000 ALTER TABLE `com_pre_export_finance_mst` DISABLE KEYS */;
INSERT INTO `com_pre_export_finance_mst` VALUES (1,'PAL-PFE-15-',1,'PAL-PFE-15-00001',1,'2015-10-25',1,'2015-10-25 14:30:16',0,'0000-00-00 00:00:00',1,0,0),(2,'PAL-PFE-15-',2,'PAL-PFE-15-00002',1,'0000-00-00',13,'2015-11-15 10:33:04',0,'0000-00-00 00:00:00',1,0,0),(3,'PAL-PFE-15-',3,'PAL-PFE-15-00003',1,'0000-00-00',13,'2015-11-15 10:33:07',0,'0000-00-00 00:00:00',1,0,0),(4,'PAL-PFE-15-',4,'PAL-PFE-15-00004',1,'0000-00-00',13,'2015-11-15 10:33:09',0,'0000-00-00 00:00:00',1,0,0);
/*!40000 ALTER TABLE `com_pre_export_finance_mst` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:40
