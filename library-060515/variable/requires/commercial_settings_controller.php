<?
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

include('../../../includes/common.php');

$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];


if ($action=="on_change_data")
{
	
	$explode_data = explode("_",$data);
	$type = $explode_data[0];
	$company_id = $explode_data[1];
	
	if($type=="5") // Commercial_Garments Export Capacity
	{
	
			$nameArray=sql_select( "select id,capacity_in_value,currency_id from  variable_settings_commercial where company_name='$company_id' and variable_list=5 order by id" );
			if(count($nameArray)>0)$is_update=1;else $is_update=0;
			?>
			<fieldset>
				<legend>Garments Export Capacity</legend>
				<div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont1" align="center">
					<table cellspacing="0" width="100%" >
                        <tr> 
                            <td width="130" align="left" id="capacity_in_value">Capacity In Value</td>
                            <td width="190" align="left">
                            	<input type="text" name="txt_capacity_value" id="txt_capacity_value" value="<? echo $nameArray[0][csf('capacity_in_value')]; ?>" class="text_boxes_numeric" style="width:158px;"/>
                            </td>
                        </tr>
                        <tr>
                            <td width="130" align="left" id="currency">Currency</td>
                            <td width="190">
                               		<? 
										echo create_drop_down( "cbo_currency_id", 170, $currency,'', 1, '---- Select -----', $nameArray[0][csf('currency_id')], "" );
									?>                               
                            </td>
                        </tr>
                    </table>                    
				</div>
				<div style="width:500px; min-height:20px; max-height:250px;" id="variable_list_cont2" align="center">
					<table cellspacing="0" width="100%" >
						<tr> 
							<td align="center" width="320">&nbsp;</td>						
 						</tr>						 
                        <tr>
                           <td colspan="3" height="40" valign="bottom" align="center" class="button_container">
                                <input  type="hidden"name="update_id" id="update_id" value="<? echo $nameArray[0][csf('id')]; ?>">
								<? 
									echo load_submit_buttons( $permission, "fnc_variable_settings_commercial", $is_update,0 ,"reset_form('commercialvariablesettings_1','','')",1);
                                ?>
                            </td>					
                        </tr>
					</table>
				</div>
			</fieldset>
<?    				
			
	}
    
    
	
	if( $type==6 ) //Commercial_Max BTB Limit
	{
			 
			$nameArray=sql_select( "select id,max_btb_limit,currency_id from  variable_settings_commercial where company_name='$company_id' and variable_list=6 order by id" );
			if(count($nameArray)>0)$is_update=1;else $is_update=0;
			?>
			<fieldset>
				<legend>Max BTB Limit</legend>
				<div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont1" align="center">
					<table cellspacing="0" width="100%" >
                        <tr> 
                            <td width="130" align="left" id="max_btb_limit">Max BTB Limit</td>
                            <td width="190" align="left">
                            	<input type="text" name="txt_max_btb_limit" id="txt_max_btb_limit" value="<? echo $nameArray[0][csf('max_btb_limit')]; ?>" class="text_boxes_numeric" style="width:158px;"/>%
                            </td>
                        </tr>                        
                    </table>                    
				</div>
				<div style="width:500px; min-height:20px; max-height:250px;" id="variable_list_cont2" align="center">
					<table cellspacing="0" width="100%" >
						<tr> 
							<td align="center" width="320">&nbsp;</td>						
 						</tr>						 
                        <tr>
                           <td colspan="3" height="40" valign="bottom" align="center" class="button_container">
                                <input  type="hidden"name="update_id" id="update_id" value="<? echo $nameArray[0][csf('id')]; ?>">
								<? 
									echo load_submit_buttons( $permission, "fnc_variable_settings_commercial", $is_update,0 ,"reset_form('commercialvariablesettings_1','','')",1);
                                ?>
                            </td>					
                        </tr>
					</table>
				</div>
			</fieldset>
<?    				
			
	}
	


	if( $type==7 ) //Commercial_Max PC Limit
	{
	
			$nameArray=sql_select( "select id,max_pc_limit,currency_id from  variable_settings_commercial where company_name='$company_id' and variable_list=7 order by id" );
			if(count($nameArray)>0)$is_update=1;else $is_update=0;
			?>
			<fieldset>
				<legend>Max PC Limit</legend>
				<div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont1" align="center">
					<table cellspacing="0" width="100%" >
                        <tr> 
                            <td width="130" align="left" id="max_pc_limit">Max PC Limit</td>
                            <td width="190" align="left">
                            	<input type="text" name="txt_max_pc_limit" id="txt_max_pc_limit" value="<? echo $nameArray[0][csf('max_pc_limit')]; ?>" class="text_boxes_numeric" style="width:158px;"/>%
                            </td>
                        </tr>                        
                    </table>                    
				</div>
				<div style="width:500px; min-height:20px; max-height:250px;" id="variable_list_cont2" align="center">
					<table cellspacing="0" width="100%" >
						<tr> 
							<td align="center" width="320">&nbsp;</td>						
 						</tr>						 
                        <tr>
                           <td colspan="3" height="40" valign="bottom" align="center" class="button_container">
                                <input  type="hidden"name="update_id" id="update_id" value="<? echo $nameArray[0][csf('id')]; ?>">
								<? 
									echo load_submit_buttons( $permission, "fnc_variable_settings_commercial", $is_update,0 ,"reset_form('commercialvariablesettings_1','','')",1);
                                ?>
                            </td>					
                        </tr>
					</table>
				</div>
			</fieldset>
<?    				
			
	}
	
	
	if( $type==17 )//Commercial Possible heads for BTB
	{
			
			/*$nameArray=sql_select( "select id,company_name,cost_heads,cost_heads_status from  variable_settings_commercial where company_name='$company_id' and variable_list=17 order by id" );
			if(count($nameArray)>0)$is_update=1;else $is_update=0;*/
			$sql="select id,company_name,cost_heads,cost_heads_status from variable_settings_commercial where company_name='$company_id' and variable_list=17 order by id" ;
			?>
                <div>
                   <fieldset>
                        <legend>Possible Heads For BTB</legend>
                        <div style="width:400px; min-height:20px; max-height:250px;" id="variable_list_cont1" align="center">
                            <table cellspacing="0" width="100%" class="rpt_table">
                                <thead> 
                                    <th width="200" align="left">Cost Heads</th>
                                    <th width="190" align="left">Status</th>
                                </thead>                                
                                    <tr align="center">
                                        <td>
                                                <? 
                                                    echo create_drop_down( "cbo_cost_heads", 155, $cost_heads_for_btb,'', 1, '---- Select ----', "0", "" );
                                                ?>
                                        </td>
                                        <td>
                                                <? 
                                                    echo create_drop_down( "cbo_cost_heads_status", 130, $yes_no,'', 1, '---- Select ----',"0", "" );
                                                ?>												
                                        </td>
                                     </tr>   
                               
                            </table>
                        </div>
                        <div style="width:500px; min-height:20px; max-height:250px;" id="variable_list_cont2" align="center">
                            <table cellspacing="0" width="100%" >
                                <tr> 
                                    <td align="center" width="320">&nbsp;</td>
                                </tr>
                                <tr>
                                   <td colspan="3" height="40" valign="bottom" align="center" class="button_container">
                                        <input  type="hidden"name="update_id" id="update_id" value="">
										<? 
                                            $permission=$_SESSION['page_permission'];
                                            echo load_submit_buttons( $permission, "fnc_variable_settings_commercial", 0,0 ,"reset_form('ordertrackingvariablesettings_1','','',1)");
                                        ?>
                                    </td>					
                                </tr>
                            </table>                            
                                <?
                                	//create_list_view( $table_id, $tbl_header_arr, $td_width_arr, $tbl_width, $tbl_height, $tbl_border, $query, $onclick_fnc_name, $onclick_fnc_param_db_arr, $onclick_fnc_param_sttc_arr,  $show_sl, $field_printed_from_array_arr,  $data_array_name_arr, $qry_field_list_array, $controller_file_path , $filter_grid_fnc, $fld_type_arr )
							$com_name=return_library_array( "select company_name,id from lib_company", "id", "company_name"  );
							$arr=array (0=>$com_name,1=>$cost_heads_for_btb,2=>$yes_no);
							echo  create_list_view ( "list_view", "Company Name,Cost Heads,Status", "150,150,150","470","220",0, $sql, "get_php_form_data", "id", "'load_php_data_to_form'", 1, "company_name,cost_heads,cost_heads_status", $arr , "company_name,cost_heads,cost_heads_status", "requires/commercial_settings_controller", 'setFilterGrid("list_view",-1);' ) ;
									
							?>   
                        </div>
                       
                    </fieldset>
                </div>
<?    				
			
	}

	exit();
	
}


if ($action=="list_view")
{
		echo  create_list_view ( "list_view", "Company Name,Cost Heads,Status", "","470","220",0, "select id,company_name,cost_heads,cost_heads_status from  variable_settings_commercial where company_name='$company_id' and variable_list=17 order by id", "get_php_form_data", "id", "'load_php_data_to_form'", 1, "0,sample_type,status_active", '' , "sample_name,sample_type,status_active", "requires/commercial_settings_controller", 'setFilterGrid("list_view",-1);' ) ;
}

if($action=="load_php_data_to_form")
{
	$sql="select cost_heads,cost_heads_status from variable_settings_commercial where id=$data";
	$data_array=sql_select($sql);
	foreach ($data_array as $row)
	{
		echo "document.getElementById('cbo_cost_heads').value = '".$row[csf("cost_heads")]."';\n";  
		echo "document.getElementById('cbo_cost_heads_status').value = '".$row[csf("cost_heads_status")]."';\n";  
		echo "document.getElementById('update_id').value = '".$data."';\n";  
		
		echo "set_button_status(1, '".$_SESSION['page_permission']."', 'fnc_variable_settings_commercial',1);\n";	 
	}
	exit();
}

if ($action=="save_update_delete")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	if ($operation==0)  // Insert Here
	{
		 
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		$field_array="";
		$data_array="";
		$id=return_next_id( "id", "variable_settings_commercial", 1 ) ;
		if($cbo_variable_list=="'5'")
		{
			$field_array="id,company_name,variable_list,capacity_in_value_hcode,capacity_in_value,currency_hcode,currency_id,inserted_by,insert_date,status_active";
			$data_array="(".$id.",".$cbo_company_name.",".$cbo_variable_list.",'".$capacity_in_value."',".$txt_capacity_value.",'".$currency."',".$cbo_currency_id.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."',1)";
		}
		if($cbo_variable_list=="'6'")
		{
			$field_array="id,company_name,variable_list,max_btb_limit_hcode,max_btb_limit,inserted_by,insert_date,status_active";
			$data_array="(".$id.",".$cbo_company_name.",".$cbo_variable_list.",'".$max_btb_limit."',".$txt_max_btb_limit.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."',1)";
		}
		if($cbo_variable_list=="'7'")
		{
			$field_array="id,company_name,variable_list,max_pc_limit_hcode,max_pc_limit,inserted_by,insert_date,status_active";
			$data_array="(".$id.",".$cbo_company_name.",".$cbo_variable_list.",'".$max_pc_limit."',".$txt_max_pc_limit.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."',1)";
		}
		if($cbo_variable_list=="'17'")
		{
			if (is_duplicate_field( "company_name", "variable_settings_commercial", "company_name=$cbo_company_name and variable_list=$cbo_variable_list and cost_heads=$cbo_cost_heads" ) == 1)
			{
				echo 11; die;
			}
			
			$field_array="id,company_name,variable_list,cost_heads,cost_heads_status,inserted_by,insert_date,status_active";
			$data_array="(".$id.",".$cbo_company_name.",".$cbo_variable_list.",".$cbo_cost_heads.",".$cbo_cost_heads_status.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."',1)";
		}
		
		//print_r($data_array);die;
		$rID=sql_insert("variable_settings_commercial",$field_array,$data_array,1);
		if($db_type==0)
		{
			if($rID ){
				mysql_query("COMMIT");  
				echo 0;
			}
			else{
				mysql_query("ROLLBACK"); 
				echo 10;
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
				 if($rID )
			    {
					oci_commit($con);   
					echo "0**".$rID;
				}
				else{
					oci_rollback($con);
					echo "10**".$rID;
				}
		}
		disconnect($con);
		die;
	}
	
	else if ($operation==1)   // Update Here
	{
		
			$con = connect();
			if($db_type==0)
			{
				mysql_query("BEGIN");
			}
			
			if($cbo_variable_list=="'5'")
			{
				$field_array="company_name*variable_list*capacity_in_value*currency_id*updated_by*update_date";
				$data_array=$cbo_company_name."*".$cbo_variable_list."*".$txt_capacity_value."*".$cbo_currency_id."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
				$rID=sql_update("variable_settings_commercial",$field_array,$data_array,"id","".$update_id."",1);
			}
			else if($cbo_variable_list=="'6'")
			{
				$field_array="company_name*variable_list*max_btb_limit_hcode*max_btb_limit*updated_by*update_date";
				$data_array=$cbo_company_name."*".$cbo_variable_list."*'".$max_btb_limit."'*".$txt_max_btb_limit."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
			$rID=sql_update("variable_settings_commercial",$field_array,$data_array,"id","".$update_id."",1);
				//echo "update set variable_settings_commercial (".$field_array.") values ".$data_array;die;
			}
			else if($cbo_variable_list=="'7'")
			{
				$field_array="company_name*variable_list*max_pc_limit_hcode*max_pc_limit*updated_by*update_date";
				$data_array=$cbo_company_name."*".$cbo_variable_list."*'".$max_pc_limit."'*".$txt_max_pc_limit."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
				$rID=sql_update("variable_settings_commercial",$field_array,$data_array,"id","".$update_id."",1);
			}
			else if($cbo_variable_list=="'17'")
			{
				if (is_duplicate_field( "company_name", "variable_settings_commercial", "company_name=$cbo_company_name and variable_list=$cbo_variable_list and cost_heads=$cbo_cost_heads and id<>$update_id" ) == 1)
				{
					echo 11; die;
				}
				$field_array="company_name*variable_list*cost_heads*cost_heads_status*updated_by*update_date";
				$data_array=$cbo_company_name."*".$cbo_variable_list."*".$cbo_cost_heads."*".$cbo_cost_heads_status."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
				$rID=sql_update("variable_settings_commercial",$field_array,$data_array,"id","".$update_id."",1);
			}
			
			if($db_type==0)
			{
				if($rID ){
					mysql_query("COMMIT");  
					echo 1;
				}
				else{
					mysql_query("ROLLBACK"); 
					echo 10;
				}
			}
			if($db_type==2 || $db_type==1 )
			{
			 if($rID )
			    {
					oci_commit($con);   
					echo "1**".$rID;
				}
				else{
					oci_rollback($con);
					echo "10**".$rID;
				}
			}
		   disconnect($con);
		   die;
		
	}	
	
}



?>