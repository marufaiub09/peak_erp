-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `pro_gar_prod_gross_mst`
--

DROP TABLE IF EXISTS `pro_gar_prod_gross_mst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pro_gar_prod_gross_mst` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `garments_nature` tinyint(1) NOT NULL DEFAULT '0',
  `company_id` int(11) NOT NULL DEFAULT '0',
  `po_break_down_id` int(11) NOT NULL DEFAULT '0',
  `item_number_id` int(11) NOT NULL DEFAULT '0',
  `country_id` int(11) NOT NULL DEFAULT '0',
  `production_source` int(11) NOT NULL DEFAULT '0',
  `serving_company` int(11) NOT NULL DEFAULT '0',
  `location` int(11) NOT NULL DEFAULT '0',
  `embel_name` int(11) NOT NULL DEFAULT '0',
  `embel_type` int(11) NOT NULL DEFAULT '0',
  `production_date` date NOT NULL,
  `production_quantity` int(20) DEFAULT NULL,
  `production_type` varchar(100) NOT NULL,
  `production_hour` time NOT NULL,
  `sewing_line` int(11) NOT NULL,
  `supervisor` varchar(100) NOT NULL,
  `remarks` varchar(450) NOT NULL,
  `floor_id` int(11) NOT NULL DEFAULT '0',
  `alter_qnty` int(11) NOT NULL,
  `reject_qnty` int(11) NOT NULL,
  `total_produced` int(20) DEFAULT NULL,
  `yet_to_produced` int(20) DEFAULT NULL,
  `receive_status` int(11) NOT NULL DEFAULT '0',
  `prod_reso_allo` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1=Yes; 0=No;',
  `spot_qnty` int(11) NOT NULL DEFAULT '0',
  `inserted_by` int(11) NOT NULL DEFAULT '0' COMMENT 'Employee Code',
  `insert_date` datetime DEFAULT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0' COMMENT 'Employee Code',
  `update_date` datetime DEFAULT NULL,
  `status_active` tinyint(1) DEFAULT '1' COMMENT '0=No; 1=Yes',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '0=No; 1=Yes',
  `is_locked` tinyint(1) DEFAULT '0' COMMENT '0=No; 1=Yes',
  PRIMARY KEY (`id`),
  KEY `garments_nature` (`garments_nature`),
  KEY `po_break_down_id` (`po_break_down_id`),
  KEY `item_number_id` (`item_number_id`),
  KEY `production_type` (`production_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pro_gar_prod_gross_mst`
--

LOCK TABLES `pro_gar_prod_gross_mst` WRITE;
/*!40000 ALTER TABLE `pro_gar_prod_gross_mst` DISABLE KEYS */;
/*!40000 ALTER TABLE `pro_gar_prod_gross_mst` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:43
