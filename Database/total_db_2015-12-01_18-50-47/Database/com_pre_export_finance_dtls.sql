-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `com_pre_export_finance_dtls`
--

DROP TABLE IF EXISTS `com_pre_export_finance_dtls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `com_pre_export_finance_dtls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mst_id` int(111) NOT NULL DEFAULT '0',
  `loan_type` int(11) NOT NULL DEFAULT '0',
  `loan_number` varchar(30) NOT NULL,
  `bank_account_id` int(11) NOT NULL DEFAULT '0',
  `loan_amount` double NOT NULL,
  `currency_id` tinyint(3) NOT NULL DEFAULT '0',
  `conversion_rate` double NOT NULL,
  `equivalent_fc` double NOT NULL,
  `inserted_by` int(11) NOT NULL DEFAULT '0',
  `insert_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL DEFAULT '0',
  `update_date` datetime NOT NULL,
  `status_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `com_pre_export_finance_dtls`
--

LOCK TABLES `com_pre_export_finance_dtls` WRITE;
/*!40000 ALTER TABLE `com_pre_export_finance_dtls` DISABLE KEYS */;
INSERT INTO `com_pre_export_finance_dtls` VALUES (1,1,20,'123',10,1,2,78,0.0128,1,'2015-10-25 14:30:16',0,'0000-00-00 00:00:00',1,0),(2,2,10,'0000',15,621882,2,77.08,8068.0073,13,'2015-11-15 10:33:04',0,'0000-00-00 00:00:00',1,0),(3,3,10,'0000',15,621882,2,77.08,8068.0073,13,'2015-11-15 10:33:07',0,'0000-00-00 00:00:00',1,0),(4,4,10,'0000',15,621882,2,77.08,8068.0073,13,'2015-11-15 10:33:09',0,'0000-00-00 00:00:00',1,0);
/*!40000 ALTER TABLE `com_pre_export_finance_dtls` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:40
