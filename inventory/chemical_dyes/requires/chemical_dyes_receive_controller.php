﻿<?php
header('Content-type:text/html; charset=utf-8');
session_start();
include('../../../includes/common.php');
$user_id = $_SESSION['logic_erp']["user_id"];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];
$data=$_REQUEST['data'];
$action=$_REQUEST['action'];
//--------------------------------------------------------------------------------------------
if($db_type==0) { $find_inset="FIND_IN_SET(a.id,b.pi_id)";	}
if($db_type==2 || $db_type==1){ $find_inset=" ',' || b.pi_id || ',' LIKE '%,a.id,%' "; }
//load drop down supplier
if ($action=="load_drop_down_supplier")
{
	//echo create_drop_down( "cbo_supplier", 170, "select id,supplier_name from lib_supplier where FIND_IN_SET($data,tag_company) and FIND_IN_SET(3,party_type) order by supplier_name","id,supplier_name", 1, "-- Select --", 0, "",0 );  	 
	echo create_drop_down( "cbo_supplier", 170, "select a.id,a.supplier_name from lib_supplier a,  lib_supplier_tag_company b,lib_supplier_party_type c where a.id=c.supplier_id and a.id=b.supplier_id and b.tag_company='$data' and c.party_type='3' order by a.supplier_name","id,supplier_name", 1, "-- Select --", 0, "",0 );  	 
	exit();
}


//load drop down store
if ($action=="load_drop_down_store")
{

echo create_drop_down( "cbo_store_name", 170, "select lib_store_location.id,lib_store_location.store_name from lib_store_location,lib_store_location_category where lib_store_location.id=lib_store_location_category.store_location_id and lib_store_location.status_active=1 and lib_store_location.is_deleted=0 and lib_store_location.company_id=$data and lib_store_location_category.category_type in(5,6,7) group by lib_store_location.id,lib_store_location.store_name order by lib_store_location.store_name","id,store_name", 1, "-- Select --", 0, "",0 );  	 
	exit();
}


if ($action=="load_drop_down_location")
{
	echo create_drop_down( "cbo_location_name", 170, "select id,location_name from lib_location where company_id='$data' and status_active =1 and is_deleted=0 order by location_name","id,location_name", 1, "-- Select Location --", 0, "" );
	exit();
}

if ($action=="load_drop_down_item_group")
{
	//echo create_drop_down( "cbo_item_group_id", 170, "select id,location_name from lib_location where company_id='$data' and status_active =1 and is_deleted=0 order by location_name","id,location_name", 1, "-- Select Location --", 0, "" );
	echo create_drop_down( "cbo_item_group_id", 130,"select id,item_name  from lib_item_group where item_category='$data' and status_active=1","id,item_name", 1, "-- Select --", "", "","","","","","");

	exit();
}



// wo/pi popup here----------------------// 
if ($action=="wopi_popup")
{
  	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	extract($_REQUEST);  
?>
     
<script>
	function js_set_value(str)
	{
		var splitData = str.split("_");		 
		$("#hidden_tbl_id").val(splitData[0]); // wo/pi id
		$("#hidden_wopi_number").val(splitData[1]); // wo/pi number
		$("#hidden_is_non_ord_sample").val(splitData[2]); // wo/pi number
		parent.emailwindow.hide();
	}
</script>

</head>

<body>
<div align="center" style="width:100%;" >
<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
	<table width="800" cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
            <thead>
                <tr>                	 
                    <th width="150">Search By</th>
                    <th width="150" align="center" id="search_by_th_up">Enter WO/PI Number</th>
                    <th width="200">Date Range</th>
                    <th><input type="reset" name="re_button" id="re_button" value="Reset" style="width:100px" class="formbutton"  /></th>           
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <?php  
                            echo create_drop_down( "cbo_search_by", 170, $receive_basis_arr,"",1, "--Select--", $receive_basis,"",1 );
                        ?>
                    </td>
                    <td width="180" align="center" id="search_by_td">				
                        <input type="text" style="width:230px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />	
                    </td>    
                    <td align="center">
                        <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px" placeholder="From Date" />
                        <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px" placeholder="To Date" />
                     </td> 
                     <td align="center">
                        <input type="button" name="btn_show" class="formbutton" value="Show" onClick="show_list_view( document.getElementById('cbo_search_by').value+'_'+document.getElementById('txt_search_common').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+<?php echo $company; ?>, 'create_wopi_search_list_view', 'search_div', 'chemical_dyes_receive_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:100px;" />				
                    </td>
            </tr>
        	<tr>                  
            	<td align="center" height="40" valign="middle" colspan="4">
					<?php echo load_month_buttons(1);  ?>
                    <!-- Hidden field here-------->
                    <input type="hidden" id="hidden_tbl_id" value="" />
                    <input type="hidden" id="hidden_wopi_number" value="" />
                    <input type="hidden" id="hidden_is_non_ord_sample" value="" />
                    <!-- ---------END------------->
                </td>
            </tr>    
            </tbody>
         </tr>         
        </table>    
        <div align="center" valign="top" id="search_div"> </div> 
        </form>
   </div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}

if($action=="create_wopi_search_list_view")
{
	
 	$ex_data = explode("_",$data);
	$txt_search_by = $ex_data[0];
	$txt_search_common = trim($ex_data[1]);
	$txt_date_from = $ex_data[2];
	$txt_date_to = $ex_data[3];
	$company = $ex_data[4];
 	
	$sql_cond="";
	if(trim($txt_search_common)!="")
	{
		if(trim($txt_search_by)==1) // for pi
		{
			$sql_cond .= " and a.pi_number LIKE '%$txt_search_common%'";	
			if( $txt_date_from!="" || $txt_date_to!="" ) 
			if($db_type==0){$sql_cond .= " and a.pi_date  between '".change_date_format($txt_date_from,'yyyy-mm-dd')."' and '".change_date_format($txt_date_to,'yyyy-mm-dd')."'";}
			if($db_type==2 || $db_type==1){$sql_cond .= " and a.pi_date  between '".change_date_format($txt_date_from, "yyyy-mm-dd", "-",1)."' and '".change_date_format($txt_date_to, "yyyy-mm-dd", "-",1)."'";}
			if(trim($company)!="") $sql_cond .= " and a.importer_id='$company'";
		}
		else if(trim($txt_search_by)==2) // for wo
		{
			$sql_cond .= " and wo_number LIKE '%$txt_search_common%'";				
 			if( $txt_date_from!="" || $txt_date_to!="" )
			{
			if($db_type==0) { $sql_cond .= " and wo_date  between '".change_date_format($txt_date_from,'yyyy-mm-dd')."' and '".change_date_format($txt_date_to,'yyyy-mm-dd')."'";}
				if($db_type==2 || $db_type==1) { $sql_cond .= " and wo_date  between '".change_date_format($txt_date_from,'yyyy-mm-dd',"-",1)."' and '".change_date_format($txt_date_to,'yyyy-mm-dd',"-",1)."'";}
			}
			if(trim($company)!="") $sql_cond .= " and company_name='$company'";
		}
 	} 
 	
	if($txt_search_by==1 )
	{
 	
	$sql = "select a.id as id,a.pi_number as wopi_number,b.lc_number as lc_number,a.pi_date as wopi_date,a.supplier_id as supplier_id,a.currency_id as currency_id,a.source as source 
		   from com_pi_master_details a left join com_btb_lc_master_details b on $find_inset
		   where  
		   a.item_category_id in( 5,6,7 )and
		   a.status_active=1 and a.is_deleted=0 and
		   a.importer_id=$company 
		   $sql_cond";
	}
	else if($txt_search_by==2)
	{
 		$sql = "select id,wo_number as wopi_number,' ' as lc_number,wo_date as wopi_date,supplier_id as supplier_id,currency_id as currency_id,source as source 
				from wo_non_order_info_mst
				where 
				status_active=1 and is_deleted=0 and
				item_category in(5,6,7) and pay_mode!=2 and 
				company_name=$company
				$sql_cond";//supplier_id in (select id from lib_supplier where FIND_IN_SET($company,tag_company) )
	}
	
	
	$result = sql_select($sql);
 	$supplier_arr=return_library_array( "select id, supplier_name from lib_supplier",'id','supplier_name');
	$arr=array(3=>$supplier_arr,4=>$currency,5=>$source);
	echo  create_list_view("list_view", "WO/PI No, LC ,Date, Supplier, Currency, Source","120,120,120,200,120,120,120","900","260",0, $sql , "js_set_value", "id,wopi_number", "", 1, "0,0,0,supplier_id,currency_id,source", $arr, "wopi_number,lc_number,wopi_date,supplier_id,currency_id,source", "",'','0,0,0,0,0,0') ;	
	exit();	
	
}

//for gate entry 
// wo/pi popup here----------------------// 
if ($action=="gate_search")
{
  	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	extract($_REQUEST);  
?>
     
<script>
	function js_set_value(str)
	{
		var splitData = str.split("_");		 
		$("#hidden_system_id").val(splitData[1]); // wo/pi id
	
		parent.emailwindow.hide();
	}
</script>

</head>

<body>
<div align="center" style="width:100%;" >
<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
	<table width="800" cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
            <thead>
                <tr>                	 
                    <th width="150">Sample </th>
                    <th width="150">Supplier </th>
                    <th width="150" align="center" id="search_by_th_up"> Item Category</th>
                     <th width="150" align="center" >Chalan no</th>
                    <th width="200">Receive Date</th>
                    <th><input type="reset" name="re_button" id="re_button" value="Reset" style="width:100px" class="formbutton"  /></th>           
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        	<?php 
										
                                    	echo create_drop_down( "cbo_sample", 150, "select id,sample_name from lib_sample where status_active=1 and is_deleted=0 order by sample_name","id,sample_name",1, "-- Select --", 0, "" );
                                    ?>
                    </td>
                    <td width="180" align="center">				
                           <?php
					
                              echo create_drop_down( "cbo_supplier", 150, "select id,supplier_name from lib_supplier where status_active=1 and is_deleted=0 order by supplier_name","id,supplier_name", 1, "-- Select --", 0, "",0 );
                          ?>
                    </td>    
                    <td align="center">
                         <?php 
					     	echo create_drop_down( "cbo_item_category", 130,$item_category,"", 1, "-- Select --", $selected, "load_drop_down( 'requires/chemical_dyes_receive_controller', this.value, 'load_drop_down_item_group', 'item_group_td' )","","5,6,7","","","");
                           ?>
                     </td> 
                     <td align="center">
                        <input type="text" id="txt_chalan_no" name="txt_chalan_no" class="text_boxes" />
                     </td> 
                     <td>
                       <input type="text" name="txt_receive_date" id="txt_receive_date" class="datepicker" style="width:120px;" placeholder="Select Date" />
                     
                     </td>
                     <td align="center">
                        <input type="button" name="btn_show" class="formbutton" value="Show" onClick="show_list_view( document.getElementById('cbo_sample').value+'_'+document.getElementById('cbo_supplier').value+'_'+document.getElementById('cbo_item_category').value+'_'+document.getElementById('txt_chalan_no').value+'_'+document.getElementById('txt_receive_date').value+'_'+<?php echo $company; ?>, 'create_gate_list_view', 'search_div', 'chemical_dyes_receive_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:100px;" />				
                    </td>
            </tr>
        	<tr>                  
            	<td align="center" height="40" valign="middle" colspan="4">
				
                    <!-- Hidden field here-------->
                    <input type="hidden" id="hidden_system_id" value="" />
                 
                    <!-- ---------END------------->
                </td>
            </tr>    
            </tbody>
         </tr>         
        </table>    
        <div align="center" valign="top" id="search_div"> </div> 
        </form>
   </div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}



if($action=="create_gate_list_view")
{
	
 	$ex_data = explode("_",$data);
	$sample = $ex_data[0];
	$supplier = $ex_data[1];
	$item_category = $ex_data[2];
	$chalan_no = $ex_data[3];
	$txt_receive_date = $ex_data[4];
	$company=$ex_data[5];
 
	$sql_cond="";
	
	  if(str_replace("'","",$sample)!=0)  $sql_cond .= " and a.sample=".str_replace("'","",$sample)."";
	   if(str_replace("'","",$supplier)!=0)  $sql_cond .= " and a.supplier_name=".str_replace("'","",$supplier)."";
	    if(str_replace("'","",$item_category)!=0) 
		{ $sql_cond .= " and a.item_category=".str_replace("'","",$item_category)."";} else {$sql_cond .= " and a.item_category in(5,6,7)";}
		 if(str_replace("'","",$chalan_no)!="")  $sql_cond .= " and a.challan_no='".str_replace("'","",$chalan_no)."'";
			
			if(str_replace("'","",$txt_receive_date)!="" ) 
			{
			if($db_type==0){$sql_cond .= " and a.receive_date='".change_date_format($txt_receive_date,'yyyy-mm-dd')."'";}
			if($db_type==2 || $db_type==1){$sql_cond .= " and a.receive_date='".change_date_format($txt_receive_date, "yyyy-mm-dd", "-",1)."'";}
			}
			if(trim($company)!="") $sql_cond .= " and a.company_name='$company'";
	
	

 	
    $sql="select a.id,a.sys_number,a.item_category,a.sample,a.supplier_name ,a.challan_no ,a.receive_date from inv_gate_in_mst a where a.status_active=1 and a.is_deleted=0 $sql_cond and a.status_active=1 and a.is_deleted=0";

	$result = sql_select($sql);
	$item_category_arr=array(5=>"Chemicals",6=>"Dyes",7=>"Auxilary Chemicals");
 	$supplier_arr=return_library_array( "select id, supplier_name from lib_supplier",'id','supplier_name');
	$sample_arr=return_library_array( "select id, sample_name from lib_sample",'id','sample_name');
	$arr=array(0=>$item_category_arr,3=>$sample_arr,2=>$supplier_arr);
	echo  create_list_view("list_view", "Item Category,Gate Entry No, Supplier, Sample, Chalan No ,Receive Date","120,170,150,150,120,120","860","260",0, $sql , "js_set_value", "id,sys_number", "", 1, "item_category,0,sample,supplier_name,0,0", $arr, "item_category,sys_number,sample,supplier_name,challan_no,receive_date", "",'','0,0,0,0,0,0') ;	
	exit();	
	
}




if($action=="populate_data_from_wopi_popup")
{
	$ex_data = explode("**",$data);
	$receive_basis = $ex_data[0];
	$wo_pi_ID = $ex_data[1];
	
	if($receive_basis==1 )
	{
 		$sql = "select b.id as id, b.lc_number as lc_number,a.supplier_id as supplier_id,a.currency_id as currency_id,a.source as source 
				from com_pi_master_details a left join com_btb_lc_master_details b on $find_inset
				where  
				a.item_category_id in( 5,6,7) and
				a.status_active=1 and a.is_deleted=0 and
				a.id=$wo_pi_ID";
	}
	else if($receive_basis==2)
	{
 		$sql = "select id,'' as lc_number,supplier_id as supplier_id,currency_id as currency_id,source as source 
				from wo_non_order_info_mst
				where 
				status_active=1 and is_deleted=0 and
				item_category in( 5,6,7) and 
				id=$wo_pi_ID";
	}
	
	$result = sql_select($sql);
	foreach($result as $row)
	{
		echo "$('#cbo_supplier').val(".$row[csf("supplier_id")].");\n";
		echo "$('#cbo_currency').val(".$row[csf("currency_id")].");\n";
		echo "$('#cbo_source').val(".$row[csf("source")].");\n";
		echo "$('#txt_lc_no').val('".$row[csf("lc_number")]."');\n";
		if($row[csf("lc_number")]!="")
		{
			echo "$('#hidden_lc_id').val(".$row[csf("id")].");\n";
		}
		if($row[csf("currency_id")]==1)
		{
			echo "$('#txt_exchange_rate').val(1);\n";
			echo "$('#txt_exchange_rate').attr('disabled','disabled');\n";
		}
		if($row[csf("currency_id")]!=1)
		{
			$sql1 = sql_select("select exchange_rate,max(id) from inv_receive_master where item_category in(5,6,7)");
			foreach($sql1 as $row1)
			{
				echo "$('#txt_exchange_rate').val(".$row1[csf("exchange_rate")].");\n";
			}
			
			echo "$('#txt_exchange_rate').removeAttr('disabled','disabled');\n";
		}
	}
	exit();	
}
if($action=="set_exchange_rate")
{
	$sql1 = sql_select("select exchange_rate,max(id) from inv_receive_master where item_category=3");
	foreach($sql1 as $row1)
	{
		echo $row1[csf("exchange_rate")];
	}
}





if($action=="show_product_listview")
{ 
	$ex_data = explode("**",$data);
	$receive_basis = $ex_data[0];
	$wo_pi_ID = $ex_data[1];
	
	if($receive_basis==1) // pi basis
	{	
	if($db_type==0)
		  {
	    	$sql = "select a.importer_id as company_id, a.supplier_id,b.id,b.item_prod_id as item_id , sum(b.quantity) as quantity,b.rate  
		from com_pi_master_details a left join com_pi_item_details b on FIND_IN_SET(a.id,b.pi_id)
		 where a.id=$wo_pi_ID group by b.item_prod_id,b.rate "; 
		  }
	   if($db_type==2 || $db_type==1)
		  {
			$sql = "select a.importer_id as company_id, a.supplier_id,b.id,b.item_prod_id as item_id , sum(b.quantity) as quantity,b.rate  
        from com_pi_master_details a left join com_pi_item_details b on a.id=b.pi_id
        where a.id=$wo_pi_ID group by b.item_prod_id,b.rate,a.importer_id , a.supplier_id,b.id,b.item_prod_id "; 
		  } 
		
	}  
	else if($receive_basis==2) // wo basis
	{
		 if($db_type==0)
		  {
		  $sql = "select a.company_name as company_id,a.supplier_id,b.id,b.item_id,sum(b.supplier_order_quantity) as quantity,b.rate
		from wo_non_order_info_mst a, wo_non_order_info_dtls b 
		where a.id=b.mst_id and a.id=$wo_pi_ID group by b.item_id,b.rate";
		  }
	 if($db_type==2 || $db_type==1)
		  {
		  $sql = "select a.company_name as company_id,a.supplier_id,b.id,b.item_id,sum(b.supplier_order_quantity) as quantity,b.rate
        from wo_non_order_info_mst a, wo_non_order_info_dtls b 
        where a.id=b.mst_id and a.id=$wo_pi_ID group by b.item_id,b.rate,a.company_name ,a.supplier_id,b.id";
		  }
	}	

	$result = sql_select($sql);
	
	   if($db_type==0)
		  {
	   $product_name_details=return_library_array( "select id, concat(sub_group_name,' ',item_description,' ',item_size ) as product_name_details from product_details_master where item_category_id in(5,6,7)",'id','product_name_details');
		  }
	 if($db_type==2 || $db_type==1)
		  {
			  $product_name_details=return_library_array( "select id, concat(concat(sub_group_name,item_description),item_size ) as product_name_details from product_details_master where item_category_id in(5,6,7)",'id','product_name_details');
		  }

	//$color_name_arr=return_library_array( "select id, color_name from lib_color",'id','color_name');
	$i=1; 
	?>
     
    	<table class="rpt_table" border="1" cellpadding="2" cellspacing="0">
        	<thead><tr><th>SL</th><th>Product Name</th><th>Qnty</th></tr></thead>
            <tbody>
            	<?php foreach($result as $row){  
					
					if ($i%2==0)$bgcolor="#E9F3FF";						
					else $bgcolor="#FFFFFF";
				?>
                	<tr bgcolor="<?php echo $bgcolor; ?>" onClick='get_php_form_data("<?php echo $receive_basis."**".$row[csf("id")]."**".$row[csf("company_id")]."**".$row[csf("supplier_id")]."**".$row[csf("item_id")];?>","wo_pi_product_form_input","requires/chemical_dyes_receive_controller")' style="cursor:pointer" >
                		<td><?php echo $i; ?></td>
                    	<td><?php echo $product_name_details[$row[csf("item_id")]]; ?></td>
                        <td><?php echo $row[csf('quantity')]; ?></td>
                    </tr>
                <?php $i++; } ?>
            </tbody>
        </table>
     </fieldset>   
	<?php	 
	exit();
}


// get form data from product click in right side
if($action=="wo_pi_product_form_input")
{
	
	$ex_data = explode("**",$data);
	$receive_basis = $ex_data[0];
	$wo_pi_ID = $ex_data[1];
	$company_id = $ex_data[2];
	$supplier_id = $ex_data[3];
	$product_id = $ex_data[4];
	
	if($receive_basis==1) // pi basis
	{	
  		$sql = "select a.item_prod_id as product_id, a.quantity,a.uom,a.rate,a.amount, b.id,b.item_category_id, b.item_group_id, b.item_description, b.lot  
		from com_pi_item_details a, product_details_master b 
		where a.item_prod_id=b.id and  a.id=$wo_pi_ID";		
	}
	
	else if($receive_basis==2) // wo basis
	{
 		$sql = "select a.item_id as product_id ,a.supplier_order_quantity,a.uom,a.rate,a.amount,b.id,b.item_category_id, b.item_group_id, b.item_description, b.lot
		from wo_non_order_info_dtls a,  product_details_master b 
		where a.item_id=b.id and a.id=$wo_pi_ID ";		
	}
	else
	{
		$sql = "select id as product_id,item_category_id, item_group_id, item_description,  unit_of_measure as  uom, avg_rate_per_unit as rate
		from   product_details_master 
		where id=$product_id";
	}
	
	$result = sql_select($sql); 
	foreach($result as $row)
	{ 
	    echo "reset_form('','','cbo_item_category_id*cbo_item_group_id*txt_description*txt_product_id*txt_lot*cbo_uom*txt_receive_qty*txt_rate*txt_ile*txt_amount*txt_book_currency*txt_bla_order_qty*txt_expire_date*txt_room*txt_rack*txt_self*txt_binbox*update_dtls_id','','','');\n";
	 	echo "$('#cbo_item_category_id').val(".$row[csf("item_category_id")].");\n";		
		echo "$('#cbo_item_group_id').val(".$row[csf("item_group_id")].");\n";
		echo "$('#txt_description').val('".$row[csf("item_description")]."');\n";
		echo "$('#txt_product_id').val('".$row[csf("product_id")]."');\n";
		echo "$('#cbo_uom').val(".$row[csf("uom")].");\n";
		if($receive_basis !=4 && $receive_basis !=6) 
		{
			echo "$('#txt_rate').val(".$row[csf("rate")].");\n";
		}
	}

		
	exit();	 
}





// LC popup here----------------------// 
if ($action=="lc_popup")
{
  	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	extract($_REQUEST);  
?>
     
<script>
function js_set_value(str)
{
		var splitData = str.split("_");		 
		$("#hidden_tbl_id").val(splitData[0]); // wo/pi id
		$("#hidden_wopi_number").val(splitData[1]); // wo/pi number
		parent.emailwindow.hide();
}
	
	
</script>

</head>

<body>
<div align="center" style="width:100%;" >
<form name="searchlcfrm_1" id="searchlcfrm_1" autocomplete="off">
	<table width="600" cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
            <thead>
                <tr>                	 
                    <th width="150">Search By</th>
                    <th width="150" align="center" id="search_by_td_up">Enter WO/PI Number</th>
                    <th>
                    	<input type="reset" name="re_button" id="re_button" value="Reset" style="width:100px" class="formbutton"  />
                   		<!-- Hidden field here-------->
                        <input type="hidden" id="hidden_tbl_id" value="" />
                        <input type="hidden" id="hidden_wopi_number" value="hidden_wopi_number" />
                        <!-- ---------END------------->
                    </th>           
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <?php  
                            $search_by_arr=array(0=>'LC Number',1=>'Supplier Name');
							$dd="change_search_event(this.value, '0*1', '0*select id, supplier_name from lib_supplier', '../../') ";							
							echo create_drop_down( "cbo_search_by", 170, $search_by_arr,"",0, "--Select--", "",$dd,0 );
                        ?>
                    </td>
                    <td width="180" align="center" id="search_by_td">				
                        <input type="text" style="width:230px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />	
                    </td> 
                     <td align="center">
                        <input type="button" name="btn_show" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_search_by').value+'_'+document.getElementById('txt_search_common').value+'_'+<?php echo $company; ?>, 'create_lc_search_list_view', 'search_div', 'chemical_dyes_receive_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:100px;" />				
                    </td>
           	 	</tr> 
            </tbody>         
        </table>    
        <div align="center" valign="top" id="search_div"> </div> 
        </form>
   </div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php

}


if($action=="create_lc_search_list_view")
{
	$ex_data = explode("_",$data);
	$cbo_search_by = $ex_data[0];
	$txt_search_common = $ex_data[1];
	$company = $ex_data[2];
	
	if($cbo_search_by==1 && $txt_search_common!="") // lc number
	{
		$sql= "select id,lc_number,item_category_id,lc_serial,supplier_id,importer_id,lc_value from com_btb_lc_master_details where lc_number LIKE '%$search_string%' and importer_id=$company and item_category_id in(5,6,7) and is_deleted=0 and status_active=1";
	} 
	else if($cbo_search_by==1 && $txt_search_common!="") //supplier
	{
		$sql= "select id,lc_number,item_category_id,lc_serial,supplier_id,importer_id,lc_value from com_btb_lc_master_details where supplier_id='$search_string' and importer_id=$company and item_category_id in(5,6,7) and is_deleted=0 and status_active=1";
	} 
	else
	{
		$sql= "select id,lc_number,item_category_id,lc_serial,supplier_id,importer_id,lc_value from com_btb_lc_master_details where importer_id=$company and item_category_id in(5,6,7) and is_deleted=0 and status_active=1";
	}
	
	$company_arr = return_library_array("select id,company_name from lib_company","id","company_name");
	$supplier_arr = return_library_array( "select id, supplier_name from lib_supplier",'id','supplier_name');
	$arr=array(1=>$company_arr,2=>$supplier_arr,3=>$item_category);
	echo  create_list_view("list_view", "LC No,Importer,Supplier Name,Item Category,Value","120,150,150,120,120","750","260",0, $sql , "js_set_value", "id,lc_number", "", 1, "0,importer_id,supplier_id,item_category_id,0", $arr, "lc_number,importer_id,supplier_id,item_category_id,lc_value", "",'','0,0,0,0,0,1') ;	
	exit();
	
}


if($action=="show_ile")
{
	$ex_data = explode("**",$data);
	$company = $ex_data[0];
	$source = $ex_data[1];
	$rate = $ex_data[2];
	$cbo_item_category_id = $ex_data[3];
	
	$sql="select standard from variable_inv_ile_standard where source='$source' and company_name='$company' and category='$cbo_item_category_id' and status_active=1 and is_deleted=0 order by id";
	//echo $sql;
	$result=sql_select($sql);
	foreach($result as $row)
	{
		// NOTE :- ILE=standard, ILE% = standard/100*rate
		$ile = $row[csf("standard")];
		$ile_percentage = ( $row[csf("standard")]/100 )*$rate;
		echo $ile."**".number_format($ile_percentage,$dec_place[3],".","");
		exit();
	}
	exit();
}



//data save update delete here------------------------------//
if($action=="save_update_delete")
{	  
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	if( $operation==0 ) // Insert Here----------------------------------------------------------
	{
		$con = connect();
		if($db_type==0)	{ mysql_query("BEGIN"); }
		//table lock here  
		check_table_status( $_SESSION['menu_id'],0);
		if( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0"; die;}
		 
		 
		//---------------Check Duplicate product in Same return number ------------------------//
		$duplicate = is_duplicate_field("b.id","inv_receive_master a, inv_transaction b","a.id=b.mst_id and a.recv_number=$txt_mrr_no and b.prod_id=$txt_product_id and b.transaction_type=1"); 
		if($duplicate==1 && str_replace("'","",$txt_mrr_no)=="") 
		{
			check_table_status( $_SESSION['menu_id'],0);
			echo "20**Duplicate Product is Not Allow in Same Return Number.";
			die;
		}
		//------------------------------Check Brand END---------------------------------------//
		 
		
		
		$sql = sql_select("select product_name_details,avg_rate_per_unit,last_purchased_qnty,current_stock,stock_value from product_details_master where id=$txt_product_id");
		$presentStock=$presentStockValue=$presentAvgRate=0;
		$product_name_details="";
		foreach($sql as $result)
		{
			$presentStock			=$result[csf("current_stock")];
			$presentStockValue		=$result[csf("stock_value")];
			$presentAvgRate			=$result[csf("avg_rate_per_unit")];
			$product_name_details 	=$result[csf("product_name_details")];
		}		 
		//----------------Check Product ID END---------------------//
	 		
		

		
		$cd_recv_num=''; $cd_update_id=''; $flag=1;
		
		if(str_replace("'","",$update_id)=="")
		{ 
		 if($db_type==0)
		    {
			$new_chemical_dyes_recv_system_id=explode("*",return_mrr_number( str_replace("'","",$cbo_company_name), '', 'CDR', date("Y",time()), 5, "select recv_number_prefix, recv_number_prefix_num from inv_receive_master where company_id=$cbo_company_name and entry_form='4' and YEAR(insert_date)=".date('Y',time())." order by recv_number_prefix_num desc ", "recv_number_prefix", "recv_number_prefix_num" ));
		    }
		   
		 if($db_type==2 || $db_type==1)
		    {
			$new_chemical_dyes_recv_system_id=explode("*",return_mrr_number( str_replace("'","",$cbo_company_name), '', 'CDR', date("Y",time()), 5, "select recv_number_prefix, recv_number_prefix_num from inv_receive_master where company_id=$cbo_company_name and entry_form='4' and extract(year from insert_date)=".date('Y',time())." order by recv_number_prefix_num desc ", "recv_number_prefix", "recv_number_prefix_num" ));
		    }
		 	
			$id=return_next_id( "id", "inv_receive_master", 1 ) ;
					 
			$field_array="id, recv_number_prefix, recv_number_prefix_num, recv_number, entry_form, item_category,company_id, receive_basis,receive_purpose,loan_party, receive_date,  booking_id, booking_no, booking_without_order,challan_no, store_id, location_id,supplier_id,lc_no, currency_id,exchange_rate, source,gate_entry_no, inserted_by, insert_date";
			
			$data_array="(".$id.",'".$new_chemical_dyes_recv_system_id[1]."',".$new_chemical_dyes_recv_system_id[2].",'".$new_chemical_dyes_recv_system_id[0]."',4,".$cbo_item_category_id.",".$cbo_company_name.",".$cbo_receive_basis.",".$cbo_receive_purpose.",".$cbo_loan_party.",".$txt_receive_date.",".$txt_wo_pi_id.",".$txt_wo_pi.",1,".$txt_challan_no.",".$cbo_store_name.",".$cbo_location_name.",".$cbo_supplier.",".$hidden_lc_id.",".$cbo_currency.",".$txt_exchange_rate.",".$cbo_source.",".$txt_gate_entry.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
			
			//echo "insert into inv_receive_master (".$field_array.") values ".$data_array;
			
			
			$cd_recv_num=$new_chemical_dyes_recv_system_id[0];
			$cd_update_id=$id;
		}
		else
		{
			$field_array_update="item_category*receive_basis*receive_purpose*loan_party*receive_date*booking_id*booking_no*booking_without_order*challan_no*store_id*location_id*supplier_id*lc_no*currency_id*exchange_rate*source*gate_entry_no*updated_by*update_date";
			
			$data_array_update=$cbo_item_category_id."*".$cbo_receive_basis."*".$cbo_receive_purpose."*".$cbo_loan_party."*".$txt_receive_date."*".$txt_wo_pi_id."*".$txt_wo_pi."*1*".$txt_challan_no."*".$cbo_store_name."*".$cbo_location_name."*".$cbo_supplier."*".$hidden_lc_id."*".$cbo_currency."*".$txt_exchange_rate."*".$cbo_source."*".$txt_gate_entry."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
			
			
			$cd_recv_num=str_replace("'","",$txt_mrr_no);
			$cd_update_id=str_replace("'","",$update_id);
		}
		
		
		
		// yarn details table entry here START-----------------------------------//		
		$rate = str_replace("'","",$txt_rate);
		$txt_ile = str_replace("'","",$txt_ile);
		$txt_receive_qty = str_replace("'","",$txt_receive_qty);
		$ile = ($txt_ile/$rate)*100; // ile cost to ile
		$ile_cost = str_replace("'","",$txt_ile); //ile cost = (ile/100)*rate
		$exchange_rate = str_replace("'","",$txt_exchange_rate);
		$conversion_factor = 1; // woven Fabric always Yds	
		$domestic_rate = return_domestic_rate($rate,$ile_cost,$exchange_rate,$conversion_factor);
 		$cons_rate = number_format($domestic_rate,$dec_place[3],".","");//number_format($rate*$exchange_rate,$dec_place[3],".","");
		$con_amount = $cons_rate*$txt_receive_qty;
		$con_ile = $ile;//($ile/$domestic_rate)*100;
		$con_ile_cost = ($ile/100)*($rate*$exchange_rate);
		
		$dtlsid = return_next_id("id", "inv_transaction", 1);		 
		//$transaction_type=array(1=>"Receive",2=>"Issue",3=>"Receive Return",4=>"Issue Return");
		$field_array1 = "id,mst_id,receive_basis,pi_wo_batch_no,company_id,supplier_id,prod_id,item_category,transaction_type,transaction_date,store_id, order_uom, order_qnty, order_rate, order_ile,order_ile_cost, order_amount, cons_uom, cons_quantity, cons_rate, cons_ile, cons_ile_cost, cons_amount,balance_qnty, balance_amount,room,rack, self,bin_box, batch_lot,expire_date,inserted_by,insert_date";
 		$data_array1= "(".$dtlsid.",".$cd_update_id.",".$cbo_receive_basis.",".$txt_wo_pi_id.",".$cbo_company_name.",".$cbo_supplier.",".$txt_product_id.",".$cbo_item_category_id.",1,".$txt_receive_date.",".$cbo_store_name.",".$cbo_uom.",".$txt_receive_qty.",".$txt_rate.",".$ile.",'".$ile_cost."',".$txt_amount.",".$cbo_uom.",".$txt_receive_qty.",".$cons_rate.",".$con_ile.",".$con_ile_cost.",".$con_amount.",".$txt_receive_qty.",".$con_amount.",".$txt_room.",".$txt_rack.",".$txt_self.",".$txt_binbox.",".$txt_lot.",".$txt_expire_date.",'".$_SESSION['logic_erp']['user_id']."','".$pc_date_time."')";
		//echo "INSERT INTO inv_transaction (".$field_array.") VALUES ".$data_array.""; die;
		//echo $field_array."<br>".$data_array;die;
	
		//yarn details table entry here END-----------------------------------//
		//echo "insert into inv_transaction ($field_array1) values ".$data_array1;die;
		
		//product master table data UPDATE START----------------------------------------------------------//	
		$stock_value 	= $domestic_rate*$txt_receive_qty;
  		$currentStock 	= $presentStock+$txt_receive_qty;
		$StockValue	 	= $presentStockValue+$stock_value;
		$avgRate		= abs($StockValue/$currentStock);
 		$field_array2="avg_rate_per_unit*last_purchased_qnty*current_stock*stock_value*updated_by*update_date";
 		$data_array2="".number_format($avgRate,$dec_place[3],".","")."*".$txt_receive_qty."*".$currentStock."*".number_format($StockValue,$dec_place[4],".","")."*'".$_SESSION['logic_erp']['user_id']."'*'".$pc_date_time."'";
		
		if(str_replace("'","",$update_id)=="")
			{ 
			   $rID=sql_insert("inv_receive_master",$field_array,$data_array,0);
			   if($rID) $flag=1; else $flag=0;
			}
		else
			{
		      $rID=sql_update("inv_receive_master",$field_array_update,$data_array_update,"id",$update_id,1);
			  if($rID) $flag=1; else $flag=0; 
				
			}
		$dtlsrID = sql_insert("inv_transaction",$field_array1,$data_array1,1);
		if($flag==1) 
		{
			if($dtlsrID) $flag=1; else $flag=0; 
		} 
		
		
		$prodUpdate = sql_update("product_details_master",$field_array2,$data_array2,"id",$txt_product_id,1);
		if($flag==1) 
		{
			if($prodUpdate) $flag=1; else $flag=0; 
		}
		//------------------ product_details_master END---------------------------------------------------//
		
		
 		
		//release lock table
		check_table_status( $_SESSION['menu_id'],0);
		
		if($db_type==0)
		{
			if($flag==1)
			{
				mysql_query("COMMIT");  
				echo "0**".$cd_update_id."**".$cd_recv_num."**0";
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "5**0**"."&nbsp;"."**0";
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
			//echo $flag;die;
			if($flag==1)
			{
				oci_commit($con);  
				echo "0**".$cd_update_id."**".$cd_recv_num."**0";
			}
			else
			{
				oci_rollback($con); 
				echo "5**0**"."&nbsp;"."**0";
			}
		}
		disconnect($con);
		die;
				
	}	
	else if ($operation==1) // Update Here----------------------------------------------------------
	{
		$con = connect();		
		if($db_type==0)	{ mysql_query("BEGIN"); }
		//table lock here 
		check_table_status( $_SESSION['menu_id'],0);
		if( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0"; die;}
		
		//check update id
		if( str_replace("'","",$update_id) == "" )
		{
			echo "15";exit(); 
		}
		//previous product stock adjust here--------------------------//
		//product master table UPDATE here START ---------------------//
		$sql = sql_select("select a.prod_id,a.cons_quantity,a.cons_rate,a.cons_amount,b.avg_rate_per_unit,b.current_stock,b.stock_value from inv_transaction a, product_details_master b where a.id=$update_dtls_id and a.prod_id=b.id");
		
		$before_prod_id="";
		$before_receive_qnty=0;
		$before_rate=0;
		$beforeAmount=0;
		$beforeStock=0;
		$beforeStockValue=0;
		$beforeAvgRate=0;
		foreach( $sql as $row)
		{
			$before_prod_id 		= $row[csf("prod_id")]; 
			$before_receive_qnty 	= $row[csf("cons_quantity")]; //stock qnty
			$before_rate 			= $row[csf("cons_rate")]; 
			$beforeAmount			= $row[csf("cons_amount")]; //stock value
			
			$before_brand 			=$row[csf("brand")];
			$beforeStock			=$row[csf("current_stock")];
			$beforeStockValue		=$row[csf("stock_value")];
			$beforeAvgRate			=$row[csf("avg_rate_per_unit")];	
		}
		
		//stock value minus here---------------------------//
		$adj_beforeStock			=$beforeStock-$before_receive_qnty;
		$adj_beforeStockValue		=$beforeStockValue-$beforeAmount;
		$adj_beforeAvgRate			=number_format(($adj_beforeStockValue/$adj_beforeStock),$dec_place[3],'.','');	
		
		//product master table UPDATE here END   ---------------------//
		//----------------- END PREVIOUS STOCK ADJUST-----------------//
				
		//current product stock-------------------------//
		$sql = sql_select("select product_name_details,avg_rate_per_unit,last_purchased_qnty,current_stock,stock_value from product_details_master where id=$txt_product_id");
		$presentStock=$presentStockValue=$presentAvgRate=0;
		$product_name_details="";
		foreach($sql as $result)
		{
			$presentStock			=$result[csf("current_stock")];
			$presentStockValue		=$result[csf("stock_value")];
			$presentAvgRate			=$result[csf("avg_rate_per_unit")];
			$product_name_details 	=$result[csf("product_name_details")];
		}	 
		//----------------Check Product ID END---------------------//
 		
		$flag=1;
		$field_array_update="receive_basis*receive_purpose*loan_party*receive_date*challan_no*booking_id*booking_no*store_id*location_id*supplier_id*lc_no*currency_id*exchange_rate*source*gate_entry_no*updated_by*update_date";
		$data_array_update=$cbo_receive_basis."*".$cbo_receive_purpose."*".$cbo_loan_party."*".$txt_receive_date."*".$txt_challan_no."*".$txt_wo_pi_id."*".$txt_wo_pi."*".$cbo_store_name."*".$cbo_location_name."*".$cbo_supplier."*".$hidden_lc_id."*".$cbo_currency."*".$txt_exchange_rate."*".$cbo_source."*".$txt_gate_entry."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
	
		//yarn master table UPDATE here END---------------------------------------// 
		// yarn details table UPDATE here START-----------------------------------//		
		$rate = str_replace("'","",$txt_rate);
		$txt_ile = str_replace("'","",$txt_ile);
		$txt_receive_qty = str_replace("'","",$txt_receive_qty);
		$ile = ($txt_ile/$rate)*100; // ile cost to ile
		$ile_cost = str_replace("'","",$txt_ile); //ile cost = (ile/100)*rate
		$exchange_rate = str_replace("'","",$txt_exchange_rate);
		$conversion_factor = 1; // yarn always KG	
		$domestic_rate = return_domestic_rate($rate,$ile_cost,$exchange_rate,$conversion_factor);
 		$cons_rate = number_format($domestic_rate,$dec_place[3],".","");//number_format($rate*$exchange_rate,$dec_place[3],".","");
		
		$con_amount = $cons_rate*$txt_receive_qty;
		$con_ile = $ile;  
		$con_ile_cost = ($ile/100)*($rate*$exchange_rate);
		//echo "20**".$con_ile_cost; mysql_query("ROLLBACK"); die;
		$field_array = "receive_basis*pi_wo_batch_no*company_id*supplier_id*prod_id*item_category*transaction_date*store_id*order_uom*order_qnty*order_rate*order_ile*order_ile_cost*order_amount*cons_uom*cons_quantity*cons_rate*cons_ile*cons_ile_cost*cons_amount*balance_qnty*balance_amount*room*rack*self*bin_box*batch_lot*expire_date*updated_by*update_date";
 		$data_array = "".$cbo_receive_basis."*".$txt_wo_pi_id."*".$cbo_company_name."*".$cbo_supplier."*".$txt_product_id."*".$cbo_item_category_id."*".$txt_receive_date."*".$cbo_store_name."*".$cbo_uom."*".$txt_receive_qty."*".$txt_rate."*".$ile."*'".$ile_cost."'*".$txt_amount."*".$cbo_uom."*".$txt_receive_qty."*".$cons_rate."*".$con_ile."*".$con_ile_cost."*".$con_amount."*".$txt_receive_qty."*".$con_amount."*".$txt_room."*".$txt_rack."*".$txt_self."*".$txt_binbox."*".$txt_lot."*".$txt_expire_date."*'".$_SESSION['logic_erp']['user_id']."'*'".$pc_date_time."'";
		//echo $field_array."<br>".$data_array;die;
 	
		//yarn details table UPDATE here END-----------------------------------//
			
    		
		//product master table data UPDATE START----------------------------------------------------------// 
		$field_array1="avg_rate_per_unit*last_purchased_qnty*current_stock*stock_value*updated_by*update_date";
		
		
		if($before_prod_id==str_replace("'","",$txt_product_id))
		{
			$currentStock	=$adj_beforeStock+$txt_receive_qty;
			$StockValue		=$adj_beforeStockValue+($domestic_rate*$txt_receive_qty);
			$avgRate		=number_format(abs($StockValue/$currentStock,$dec_place[3]),'.','');
 			$data_array1= "".$avgRate."*".$txt_receive_qty."*".$currentStock."*".number_format($StockValue,$dec_place[4],'.','')."*'".$user_id."'*'".$pc_date_time."'";
			 
		}
		
		else
		{ 
			//before
			$updateID_array=$update_data=array();
			$updateID_array[]=$before_prod_id;
			$update_data[$before_prod_id]=explode("*",("".$adj_beforeAvgRate."*0*".$adj_beforeStock."*".number_format($adj_beforeStockValue,$dec_place[4],'.','')."*'".$user_id."'*'".$pc_date_time."'"));
			//current			 
 			$presentStock 			= $presentStock+$txt_receive_qty;
			$presentStockValue	 	= $presentStockValue+($domestic_rate*$txt_receive_qty);
			$presentAvgRate			= number_format(abs($presentStockValue/$presentStock,$dec_place[3]),'.','');
			$updateID_array[]=$txt_product_id;
			$update_data[$txt_product_id]=explode("*",("".$presentAvgRate."*0*".$presentStock."*".number_format($presentStockValue,$dec_place[4],'.','')."*'".$user_id."'*'".$pc_date_time."'"));
			//echo bulk_update_sql_statement("product_details_master","id",$field_array,$update_data,$updateID_array);
		
		}
		$rID=sql_update("inv_receive_master",$field_array_update,$data_array_update,"id",$update_id,1);
		if($flag==1) { if($rID) $flag=1; else $flag=0; }
		
		$dtlsrID = sql_update("inv_transaction",$field_array,$data_array,"id",$update_dtls_id,1); 
		if($flag==1) { if($dtlsrID) $flag=1; else $flag=0; } 
		
		if($before_prod_id==str_replace("'","",$txt_product_id))
			{
				$prodUpdate = sql_update("product_details_master",$field_array1,$data_array1,"id",$txt_product_id,1);
				if($flag==1) {if($prodUpdate) $flag=1; else $flag=0; }
			}
		else
			{
				
				$prodUpdate=execute_query(bulk_update_sql_statement("product_details_master","id",$field_array1,$update_data,$updateID_array));
				if($flag==1) {if($prodUpdate) $flag=1; else $flag=0;} 	
			}
			
		check_table_status( $_SESSION['menu_id'],0);
		
		if($db_type==0)
		{
			if($flag==1)
			{
				mysql_query("COMMIT");  
				echo "1**".str_replace("'","",$update_id)."**".str_replace("'","",$txt_mrr_no)."**0";
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "6**0**"."&nbsp;"."**0";
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
			if($flag==1)
			{
				oci_commit($con);
				echo "1**".str_replace("'","",$update_id)."**".str_replace("'","",$txt_mrr_no)."**0";
			}
			else
			{
				oci_rollback($con);
				echo "6**0**"."&nbsp;"."**0";
			}
		}
		disconnect($con);
		die;
 	}
	else if ($operation==2) // Delete Here----------------------------------------------------------
	{
		$con = connect(); 
		if($db_type==0)	{ mysql_query("BEGIN"); }
		// master table delete here---------------------------------------
		$mst_id = return_field_value("id","inv_receive_master","recv_number like $txt_recv_number");	
		if($mst_id=="" || $mst_id==0){ echo "15**0"; die;}
 		$rID = sql_update("inv_receive_master",'status_active*is_deleted','0*1',"id","$mst_id",0);
		$dtlsrID = sql_update("inv_transaction",'status_active*is_deleted','0*1',"mst_id","$mst_id",1);
		if($db_type==0)
		{
			if($rID && $dtlsrID)
			{
				mysql_query("COMMIT");  
					echo "2**".str_replace("'","",$update_id)."**".str_replace("'","",$txt_mrr_no)."**0";
			}
			else
			{
				mysql_query("ROLLBACK"); 
					echo "10**".str_replace("'","",$update_id)."**".str_replace("'","",$txt_mrr_no)."**0";
			}
		}
		if($db_type==2 || $db_type==1 )
		{  
		 if($rID && $dtlsrID)
		     {
				 oci_commit($con);
				echo "2**".str_replace("'","",$update_id)."**".str_replace("'","",$txt_mrr_no)."**0";
		     }
		else
			{
				oci_rollback($con);
				echo "10**".str_replace("'","",$update_id)."**".str_replace("'","",$txt_mrr_no)."**0";
			}
		}
		disconnect($con);
		die;
	}		
}




if($action=="mrr_popup")
{
  	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	extract($_REQUEST);  
?>
     
<script>
	function js_set_value(mrr)
	{
 		$("#hidden_recv_number").val(mrr); // mrr number
		parent.emailwindow.hide();
	}
</script>

</head>

<body>
<div align="center" style="width:100%;" >
<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
	<table width="880" cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
            <thead>
                <tr>                	 
                    <th width="150">Supplier</th>
                    <th width="150">Search By</th>
                    <th width="250" align="center" id="search_by_td_up">Enter WO/PI Number</th>
                    <th width="200">Date Range</th>
                    <th><input type="reset" name="re_button" id="re_button" value="Reset" style="width:100px" class="formbutton"  /></th>           
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <?php  
 							// echo create_drop_down( "cbo_supplier", 150, "select id,supplier_name from lib_supplier where FIND_IN_SET($company,tag_company) and FIND_IN_SET(3,party_type) order by supplier_name","id,supplier_name", 1, "-- Select --", 0, "",0 );
							echo create_drop_down( "cbo_supplier", 170, "select a.id,a.supplier_name from lib_supplier a,  lib_supplier_tag_company b,lib_supplier_party_type c where a.id=c.supplier_id and a.id=b.supplier_id and b.tag_company='$company' and c.party_type='3' order by a.supplier_name","id,supplier_name", 1, "-- Select --", 0, "",0 ); 
                        ?>
                    </td>
                    <td>
                        <?php  
                            $search_by = array(1=>'MRR No',2=>'Challan No');
							$dd="change_search_event(this.value, '0*0', '0*0', '../../') ";
							echo create_drop_down( "cbo_search_by", 120, $search_by,"",0, "--Select--", "",$dd,0 );
                        ?>
                    </td>
                    <td width="" align="center" id="search_by_td">				
                        <input type="text" style="width:230px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />	
                    </td>    
                    <td align="center">
                        <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px" placeholder="From Date" />
                        <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px" placeholder="To Date" />
                    </td> 
                    <td align="center">
                        <input type="button" name="btn_show" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_supplier').value+'_'+document.getElementById('cbo_search_by').value+'_'+document.getElementById('txt_search_common').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+<?php echo $company; ?>, 'create_mrr_search_list_view', 'search_div', 'chemical_dyes_receive_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:100px;" />				
                    </td>
            </tr>
        	<tr>                  
            	<td align="center" height="40" valign="middle" colspan="5">
					<?php echo load_month_buttons(1);  ?>
                    <!-- Hidden field here-------->
                     <input type="hidden" id="hidden_recv_number" value="hidden_recv_number" />
                    <!-- ---------END------------->
                </td>
            </tr>    
            </tbody>
         </tr>         
        </table>    
        <div align="center" valign="top" id="search_div"> </div> 
        </form>
   </div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}

if($action=="create_mrr_search_list_view")
{
	$ex_data = explode("_",$data);
	$supplier = $ex_data[0];
	$txt_search_by = $ex_data[1];
	$txt_search_common = $ex_data[2];
	$fromDate = $ex_data[3];
	$toDate = $ex_data[4];
	$company = $ex_data[5];
	
	 
	$sql_cond="";
	if(trim($txt_search_common)!="")
	{
		if(trim($txt_search_by)==1) // for mrr
		{
			$sql_cond .= " and a.recv_number LIKE '%$txt_search_common%'";	
			
		}
		else if(trim($txt_search_by)==2) // for chllan no
		{
			$sql_cond .= " and a.challan_no LIKE '%$txt_search_common%'";				
 		}		 
		 
 	} 
	
	if(trim($company)!="") $sql_cond .= " and a.company_id='$company'";
	if(trim($supplier)!=0) $sql_cond .= " and a.supplier_id='$supplier'";
	if($db_type==0)
	{
		if( $fromDate!="" || $toDate!="" ) $sql_cond .= " and a.receive_date  between '".change_date_format($fromDate,'yyyy-mm-dd')."' and '".change_date_format($toDate,'yyyy-mm-dd')."'";
		$sql = "select a.recv_number,a.supplier_id,a.challan_no,c.lc_number,a.receive_date,a.receive_basis,sum(b.cons_quantity) as receive_qnty from inv_transaction b, inv_receive_master a left join com_btb_lc_master_details c on a.lc_no=c.id where a.id=b.mst_id and a.entry_form=4 and a.status_active=1 and b.transaction_type=1 $sql_cond group by b.mst_id";
	}
	if($db_type==1 || $db_type==2)
	{
		if( $fromDate!="" || $toDate!="" ) $sql_cond .= " and a.receive_date  between '".change_date_format($fromDate, "yyyy-mm-dd", "-",1)."' and '".change_date_format($toDate, "yyyy-mm-dd", "-",1)."'";
		$sql = "select a.recv_number,a.supplier_id,a.challan_no,c.lc_number,a.receive_date,a.receive_basis,sum(b.cons_quantity) as receive_qnty from inv_transaction b, inv_receive_master a left join com_btb_lc_master_details c on a.lc_no=c.id where a.id=b.mst_id and a.entry_form=4 and a.status_active=1  and b.transaction_type=1 $sql_cond group by a.recv_number,a.supplier_id,a.challan_no,c.lc_number,a.receive_date,a.receive_basis order by a.recv_number ";
	}
	$supplier_arr = return_library_array( "select id, supplier_name from lib_supplier",'id','supplier_name');
	$arr=array(1=>$supplier_arr,5=>$receive_basis_arr);
	echo create_list_view("list_view", "MRR No, Supplier Name, Challan No, LC No, Receive Date, Receive Basis, Receive Qnty","120,120,120,120,120,100,100","900","260",0, $sql , "js_set_value", "recv_number", "", 1, "0,supplier_id,0,0,0,receive_basis,0", $arr, "recv_number,supplier_id,challan_no,lc_number,receive_date,receive_basis,receive_qnty", "",'','0,0,0,0,0,0,0') ;	
	exit();
}

if($action=="populate_data_from_data")
{
	$sql = "select id, recv_number, company_id, receive_basis, booking_id, booking_no,receive_purpose,loan_party,gate_entry_no, location_id, receive_date, challan_no, store_id, lc_no, supplier_id, exchange_rate, currency_id, lc_no,source from inv_receive_master where recv_number='$data' and entry_form=4";
	$res = sql_select($sql);
	foreach($res as $row)
	{
		echo "fn_independent(".$row[csf("receive_basis")].");\n";
		echo "$('#txt_mrr_no').val('".$row[csf("recv_number")]."');\n";
		echo "$('#update_id').val(".$row[csf("id")].");\n";
		echo "$('#cbo_company_name').val(".$row[csf("company_id")].");\n";
		echo "$('#cbo_receive_basis').val(".$row[csf("receive_basis")].");\n";
		echo "$('#txt_wo_pi').val('".$row[csf("booking_no")]."');\n";
		echo "$('#txt_wo_pi_id').val(".$row[csf("booking_id")].");\n";
		echo "$('#cbo_receive_purpose').val(".$row[csf("receive_purpose")].");\n";
		echo "$('#cbo_loan_party').val(".$row[csf("loan_party")].");\n";
		echo "$('#txt_gate_entry').val('".$row[csf("gate_entry_no")]."');\n";

		echo "$('#cbo_location_name').val(".$row[csf("location_id")].");\n";
		echo "$('#txt_receive_date').val('".change_date_format($row[csf("receive_date")])."');\n";
		echo "$('#txt_challan_no').val('".$row[csf("challan_no")]."');\n";
		echo "$('#cbo_store_name').val(".$row[csf("store_id")].");\n";
		echo "$('#cbo_supplier').val(".$row[csf("supplier_id")].");\n";
		echo "$('#cbo_currency').val(".$row[csf("currency_id")].");\n";
		echo "$('#txt_exchange_rate').val(".$row[csf("exchange_rate")].");\n";
		echo "$('#cbo_source').val(".$row[csf("source")].");\n";
		echo "$('#hidden_lc_id').val(".$row[csf("lc_no")].");\n";
		$lcNumber = return_field_value("lc_number","com_btb_lc_master_details","id=".$row[csf("lc_no")]."");
		echo "$('#cbo_currency').attr('disabled',false);\n";
		echo "$('#txt_exchange_rate').attr('disabled',false);\n";
		echo "show_list_view('".$row[csf("recv_number")]."**".$row[csf("id")]."','show_dtls_list_view','list_container_yarn','requires/chemical_dyes_receive_controller','');\n";
		echo "$('#cbo_currency').val(".$row[csf("currency_id")].");\n";
 	}
	exit();	
}

if($action=="show_dtls_list_view")
{
	$ex_data = explode("**",$data);
	$recv_number = $ex_data[0];
	$rcv_mst_id = $ex_data[1];
	$cond="";
	if($db_type==0) $prod_name="concat(c.sub_group_name,' ',c.item_description,' ',c.item_size )";
	if($db_type==2) $prod_name="(c.sub_group_name||c.item_description||c.item_size )";
	if($recv_number!="") $cond .= " and a.recv_number='$recv_number'";
	if($rcv_mst_id!="") $cond .= " and a.id='$rcv_mst_id'";
	 $sql = "select a.recv_number, b.id, b.receive_basis, b.pi_wo_batch_no,$prod_name as product_name_details, c.lot, b.order_uom, b.order_qnty, b.order_rate, b.order_ile_cost, b.order_amount,    b.cons_amount,b.batch_lot from inv_receive_master a, inv_transaction b,  product_details_master c where a.id=b.mst_id and b.prod_id=c.id and b.transaction_type=1 and b.item_category in(5,6,7) and a.entry_form=4 $cond";
	
	$result = sql_select($sql);
	$i=1;
	$totalQnty=0;
	$totalAmount=0;
	$totalbookCurr=0;
	?>
    	<table class="rpt_table" border="1" cellpadding="2" cellspacing="0" style="width:1000px" >
        	<thead>
            	<tr>
                	<th>SL</th>
                    <th>WO/PI No</th>
                    <th>MRR No</th>
                    <th>Product Details</th>
                    <th>Batch/Lot</th>
                    <th>UOM</th>
                    <th>Receive Qty</th>
                    <th>Rate</th>
                    <th>ILE Cost</th>
                    <th>Amount</th>
                    <th>Book Currency</th>
                </tr>
            </thead>
            <tbody>
            	<?php 
				    foreach($result as $row)
					{  
					if ($i%2==0)$bgcolor="#E9F3FF";						
					else $bgcolor="#FFFFFF";
					$wopi="";
					if($row[csf("receive_basis")]==1)
						$wopi=return_field_value("pi_number","com_pi_master_details","id=".$row[csf("pi_wo_batch_no")]."");	
					else if($row[csf("receive_basis")]==2)
					$wopi=return_field_value("booking_no","inv_receive_master","id=$rcv_mst_id");	
						//$wopi=return_field_value("booking_no","wo_booking_mst","id=".$row[csf("pi_wo_batch_no")]."");	
					$totalQnty +=$row[csf("order_qnty")];
					$totalAmount +=$row[csf("order_amount")];
					$totalbookCurr +=$row[csf("cons_amount")];	
 				?>
                	<tr bgcolor="<?php echo $bgcolor; ?>" onClick='get_php_form_data("<?php echo $row[csf("id")];?>","child_form_input_data","requires/chemical_dyes_receive_controller")' style="cursor:pointer" >
                        <td width="30"><?php echo $i; ?></td>
                        <td width="100"><p><?php echo $wopi; ?></p></td>
                        <td width="100"><p><?php echo $row[csf("recv_number")]; ?></p></td>
                        <td width="200"><p><?php echo $row[csf("product_name_details")]; ?></p></td>
                        <td width="80"><p><?php echo $row[csf("batch_lot")]; ?></p></td>
                        <td width="70"><p><?php echo $unit_of_measurement[$row[csf("order_uom")]]; ?></p></td>
                        <td width="80" align="right"><p><?php echo $row[csf("order_qnty")]; ?></p></td>
                        <td width="60" align="right"><p><?php echo $row[csf("order_rate")]; ?></p></td>
                        <td width="70" align="right"><p><?php echo $row[csf("order_ile_cost")]; ?></p></td>                       
                        <td width="70" align="right"><p><?php echo $row[csf("order_amount")]; ?></p></td>
                        <td width="80" align="right"><p><?php echo $row[csf("cons_amount")]; ?></p></td>
                   </tr>
                   <?php $i++; } ?>
                	<tfoot>
                        <th colspan="6">Total</th>                         
                        <th><?php echo $totalQnty; ?></th>
                        <th colspan="2"></th>
                        <th><?php echo $totalAmount; ?></th>
                        <th><?php echo $totalbookCurr; ?></th>
                        <th></th>
                  </tfoot>
            </tbody>
        </table>
    <?php
	exit();
}

if($action=="child_form_input_data")
{
	$rcv_dtls_id = $data;	
	$color_library=return_library_array( "select id,color_name from lib_color",'id','color_name');
	$woben_fabric_type_library = return_library_array( "select id,fabric_type from lib_woben_fabric_type",'id','fabric_type');
	
	$sql = "select a.company_id, a.currency_id, a.exchange_rate, a.booking_without_order, a.booking_no, b.id, b.receive_basis, b.pi_wo_batch_no, b.prod_id, b. 	item_category,b.batch_lot, c.item_group_id,c.item_description, b.order_uom, b.order_qnty, b.order_rate,b.order_ile_cost,b.order_amount,b.cons_amount,b.expire_date,b.room,b.rack,b.self,b.bin_box from inv_receive_master a, inv_transaction b, product_details_master c where a.id=b.mst_id and b.prod_id=c.id and b.id='$rcv_dtls_id'";
	$result = sql_select($sql);
	foreach($result as $row)
	{
		echo "$('#cbo_item_category_id').val('".$row[csf("item_category")]."');\n";
		echo "load_drop_down( 'requires/chemical_dyes_receive_controller',".$row[csf('item_category')].", 'load_drop_down_item_group', 'item_group_td' )\n";
		echo "$('#cbo_item_group_id').val('".$row[csf("item_group_id")]."');\n";
		echo "$('#txt_description').val('".$row[csf("item_description")]."');\n";
		echo "$('#txt_product_id').val('".$row[csf("prod_id")]."');\n";
		echo "$('#txt_lot').val('".$row[csf("batch_lot")]."');\n";
		echo "$('#cbo_uom').val(".$row[csf("order_uom")].");\n";
		echo "$('#txt_receive_qty').val(".$row[csf("order_qnty")].");\n";
		echo "$('#txt_rate').val(".$row[csf("order_rate")].");\n";
		echo "$('#txt_ile').val(".$row[csf("order_ile_cost")].");\n";
		echo "$('#txt_amount').val(".$row[csf("order_amount")].");\n";
		echo "$('#txt_book_currency').val(".$row[csf("cons_amount")].");\n";
		//echo "$('#txt_order_qty').val(0);\n";
		if($row[csf("expire_date")]!="") echo "$('#txt_expire_date').val('".change_date_format($row[csf("expire_date")])."');\n";
		echo "$('#txt_room').val(".$row[csf("room")].");\n";
		echo "$('#txt_rack').val(".$row[csf("rack")].");\n";
		echo "$('#txt_self').val(".$row[csf("self")].");\n";
		echo "$('#txt_binbox').val(".$row[csf("bin_box")].");\n";
		echo "$('#update_dtls_id').val(".$row[csf("id")].");\n";
		if($row[csf("receive_basis")]==1 || $row[csf("receive_basis")] ==2)
		{
			echo "show_list_view(".$row[csf("receive_basis")]."+'**'+".$row[csf("pi_wo_batch_no")]."+'**'+".$row[csf("booking_without_order")]."+'**'+'".$row[csf("booking_no")]."','show_product_listview','list_product_container','requires/chemical_dyes_receive_controller','');\n";
		}
		echo "set_button_status(1, permission, 'fnc_chemical_dyes_receive_entry',1,1);\n";
		
		//echo "disable_enable_fields('cbo_receive_basis*cbo_receive_purpose*txt_receive_date*txt_challan_no*cbo_store_name*txt_exchange_rate', 0, '', '');\n";
		echo "fn_calile();\n";
	}
	exit();
}
//################################################# function Here #########################################//
//function for domestic rate find--------------//
//parameters rate,ile cost,exchange rate,conversion factor
function return_domestic_rate($rate,$ile_cost,$exchange_rate,$conversion_factor){
	$rate_ile=$rate+$ile_cost;
	$rate_ile_exchange=$rate_ile*$exchange_rate;
	$doemstic_rate=$rate_ile_exchange/$conversion_factor;
	return $doemstic_rate;	
}

if ($action=="ItemDescription_popup")
{
	echo load_html_head_contents("Item Description Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
	if($cbo_company_name !=0)
		$CompanyCond="and company_id=$cbo_company_name";
	else
		$CompanyCond="";
	if($cbo_item_category_id !=0)
		$item_category_id="item_category_id=$cbo_item_category_id";
	else
		$item_category_id="item_category_id in(5,6,7)";
	if($cbo_item_group_id !=0)
		$item_group_id="and item_group_id=$cbo_item_group_id";
	else
		$item_group_id="";
?> 

	<script>
		
		$(document).ready(function(e) {
            setFilterGrid('tbl_list_search',-1);
        });
		
		function js_set_value(id)
		{
			$('#hidden_prod_id').val(id);
			parent.emailwindow.hide();
		}
    </script>
</head>
<body>
	<form name="searchdescfrm"  id="searchdescfrm">
		<fieldset style="width:720px;margin-left:10px">
			<?php
			    $company_arr = return_library_array("select id,company_name from lib_company","id","company_name");
				$item_group_arr = return_library_array( "select id,item_name from lib_item_group",'id','item_name');
			
                $data_array=sql_select("select id,company_id,supplier_id,item_category_id,item_group_id, product_name_details,current_stock,avg_rate_per_unit,unit_of_measure from product_details_master where $item_category_id $CompanyCond $item_group_id and status_active=1 and is_deleted=0");
            ?>
            <input type="hidden" name="hidden_prod_id" id="hidden_prod_id" class="text_boxes" value="">
            <div style="margin-left:10px; margin-top:10px">
                <table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="880">
                    <thead>
                        <th width="50">SL</th>
                        <th width="100">Company</th>
                        <th width="100">Item Categ</th>
                        <th width="100">Item Group</th>
                        <th width="150">Product Name</th>
                        <th width="100">Qnty</th>
                        <th width="">UOM</th>
                       <!-- <th>Rate</th>-->
                    </thead>
                </table>
                <div style="width:900px; max-height:280px; overflow-y:scroll" id="list_container" align="left"> 
                    <table class="rpt_table" border="1" cellpadding="0" cellspacing="0" rules="all" width="880" id="tbl_list_search">  
                        <?php 
                        $i=1;
                        foreach($data_array as $row)
                        {  
                            if ($i%2==0)  
                                $bgcolor="#E9F3FF";
                            else
                                $bgcolor="#FFFFFF";
                         ?>
                            <tr bgcolor="<?php echo $bgcolor; ?>" onClick="js_set_value(<?php echo $row[csf('id')]; ?>)" style="cursor:pointer" >
                                <td width="50"><?php echo $i; ?></td>
                                <td width="100"><?php echo  $company_arr[$row[csf('company_id')]]; ?></td>
                                <td width="100"><?php echo $item_category[$row[csf('item_category_id')]]; ?></td>
                                <td width="100"><?php echo $item_group_arr[$row[csf('item_group_id')]]; ?></td>
                                <td width="150"><p><?php echo $row[csf('product_name_details')]; ?></p></td>
                                <td width="100"><?php echo $row[csf('current_stock')]; ?></td>
                                <td width=""><?php echo $unit_of_measurement[$row[csf('unit_of_measure')]]; ?></td>
                                <?php //echo $row[csf('avg_rate_per_unit')]; ?>
                            </tr>
                        <?php 
                        $i++; 
                        } 
                        ?>
                    </table>
                </div> 
            </div>
		</fieldset>
	</form>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
exit();
}

if($action=="chemical_dyes_receive_print")
{
    extract($_REQUEST);
	$data=explode('*',$data);
	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	
	$sql=" select id, recv_number, receive_basis, receive_purpose, booking_id, loan_party, gate_entry_no, receive_date, challan_no, location_id, store_id, supplier_id, lc_no, currency_id, exchange_rate, source from inv_receive_master where id='$data[1]'";
	//echo $sql;die;
	$dataArray=sql_select($sql);
	
	$company_library=return_library_array( "select id, company_name from lib_company", "id", "company_name"  );
	$supplier_library=return_library_array( "select id,supplier_name from  lib_supplier", "id","supplier_name"  );
	$store_library=return_library_array( "select id, store_name from  lib_store_location", "id", "store_name"  );
	$pi_library=return_library_array( "select id,pi_number from  com_pi_master_details", "id","pi_number"  );
	$wo_library=return_library_array( "select id,wo_number from  wo_non_order_info_mst", "id","wo_number"  );
	$country_arr=return_library_array( "select id, country_name from  lib_country", "id", "country_name"  );

?>
	<div style="width:930px;">
    <table width="900" cellspacing="0" align="right">
        <tr>
            <td colspan="6" align="center" style="font-size:xx-large"><strong><?php echo $company_library[$data[0]]; ?></strong></td>
        </tr>
        <tr class="">
        	<td colspan="6" align="center" style="font-size:14px">  
				<?php
					$nameArray=sql_select( "select plot_no,level_no,road_no,block_no,country_id,province,city,zip_code,email,website from lib_company where id=$data[0]"); 
					foreach ($nameArray as $result)
					{ 
					?>
						Plot No: <?php echo $result['plot_no']; ?> 
						Level No: <?php echo $result['level_no']?>
						Road No: <?php echo $result['road_no']; ?> 
						Block No: <?php echo $result['block_no'];?> 
						City No: <?php echo $result['city'];?> 
						Zip Code: <?php echo $result['zip_code']; ?> 
						Province No: <?php echo $result['province'];?> 
						Country: <?php echo $country_arr[$result['country_id']]; ?><br> 
						Email Address: <?php echo $result['email'];?> 
						Website No: <?php echo $result['website'];
					}
                ?> 
            </td>  
        </tr>
        <tr>
            <td colspan="6" align="center" style="font-size:x-large"><strong><u>Dyes & Chemical Receive Reprot</u></strong></td>
        </tr>
        <tr>
        	<td width="120"><strong>MRIR Number:</strong></td><td width="175px"><?php echo $dataArray[0][csf('recv_number')]; ?></td>
            <td width="130"><strong>Receive Basis :</strong></td> <td width="175px"><?php echo $receive_basis_arr[$dataArray[0][csf('receive_basis')]]; ?></td>
            <td width="125"><strong>Receive Date:</strong></td><td width="175px"><?php echo change_date_format($dataArray[0][csf('receive_date')]); ?></td>
        </tr>
        <tr>
            <td><strong>Challan No:</strong></td> <td width="175px"><?php echo $dataArray[0][csf('challan_no')]; ?></td>
            <td><strong>L/C No:</strong></td><td width="175px"><?php echo $dataArray[0][csf('lc_no')]; ?></td>
            <td><strong>Store Name:</strong></td><td width="175px"><?php echo $store_library[$dataArray[0][csf('store_id')]]; ?></td>
        </tr>
        <tr>
            <td><strong>Supplier:</strong></td> <td width="175px"><?php echo $supplier_library[$dataArray[0][csf('supplier_id')]]; ?></td>
            <td><strong>Currency:</strong></td><td width="175px"><?php echo $currency[$dataArray[0][csf('currency_id')]]; ?></td>
            <td><strong>Exchange Rate:</strong></td><td width="175px"><?php echo $dataArray[0][csf('exchange_rate')]; ?></td>
        </tr>
        <tr>
            <td><strong>Gate Entry No:</strong></td> <td width="175px"><?php echo $dataArray[0][csf('gate_entry_no')]; ?></td>
            <td><strong>Source:</strong></td><td width="175px"><?php echo $source[$dataArray[0][csf('source')]]; ?></td>
            <td><strong>WO/PI:</strong></td> <td width="175px"><?php if ($dataArray[0][csf('receive_basis')]==1) echo $pi_library[$dataArray[0][csf('booking_id')]]; else echo $wo_library[$dataArray[0][csf('booking_id')]]; ?></td>
        </tr>
          <tr>
            <td><strong>Bar Code:</strong></td> <td  colspan="4" id="barcode_img_id"></td>
           
        </tr>
    </table>
    <?php
	  $sql_dtls = "select b.item_category,
	  b.id, b.receive_basis, b.pi_wo_batch_no, b.order_uom, b.order_qnty, b.order_rate, b.order_amount, b.cons_amount, b.balance_qnty, b.expire_date, b.batch_lot,
	  (c.sub_group_name||' '||c.item_description||' '||c.item_size) as product_name_details, c.item_group_id, c.item_description
	  from inv_receive_master a, inv_transaction b, product_details_master c where a.company_id=$data[0] and a.id=$data[1] and a.id=b.mst_id and b.prod_id=c.id and b.transaction_type=1 and b.item_category in(5,6,7) and a.entry_form=4";
	
	  $sql_result= sql_select($sql_dtls);
	 $i=1;
	 $item_name_arr=return_library_array( "select id, item_name from  lib_item_group", "id", "item_name"  );
	  ?>
         <br>
<div style="width:100%;">
    <table align="right" cellspacing="0" width="900"  border="1" rules="all" class="rpt_table" >
        <thead bgcolor="#dddddd" align="center">
            <th width="30">SL</th>
            <th width="80" align="center">Item Category</th>
            <th width="130" align="center">Item Group</th>
            <th width="180" align="center">Item Description</th>
            <th width="50" align="center">Lot</th> 
            <th width="50" align="center">UOM</th> 
            <th width="80" align="center">Recv. Qnty.</th>
            <th width="50" align="center">Rate</th>
            <th width="70" align="center">Amount</th>
            <?php
			if($data[3]!=4)
			{
			?>
            <th width="80" align="center">PI/WO Qnty Bal.</th> 
            <?php
			}
			?>
            <th width="80" align="center">Warranty Exp. Date</th>                  
        </thead>
      <?php
	foreach($sql_result as $row)
	{
		if ($i%2==0)  
			$bgcolor="#E9F3FF";
		else
			$bgcolor="#FFFFFF";
			$order_qnty=$row[csf('order_qnty')];
			$order_qnty_sum += $order_qnty;
			
			$order_amount=$row[csf('order_amount')];
			$order_amount_sum += $order_amount;
			
			$balance_qnty=$row[csf('balance_qnty')];
			$balance_qnty_sum += $balance_qnty;
			
			$desc=$row[csf('item_description')];
			
			if($row[csf('item_size')]!="")
			{
				$desc.=", ".$row[csf('item_size')];
			}
		?>
			<tr bgcolor="<?php echo $bgcolor; ?>">
                <td><?php echo $i; ?></td>
                <td><?php echo $item_category[$row[csf('item_category')]]; ?></td>
                <td><?php echo $item_name_arr[$row[csf('item_group_id')]]; ?></td>
                <td><?php echo $row[csf('product_name_details')]; ?></td>
                <td><?php echo $row[csf('batch_lot')]; ?></td>
                <td align="center"><?php echo $unit_of_measurement[$row[csf('order_uom')]]; ?></td>
                <td align="right"><?php echo $row[csf('order_qnty')]; ?></td>
                <td align="right"><?php echo $row[csf('order_rate')]; ?></td>
                <td align="right"><?php echo number_format($row[csf('order_amount')],0,'',',');  ?></td>
                <?php
				if($data[3]!=4)
		        	{
			     ?>
                      <td align="right"><?php echo $row[csf('balance_qnty')]; ?></td>
                  <?php
		        	}
			
			     ?>
                <td><?php if($row[csf('expire_date')]!="") echo change_date_format($row[csf('expire_date')]); ?></td>
			</tr>
			<?php
			$i++;
			}
			?>
        	<tr> 
                <td align="right" colspan="6" >Total</td>
                <td align="right"><?php echo number_format($order_qnty_sum,2,'.',','); ?></td>
                <td align="right" colspan="2" ><?php echo number_format($order_amount_sum,0,'',','); ?></td>
                <?php
				if($data[3]!=4)
		        	{
			     ?>
                <td align="right" ><?php echo number_format($balance_qnty_sum,0,'',','); ?></td>
                 <?php
		        	}
			
			     ?>
                <td align="right">&nbsp;</td>
			</tr>
		</table>
        <br>
		 <?php
            echo signature_table(8, $data[0], "900px");
         ?>
      </div>
   </div> 
   
     <script type="text/javascript" src="../../../js/jquerybarcode.js"></script>
     <script>

	function generateBarcode( valuess ){
		   
			var value = valuess;//$("#barcodeValue").val();
		 // alert(value)
			var btype = 'code39';//$("input[name=btype]:checked").val();
			var renderer ='bmp';// $("input[name=renderer]:checked").val();
			 
			var settings = {
			  output:renderer,
			  bgColor: '#FFFFFF',
			  color: '#000000',
			  barWidth: 1,
			  barHeight: 30,
			  moduleSize:5,
			  posX: 10,
			  posY: 20,
			  addQuietZone: 1
			};
			//$("#barcode_img_id").html('11');
			 value = {code:value, rect: false};
			
			$("#barcode_img_id").show().barcode(value, btype, settings);
		  
		} 
  
	 generateBarcode('<?php echo $dataArray[0][csf('recv_number')]; ?>');
	 
	 
	 </script>
   
   
   
   
 <?php
 exit(); 
}
?>
