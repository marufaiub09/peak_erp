<?
/*-------------------------------------------- Comments 

Purpose			: 	This form will create Yarn Dyeing Charge Booking
					
Functionality	:	

JS Functions	:

Created by		:	Jahid
Creation date 	: 	04-03-2013
Updated by 		: 		
Update date		: 		   

QC Performed BY	:		

QC Date			:	
Comments		:
*/

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
require_once('../../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;
//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Woven Service Booking", "../../", 1, 1,$unicode,'','');
?>	
<script>

if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../../logout.php"; 
var permission='<? echo $permission; ?>';


function openmypage_job(title)
{
	if (form_validation('cbo_company_name','Company Name')==false)
	{
		return;
	}	
	else
	{
	
		var company = $("#cbo_company_name").val();
		//alert(company);
		page_link='requires/yarn_dyeing_charge_booking_controller.php?action=order_search_popup&company='+company;
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=600px,height=370px,center=1,resize=1,scrolling=0','../')
		emailwindow.onclose=function()
		{
			var theform=this.contentDoc.forms[0];;
			var data=this.contentDoc.getElementById("hidden_tbl_id").value.split("_");
			//alert(data[0]);
			freeze_window(5);
			document.getElementById('txt_job_id').value=data[0];
			document.getElementById('txt_job_no').value=data[1];
			
			release_freezing();
	
		}
	}
}




function fnc_yarn_dyeing(operation)
{
	
	if( form_validation('cbo_company_name*cbo_supplier_name*txt_job_no','Company Name *Supplier Name*Job Number')==false )
	{
		return;
	}
	else
	{
	var dataString = "cbo_company_name*cbo_supplier_name*txt_booking_date*txt_delivery_date*cbo_currency*txt_exchange_rate*cbo_pay_mode*txt_attention*txt_job_no*cbo_count*txt_item_des*txt_yern_color*cbo_color_range*cbo_uom*txt_wo_qty*txt_dyeing_charge*txt_amount*txt_bag*txt_cone*update_id*txt_booking_no*txt_job_id*dtls_update_id*txt_pro_id*txt_min_req_cone*cbo_source";
 	var data="action=save_update_delete&operation="+operation+get_submitted_data_string(dataString,"../../");//alert(data);
	freeze_window(operation);
	http.open("POST","requires/yarn_dyeing_charge_booking_controller.php",true);
	http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	http.send(data);
	http.onreadystatechange = fnc_yarn_dyeing_response;
	}
	
}

function fnc_yarn_dyeing_response()
{
	if(http.readyState == 4) 
	{
		var response=trim(http.responseText).split('**');
		release_freezing();
		//alert (response);
		
		if(response[0]==0)
		{
			show_msg(trim(response[0]));
		}
		else if(response[0]==1)
		{
			show_msg(trim(response[0]));
		}
		else if(response[0]==10)
		{
			show_msg(trim(response[0]));
			return;
		}
		$("#update_id").val(response[2]);
		$("#txt_booking_no").val(response[1]);
		show_list_view(response[2],'show_dtls_list_view','list_container','requires/yarn_dyeing_charge_booking_controller','');
		set_button_status(0, permission, 'fnc_yarn_dyeing',1,1);
		reset_form('','','txt_yern_color*cbo_color_range*txt_wo_qty*txt_dyeing_charge*txt_amount*txt_bag*txt_cone','','','');
 	}
}


function fnc_calculate()
{
	var wo_qty=$('#txt_wo_qty').val();
	var dyeing_charge=$('#txt_dyeing_charge').val();
	//alert(dyeing_charge);
	var amount=(wo_qty*1)*(dyeing_charge*1);
	$('#txt_amount').val(number_format_common( amount, 2));
}





function chack_variable(id)
{
	//alert(id);
	var data="action=variable_chack&company="+id;
	http.open("POST","requires/yarn_dyeing_charge_booking_controller.php",true);
	http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	http.send(data);
	http.onreadystatechange =  fnc_variable_response;
}

function fnc_variable_response()
{
	if(http.readyState == 4) 
	{
		
		var response=trim(http.responseText).split('**');
		if(response==1)
		{
			$('#dyeing_charge_td').html('<input type="text" id="txt_dyeing_charge" name="txt_dyeing_charge" style="width:85px;" class="text_boxes" placeholder="Browse" onDblClick="openmypage_charge()" readonly />');
		}
		else
		{
			$('#dyeing_charge_td').html('<input type="text" id="txt_dyeing_charge" name="txt_dyeing_charge" style="width:85px;" class="text_boxes"  onKeyUp="fnc_calculate()"/>');
		}

 	}
}


function openmypage_booking()
{
	if( form_validation('cbo_company_name','Company Name')==false )
	{
		return;
	}
	var company = $("#cbo_company_name").val();
	page_link='requires/yarn_dyeing_charge_booking_controller.php?action=yern_dyeing_booking_popup&company='+company;
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe',page_link,'Yarn Dyeing Booking Search', 'width=600px, height=400px, center=1, resize=0, scrolling=0','../');
	emailwindow.onclose=function()
	{
		var theform=this.contentDoc.forms[0];
		var mst_id=this.contentDoc.getElementById("hidden_sys_number").value.split("_"); // system number
		//alert(boking_date);
		if(mst_id!="")
		{
			freeze_window(5);
			$("#txt_booking_no").val(mst_id[1]);
			$("#cbo_company_name").val(mst_id[2]);
			$("#cbo_supplier_name").val(mst_id[3]);
			$("#txt_booking_date").val(change_date_format(mst_id[4]));
			$("#txt_delivery_date").val(change_date_format(mst_id[5]));
			$("#cbo_currency").val(mst_id[6]);
			$("#txt_exchange_rate").val(mst_id[7]);
			$("#cbo_pay_mode").val(mst_id[8]);
			$("#txt_attention").val(mst_id[9]);
			$("#cbo_source").val(mst_id[10]);
			$("#update_id").val(mst_id[0]);
			//reset_form('','','txt_item_description*txt_quantity*cbo_uom*txt_rate*txt_amount','','','');
			//get_php_form_data(sysNumber, "populate_master_from_data", "requires/get_out_entry_controller" );
			show_list_view(mst_id[0],'show_dtls_list_view','list_container','requires/yarn_dyeing_charge_booking_controller','');
			set_button_status(0, permission, 'fnc_yarn_dyeing',1,1);
			release_freezing();
		}
	}
}




function openmypage_charge()
{
	if (form_validation('cbo_company_name*txt_job_no','Company Name*Job Number')==false)
	{
		return;
	}	
	else
	{
	
		var company = $("#cbo_company_name").val();
		//alert(company);
		page_link='requires/yarn_dyeing_charge_booking_controller.php?action=dyeing_search_popup&company='+company;
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, 'Dyeing Charge', 'width=600px,height=370px,center=1,resize=1,scrolling=0','../')
		emailwindow.onclose=function()
		{
			var theform=this.contentDoc.forms[0];;
			var data=this.contentDoc.getElementById("hidden_rate").value;
			//alert(data);
			freeze_window(5);
			document.getElementById('txt_dyeing_charge').value=data;
			release_freezing();
			fnc_calculate();

		}
	}
}


function openmypage_lot()
{
	if (form_validation('cbo_company_name','Company Number')==false)
	{
		return;
	}	
	else
	{
	
		var company = $("#cbo_company_name").val();
		var job_no = $("#txt_job_no").val();
		//alert(job_no);
		page_link='requires/yarn_dyeing_charge_booking_controller.php?action=lot_search_popup&company='+company+'&job_no='+job_no;
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, 'Lot Number Search', 'width=600px,height=300px,center=1,resize=1,scrolling=0','../')
		emailwindow.onclose=function()
		{
			var theform=this.contentDoc.forms[0];;
			var data=this.contentDoc.getElementById("hidden_product").value.split(",");
			//alert(data[1]);
			freeze_window(5);
			//document.getElementById('txt_item_des').value=data[0];
			//document.getElementById('cbo_count').value=data[1];
			//document.getElementById('txt_lot').value=data[2];
			//document.getElementById('txt_pro_id').value=data[3];
			$('#txt_item_des').val(data[0]).attr('disabled',true);
			$('#cbo_count').val(0).attr('disabled',true);
			$('#txt_lot').val(data[2]);
			$('#txt_pro_id').val(data[3]);
			release_freezing();

		}
	}
}

function open_terms_condition_popup(page_link,title)
{
	var txt_booking_no=document.getElementById('txt_booking_no').value;
	if (txt_booking_no=="")
	{
		alert("Save The Booking First")
		return;
	}	
	else
	{
	    page_link=page_link+get_submitted_data_string('txt_booking_no','../../');
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=720px,height=470px,center=1,resize=1,scrolling=0','../')
		emailwindow.onclose=function()
		{
		}
	}
}


function generate_trim_report()
{
if (form_validation('txt_booking_no','Booking No')==false)
	{
		return;
	}
	else
	{
		var data="action=show_trim_booking_report"+get_submitted_data_string('txt_booking_no*cbo_company_name*update_id',"../../");
		http.open("POST","requires/yarn_dyeing_charge_booking_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = generate_trim_report_reponse;
	}	
}

function generate_trim_report_reponse()
{
	if(http.readyState == 4) 
	{
		//alert( http.responseText);return;
		$('#data_panel').html( http.responseText );
		var w = window.open("Surprise", "#");
		var d = w.document.open();
		d.write('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN""http://www.w3.org/TR/html4/strict.dtd">'+
'<html><head><link rel="stylesheet" href="../../../css/style_common.css" type="text/css" /><title></title></head><body>'+document.getElementById('data_panel').innerHTML+'</body</html>');
		d.close();
	}
}
function fnResetForm()
{
	reset_form('yarn_dyeing_wo_booking','list_container','','','disable_enable_fields("txt_item_des*txt_lot*cbo_count",0)','cbo_uom');
	set_button_status(0, permission, 'fnc_yarn_dyeing',1,0);
}

</script>
 
</head>
<body onLoad="set_hotkey()">
<div style="width:100%;" align="center">
     <? echo load_freeze_divs ("../../",$permission);  ?>
    <form name="yarn_dyeing_wo_booking"  autocomplete="off" id="yarn_dyeing_wo_booking">
        <fieldset style="width:1120px;">
        <legend>Yarn Dyeing Wo</legend>
        <table  width="1100" cellspacing="2" cellpadding="0" border="0">
            <tr>
                    <td align="" width="130"></td>
                    <td align="" width="170"></td>
                    <td  width="130" height="" align="right" class="must_entry_caption"> Yarn Dyeing Wo No </td>              <!-- 11-00030  -->
                    <td  width="170" >
                        <input class="text_boxes" type="text" style="width:160px" onDblClick="openmypage_booking()" readonly placeholder="Double Click for Booking" name="txt_booking_no" id="txt_booking_no" />
                    </td>
                    <td align="" width="130"></td>
                    <td align="" width="170"></td>
                </tr>
                <tr>
                    <td align="right"><span class="must_entry_caption">Company Name</span></td>
                    <td><? 
                    //function create_drop_down( $field_id, $field_width, $query, $field_list, $show_select, $select_text_msg, $selected_index, $onchange_func, $is_disabled, $array_index, $fixed_options, $fixed_values, $not_show_array_index, $tab_index, $new_conn, $field_name )
                    echo create_drop_down( "cbo_company_name", 172, "select id,company_name from lib_company comp where status_active=1 and is_deleted=0 $company_cond order by company_name", "id,company_name",1, "-- Select Company --", $selected,"chack_variable(this.value)",0);
                    ?></td>
                    <td  align="right" class="must_entry_caption">Yarn Dyeing Factory</td>
                    <td><?
                    echo create_drop_down( "cbo_supplier_name", 172, "select a.id,a.supplier_name from lib_supplier a, lib_supplier_party_type b where a.id=b.supplier_id and a.status_active =1 and b.party_type in(2,21) group by a.id order by supplier_name","id,supplier_name", 1, "-- Select Supplier --", $selected, "",0 );
                    ?></td>
                    <td align="right">Booking Date</td>   
                    <td ><input class="datepicker" type="text" style="width:160px" name="txt_booking_date" id="txt_booking_date"/></td>
                </tr>
                <tr>
                    <td align="right">Delivery Date</td>
                    <td><input class="datepicker" type="text" style="width:160px" name="txt_delivery_date" id="txt_delivery_date"/></td>
                    <td align="right">Currency</td>
                    <td><? 
                    echo create_drop_down( "cbo_currency", 172, $currency,"", 1, "-- Select --", 2, "",0 );		
                    ?></td>
                    
                    <td  align="right">Exchange Rate</td>
                    <td ><input style="width:160px;" type="text" class="text_boxes"  name="txt_exchange_rate" id="txt_exchange_rate"  /></td>
                </tr>
                <tr>
                    <td  align="right">Pay Mode</td>
                    <td><?
                    echo create_drop_down( "cbo_pay_mode", 172, $pay_mode,"", 1, "-- Select Pay Mode --", "", "","" );
                    ?></td>
                    <td align="right">Source</td>   
                    <td align="left" height="10">
					<?
                    	echo create_drop_down( "cbo_source", 170, $source,"", 1, "-- Select --", $selected, "",0 );
                    ?>
                    </td> 
                    <td align="right">Attention</td>   
                    <td align="left" height="10">
                    <input class="text_boxes" type="text" style="width:160px;"  name="txt_attention" id="txt_attention"/>
                    </td>
                </tr>
                
                <tr>
                    <td align="center" height="10" colspan="6">
                    <input type="button" id="set_button" class="image_uploader" style="width:160px;" value="Terms & Condition/Notes" onClick="open_terms_condition_popup( 'requires/yarn_dyeing_charge_booking_controller.php?action=terms_condition_popup','Terms Condition'
)"/>
					
                    </td>
                </tr>
                
               <tr>
                    <td align="center" height="10" colspan="6">&nbsp;</td>
                </tr>
                
                
                <tr>
                    <td  colspan="6">
                        <table width="1100" cellspacing="0" cellpadding="0" border="0" class="rpt_table">
                            <thead>
                                <tr>
                                    <th width="100" class="must_entry_caption">Job No</th>
                                    <th width="70">Lot No</th>
                                    <th width="70">Count</th>
                                    <th width="200">Yarn Description</th>
                                    <th width="90">Yarn Color</th>
                                    <th width="90">Color Range</th>
                                    <th width="50">UOM</th>
                                    <th width="80">Yarn Wo. Qnty</th>
                                    <th width="90">Dyeing Charge</th>
                                    <th width="100">Amount</th>
                                    <th width="50">No of Bag</th>
                                    <th width="50">No of Cone</th>
                                    <th width="60">Min Req. Cone</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td >
                                    <input type="text" id="txt_job_no" name="txt_job_no" placeholder="Doubole Click for Job" readonly style="width:100px;" class="text_boxes" onDblClick="openmypage_job('job Search')"  />
                                    <input type="hidden" id="txt_job_id" name="txt_job_id" style="width:100px;">
                                    </td>
                                    <td>
                                    <input type="text" id="txt_lot" name="txt_lot" style="width:70px;" class="text_boxes" placeholder="Browse" onDblClick="openmypage_lot()" readonly />
                                    <input type="hidden" id="txt_pro_id" name="txt_pro_id" style="width:70px;"    />
                                    </td>
                                    <td>
                                    <?
                                        echo create_drop_down( "cbo_count", 70, "Select id, yarn_count from  lib_yarn_count where  status_active=1","id,yarn_count", 1, "-select-", $selected,"","0" );
                                    ?>
                                    </td>
                                    <td>
                                    <input type="text" id="txt_item_des" name="txt_item_des" style="width:190px;" class="text_boxes"   />
                                    </td>
                                    <td>
                                    <input type="text" id="txt_yern_color" name="txt_yern_color" style="width:90px;" class="text_boxes"   />
                                    </td>
                                    <td>
                                    <?
                                        echo create_drop_down( "cbo_color_range", 90, $color_range,"", 1, "-- Select--",$selected );
                                    ?>
                                    </td>
                                    <td>
                                    <?
                                        echo create_drop_down( "cbo_uom", 50, $unit_of_measurement,"", 1, "-- UOM--",12,"",1 );
                                    ?>
                                    </td>
                                    <td>
                                    <input type="text" id="txt_wo_qty" name="txt_wo_qty" style="width:80px;" class="text_boxes_numeric" onKeyUp="fnc_calculate()"   />
                                    </td>
                                    <td id="dyeing_charge_td">
                                        <input type="text" id="txt_dyeing_charge" name="txt_dyeing_charge" style="width:85px;" class="text_boxes_numeric"  onKeyUp="fnc_calculate()" />
                                    </td>
                                    <td>
                                    <input type="text" id="txt_amount" name="txt_amount" style="width:90px;" class="text_boxes_numeric" readonly />
                                    </td>
                                    <td>
                                    <input type="text" id="txt_bag" name="txt_bag" style="width:40px;" class="text_boxes_numeric"   />
                                    </td>
                                     <td>
                                    <input type="text" id="txt_cone" name="txt_cone" style="width:40px;" class="text_boxes_numeric"   />
                                    </td>
                                    <td>
                                    <input type="text" id="txt_min_req_cone" name="txt_min_req_cone" style="width:50px;" class="text_boxes_numeric"   />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                       </td>
                </tr>
                <br>
                <tr>
                    <td align="center" colspan="6" valign="middle" class="button_container">
                      <? echo load_submit_buttons( $permission, "fnc_yarn_dyeing", 0,0 ,"fnResetForm()",1) ; ?><input type="hidden" id="update_id" ><input type="hidden" id="dtls_update_id" >
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="6" height="10">
                    	<input type="button" value="Print Order" onClick="generate_trim_report()"  style="width:100px" name="print_booking" id="print_booking" class="formbutton" />                         
                    </td>
                </tr>
                
            </table>
         
      </fieldset>
  </form>
  <br>
  <fieldset style="width:1030px;">
           <div id="list_container"></div>
  </fieldset> 
	</div>
    
   <div style="display:none" id="data_panel"></div>

</body>
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>