<?php
/*-------------------------------------------- Comments
Purpose			: 	This form will create Quotation Evaluation
				
Functionality	:	
JS Functions	:
Created by		:	REZA 
Creation date 	: 	17-12-2013
Updated by 		: 		
Update date		: 		   
QC Performed BY	:		
QC Date			:	
Comments		:
*/



session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;

//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Quotation Evaluation Info","../", 1, 1, $unicode,1,1); 
?>	

<script>
if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../logout.php";
var permission='<?php echo $permission; ?>';
 
<?php
$designation_a=return_library_array("select id,custom_designation from lib_designation","id","custom_designation");
$designation_ar= json_encode($designation_a);
echo "var designation_arr = ". $designation_ar . ";\n";
?>

function fnc_electronic_approval_setup(operation){

var vfullname='cbo_Report_id*cbo_tag_report*'; var vfullname2 ='*';
for(i=1; i< $("#evaluation_tbl tbody tr").length; i++){
	vfullname += 'userid_'+i+'*txtcanbypass_'+i+'*txtsequenceno_'+i+'*';	
	vfullname2 += 'updateid_'+i+'*';	
	}
	vfullname += 'userid_'+i+'*txtcanbypass_'+i+'*txtsequenceno_'+i;	
	vfullname2 += 'updateid_'+i;	



 if( form_validation(vfullname,'Page/Report Name* Tag Report*Full Name*Designation*Can Bypass*Sequenceno No')==false )
	{
		return;
	}
//alert(vfullname);

	var dataString =vfullname+vfullname2;
 	var data="torow="+$("#evaluation_tbl tbody tr").length+"&action=save_update_delete&operation="+operation+get_submitted_data_string(dataString,"../");
	freeze_window(operation);
	http.open("POST","requires/electronic_approval_setup_controller.php",true);
	http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	http.send(data);
	http.onreadystatechange = fnc_emp_info_reponse;
}

function fnc_emp_info_reponse()
{
	if(http.readyState == 4) 
	{
		//alert(http.responseText);
		var response=trim(http.responseText).split('**');
		//alert (response); release_freezing();
		if(response[0]==0||response[0]==1)
		{
			show_msg(trim(response[0]));
			
			show_list_view(response[1],'create_emp_list_view','list_container','requires/electronic_approval_setup_controller','setFilterGrid("list_view",-1)');
			reset_form('electronic_approval_setup','','','','','');
			set_button_status(0, permission, 'fnc_electronic_approval_setup',1,1);
			//function set_button_status(is_update, permission, submit_func, btn_id, show_print)
			release_freezing();
		}
		if(response[0]==11)
		{
			alert("Id Card Number Should not be Duplicate");
		}
			release_freezing();

 	}
}


<!--System ID-->
function open_qepopup(id)
{ 
	var row_num=$('#evaluation_tbl tbody tr').length;	
	//alert (id);
	if( form_validation('cbo_Report_id','Page/Report Name')==false )
	{
		return;
	}
	var uid = $("#allid").val();
	var ud = $("#alldeg").val();
	var tri = $("#alltrid").val();
	//id++;
	var page_link="requires/electronic_approval_setup_controller.php?action=quot_popup"; 
	var title="Electronic Approval Setup";
	emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=590px,height=420px,center=1,resize=0,scrolling=0','')

	emailwindow.onclose=function()
	{
		var theemail=this.contentDoc.getElementById("selected_id");
		var response=theemail.value.split('_');
		if (theemail.value!="")
		{
			//freeze_window(5);
			for(var j=1; j<=row_num; j++)
			{
				if(document.getElementById('userid_'+j).value==response[0]){alert("This user is duplicate."); return;}
				
			}
				document.getElementById('userid_'+id).value=response[0];
				document.getElementById('txtsigningauthority_'+id).value=response[1];
				document.getElementById('txtfullname_'+id).value=response[2];
				document.getElementById('txtdesignation_'+id).value=designation_arr[response[3]];
				//continue;
			
		release_freezing();
		}
		
	}
}


function add_factor_row( i) 
{	
	
	
	
 if( form_validation('txtsigningauthority_'+i,'Full Name')==false )
	{
		return;
	}
	
	
	var row_num=$('#evaluation_tbl tr').length-1;
	if (row_num!=i)
	{
		return false;
	}
		i++;

 
 	$("#evaluation_tbl tr:last").clone().find("input,select").each(function() {
		$(this).attr({
		'id': function(_, id) { var id=id.split("_"); return id[0] +"_"+ i; },
		'name': function(_, name) { var name=name.split("_"); return name[0] +"_"+ i; },
		'value': function(_, value) { if(value=='+' || value=="-"){return value}else{return ''} }              
		});
		
		
	}).end().appendTo("#evaluation_tbl");
		var k=i-1;
		$('#incrementfactor_'+k).hide();
	  	$('#decrementfactor_'+k).hide();
		//$('#updateiddtls_'+i).val('');
		$('#txtsigningauthority_'+i).removeAttr("onDblClick").attr("onDblClick","open_qepopup("+i+");");
		$('#txtsequenceno_'+i).removeAttr("onKeyUp").attr("onKeyUp","checkSequence("+i+",this.value);");

	  
	  $('#incrementfactor_'+i).removeAttr("onClick").attr("onClick","add_factor_row("+i+");");
	  $('#decrementfactor_'+i).removeAttr("onClick").attr("onClick","fn_deletebreak_down_tr("+i+','+'"evaluation_tbl"'+");");

sl=i<10?'0'+i:i;
$( "#evaluation_tbl tr:last" ).find( "td:first" ).html(sl);
//$( "#evaluation_tbl tr::input[type='text']" ).find( "input[type=text]:last").value(i);
document.getElementById('txtsequenceno_'+i).value=i;
}

function fn_deletebreak_down_tr(rowNo,table_id ) 
{
	var numRow = $('#'+table_id+' tbody tr').length;
	if(numRow==rowNo && rowNo!=1)
	{
		var k=rowNo-1;
		$('#incrementfactor_'+k).show();
		$('#decrementfactor_'+k).show();
		
		$('#'+table_id+' tbody tr:last').remove();
	}
	else
		return false;
	
}


function checkSequence(id,v)
{
	var id1=((id-1)==0)?id:(id-1);

	if(($('#evaluation_tbl tbody tr').length) < v)
	{
		alert("Serial brake not allowed");
		document.getElementById('txtsequenceno_'+id).value=id;
	}
	else if(v != id)
	{
		alert('Duplicate sequence number not allowed.');
		document.getElementById('txtsequenceno_'+id).value=id;
	}
}

</script>
</head>
<body onLoad="set_hotkey()">

    <div style="width:850px; margin:0 auto;">
    <?php echo load_freeze_divs ("../",$permission);  ?>
    </div>
        <fieldset style="width:800px; margin:0 auto;">
        <legend>Electronic Approval Setup</legend>
        <form name="electronic_approval_setup" id="electronic_approval_setup" autocomplete="off">
        <table width="800" cellspacing="2" cellpadding="0" border="1"  id="tbl_quotation_evalu" rules="all">
            <tr>
                <td align="center">
                    <fieldset style="width:450px;" id="quotationevaluation_3">
                        <table width="750" cellspacing="2" cellpadding="0" border="1" rules="all">
                            <tr>
                                <td width="150" class="must_entry_caption">Page/Report Name </td>
                                <td id="supplier_td" width="320">
									<?php 
									
										echo create_drop_down( "cbo_Report_id", 310, "select m_menu_id,menu_name from main_menu where report_menu=1 order by menu_name","m_menu_id,menu_name", 1, "-- Select Page/Report Name --", $selected, "",0,"","","","",10);
                                    ?>	 
                                </td>
                                <td class="must_entry_caption">Tag Report</td>
                                <td id="supplier_td" width="200">
									<?php 
										echo create_drop_down( "cbo_tag_report", 200, $entry_form_for_approval,"", 1, "-- Select Page/Report Name --", $selected, "",0,"","","","",10);
                                    ?>	 
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <fieldset style="width:800px;">
                        <table align="left" cellspacing="0" cellpadding="0" id="evaluation_tbl"  border="1" class="rpt_table"  rules="all">
                            <thead>
                                <th>SL 
                                <input type="hidden" id="allid">
                                <input type="hidden" id="alldeg">
                                <input type="hidden" id="alltrid">
                                </th>
                                <th>Signing Authority</th>
                                <th>Full Name</th>
                                <th>Designation</th>
                                <th>Can Bypass</th>
                                <th>Sequence No</th>
                                <th width="70">&nbsp;</th>
                            </thead>
                            <tbody>
                                <tr>
                                <td> 01</td>
                                    <td>
                                    	<input type="hidden" name="updateid_1" id="updateid_1" />
                                        <input type="hidden" name="userid_1" id="userid_1" />
                                        <input style="width:150px;" type="text" name="txtsigningauthority_1" readonly placeholder="Double Click" id="txtsigningauthority_1" onDblClick="open_qepopup(1)"  class="text_boxes" />
                                       
                                    </td>
                                    <td>
                                        <input style="width:200px;" type="text" readonly name="txtfullname_1" id="txtfullname_1"  class="text_boxes" />
                                    </td>
                                    <td>
                                        <input style="width:120px;" type="text" readonly name="txtdesignation_1" id="txtdesignation_1"  class="text_boxes" />
                                    </td>
                                    <td>
									<?php 
										echo create_drop_down( "txtcanbypass_1", 100, $yes_no,"", 1, "-- Select --", 2, "",0,"","","","",10);
                                    ?>	 
                                    
                                    </td>
                                    <td>
                                        <input style="width:80px;" type="text"  name="txtsequenceno_1" id="txtsequenceno_1"  value="1" onKeyUp="checkSequence(1,this.value)" class="text_boxes" />
                                    </td>
                                    <td width="70" align="left">
                                      &nbsp;
                                        <input style="width:25px;" type="button" id="incrementfactor_1" name="incrementfactor_1"  class="formbutton" value="+" onClick="add_factor_row(1)"/>
                                        <input style="width:25px;" type="button" id="decrementfactor_1" name="decrementfactor_1"  class="formbutton" value="-" onClick="javascript:fn_deletebreak_down_tr(1,'evaluation_tbl')"/>
                                        
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <td align="center" class="button_container">
                
                    <?php
						echo load_submit_buttons($permission,"fnc_electronic_approval_setup",0,0,"reset_form('electronic_approval_setup','','','','')",1);
	
                    ?>
                    
                </td>
            </tr>
        </table>
        </form>
    
<div id="list_container" style="margin:0 auto; width:40%;">
<?php
	if($db_type==2)
	{
 $sql="select b.menu_name,a.page_id from electronic_approval_setup  a, main_menu  b where b.m_menu_id=a.page_id and a.is_deleted=0 GROUP BY b.menu_name,a.page_id ";	
	}
	if($db_type==0)
	{
 $sql="select b.menu_name,a.page_id from electronic_approval_setup  a, main_menu  b where b.m_menu_id=a.page_id and a.is_deleted=0 GROUP BY b.menu_name ";	
	}

$arr=array(3=>$yes_no);
	
	echo  create_list_view("list_view", "Page/Report Name", "300","350","260",0, $sql, "get_php_form_data", "page_id", "'electronic_approval_setup_from_data','requires/electronic_approval_setup_controller'", 1, "0", $arr , "menu_name", "employee_info_controller",'setFilterGrid("list_view",-1);','0') ;

?>

 </div> 
    
    </fieldset>
    
<script src="../includes/functions_bottom.js" type="text/javascript"></script> 
</body>
</html> 