<?php 
header('Content-type:text/html; charset=utf-8');
session_start();
include('../../../includes/common.php');

$user_id = $_SESSION['logic_erp']["user_id"];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];


if($db_type==2 || $db_type==1 )
{
	$mrr_date_check="and to_char(insert_date,'YYYY')=".date('Y',time())."";
}
else if ($db_type==0)
{
	$mrr_date_check="and year(insert_date)=".date('Y',time())."";
}

if ($action=="load_drop_down_store")
{	
	echo create_drop_down( "cbo_store_name", 130, "select a.id,a.store_name from lib_store_location a, lib_store_location_category b where a.id=b.store_location_id and b.category_type in (8,9,10,11,15,16,17,18,19,20,21,22) and a.status_active=1 and a.is_deleted=0 and FIND_IN_SET($data,a.company_id) group by a.id order by a.store_name","id,store_name", 1, "-- Select --", "", "","" );  	 
	exit();
}

if ($action=="load_drop_down_supplier")
{	 
	echo create_drop_down( "cbo_supplier", 130, "select a.id,a.supplier_name from lib_supplier a, lib_supplier_party_type b where FIND_IN_SET($data,a.tag_company) and a.id=b.supplier_id and b.party_type in (1,6,7,8,90) and a.status_active=1 and a.is_deleted=0 group by a.id order by a.supplier_name","id,supplier_name", 1, "-- Select --", "", "" );  	 
	exit();
}

?>
<?php
if($action=="mrr_popup")
{
  	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	extract($_REQUEST); 
	//echo "$company"; 
?>
     
<script>
	function js_set_value(mrr)
	{
 		$("#hidden_recv_number").val(mrr); // mrr number
		parent.emailwindow.hide();
	}
</script>
</head>
<body>
<div align="center" style="width:100%;" >
<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
	<table width="820" cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
            <thead>
                <tr>                	 
                    <th width="150">Supplier</th>
                     <th width="150">Item Category</th>
                   <!-- <th width="150">Search By</th>-->
                    <th width="200">Date Range</th>
                    <th><input type="reset" name="re_button" id="re_button" value="Reset" style="width:100px" class="formbutton"  /></th>           
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <?php  
 							echo create_drop_down( "cbo_supplier", 150, "select a.id,a.supplier_name from lib_supplier a, lib_supplier_party_type b where a.id=b.supplier_id and  b.party_type in(1,6,7,8) and a.status_active=1 and a.is_deleted=0  group by a.id,a.supplier_name order by a.supplier_name","id,supplier_name", 1, "-- Select --", 0, "",0 );
                        ?>
                    </td>
                    <td width="">
                     <?php //create_drop_down( $field_id, $field_width, $query, $field_list, $show_select, $select_text_msg, $selected_index, $onchange_func, $is_disabled, $array_index, $fixed_options, $fixed_values, $not_show_array_index, $tab_index ) 
					 echo create_drop_down( "cbo_item_category", 130, $item_category,"", 1, "-- Select --", 0, "", 0,"","","","1,2,3,4,5,6,7,12,13,14" );
                     ?>
                    <td align="center">
                        <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px" placeholder="From Date" />
                        <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px" placeholder="To Date" />
                    </td> 
                    <td align="center">
                        <input type="button" name="btn_show" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_supplier').value+'_'+document.getElementById('cbo_item_category').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+<?php echo $company; ?>, 'create_mrr_search_list_view', 'search_div', 'general_item_receive_return_entry_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:100px;" />				
                    </td>
            </tr>
        	<tr>                  
            	<td align="center" height="40" valign="middle" colspan="5">
					<?php echo load_month_buttons(1);  ?>
                    <!-- Hidden field here-------->
                     <input type="hidden" id="hidden_recv_number" value="" />
                    <!-- ---------END------------->
                </td>
            </tr>    
            </tbody>
         </tr>         
        </table>    
        <div align="center" valign="top" id="search_div"> </div> 
        </form>
   </div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}

if($action=="serial_popup")
{
	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	extract($_REQUEST); 
	//echo $txt_received_id; die;
	//echo $current_prod_id; die;

 	$serialStringID = str_replace("'","",$serialStringID);
 	//$serialStringNo = str_replace("'","",$serialStringNo);
	$txt_received_id = str_replace("'","",$txt_received_id);
	$current_prod_id = str_replace("'","",$current_prod_id);
	
 	?>
	<script>
	var selected_id = new Array();
	var selected_no = new Array();	
	
	 
	var serialNoArr="<?php echo $serialStringID; ?>";
 	var chk_selected_no = new Array();
	var chk_selected_id = new Array();
	if(serialNoArr!=""){chk_selected_no=serialNoArr.split(",");}
	
	 
	
	function check_all_data() 
	{
		var tbl_row_count = document.getElementById( 'hidden_all_id' ).value.split(","); 
 		//tbl_row_count = tbl_row_count-1;
		for( var i = 0; i < tbl_row_count.length; i++ ) {
 			if( jQuery.inArray( $('#txt_serial_id' + tbl_row_count[i]).val(), chk_selected_id ) != -1 )
			js_set_value( tbl_row_count[i] );
		}
	}
	
	function toggle( x, origColor ) {
			var newColor = 'yellow';
			if ( x.style ) {
				x.style.backgroundColor = ( newColor == x.style.backgroundColor )? origColor : newColor;
				//x.style.backgroundColor = ( $serialStringID != "")? newColor : origColor;
			}
		} 
		
	function js_set_value( str ) {
		toggle( document.getElementById( 'search' + str ), '#FFFFCC' );
		
		if( jQuery.inArray( $('#txt_serial_id' + str).val(), selected_id ) == -1 ) {
			selected_id.push( $('#txt_serial_id' + str).val() );
			selected_no.push( $('#txt_serial_no' + str).val() );
 		}
		else {
			for( var i = 0; i < selected_id.length; i++ ) {
				if( selected_id[i] == $('#txt_serial_id' + str).val() ) break;
			}
			selected_id.splice( i, 1 );
			selected_no.splice( i, 1 );
		}
		var id = '';	var no = '';
		for( var i = 0; i < selected_id.length; i++ ) {
			id += selected_id[i] + ',';
			no += selected_no[i] + ',';
		}
		id = id.substr( 0, id.length - 1 );
		no = no.substr( 0, no.length - 1 );
  		$('#txt_string_id').val( id );
		$('#txt_string_no').val( no );
	}
	 
	function fn_onClosed()
	{
		var txt_string = $('#txt_string').val();
		if(txt_string==""){ alert("Please Select The Serial"); return;}
		parent.emailwindow.hide();
	}
	 
	</script>
	</head>
	<body>
	<div align="center" style="width:100%;" >
	<form name="searchlcfrm_1" id="searchlcfrm_1" autocomplete="off">
    	<table width="500" cellspacing="0" cellpadding="0" border="0" class="rpt_table" id="tbl_header" >
				<thead>
					<tr>                	 
						<th width="300">Serial No</th>
                        <th width="">Warranty Date</th>
 					</tr>
				</thead>
        </table>        
        <div style="width:500px; overflow-y:scroll; min-height:220px">
		<table width="480" cellspacing="0" cellpadding="0" border="0" class="rpt_table" id="tbl_serial" style="overflow:scroll; min-height:300px" >
 				<tbody>
                	<?php
						$i=1;
						$sql="select a.id,a.recv_trans_id,a.issue_trans_id,a.prod_id,a.serial_no,b.expire_date,a.is_issued from inv_serial_no_details a, inv_transaction b where a.prod_id=$current_prod_id and a.recv_trans_id=b.id and a.prod_id=b.prod_id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.is_issued=0 and b.mst_id=$txt_received_id";
						//echo $sql;
						$result = sql_select($sql);
						$count=count($result );
						foreach($result as $row) 
						{
							if ($i%2==0)$bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
							if($new_data=="") $new_data=$row[csf("id")]; else $new_data .=",".$row[csf("id")];				
						?>	
							<tr bgcolor="<?php echo $bgcolor; ?>"  onClick="js_set_value(<?php echo $row[csf("id")]; ?>)" id="search<?php echo $row[csf("id")]; ?>" style="cursor:pointer">
								<td  width="300">
									<?php echo trim($row[csf("serial_no")]); ?> 
									<input type="hidden" id="txt_serial_id<?php echo $row[csf("id")]; ?>" value="<?php echo $row[csf("id")]; ?>" >
                                    <input type="hidden" id="txt_serial_no<?php echo $row[csf("id")]; ?>" value="<?php echo $row[csf("serial_no")]; ?>" >
								</td>
								<td  width="">
									<?php echo change_date_format($row[csf("expire_date")]); 
									
									if($count==$i)
									{
									?> 
                                    <input type="hidden" id="hidden_all_id" value="<?php echo $new_data; ?>" >
                                    <?php } ?>
								</td>
							</tr> 
					<?php 
						
							$i++;
						}

				?>
				</tbody>         
			</table>  
            </div>
            <div><input type="button" name="btn_close" class="formbutton" style="width:100px" value="Close" onClick="fn_onClosed()" /></div>  
            <!-- Hidden field here-------->
			<input type="hidden" id="txt_string_id" value="" />
            <input type="hidden" id="txt_string_no" value="" />				 
			<!-- ---------END-------------> 
			</form>
	   </div>
	</body>           
	<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
    
    <script>
		if( serialNoArr!="" )
		{
			serialNoArr=serialNoArr.split(",");
			for(var k=0;k<serialNoArr.length; k++)
			{
				js_set_value(serialNoArr[k] );
				//alert(serialNoArr[k]);
			}
		}
	</script>
	</html>
	<?php
}
?>
<?php
if($action=="create_mrr_search_list_view")
{
	$ex_data = explode("_",$data);
	//print_r($ex_data);
	$supplier = $ex_data[0];
	$txt_item_category1 = $ex_data[1];
	//$txt_search_by = $ex_data[2];
	$fromDate = $ex_data[2];
	$toDate = $ex_data[3];
	//echo $ex_data[5];
	$company = $ex_data[4];
	//print $company;
	
		if ($fromDate!="" && $toDate!="") $sql_cond .= " and a.receive_date between '".change_date_format($fromDate, "yyyy-mm-dd", "-")."' and '".change_date_format($toDate, "yyyy-mm-dd", "-")."'"; else $sql_cond ="";
	//$sql_cond="";
	if(($company)!="") $sql_cond .= " and a.company_id='$company'"; 
	//if(($supplier)!=0) $sql_cond .= " and a.supplier_id='$supplier' "; else { echo "Please Select Supplier First."; die; }
	//if(($txt_item_category1)!=0) $sql_cond .= " and a.item_category='$txt_item_category1' "; else { echo "Please Select Item Category ."; die; }
	
	//echo "select a.recv_number,a.supplier_id,a.item_category,a.challan_no,c.lc_number,a.receive_date,a.receive_basis,sum(b.cons_quantity) as receive_qnty from inv_receive_master a,inv_transaction b,com_btb_lc_master_details c where a.lc_no=c.id and a.item_category='$txt_item_category' and a.id=b.mst_id  and a.status_active=1 and a.company_id='$company' "; 
	$sql = "select a.recv_number_prefix_num,a.recv_number,a.supplier_id,b.item_category,a.challan_no,c.lc_number,a.receive_date,a.receive_basis,sum(b.cons_quantity) as receive_qnty from inv_transaction b,inv_receive_master a left join com_btb_lc_master_details c on a.lc_no=c.id where a.item_category not in(1,2,3,4,5,6,7,12,13,14) and a.id=b.mst_id and  a.status_active=1 $sql_cond 
	group by 
			b.mst_id,a.recv_number , a.recv_number_prefix_num  ,a.supplier_id ,a.challan_no ,a.receive_date,a.receive_basis,b.item_category, c.lc_number ";
	$supplier_arr = return_library_array( "select id, supplier_name from lib_supplier",'id','supplier_name');
	//echo $sql;
	$arr=array(1=>$item_category,2=>$supplier_arr,6=>$receive_basis_arr);
	echo  create_list_view("list_view", "MRR No, Item Category, Supplier Name, Challan No, LC No, Receive Date, Receive Basis, Receive Qnty","120,100,120,100,100,100,100,120","860","250",0, $sql , "js_set_value", "recv_number","", 1, "0,item_category,supplier_id,0,0,0,receive_basis,0", $arr, "recv_number_prefix_num,item_category,supplier_id,challan_no,lc_number,receive_date,receive_basis,receive_qnty", "",'','0,0,0,0,0,0,1,0') ;
	//echo  create_list_view("list_view", "Requisition No,Requisition Date,Company,Item Category,Location,Department,Section,status", "80,80,100,100,100,90,90,80","780","350",0, $sql , "js_set_value", "id", "",1,"0,0,company_id,item_category_id,location_id,department_id,section_id,status_active", $arr , "requ_prefix_num,requisition_date,company_id,item_category_id,location_id,department_id,section_id,status_active","purchase_requisition_controller","",'0,3,0,0,0,0,0,0') ;	
	exit();
}

if($action=="populate_data_from_data")
{
	$sql = "select id,recv_number,company_id,receive_basis,receive_purpose,receive_date,challan_no,store_id,lc_no,supplier_id,exchange_rate,currency_id,lc_no,source 
			from inv_receive_master 
			where recv_number='$data' and entry_form=20 ";
	//echo $sql;
	$res = sql_select($sql);
	foreach($res as $row)
	{
		echo "$('#txt_received_id').val('".$row[csf("id")]."');\n";
		echo "$('#txt_mrr_no').val('".$row[csf("recv_number")]."');\n";
		echo "$('#cbo_company_name').val(".$row[csf("company_id")].");\n";
 		echo "$('#cbo_return_to').val(".$row[csf("supplier_id")].");\n";
		//right side list view
		echo"show_list_view('".$row[csf("id")]."','show_product_listview','list_product_container','requires/general_item_receive_return_entry_controller','');\n";
   	}	
	exit();	
}
?>
<?php
//right side product list create here--------------------//
if($action=="show_product_listview")
{ 
	
	 $sql = "select b.store_id,b.item_category,b.transaction_type,c.item_group_id,c.item_description,b.cons_quantity,b.cons_rate,b.mst_id as mrr_id,b.id as tr_id, c.id as prod_id
	from  inv_transaction b, product_details_master c 
	where b.prod_id=c.id and b.mst_id='$data' and b.transaction_type=1 and b.item_category not in (1,2,3,4,5,6,7,12,13,14)";
	//echo $sql;
  	$result = sql_select($sql);
	//print_r( $result) ;
	$store_name=return_library_array("select id,store_name from lib_store_location", "id","store_name"); 
	$item_name_arr=return_library_array("select id,item_name from lib_item_group", "id","item_name");
	$i=1; 
 	?>
    	<table class="rpt_table" border="1" cellpadding="2" cellspacing="0">
        <caption>Display Received Items </caption>
        	<thead><tr><th>SL</th><th>Item Cetagory</th><th>Group Name</th><th>Description</th><th>Recv. Qty</th><th>Cons Rate</th></tr></thead>
            <tbody>
            	<?php
				
				foreach($result as $row)
				{ 
					if ($i%2==0)$bgcolor="#E9F3FF";						
					else $bgcolor="#FFFFFF";  
					?>
					<tr bgcolor="<?php echo $bgcolor; ?>" onClick='get_php_form_data("<?php echo $row[csf("tr_id")];?>","item_details_form_input","requires/general_item_receive_return_entry_controller")' style="cursor:pointer" >
                        <td><?php echo $i; ?></td>
                        
                        <!--<td><?php //echo $item_name_arr[$row[csf("store_id")]]; txt_prod_id ?></td>-->
                        <td><?php echo $item_category[$row[csf("item_category")]]; ?></td>
                        <td><?php echo $item_name_arr[$row[csf("item_group_id")]]; ?></td>
                        <!-- <td><?php //echo $row[csf("txt_prod_id")]; ?></td>-->
                        <td><?php echo $row[csf("item_description")]; ?></td>
                        <td><?php echo $row[csf("cons_quantity")]; ?></td>
                        <td><?php echo $row[csf("cons_rate")]; ?></td>
					</tr>
					<?php
					$i++; 
				 } 
				 ?>
            </tbody>
        </table>
     </fieldset>   
	<?php	 
	exit();
}
//child form data input here-----------------------------//
if($action=="item_details_form_input")
{
	 $sql = "select b.id as prod_id,b.item_group_id,b.item_description,b.current_stock,a.id,a.item_category,a.balance_qnty,a.cons_quantity,a.cons_rate,a.cons_uom,a.store_id
			from inv_transaction a,product_details_master b
 			where a.id=$data and a.status_active=1 and a.prod_id=b.id and b.status_active=1";
			$store_name=return_library_array("select id,store_name from lib_store_location", "id","store_name"); 
 	//echo $sql;die;
		$item_name_arr=return_library_array("select id,item_name from lib_item_group", "id","item_name");
	$result = sql_select($sql);
	foreach($result as $row)
	{
		echo "$('#txt_store_name').val('".$store_name[$row[csf("store_id")]]."');\n";
		echo "$('#category').val('".$row[csf("item_category")]."');\n";
 		echo "$('#txt_item_category').val('".$item_category[$row[csf("item_category")]]."');\n";
		echo "$('#txt_item_group').val('".$item_name_arr[$row[csf("item_group_id")]]."');\n";
		echo "$('#txt_prod_id').val('".$row[csf("prod_id")]."');\n";
		echo "$('#store').val('".$row[csf("store_id")]."');\n";
		//echo "$('#store').val('id');\n";
		//echo "$('#category_store_uom').val('".$row[csf("id")]."');\n";
		echo "$('#txt_item_description').val('".$row[csf("item_description")]."');\n";
		echo "$('#transaction_id').val('".$row[csf("id")]."');\n";
		//echo "$('#txt_receive_qty').val('".$row[csf("cons_quantity")]."');\n";
		//echo "$('#txt_item_description').val('".$row[csf("cons_rate")]."');\n";
		//echo "$('#txt_receive_qty').val('');\n";
		echo "$('#txt_return_rate').val('".$row[csf("cons_rate")]."');\n";
		echo "$('#txt_curr_stock').val('".$row[csf("balance_qnty")]."');\n";
		echo "$('#txt_cons_quantity').val('".$row[csf("cons_quantity")]."');\n";
		//echo "$('#txt_curr_stock').val('".$row[csf("cons_quantity")]."');\n";
		echo "$('#txt_uom').val('".$unit_of_measurement[$row[csf("cons_uom")]]."');\n";
		echo "$('#uom').val('".$row[csf("cons_uom")]."');\n";
		echo "set_button_status(0, permission, 'fnc_general_receive_return_entry',1,1);\n";
		echo "$('#txt_receive_qty').val('');\n";
	}

	exit();		
}
?>
<?php
//data save update delete here------------------------------//
if($action=="save_update_delete")
{	 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	if( $operation==0 ) // Insert Here----------------------------------------------------------
	{
		$con = connect();
		if($db_type==0)	{ mysql_query("BEGIN"); }
		//table lock here  
		///if( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0"; die;}
 		
		//---------------Check Duplicate product in Same return number ------------------------//
		$duplicate = is_duplicate_field("b.id","inv_issue_master a, inv_transaction b","a.id=b.mst_id and a.issue_number=$txt_mrr_retrun_no and b.prod_id=$txt_prod_id and b.transaction_type=3"); 
		if($duplicate==1) 
		{
			echo "20**Duplication is Not Allowed in Same Return Number And Same Product.";
			die;
		}
		//------------------------------Check Brand END---------------------------------------//
		
 		if(str_replace("'","",$txt_mrr_retrun_no)!="")
		{
			$new_return_number[0] = str_replace("'","",$txt_mrr_retrun_no);
			$id=return_field_value("id","inv_issue_master","issue_number=$txt_mrr_retrun_no");
			//General master table UPDATE here START----------------------//		
 			$field_array_receive="entry_form*company_id*supplier_id*issue_date*challan_no*received_id*received_mrr_no*updated_by*update_date";
			$data_array_receive="26*".$cbo_company_name."*".$cbo_return_to."*".$txt_receive_date."*".$txt_challan_no."*".$txt_received_id."*".$txt_mrr_no."*'".$user_id."'*'".$pc_date_time."'";
			//echo $field_array."<br>".$data_array;die;
			//$rID=sql_update("inv_issue_master",$field_array_receive,$data_array_receive,"id",$id,1);
				
			//General master table UPDATE here END---------------------------------------// 
		}
		else  	
		{	 
			//General master table entry here START---------------------------------------txt_challan_no//		
			$id=return_next_id("id", "inv_issue_master", 1);		
			$new_return_number=explode("*",return_mrr_number( str_replace("'","",$cbo_company_name), '', 'GRR', date("Y",time()), 5, "select issue_number_prefix,issue_number_prefix_num from inv_issue_master where company_id=$cbo_company_name and entry_form=26 $mrr_date_check order by id DESC ", "issue_number_prefix", "issue_number_prefix_num" ));
 			$field_array_receive="id, issue_number_prefix, issue_number_prefix_num, issue_number, entry_form, company_id, supplier_id, issue_date,challan_no, received_id, received_mrr_no, inserted_by, insert_date";
			$data_array_receive="(".$id.",'".$new_return_number[1]."','".$new_return_number[2]."','".$new_return_number[0]."',26,".$cbo_company_name.",".$cbo_return_to.",".$txt_receive_date.",".$txt_challan_no.",".$txt_received_id.",".$txt_mrr_no.",'".$user_id."','".$pc_date_time."')";
			//echo "20**".$field_array."<br>".$data_array;die;
			//$rID=sql_insert("inv_issue_master",$field_array_receive,$data_array_receive,1);
			//General master table entry here END---------------------------------------// 
		}
		
	 	 //transaction table insert here START--------------------------------//
		//$transaction_type=array(1=>"Receive",2=>"Issue",3=>"Receive Return",4=>"Issue Return");
		$txt_issue_qnty = str_replace("'","",$txt_receive_qty);
		$txt_rate = str_replace("'","",$txt_return_rate);
 		$issue_stock_value = str_replace("'","",$txt_return_value);
		$transactionID = return_next_id("id", "inv_transaction", 1); 				
		$field_array_trans = "id,mst_id,company_id,supplier_id,prod_id,item_category,transaction_type,transaction_date,store_id,cons_uom,cons_quantity,cons_rate,cons_amount,inserted_by,insert_date";
 		$data_array_trans = "(".$transactionID.",".$id.",".$cbo_company_name.",".$cbo_return_to.",".$txt_prod_id.",".$category.",3,".$txt_receive_date.",".$store.",".$uom.",".$txt_issue_qnty.",".$txt_rate.",".$issue_stock_value.",'".$user_id."','".$pc_date_time."')"; 

		//$transID = sql_insert("inv_transaction",$field_array_trans,$data_array_trans,1);
		
		
		//adjust product master table START-------------------------------------//
 		$txt_receive_qty = str_replace("'","",$txt_receive_qty);
		$txt_return_value = str_replace("'","",$txt_return_value);
		$sql = sql_select("select item_group_id,item_description,avg_rate_per_unit,last_purchased_qnty,current_stock,stock_value from product_details_master where id=$txt_prod_id ");	//print_r($sql); die;
		$presentStock=$presentStockValue=$presentAvgRate=0;
		$item_description="";
		foreach($sql as $result)
		{
			$presentStock			=$result[csf("current_stock")];
			$presentStockValue		=$result[csf("stock_value")];
			$presentAvgRate			=$result[csf("avg_rate_per_unit")];
			$item_group_id 			=$result[csf("item_group_id")];
			$item_description		=$result[csf("item_description")];
		}
		$nowStock 		= $presentStock-$txt_receive_qty;
		$nowStockValue 	= $presentStockValue-$txt_return_value;
		$nowAvgRate		= number_format($nowStockValue/$nowStock,$dec_place[3],".","");	
		$field_array_product="last_issued_qnty*current_stock*stock_value*updated_by*update_date";
		$data_array_product="".$txt_receive_qty."*".$nowStock."*".$nowStockValue."*'".$user_id."'*'".$pc_date_time."'";
		
		
		//$prodUpdate = sql_update("product_details_master",$field_array_product,$data_array_product,"id",$txt_prod_id,1);
		
		
		//adjust product master table END  -------------------------------------//
				
		//if LIFO/FIFO then START -----------------------------------------//
		//$field_array1 = "id,recv_trans_id";
		$field_array1 = "id,recv_trans_id,issue_trans_id,entry_form,prod_id,issue_qnty,rate,amount,inserted_by,insert_date";
		$update_array = "balance_qnty*balance_amount*updated_by*update_date";
		$cons_rate=0; 
		$data_array1="";
		$updateID_array=array();
		$update_data=array();
		$issueQnty = $txt_receive_qty;
		$mrrWiseIsID = return_next_id("id", "inv_mrr_wise_issue_details", 1);  
		
		$isLIFOfifo=return_field_value("store_method","variable_settings_inventory","company_name=$cbo_company_name and variable_list=16","store_method");
		if($isLIFOfifo==2) $cond_lifofifo=" DESC"; else $cond_lifofifo=" ASC"; 
		
		$sql = sql_select("select id,cons_rate,balance_qnty,balance_amount from inv_transaction where id=$transaction_id and prod_id=$txt_prod_id and balance_qnty>0 and transaction_type in(1,4,5) and item_category=$category order by transaction_date $cond_lifofifo");			
		foreach($sql as $result)
		{				
			$issue_trans_id = $result[csf("id")]; // this row will be updated
			$balance_qnty = $result[csf("balance_qnty")];
			$balance_amount = $result[csf("balance_amount")];
			$cons_rate = $result[csf("cons_rate")]; 
			$issueQntyBalance = $balance_qnty-$issueQnty; // minus issue qnty
			$issueStockBalance = $balance_amount-($issueQnty*$cons_rate);
			if($issueQntyBalance>=0)
			{					
				$amount = $issueQnty*$cons_rate;
				//for insert
				if($data_array1!="") $data_array1 .= ",";  
				//$data_array1 .= "(".$mrrWiseIsID.",".$issue_trans_id.")";
				$data_array1 .= "(".$mrrWiseIsID.",".$issue_trans_id.",".$transactionID.",26,".$txt_prod_id.",".$issueQnty.",".$cons_rate.",".$amount.",'".$user_id."','".$pc_date_time."')";
				//for update
				$updateID_array[]=$issue_trans_id; 
				$update_data[$issue_trans_id]=explode("*",("".$issueQntyBalance."*".$issueStockBalance."*'".$user_id."'*'".$pc_date_time."'"));
				break;
			}
			else if($issueQntyBalance<0)
			{
				
				$issueQntyBalance  = $issueQnty-$balance_qnty;				
				$issueQnty = $balance_qnty;				
				$amount = $issueQnty*$cons_rate;
				
				//for insert
				if($data_array1!="") $data_array1 .= ",";  
				//$data_array1 .= "(".$mrrWiseIsID.",".$issue_trans_id.")";
				$data_array1 .= "(".$mrrWiseIsID.",".$issue_trans_id.",".$transactionID.",26,".$txt_prod_id.",".$issueQnty.",".$cons_rate.",".$amount.",'".$user_id."','".$pc_date_time."')";
				//echo "20**".$data_array;die;
				//for update
				$updateID_array[]=$issue_trans_id; 
				$update_data[$issue_trans_id]=explode("*",("0*0*'".$user_id."'*'".$pc_date_time."'"));
				$issueQnty = $issueQntyBalance;
			}
			$mrrWiseIsID++;
			
		}//end foreach 


 		if(str_replace("'","",$txt_mrr_retrun_no)!="")
		{
			$rID=sql_update("inv_issue_master",$field_array_receive,$data_array_receive,"id",$id,1);
		}
		else
		{
			$rID=sql_insert("inv_issue_master",$field_array_receive,$data_array_receive,1);
		}
		$transID = sql_insert("inv_transaction",$field_array_trans,$data_array_trans,1);
		$prodUpdate = sql_update("product_details_master",$field_array_product,$data_array_product,"id",$txt_prod_id,1);

		//transaction table stock update here------------------------//
		$upTrID=true;
		if(count($updateID_array)>0)
		{
			//echo bulk_update_sql_statement("inv_transaction","id",$update_array,$update_data,$updateID_array);die;
 			$upTrID=execute_query(bulk_update_sql_statement("inv_transaction","id",$update_array,$update_data,$updateID_array),1);
				//if($upTrID) echo "aaaaaaaaa";
		}
		//mrr wise issue data insert here----------------------------//
		$mrrWiseIssueID=true;
		if($data_array1!="")
		{		
			$mrrWiseIssueID=sql_insert("inv_mrr_wise_issue_details",$field_array1,$data_array1,1);
			
		}
		$txt_serial_id 	= str_replace("'","",$txt_serial_id);
		$serialUpdate=true;
 		if($txt_serial_id!="")
		{
			if( strpos(trim($txt_serial_no), ",")>0)
			{
				$se_data=explode(",",str_replace("'","",$txt_serial_no));
				if( (count($se_data)<=str_replace("'","",$txt_receive_qty)))
				{
					$serialUpdate = execute_query("update inv_serial_no_details set rev_rtn_trans_id=$transactionID , is_issued=1 where id in ($txt_serial_id)",1);
					
				}
				else
				{
					echo "50";
					die;
				}
			}
			else
			{
				$serialUpdate 	= execute_query("update inv_serial_no_details set rev_rtn_trans_id=$transactionID , is_issued=1 where id in ($txt_serial_id)",1);
			}
		}
		
		
 		//if LIFO/FIFO then END -----------------------------------------//
		
		/*$txt_prod_id 	= str_replace("'","",$txt_prod_id);
		$serialUpdate=true;
 		if($txt_prod_id!=""){
			$serialUpdate 	= execute_query("update inv_serial_no_details set rev_rtn_trans_id=$transactionID, is_issued=1 where prod_id=$txt_prod_id");
		}*/
		
		
		

		//echo "20**".$rID." && ".$transID." && ".$prodUpdate." && ".$mrrWiseIssueID." && ".$upTrID;mysql_query("ROLLBACK"); die;
		//release lock table
		
		//&& $transID && $prodUpdate && $mrrWiseIssueID && $upTrID
		
		if($db_type==0)
		{
			if($rID && $transID && $prodUpdate && $mrrWiseIssueID && $upTrID && $serialUpdate)
			{
				mysql_query("COMMIT");  
				echo "0**".$new_return_number[0];
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".$new_return_number[0];
			}
		}
		if($db_type==2 || $db_type==1 )
		{
			if($rID && $transID && $prodUpdate && $mrrWiseIssueID && $upTrID && $serialUpdate)
			{
				oci_commit($con);
				echo "0**".$new_return_number[0]."**".$id;
			}
			else
			{
				oci_rollback($con); 
				echo "10**".$new_return_number[0];
			}
		}
		disconnect($con);
		die;
				
	}
	
	else if ($operation==1) // Update Here----------------------------------------------------------
	{
		$con = connect();		
		if($db_type==0)	{ mysql_query("BEGIN"); }
		//table lock here 
		//check_table_status( $_SESSION['menu_id'],0);
		//if( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0"; die;}
		
		//check update id
		/*if( str_replace("'","",$update_id) == "" )
		{
			//echo "15";exit(); 
		}*/
		
		//****************************************** BEFORE ENTRY ADJUST START *****************************************//
		
		//product master table information
		//before stock update
		$sql = sql_select( "select a.id,a.avg_rate_per_unit,a.current_stock,a.stock_value, b.cons_quantity, b.cons_amount from product_details_master a, inv_transaction b where a.id=b.prod_id and b.id=$update_id and a.item_category_id not in (1,2,3,4,5,6,7,12,13,14) and b.item_category not in (1,2,3,4,5,6,7,12,13,14) and b.transaction_type=3" );
		$before_prod_id=$before_issue_qnty=$before_stock_qnty=$before_stock_value=0;
		foreach($sql as $result)
		{
			$before_prod_id 	= $result[csf("id")];
 			$before_stock_qnty 	= $result[csf("current_stock")];
			$before_stock_value = $result[csf("stock_value")];
			//before quantity and stock value
			$before_issue_qnty	= $result[csf("cons_quantity")];
			$before_issue_value	= $result[csf("cons_amount")];
		}
		
		//current product ID
		$txt_prod_id = str_replace("'","",$txt_prod_id);
		$txt_issue_qnty = str_replace("'","",$txt_receive_qty);
		$txt_return_value = str_replace("'","",$txt_return_value);
		
		$sql = sql_select("select avg_rate_per_unit,current_stock,stock_value from product_details_master where id=$txt_prod_id and item_category_id not in (1,2,3,4,5,6,7,12,13,14)");
		$curr_avg_rate=$curr_stock_qnty=$curr_stock_value=0;
		foreach($sql as $result)
		{
			$curr_avg_rate 		= $result[csf("avg_rate_per_unit")];
			$curr_stock_qnty 	= $result[csf("current_stock")];
			$curr_stock_value 	= $result[csf("stock_value")];
		}
		
		//weighted and average rate START here------------------------//
		//product master table data UPDATE START----------------------//		
		$update_array_product	= "last_issued_qnty*current_stock*stock_value*updated_by*update_date";
		if($before_prod_id==$txt_prod_id)
		{
			$adj_stock_qnty = $curr_stock_qnty+$before_issue_qnty-$txt_issue_qnty; // CurrentStock + Before Issue Qnty - Current Issue Qnty
			$adj_stock_val  = $curr_stock_value+$before_issue_value-$txt_return_value; // CurrentStockValue + Before Issue Value - Current Issue Value
			//$adj_avgrate	= number_format($adj_stock_val/$adj_stock_qnty,$dec_place[3],'.','');
			 
			$data_array_product	= "".$txt_issue_qnty."*".$adj_stock_qnty."*".number_format($adj_stock_val,$dec_place[4],'.','')."*'".$user_id."'*'".$pc_date_time."'";
 			//$query1 		= sql_update("product_details_master",$update_array_product,$data_array_product,"id",$before_prod_id,0);
			
			//now current stock
			$curr_avg_rate 		= $adj_avgrate;
			$curr_stock_qnty 	= $adj_stock_qnty;
			$curr_stock_value 	= $adj_stock_val;
		}
		else
		{
			$updateID_array = $update_data = array();
			//before product adjust
			$adj_before_stock_qnty 	= $before_stock_qnty+$before_issue_qnty; // CurrentStock + Before Issue Qnty
			$adj_before_stock_val  	= $before_stock_value+$before_issue_value; // CurrentStockValue + Before Issue Value
			$adj_before_avgrate		= number_format($adj_before_stock_val/$adj_before_stock_qnty,$dec_place[3],'.','');
			 
			$updateID_array[]=$before_prod_id;
			$update_data[$before_prod_id]=explode("*",("".$txt_issue_qnty."*".$adj_before_stock_qnty."*".number_format($adj_before_stock_val,$dec_place[4],'.','')."*'".$user_id."'*'".$pc_date_time."'"));
  			//$query1 		= sql_update("product_details_master",$field_array,$data_array,"id",$before_prod_id,1);
			
			//current product adjust
			$adj_curr_stock_qnty = 	$curr_stock_qnty-$txt_issue_qnty; // CurrentStock + Before Issue Qnty
			$adj_curr_stock_val  = 	$curr_stock_value-$txt_return_value; // CurrentStockValue + Before Issue Value
			$adj_curr_avgrate	 =	number_format($adj_curr_stock_val/$adj_curr_stock_qnty,$dec_place[3],'.','');
			
			$updateID_array[]=$txt_prod_id;
			$update_data_product[$txt_prod_id]=explode("*",("".$txt_issue_qnty."*".$adj_curr_stock_qnty."*".number_format($adj_curr_stock_val,$dec_place[4],'.','')."*'".$user_id."'*'".$pc_date_time."'"));
			//$query1=execute_query(bulk_update_sql_statement("product_details_master","id",$update_array_product,$update_data_product,$updateID_array),0);
			
			//now current stock
			$curr_avg_rate 		= $adj_curr_avgrate;
			$curr_stock_qnty 	= $adj_curr_stock_qnty;
			$curr_stock_value 	= $adj_curr_stock_val;
		}
		
		
  		//------------------ product_details_master END--------------//
		//weighted and average rate END here-------------------------//
		//transaction table START--------------------------//
		$update_id = str_replace("'","",$update_id);
		$update_array_trans = "balance_qnty*balance_amount*updated_by*update_date";
		$sql = sql_select("select a.id,a.balance_qnty,a.balance_amount,b.issue_qnty,b.rate,b.amount from inv_transaction a, inv_mrr_wise_issue_details b where a.id=b.recv_trans_id and b.issue_trans_id=$update_id and b.entry_form=26 and a.item_category not in (1,2,3,4,5,6,7,12,13,14)"); 
		$updateID_array = array();
		$update_data = array();
		foreach($sql as $result)
		{
			$adjBalance = $result[csf("balance_qnty")]+$result[csf("issue_qnty")];
			$adjAmount = $result[csf("balance_amount")]+$result[csf("amount")];
			$updateID_array[]=$result[csf("id")]; 
			$update_data_trans[$result[csf("id")]]=explode("*",("".$adjBalance."*".$adjAmount."*'".$user_id."'*'".$pc_date_time."'"));
		}
/*		$query2=true; 
		if(count($updateID_array)>0)
		{
 			$query2=execute_query(bulk_update_sql_statement("inv_transaction","id",$update_array_trans,$update_data_trans,$updateID_array),1);
		}
*/		//transaction table END----------------------------//
		//LIFO/FIFO  START here------------------------//
/*		$query3=true;
		if(count($updateID_array)>0)
		{
			 $updateIDArray = implode(",",$updateID_array);
			 $query3 = execute_query("DELETE FROM inv_mrr_wise_issue_details WHERE issue_trans_id=$update_id and entry_form=26",1);
		} 
*/		 //****************************************** BEFORE ENTRY ADJUST END *****************************************//
		
		//############## SAVE POINT START  ###################
		$savepoint="updatesql";
		mysql_query("SAVEPOINT $savepoint");
		//############## SAVE POINT END    ###################
				
  		$id=return_field_value("id","inv_issue_master","issue_number=$txt_mrr_retrun_no");
		//General Item master table UPDATE here START----------------------//		
		$field_array_receive="entry_form*company_id*supplier_id*issue_date*challan_no*received_id*received_mrr_no*updated_by*update_date";
		$data_array_receive="26*".$cbo_company_name."*".$cbo_return_to."*".$txt_receive_date."*".$txt_challan_no."*".$txt_received_id."*".$txt_mrr_no."*'".$user_id."'*'".$pc_date_time."'";
		//echo $field_array."<br>".$data_array;die;
		//$rID=sql_update("inv_issue_master",$field_array_receive,$data_array_receive,"id",$id,1);
		
		
		//General Item master table UPDATE here END---------------------------------------//	 

	//transaction table insert here START--------------------------------//
		//$transaction_type=array(1=>"Receive",2=>"Issue",3=>"Receive Return",4=>"Issue Return");
		$txt_return_qnty = str_replace("'","",$txt_receive_qty);
		$txt_rate = str_replace("'","",$txt_return_rate);
 		$issue_stock_value = str_replace("'","",$txt_return_value);
 		$field_array_trans = "company_id*supplier_id*prod_id*item_category*transaction_type*transaction_date*store_id*cons_uom*cons_quantity*cons_rate*cons_amount*updated_by*update_date";
 		$data_array_trans = "".$cbo_company_name."*".$cbo_return_to."*".$txt_prod_id."*".$category."*3*".$txt_receive_date."*".$store."*".$uom."*".$txt_return_qnty."*".$txt_rate."*".$issue_stock_value."*'".$user_id."'*'".$pc_date_time."'"; 
		//echo $field_array."<br>".$data_array;die;
 		//$transID = sql_update("inv_transaction",$field_array_trans,$data_array_trans,"id",$update_id,1);
		
		//transaction table insert here END ---------------------------------//
		
		//if LIFO/FIFO then START -----------------------------------------//
		$field_array = "id,recv_trans_id,issue_trans_id,entry_form,prod_id,issue_qnty,rate,amount,inserted_by,insert_date";
		$update_array = "balance_qnty*balance_amount*updated_by*update_date";
		$cons_rate=0; 
		$data_array="";
		$updateID_array=array();
		$update_data=array();
		$issueQnty = $txt_return_qnty;
		$mrrWiseIsID = return_next_id("id", "inv_mrr_wise_issue_details", 1);  
		
		$isLIFOfifo=return_field_value("store_method","variable_settings_inventory","company_name=$cbo_company_name and variable_list=16","store_method");
		if($isLIFOfifo==2) $cond_lifofifo=" DESC"; else $cond_lifofifo=" ASC"; 
		
		$sql = sql_select("select id,cons_rate,balance_qnty,balance_amount from inv_transaction where id=$transaction_id and prod_id=$txt_prod_id and balance_qnty>0 and transaction_type in(1,4,5) and item_category=$category order by transaction_date $cond_lifofifo");			
		foreach($sql as $result)
		{				
			$issue_trans_id = $result[csf("id")]; // this row will be updated
			$balance_qnty = $result[csf("balance_qnty")];
			$balance_amount = $result[csf("balance_amount")];
			$cons_rate = $result[csf("cons_rate")]; 
			$issueQntyBalance = $balance_qnty-$issueQnty; // minus issue qnty
			$issueStockBalance = $balance_amount-($issueQnty*$cons_rate);
			if($issueQntyBalance>=0)
			{					
				$amount = $issueQnty*$cons_rate;
				//for insert
				if($data_array!="") $data_array .= ",";  
				$data_array .= "(".$mrrWiseIsID.",".$issue_trans_id.",".$update_id.",26,".$txt_prod_id.",".$issueQnty.",".$cons_rate.",".$amount.",'".$user_id."','".$pc_date_time."')";
				//for update
				$updateID_array[]=$issue_trans_id; 
				$update_data[$issue_trans_id]=explode("*",("".$issueQntyBalance."*".$issueStockBalance."*'".$user_id."'*'".$pc_date_time."'"));
				break;
			}
			else if($issueQntyBalance<0)
			{
				$issueQntyBalance  = $issueQnty-$balance_qnty;				
				$issueQnty = $balance_qnty;				
				$amount = $issueQnty*$cons_rate;
				//for insert
				if($data_array!="") $data_array .= ",";  
				$data_array .= "(".$mrrWiseIsID.",".$issue_trans_id.",".$update_id.",26,".$txt_prod_id.",".$issueQnty.",".$cons_rate.",".$amount.",'".$user_id."','".$pc_date_time."')";
				//echo "20**".$data_array;die;
				//for update
				$updateID_array[]=$issue_trans_id; 
				$update_data[$issue_trans_id]=explode("*",("0*0*'".$user_id."'*'".$pc_date_time."'"));
				$issueQnty = $issueQntyBalance;
			}
			$mrrWiseIsID++;
		}//end foreach 
		
		
		if($before_prod_id==$txt_prod_id)
		{
 			$query1 = sql_update("product_details_master",$update_array_product,$data_array_product,"id",$before_prod_id,1);
		}
		else
		{
			$query1=execute_query(bulk_update_sql_statement("product_details_master","id",$update_array_product,$update_data_product,$updateID_array),1);
		}
		
		$query2=true; 
		if(count($updateID_array)>0)
		{
 			$query2=execute_query(bulk_update_sql_statement("inv_transaction","id",$update_array_trans,$update_data_trans,$updateID_array),1);
		}
		$query3=true;
		if(count($updateID_array)>0)
		{
			 $updateIDArray = implode(",",$updateID_array);
			 $query3 = execute_query("DELETE FROM inv_mrr_wise_issue_details WHERE issue_trans_id=$update_id and entry_form=26",1);
		} 
		$rID=sql_update("inv_issue_master",$field_array_receive,$data_array_receive,"id",$id,1);
 		$transID = sql_update("inv_transaction",$field_array_trans,$data_array_trans,"id",$update_id,1);
		//mrr wise issue data insert here----------------------------//
		$mrrWiseIssueID=true;
		if($data_array!="")
		{		
			$mrrWiseIssueID=sql_insert("inv_mrr_wise_issue_details",$field_array,$data_array,1);
			
		}
		$upTrID=true;
		if(count($updateID_array)>0)
		{
 			$upTrID=execute_query(bulk_update_sql_statement("inv_transaction","id",$update_array,$update_data,$updateID_array));
		}
		$serialUpdate=true;
		$serialDelete=true;
		$txt_serial_id 	= trim(str_replace("'","",$txt_serial_id));
		//print_r($txt_serial_id);die;
		
 		if($txt_serial_id!="")
		{
			$before_serial_id=trim(str_replace("'","",$before_serial_id));$txt_serial_id=trim(str_replace("'","",$txt_serial_id));$update_id=trim(str_replace("'","",$update_id));
			if( strpos(trim($txt_serial_no), ",")>0)
			{
				$se_data=explode(",",str_replace("'","",$txt_serial_no));
				if( (count($se_data)<=str_replace("'","",$txt_receive_qty)))
				{
					if($before_serial_id !="")
					{
						$serialDelete=execute_query("update inv_serial_no_details set rev_rtn_trans_id=0 , is_issued=0 where id in ($before_serial_id)",1);
					}
					else
					{
						$serialUpdate = execute_query("update inv_serial_no_details set rev_rtn_trans_id=$update_id , is_issued=1 where id in ($txt_serial_id)",1);
					}
				}
				else
				{
					echo "50";
					die;
				}
			}
			else
			{
				if($before_serial_id !="")
				{
					$serialDelete	=execute_query("update inv_serial_no_details set rev_rtn_trans_id=0 , is_issued=0 where id in ($before_serial_id)",1);
				}
				else
				{
					$serialUpdate 	= execute_query("update inv_serial_no_details set rev_rtn_trans_id=$update_id , is_issued=1 where id in ($txt_serial_id)");
				}
			}
		}
		
		if($db_type==0)
		{
			if($query1 && $query2 && $query3 && $rID && $transID  && $mrrWiseIssueID && $upTrID  && $serialUpdate && $serialDelete)
			{
				mysql_query("COMMIT");  
				echo "1**".str_replace("'","",$txt_mrr_retrun_no);
			}
			else
			{
				mysql_query("ROLLBACK"); 
				mysql_query("ROLLBACK TO $savepoint");
				echo "10**".str_replace("'","",$txt_mrr_retrun_no);
			}
		}
		if($db_type==2 || $db_type==1 )
		{
			if($query1 && $query2 && $query3 && $rID && $transID && $mrrWiseIssueID && $upTrID  && $serialUpdate && $serialDelete)
			{
				oci_commit($con);
				echo "1**".str_replace("'","",$txt_mrr_retrun_no)."**".$id;
			}
			else
			{
				echo "10**".str_replace("'","",$txt_mrr_retrun_no)."**".$id;
			}
		}
		disconnect($con);
		die;
 	}
	else if ($operation==2) // Delete Here----------------------------------------------------------
	{
		 //no operation
	}
}
/* Return ID List View Action*/
if($action=="return_number_popup")
{
  	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	extract($_REQUEST);  
?>
     
<script>
	function js_set_value(mrr)
	{
 		$("#hidden_return_number").val(mrr); // mrr number
		parent.emailwindow.hide();
	}
</script>
</head>
<body>
<div align="center" style="width:100%;" >
<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
	<table width="880" cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
            <thead>
                <tr>                	 
                    <th width="250" align="center" id="search_by_td_up">Enter Return Number</th>
                    <th width="200">Date Range</th>
                    <th><input type="reset" name="re_button" id="re_button" value="Reset" style="width:100px" class="formbutton"  /></th>           
                </tr>
            </thead>
            <tbody>
                <tr>                    
                    <td width="" align="center" id="search_by_td">				
                        <input type="text" style="width:230px" class="text_boxes"  name="txt_search_common" id="txt_search_common" />	
                    </td>    
                    <td align="center">
                        <input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px" placeholder="From Date" />
                        <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px" placeholder="To Date" />
                    </td> 
                    <td align="center">
                        <input type="button" name="btn_show" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('txt_search_common').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+<?php echo $company; ?>, 'create_return_search_list_view', 'search_div', 'general_item_receive_return_entry_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:100px;" />				
                    </td>
            </tr>
        	<tr>                  
            	<td align="center" height="40" valign="middle" colspan="5">
					<?php echo load_month_buttons(1);  ?>
                     <input type="hidden" id="hidden_return_number" value="" />
                </td>
            </tr>    
            </tbody>
         </tr>         
        </table>    
        <div align="center" valign="top" id="search_div"> </div> 
        </form>
   </div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}
if($action=="create_return_search_list_view")
{
	$ex_data = explode("_",$data);
	$search_by = $ex_data[0];
	$search_common = $ex_data[0];
	$txt_date_from = $ex_data[1];
	$txt_date_to = $ex_data[2];
	$company = $ex_data[3];
	
	$sql_cond="";	 
	if( $txt_date_from!="" || $txt_date_to!="" ) $sql_cond .= " and issue_date  between '".change_date_format($txt_date_from,'yyyy-mm-dd')."' and '".change_date_format($txt_date_to,'yyyy-mm-dd')."'";
	if($company!="") $sql_cond .= " and company_id='$company'";
	if($search_common!="") $sql_cond .= "and issue_number_prefix_num='$search_common'";
	
	$sql = "select id,issue_number_prefix_num,issue_number,company_id,supplier_id,issue_date,item_category,received_id,received_mrr_no   
			from inv_issue_master 
			where status_active=1 and entry_form=26 $sql_cond";
	//echo $sql;
	$company_arr = return_library_array("select id,company_name from lib_company","id","company_name");
	$supplier_arr=return_library_array( "select id, supplier_name from lib_supplier",'id','supplier_name');
	$arr=array(1=>$company_arr,2=>$supplier_arr);
 	echo create_list_view("list_view", "Return No, Company Name, Returned To, Return Date, Receive MRR","150,150,120,120,150","850","260",0, $sql , "js_set_value", "issue_number,id", "", 1, "0,company_id,supplier_id,0,0", $arr, "issue_number_prefix_num,company_id,supplier_id,issue_date,received_mrr_no","","",'0,0,0,3,0') ;	
 	exit();
}
if($action=="populate_master_from_data")
{  
	$sql = "select id,issue_number,company_id,supplier_id,issue_date,challan_no,item_category,received_id,received_mrr_no   
			from inv_issue_master 
			where id='$data' and entry_form=26";
	//echo $sql;
	$res = sql_select($sql);
	foreach($res as $row)
	{
		echo "$('#txt_mrr_retrun_no').val('".$row[csf("issue_number")]."');\n";
 		echo "$('#cbo_company_name').val(".$row[csf("company_id")].");\n";
 		echo "$('#cbo_return_to').val('".$row[csf("supplier_id")]."');\n";
		echo "$('#txt_challan_no').val('".$row[csf("challan_no")]."');\n";
		echo "$('#txt_receive_date').val('".change_date_format($row[csf("issue_date")])."');\n";
		echo "$('#txt_mrr_no').val('".$row[csf("received_mrr_no")]."');\n";
		echo "$('#txt_received_id').val('".$row[csf("received_id")]."');\n";
		//right side list view
		
 		echo "set_button_status(1, permission, 'fnc_general_receive_return_entry',1,1);\n";
	
		$sql = sql_select("select b.id as tr_id, c.id as prod_id, a.id as rec_id
		from inv_receive_master a, inv_transaction b, product_details_master c 
		where a.id=b.mst_id and b.prod_id=c.id and a.recv_number='".$row[csf("received_mrr_no")]."' and b.transaction_type=1 and b.item_category not in (1,2,3,4,5,6,7,12,13,14)");
		foreach($sql as $row_t)
		{
			echo "$('#transaction_id').val('".$row_t[csf("tr_id")]."');\n";
			echo"show_list_view('".$row_t[csf("rec_id")]."','show_product_listview','list_product_container','requires/general_item_receive_return_entry_controller','');\n";
		}
   	}	
	exit();	
}
/*After Save List View*/
if($action=="show_dtls_list_view")
{
	$ex_data = explode("**",$data);
	$return_number = $ex_data[0];
	$ret_mst_id = str_replace("'","",$ex_data[1] );
	//echo $ret_mst_id;die;
	$cond="";
	if($ret_mst_id!="") $cond .= " and a.id='$ret_mst_id'";
	$item_name_arr=return_library_array("select id,item_name from lib_item_group", "id","item_name");

	$sql = "select a.issue_number,a.company_id,a.supplier_id,a.issue_date,a.received_id,a.received_mrr_no,b.id,b.item_category,b.prod_id,b.cons_quantity,b.cons_uom,b.cons_rate,b.cons_amount,c.item_description,c.item_group_id from inv_issue_master a, inv_transaction b,product_details_master c 
			where a.id=b.mst_id and b.prod_id=c.id and b.transaction_type=3 $cond ";
	//echo $sql;
	$result = sql_select($sql);
	$i=1;
	$rettotalQnty=0;
	$rcvtotalQnty=0;
	$totalAmount=0;
	?> 
     	<table class="rpt_table" border="1" cellpadding="2" cellspacing="0" style="width:850px" >
        	<thead>
            	<tr>
                	<th>SL</th>
                    <th>Item Category</th>
                    <th>Item Group</th>
                    <th>Item Description</th>
                    <th>Returned Qty.</th>
                    <th>UOM</th>
                    <th>Rate</th>
                    <th>Return Value</th> 
                </tr>
            </thead>
            <tbody>
            	<?php 
				foreach($result as $row){					
					if($i%2==0)
						$bgcolor="#E9F3FF";
					else 
						$bgcolor="#FFFFFF";
					$pro_id=$row[csf("prod_id")];
					//echo "select b.balance_qnty,b.cons_quantity from inv_receive_master a, inv_transaction b where a.id=b.mst_id and b.prod_id='$pro_id' and b.transaction_type=1 and b.item_category not in (1,2,3,4,5,6,7,12,13,14) and  a.recv_number='".$row[csf("received_mrr_no")]."'";
					$sqlTr = sql_select("select a.id, b.balance_qnty,b.cons_quantity from inv_receive_master a, inv_transaction b where a.id=b.mst_id and b.prod_id='$pro_id' and b.transaction_type=1 and b.item_category not in (1,2,3,4,5,6,7,12,13,14) and  a.recv_number='".$row[csf("received_mrr_no")]."'");
					$rcvQnty = $sqlTr[0][csf('balance_qnty')];
					
					$total_cons_quantity=$sqlTr[0][csf('cons_quantity')];
					//$rcvtotalQnty +=$rcvQnty;
					$totalAmount +=$row[csf("cons_amount")];		
 				?>
                	<tr bgcolor="<?php echo $bgcolor; ?>" onClick='get_php_form_data("<?php echo $row[csf("id")];?>,<?php echo $rcvQnty;?>,<?php echo $total_cons_quantity;?>","child_form_input_data","requires/general_item_receive_return_entry_controller")' style="cursor:pointer" >
                        <td width="30"><?php echo $i; ?></td>
                        <td width="100"><p><?php echo $item_category[$row[csf("item_category")]]; ?></p></td>
                        <td width="200"><p><?php echo $item_name_arr[$row[csf("item_group_id")]]; ?></p></td>
                        <td width="80"><p><?php echo $row[csf("item_description")]; ?></p></td>
                        <td width="70" align="right"><p><?php echo $row[csf("cons_quantity")]; ?></p></td>
                        <td width="70"><p><?php echo $unit_of_measurement[$row[csf("cons_uom")]]; ?></p></td>
                        <td width="70" align="right"><p><?php echo $row[csf("cons_rate")]; ?></p></td>
                        <td width="70" align="right"><p><?php echo $row[csf("cons_amount")]; ?></p></td>
                   </tr>
                <?php $i++; } ?>
                	<tfoot>
                        <th colspan="4">Total</th>                         
                        <th><?php echo number_format($rettotalQnty,0,'',',')  //$total_order_qnty; ?></th> 
                        <th colspan="2"></th>
                        <th><?php echo number_format($totalAmount,0,'',','); ?></th>
                   </tfoot>
            </tbody>
        </table>
    <?php
	exit();
}
if($action=="child_form_input_data")
{
	$ex_data = explode(",",$data);
	$data2 = $ex_data[0]; 	// transaction id
	$rcvQnty = $ex_data[1];
	$total_cons_quantity= $ex_data[2]; 
 	$sql = "select b.id as prod_id, b.item_description,b.item_group_id, a.id as tr_id, a.item_category, a.store_id, a.cons_uom, a.cons_rate, a.cons_quantity, a.cons_amount
			from inv_transaction a, product_details_master b
 			where a.id=$data2 and a.status_active=1 and transaction_type=3 and a.item_category not in (1,2,3,4,5,6,7,12,13,14) and a.prod_id=b.id and b.status_active=1";
 	//echo $sql;die;
	$store_name=return_library_array("select id,store_name from lib_store_location", "id","store_name"); 
	$item_name_arr=return_library_array("select id,item_name from lib_item_group", "id","item_name");
	$result = sql_select($sql);
	foreach($result as $row)
	{
		$rcvQnty=$rcvQnty+$row[csf("cons_quantity")];
 		echo "$('#txt_item_category').val('".$item_category[$row[csf("item_category")]]."');\n";
		echo "$('#category').val('".$row[csf("item_category")]."');\n";
		echo "$('#txt_store_name').val('".$store_name[$row[csf("store_id")]]."');\n";
		echo "$('#store').val('".$row[csf("store_id")]."');\n";
		echo "$('#txt_prod_id').val('".$row[csf("prod_id")]."');\n";
		echo "$('#before_prod_id').val('".$row[csf("prod_id")]."');\n";
		echo "$('#txt_item_group').val('".$item_name_arr[$row[csf("item_group_id")]]."');\n";
		echo "$('#txt_item_description').val('".$row[csf("item_description")]."');\n";
		echo "$('#txt_receive_qty').val('".$row[csf("cons_quantity")]."');\n";	
		
		echo "$('#txt_curr_stock').val('".$rcvQnty."');\n";
		echo "$('#txt_cons_quantity').val('".$total_cons_quantity."');\n";
		
		echo "$('#txt_uom').val('".$unit_of_measurement[$row[csf("cons_uom")]]."');\n";
		echo "$('#uom').val('".$row[csf("cons_uom")]."');\n";
		echo "$('#txt_return_rate').val('".$row[csf("cons_rate")]."');\n";		
		echo "$('#txt_return_value').val(".$row[csf("cons_amount")].");\n";
		echo "$('#update_id').val(".$row[csf("tr_id")].");\n";
		$serialString = return_field_value("group_concat(serial_no) as sr","inv_serial_no_details","rev_rtn_trans_id=".$row[csf("tr_id")]." group by rev_rtn_trans_id","sr");
		echo "$('#txt_serial_no').val('".$serialString."');\n";
		$serialStringId = return_field_value("group_concat(id) as id","inv_serial_no_details","rev_rtn_trans_id=".$row[csf("tr_id")]." group by rev_rtn_trans_id","id");
		echo "$('#txt_serial_id').val('".$serialStringId."');\n";
		echo "$('#before_serial_id').val('".$serialStringId."');\n";
	}
	echo "set_button_status(1, permission, 'fnc_general_receive_return_entry',1,1);\n";
	echo "$('#cbo_company_name').attr('disabled',true);\n";
	echo "$('#txt_mrr_no').attr('disabled',true);\n";
  	exit();
}

if ($action=="general_item_receive_return_print")
{
    extract($_REQUEST);
	$data=explode('*',$data);
	//print_r ($data);
	
	$sql=" select id, issue_number, issue_date, received_id, challan_no, supplier_id from  inv_issue_master where received_mrr_no='$data[1]' and company_id='$data[0]' and item_category not in (1,2,3,4,5,6,7,12,13,14)";//and item_category in (8,9,10,11,15,16,17,18,19,20,21,22)
	//echo $sql;
	$dataArray=sql_select($sql);

	$company_library=return_library_array( "select id, company_name from lib_company", "id", "company_name"  );
	$supplier_library=return_library_array( "select id,supplier_name from  lib_supplier", "id","supplier_name"  );
	$store_library=return_library_array( "select id, store_name from  lib_store_location", "id", "store_name"  );
	$country_arr=return_library_array( "select id, country_name from  lib_country", "id", "country_name"  );
	$rec_id_arr=return_library_array( "select id, recv_number from  inv_receive_master", "id", "recv_number"  );
	
?>
<div style="width:930px;">
    <table width="900" cellspacing="0" align="right">
        <tr>
            <td colspan="6" align="center" style="font-size:xx-large"><strong><?php echo $company_library[$data[0]]; ?></strong></td>
        </tr>
        <tr class="form_caption">
        	<td colspan="6" align="center" style="font-size:14px">  
				<?php
					$nameArray=sql_select( "select plot_no,level_no,road_no,block_no,country_id,province,city,zip_code,email,website from lib_company where id=$data[0]"); 
					foreach ($nameArray as $result)
					{ 
					?>
						Plot No: <?php echo $result['plot_no']; ?> 
						Level No: <?php echo $result['level_no']?>
						Road No: <?php echo $result['road_no']; ?> 
						Block No: <?php echo $result['block_no'];?> 
						City No: <?php echo $result['city'];?> 
						Zip Code: <?php echo $result['zip_code']; ?> 
						Province No: <?php echo $result['province'];?> 
						Country: <?php echo $country_arr[$result['country_id']]; ?><br> 
						Email Address: <?php echo $result['email'];?> 
						Website No: <?php echo $result['website'];
					}
                ?> 
            </td>  
        </tr>
        <tr>
            <td colspan="6" align="center" style="font-size:x-large"><strong><u><?php echo $data[2]; ?> Reprot</u></strong></td>
        </tr>
        <tr>
        	<td width="120"><strong>Return ID:</strong></td><td width="175px"><?php echo $dataArray[0][csf('issue_number')]; ?></td>
            <td width="100"><strong>Return Date :</strong></td> <td width="230px"><?php echo change_date_format($dataArray[0][csf('issue_date')]); ?></td>
            <td width="100"><strong>Receive ID:</strong></td><td width="175px"><?php echo $rec_id_arr[$dataArray[0][csf('received_id')]]; ?></td>
        </tr>
        <tr>
            <td><strong>Challan No:</strong></td> <td width="175px"><?php echo $dataArray[0][csf('challan_no')]; ?></td>
            <td><strong>Returned To:</strong></td><td width="230px"><?php echo $supplier_library[$dataArray[0][csf('supplier_id')]]; ?></td>
            <td><strong>&nbsp;</strong></td><td width="175px"><?php //echo $store_library[$dataArray[0][csf('store_id')]]; ?></td>
        </tr>
    </table>
    <br>
	<div style="width:100%;">
		<table align="right" cellspacing="0" width="900"  border="1" rules="all" class="rpt_table" >
            <thead bgcolor="#dddddd" align="center">
                <th width="40">SL</th>
                <th width="80" align="center">Item Category</th>
                <th width="150" align="center">Item Group</th>
                <th width="200" align="center">Item Description</th>
                <th width="50" align="center">UOM</th> 
                <th width="80" align="center">Returned. Qnty.</th>
                <th width="50" align="center">Rate</th>
                <th width="70" align="center">Return Value</th>
                <th width="80" align="center">Store</th> 
            </thead>
<?php
	$i=1;
	$item_name_arr=return_library_array( "select id, item_name from  lib_item_group", "id", "item_name"  );
	if($db_type==0)
	{
		$sql_dtls= "select a.id, a.item_category, b.cons_uom, b.cons_quantity, b.cons_rate, b.cons_amount, b.store_id, c.item_group_id, concat(c.sub_group_name,' ',c.item_description,' ',c.item_size ) as product_name_details from  inv_issue_master a, inv_transaction b,  product_details_master c where a.issue_number='$data[1]' and a.company_id='$data[0]' and a.id=b.mst_id and b.prod_id=c.id and b.transaction_type=3 and b.item_category not in (1,2,3,4,5,6,7,12,13,14) and a.entry_form=26 ";
	}
	else
	{
		$sql_dtls= "select a.id, a.item_category, b.cons_uom, b.cons_quantity, b.cons_rate, b.cons_amount, b.store_id, c.item_group_id,(c.sub_group_name || ' ' || c.item_description || ' ' || c.item_size ) as product_name_details from  inv_issue_master a, inv_transaction b,  product_details_master c where a.issue_number='$data[1]' and a.company_id='$data[0]' and a.id=b.mst_id and b.prod_id=c.id and b.transaction_type=3 and b.item_category not in (1,2,3,4,5,6,7,12,13,14) and a.entry_form=26 ";
	}
	//echo $sql_dtls;die;
	$sql_result=sql_select($sql_dtls);
	foreach($sql_result as $row)
	{
		if ($i%2==0)  
			$bgcolor="#E9F3FF";
		else
			$bgcolor="#FFFFFF";
			$cons_quantity=$row[csf('cons_quantity')];
			$cons_quantity_sum += $cons_quantity;
			
			$cons_amount=$row[csf('cons_amount')];
			$cons_amount_sum += $cons_amount;
			
			$desc=$row[csf('item_description')];
			
			if($row[csf('item_size')]!="")
			{
				$desc.=", ".$row[csf('item_size')];
			}
		?>
			<tr bgcolor="<?php echo $bgcolor; ?>">
                <td><?php echo $i; ?></td>
                <td><?php echo $item_category[$row[csf('item_category')]]; ?></td>
                <td><?php echo $item_name_arr[$row[csf('item_group_id')]]; ?></td>
                <td><?php echo $row[csf('product_name_details')]; ?></td>
                <td align="center"><?php echo $unit_of_measurement[$row[csf('cons_uom')]]; ?></td>
                <td align="right"><?php echo number_format($row[csf('cons_quantity')],0,'',','); ?></td>
                <td align="right"><?php echo number_format($row[csf('cons_rate')],0,'',','); ?></td>
                <td align="right"><?php echo number_format($row[csf('cons_amount')],0,'',','); ?></td>
                <td><?php echo $store_library[$row[csf('store_id')]]; ?></td>
			</tr>
			<?php
			$i++;
			}
			?>
        	<tr> 
                <td align="right" colspan="5" >Total</td>
                <td align="right"><?php echo number_format($cons_quantity_sum,0,'',','); ?></td>
                <td align="right" colspan="2" ><?php echo number_format($cons_amount_sum,0,'',','); ?></td>
                <td align="right">&nbsp;</td>
			</tr>
		</table> 
        <br>
		 <?php
            echo signature_table(13, $data[0], "900px");
         ?>
      </div>
   </div> 
 <?php 
 exit();  
 }
?>