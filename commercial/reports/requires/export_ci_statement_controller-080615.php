<?
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../../../includes/common.php');

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];
$user_id = $_SESSION['logic_erp']["user_id"];


$user_arr = return_library_array("select id,user_name from user_passwd ","id","user_name");
$bank_arr=return_library_array("select id,bank_name from   lib_bank","id","bank_name");
$company_arr=return_library_array( "select id, company_name from lib_company",'id','company_name');
$location_arr=return_library_array("select id,location_name from  lib_location","id","location_name");
$buyer_arr=return_library_array("select id,buyer_name from  lib_buyer","id","buyer_name");
// $ref_field=return_field_value("a.style_ref_no","wo_po_details_master a, wo_po_break_down b,  com_export_invoice_ship_dtls c","a.job_no=b.job_no_mst and b.id=c.po_breakdown_id and c.mst_id=".$row_result[csf('id')],"style_ref_no");
if($db_type==0)
{
$style_ref_arr=return_library_array("select c.mst_id,group_concat(distinct a.style_ref_no) as style_ref_no from  wo_po_details_master a, wo_po_break_down b,  com_export_invoice_ship_dtls c where a.job_no=b.job_no_mst and b.id=c.po_breakdown_id and c.current_invoice_qnty>0  group by  c.mst_id","mst_id","style_ref_no");
}
else
{
	//$style_ref_arr=return_library_array("select c.mst_id,LISTAGG(CAST(a.style_ref_no AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY a.style_ref_no) as style_ref_no from  wo_po_details_master a, wo_po_break_down b,  com_export_invoice_ship_dtls c where a.job_no=b.job_no_mst and b.id=c.po_breakdown_id  group by  c.mst_id","mst_id","style_ref_no");
	$sql=sql_select("select c.mst_id,LISTAGG(CAST(a.style_ref_no AS VARCHAR(4000)), ',') WITHIN GROUP (ORDER BY a.style_ref_no) as style_ref_no from  wo_po_details_master a, wo_po_break_down b,  com_export_invoice_ship_dtls c where a.job_no=b.job_no_mst and b.id=c.po_breakdown_id and c.current_invoice_qnty>0  group by  c.mst_id");
	foreach($sql as $row)
	{
		$style=implode(",",array_unique(explode(",",$row[csf('style_ref_no')])));
		$style_ref_arr[$row[csf('mst_id')]]=$style;
	}
}
//var_dump($style_ref_arr);die;
//$curier_receipt_date=return_field_value("a.courier_date","com_export_doc_submission_mst a,com_export_doc_submission_invo b","a.id=b.doc_submission_mst_id and b.invoice_id=".$row_result[csf("id")]."","courier_date");
$currier_date_arr=return_library_array("select b.invoice_id,a.courier_date from  com_export_doc_submission_mst a,com_export_doc_submission_invo b where a.id=b.doc_submission_mst_id ","invoice_id","courier_date");

//$curier_submit_date_arr=return_library_array("a.submit_date","com_export_doc_submission_mst a,com_export_doc_submission_invo b","a.id=b.doc_submission_mst_id and b.invoice_id=".$row_result[csf('id')]."","submit_date");

$curier_submit_date_arr=return_library_array(" select b.invoice_id,a.submit_date from com_export_doc_submission_mst a,com_export_doc_submission_invo b where a.id=b.doc_submission_mst_id","invoice_id","submit_date");
//$bnk_to_bnk_cour_no=return_field_value("a.bnk_to_bnk_cour_no","com_export_doc_submission_mst a,com_export_doc_submission_invo b","a.id=b.doc_submission_mst_id and b.invoice_id=".$row_result[csf('id')],"bnk_to_bnk_cour_no");
$bnk_to_bnk_cour_no_arr=return_library_array(" select b.invoice_id,a.bnk_to_bnk_cour_no from com_export_doc_submission_mst a,com_export_doc_submission_invo b where a.id=b.doc_submission_mst_id","invoice_id","bnk_to_bnk_cour_no");
//$bank_ref_no=return_field_value("a.bank_ref_no","com_export_doc_submission_mst a,com_export_doc_submission_invo b","a.id=b.doc_submission_mst_id and b.invoice_id=".$row_result[csf('id')],"bank_ref_no");
$bank_ref_no_arr=return_library_array(" select b.invoice_id,a.bank_ref_no from com_export_doc_submission_mst a,com_export_doc_submission_invo b where a.id=b.doc_submission_mst_id","invoice_id","bank_ref_no");
//$bank_date_no=return_field_value("a.bank_ref_date","com_export_doc_submission_mst a,com_export_doc_submission_invo b","a.id=b.doc_submission_mst_id and b.invoice_id=".$row_result[csf('id')],"bank_ref_date");
$bank_date_no_arr=return_library_array(" select b.invoice_id,a.bank_ref_date from com_export_doc_submission_mst a,com_export_doc_submission_invo b where a.id=b.doc_submission_mst_id","invoice_id","bank_ref_date");
//$possible_rlz_date=return_field_value("a.possible_reali_date","com_export_doc_submission_mst a,com_export_doc_submission_invo b","a.id=b.doc_submission_mst_id and b.invoice_id=".$row_result[csf('id')],"possible_reali_date");
$possible_rlz_date_arr=return_library_array(" select b.invoice_id,a.possible_reali_date from com_export_doc_submission_mst a,com_export_doc_submission_invo b where a.id=b.doc_submission_mst_id","invoice_id","possible_reali_date");


if ($action=="load_drop_down_buyer")
{
	echo create_drop_down( "cbo_buyer_name", 155, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' and buy.id in (select buyer_id from lib_buyer_party_type where party_type in (1,3,21,90))  $buyer_cond order by buyer_name","id,buyer_name", 1, "-- All Buyer --", $selected, "" ,0); 
	exit();
}
//Company Details

if($action=="report_generate")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process ));
	 $cbo_based_on=str_replace("'","",$cbo_based_on);
	//echo $cbo_based_on;die;
	?>
<div style="width:4270px">
            <table width="3500" cellpadding="0" cellspacing="0" id="caption">
            <tr>
            <td align="center" width="100%" colspan="20" class="form_caption" ><strong style="font-size:18px">Company Name:<? echo " ". $company_arr[str_replace("'","",$cbo_company_name)]; ?></strong></td>
            </tr> 
            <tr>  
            <td align="center" width="100%" colspan="20" class="form_caption" ><strong style="font-size:18px"><? echo $report_title; ?>(
            <?
			if($cbo_based_on!=0)
			{
			?>
            <strong style="font-size:18px">Based On:<? $based_on_arr=array(1=>"Invoice Date",2=>"Exfactory Date",3=>"Actual Date"); echo " ". $based_on_arr[$cbo_based_on]; ?></strong>
            
            <?
			}
			?>)</strong></td>
            </tr>
            
           
            </table>
    	<br />
    
    
            <table width="4250" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all" id="table_header_1" align="left">
                <thead>
                    <tr>
                        <th width="50">Sl</th>
                        <th width="100">Company Name</th>
                        <th width="100">Location</th>
                        <th width="100">Invoice No.</th>
                        <th width="80">Invoice Date</th>
                        <th width="70">SC/LC</th>
                        <th width="100">SC/LC No.</th>
                        <th width="70">Buyer Name</th>
                        <th width="150">Lien Bank</th>
                        <th width="90">EXP Form No</th>
                        <th width="80">EXP Form Date</th>
                        <th width="100">Grs. Invoice Amount</th>
                        <th width="80">Discount</th>
                        <th width="70">Bonous</th>
                        <th width="70">Claim</th>
                        <th width="80">Commission</th>
                        <th width="100">Net Invoice Amount</th>
                        <th width="80">Currency</th>
                        <th width="80">PO NO</th>
                        <th width="110">Style Ref.</th>
                        <th width="110">Invoice Qnty.</th>
                        <th width="110">Ctn Qnty.</th>
                        <th width="80">Actual Ship Date</th>
                        <th width="100">B/L No</th>
                        <th width="80">B/L Date</th>
                        <th width="50">B/L Days</th>
                        <th width="90">Ship Bl No</th>
                        <th width="80">Ship Bl Date</th>
                        <th width="110">ETD</th>
                        <th width="100">Feeder Vessle</th>
                        <th width="100">Mother Vessle</th>
                        <th width="80">ETA Dest.</th>
                        <th width="100">Courier No(NN Docs)</th>
                        <th width="100">GSP/CO No.</th>
                        <th width="80">GSP/CO Date</th>
                        <th width="80">GSP Cour. Date</th>
                        <th width="80">Org B/L Rcv</th>
                        <th width="80">I/C Rcv Date</th>
                        <th width="80">Ex-Factory Date</th>
                        <th width="70">Document In Hand</th>
                        <th width="80">Doc Sub Date</th>
                        <th width="50">Sub. Days</th>
                        <th width="100">B TO B Courier No</th>
                        <th width="100">Bank Bill No.</th>
                        <th width="80">Bank Bill Date</th>
                        <th width="80">Pay Term</th>
                        <th width="80">Possible Rlz. Date</th>
                        <th width="100">Remarks</th>
                    </tr>
                </thead>
            </table>
            
            <div style="width:4270px; overflow-y:scroll; max-height:250px;font-size:12px; overflow-x:hidden;" id="scroll_body">
            <table width="4250" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all" id="table_body">
                <tbody>
                <?
                $cbo_company_name=str_replace("'","",$cbo_company_name);
                if($cbo_company_name!=0) $cbo_company_name = $cbo_company_name; else $cbo_company_name="%%";
				
                $cbo_buyer_name=str_replace("'","",$cbo_buyer_name);
                if($cbo_buyer_name == 0) $cbo_buyer_name="%%"; else $cbo_buyer_name = $cbo_buyer_name;
				
                $cbo_lien_bank=str_replace("'","",$cbo_lien_bank);
                if($cbo_lien_bank == 0) $cbo_lien_bank="%%"; else $cbo_lien_bank = $cbo_lien_bank;
				//echo  $cbo_lien_bank;die;
                $cbo_location=str_replace("'","",$cbo_location);
                if($cbo_location == 0) $cbo_location="%%"; else $cbo_location = $cbo_location;
				
                $cbo_based_on=str_replace("'","",$cbo_based_on);
                if($cbo_based_on == 0) $cbo_based_on="%%"; else $cbo_based_on = $cbo_based_on;
				
                $txt_date_from=str_replace("'","",$txt_date_from);
                if(trim($txt_date_from)!= "") $txt_date_from  =$txt_date_from;
				
                $txt_date_to=str_replace("'","",$txt_date_to);
                if(trim($txt_date_to)!= "") $txt_date_to = $txt_date_to;
				
                //if(trim($data[7])!="") $cbo_year2=$data[7];
                if ($txt_date_from!="" && $txt_date_to!="")
                {
					if($cbo_based_on ==0)
					{
						$str_cond=" and a.invoice_date between '$txt_date_from' and  '$txt_date_to'";
					}
					if($cbo_based_on ==1)
					{
						$str_cond=" and a.invoice_date between '$txt_date_from' and  '$txt_date_to'";
					}
					if($cbo_based_on ==2)
					{
						$str_cond=" and a.ex_factory_date between '$txt_date_from' and  '$txt_date_to'";
					}
					if($cbo_based_on ==3)
					{
						$str_cond=" and a.actual_shipment_date between '$txt_date_from' and  '$txt_date_to'";
					}
                }
                else
                {
                $str_cond="";
                }
				//echo  $str_cond;die;
				
				if($db_type==0)
				{
					$sql="SELECT a.id, a.benificiary_id, a.location_id, a.invoice_no, a.invoice_date, a.ex_factory_date, a.is_lc, a.buyer_id, a.exp_form_no, a.exp_form_date,a. 	invoice_value,a.discount_ammount,a.bonus_ammount,a.claim_ammount,a.commission, a.net_invo_value, a.invoice_quantity, a.total_carton_qnty, a.actual_shipment_date, a.bl_no, a.bl_date, a.etd, a.feeder_vessel, a.mother_vessel, a.etd_destination, a.bl_rev_date, a.ic_recieved_date, a.remarks, a.gsp_co_no, a.gsp_co_no_date,a.shipping_bill_n,a.ship_bl_date, b.currency_name as currency_name , b.pay_term as pay_term, b.lien_bank as lien_bank, b.export_lc_no as lc_sc_no, 1 as type
					FROM com_export_invoice_ship_mst a, com_export_lc b 
					WHERE a.benificiary_id LIKE '$cbo_company_name' AND a.location_id LIKE '$cbo_location' AND a.buyer_id LIKE '$cbo_buyer_name' AND b.lien_bank LIKE '$cbo_lien_bank' and a.is_lc=1 and a.lc_sc_id=b.id and a.status_active=1  and a.is_deleted=0  and b.status_active=1 and b.is_deleted=0  $str_cond
					 
					UNION ALL
					
					SELECT a.id, a.benificiary_id, a.location_id, a.invoice_no, a.invoice_date, a.ex_factory_date, a.is_lc, a.buyer_id, a.exp_form_no, a.exp_form_date,a. 	invoice_value,a.discount_ammount,a.bonus_ammount,a.claim_ammount,a.commission, a.net_invo_value, a.invoice_quantity, a.total_carton_qnty, a.actual_shipment_date, a.bl_no, a.bl_date, a.etd, a.feeder_vessel, a.mother_vessel, a.etd_destination, a.bl_rev_date, a.ic_recieved_date, a.remarks, a.gsp_co_no, a.gsp_co_no_date,a.shipping_bill_n,a.ship_bl_date, c.currency_name as currency_name, c.pay_term as pay_term, c.lien_bank as lien_bank, c.contract_no as lc_sc_no, 2 as type 
					FROM com_export_invoice_ship_mst a, com_sales_contract c 
					WHERE a.benificiary_id LIKE '$cbo_company_name' AND a.location_id LIKE '$cbo_location' AND a.buyer_id LIKE '$cbo_buyer_name' AND c.lien_bank LIKE '$cbo_lien_bank' and a.is_lc=2 and a.lc_sc_id=c.id and a.status_active=1 and a.is_deleted=0 and c.status_active=1 and c.is_deleted=0 $str_cond  ORDER BY id";
					
				}
				else
				{
					$sql="SELECT a.id, a.benificiary_id, a.location_id, a.invoice_no, a.invoice_date, a.ex_factory_date, a.is_lc, a.buyer_id, a.exp_form_no, a.exp_form_date,a. 	invoice_value,a.discount_ammount,a.bonus_ammount,a.claim_ammount,a.commission, a.net_invo_value, a.invoice_quantity, a.total_carton_qnty, a.actual_shipment_date, a.bl_no, a.bl_date, a.etd, a.feeder_vessel, a.mother_vessel, a.etd_destination, a.bl_rev_date, a.ic_recieved_date, a.remarks, a.gsp_co_no, a.gsp_co_no_date,a.shipping_bill_n,a.ship_bl_date,b.currency_name as currency_name , b.pay_term as pay_term, b.lien_bank as lien_bank, b.export_lc_no as lc_sc_no, 1 as type
                FROM com_export_invoice_ship_mst a, com_export_lc b 
                WHERE a.benificiary_id LIKE '$cbo_company_name' AND a.location_id LIKE '$cbo_location' AND a.buyer_id LIKE '$cbo_buyer_name' AND b.lien_bank LIKE '$cbo_lien_bank' and a.is_lc=1 and a.lc_sc_id=b.id and a.status_active=1  and a.is_deleted=0  and b.status_active=1 and b.is_deleted=0  $str_cond
				 
				UNION ALL
				
				SELECT a.id, a.benificiary_id, a.location_id, a.invoice_no, a.invoice_date, a.ex_factory_date, a.is_lc, a.buyer_id, a.exp_form_no, a.exp_form_date,a. 	invoice_value,a.discount_ammount,a.bonus_ammount,a.claim_ammount,a.commission, a.net_invo_value, a.invoice_quantity, a.total_carton_qnty, a.actual_shipment_date, a.bl_no, a.bl_date, a.etd, a.feeder_vessel, a.mother_vessel, a.etd_destination, a.bl_rev_date, a.ic_recieved_date, a.remarks, a.gsp_co_no, a.gsp_co_no_date,a.shipping_bill_n,a.ship_bl_date, c.currency_name as currency_name, c.pay_term as pay_term, c.lien_bank as lien_bank, c.contract_no as lc_sc_no, 2 as type 
                FROM com_export_invoice_ship_mst a, com_sales_contract c 
                WHERE a.benificiary_id LIKE '$cbo_company_name' AND a.location_id LIKE '$cbo_location' AND a.buyer_id LIKE '$cbo_buyer_name' AND c.lien_bank LIKE '$cbo_lien_bank' and a.is_lc=2 and a.lc_sc_id=c.id and a.status_active=1 and a.is_deleted=0 and c.status_active=1 and c.is_deleted=0 $str_cond  ORDER BY id";
				}
				//echo $sql;die;
                $sql_re=sql_select($sql);$k=1;
                foreach($sql_re as $row_result)
                {
					if ($k%2==0)
					$bgcolor="#E9F3FF";
					else
					$bgcolor="#FFFFFF";
					$id=$row_result[csf('id')];
					?>
					<tr bgcolor="<? echo $bgcolor; ?>" onclick="change_color('tr_<? echo $k; ?>','<? echo $bgcolor; ?>')" id="tr_<? echo $k; ?>">
                        <td  width="50"><p><? echo $k;//$row_result[csf('id')];?></p></td>
                        <td  width="100"><p><? echo  $company_arr[$row_result[csf('benificiary_id')]]; ?></p></td>
                        <td  width="100"><p><? echo $location_arr[$row_result[csf('location_id')]];?></p></td>
                        <td align="center" width="100"><p><? echo $row_result[csf('invoice_no')];?></p></td>
                        <td align="center" width="80"><p>
                        <? if($row_result[csf('invoice_date')]!="0000-00-00") {echo change_date_format($row_result[csf('invoice_date')]);} else {echo "&nbsp;";}?>
                        </p></td> 
                        <td align="center" width="70"><p><? if($row_result[csf('type')] == 1) echo "LC"; else echo "SC"; ?></p></td>
                        <td width="100"><p><? echo $row_result[csf('lc_sc_no')];?></p></td>
                        <td width="70"><p><? echo  $buyer_arr[$row_result[csf('buyer_id')]];?></p></td>
                        <td width="150"><p><? echo $bank_arr[$row_result[csf('lien_bank')]];?></p></td>
                        <td width="90"><p><? echo $row_result[csf('exp_form_no')];?></p></td>
                        <td align="center" width="80"><p><?  if($row_result[csf('exp_form_date')]!="0000-00-00") {echo change_date_format($row_result[csf('exp_form_date')]);} else {echo "&nbsp;";} ?></p></td>
                        <td width="100" align="right"><p><? echo number_format($row_result[csf('invoice_value')],2,'.',''); $total_grs_value +=$row_result[csf('invoice_value')];?></p></td>
                        <td width="80" align="right"><p><? echo number_format($row_result[csf('discount_ammount')],2,'.',''); $total_discount_value +=$row_result[csf('discount_ammount')]; ?></p></td>
                        <td width="70" align="right"><p><? echo number_format($row_result[csf('bonus_ammount')],2,'.',''); $total_bonous_value +=$row_result[csf('bonus_ammount')];  ?></p></td>
                        <td width="70" align="right"><p><? echo number_format($row_result[csf('claim_ammount')],2,'.',''); $total_claim_value +=$row_result[csf('claim_ammount')];  ?></p></td>
                        <td width="80" align="right"><p><?  echo number_format($row_result[csf('commission')],2,'.',''); $total_commission_value +=$row_result[csf('commission')]; ?></p></td>
                        <td width="100" align="right"><p><? echo number_format($row_result[csf('net_invo_value')],2,'.',''); $total_order_qnty +=$row_result[csf('net_invo_value')];?></p></td>
                        <td align="center" width="80"><p><? echo $currency[$row_result[csf('currency_name')]];?></p></td>
                        <td align="center" width="80">
                        <p><? //echo $row[import_invoice_id]; 
                        echo "<a href='#report_detals'  onclick= \"openmypage('$id','$k');\"> View</a>";
                        ?></p>
                        </td>
                        <td width="110">
                        <p><?
                        //$ref_field=return_field_value("a.style_ref_no","wo_po_details_master a, wo_po_break_down b,  com_export_invoice_ship_dtls c","a.job_no=b.job_no_mst and b.id=c.po_breakdown_id and c.mst_id=".$row_result[csf('id')],"style_ref_no");
                        echo $style_ref_arr[$row_result[csf('id')]];
                       //echo "select a.style_ref_no from wo_po_details_master a, wo_po_break_down b,  com_export_invoice_ship_dtls c where a.job_no=b.job_no_mst and b.id=c.po_breakdown_id and c.mst_id=".$row_result[csf('id')]."";
                        ?></p>
                        </td>
                        <td align="right" width="110"><p><? echo number_format($row_result[csf('invoice_quantity')],2); $total_invoice_qty +=$row_result[csf('invoice_quantity')];?></p></td>
                        <td align="right" width="110"><p><? echo number_format($row_result[csf('total_carton_qnty')],2); $total_carton_qty +=$row_result[csf('total_carton_qnty')];?></p></td>
                        <td align="center" width="80"><p><?  if($row_result[csf('actual_shipment_date')]!="0000-00-00") {echo change_date_format($row_result[csf('actual_shipment_date')]);} else {echo "&nbsp;";}  ?></p></td>
                        <td width="100"><p><? echo $row_result[csf('bl_no')];?></p></td>
                        <td align="center" width="80"><p><?  if($row_result[csf('bl_date')]!="0000-00-00") {echo change_date_format($row_result[csf('bl_date')]);} else {echo "&nbsp;";} ?></p></td>
                        <td align="center" width="50"><p><?  $diff_bl=datediff("d",$row_result[csf('ex_factory_date')], $row_result[csf('bl_date')]); if($diff_bl>0) echo $diff_bl."days";  ?></p></td>
                        <td width="90"><p><? echo $row_result[csf('shipping_bill_n')]; //a.shipping_bill_n,a.ship_bl_date, ?></p></td>
                        <td align="center" width="80"><p><?  if($row_result[csf('ship_bl_date')]!="0000-00-00") {echo change_date_format($row_result[csf('ship_bl_date')]);} else {echo "&nbsp;";} ?></p></td>
                        <td align="center" width="110"><p><?  if($row_result[csf('etd')]!="0000-00-00") {echo change_date_format($row_result[csf('etd')]);} else {echo "&nbsp;";}?></p></td>
                        <td width="100"><p><? echo $row_result[csf('feeder_vessel')];?></p></td>
                        <td width="100"><p><? echo $row_result[csf('mother_vessel')];?></p></td>
                        <td align="center" width="80"><p><?  if($row_result[csf('etd_destination')]!="0000-00-00") {echo change_date_format($row_result[csf('etd_destination')]);} else { echo "&nbsp;";} ?></p></td>
                        <td width="100">
                        <p></p>
                        </td>
                        <td width="100" align="center"><p><? echo $row_result[csf('gsp_co_no')];?></p></td>
                        <td width="80" align="center"><p>
                        <? if(trim($row_result[csf('gsp_co_no_date')])!="0000-00-00") {echo change_date_format($row_result[csf('gsp_co_no_date')]);}else {echo "&nbsp;";}?>
                        </p></td>
                        <td align="center" width="80">
                        <p><?
                        //$curier_receipt_date=return_field_value("a.courier_date","com_export_doc_submission_mst a,com_export_doc_submission_invo b","a.id=b.doc_submission_mst_id and b.invoice_id=".$row_result[csf("id")]."","courier_date");
                        
                        //echo $row_result[csf("id")];
						$curier_receipt_date=$currier_date_arr[$row_result[csf("id")]];
                        if(!(trim($curier_receipt_date)=="0000-00-00" || trim($curier_receipt_date)=="")) 
                        {
                            echo change_date_format($curier_receipt_date);
                        }
                        else
                        {
                            echo "&nbsp;";	
                        }
                        ?></p>
                        </td>
                        <td align="center" width="80"><p><? if($row_result[csf('bl_rev_date')]!="0000-00-00") {echo change_date_format($row_result[csf('bl_rev_date')]);} else { echo "&nbsp;";}?></p></td>
                        <td align="center" width="80"><p><? if($row_result[csf('etd')]!="0000-00-00") echo change_date_format( $row_result[csf('ic_recieved_date')]); else echo "";?></p></td>
                        <td align="center" width="80"><p><? if($row_result[csf('ex_factory_date')]!="0000-00-00") {echo change_date_format($row_result[csf('ex_factory_date')]);} else {echo "&nbsp;";} ?></p></td>
                        <td align="center" width="70"><p>
                        <?
                        //$curier_submit_date=return_field_value("a.submit_date","com_export_doc_submission_mst a,com_export_doc_submission_invo b","a.id=b.doc_submission_mst_id and b.invoice_id=".$row_result[csf('id')]."","submit_date");
                        //var_dump($curier_submit_date);die;
                        if(($row_result[csf('ex_factory_date')]!='0000-00-00') )
                        {
                            $current_date=date("Y-m-d");
                            if($curier_submit_date_arr[$row_result[csf('id')]]=='0000-00-00' || $curier_submit_date_arr[$row_result[csf('id')]]==null)
                            {
                            $diff=datediff("d",$row_result[csf('ex_factory_date')], $current_date);
                            }
                            else
                            {
                            $diff=datediff("d",$row_result[csf('ex_factory_date')], $curier_submit_date_arr[$row_result[csf('id')]]);
                            }
                            
                        }
                        else 
                        {
                            $diff="";
                        }
                        
                        echo $diff;
                        ?>
                        </p></td>
                        <td align="center" width="80"><p>
                        <?
                            if($curier_submit_date_arr[$row_result[csf('id')]]!='0000-00-00') {echo change_date_format($curier_submit_date_arr[$row_result[csf('id')]]);} else {echo "";}
                        ?>
                        </p></td>
                        <td align="center" width="50"><p><?  $diff_sub=datediff("d",$row_result[csf('bl_date')], $curier_submit_date_arr[$row_result[csf('id')]]); if($diff_sub>0) echo $diff_sub."days";   ?></p></td>
                        <td width="100" align="center">
                        <p><?
                        //$bnk_to_bnk_cour_no=return_field_value("a.bnk_to_bnk_cour_no","com_export_doc_submission_mst a,com_export_doc_submission_invo b","a.id=b.doc_submission_mst_id and b.invoice_id=".$row_result[csf('id')],"bnk_to_bnk_cour_no");
                        echo $bnk_to_bnk_cour_no_arr[$row_result[csf('id')]];
                        ?></p>
                        </td>
                        <td width="100">
                        <p><?
                        //$bank_ref_no=return_field_value("a.bank_ref_no","com_export_doc_submission_mst a,com_export_doc_submission_invo b","a.id=b.doc_submission_mst_id and b.invoice_id=".$row_result[csf('id')],"bank_ref_no");
                        echo $bank_ref_no_arr[$row_result[csf('id')]];
                        ?></p>
                        </td>
                        <td align="center" width="80"> 
                        <p><?
                       // $bank_date_no=return_field_value("a.bank_ref_date","com_export_doc_submission_mst a,com_export_doc_submission_invo b","a.id=b.doc_submission_mst_id and b.invoice_id=".$row_result[csf('id')],"bank_ref_date");
                        if(!(trim($bank_date_no_arr[$row_result[csf('id')]])=="0000-00-00" || trim($bank_date_no_arr[$row_result[csf('id')]])=="")) 
                        {
                            echo change_date_format($bank_date_no_arr[$row_result[csf('id')]]);
                        }
                        else
                        {
                            echo "&nbsp;";	
                        }
                        ?></p>
                        </td>
                        <td width="80" align="center"><p><? echo $pay_term[$row_result[csf('pay_term')]];?></p></td>
                        <td width="80" align="center"><p>
                        <?
                        // $possible_rlz_date_arr=return_field_value("a.possible_reali_date","com_export_doc_submission_mst a,com_export_doc_submission_invo b","a.id=b.doc_submission_mst_id and b.invoice_id=".$row_result[csf('id')],"possible_reali_date");
                         if(!(trim($possible_rlz_date_arr[$row_result[csf('id')]])=="0000-00-00" || trim($possible_rlz_date_arr[$row_result[csf('id')]])=="")) 
                        {
                            echo change_date_format($possible_rlz_date_arr[$row_result[csf('id')]]);
                        }
                        else
                        {
                            echo "&nbsp;";	
                        }
                         //if($possible_rlz_date !='0000-00-00' ) echo change_date_format($possible_rlz_date); else echo "";
                         ?>
                        </p></td>
                        <td  width="100"><p><? echo $row_result[csf('remarks')];?></p></td>
					</tr>
					<?
					$k++;
                }
                //print_r($sc_value_1_3);
                
                ?>
                </tbody>
                <!--<tfoot>
                <tr>
                	<th >&nbsp;</th>
                    <th >&nbsp;</th>
                    <th >&nbsp;</th>
                    <th >&nbsp;</th>
                    <th >&nbsp;</th>
                    <th >&nbsp;</th>
                    <th >&nbsp;</th>
                    <th >&nbsp;</th>
                    <th >&nbsp;</th>
                    <th >&nbsp;</th>
                    <th align="right">Grand Total:</th>
                    <th id="value_total_grs_value"><?// echo number_format($total_grs_value,2);  ?></th>
                    <th id="value_total_discount_value"><?// echo number_format($total_discount_value,2);  ?></th>
                    <th id="value_total_bonous_value"><?// echo number_format($total_bonous_value,2);  ?></th>
                    <th id="value_total_claim_value"><?// echo number_format($total_claim_value,2);  ?></th>
                    <th id="value_total_commission_value"><?// echo number_format($total_commission_value,2);  ?></th>
                    <th  id="value_total_net_invo_value"><?// echo number_format($total_order_qnty,2);  ?></th>
                    <th >&nbsp;</th>
                    <th >&nbsp;</th>
                    <th  ></th>
                    <th  id="total_invoice_qty"><?// echo number_format($total_invoice_qty,2); ?></th>
                    <th id="total_carton_qty"><?// echo number_format($total_carton_qty,2); ?></th>
                    <th >&nbsp;</th>
                    <th >&nbsp;</th>
                    <th >&nbsp;</th>
                    <th >&nbsp;</th>
                    <th >&nbsp;</th>
                    <th >&nbsp;</th>
                    <th >&nbsp;</th>
                    <th >&nbsp;</th>
                    <th >&nbsp;</th>
                    <th >&nbsp;</th>
                    <th >&nbsp;</th>
                    <th >&nbsp;</th>
                    <th >&nbsp;</th>
                    <th >&nbsp;</th>
                    <th >&nbsp;</th>
                    <th >&nbsp;</th>
                    <th >&nbsp;</th>
                    <th >&nbsp;</th>
                    <th >&nbsp;</th>
                    <th >&nbsp;</th>
                    <th >&nbsp;</th>
                    <th >&nbsp;</th>
                    <th >&nbsp;</th>
                    <th >&nbsp;</th>
                    <th >&nbsp;</th>
                    <th >&nbsp;</th>
                </tr>
                </tfoot>-->
            </table>

            <table width="4250" class="rpt_table" cellpadding="0" cellspacing="0" border="1" rules="all" id="report_table_footer">
            	<tfoot>
                <tr>
                	<th width="50" >&nbsp;</th>
                    <th width="100">&nbsp;</th>
                    <th width="100">&nbsp;</th>
                    <th width="100">&nbsp;</th>
                    <th width="80">&nbsp;</th>
                    <th width="70">&nbsp;</th>
                    <th width="100">&nbsp;</th>
                    <th width="70">&nbsp;</th>
                    <th width="150">&nbsp;</th>
                    <th width="90">&nbsp;</th>
                    <th width="80" align="right">Grand Total:</th>
                    <th width="100"id="value_total_grs_value"><? echo number_format($total_grs_value,2);  ?></th>
                    <th width="80"id="value_total_discount_value"><? echo number_format($total_discount_value,2);  ?></th>
                    <th width="70"id="value_total_bonous_value"><? echo number_format($total_bonous_value,2);  ?></th>
                    <th width="70"id="value_total_claim_value"><? echo number_format($total_claim_value,2);  ?></th>
                    <th width="80"id="value_total_commission_value"><? echo number_format($total_commission_value,2);  ?></th>
                    <th width="100" id="value_total_net_invo_value"><? echo number_format($total_order_qnty,2);  ?></th>
                    <th width="80">&nbsp;</th>
                    <th width="80">&nbsp;</th>
                    <th width="110" ></th>
                    <th width="110" id="total_invoice_qty"><? echo number_format($total_invoice_qty,2); ?></th>
                    <th width="110"id="total_carton_qty"><? echo number_format($total_carton_qty,2); ?></th>
                    <th width="80">&nbsp;</th>
                    <th width="100">&nbsp;</th>
                    <th width="80">&nbsp;</th>
                    <th width="50">&nbsp;</th>
                    <th width="90">&nbsp;</th>
                    <th width="80">&nbsp;</th>
                    <th width="110">&nbsp;</th>
                    <th width="100">&nbsp;</th>
                    <th width="100">&nbsp;</th>
                    <th width="80">&nbsp;</th>
                    <th width="100">&nbsp;</th>
                    <th width="100">&nbsp;</th>
                    <th width="80">&nbsp;</th>
                    <th width="80">&nbsp;</th>
                    <th width="80">&nbsp;</th>
                    <th width="80">&nbsp;</th>
                    <th width="80">&nbsp;</th>
                    <th width="70">&nbsp;</th>
                    <th width="80">&nbsp;</th>
                    <th width="50">&nbsp;</th>
                    <th width="100">&nbsp;</th>
                    <th width="100">&nbsp;</th>
                    <th width="80">&nbsp;</th>
                    <th width="80">&nbsp;</th>
                    <th width="80">&nbsp;</th>
                    <th width="100">&nbsp;</th>
                </tr>
                </tfoot>
            </table>
               <div align="left" style="font-weight:bold; margin-left:30px;"><? echo "User Id : ". $user_arr[$user_id] ." , &nbsp; THIS IS SYSTEM GENERATED STATEMENT, NO SIGNATURE REQUIRED ."; ?></div>
 </div>
</div>
<?
	exit();
}
if($action=="po_id_details")
{
	echo load_html_head_contents("Report Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
	$po_id=str_replace("'","",$po_id);
	//print_r($po_id);die;
?> 

<div style="width:460px">
<fieldset style="width:100%"  >
    <table border="1" cellpadding="0" cellspacing="0" class="rpt_table" rules="all" width="440">
        <thead>
            <th width="120">PO NO</th>
            <th width="110">PO Qnty</th>
            <th width="110">PO Attach Qnty</th>
            <th width="100">Invoice Qnty</th>
        </thead>
    </table>
	<div style="width:460px; max-height:280px; overflow-y:scroll">   
    <table border="1" cellpadding="0" cellspacing="0" class="rpt_table" rules="all" width="440">
		<?
		if($db_type==0)
		{
			$company_sql="select c.invoice_no, b.po_number, b.po_quantity, a.po_breakdown_id, a.current_invoice_qnty from com_export_invoice_ship_dtls a, wo_po_break_down b, com_export_invoice_ship_mst c where a.po_breakdown_id=b.id and a.mst_id=c.id and c.id='$po_id' and a.current_invoice_qnty not in(0) and a.status_active=1 and a.is_deleted=0";
		}
		else if($db_type==2)
		{
			$company_sql="select c.invoice_no, b.po_number, b.po_quantity, a.po_breakdown_id, a.current_invoice_qnty from com_export_invoice_ship_dtls a, wo_po_break_down b, com_export_invoice_ship_mst c where a.po_breakdown_id=b.id and a.mst_id=c.id and c.id='$po_id' and a.current_invoice_qnty not in(0) and a.status_active=1 and a.is_deleted=0";
		}
        
		//echo $company_sql;die;
        //$sql_re=sql_select($sql);
		$i=$k+1;
        $sql_re=sql_select($company_sql);
        $total_invoice_qty=0;  $total_order_qty=0;  $total_attach_qty=0;
        $result=0;
        foreach($sql_re as $row)
        {  
        ?>
    
        <tr bgcolor="<? echo $bgcolor; ?>" onclick="change_color('tr_<? echo $i; ?>','<? echo $bgcolor; ?>')" id="tr_<? echo $i; ?>">
        	<td width="120"><p><? echo $row[csf('po_number')]; ?></p></td>
            <td width="110" align="right">
            <p><? 
            echo number_format($row[csf('po_quantity')],0); 
            $total_order_qty +=$row[csf('po_quantity')];
            ?></p>
            </td>
            <td width="110" align="right">
            <p><? 
			$borken_id=number_format($row[csf('po_breakdown_id')],0);
            $att_qnty_lc=return_field_value("sum(attached_qnty)","com_export_lc_order_info","wo_po_break_down_id='$borken_id' and status_active=1 and is_deleted=0 ");
			//echo $att_qnty_lc;
            $att_qnty_sc=return_field_value("sum(attached_qnty)","com_sales_contract_order_info","wo_po_break_down_id='$borken_id' and status_active=1 and is_deleted=0");
            $attached_qnty=$att_qnty_lc+$att_qnty_sc;
            echo number_format($attached_qnty,0); 
            $total_attach_qty+=$attached_qnty;
            ?></p>
            </td>
            <td align="right" width="100">
            <p><? 
            $result=$row[csf('current_invoice_qnty')]; 
            echo number_format( $result,0);
            $total_invoice_qty+=$result;
            ?></p>
            </td>
        
        </tr>
		<?
		$i++;
        }
        ?>
        
        <tr >
            <td align="right" width="120">Total</td>
            <td align="right" width="110"><p><?php echo number_format($total_order_qty,0); ?></p></td>
            <td align="right" width="110"><p><?php echo number_format($total_attach_qty,0); ?></p></td>
            <td align="right" width="100"><p><?php echo number_format($total_invoice_qty,0); ?></p></td>
        </tr>
    </table>
    </div>
</fieldset>
</div>
<?
exit();
}

disconnect($con);
?>


