﻿<?php 
/*-------------------------------------------- Comments
Version          :  V1
Purpose			 : This form will create print Booking
Functionality	 :	
JS Functions	 :
Created by		 : MONZU 
Creation date 	 : 
Requirment Client: 
Requirment By    : 
Requirment type  : 
Requirment       : 
Affected page    : 
Affected Code    :              
DB Script        : 
Updated by 		 : 
Update date		 : 
QC Performed BY	 :		
QC Date			 :	
Comments		 : From this version oracle conversion is start
*/
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
include('../../../includes/common.php');
$data=$_REQUEST['data'];
$action=$_REQUEST['action'];
$permission=$_SESSION['page_permission'];
//---------------------------------------------------- Start---------------------------------------------------------------------------
//$po_number=return_library_array( "select id,po_number from wo_po_break_down", "id", "po_number");
$color_library=return_library_array( "select id,color_name from lib_color", "id", "color_name"  );
$size_library=return_library_array( "select id,size_name from lib_size", "id", "size_name"  );
$company_library=return_library_array( "select id,company_name from lib_company", "id", "company_name"  );
$buyer_arr=return_library_array( "select id,buyer_name from lib_buyer", "id", "buyer_name"  );
if ($action=="load_drop_down_buyer")
{
	echo create_drop_down( "cbo_buyer_name", 172, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' $buyer_cond and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (1,3,21,90))  order by buyer_name","id,buyer_name", 1, "-- Select Buyer --", $selected, "" );
}


if ($action=="order_popup")
{
  	echo load_html_head_contents("Popup Info","../../../", 1, 1, $unicode);
	extract($_REQUEST);
?>
	<script>
			function set_checkvalue()
			{
				if(document.getElementById('chk_job_wo_po').value==0) document.getElementById('chk_job_wo_po').value=1;
				else document.getElementById('chk_job_wo_po').value=0;
			}
			
			function js_set_value( job_no )
			{
				document.getElementById('selected_job').value=job_no;
				parent.emailwindow.hide();
			}
    </script>
</head>
<body>
<div align="center" style="width:100%;" >
<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
	<table width="1100" cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
    	<tr>
        	<td align="center" width="100%">
            	<table  cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
                    <thead> 
                    <tr>
                     <th width="150" colspan="3"> </th>
                            <th>
                              <?php
                               echo create_drop_down( "cbo_string_search_type", 100, $string_search_type,'', 1, "-- Searching Type --" );
                              ?>
                            </th>
                          <th width="150" colspan="3"> </th>
                    </tr>  
                    <tr>               	 
                        <th width="150">Company Name</th>
                        <th width="150">Buyer Name</th>
                         <th width="100">Job No</th>
                          <th width="100">Style Ref </th>
                        <th width="150">Order No</th>
                        <th width="200">Date Range</th><th></th>  
                        </tr>         
                    </thead>
        			<tr>
                    	<td> 
                        <input type="hidden" id="selected_job">
                        
							<?php 
								echo create_drop_down( "cbo_company_mst", 150, "select id,company_name from lib_company comp where status_active =1 and is_deleted=0 $company_cond order by company_name","id,company_name",1, "-- Select Company --", '',"load_drop_down( 'print_booking_controller', this.value, 'load_drop_down_buyer', 'buyer_td' );" );
							?>
                    </td>
                   	<td id="buyer_td">
                     <?php 
						echo create_drop_down( "cbo_buyer_name", 172, $blank_array,'', 1, "-- Select Buyer --" );
					?>	</td>
                    <td><input name="txt_job_prifix" id="txt_job_prifix" class="text_boxes" style="width:90px"></td>
                     <td><input name="txt_style" id="txt_style" class="text_boxes" style="width:100px"></td>
                    <td><input name="txt_order_search" id="txt_order_search" class="text_boxes" style="width:150px"></td>
                    <td><input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px">
					  <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px">
					 </td> 
            		 <td align="center">
                     <input type="button" name="button2" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_company_mst').value+'_'+document.getElementById('cbo_buyer_name').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+document.getElementById('txt_job_prifix').value+'_'+document.getElementById('cbo_year_selection').value+'_'+document.getElementById('txt_order_search').value+'_'+document.getElementById('txt_style').value+'_'+document.getElementById('cbo_string_search_type').value, 'create_po_search_list_view', 'search_div', 'print_booking_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:100px;" /></td>
        		</tr>
             </table>
          </td>
        </tr>
        <tr>
            <td  align="center" height="40" valign="middle">
            <?php 
			    echo create_drop_down( "cbo_year_selection", 70, $year,"", 1, "-- Select --", date('Y'), "",0 );		
			?>
			<?php echo load_month_buttons();  ?>
            </td>
            </tr>
        <tr>
            <td align="center" valign="top" id="search_div"> 
	
            </td>
        </tr>
    </table>    
     
    </form>
   </div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}

if($action=="create_po_search_list_view")
{
	$data=explode('_',$data);
	if ($data[0]!=0) $company=" and a.company_name='$data[0]'"; else { echo "Please Select Company First."; die; }
	if ($data[1]!=0) $buyer=" and a.buyer_name='$data[1]'"; else  $buyer="";//{ echo "Please Select Buyer First."; die; }
	if($db_type==0) { $year_cond=" and YEAR(a.insert_date)=$data[5]";   }
	if($db_type==2) {$year_cond=" and to_char(a.insert_date,'YYYY')=$data[5]";}
	//if (str_replace("'","",$data[4])!="") $job_cond=" and a.job_no_prefix_num='$data[4]'  $year_cond "; else  $job_cond=""; 
	//if (str_replace("'","",$data[6])!="") $order_cond=" and b.po_number like '%$data[6]%'  "; else  $order_cond=""; 
	$job_cond="";
	$order_cond=""; 
	$style_cond="";
	if($data[8]==1)
	{
	if (str_replace("'","",$data[4])!="") $job_cond=" and a.job_no_prefix_num='$data[4]' $year_cond"; //else  $job_cond=""; 
	if (str_replace("'","",$data[6])!="") $order_cond=" and b.po_number = '$data[6]'  "; //else  $order_cond=""; 
	if (trim($data[7])!="") $style_cond=" and a.style_ref_no ='$data[7]'"; //else  $style_cond=""; 
	}
	if($data[8]==2)
	{
	if (str_replace("'","",$data[4])!="") $job_cond=" and a.job_no_prefix_num like '$data[4]%' $year_cond"; //else  $job_cond=""; 
	if (str_replace("'","",$data[6])!="") $order_cond=" and b.po_number like '$data[6]%'  "; //else  $order_cond=""; 
	if (trim($data[7])!="") $style_cond=" and a.style_ref_no like '$data[7]%'  "; //else  $style_cond=""; 
	}
	if($data[8]==3)
	{
	if (str_replace("'","",$data[4])!="") $job_cond=" and a.job_no_prefix_num like '%$data[4]' $year_cond"; //else  $job_cond=""; 
	if (str_replace("'","",$data[6])!="") $order_cond=" and b.po_number like '%$data[6]'  "; //else  $order_cond=""; 
	if (trim($data[7])!="") $style_cond=" and a.style_ref_no like '%$data[7]'"; //else  $style_cond=""; 
	}
	if($data[8]==4 || $data[8]==0)
	{
	if (str_replace("'","",$data[4])!="") $job_cond=" and a.job_no_prefix_num like '%$data[4]%' $year_cond"; //else  $job_cond=""; 
	if (str_replace("'","",$data[6])!="") $order_cond=" and b.po_number like '%$data[6]%'  "; //else  $order_cond=""; 
	if (trim($data[7])!="") $style_cond=" and a.style_ref_no like '%$data[7]%'"; //else  $style_cond=""; 
	}
	if($db_type==0)
	{
	if ($data[2]!="" &&  $data[3]!="") $shipment_date = "and b.pub_shipment_date between '".change_date_format($data[2], "yyyy-mm-dd", "-")."' and '".change_date_format($data[3], "yyyy-mm-dd", "-")."'"; else $shipment_date ="";
	}
	if($db_type==2)
	{
	if ($data[2]!="" &&  $data[3]!="") $shipment_date = "and b.pub_shipment_date between '".change_date_format($data[2], "yyyy-mm-dd", "-",1)."' and '".change_date_format($data[3], "yyyy-mm-dd", "-",1)."'"; else $shipment_date ="";
	}
	$buyer_arr=return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
	$comp=return_library_array( "select id, company_name from lib_company",'id','company_name');
	$arr=array (2=>$comp,3=>$buyer_arr);
	//$sql= "select YEAR(a.insert_date) as year, a.job_no_prefix_num,a.company_name,a.buyer_name,a.style_ref_no,a.job_quantity, b.po_number,b.po_quantity,b.shipment_date,a.job_no,c.id as pre_id from wo_po_details_master  a, wo_po_break_down b left join wo_pre_cost_mst c on b.job_no_mst=c.job_no and c.status_active=1 and c.is_deleted=0 where a.job_no=b.job_no_mst and a.status_active=1 and b.status_active=1 $shipment_date $company $buyer order by a.job_no"; 
	if($db_type==0)
	{
		$sql= "select YEAR(a.insert_date) as year, a.job_no_prefix_num,a.company_name,a.buyer_name,a.style_ref_no,a.job_quantity, b.po_number,b.po_quantity,b.shipment_date,a.job_no,c.id as pre_id from wo_po_details_master  a, wo_po_break_down b, wo_pre_cost_mst c,wo_pre_cost_embe_cost_dtls d   where a.job_no=b.job_no_mst and a.job_no=c.job_no and a.job_no=d.job_no and a.status_active=1 and a.is_deleted=0  and b.status_active=1 and b.is_deleted=0  and c.status_active=1 and c.is_deleted=0 and d.status_active=1 and d.is_deleted=0  $shipment_date $company $buyer  $job_cond $style_cond $order_cond  order by a.job_no";  
	}
	if($db_type==2)
	{
		 $sql= "select to_char(a.insert_date,'YYYY') as year, a.job_no_prefix_num,a.company_name,a.buyer_name,a.style_ref_no,a.job_quantity, b.po_number,b.po_quantity,b.shipment_date,a.job_no,c.id as pre_id from wo_po_details_master  a, wo_po_break_down b, wo_pre_cost_mst c,wo_pre_cost_embe_cost_dtls d   where a.job_no=b.job_no_mst and a.job_no=c.job_no and a.job_no=d.job_no and a.status_active=1 and a.is_deleted=0  and b.status_active=1 and b.is_deleted=0  and c.status_active=1 and c.is_deleted=0 and d.status_active=1 and d.is_deleted=0  $shipment_date $company $buyer  
$job_cond $style_cond $order_cond  order by a.job_no";  
	}

	
	echo  create_list_view("list_view", "Year,Job No,Company,Buyer Name,Style Ref. No,Job Qty.,PO number,PO Quantity,Shipment Date, Precost id", "90,80,120,100,100,90,90,90,80,100","1080","320",0, $sql , "js_set_value", "job_no", "", 1, "0,0,company_name,buyer_name,0,0,0,0,0", $arr , "year,job_no_prefix_num,company_name,buyer_name,style_ref_no,job_quantity,po_number,po_quantity,shipment_date,pre_id", "",'','0,0,0,0,0,1,0,1,3,0') ;
}


if ($action=="populate_order_data_from_search_popup")
{
	
	$po_number=return_library_array( "select id,po_number from wo_po_break_down where job_no_mst='".$data."'", "id", "po_number"  );
 	$cbo_po_no=create_drop_down( "txt_order_no_id", 172, $po_number,"", 1, "-- Select PO --", "", "","",""); 
	$po_number=return_library_array( "select id,po_number from wo_po_break_down where job_no_mst='".$data."'", "id", "po_number"  );
    $emb_name="";
	//$data_array=sql_select("select a.job_no,a.company_name,a.buyer_name,a.gmts_item_id,b.emb_name from wo_po_details_master a, wo_pre_cost_embe_cost_dtls b where a.job_no ='".$data."' and b.cons_dzn_gmts !=0 and b.emb_type in(1,2,3) and a.job_no=b.job_no");
	$data_array=sql_select("select a.job_no,a.company_name,a.buyer_name,a.gmts_item_id,b.emb_name from wo_po_details_master a, wo_pre_cost_embe_cost_dtls b where a.job_no ='".$data."' and b.cons_dzn_gmts !=0 and b.emb_name in(1,2,3,4) and a.job_no=b.job_no");
	foreach ($data_array as $row)
	{
		$emb_name.=$row[csf("emb_name")].",";
		echo "document.getElementById('cbo_company_name').value = '".$row[csf("company_name")]."';\n";  
		echo "document.getElementById('cbo_buyer_name').value = '".$row[csf("buyer_name")]."';\n";  
		echo "document.getElementById('txt_job_no').value = '".$row[csf("job_no")]."';\n";
		$gmt_item=create_drop_down( "cbo_gmt_item", 172, $garments_item,"", 1, "-- Select Item --", "", "","",$row[csf("gmts_item_id")] ); 
		echo "document.getElementById('gmt_item_td').innerHTML = '".$gmt_item."';\n";
		echo "document.getElementById('po_id_td').innerHTML = '".$cbo_po_no."';\n";
	}
	 $emb_name=rtrim($emb_name,",");
	 //echo $emb_name;
	 if($emb_name !="")
	 {
		$cbo_booking_natu= create_drop_down( "cbo_booking_natu", 172, $emblishment_name_array,"", 1, "-- Select --", "","fnc_generate_booking()", "", $emb_name);
	    echo "document.getElementById('booking_natu_td').innerHTML ='".$cbo_booking_natu."';\n"; 
	 }
	 else
	 {
		echo "alert('No Embellishment Data found');\n";
	 }
	

}




if ($action=="generate_print_booking")
{
	extract($_REQUEST);
	$cbo_company_name=str_replace("'","",$cbo_company_name);
	$cbo_buyer_name=str_replace("'","",$cbo_buyer_name);
	$txt_job_no=str_replace("'","",$txt_job_no);
	$txt_order_no_id=str_replace("'","",$txt_order_no_id);
	$cbo_booking_natu=str_replace("'","",$cbo_booking_natu);
	$cbo_gmt_item=str_replace("'","",$cbo_gmt_item);
	$txt_booking_no=str_replace("'","",$txt_booking_no);
	$txt_booking_date=change_date_format(str_replace("'","",$txt_booking_date),"dd-mm-yyyy","-");
	$calculation_basis=str_replace("'","",$calculation_basis);
	
	$costing_per=return_field_value("costing_per", "wo_pre_cost_mst", "job_no='$txt_job_no'");
	if($costing_per==1)
	{
		$costing_per_dzn="1 Dzn";
	}
	else if($costing_per==2)
	{
		$costing_per_dzn="1 Pcs";
	}
	else if($costing_per==3)
	{
		$costing_per_dzn="2 Dzn";
	}
	else if($costing_per==4)
	{
		$costing_per_dzn="3 Dzn";
	}
	else if($costing_per==5)
	{
		$costing_per_dzn="4 Dzn";
	}


     if($cbo_company_name==0)
	 {
		 $cbo_company_name_cond="";
	 }
	 else
	 {
		 $cbo_company_name_cond =" and a.company_name='$cbo_company_name'";
	 }
	 
	 
	 if($cbo_buyer_name==0)
	 {
		 $cbo_buyer_name_cond="";
	 }
	 else
	 {
		 $cbo_buyer_name_cond =" and a.buyer_name='$cbo_buyer_name'";
	 }
	 
	 if($txt_job_no=="")
	 {
		 $txt_job_no_cond="";
	 }
	 else
	 {
		 $txt_job_no_cond ="and a.job_no='$txt_job_no'";
	 }
	 
	 
	 
	 $booking_month=0;
	 if($cbo_booking_month<10)
	 {
		 $booking_month.=$cbo_booking_month;
	 }
	 else
	 {
		$booking_month=$cbo_booking_month; 
	 }
	$start_date=$cbo_booking_year."-".$booking_month."-01";
	$end_date=$cbo_booking_year."-".$booking_month."-".cal_days_in_month(CAL_GREGORIAN, $booking_month, $cbo_booking_year);
	?>
   <table cellspacing="0" cellpadding="0" border="1" rules="all" width="1247" class="rpt_table" >
                <thead>
                    <th width="40">SL</th>
                    <th width="90">Ord. No</th>
                    <th width="100">Gmt Item</th>
                    <th width="90">Booking Nature</th>
                    <th width="100">Booking Type</th>
                    <th width="80">Color</th>
                    <th width="60"><?php if($calculation_basis==1){echo "Order Qty (Pcs)";} else{echo "Plan Cut(Pcs)";} ?></th>
                    <th width="200">Description</th>
                    <th width="60">Req Qty /DZN</th>
                    <th width="60">CU WOQ/DZN</th>
                    <th width="60">Bal Qty/DZN</th>
                    <th width="60">WOQ /DZN</th>
                    <th width="50">Rate/DZN</th>
                    <th width="60">Amount/DZN</th>
                    <th width="">Delv. Date</th>
                </thead>
            </table>
            <div style="width:1247px; overflow-y:scroll; max-height:350px;" id="buyer_list_view" align="center">
            <table cellspacing="0" cellpadding="0" border="1" rules="all" width="1230" class="rpt_table" id="tbl_list_search" >
       <tbody>
       <?php
				   $booking_data_array_cu=array();
				   
				   $sql_booking_cu= sql_select( "select b.po_break_down_id,b.gmt_item,b.pre_cost_fabric_cost_dtls_id,b.gmts_color_id,sum(b.wo_qnty) as cu_woq,c.emb_name,c.emb_type from wo_booking_mst a, wo_booking_dtls b,  wo_pre_cost_embe_cost_dtls c where a.job_no=b.job_no and  a.job_no=c.job_no and a.booking_no=b.booking_no and b.pre_cost_fabric_cost_dtls_id=c.id and a.job_no='$txt_job_no' and a.booking_type=6 and a.is_short=2 and  a.status_active=1  and 	a.is_deleted=0  group by b.po_break_down_id,b.gmt_item,b.pre_cost_fabric_cost_dtls_id,c.emb_name,c.emb_type,b.gmts_color_id  order by b.pre_cost_fabric_cost_dtls_id");
				   
				   foreach($sql_booking_cu as $sql_booking_row_cu )
				   {
					  $booking_data_array_cu[$sql_booking_row_cu[csf('po_break_down_id')]][$sql_booking_row_cu[csf('gmt_item')]][$sql_booking_row_cu[csf('pre_cost_fabric_cost_dtls_id')]][$sql_booking_row_cu[csf('emb_name')]][$sql_booking_row_cu[csf('emb_type')]][$sql_booking_row_cu[csf('gmts_color_id')]]=$sql_booking_row_cu[csf('cu_woq')];
				   }
				   
				   $booking_data_array=array();
				   $updateid="";
				    $sql_booking= sql_select( "select id,job_no,po_break_down_id,pre_cost_fabric_cost_dtls_id,booking_no,booking_type,gmts_color_id,gmt_item,wo_qnty,rate,amount,delivery_date,description from  wo_booking_dtls where booking_no='$txt_booking_no'");
					
				   foreach($sql_booking as $sql_booking_row )
				   {
					   $booking_data_array[$sql_booking_row[csf('po_break_down_id')]][$sql_booking_row[csf('gmt_item')]][$sql_booking_row[csf('pre_cost_fabric_cost_dtls_id')]][$sql_booking_row[csf('gmts_color_id')]][id]=$sql_booking_row[csf('id')];
					   $booking_data_array[$sql_booking_row[csf('po_break_down_id')]][$sql_booking_row[csf('gmt_item')]][$sql_booking_row[csf('pre_cost_fabric_cost_dtls_id')]][$sql_booking_row[csf('gmts_color_id')]][wo_qnty]=$sql_booking_row[csf('wo_qnty')];
					   $booking_data_array[$sql_booking_row[csf('po_break_down_id')]][$sql_booking_row[csf('gmt_item')]][$sql_booking_row[csf('pre_cost_fabric_cost_dtls_id')]][$sql_booking_row[csf('gmts_color_id')]][rate]=$sql_booking_row[csf('rate')];
					   $booking_data_array[$sql_booking_row[csf('po_break_down_id')]][$sql_booking_row[csf('gmt_item')]][$sql_booking_row[csf('pre_cost_fabric_cost_dtls_id')]][$sql_booking_row[csf('gmts_color_id')]][amount]=$sql_booking_row[csf('amount')];
					   $booking_data_array[$sql_booking_row[csf('po_break_down_id')]][$sql_booking_row[csf('gmt_item')]][$sql_booking_row[csf('pre_cost_fabric_cost_dtls_id')]][$sql_booking_row[csf('gmts_color_id')]][delivery_date]=$sql_booking_row[csf('delivery_date')];
					   $booking_data_array[$sql_booking_row[csf('po_break_down_id')]][$sql_booking_row[csf('gmt_item')]][$sql_booking_row[csf('pre_cost_fabric_cost_dtls_id')]][$sql_booking_row[csf('gmts_color_id')]][description]=$sql_booking_row[csf('description')];

				   }
	   
				    $sql="select
					b.costing_per,
					c.id as wo_pre_cost_embe_cost_dtls,
					c.job_no,
					c.emb_name,
					c.emb_type,
					c.cons_dzn_gmts,
					c.rate,
					c.amount,
					d.id as po_id,
					d.po_number,
					d.pub_shipment_date,
					d.grouping,
					d.plan_cut,
					e.item_number_id,
					e.color_number_id,
					sum(e.plan_cut_qnty) as plan_cut_qnty,
					sum(e.order_quantity) as order_quantity
					from 
					wo_po_details_master a,
					wo_pre_cost_mst b,
					wo_pre_cost_embe_cost_dtls c,
					wo_po_break_down d,
					wo_po_color_size_breakdown e
					where
					a.job_no=b.job_no and 
					a.job_no=c.job_no and 
					a.job_no=d.job_no_mst  and
					a.job_no=e.job_no_mst  and
					d.id=e.po_break_down_id
					$cbo_company_name_cond $txt_job_no_cond $cbo_buyer_name_cond and
					c.emb_name=$cbo_booking_natu and
					d.id in($txt_order_no_id) and
					e.item_number_id=$cbo_gmt_item and
					a.is_deleted=0 and 
					a.status_active=1 and
					b.is_deleted=0 and 
					b.status_active=1 and
					c.is_deleted=0 and 
					c.status_active=1 and
					d.is_deleted=0 and 
					d.status_active=1 and
					e.is_deleted=0 and 
					e.status_active=1
					group by
					b.costing_per,
					c.id,
					c.job_no,
					c.emb_name,
					c.emb_type,
					c.cons_dzn_gmts,
					c.rate,
					c.amount,
					d.id,
					d.po_number,
					d.pub_shipment_date,
					d.grouping,
					d.plan_cut,
					e.item_number_id,
					e.color_number_id
					order by d.id";
					$i=1;
                    $nameArray=sql_select( $sql );
					
                    foreach ($nameArray as $selectResult)
                    {
						if ($i%2==0)  
							$bgcolor="#E9F3FF";
						else
							$bgcolor="#FFFFFF";
							
						if($selectResult[csf('costing_per')]==1)
						{
							$costing_per_qty=12;
						}
						else if($selectResult[csf('costing_per')]==2)
						{
							$costing_per_qty=1;
						}
						else if($selectResult[csf('costing_per')]==3)
						{
							$costing_per_qty=24;
						}
						else if($selectResult[csf('costing_per')]==4)
						{
							$costing_per_qty=36;
						}
						else if($selectResult[csf('costing_per')]==5)
						{
							$costing_per_qty=48;
						}
						$booking_dtls_id=$booking_data_array[$selectResult[csf('po_id')]][$selectResult[csf('item_number_id')]][$selectResult[csf('wo_pre_cost_embe_cost_dtls')]][$selectResult[csf('color_number_id')]][id];
						$updateid.=$booking_dtls_id;

						$wo_qnty_pre=$booking_data_array[$selectResult[csf('po_id')]][$selectResult[csf('item_number_id')]][$selectResult[csf('wo_pre_cost_embe_cost_dtls')]][$selectResult[csf('color_number_id')]][wo_qnty];
						$wo_qnty_pre=def_number_format($wo_qnty_pre,2,"");
						
						$rate_pre=$booking_data_array[$selectResult[csf('po_id')]][$selectResult[csf('item_number_id')]][$selectResult[csf('wo_pre_cost_embe_cost_dtls')]][$selectResult[csf('color_number_id')]][rate];
						$rate_pre=def_number_format($rate_pre,5,"");
						
						$amount_pre=$booking_data_array[$selectResult[csf('po_id')]][$selectResult[csf('item_number_id')]][$selectResult[csf('wo_pre_cost_embe_cost_dtls')]][$selectResult[csf('color_number_id')]][amount];
						$amount_pre=def_number_format($amount_pre,5,"");
						
						$delivery_date=$booking_data_array[$selectResult[csf('po_id')]][$selectResult[csf('item_number_id')]][$selectResult[csf('wo_pre_cost_embe_cost_dtls')]][$selectResult[csf('color_number_id')]][delivery_date];
						$delivery_date=change_date_format($delivery_date,"dd-mm-yyyy","-");
						
						$description=$booking_data_array[$selectResult[csf('po_id')]][$selectResult[csf('item_number_id')]][$selectResult[csf('wo_pre_cost_embe_cost_dtls')]][$selectResult[csf('color_number_id')]][description];
						
						$cu_woq=$booking_data_array_cu[$selectResult[csf('po_id')]][$selectResult[csf('item_number_id')]][$selectResult[csf('wo_pre_cost_embe_cost_dtls')]][$selectResult[csf('emb_name')]][$selectResult[csf('emb_type')]][$selectResult[csf('color_number_id')]];
						$cu_woq=def_number_format($cu_woq-$wo_qnty_pre,2,"");
						
						if($calculation_basis==1)
						{
						  $req_qnty=def_number_format(((($selectResult[csf('cons_dzn_gmts')])*$selectResult[csf('order_quantity')])/12) ,2,"" );
						}
						else
						{
						  $req_qnty=def_number_format(((($selectResult[csf('cons_dzn_gmts')])*$selectResult[csf('order_quantity')])/12),2,"");
						}
						$bal_woq=def_number_format($req_qnty-$cu_woq,2,"");

						if($booking_dtls_id=="")
						{
						$wo_qnty_new=$bal_woq;
						$rate=def_number_format((($selectResult[csf('rate')]/$costing_per_qty)*12),5,"");
						$amount=def_number_format($rate*$wo_qnty_new,5,"");
						$delv_date=change_date_format(str_replace("'","",$txt_delivery_date),"dd-mm-yyyy","-");;
						
						}
						else
						{
						$wo_qnty_new=$wo_qnty_pre;
						$rate=$rate_pre;
						$amount=$amount_pre;
						$booking_date=$txt_booking_date;
						$delv_date=$delivery_date;
						}
	   ?>
                    <tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" id="search<?php echo $i;?>">
                    <td width="40">
					<?php echo $i;?>
                    <input type="hidden" id="txtbookingdtlasid_<?php echo $i;?>" value=" <?php echo $booking_dtls_id ;?>" style="width:40px" readonly/>
                    </td>
                   
                    <td width="90"><?php echo $selectResult[csf('po_number')];?> 
                    <input type="hidden" id="txtpoid_<?php echo $i;?>" value="<?php echo $selectResult[csf('po_id')];?>" readonly/>
                    </td>
                    <td width="100">
					<?php echo $garments_item[$selectResult[csf('item_number_id')]];?>
                    <input type="hidden" id="txtitemnumberid_<?php echo $i;?>" value="<?php echo $selectResult[csf('item_number_id')];?>" readonly />
                    </td>
                    <td width="90" >
                    <input type="hidden" id="txtembcostid_<?php echo $i;?>" value="<?php echo $selectResult[csf('wo_pre_cost_embe_cost_dtls')];?>" readonly/> 
                    <?php echo $emblishment_name_array[$selectResult[csf('emb_name')]];?>
                    </td>
                    <td width="100">
                    <?php
					if($cbo_booking_natu==1)
					{
						$emb_type=$emblishment_print_type[$selectResult[csf('emb_type')]];
					}
					if($cbo_booking_natu==2)
					{
						$emb_type=$emblishment_embroy_type[$selectResult[csf('emb_type')]];
					}
					if($cbo_booking_natu==3)
					{
						$emb_type=$emblishment_wash_type[$selectResult[csf('emb_type')]];
					}
					if($cbo_booking_natu==4)
					{
						$emb_type=$emblishment_spwork_type[$selectResult[csf('emb_type')]];
					}
					echo $emb_type;
					?> 
                    <input type="hidden" id="txtembtype_<?php echo $i;?>" value="<?php echo $selectResult[csf('emb_type')];?>" readonly/>
                    </td>
                    
                    <td width="80">
                    <?php echo $color_library[$selectResult[csf('color_number_id')]];?>
					<input type="hidden"  style="width:100%; font-family:Verdana, Geneva, sans-serif; font-size:11px; background-color:<?php echo $bgcolor; ?>" id="txtcolorid_<?php echo $i;?>" value="<?php echo $selectResult[csf('color_number_id')];?>"/>
                    </td>
                     <td width="60">
					<input type="text"  style="width:100%; font-family:Verdana, Geneva, sans-serif; text-align:right; font-size:11px; background-color:<?php echo $bgcolor; ?>" id="txtplancut_<?php echo $i;?>" value="<?php if($calculation_basis==1){echo $selectResult[csf('order_quantity')];}else{ echo $selectResult[csf('plan_cut_qnty')];}?>" readonly/>
                    </td>
                    <td width="200">
					<input type="text"  style="width:100%; font-family:Verdana, Geneva, sans-serif; font-size:11px; background-color:<?php echo $bgcolor; ?>" id="description_<?php echo $i;?>" value="<?php echo $description;?>" />
                    </td>
                    <td width="60">
					<input type="text"  style="width:100%; font-family:Verdana, Geneva, sans-serif; text-align:right; font-size:11px; background-color:<?php echo $bgcolor; ?>" id="txtreqqty_<?php echo $i;?>" value="<?php echo $req_qnty;?>" readonly/>
                    </td>
                    <td width="60" align="right">
                    <input type="text"  style="width:100%; font-family:Verdana, Geneva, sans-serif; font-size:11px;  text-align:right; background-color:<?php echo $bgcolor; ?>" id="txtcuwoq_<?php echo $i;?>" value="<?php echo $cu_woq;?>"  readonly  />
                    </td>
                    <td width="60" align="right">
                    <input type="text"  style="width:100%; font-family:Verdana, Geneva, sans-serif; font-size:11px;  text-align:right; background-color:<?php echo $bgcolor; ?>" id="txtbalqty_<?php echo $i;?>" value="<?php echo $bal_woq; ?>"  readonly  />
                    </td>
                    <td width="60" align="right">
					 <input type="hidden"  style="width:100%; font-family:Verdana, Geneva, sans-serif; font-size:11px;  text-align:right; background-color:<?php echo $bgcolor; ?>" id="txt_bal_qty_precost_<?php echo $i;?>" value="<?php echo def_number_format($wo_qnty_pre,5,"");?>" />
                    <input type="text"  style="width:100%; font-family:Verdana, Geneva, sans-serif; font-size:11px;  text-align:right; background-color:#FFC" id="txtwoq_<?php echo $i;?>" value="<?php echo $wo_qnty_new;?>" onChange="calculate_amount(<?php echo $i; ?>);calculate_amount2(<?php echo $i; ?>);" />
                    </td>
                    <td width="50" align="right"> 
                    <input type="text"  style="width:100%; font-family:Verdana, Geneva, sans-serif; font-size:11px;  text-align:right; background-color:<?php echo $bgcolor; ?>" id="txtrate_<?php echo $i;?>" value="<?php echo $rate;?>" onChange="calculate_amount(<?php echo $i; ?>)" />
                     <input type="hidden"  style="width:100%; font-family:Verdana, Geneva, sans-serif; font-size:11px;  text-align:right; background-color:<?php echo $bgcolor; ?>" id="txtrate_precost_<?php echo $i;?>" value="<?php echo def_number_format((($selectResult[csf('rate')]/$costing_per_qty)*12),5,"");?>" />
                    </td>
                    <td width="60" align="right">
                    
                    <input type="text"  style="width:100%; font-family:Verdana, Geneva, sans-serif; font-size:11px;  text-align:right; background-color:<?php echo $bgcolor; ?>" id="txtamount_<?php echo $i;?>"   value="<?php echo $amount; ?>"  readonly  />
                      <input type="hidden"  style="width:100%; font-family:Verdana, Geneva, sans-serif; font-size:11px;  text-align:right; background-color:<?php echo $bgcolor; ?>" id="txtamount_precost_<?php echo $i;?>" value="<?php echo def_number_format(($bal_woq*(($selectResult[csf('rate')]/$costing_per_qty)*12)),5,"");?>" />
                    </td>
                    <td width="" align="right">
                    <input type="text"   style="width:90%; font-family:Verdana, Geneva, sans-serif; font-size:11px;  text-align:right; background-color:<?php echo $bgcolor; ?>" id="txtddate_<?php echo $i;?>"  class="datepicker" value="<?php echo $delv_date; ?>"  readonly  />
                    </td>
                    </tr>
	   <?php
	   $i++;
					}
					
       ?>
	</tbody>
    </table>
    </div>
 
    <table width="1247" class="rpt_table" border="0" rules="all">
    <tfoot>
    <tr>
	   
                    <th width="40">SL</th>
                    <th width="90"></th>
                    <th width="100"></th>
                    <th width="90"></th>
                    <th width="100"></th>
                    <th width="80"></th>
                    <th width="60"></th>
                    <th width="200"></th>
                    <th width="60"></th>
                    <th width="60"></th>
                    <th width="60"></th>
                    <th width="60"></th>
                    <th width="50"></th>
                    <th width="60"></th>
                    <th width=""></th>               
   </tr>
   </tfoot>
   </table>
   <table width="1247" class="rpt_table" border="0" rules="all">
   <tr>
	   <td align="center" colspan="15" valign="middle" class="button_container">
                <?php 
				if($updateid=="")
				{
				echo load_submit_buttons( $permission, "fnc_fabric_booking_dtls", 0,0 ,"",2) ;
				}
				else
				{
				echo load_submit_buttons( $permission, "fnc_fabric_booking_dtls", 1,0 ,"",2) ;
				}
				?>
       </td>
                                
   </tr>
   </table>
       
   
<?php
}
if ($action=="show_print_booking")
{
	extract($_REQUEST);
	$cbo_company_name=str_replace("'","",$cbo_company_name);
	$cbo_buyer_name=str_replace("'","",$cbo_buyer_name);
	$txt_job_no=str_replace("'","",$txt_job_no);
	$txt_order_no_id=str_replace("'","",$txt_order_no_id);
	$cbo_booking_natu=str_replace("'","",$cbo_booking_natu);
	$cbo_gmt_item=str_replace("'","",$cbo_gmt_item);
	$txt_booking_no=str_replace("'","",$txt_booking_no);

     if($cbo_company_name==0)
	 {
		 $cbo_company_name_cond="";
	 }
	 else
	 {
		 $cbo_company_name_cond =" and a.company_name='$cbo_company_name'";
	 }
	 
	 
	 if($cbo_buyer_name==0)
	 {
		 $cbo_buyer_name_cond="";
	 }
	 else
	 {
		 $cbo_buyer_name_cond =" and a.buyer_name='$cbo_buyer_name'";
	 }
	 
	 if($txt_job_no=="")
	 {
		 $txt_job_no_cond="";
	 }
	 else
	 {
		 $txt_job_no_cond ="and a.job_no='$txt_job_no'";
	 }
	 
	 
	 
	 $booking_month=0;
	 if($cbo_booking_month<10)
	 {
		 $booking_month.=$cbo_booking_month;
	 }
	 else
	 {
		$booking_month=$cbo_booking_month; 
	 }
	$start_date=$cbo_booking_year."-".$booking_month."-01";
	$end_date=$cbo_booking_year."-".$booking_month."-".cal_days_in_month(CAL_GREGORIAN, $booking_month, $cbo_booking_year);
	?>
   <table cellspacing="0" cellpadding="0" border="1" rules="all" width="1047" class="rpt_table" >
                <thead>
                    <th width="40">SL</th>
                    <th width="90">Ord. No</th>
                    <th width="100">Gmt Item</th>
                    <th width="90">Booking Nature</th>
                    <th width="100">Booking Type</th>
                    <th width="80">Color</th>
                    <th width="60">Plan Cut</th>
                    <th width="60">Req Qty</th>
                    <th width="60">CU WOQ</th>
                    <th width="60">Bal Qty</th>
                    <th width="60">WOQ</th>
                    <th width="50">Rate</th>
                    <th width="60">Amount</th>
                    <th width="">Del. Date</th>
                </thead>
            </table>
            <div style="width:1047px; overflow-y:scroll; max-height:350px;" id="buyer_list_view" align="center">
            <table cellspacing="0" cellpadding="0" border="1" rules="all" width="1030" class="rpt_table" id="tbl_list_search" >
       <tbody>
       <?php
				   $booking_data_array_cu=array();
				   $sql_booking_cu= sql_select( "select b.po_break_down_id,b.gmt_item,b.pre_cost_fabric_cost_dtls_id,b.gmts_color_id,b.wo_qnty,sum(b.wo_qnty) as cu_woq ,c.emb_name,c.emb_type from wo_booking_mst a, wo_booking_dtls b,  wo_pre_cost_embe_cost_dtls c where a.job_no=b.job_no and  a.job_no=c.job_no and a.booking_no=b.booking_no and b.pre_cost_fabric_cost_dtls_id=c.id and a.job_no='$txt_job_no' and a.booking_type=6 and a.is_short=2 and  a.status_active=1  and 	a.is_deleted=0  group by b.po_break_down_id,b.gmt_item,b.pre_cost_fabric_cost_dtls_id,c.emb_name,c.emb_type,b.gmts_color_id  order by b.pre_cost_fabric_cost_dtls_id");
				   foreach($sql_booking_cu as $sql_booking_row_cu )
				   {
					  $booking_data_array_cu[$sql_booking_row_cu[csf('po_break_down_id')]][$sql_booking_row_cu[csf('gmt_item')]][$sql_booking_row_cu[csf('pre_cost_fabric_cost_dtls_id')]][$sql_booking_row_cu[csf('emb_name')]][$sql_booking_row_cu[csf('emb_type')]][$sql_booking_row_cu[csf('gmts_color_id')]][cu_woq]=$sql_booking_row_cu[csf('cu_woq')];

				   }
				  
				    $sql="select
					b.costing_per,
					c.id as wo_pre_cost_embe_cost_dtls,
					c.job_no,
					c.emb_name,
					c.emb_type,
					c.cons_dzn_gmts,
					c.rate,
					d.id as po_id,
					d.po_number,
					d.plan_cut,
					e.item_number_id,
					e.color_number_id,
					sum(e.plan_cut_qnty) as plan_cut_qnty,
					f.id as bookingdtlsid,
					f.wo_qnty as wo_qnty,
					f.rate as frate,
					f.amount as famount
					from 
					wo_po_details_master a,
					wo_pre_cost_mst b,
					wo_pre_cost_embe_cost_dtls c,
					wo_po_break_down d,
					wo_po_color_size_breakdown e,
					wo_booking_dtls f
					where
					a.job_no=b.job_no and 
					a.job_no=c.job_no and 
					a.job_no=d.job_no_mst  and
					a.job_no=e.job_no_mst  and
					a.job_no=f.job_no  and
					c.id=f.pre_cost_fabric_cost_dtls_id  and
					d.id=e.po_break_down_id and
					d.id=f.po_break_down_id and
					f.gmt_item=e.item_number_id and 
					f.gmts_color_id=e.color_number_id

					$cbo_company_name_cond $txt_job_no_cond $cbo_buyer_name_cond and
					c.emb_name=$cbo_booking_natu and
					d.id in($txt_order_no_id) and
					e.item_number_id=$cbo_gmt_item and
					f.booking_no='$txt_booking_no' and
					a.is_deleted=0 and 
					a.status_active=1 and
					b.is_deleted=0 and 
					b.status_active=1 and
					c.is_deleted=0 and 
					c.status_active=1 and
					d.is_deleted=0 and 
					d.status_active=1 and
					e.is_deleted=0 and 
					e.status_active=1
					group by 
					f.id,
					f.wo_qnty,
					f.rate,
					f.amount,
					e.item_number_id,
					e.color_number_id,
					d.id,
					d.po_number,
					d.plan_cut,
					c.id,
					c.job_no,
					c.emb_name,
					c.emb_type,
					c.cons_dzn_gmts,
					c.rate,
					b.costing_per
					order by d.id";
				   
	   
				    
					$i=1;
                    $nameArray=sql_select( $sql );
					
                    foreach ($nameArray as $selectResult)
                    {
						if ($i%2==0)  
							$bgcolor="#E9F3FF";
						else
							$bgcolor="#FFFFFF";
							
						if($selectResult[csf('costing_per')]==1)
						{
							$costing_per_qty=12;
						}
						else if($selectResult[csf('costing_per')]==2)
						{
							$costing_per_qty=1;
						}
						else if($selectResult[csf('costing_per')]==3)
						{
							$costing_per_qty=24;
						}
						else if($selectResult[csf('costing_per')]==4)
						{
							$costing_per_qty=36;
						}
						else if($selectResult[csf('costing_per')]==5)
						{
							$costing_per_qty=48;
						}
						
						$req_qnty=def_number_format(($selectResult[csf('cons_dzn_gmts')]*($selectResult[csf('plan_cut_qnty')]/12)),5,"");
						
						
						$wo_qnty=$selectResult[csf('wo_qnty')];
						$wo_qnty=def_number_format($wo_qnty,5,"");
						
						$cu_woq=$booking_data_array_cu[$selectResult[csf('po_id')]][$selectResult[csf('item_number_id')]][$selectResult[csf('wo_pre_cost_embe_cost_dtls')]][$selectResult[csf('emb_name')]][$selectResult[csf('emb_type')]][$selectResult[csf('color_number_id')]][cu_woq];
						$cu_woq=def_number_format($cu_woq-$wo_qnty,5,"");
						
						$bal_woq=def_number_format($req_qnty-$cu_woq,5,"");
						
						$rate=$selectResult[csf('frate')];
						$rate=def_number_format($rate,5,"");
						$pre_cost_rate=$selectResult[csf('rate')];
						$pre_cost_rate=def_number_format($pre_cost_rate,5,"");

						$amount=$selectResult[csf('famount')];
						
						$amount=def_number_format($amount,5,"");
					    //$total_amount+=$amount;
	   ?>
                    <tr bgcolor="<?php echo $bgcolor; ?>" style="text-decoration:none; cursor:pointer" id="search<?php echo $i;?>">
                    <td width="40">
					<?php echo $i;?>
                     <input type="hidden" id="txtbookingdtlasid_<?php echo $i;?>" value="<?php echo $selectResult[csf('bookingdtlsid')];?>" readonly/>
                    </td>
                   
                    <td width="90"><?php echo $selectResult[csf('po_number')];?> 
                    <input type="hidden" id="txtpoid_<?php echo $i;?>" value="<?php echo $selectResult[csf('po_id')];?>" readonly/>
                    </td>
                    <td width="100">
					<?php echo $garments_item[$selectResult[csf('item_number_id')]];?>
                    <input type="hidden" id="txtitemnumberid_<?php echo $i;?>" value="<?php echo $selectResult[csf('item_number_id')];?>" readonly />
                    </td>
                    <td width="90" >
                    <input type="hidden" id="txtembcostid_<?php echo $i;?>" value="<?php echo $selectResult[csf('wo_pre_cost_embe_cost_dtls')];?>" readonly/> 
                    <?php echo $emblishment_name_array[$selectResult[csf('emb_name')]];?>
                    </td>
                    <td width="100">
                    <?php
					if($cbo_booking_natu==1)
					{
						$emb_type=$emblishment_print_type[$selectResult[csf('emb_type')]];
					}
					if($cbo_booking_natu==2)
					{
						$emb_type=$emblishment_embroy_type[$selectResult[csf('emb_type')]];
					}
					if($cbo_booking_natu==3)
					{
						$emb_type=$emblishment_wash_type[$selectResult[csf('emb_type')]];
					}
					if($cbo_booking_natu==4)
					{
						$emb_type=$emblishment_spwork_type[$selectResult[csf('emb_type')]];
					}
					echo $emb_type;
					?> 
                    <input type="hidden" id="txtembtype_<?php echo $i;?>" value="<?php echo $selectResult[csf('emb_type')];?>" readonly/>
                    </td>
                    
                    <td width="80">
                    <?php echo $color_library[$selectResult[csf('color_number_id')]];?>
					<input type="hidden"  style="width:100%; font-family:Verdana, Geneva, sans-serif; font-size:11px; background-color:<?php echo $bgcolor; ?>" id="txtcolorid_<?php echo $i;?>" value="<?php echo $selectResult[csf('color_number_id')];?>"/>
                    </td>
                     <td width="60">
					<input type="text"  style="width:100%; font-family:Verdana, Geneva, sans-serif; text-align:right; font-size:11px; background-color:<?php echo $bgcolor; ?>" id="txtplancut_<?php echo $i;?>" value="<?php echo $selectResult[csf('plan_cut_qnty')];?>" readonly/>
                    </td>
                    <td width="60">
					<input type="text"  style="width:100%; font-family:Verdana, Geneva, sans-serif; text-align:right; font-size:11px; background-color:<?php echo $bgcolor; ?>" id="txtreqqty_<?php echo $i;?>" value="<?php echo $req_qnty;?>" readonly/>
                    </td>
                    <td width="60" align="right">
                    <input type="text"  style="width:100%; font-family:Verdana, Geneva, sans-serif; font-size:11px;  text-align:right; background-color:<?php echo $bgcolor; ?>" id="txtcuwoq_<?php echo $i;?>" value="<?php echo $cu_woq;?>"  readonly  />
                    </td>
                    <td width="60" align="right">
                    <input type="text"  style="width:100%; font-family:Verdana, Geneva, sans-serif; font-size:11px;  text-align:right; background-color:<?php echo $bgcolor; ?>" id="txtbalqty_<?php echo $i;?>" value="<?php echo $bal_woq; ?>"  readonly  />
                    </td>
                    <td width="60" align="right">
					
                    <input type="text"  style="width:100%; font-family:Verdana, Geneva, sans-serif; font-size:11px;  text-align:right; background-color:#FFC" id="txtwoq_<?php echo $i;?>" value="<?php echo $wo_qnty;?>" />
                    </td>
                    <td width="50" align="right"> 
                    <input type="text"  style="width:100%; font-family:Verdana, Geneva, sans-serif; font-size:11px;  text-align:right; background-color:<?php echo $bgcolor; ?>" id="txtrate_<?php echo $i;?>" value="<?php echo $rate;?>" onChange="calculate_amount(<?php echo $i; ?>)" />
                    <input type="hidden"  style="width:100%; font-family:Verdana, Geneva, sans-serif; font-size:11px;  text-align:right; background-color:<?php echo $bgcolor; ?>" id="txtrate_precost_<?php echo $i;?>" value="<?php echo $pre_cost_rate;?>" />

                    </td>
                    <td width="60" align="right">
                    
                    <input type="text"  style="width:100%; font-family:Verdana, Geneva, sans-serif; font-size:11px;  text-align:right; background-color:<?php echo $bgcolor; ?>" id="txtamount_<?php echo $i;?>" value="<?php echo $amount; ?>"  readonly  />
                    </td>
                    <td width="" align="right">
                    <input type="text"   style="width:90%; font-family:Verdana, Geneva, sans-serif; font-size:11px;  text-align:right; background-color:<?php echo $bgcolor; ?>" id="txtddate_<?php echo $i;?>"  class="datepicker" value="<?php echo $txt_delivery_date; ?>"  readonly  />
                    </td>
                    </tr>
	   <?php
	   $i++;
					}
       ?>
	</tbody>
    </table>
    </div>
 
    <table width="1047" class="rpt_table" border="0" rules="all">
    <tfoot>
    <tr>
	   
                    <th width="40">SL</th>
                    <th width="90"></th>
                    <th width="100"></th>
                    <th width="90"></th>
                    <th width="100"></th>
                    <th width="80"></th>
                    <th width="60"></th>
                    <th width="60"></th>
                    <th width="60"></th>
                    <th width="60"></th>
                    
                    <th width="60"></th>
                    <th width="50"></th>
                    <th width="60"></th>
                    <th width=""></th>               
   </tr>
   </tfoot>
   </table>
   <table width="1047" class="rpt_table" border="0" rules="all">
   <tr>
	   <td align="center" colspan="14" valign="middle" class="button_container">
                <?php echo load_submit_buttons( $permission, "fnc_fabric_booking_dtls", 1,0 ,"",2) ; ?>
       </td>
                                
   </tr>
   </table>
       
   
<?php
}
if ($action=="save_update_delete")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	if ($operation==0)  // Insert Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
		if($db_type==0)
		{
		$new_booking_no=explode("*",return_mrr_number( str_replace("'","",$cbo_company_name), '', 'EB', date("Y",time()), 5, "select booking_no_prefix, booking_no_prefix_num from wo_booking_mst where company_id=$cbo_company_name and booking_type=6 and YEAR(insert_date)=".date('Y',time())." order by booking_no_prefix_num desc ", "booking_no_prefix", "booking_no_prefix_num" ));
		}
		if($db_type==2)
		{
		$new_booking_no=explode("*",return_mrr_number( str_replace("'","",$cbo_company_name), '', 'EB', date("Y",time()), 5, "select booking_no_prefix, booking_no_prefix_num from wo_booking_mst where company_id=$cbo_company_name and booking_type=6 and to_char(insert_date,'YYYY')=".date('Y',time())." order by booking_no_prefix_num desc ", "booking_no_prefix", "booking_no_prefix_num" ));
		}
		
		$id=return_next_id( "id", "wo_booking_mst", 1 ) ;
		$field_array="id,booking_type,is_short,booking_no_prefix,booking_no_prefix_num,booking_no,company_id,buyer_id,job_no,currency_id,item_category,exchange_rate,pay_mode,source,booking_date,delivery_date,booking_month,booking_year,supplier_id,attention,ready_to_approved,inserted_by,insert_date,calculation_basis"; 
		 $data_array ="(".$id.",6,2,'".$new_booking_no[1]."',".$new_booking_no[2].",'".$new_booking_no[0]."',".$cbo_company_name.",".$cbo_buyer_name.",".$txt_job_no.",".$cbo_currency.",25,".$txt_exchange_rate.",".$cbo_pay_mode.",".$cbo_source.",".$txt_booking_date.",".$txt_delivery_date.",".$cbo_booking_month.",".$cbo_booking_year.",".$cbo_supplier_name.",".$txt_attention.",".$cbo_ready_to_approved.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."',".$calculation_basis.")";
		 $rID=sql_insert("wo_booking_mst",$field_array,$data_array,0);
		if($db_type==0)
		{
			if($rID){
				mysql_query("COMMIT");  
				echo "0**".$new_booking_no[0]."**".$id;
			}
			else{
				mysql_query("ROLLBACK"); 
				echo "10**".$new_booking_no[0]."**".$id;
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
			if($rID){
				oci_commit($con);  
				echo "0**".$new_booking_no[0]."**".$id;
			}
			else{
				oci_rollback($con);  
				echo "10**".$new_booking_no[0]."**".$id;
			}
		}
		disconnect($con);
		die;
	}
	
	else if ($operation==1)   // Update Here
	{
		
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		$field_array="company_id*buyer_id*job_no*currency_id*item_category*exchange_rate*pay_mode*source*booking_date*delivery_date*booking_month*booking_year*supplier_id*attention*ready_to_approved*updated_by*update_date*calculation_basis"; 
		 $data_array ="".$cbo_company_name."*".$cbo_buyer_name."*".$txt_job_no."*".$cbo_currency."*25*".$txt_exchange_rate."*".$cbo_pay_mode."*".$cbo_source."*".$txt_booking_date."*".$txt_delivery_date."*".$cbo_booking_month."*".$cbo_booking_year."*".$cbo_supplier_name."*".$txt_attention."*".$cbo_ready_to_approved."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'*".$calculation_basis."";
		$rID=sql_update("wo_booking_mst",$field_array,$data_array,"booking_no","".$txt_booking_no."",0);
		
		if($db_type==0)
		{
			if($rID ){
				mysql_query("COMMIT");  
				echo "1**".str_replace("'","",$txt_booking_no);
			}
			else{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'","",$txt_booking_no);
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
			if($rID ){
				oci_commit($con);  
				echo "1**".str_replace("'","",$txt_booking_no);
			}
			else{
				oci_rollback($con);  
				echo "10**".str_replace("'","",$txt_booking_no);
			}
		}
		disconnect($con);
		die;
	}
	else if ($operation==2)   // Delete Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
		$field_array="updated_by*update_date*status_active*is_deleted";
		$data_array="'".$_SESSION['logic_erp']['user_id']."'*'".$pc_date_time."'*'2'*'1'";
		$rID=sql_delete("wo_booking_mst",$field_array,$data_array,"id","".$update_id."",1);
		if($db_type==0)
		{
			if($rID ){
				mysql_query("COMMIT");  
				echo "2**".str_replace("'","",$txt_booking_no);
			}
			else{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'","",$txt_booking_no);
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
			if($rID ){
				oci_commit($con);    
				echo "2**".str_replace("'","",$txt_booking_no);
			}
			else{
				oci_rollback($con);  
				echo "10**".str_replace("'","",$txt_booking_no);
			}
		}
		disconnect($con);
		die;
		 
		
	}
}

if($action=="save_update_delete_dtls")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	if ($operation==0)  // Insert Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
		 if  ( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0"; die;}		
		 $id_dtls=return_next_id( "id", "wo_booking_dtls", 1 ) ;
		 $field_array1="id,pre_cost_fabric_cost_dtls_id,po_break_down_id,job_no,booking_no,booking_type,is_short,gmt_item,gmts_color_id,wo_qnty,rate,amount,delivery_date,description,inserted_by,insert_date";
		 for ($i=1;$i<=$total_row;$i++)
		 {
			 $txtbookingdtlasid="txtbookingdtlasid_".$i;
			 $txtpoid="txtpoid_".$i;
			 $txtitemnumberid="txtitemnumberid_".$i;
			 $txtembcostid="txtembcostid_".$i;
			 $txtcolorid="txtcolorid_".$i;
			 $txtwoq="txtwoq_".$i;
			 $txtrate="txtrate_".$i;
			 $txtamount="txtamount_".$i;
			 $txtddate="txtddate_".$i;
			 $description="description_".$i;
			 if ($i!=1) $data_array1 .=",";
			 $data_array1 .="(".$id_dtls.",".$$txtembcostid.",".$$txtpoid.",".$txt_job_no.",".$txt_booking_no.",6,2,".$$txtitemnumberid.",".$$txtcolorid.",".$$txtwoq.",".$$txtrate.",".$$txtamount.",".$$txtddate.",".$$description.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
			 $id_dtls=$id_dtls+1;
		 }
		 //echo "INSERT INTO wo_booking_dtls".$strTable." (".$field_array1.") VALUES ".$data_array1.""; 

		 //echo $data_array1 ; 
		 $rID=sql_insert("wo_booking_dtls",$field_array1,$data_array1,0);
		 check_table_status( $_SESSION['menu_id'],0);
		if($db_type==0)
		{
			if($rID ){
				mysql_query("COMMIT");  
				echo "0**".str_replace("'","",$txt_booking_no);
			}
			else{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'","",$txt_booking_no);
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
			if($rID ){
				oci_commit($con);  
				echo "0**".str_replace("'","",$txt_booking_no);
			}
			else{
				oci_rollback($con); 
				echo "10**".str_replace("'","",$txt_booking_no);
			}
		}
		disconnect($con);
		die;
	}
	if ($operation==1)  // Insert Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
	    if  ( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**1"; die;}		
		$field_array_up1="pre_cost_fabric_cost_dtls_id*po_break_down_id*job_no*booking_no*booking_type*is_short*gmt_item*gmts_color_id*wo_qnty*rate*amount*delivery_date*description*updated_by*update_date";
		
		 $add_comma=0;
		 $id_dtls=return_next_id( "id", "wo_booking_dtls", 1 ) ;
		 $field_array1="id,pre_cost_fabric_cost_dtls_id,po_break_down_id,job_no,booking_no,booking_type,is_short,gmt_item,gmts_color_id,wo_qnty,rate,amount,delivery_date,description,inserted_by,insert_date";
		 
		 for ($i=1;$i<=$total_row;$i++)
		 {
			$txtbookingdtlasid="txtbookingdtlasid_".$i;
			 $txtpoid="txtpoid_".$i;
			 $txtitemnumberid="txtitemnumberid_".$i;
			 $txtembcostid="txtembcostid_".$i;
			 $txtcolorid="txtcolorid_".$i;
			 $txtwoq="txtwoq_".$i;
			 $txtrate="txtrate_".$i;
			 $txtamount="txtamount_".$i;
			 $txtddate="txtddate_".$i;
			 $description="description_".$i;
			if(str_replace("'",'',$$txtbookingdtlasid)!="")
			{
				$id_arr[]=str_replace("'",'',$$txtbookingdtlasid);
				$data_array_up1[str_replace("'",'',$$txtbookingdtlasid)] =explode("*",("".$$txtembcostid."*".$$txtpoid."*".$txt_job_no."*".$txt_booking_no."*6*2*".$$txtitemnumberid."*".$$txtcolorid."*".$$txtwoq."*".$$txtrate."*".$$txtamount."*".$$txtddate."*".$$description."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'"));
			}
			if(str_replace("'",'',$$txtbookingdtlasid)=="")
			{
				if ($add_comma!=0) $data_array1 .=",";
			 $data_array1 .="(".$id_dtls.",".$$txtembcostid.",".$$txtpoid.",".$txt_job_no.",".$txt_booking_no.",6,2,".$$txtitemnumberid.",".$$txtcolorid.",".$$txtwoq.",".$$txtrate.",".$$txtamount.",".$$txtddate.",".$$description.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
				$id_dtls=$id_dtls+1;
				$add_comma++;
			}
		 }
		// echo bulk_update_sql_statement( "wo_booking_dtls", "id", $field_array_up1, $data_array_up1, $id_arr );
		$rID=execute_query(bulk_update_sql_statement( "wo_booking_dtls", "id", $field_array_up1, $data_array_up1, $id_arr ));
		$rID1=1;
		if($data_array1 !="")
		 {
			$rID1=sql_insert("wo_booking_dtls",$field_array1,$data_array1,0);
		 }

        check_table_status( $_SESSION['menu_id'],0);
		if($db_type==0)
		{
			if($rID && $rID1){
				mysql_query("COMMIT");  
				echo "1**".str_replace("'","",$txt_booking_no);
			}
			else{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'","",$txt_booking_no);
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
			if($rID && $rID1){
				oci_commit($con);  
				echo "1**".str_replace("'","",$txt_booking_no);
			}
			else{
				oci_rollback($con);  
				echo "10**".str_replace("'","",$txt_booking_no);
			}
		}
		disconnect($con);
		die;
	}
	if ($operation==2)  // Insert Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		$rID=execute_query( "update wo_booking_dtls set status_active=0,is_deleted =1 where  id =$update_id_details",0);	
			
		if($db_type==0)
		{
			if($rID ){
				mysql_query("COMMIT");  
				echo "2**".str_replace("'","",$txt_booking_no);
			}
			else{
				mysql_query("ROLLBACK"); 
				echo "10**".str_replace("'","",$txt_booking_no);
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
			if($rID ){
				oci_commit($con);  
				echo "2**".str_replace("'","",$txt_booking_no);
			}
			else{
				oci_rollback($con);  
				echo "10**".str_replace("'","",$txt_booking_no);
			}
		}
		disconnect($con);
		die;
	}
}

if ($action=="print_booking_list_view")
{
	$approved=array(0=>"No",1=>"Yes");
	$is_ready=array(0=>"No",1=>"Yes",2=>"No");
	$buyer_arr=return_library_array( "select id, short_name from lib_buyer",'id','short_name');
	$comp=return_library_array( "select id, company_short_name from lib_company",'id','company_short_name');
	$suplier=return_library_array( "select id, short_name from lib_supplier",'id','short_name');
	$po_num=return_library_array( "select job_no, job_no_prefix_num from wo_po_details_master",'job_no','job_no_prefix_num');
	$po_array=return_library_array( "select id, po_number from wo_po_break_down",'id','po_number');

	$arr=array (2=>$comp,3=>$buyer_arr,4=>$po_num,5=>$po_array,6=>$garments_item,7=>$emblishment_name_array,8=>$suplier,9=>$approved,10=>$is_ready);
	
	  $sql= "select a.booking_no_prefix_num, a.booking_no,a.booking_date,company_id,a.buyer_id,a.job_no,b.po_break_down_id,b.gmt_item,c.emb_name,a.supplier_id,a.is_approved,a.ready_to_approved from wo_booking_mst a, wo_booking_dtls b,  wo_pre_cost_embe_cost_dtls c where a.job_no=b.job_no and  a.job_no=c.job_no and a.booking_no=b.booking_no and b.pre_cost_fabric_cost_dtls_id=c.id and a.booking_no='$data' and a.booking_type=6 and a.is_short=2 and  a.status_active=1  and 	a.is_deleted=0 group by a.booking_no_prefix_num,a.booking_no,a.booking_date,company_id,a.buyer_id,a.job_no,b.po_break_down_id,b.gmt_item,c.emb_name,a.supplier_id,a.is_approved,a.ready_to_approved order by a.booking_no";
	
	echo  create_list_view("list_view", "Booking No,Booking Date,Company,Buyer,Job No.,PO number,Gmts Item,Embl Name,Supplier,Approved,Is-Ready", "80,80,70,100,90,200,80,80,50,50","1020","320",0, $sql , "update_booking_data", "po_break_down_id,gmt_item,emb_name", "", 1, "0,0,company_id,buyer_id,job_no,po_break_down_id,gmt_item,emb_name,supplier_id,is_approved,ready_to_approved", $arr , "booking_no_prefix_num,booking_date,company_id,buyer_id,job_no,po_break_down_id,gmt_item,emb_name,supplier_id,is_approved,ready_to_approved", '','','0,0,0,0,0,0,0,0,0,0,0','','');
}

if ($action=="fabric_booking_popup")
{
  	echo load_html_head_contents("Booking Search","../../../", 1, 1, $unicode);
?>
     
	<script>
	 
	function js_set_value(booking_no)
	{
		document.getElementById('selected_booking').value=booking_no;
		parent.emailwindow.hide();
	}
	
    </script>

</head>

<body>
<div align="center" style="width:100%;" >
<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
	<table width="750" cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
    	<tr>
        	<td align="center" width="100%">
            	<table  cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
                    <thead>
                           <th colspan="2"> </th>
                        	<th  >
                              <?php
                               echo create_drop_down( "cbo_search_category", 130, $string_search_type,'', 1, "-- Search Catagory --" );
                              ?>
                            </th>
                            <th colspan="3"></th>
                    </thead>
                    <thead>                	 
                        <th width="150">Company Name</th>
                        <th width="150">Buyer Name</th>
                         <th width="100">Booking No</th>
                        <th width="200">Date Range</th>
                        <th></th>           
                    </thead>
        			<tr>
                    	<td> <input type="hidden" id="selected_booking">
							<?php 
								echo create_drop_down( "cbo_company_mst", 150, "select id,company_name from lib_company comp where status_active=1 $company_cond order by company_name","id,company_name",1, "-- Select Company --", '', "load_drop_down( 'print_booking_controller', this.value, 'load_drop_down_buyer', 'buyer_td' );");
							?>
                        </td>
                   
                   	<td id="buyer_td">
                     <?php 
						echo create_drop_down( "cbo_buyer_name", 172, $blank_array,"", 1, "-- Select Buyer --" );
					?>	</td>
                     <td><input name="txt_booking_prifix" id="txt_booking_prifix" class="text_boxes" style="width:100px"></td>
                    <td><input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px">
					  <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px">
					 </td> 
            		 <td align="center">
                     	<input type="button" name="button2" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_company_mst').value+'_'+document.getElementById('cbo_buyer_name').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value+'_'+document.getElementById('cbo_year_selection').value+'_'+document.getElementById('txt_booking_prifix').value+'_'+document.getElementById('cbo_search_category').value, 'create_booking_search_list_view', 'search_div', 'print_booking_controller','setFilterGrid(\'list_view\',-1)')" style="width:100px;" /></td>
        		</tr>
             </table>
          </td>
        </tr>
        <tr>
            <td  align="center" height="40" valign="middle">
			<?php 
			echo create_drop_down( "cbo_year_selection", 70, $year,"", 1, "-- Select --", date('Y'), "",0 );	
			echo load_month_buttons(); 
			
			?>
            </td>
            </tr>
        <tr>
            <td align="center"valign="top" id="search_div"> 
	
            </td>
        </tr>
    </table>    
    
    </form>
   </div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}

if ($action=="create_booking_search_list_view")
{
	$data=explode('_',$data);
	if ($data[0]!=0) $company="  a.company_id='$data[0]'"; else { echo "Please Select Company First."; die; }
	if ($data[1]!=0) $buyer=" and a.buyer_id='$data[1]'"; else $buyer="";//{ echo "Please Select Buyer First."; die; }
	if($db_type==0)
	{
	$booking_year_cond=" and SUBSTRING_INDEX(a.insert_date, '-', 1)=$data[4]";
	if ($data[2]!="" &&  $data[3]!="") $booking_date  = "and a.booking_date  between '".change_date_format($data[2], "yyyy-mm-dd", "-")."' and '".change_date_format($data[3], "yyyy-mm-dd", "-")."'"; else $booking_date ="";
	}
	if($db_type==2)
	{
	$booking_year_cond=" and to_char(a.insert_date,'YYYY')=$data[4]";	
	if ($data[2]!="" &&  $data[3]!="") $booking_date  = "and a.booking_date  between '".change_date_format($data[2], "yyyy-mm-dd", "-",1)."' and '".change_date_format($data[3], "yyyy-mm-dd", "-",1)."'"; else $booking_date ="";
	}
	if($data[6]==4 || $data[6]==0)
		{
		 if (str_replace("'","",$data[5])!="") $booking_cond=" and a.booking_no_prefix_num like '%$data[5]%'  $booking_year_cond  "; else $booking_cond="";
		}
    if($data[6]==1)
		{
		if (str_replace("'","",$data[5])!="") $booking_cond=" and a.booking_no_prefix_num ='$data[5]' "; else $booking_cond="";
		}
   if($data[6]==2)
		{
		if (str_replace("'","",$data[5])!="") $booking_cond=" and a.booking_no_prefix_num like '$data[5]%'  $booking_year_cond  "; else $booking_cond="";
		}
	if($data[6]==3)
		{
		if (str_replace("'","",$data[5])!="") $booking_cond=" and a.booking_no_prefix_num like '%$data[5]'  $booking_year_cond  "; else $booking_cond="";
		}
	$approved=array(0=>"No",1=>"Yes");
	$is_ready=array(0=>"No",1=>"Yes",2=>"No");
	$buyer_arr=return_library_array( "select id, short_name from lib_buyer",'id','short_name');
	$comp=return_library_array( "select id, company_short_name from lib_company",'id','company_short_name');
	$suplier=return_library_array( "select id, short_name from lib_supplier",'id','short_name');
	$po_num=return_library_array( "select job_no, job_no_prefix_num from wo_po_details_master",'job_no','job_no_prefix_num');
	$po_array=return_library_array( "select id, po_number from wo_po_break_down",'id','po_number');

	//$arr=array (2=>$comp,3=>$buyer_arr,4=>$po_num,5=>$po_array,6=>$item_category,7=>$fabric_source,8=>$suplier,9=>$approved,10=>$is_ready);
	//$sql= "select booking_no_prefix_num, booking_no,booking_date,company_id,buyer_id,job_no,po_break_down_id,item_category,fabric_source,supplier_id,is_approved,ready_to_approved from wo_booking_mst  where $company $buyer $booking_date and booking_type=6 and is_short=2 and  status_active=1  and 	is_deleted=0 order by booking_no"; 

	//echo  create_list_view("list_view", "WO No,WO Date,Company,Buyer,Job No.,PO number,Fabric Nature,Fabric Source,Supplier,Approved,Is-Ready", "80,80,70,100,90,200,80,80,50,50","1020","320",0, $sql , "js_set_value", "booking_no", "", 1, "0,0,company_id,buyer_id,job_no,po_break_down_id,item_category,fabric_source,supplier_id,is_approved,ready_to_approved", $arr , "booking_no_prefix_num,booking_date,company_id,buyer_id,job_no,po_break_down_id,item_category,fabric_source,supplier_id,is_approved,ready_to_approved", '','','0,0,0,0,0,0,0,0,0,0,0','','');
	 $arr=array (2=>$comp,3=>$buyer_arr,4=>$po_num,5=>$po_array,6=>$garments_item,7=>$emblishment_name_array,8=>$suplier,9=>$approved,10=>$is_ready);
	 $sql= "select a.booking_no_prefix_num, a.booking_no,a.booking_date,company_id,a.buyer_id,a.job_no,b.po_break_down_id,b.gmt_item,c.emb_name,a.supplier_id,a.is_approved,a.ready_to_approved from wo_booking_mst a, wo_booking_dtls b,  wo_pre_cost_embe_cost_dtls c where $company $buyer $booking_date $booking_cond and  a.job_no=b.job_no and  a.job_no=c.job_no and a.booking_no=b.booking_no and b.pre_cost_fabric_cost_dtls_id=c.id  and a.booking_type=6 and a.is_short=2 and  a.status_active=1  and 	a.is_deleted=0 group by a.booking_no_prefix_num, a.booking_no,a.booking_date,company_id,a.buyer_id,a.job_no,b.po_break_down_id,b.gmt_item,c.emb_name,a.supplier_id,a.is_approved,a.ready_to_approved order by a.booking_no";
	
	 echo  create_list_view("list_view", "Booking No,Booking Date,Company,Buyer,Job No.,PO number,Gmts Item,Embl Name,Supplier,Approved,Is-Ready", "80,80,70,100,90,200,80,80,50,50","1020","320",0, $sql , "js_set_value", "booking_no", "", 1, "0,0,company_id,buyer_id,job_no,po_break_down_id,gmt_item,emb_name,supplier_id,is_approved,ready_to_approved", $arr , "booking_no_prefix_num,booking_date,company_id,buyer_id,job_no,po_break_down_id,gmt_item,emb_name,supplier_id,is_approved,ready_to_approved", '','','0,3,0,0,0,0,0,0,0,0,0','','');
}

if ($action=="populate_data_from_search_popup")
{
	 $sql= "select booking_no,booking_date,company_id,buyer_id,job_no,currency_id,exchange_rate,pay_mode,booking_month,supplier_id,attention,delivery_date,source,booking_year,is_approved,ready_to_approved,calculation_basis from wo_booking_mst  where booking_no='$data'"; 
	
	 $data_array=sql_select($sql);
	 foreach ($data_array as $row)
	 {
		echo "document.getElementById('cbo_company_name').value = '".$row[csf("company_id")]."';\n";  
		echo "document.getElementById('cbo_buyer_name').value = '".$row[csf("buyer_id")]."';\n";  
		echo "document.getElementById('txt_job_no').value = '".$row[csf("job_no")]."';\n";
		echo "document.getElementById('txt_booking_no').value = '".$row[csf("booking_no")]."';\n";
		echo "document.getElementById('cbo_currency').value = '".$row[csf("currency_id")]."';\n";
		echo "document.getElementById('txt_exchange_rate').value = '".$row[csf("exchange_rate")]."';\n";
		echo "document.getElementById('cbo_pay_mode').value = '".$row[csf("pay_mode")]."';\n";
		echo "document.getElementById('txt_booking_date').value = '".change_date_format($row[csf("booking_date")],'dd-mm-yyyy','-')."';\n";
		echo "document.getElementById('cbo_booking_month').value = '".$row[csf("booking_month")]."';\n";
		echo "document.getElementById('cbo_supplier_name').value = '".$row[csf("supplier_id")]."';\n";
		echo "document.getElementById('txt_attention').value = '".$row[csf("attention")]."';\n";
		echo "document.getElementById('txt_delivery_date').value = '".change_date_format($row[csf("delivery_date")],'dd-mm-yyyy','-')."';\n";
	    echo "document.getElementById('cbo_source').value = '".$row[csf("source")]."';\n";
		echo "document.getElementById('cbo_booking_year').value = '".$row[csf("booking_year")]."';\n";
		echo "document.getElementById('id_approved_id').value = '".$row[csf("is_approved")]."';\n";
		echo "document.getElementById('cbo_ready_to_approved').value = '".$row[csf("ready_to_approved")]."';\n";
		echo "document.getElementById('calculation_basis').value = '".$row[csf("calculation_basis")]."';\n";

		if($row[csf("is_approved")]==1)
		{
			///echo "document.getElementById('app_sms').innerHTML = 'This booking is approved';\n";
			echo "document.getElementById('app_sms2').innerHTML = 'This booking is approved';\n";
		}
		else
		{
			//echo "document.getElementById('app_sms').innerHTML = '';\n";
			echo "document.getElementById('app_sms2').innerHTML = '';\n";
		}
	 }
}







if($action=="terms_condition_popup")
{
	echo load_html_head_contents("Order Search","../../../", 1, 1, $unicode);
	extract($_REQUEST);
?>
	<script>
function add_break_down_tr(i) 
 {
	var row_num=$('#tbl_termcondi_details tr').length-1;
	if (row_num!=i)
	{
		return false;
	}
	else
	{
		i++;
	 
		 $("#tbl_termcondi_details tr:last").clone().find("input,select").each(function() {
			$(this).attr({
			  'id': function(_, id) { var id=id.split("_"); return id[0] +"_"+ i },
			  'name': function(_, name) { return name + i },
			  'value': function(_, value) { return value }              
			});  
		  }).end().appendTo("#tbl_termcondi_details");
		 $('#increase_'+i).removeAttr("onClick").attr("onClick","add_break_down_tr("+i+");");
		  $('#decrease_'+i).removeAttr("onClick").attr("onClick","fn_deletebreak_down_tr("+i+")");
		  $('#termscondition_'+i).val("");
	}
		  
}

function fn_deletebreak_down_tr(rowNo) 
{   
	
	
		var numRow = $('table#tbl_termcondi_details tbody tr').length; 
		if(numRow==rowNo && rowNo!=1)
		{
			$('#tbl_termcondi_details tbody tr:last').remove();
		}
	
}

function fnc_fabric_booking_terms_condition( operation )
{
	    var row_num=$('#tbl_termcondi_details tr').length-1;
		var data_all="";
		for (var i=1; i<=row_num; i++)
		{
			
			if (form_validation('termscondition_'+i,'Term Condition')==false)
			{
				return;
			}
			
			data_all=data_all+get_submitted_data_string('txt_booking_no*termscondition_'+i,"");
		}
		var data="action=save_update_delete_fabric_booking_terms_condition&operation="+operation+'&total_row='+row_num+data_all;
		//freeze_window(operation);
		http.open("POST","print_booking_controller.php",true);
		http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		http.send(data);
		http.onreadystatechange = fnc_fabric_booking_terms_condition_reponse;
}

function fnc_fabric_booking_terms_condition_reponse()
{
	
	if(http.readyState == 4) 
	{
	    var reponse=trim(http.responseText).split('**');
			if (reponse[0].length>2) reponse[0]=10;
			if(reponse[0]==0 || reponse[0]==1)
			{
				parent.emailwindow.hide();
			}
	}
}
    </script>

</head>

<body>
<div align="center" style="width:100%;" >
 <?php echo load_freeze_divs ("../../../",$permission);  ?>
<fieldset>
        	<form id="termscondi_1" autocomplete="off">
           <input type="hidden" id="txt_booking_no" name="txt_booking_no" value="<?php echo str_replace("'","",$txt_booking_no) ?>"/>
            
            
            <table width="650" cellspacing="0" class="rpt_table" border="0" id="tbl_termcondi_details" rules="all">
                	<thead>
                    	<tr>
                        	<th width="50">Sl</th><th width="530">Terms</th><th ></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
					$data_array=sql_select("select id, terms from  wo_booking_terms_condition where booking_no=$txt_booking_no");// quotation_id='$data'
					if ( count($data_array)>0)
					{
						$i=0;
						foreach( $data_array as $row )
						{
							$i++;
							?>
                            	<tr id="settr_1" align="center">
                                    <td>
                                    <?php echo $i;?>
                                    </td>
                                    <td>
                                    <input type="text" id="termscondition_<?php echo $i;?>"   name="termscondition_<?php echo $i;?>" style="width:95%"  class="text_boxes"  value="<?php echo $row[csf('terms')]; ?>"  /> 
                                    </td>
                                    <td> 
                                    <input type="button" id="increase_<?php echo $i; ?>" style="width:30px" class="formbutton" value="+" onClick="add_break_down_tr(<?php echo $i; ?> )" />
                                    <input type="button" id="decrease_<?php echo $i; ?>" style="width:30px" class="formbutton" value="-" onClick="javascript:fn_deletebreak_down_tr(<?php echo $i; ?>);" />
                                    </td>
                                </tr>
                            <?php
						}
					}
					else
					{
					$data_array=sql_select("select id, terms from  lib_terms_condition where is_default=1");// quotation_id='$data'
					foreach( $data_array as $row )
						{
							$i++;
					?>
                    <tr id="settr_1" align="center">
                                    <td>
                                    <?php echo $i;?>
                                    </td>
                                    <td>
                                    <input type="text" id="termscondition_<?php echo $i;?>"   name="termscondition_<?php echo $i;?>" style="width:95%"  class="text_boxes"  value="<?php echo $row[csf('terms')]; ?>"  /> 
                                    </td>
                                    <td>
                                    <input type="button" id="increase_<?php echo $i; ?>" style="width:30px" class="formbutton" value="+" onClick="add_break_down_tr(<?php echo $i; ?> )" />
                                    <input type="button" id="decrease_<?php echo $i; ?>" style="width:30px" class="formbutton" value="-" onClick="javascript:fn_deletebreak_down_tr(<?php echo $i; ?> );" />
                                    </td>
                                </tr>
                    <?php 
						}
					} 
					?>
                </tbody>
                </table>
                
                <table width="650" cellspacing="0" class="" border="0">
                	<tr>
                        <td align="center" height="15" width="100%"> </td>
                    </tr>
                	<tr>
                        <td align="center" width="100%" class="button_container">
						        <?php
									echo load_submit_buttons( $permission, "fnc_fabric_booking_terms_condition", 0,0 ,"reset_form('termscondi_1','','','','')",1) ; 
									?>
                        </td> 
                    </tr>
                </table>
            </form>
        </fieldset>
</div>
</body>           
<script src="../../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}

if($action=="save_update_delete_fabric_booking_terms_condition")
{
$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	
	if ($operation==0)  // Insert Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		
		if  ( check_table_status( $_SESSION['menu_id'], 1 )==0 ) { echo "15**0"; die;}		
		 $id=return_next_id( "id", "wo_booking_terms_condition", 1 ) ;
		 $field_array="id,booking_no,terms";
		 for ($i=1;$i<=$total_row;$i++)
		 {
			 $termscondition="termscondition_".$i;
			if ($i!=1) $data_array .=",";
			$data_array .="(".$id.",".$txt_booking_no.",".$$termscondition.")";
			$id=$id+1;
		 }
		// echo  $data_array;
		$rID_de3=execute_query( "delete from wo_booking_terms_condition where  booking_no =".$txt_booking_no."",0);

		 $rID=sql_insert("wo_booking_terms_condition",$field_array,$data_array,1);
		 check_table_status( $_SESSION['menu_id'],0);
		if($db_type==0)
		{
			if($rID ){
				mysql_query("COMMIT");  
				echo "0**".$new_booking_no[0];
			}
			else{
				mysql_query("ROLLBACK"); 
				echo "10**".$new_booking_no[0];
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
			if($rID ){
				oci_commit($con);  
				echo "0**".$new_booking_no[0];
			}
			else{
				oci_rollback($con);  
				echo "10**".$new_booking_no[0];
			}
		}
		disconnect($con);
		die;
	}	
}
if($action=="show_trim_booking_report")
{
	//echo "fgfgfffgf";die;
 extract($_REQUEST);
//$data=explode('*',$data);
//$txt_booking_no=$data[0];
$txt_booking_no=str_replace("'","",$txt_booking_no);
$cbo_company_name=str_replace("'","",$cbo_company_name);
$cbo_supplier_name=str_replace("'","",$cbo_supplier_name);
//echo $cbo_supplier_name;die;
$txt_job_no=str_replace("'","",$txt_job_no);
$txt_booking_date=str_replace("'","",$txt_booking_date);
$txt_delivery_date=str_replace("'","",$txt_delivery_date);
$cbo_buyer_name=str_replace("'","",$cbo_buyer_name);
//$txt_booking_no=str_replace("'","",$txt_booking_no);
$cbo_currency=str_replace("'","",$cbo_currency);
$cbo_pay_mode=str_replace("'","",$cbo_pay_mode);
$cbo_source=str_replace("'","",$cbo_source);
$txt_exchange_rate=str_replace("'","",$txt_exchange_rate);

$company_library=return_library_array( "select id, company_name from lib_company", "id", "company_name"  );
$buyer_name= return_library_array("select id, short_name from  lib_buyer","id","short_name");
$supplier_name=return_library_array("select id,supplier_name from  lib_supplier", "id", "supplier_name");
$color_id=return_library_array("select id,color_name from lib_color", "id", "color_name");
$order_arr = return_library_array( "select id, po_number from wo_po_break_down",'id','po_number');
?>
<div style="width:1100px;" align="center">
 <table width="1100" cellspacing="0" align="center" border="0" >
        <tr>
            <td colspan="6" align="center" style="font-size:28px"><strong><?php echo $company_library[$cbo_company_name]; ?></strong></td>
        </tr>
        <tr class="form_caption">
            <td colspan="6" align="center">
				<?php 		
					
					$style_ref_no=sql_select("SELECT c.id, d.style_ref_no FROM wo_po_break_down c,wo_po_details_master d WHERE d.job_no=c.job_no_mst and d.job_no='$txt_job_no' and c.status_active=1 and c.is_deleted=0 and d.status_active=1 and d.is_deleted=0 group by c.id");
					$nameArray=sql_select( "select plot_no,level_no,road_no,block_no,country_id,province,city,zip_code,email,website from lib_company where id=$cbo_company_name"); 
					foreach ($nameArray as $result)
					{ 
					?>
						Plot No: <?php echo $result[csf('plot_no')]; ?> 
						Level No: <?php echo $result[csf('level_no')]?>
						Road No: <?php echo $result[csf('road_no')]; ?> 
						Block No: <?php echo $result[csf('block_no')];?> 
						City No: <?php echo $result[csf('city')];?> 
						Zip Code: <?php echo $result[csf('zip_code')]; ?> 
						Province No: <?php echo $result[csf('province')];?> 
						Country: <?php echo $country_arr[$result[csf('country_id')]]; ?><br> 
						Email Address: <?php echo $result[csf('email')];?> 
						Website No: <?php echo $result[csf('website')];
					}
					$nameArray_sup=sql_select( "select address_1 from lib_supplier where id=$cbo_supplier_name");
                ?> 
            </td>
        </tr>
        <tr>
            <td colspan="6" align="center" style="font-size:20px"><strong>Embellishment Work Order</strong></td>
        </tr>
         <tr>
            <td colspan="6" align="center" style="font-size:20">&nbsp;</td>
        </tr>
         <tr>
            <td style="font-size:20" width="130" >Supplier Name </td> <td style="font-size:20"  width="170">: &nbsp;<?php echo $supplier_name[$cbo_supplier_name]; ?></td>
            <td style="font-size:20">Work Order No</td> <td style="font-size:20">: &nbsp;<?php echo $txt_booking_no; //change_date_format($txt_booking_date); ?></td>
            <td style="font-size:20">Work Order Date  </td> <td style="font-size:20">: &nbsp;<?php echo change_date_format($txt_booking_date); ?></td>
        </tr>
         <tr>
            <td style="font-size:20">Supplier Address  </td> <td style="font-size:20">: &nbsp;<?php echo $nameArray_sup[0][csf('address_1')]; ?></td>
            <td style="font-size:20"> Job No. </td> <td style="font-size:20">: &nbsp;<?php echo $txt_job_no; ?></td>
           
            <td style="font-size:20">Delivery Date </td> <td style="font-size:20">: &nbsp;<?php echo change_date_format($txt_delivery_date); ?></td>
        </tr>
        <tr>
            <td style="font-size:20"> Buyer  </td> <td style="font-size:20">: &nbsp;<?php echo $buyer_name[$cbo_buyer_name]; ?></td>
            <td style="font-size:20">Currency </td> <td style="font-size:20">: &nbsp;<?php echo $currency[$cbo_currency]; ?></td>
            <td style="font-size:20">Exchange Rate  </td> <td style="font-size:20">: &nbsp;<?php echo $txt_exchange_rate; ?></td>
        </tr>
        <tr>
            <td style="font-size:20">Style Ref.  </td> <td style="font-size:20">: &nbsp;<?php echo $style_ref_no[0][csf('style_ref_no')]; ?></td>
            <td style="font-size:20"> Pay mode </td> <td style="font-size:20">: &nbsp;<?php echo $pay_mode[$cbo_pay_mode]; ?></td>
            <td style="font-size:20">Source </td>  <td style="font-size:20">: &nbsp;<?php echo $source[$cbo_source]; ?></td> 
        </tr>
          <tr>
          <td style="font-size:20">Article No</td> 
          <td style="font-size:20">: &nbsp;
		  <?php 
		  
		  $nameArray_article_number=sql_select( "select article_number from  wo_po_color_size_breakdown where job_no_mst='$txt_job_no' and article_number!=0 and is_deleted=0 and status_active=1");
		  echo $nameArray_article_number[0][csf('article_number')]; 
		  ?>
          </td>
          
          <td style="font-size:20">Image:</td> 
            <td colspan="3"> 
			<?php
			$data_array=sql_select("select image_location  from common_photo_library  where master_tble_id='$txt_job_no' and form_name='knit_order_entry' and is_deleted=0 and file_type=1");
			foreach($data_array as $row)
			{?>
				<img src='../../<?php echo $row[csf('image_location')]; ?>' height='65' width='90' align="middle" />
			<?php 
			}?>
           </td>
        </tr>
        
 </table><br/>
 <div style="width:100%;">
 <?php
 $costing_per=return_field_value("costing_per", "wo_pre_cost_mst", "job_no='$txt_job_no'");
	if($costing_per==1)
	{
		$costing_per_dzn="1 Dzn";
	}
	else if($costing_per==2)
	{
		$costing_per_dzn="1 Pcs";
	}
	else if($costing_per==3)
	{
		$costing_per_dzn="2 Dzn";
	}
	else if($costing_per==4)
	{
		$costing_per_dzn="3 Dzn";
	}
	else if($costing_per==5)
	{
		$costing_per_dzn="4 Dzn";
	}
 ?>
 <table width="1100" cellspacing="0" align="center" border="1" rules="all" class="rpt_table" >
      <thead bgcolor="#dddddd" align="center">
         <th width="30">SL</th>
         <th width="100">Order No.</th>
         <th width="80">Emb. Name</th>
         <th width="80">Emb. type</th>
         <th width="150">Description</th>
         <th width="80">Color</th>
         <th width="80">Plan Cut(PCS)</th>
         <th width="100">Req. Qty /DZN</th>
         <th width="80">CU WOQ/DZN</th>
         <th width="100">Bal Qty/DZN</th>
         <th width="80">WQO/DZN</th>
         <th width="70">Rate/DZN</th>
         <th width="70">Amount/DZN</th>
      </thead>
      <?php
	   			   $booking_data_array_cu=array();
				   $sql_booking_cu= sql_select( "select b.po_break_down_id,b.gmt_item,b.pre_cost_fabric_cost_dtls_id,b.gmts_color_id,b.wo_qnty,sum(b.wo_qnty) as cu_woq ,c.emb_name,c.emb_type from wo_booking_mst a, wo_booking_dtls b,  wo_pre_cost_embe_cost_dtls c where a.job_no=b.job_no and  a.job_no=c.job_no and a.booking_no=b.booking_no and b.pre_cost_fabric_cost_dtls_id=c.id and a.job_no='$txt_job_no' and a.booking_type=6 and a.is_short=2 and  a.status_active=1  and 	a.is_deleted=0  group by b.po_break_down_id,b.gmt_item,b.pre_cost_fabric_cost_dtls_id,c.emb_name,c.emb_type,b.gmts_color_id,b.wo_qnty  order by b.pre_cost_fabric_cost_dtls_id");
				   foreach($sql_booking_cu as $sql_booking_row_cu )
				   {
					  $booking_data_array_cu[$sql_booking_row_cu[csf('po_break_down_id')]][$sql_booking_row_cu[csf('gmt_item')]][$sql_booking_row_cu[csf('pre_cost_fabric_cost_dtls_id')]][$sql_booking_row_cu[csf('emb_name')]][$sql_booking_row_cu[csf('emb_type')]][$sql_booking_row_cu[csf('gmts_color_id')]]['cu_woq']=$sql_booking_row_cu[csf('cu_woq')];
				   }
				  
				 $sql="select
					b.costing_per,c.id as wo_pre_cost_embe_cost_dtls,c.job_no,c.emb_name,c.emb_type,c.cons_dzn_gmts,c.rate,d.id as po_id,d.po_number,d.plan_cut,e.item_number_id,e.color_number_id,sum(e.plan_cut_qnty) as plan_cut_qnty,f.id as bookingdtlsid,f.wo_qnty as wo_qnty,f.rate as frate,f.amount as famount,f.description 
					from wo_po_details_master a, wo_pre_cost_mst b,wo_pre_cost_embe_cost_dtls c,wo_po_break_down d,wo_po_color_size_breakdown e,wo_booking_dtls f 
					where a.job_no=b.job_no and a.job_no=c.job_no and a.job_no=d.job_no_mst  and a.job_no=e.job_no_mst  and a.job_no=f.job_no  and c.id=f.pre_cost_fabric_cost_dtls_id  and d.id=e.po_break_down_id and d.id=f.po_break_down_id and f.gmt_item=e.item_number_id and  f.gmts_color_id=e.color_number_id and f.booking_no='$txt_booking_no' and a.is_deleted=0 and  a.status_active=1 and b.is_deleted=0 and b.status_active=1 and c.is_deleted=0 and c.status_active=1 and d.is_deleted=0 and d.status_active=1 and e.is_deleted=0 and e.status_active=1 and f.wo_qnty>0 
					group by f.id,f.wo_qnty,f.rate,f.amount,e.item_number_id,e.color_number_id,d.id,d.po_number,d.plan_cut,c.id,c.job_no,c.emb_name,c.emb_type,c.cons_dzn_gmts,c.rate,b.costing_per,f.description order by d.id";
					$order_id_array=array();
					$i=1;
					$k=1;
                    $nameArray=sql_select( $sql );
					foreach ($nameArray as $selectResult)
					{
						if ($i%2==0)  
							$bgcolor="#E9F3FF";
						else
							$bgcolor="#FFFFFF";
							
						if($selectResult[csf('costing_per')]==1)
						{
							$costing_per_qty=12;
						}
						else if($selectResult[csf('costing_per')]==2)
						{
							$costing_per_qty=1;
						}
						else if($selectResult[csf('costing_per')]==3)
						{
							$costing_per_qty=24;
						}
						else if($selectResult[csf('costing_per')]==4)
						{
							$costing_per_qty=36;
						}
						else if($selectResult[csf('costing_per')]==5)
						{
							$costing_per_qty=48;
						}
						
						$req_qnty=def_number_format(($selectResult[csf('cons_dzn_gmts')]*($selectResult[csf('plan_cut_qnty')]/$costing_per_qty)),5,"");
						$wo_qnty1=$selectResult[csf('wo_qnty')];
						//$wo_qnty=def_number_format($wo_qnty1,5,"");
						$cu_woq=$booking_data_array_cu[$selectResult[csf('po_id')]][$selectResult[csf('item_number_id')]][$selectResult[csf('wo_pre_cost_embe_cost_dtls')]][$selectResult[csf('emb_name')]][$selectResult[csf('emb_type')]][$selectResult[csf('color_number_id')]]['cu_woq'];
						$cu_woq=def_number_format($cu_woq-$wo_qnty1,5,"");
						$bal_woq=def_number_format($req_qnty-$cu_woq,5,"");
						$rate=$selectResult[csf('frate')];
						$description=$selectResult[csf('description')];
						$rate=def_number_format($rate,5,"");
						$pre_cost_rate=$selectResult[csf('rate')];
						$pre_cost_rate=def_number_format($pre_cost_rate,5,"");
						$amount=$selectResult[csf('famount')];
						$amount=def_number_format($amount,5,"");
						if (!in_array($selectResult[csf('po_id')],$order_id_array))
						{  
							if($k!=1)
							{
							
								?>
								<tr>
                                    <td colspan="10" align="right"><b>Sub Total</b></td> 
                                    <td   align="right"><b><?php echo number_format($wo_qnty,2);//]$emblishment_name_array[; ?></b> </td> 
                                    <td  align="right">&nbsp;</td>
                                    <td  align="right"><b><?php echo number_format($amount_tot,2);?> </b></td>
								</tr>
								<?php		
							
							}
							
							unset($wo_qnty);
							unset($amount_tot);
							$k++;
							$order_id_array[]=$selectResult[csf('po_id')];	
						}
						
						?>
						<tr <?php echo $bgcolor; ?>>
                            <td width="30"><?php echo $i; ?> </td> 
                            <td width="100"><p><?php echo $order_arr[$selectResult[csf('po_id')]];?></p> </td> 
                            <td  width="80" align="center"><p><?php echo $emblishment_name_array[$selectResult[csf('emb_name')]]; ?> </p></td> 
                            <td  width="80" align="center">
                            <p>
							<?php 
							 
							if($selectResult[csf('emb_name')]==1)
							{
								$emb_type=$emblishment_print_type[$selectResult[csf('emb_type')]];
							}
							if($selectResult[csf('emb_name')]==2)
							{
								$emb_type=$emblishment_embroy_type[$selectResult[csf('emb_type')]];
							}
							if($selectResult[csf('emb_name')]==3)
							{
								$emb_type=$emblishment_wash_type[$selectResult[csf('emb_type')]];
							}
							echo $emb_type;
							
							
							//echo $emblishment_print_type[$selectResult[csf('emb_type')]];
							?> 
                            </p>
                            </td>
                            <td  width="150"><div style="word-break:break-all; width:98px"><?php echo $description; ?> &nbsp; </div> </td>
                            <td width="80"><p><?php echo $color_id[$selectResult[csf('color_number_id')]]; ?> </p></td> 
                            <td width="80" align="right"><p><?php echo $selectResult[csf('plan_cut_qnty')]; ?> </p></td> 
                            <td width="100" align="right"><p><?php echo number_format($req_qnty,2); ?></p> </td>
                            <td  width="80" align="right"><p><?php echo number_format($cu_woq,2); ?> </p></td>
                            <td width="100" align="right"><p><?php  echo number_format($bal_woq,2)?></p> </td> 
                            <td width="80" align="right"><p><?php $w_qty=$selectResult[csf('wo_qnty')]; echo number_format($w_qty,2); ?></p></td>
                            <td width="70" align="right"><p> <?php echo number_format($rate,2); ?> </p></td> 
                            <td width="70" align="right"><p> <?php echo number_format($amount,2); ?> </p></td>
						</tr>
						
						<?php
						$i++;
						$wo_qnty+=$w_qty;
						$amount_tot+=$amount;
						
						$wo_qnty_r+=$w_qty;
						$amount_tot_t+=$amount;
						//end foreach	
					}
 ?> 
                      <tr>
                          <td  align="right" colspan="10"><b>Sub Total</b></td> 
                          <td  align="right"><b><?php echo number_format($wo_qnty,2);//]$emblishment_name_array[; ?> </b></td> 
                          <td  align="right">&nbsp;</td>
                          <td  align="right"><b><?php echo number_format($amount_tot,2);?></b> </td>
                      </tr>
                      <tr>
                          <td  align="right" colspan="10"><strong>Grand Total</strong></td> 
                          <td  align="right"><b><?php echo number_format($wo_qnty_r,2);//]$emblishment_name_array[; ?> </b></td> 
                          <td  align="right">&nbsp;</td>
                          <td  align="right"><b><?php echo number_format($amount_tot_t,2);?> </b></td>
                      </tr>
 </table>
    <br/>    
 <table  width="1100" class="rpt_table" border="1" cellpadding="0" cellspacing="0" align="center" rules="all">
                <thead>
                    <tr style="border:1px solid black;">
                        <th width="3%" style="border:1px solid black;">Sl</th><th width="97%" style="border:1px solid black;">Terms & Condition/Note</th>
                    </tr>
                </thead>
             <tbody>
            <?php
            $data_array=sql_select("select id, terms from  wo_booking_terms_condition where booking_no='$txt_booking_no'");
            if ( count($data_array)>0)
            {
                $i=0;
                foreach( $data_array as $row )
                {
                    $i++;
                    ?>
                <tr id="settr_1" align="" style="border:1px solid black;">
                    <td style="border:1px solid black;">
                    <?php echo $i;?>
                    </td>
                    <td style="border:1px solid black;">
                    <?php echo $row[csf('terms')]; ?>
                    </td>
                </tr>
                    <?php
                }
            }
            else
            {
                $i=0;
            $data_array=sql_select("select id, terms from  lib_terms_condition where is_default=1");// quotation_id='$data'
            foreach( $data_array as $row )
                {
                    $i++;
                    ?>
                <tr id="settr_1" align="" style="border:1px solid black;">
                    <td style="border:1px solid black;">
                    <?php echo $i;?>
                    </td>
                    <td style="border:1px solid black;">
                    <?php echo $row[csf('terms')]; ?>
                    </td>
                </tr>
                    <?php 
                }
            } 
            ?>
        </tbody>
  </table>
   <br>
		 <?php
            echo signature_table(57, $cbo_company_name, "1100px");
         ?>
 </div>

</div>	
	
<?php 
exit(); 
}
?>


<?php
if($action=="show_trim_booking_report1")
{
	//echo "fgfgfffgf";die;
 extract($_REQUEST);
//$data=explode('*',$data);
//$txt_booking_no=$data[0];
$txt_booking_no=str_replace("'","",$txt_booking_no);
$cbo_company_name=str_replace("'","",$cbo_company_name);
$cbo_supplier_name=str_replace("'","",$cbo_supplier_name);
//echo $cbo_supplier_name;die;
$txt_job_no=str_replace("'","",$txt_job_no);
$txt_booking_date=str_replace("'","",$txt_booking_date);
$txt_delivery_date=str_replace("'","",$txt_delivery_date);
$cbo_buyer_name=str_replace("'","",$cbo_buyer_name);
//$txt_booking_no=str_replace("'","",$txt_booking_no);
$cbo_currency=str_replace("'","",$cbo_currency);
$cbo_pay_mode=str_replace("'","",$cbo_pay_mode);
$cbo_source=str_replace("'","",$cbo_source);
$txt_exchange_rate=str_replace("'","",$txt_exchange_rate);

$company_library=return_library_array( "select id, company_name from lib_company", "id", "company_name"  );
$buyer_name= return_library_array("select id, short_name from  lib_buyer","id","short_name");
$supplier_name=return_library_array("select id,supplier_name from  lib_supplier", "id", "supplier_name");
$color_id=return_library_array("select id,color_name from lib_color", "id", "color_name");
$order_arr = return_library_array( "select id, po_number from wo_po_break_down",'id','po_number');
?>
<div style="width:1100px;" align="center">
 <table width="1100" cellspacing="0" align="center" border="0" >
        <tr>
            <td colspan="6" align="center" style="font-size:28px"><strong><?php echo $company_library[$cbo_company_name]; ?></strong></td>
        </tr>
        <tr class="form_caption">
            <td colspan="6" align="center">
				<?php 		
					
					$style_ref_no=sql_select("SELECT c.id, d.style_ref_no FROM wo_po_break_down c,wo_po_details_master d WHERE d.job_no=c.job_no_mst and d.job_no='$txt_job_no' and c.status_active=1 and c.is_deleted=0 and d.status_active=1 and d.is_deleted=0 group by c.id");
					$nameArray=sql_select( "select plot_no,level_no,road_no,block_no,country_id,province,city,zip_code,email,website from lib_company where id=$cbo_company_name"); 
					foreach ($nameArray as $result)
					{ 
					?>
						Plot No: <?php echo $result[csf('plot_no')]; ?> 
						Level No: <?php echo $result[csf('level_no')]?>
						Road No: <?php echo $result[csf('road_no')]; ?> 
						Block No: <?php echo $result[csf('block_no')];?> 
						City No: <?php echo $result[csf('city')];?> 
						Zip Code: <?php echo $result[csf('zip_code')]; ?> 
						Province No: <?php echo $result[csf('province')];?> 
						Country: <?php echo $country_arr[$result[csf('country_id')]]; ?><br> 
						Email Address: <?php echo $result[csf('email')];?> 
						Website No: <?php echo $result[csf('website')];
					}
					$nameArray_sup=sql_select( "select address_1 from lib_supplier where id=$cbo_supplier_name");
                ?> 
            </td>
        </tr>
        <tr>
            <td colspan="6" align="center" style="font-size:20px"><strong>Embellishment Work Order</strong></td>
        </tr>
         <tr>
            <td colspan="6" align="center" style="font-size:20">&nbsp;</td>
        </tr>
         <tr>
            <td style="font-size:20" width="130" >Supplier Name </td> <td style="font-size:20"  width="170">: &nbsp;<?php echo $supplier_name[$cbo_supplier_name]; ?></td>
            <td style="font-size:20">Work Order No</td> <td style="font-size:20">: &nbsp;<?php echo $txt_booking_no; //change_date_format($txt_booking_date); ?></td>
            <td style="font-size:20">Work Order Date  </td> <td style="font-size:20">: &nbsp;<?php echo change_date_format($txt_booking_date); ?></td>
        </tr>
         <tr>
            <td style="font-size:20">Supplier Address  </td> <td style="font-size:20">: &nbsp;<?php echo $nameArray_sup[0][csf('address_1')]; ?></td>
            <td style="font-size:20"> Job No. </td> <td style="font-size:20">: &nbsp;<?php echo $txt_job_no; ?></td>
            <td style="font-size:20">Delivery Date </td> <td style="font-size:20">: &nbsp;<?php echo change_date_format($txt_delivery_date); ?></td>
        </tr>
        <tr>
            <td style="font-size:20"> Buyer  </td> <td style="font-size:20">: &nbsp;<?php echo $buyer_name[$cbo_buyer_name]; ?></td>
            <td style="font-size:20">Currency </td> <td style="font-size:20">: &nbsp;<?php echo $currency[$cbo_currency]; ?></td>
            <td style="font-size:20">Exchange Rate  </td> <td style="font-size:20">: &nbsp;<?php echo $txt_exchange_rate; ?></td>
        </tr>
        <tr>
            <td style="font-size:20">Style Ref.  </td> <td style="font-size:20">: &nbsp;<?php echo $style_ref_no[0][csf('style_ref_no')]; ?></td>
            <td style="font-size:20"> Pay mode </td> <td style="font-size:20">: &nbsp;<?php echo $pay_mode[$cbo_pay_mode]; ?></td>
            <td style="font-size:20">Source </td>  <td style="font-size:20">: &nbsp;<?php echo $source[$cbo_source]; ?></td> 
        </tr>
          <tr>
          <td style="font-size:20">Article No</td> 
          <td style="font-size:20">: &nbsp;
		  <?php 
		  
		  $nameArray_article_number=sql_select( "select article_number from  wo_po_color_size_breakdown where job_no_mst='$txt_job_no' and article_number!=0 and is_deleted=0 and status_active=1");
		  echo $nameArray_article_number[0][csf('article_number')]; 
		  ?>
          </td>
          
          <td style="font-size:20">Image:</td> 
            <td colspan="3"> 
			<?php
			$data_array=sql_select("select image_location  from common_photo_library  where master_tble_id='$txt_job_no' and form_name='knit_order_entry' and is_deleted=0 and file_type=1");
			foreach($data_array as $row)
			{?>
				<img src='../../<?php echo $row[csf('image_location')]; ?>' height='65' width='90' align="middle" />
			<?php 
			}?>
           </td>
        </tr>
        
 </table><br/>
 <div style="width:100%;">
  <?php
 $costing_per=return_field_value("costing_per", "wo_pre_cost_mst", "job_no='$txt_job_no'");
	if($costing_per==1)
	{
		$costing_per_dzn="1 Dzn";
	}
	else if($costing_per==2)
	{
		$costing_per_dzn="1 Pcs";
	}
	else if($costing_per==3)
	{
		$costing_per_dzn="2 Dzn";
	}
	else if($costing_per==4)
	{
		$costing_per_dzn="3 Dzn";
	}
	else if($costing_per==5)
	{
		$costing_per_dzn="4 Dzn";
	}
 ?>
 <table width="1100" cellspacing="0" align="center" border="1" rules="all" class="rpt_table" >
      <thead bgcolor="#dddddd" align="center">
         <th width="30">SL</th>
         <th width="100">Order No.</th>
         <th width="80">Emb. Name</th>
         <th width="80">Emb. type</th>
         <th width="150">Description</th>
         <th width="80">Color</th>
         <th width="80">Order Qty (PCS)</th>
        
         <th width="80">WOQ/DZN</th>
         <th width="70">Rate/DZN</th>
         <th width="70">Amount/DZN</th>
      </thead>
      <?php
	   			  
				  
				 $sql="select
					b.costing_per,c.id as wo_pre_cost_embe_cost_dtls,c.job_no,c.emb_name,c.emb_type,c.cons_dzn_gmts,c.rate,d.id as po_id,d.po_number,d.plan_cut,e.item_number_id,e.color_number_id,sum(e.order_quantity) as order_quantity,f.id as bookingdtlsid,f.wo_qnty as wo_qnty,f.rate as frate,f.amount as famount,f.description 
					from wo_po_details_master a, wo_pre_cost_mst b,wo_pre_cost_embe_cost_dtls c,wo_po_break_down d,wo_po_color_size_breakdown e,wo_booking_dtls f 
					where a.job_no=b.job_no and a.job_no=c.job_no and a.job_no=d.job_no_mst  and a.job_no=e.job_no_mst  and a.job_no=f.job_no  and c.id=f.pre_cost_fabric_cost_dtls_id  and d.id=e.po_break_down_id and d.id=f.po_break_down_id and f.gmt_item=e.item_number_id and  f.gmts_color_id=e.color_number_id and f.booking_no='$txt_booking_no' and a.is_deleted=0 and  a.status_active=1 and b.is_deleted=0 and b.status_active=1 and c.is_deleted=0 and c.status_active=1 and d.is_deleted=0 and d.status_active=1 and e.is_deleted=0 and e.status_active=1  and f.wo_qnty>0 
					group by f.id,f.wo_qnty,f.rate,f.amount,e.item_number_id,e.color_number_id,d.id,d.po_number,d.plan_cut,c.id,c.job_no,c.emb_name,c.emb_type,c.cons_dzn_gmts,c.rate,b.costing_per,f.description order by d.id";
					$order_id_array=array();
					$i=1;
					$k=1;
                    $nameArray=sql_select( $sql );
					foreach ($nameArray as $selectResult)
					{
						if ($i%2==0)  
							$bgcolor="#E9F3FF";
						else
							$bgcolor="#FFFFFF";
							
						/*if($selectResult[csf('costing_per')]==1)
						{
							$costing_per_qty=12;
						}
						else if($selectResult[csf('costing_per')]==2)
						{
							$costing_per_qty=1;
						}
						else if($selectResult[csf('costing_per')]==3)
						{
							$costing_per_qty=24;
						}
						else if($selectResult[csf('costing_per')]==4)
						{
							$costing_per_qty=36;
						}
						else if($selectResult[csf('costing_per')]==5)
						{
							$costing_per_qty=48;
						}*/
						
						
						$rate=$selectResult[csf('frate')];
						$description=$selectResult[csf('description')];
						$rate=def_number_format($rate,5,"");
						$pre_cost_rate=$selectResult[csf('rate')];
						$pre_cost_rate=def_number_format($pre_cost_rate,5,"");
						$amount=$selectResult[csf('famount')];
						$amount=def_number_format($amount,5,"");
						if (!in_array($selectResult[csf('po_id')],$order_id_array))
						{  
							if($k!=1)
							{
							
								?>
								<tr>
                                    <td colspan="7" align="right"><b>Sub Total</b></td> 
                                    <td   align="right"><b><?php echo number_format($wo_qnty,2);//]$emblishment_name_array[; ?></b> </td> 
                                    <td  align="right">&nbsp;</td>
                                    <td  align="right"><b><?php echo number_format($amount_tot,2);?> </b></td>
								</tr>
								<?php		
							
							}
							
							unset($wo_qnty);
							unset($amount_tot);
							$k++;
							$order_id_array[]=$selectResult[csf('po_id')];	
						}
						
						?>
						<tr <?php echo $bgcolor; ?>>
                            <td width="30"><?php echo $i; ?> </td> 
                            <td width="100"><p><?php echo $order_arr[$selectResult[csf('po_id')]];?></p> </td> 
                            <td  width="80" align="center"><p><?php echo $emblishment_name_array[$selectResult[csf('emb_name')]]; ?> </p></td> 
                            <td  width="80" align="center">
                            <p>
							<?php 
							 
							if($selectResult[csf('emb_name')]==1)
							{
								$emb_type=$emblishment_print_type[$selectResult[csf('emb_type')]];
							}
							if($selectResult[csf('emb_name')]==2)
							{
								$emb_type=$emblishment_embroy_type[$selectResult[csf('emb_type')]];
							}
							if($selectResult[csf('emb_name')]==3)
							{
								$emb_type=$emblishment_wash_type[$selectResult[csf('emb_type')]];
							}
							echo $emb_type;
							
							
							//echo $emblishment_print_type[$selectResult[csf('emb_type')]];
							?> 
                            </p>
                            </td>
                            <td  width="150"><div style="word-break:break-all; width:98px"><?php echo $description; ?> &nbsp; </div> </td>
                            <td width="80"><p><?php echo $color_id[$selectResult[csf('color_number_id')]]; ?> </p></td> 
                            <td width="80" align="right"><p><?php echo $selectResult[csf('order_quantity')]; ?> </p></td> 
                            
                            <td width="80" align="right"><p><?php $w_qty=$selectResult[csf('wo_qnty')]; echo number_format($w_qty,2); ?></p></td>
                            <td width="70" align="right"><p> <?php echo number_format($rate,2); ?> </p></td> 
                            <td width="70" align="right"><p> <?php echo number_format($amount,2); ?> </p></td>
						</tr>
						
						<?php
						$i++;
						$wo_qnty+=$w_qty;
						$amount_tot+=$amount;
						
						$wo_qnty_r+=$w_qty;
						$amount_tot_t+=$amount;
						//end foreach	
					}
 ?> 
                      <tr>
                          <td  align="right" colspan="7"><b>Sub Total</b></td> 
                          <td  align="right"><b><?php echo number_format($wo_qnty,2);//]$emblishment_name_array[; ?> </b></td> 
                          <td  align="right">&nbsp;</td>
                          <td  align="right"><b><?php echo number_format($amount_tot,2);?></b> </td>
                      </tr>
                      <tr>
                          <td  align="right" colspan="7"><strong>Grand Total</strong></td> 
                          <td  align="right"><b><?php echo number_format($wo_qnty_r,2);//]$emblishment_name_array[; ?> </b></td> 
                          <td  align="right">&nbsp;</td>
                          <td  align="right"><b><?php echo number_format($amount_tot_t,2);?> </b></td>
                      </tr>
 </table>
    <br/>    
 <table  width="1100" class="rpt_table" border="1" cellpadding="0" cellspacing="0" align="center" rules="all">
                <thead>
                    <tr style="border:1px solid black;">
                        <th width="3%" style="border:1px solid black;">Sl</th><th width="97%" style="border:1px solid black;">Terms & Condition/Note</th>
                    </tr>
                </thead>
             <tbody>
            <?php
            $data_array=sql_select("select id, terms from  wo_booking_terms_condition where booking_no='$txt_booking_no'");
            if ( count($data_array)>0)
            {
                $i=0;
                foreach( $data_array as $row )
                {
                    $i++;
                    ?>
                <tr id="settr_1" align="" style="border:1px solid black;">
                    <td style="border:1px solid black;">
                    <?php echo $i;?>
                    </td>
                    <td style="border:1px solid black;">
                    <?php echo $row[csf('terms')]; ?>
                    </td>
                </tr>
                    <?php
                }
            }
            else
            {
                $i=0;
            $data_array=sql_select("select id, terms from  lib_terms_condition where is_default=1");// quotation_id='$data'
            foreach( $data_array as $row )
                {
                    $i++;
                    ?>
                <tr id="settr_1" align="" style="border:1px solid black;">
                    <td style="border:1px solid black;">
                    <?php echo $i;?>
                    </td>
                    <td style="border:1px solid black;">
                    <?php echo $row[csf('terms')]; ?>
                    </td>
                </tr>
                    <?php 
                }
            } 
            ?>
        </tbody>
  </table>
   <br>
		 <?php
            echo signature_table(57, $cbo_company_name, "1100px");
         ?>
 </div>

</div>	
	 
<?php }
exit();
?>