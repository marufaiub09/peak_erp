<?php 
header('Content-type:text/html; charset=utf-8');
session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../../../includes/common.php');

$user_name=$_SESSION['logic_erp']['user_id'];
$data=$_REQUEST['data'];
$action=$_REQUEST['action'];
$color_library=return_library_array( "select id,color_name from lib_color", "id", "color_name"  );
$buyer_arr = return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
$company_library=return_library_array( "select id,company_name from lib_company", "id", "company_name"  );

$ltb_btb=array(1=>'BTB',2=>'LTB');

//--------------------------------------------------------------------------------------------------------------------


if($action=="check_color_id")
{	echo load_html_head_contents("PO Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
		?>
<script type="text/javascript">
  function js_set_value(id)
	  { //alert(id);
	document.getElementById('selected_id').value=id;
	parent.emailwindow.hide();
	  }

</script>
<input type="hidden" id="selected_id" name="selected_id" /> 
<?php
	$sql="select id, color_name from lib_color where is_deleted=0 and status_active=1 order by id";
	$arr=array(1=>$color_library);
	echo  create_list_view("list_view", "ID,Color Name", "50,200","300","300",0, $sql, "js_set_value", "id,color_name", "", 1, "0,0", $arr , "id,color_name", "",'setFilterGrid("list_view",-1);','0') ;

exit();	
}
if($action=="batchnumbershow")
{
	echo load_html_head_contents("PO Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
?>
<script type="text/javascript">
  function js_set_value(id)
	  { 
		document.getElementById('selected_id').value=id;
		parent.emailwindow.hide();
	  }

</script>
<input type="hidden" id="selected_id" name="selected_id" /> 
<?php if($db_type==0) $field_grpby=" GROUP BY batch_no"; 
else if($db_type==2) $field_grpby="GROUP BY batch_no,id,batch_no,batch_for,booking_no,color_id,batch_weight";
$batch_type = str_replace("'","",$batch_type);
if ($batch_type==0 || $batch_type==1)
{
$sql="select id,batch_no,batch_for,booking_no,color_id,batch_weight from pro_batch_create_mst where company_id=$company_name and entry_form in(0) and is_deleted = 0 $field_grpby ";	
}
if ($batch_type==0 || $batch_type==2)
{
	
$sql="select id,batch_no,batch_for,booking_no,color_id,batch_weight from pro_batch_create_mst where company_id=$company_name and entry_form in(36) and is_deleted = 0 $field_grpby ";	
}
$arr=array(1=>$color_library);
	echo  create_list_view("list_view", "Batch no,Color,Booking no, Batch for,Batch weight ", "100,100,100,100,170","620","350",0, $sql, "js_set_value", "id,batch_no", "", 1, "0,color_id,0,0,0", $arr , "batch_no,color_id,booking_no,batch_for,batch_weight", "employee_info_controller",'setFilterGrid("list_view",-1);','0') ;
	exit();
}//batchnumbershow;
if($action=="load_drop_down_buyer")
{ 
	echo load_html_head_contents("PO Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
	$data=explode('_',$data);
	$company=$data[0];
	$report_type=$data[1];
	//echo $report_type;
	if($report_type==1)
	{
    echo create_drop_down( "cbo_buyer_name", 120, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$company' $buyer_cond and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (1,3,21,90))  order by buyer_name","id,buyer_name", 1, "-- Select Buyer --", 0, "" );
	}
	else if($report_type==2)
	{
		 echo create_drop_down( "cbo_buyer_name", 120, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$company' $buyer_cond and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (2,3))  order by buyer_name","id,buyer_name", 1, "-- Select Buyer --", 0, "" );
	}
	else if($report_type==0)
	{
		echo create_drop_down( "cbo_buyer_name", 120, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$company' $buyer_cond and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (1,2,3,21,90))  order by buyer_name","id,buyer_name", 1, "-- Select Buyer --", 0, "" );
	}
	else
	{
	 echo create_drop_down( "cbo_buyer_name", 120, $blank_array,"", 1, "-- Select Party --", $selected, "",0,"","","","");	
	}
	exit();
}//cbo_buyer_name_td
if($action=="jobnumbershow")
{
	echo load_html_head_contents("PO Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
?>
<script type="text/javascript">
  function js_set_value(id)
	  {
		document.getElementById('selected_id').value=id;
		parent.emailwindow.hide();
	  }
</script>
<input type="hidden" id="selected_id" name="selected_id" /> 
<?php
$year_job = str_replace("'","",$year);
$batch_type = str_replace("'","",$batch_type);
if($db_type==0)
{
	$year_field_by="and YEAR(a.insert_date)"; 
	$year_field="SUBSTRING_INDEX(a.insert_date, '-', 1) as year"; 
	$field_grpby="GROUP BY a.job_no order by b.id desc"; 
}
else if($db_type==2)
{
$year_field_by=" and to_char(a.insert_date,'YYYY')";
$year_field="to_char(a.insert_date,'YYYY') as year";	
$field_grpby=" GROUP BY a.job_no,a.id,a.buyer_name,a.style_ref_no,a.gmts_item_id,b.po_number,a.job_no_prefix_num,a.insert_date,b.id order by b.id,a.job_no_prefix_num  desc ";
}

if(trim($year)!=0) $year_cond=" $year_field_by=$year_job"; else $year_cond="";
//echo $year_job;
//$cbo_buyer_name=($cbo_buyer_name==0)?"%%" : "%$cbo_buyer_name%";
if(trim($cbo_buyer_name)==0) $buyer_name_cond=""; else $buyer_name_cond=" and a.buyer_name=$cbo_buyer_name";
if(trim($cbo_buyer_name)==0) $sub_buyer_name_cond=""; else $sub_buyer_name_cond=" and a.party_id=$cbo_buyer_name";
if ($batch_type==0 || $batch_type==1)
{
$sql="select a.id,a.buyer_name,a.style_ref_no,a.gmts_item_id,b.po_number,a.job_no_prefix_num as job_prefix,$year_field from wo_po_details_master a,wo_po_break_down b where b.job_no_mst=a.job_no and a.company_name=$company_id  $buyer_name_cond $year_cond and a.is_deleted = 0 $field_grpby";
}
else
{
$sql="select a.id,a.party_id as buyer_name,c.item_id as gmts_item_id,b.cust_style_ref as style_ref_no,b.order_no as po_number,a.job_no_prefix_num as job_prefix,$year_field from  subcon_ord_mst a, subcon_ord_dtls b, subcon_ord_breakdown c where b.job_no_mst=a.subcon_job and a.id=c.mst_id and b.id=c.order_id and a.company_id=$company_id  $sub_buyer_name_cond $year_cond and a.is_deleted = 0 group by a.id,a.party_id,b.cust_style_ref,b.order_no ,a.job_no_prefix_num,a.insert_date,c.item_id";
	
}
//echo $sql;
$buyer=return_library_array( "select id,buyer_name from lib_buyer", "id", "buyer_name"  );
?>
<table width="580" border="1" rules="all" class="rpt_table">
	<thead>
         <tr><th colspan="7"><?php if($batch_type==0 || $batch_type==1)
		 { echo "Self Batch Order";} else if($batch_type==2) { echo "SubCon Batch Order";}?>  </th></tr>

        <tr>
            <th width="35">SL</th>
            <th width="100">Po Number</th>
            <th width="100">Job no</th>
            <th width="50">Year</th>
            <th width="80">Buyer</th>
            <th width="100">Style</th>
            <th>Item Name</th>
        </tr>
   </thead>
</table>
<div style="max-height:390px; overflow-y:scroll; width:600px;">
<table id="table_body2" width="580" border="1" rules="all" class="rpt_table">
 <?php $rows=sql_select($sql);
	 $i=1;
 foreach($rows as $data)
 {  if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
  ?>
	<tr bgcolor="<?php echo $bgcolor; ?>" onClick="js_set_value('<?php echo $data[csf('job_prefix')]; ?>')" style="cursor:pointer;">
		<td width="35"><?php echo $i; ?></td>
		<td width="100"><p><?php echo $data[csf('po_number')]; ?></p></td>
		<td width="100"><p><?php echo $data[csf('job_prefix')]; ?></p></td>
        <td width="50"><p><?php echo $data[csf('year')]; ?></p></td>
		<td width="80"><p><?php echo $buyer[$data[csf('buyer_name')]]; ?></p></td>
		<td width="100"><p><?php echo $data[csf('style_ref_no')]; ?></p></td>
		<td><p><?php 
		$itemid=explode(",",$data[csf('gmts_item_id')]);
		foreach($itemid as $index=>$id){
			echo ($itemid[$index]==end($itemid))? $garments_item[$id] : $garments_item[$id].', ';
		}
		?></p></td>
	</tr>
    <?php $i++; } ?>
</table>
</div>
<script> setFilterGrid("table_body2",-1); </script>
<?php
	exit();
}//JobNumberShow
if($action=="order_number_popup")
{
	echo load_html_head_contents("PO Info", "../../../", 1, 1,'','','');
	extract($_REQUEST);
?>
<script type="text/javascript">
  function js_set_value(id)
	  { 
		document.getElementById('selected_id').value=id;
		parent.emailwindow.hide();
	  }
</script>
<input type="hidden" id="selected_id" name="selected_id" /> 
<?php
$buyer = str_replace("'","",$buyer_name);
$year = str_replace("'","",$year);
$year_job = str_replace("'","",$year);
if($db_type==0)
{
	$year_field_by=" and YEAR(b.insert_date)"; 
	$year_field="SUBSTRING_INDEX(b.insert_date, '-', 1) as year "; 
	
}
else if($db_type==2)
{
$year_field_by=" and to_char(b.insert_date,'YYYY')";
$year_field="to_char(b.insert_date,'YYYY') as year";	
}
if ($company_name==0) $company=""; else $company=" and b.company_name=$company_name";
if(trim($year)!=0) $year_cond=" $year_field_by=$year"; else $year_cond="";
//echo $buyer;die;
if ($buyer==0) $buyername=""; else $buyername=" and b.buyer_name=$buyer";//$cbo_buyer_name=($cbo_buyer_name==0)?"%%" : "%$cbo_buyer_name%";
if(trim($buyer)==0) $sub_buyer_name_cond=""; else $sub_buyer_name_cond=" and a.party_id=$buyer";
if ($batch_type==0 || $batch_type==1)
{
$sql = "select distinct a.id,b.job_no,a.po_number,b.company_name,b.buyer_name,b.job_no_prefix_num as job_prefix,$year_field from wo_po_break_down a, wo_po_details_master b where a.job_no_mst=b.job_no $company $buyername $year_cond order by a.id asc"; 
}
else
{
$sql="select distinct a.id,b.job_no_mst as job_no ,a.party_id as buyer_name,a.company_id as company_name ,b.order_no as po_number,a.job_no_prefix_num as job_prefix,$year_field from  subcon_ord_mst a , subcon_ord_dtls b, subcon_ord_breakdown c where b.job_no_mst=a.subcon_job and a.id=c.mst_id and b.id=c.order_id and a.company_id=$company_name  $sub_buyer_name_cond $year_cond and a.is_deleted =0 group by a.id,a.party_id,b.job_no_mst,b.order_no ,a.job_no_prefix_num,a.company_id,b.insert_date";
}
$buyer=return_library_array( "select id,buyer_name from lib_buyer", "id", "buyer_name"  );
?>
<table width="490" border="1" rules="all" class="rpt_table">
	<thead>
     <tr><th colspan="5"><?php if($batch_type==0 || $batch_type==1) echo "Self Batch Order"; else echo "SubCon Batch Order";?>  </th></tr>
        <tr>
        <th width="30">SL</th>
        <th width="80">Order Number</th>
        <th width="50">Job no</th>
        <th width="80">Buyer</th>
        <th width="40">Year</th>
        </tr>
   </thead>
</table>
<div style="max-height:380px; overflow:auto;">
<table id="table_body2" width="490" border="1" rules="all" class="rpt_table">
 <?php $rows=sql_select($sql);
	 $i=1;
 foreach($rows as $data)
 {
	  if($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
  ?>
	<tr bgcolor="<?php echo $bgcolor;?>" onClick="js_set_value('<?php echo $data[csf('po_number')]; ?>')" style="cursor:pointer;">
		<td width="30"><?php echo $i; ?></td>
		<td width="80"><p><?php echo $data[csf('po_number')]; ?></p></td>
		<td width="50"><p><?php echo $data[csf('job_prefix')]; ?></p></td>
		<td width="80"><p><?php echo $buyer[$data[csf('buyer_name')]]; ?></p></td>
		<td width="40" align="center"><?php echo $data[csf('year')]; ?></p></td>
	</tr>
    <?php $i++; } ?>
</table>
</div>
<script> setFilterGrid("table_body2",-1); </script>
<?php
	exit();
}
if($action=="dyeing_production_report")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	if($db_type==0) $year_field_by="and YEAR(a.insert_date)"; 
	else if($db_type==2) $year_field_by=" and to_char(a.insert_date,'YYYY')";
	if($db_type==0) $field_concat="concat(machine_no,'-',brand) as machine_name"; 
	else if($db_type==2) $field_concat="machine_no || '-' || brand as machine_name";
// machine_no || '-' || brand as machine_name
	$company = str_replace("'","",$cbo_company_name);
	$buyer = str_replace("'","",$cbo_buyer_name);
	$job_number = str_replace("'","",$job_number);
	$job_number_id = str_replace("'","",$job_number_show);
	$batch_no = str_replace("'","",$batch_number_show);
	$color = str_replace("'","",$txt_color);
	//echo $batch_no;die;
	$batch_number_hidden = str_replace("'","",$batch_number);
	$ext_num = str_replace("'","",$txt_ext_no);
	$hidden_ext = str_replace("'","",$hidden_ext_no);
	$txt_order = str_replace("'","",$order_no);
	$hidden_order = str_replace("'","",$hidden_order_no);
	$cbo_type = str_replace("'","",$cbo_type);
	$cbo_result_name = str_replace("'","",$cbo_result_name);
	$year = str_replace("'","",$cbo_year);
	//echo $cbo_type;die;
	$txt_date_from = str_replace("'","",$txt_date_from);
	$txt_date_to = str_replace("'","",$txt_date_to);
	//$jobdata=($job_number_id )? " and d.job_no_prefix_num='".$job_number_id ."'" : '';
	//if ($job_number==0) $sub_job_cond=""; else $sub_job_cond="  and d.job_no_prefix_num='".$job_number."' ";
	if ($buyer==0) $sub_buyer_cond=""; else $sub_buyer_cond="  and d.party_id='".$buyer."' ";
	if ($cbo_result_name==0) $result_name_cond=""; else $result_name_cond="  and f.result='".$cbo_result_name."' ";
	$buyerdata=($buyer)?' and d.buyer_name='.$buyer : '';
	$batch_num=($batch_no)?" and a.batch_no='".$batch_no."'" : '';
	//echo $cbo_batch_type;
	if(trim($ext_no)!="") $ext_no_search="%".trim($ext_no)."%"; else $ext_no_search="%%";
	if(str_replace("'",'',$cbo_batch_type)==0 || str_replace("'",'',$cbo_batch_type)==1)
	{
	if ($txt_order!='') $order_no="  and c.po_number='$txt_order'"; else $order_no="";
		$jobdata=($job_number_id )? " and d.job_no_prefix_num='".$job_number_id ."'" : '';
	}
	if(str_replace("'",'',$cbo_batch_type)==0 || str_replace("'",'',$cbo_batch_type)==2)
	{
		if ($txt_order!='') $suborder_no="and c.order_no='$txt_order'"; else $suborder_no="";
		if ($job_number_id!='') $sub_job_cond="  and d.job_no_prefix_num='".$job_number_id."' "; else $sub_job_cond="";
	}
	if ($color=="") $color_name=""; else $color_name="  and g.color_name='$color'";
	//echo $order_no;die;
	if ($ext_num=="") $ext_no=""; else $ext_no="  and a.extention_no=$ext_num ";
	if(trim($year)!=0) $year_cond=" $year_field_by=$year"; else $year_cond="";
	
	if($txt_date_from && $txt_date_to)
	{
		if($db_type==0)
		{
			$date_from=change_date_format($txt_date_from,'yyyy-mm-dd');
			$date_to=change_date_format($txt_date_to,'yyyy-mm-dd');
			$dates_com=" and  f.process_end_date BETWEEN '$date_from' AND '$date_to' ";
		}
		elseif($db_type==2)
		{
			$date_from=change_date_format($txt_date_from,'','',1);
			$date_to=change_date_format($txt_date_to,'','',1);
			$dates_com="and  f.process_end_date BETWEEN '$date_from' AND '$date_to'";
		}
	}
	$yarn_lot_arr=array();
	if($db_type==0)
	{
		$yarn_lot_data=sql_select("select b.po_breakdown_id, a.prod_id, group_concat(distinct(a.yarn_lot)) as yarn_lot from pro_grey_prod_entry_dtls a, order_wise_pro_details b where a.id=b.dtls_id and b.entry_form in(2,22) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.yarn_lot!=''  group by a.prod_id, b.po_breakdown_id");
	}
	else if($db_type==2)
	{
		$yarn_lot_data=sql_select("select b.po_breakdown_id, a.prod_id, LISTAGG(CAST(a.yarn_lot AS VARCHAR2(4000)),',') WITHIN GROUP ( ORDER BY a.yarn_lot)  as yarn_lot from pro_grey_prod_entry_dtls a, order_wise_pro_details b where a.id=b.dtls_id and b.entry_form in(2,22) and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 and a.yarn_lot!='0' group by a.prod_id, b.po_breakdown_id");
	}
	foreach($yarn_lot_data as $rows)
	{
		$yarn_lot=explode(",",$rows[csf('yarn_lot')]);
		$yarn_lot_arr[$rows['prod_id']][$rows['po_breakdown_id']]=implode(",",array_unique($yarn_lot));
	}
		//print_r($yarn_lot_arr);
	$load_hr=array();
	$load_min=array();
	$load_date=array();
	$load_time_data=sql_select("select batch_id,batch_no,load_unload_id,process_end_date,end_hours,end_minutes from pro_fab_subprocess where load_unload_id=1 and entry_form=35 and company_id=$company and status_active=1  and is_deleted=0 ");
	foreach($load_time_data as $row_time)// for Loading time
	{
		$load_hr[$row_time[csf('batch_id')]]=$row_time[csf('end_hours')];
		$load_min[$row_time[csf('batch_id')]]=$row_time[csf('end_minutes')];
		$load_date[$row_time[csf('batch_id')]]=$row_time[csf('process_end_date')];
	}
	$subcon_load_hr=array();
	$subcon_load_min=array();
	$subcon_load_date=array();
	$subcon_load_time_data=sql_select("select batch_id,batch_no,load_unload_id,process_end_date,end_hours,end_minutes from pro_fab_subprocess where load_unload_id=1 and entry_form=38 and company_id=$company and status_active=1  and is_deleted=0 ");
	foreach($subcon_load_time_data as $row_time)// for Loading time
	{
		$subcon_load_hr[$row_time[csf('batch_id')]]=$row_time[csf('end_hours')];
		$subcon_load_min[$row_time[csf('batch_id')]]=$row_time[csf('end_minutes')];
		$subcon_load_date[$row_time[csf('batch_id')]]=$row_time[csf('process_end_date')];
	}
	$subcon_unload_hr=array();
	$subcon_unload_min=array();
	$subcon_unload_date=array();
	$subcon_unload_time_data=sql_select("select batch_id,batch_no,load_unload_id,process_end_date,end_hours,end_minutes from pro_fab_subprocess where load_unload_id=2 and entry_form=38 and company_id=$company and status_active=1  and is_deleted=0 ");
	foreach($subcon_load_time_data as $row_time)// for Loading time
	{
	$subcon_unload_hr[$row_time[csf('batch_id')]]=$row_time[csf('end_hours')];
	$subcon_unload_min[$row_time[csf('batch_id')]]=$row_time[csf('end_minutes')];
	$subcon_unload_time_data[$row_time[csf('batch_id')]]=$row_time[csf('process_end_date')];
	}
	//var_dump($load_hr);
	
	$m_capacity=array();
	$unload_min=array();
	$machine_arr=return_library_array( "select id,$field_concat from  lib_machine_name",'id','machine_name');
	$machine_capacity_data=sql_select("select id,prod_capacity as m_capacity  from lib_machine_name where status_active=1  and is_deleted=0 ");
	foreach($machine_capacity_data as $capacity)// for Un-Loading time
	{
		$m_capacity[$capacity[csf('id')]]=$capacity[csf('m_capacity')];
	}
	$sql_batch_id=sql_select("select batch_id from  pro_fab_subprocess where entry_form=35 and load_unload_id=2 and status_active=1 and is_deleted=0");
	$i=1;
	foreach($sql_batch_id as $row_batch)
	{
		if($i!=1) $batch_id.=",";	
		$batch_id.=$row_batch[csf('batch_id')];	
		
		$i++;
	}//echo $batch_id;die;
	$sub_sql_batch_id=sql_select("select batch_id from  pro_fab_subprocess where entry_form=38 and load_unload_id=2 and status_active=1 and is_deleted=0");
	$k=1;
	foreach($sub_sql_batch_id as $row_batch)
	{
		if($k!=1) $sub_batch_id.=",";	
		$sub_batch_id.=$row_batch[csf('batch_id')];	
		
		$k++;
	}
	if($batch_id=="") $batch_id=0;
	if($sub_batch_id=="") $sub_batch_id=0;
	if($db_type==0)
	{
		if($cbo_type==1)//  For Dyeing WIP
		{
			if(str_replace("'",'',$cbo_batch_type)==0 || str_replace("'",'',$cbo_batch_type)==1)
			{
			$sql="select a.company_id,a.batch_no,a.batch_date,a.batch_weight,a.color_id,a.booking_no_id,a.extention_no,a.batch_against,SUM(b.batch_qnty) AS batch_qnty,b.item_description,b.po_id,b.prod_id,b.width_dia_type,group_concat( distinct c.po_number ) AS po_number,c.job_no_mst,d.job_no_prefix_num,d.buyer_name,f.process_end_date,f.end_hours,f.end_minutes,f.machine_id,f.remarks from pro_batch_create_dtls b,wo_po_break_down c,wo_po_details_master d,pro_batch_create_mst a, pro_fab_subprocess f,lib_color g	where a.company_id=$company $dates_com $jobdata $batch_num $buyerdata $order_no $year_cond $color_name and g.id=a.color_id and a.entry_form=0 and a.id=b.mst_id and f.batch_id=a.id and f.entry_form=35 and f.load_unload_id=1 and a.batch_against in(1,2) and a.id not in ($batch_id) and b.po_id=c.id and d.job_no=c.job_no_mst and b.status_active=1 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0 GROUP BY b.id order by a.batch_no ";
			}
			if(str_replace("'",'',$cbo_batch_type)==0 || str_replace("'",'',$cbo_batch_type)==2)
			{
		$sql_subcon="select a.company_id,a.id,a.batch_no,a.batch_date,a.batch_weight,a.color_id,a.booking_no_id,a.extention_no,a.batch_against,d.party_id as buyer_name, SUM(b.batch_qnty) AS sub_batch_qnty,b.item_description,b.po_id,b.prod_id,b.width_dia_type,group_concat(distinct c.order_no) AS po_number,d.subcon_job,d.job_no_prefix_num,d.party_id,f.process_end_date,f.end_hours,f.end_minutes,f.machine_id,f.load_unload_id,f.result,f.remarks from pro_batch_create_dtls b,  subcon_ord_dtls c, subcon_ord_mst d, pro_batch_create_mst a, pro_fab_subprocess f, lib_color g where a.company_id=$company and f.batch_id=a.id and a.entry_form=36 and g.id=a.color_id and a.id=b.mst_id and f.entry_form=38 and f.load_unload_id=1 and a.batch_against in(0) and a.id not in ($sub_batch_id) and b.po_id=c.id and d.subcon_job=c.job_no_mst and b.status_active=1 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0  $dates_com $sub_job_cond $batch_num $sub_buyer_cond $suborder_no $year_cond $color_name $result_name_cond GROUP BY a.id,a.batch_no,a.company_id, d.party_id,a.batch_date,a.batch_weight,a.color_id,a.booking_no_id,a.extention_no,a.batch_against,b.item_description,b.po_id,b.prod_id,b.width_dia_type,d.subcon_job,d.job_no_prefix_num,d.party_id,f.process_end_date,f.end_hours,f.end_minutes,f.machine_id,f.load_unload_id,f.result,f.remarks ";
			}
			
		}
		else if($cbo_type==2)//  For Order Wise Dyeing 
		{
			if(str_replace("'",'',$cbo_batch_type)==0 || str_replace("'",'',$cbo_batch_type)==1)
			{
			$sql="select a.company_id,a.id,a.batch_no,a.batch_date,a.batch_weight,a.color_id,a.booking_no_id,a.extention_no,a.batch_against,SUM(b.batch_qnty) AS batch_qnty,b.item_description,b.po_id,b.prod_id,b.width_dia_type,group_concat( distinct c.po_number ) AS po_number,c.job_no_mst,d.job_no_prefix_num,d.buyer_name,f.process_end_date,f.end_hours,f.end_minutes,f.machine_id,f.load_unload_id,f.result,f.remarks from pro_batch_create_dtls b,wo_po_break_down c,wo_po_details_master d,pro_batch_create_mst a,pro_fab_subprocess f,lib_color g where a.company_id=$company and  f.batch_id=a.id $dates_com $jobdata $batch_num $buyerdata $order_no $year_cond $color_name and a.entry_form=0 and  g.id=a.color_id and a.id=b.mst_id and f.entry_form=35 and f.load_unload_id=2 and a.batch_against in(1,2) and b.po_id=c.id and d.job_no=c.job_no_mst and b.status_active=1 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0 GROUP BY b.id,b.po_id, b.item_description order by a.batch_no ";
			}
			
			if(str_replace("'",'',$cbo_batch_type)==0 || str_replace("'",'',$cbo_batch_type)==2)
			{
			 $sql_subcon="select a.company_id,a.id,a.batch_no,a.batch_date,a.batch_weight,a.color_id,a.booking_no_id,a.extention_no,a.batch_against,d.party_id as buyer_name, SUM(b.batch_qnty) AS sub_batch_qnty,b.item_description,b.po_id,b.prod_id,b.width_dia_type,group_concat(c.order_no) AS po_number,d.subcon_job,d.job_no_prefix_num,d.party_id,f.process_end_date,f.end_hours,f.end_minutes,f.machine_id,f.load_unload_id,f.result,f.remarks from pro_batch_create_dtls b,  subcon_ord_dtls c, subcon_ord_mst d, pro_batch_create_mst a, pro_fab_subprocess f, lib_color g where a.company_id=$company and f.batch_id=a.id and a.entry_form=36 and g.id=a.color_id and a.id=b.mst_id and f.entry_form=38 and f.load_unload_id=2 and a.batch_against in(1,2)  and b.po_id=c.id and d.subcon_job=c.job_no_mst and b.status_active=1 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0  $dates_com $sub_job_cond $batch_num $sub_buyer_cond $order_no  $suborder_no $year_cond $color_name GROUP BY a.id,a.batch_no,a.company_id, d.party_id,a.batch_date,a.batch_weight,a.color_id,a.booking_no_id,a.extention_no,a.batch_against,b.item_description,b.po_id,b.prod_id,b.width_dia_type,d.subcon_job,d.job_no_prefix_num,d.party_id,f.process_end_date,f.end_hours,f.end_minutes,f.machine_id,f.load_unload_id,f.result,f.remarks ";
			}
		}
		else if($cbo_type==3)//  For Daily Right First Time 
		{
			if(str_replace("'",'',$cbo_batch_type)==0 || str_replace("'",'',$cbo_batch_type)==2)
			{
			$sql="select a.company_id,a.id,a.batch_no,a.batch_date,a.batch_weight,a.color_id,a.booking_no_id,a.extention_no,a.batch_against,SUM(b.batch_qnty) AS batch_qnty,b.item_description,b.po_id,b.prod_id,b.width_dia_type,group_concat( distinct c.po_number ) AS po_number,c.job_no_mst,d.job_no_prefix_num,d.buyer_name,f.process_end_date,f.end_hours,f.end_minutes,f.machine_id,f.load_unload_id,f.ltb_btb_id,f.result,f.remarks from pro_batch_create_dtls b,wo_po_break_down c,wo_po_details_master d,pro_batch_create_mst a,pro_fab_subprocess f,lib_color g	where a.company_id=$company and  f.batch_id=a.id  $dates_com $jobdata $batch_num $buyerdata $order_no $year_cond $color_name	and  g.id=a.color_id and a.entry_form=0 and f.batch_id=a.id  and a.id=b.mst_id and a.batch_against in(1) and f.result=1 and f.ltb_btb_id=1 and f.entry_form=35 and f.load_unload_id in(2)  and b.po_id=c.id and d.job_no=c.job_no_mst and b.status_active=1 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0 GROUP BY b.id,b.po_id, b.item_description order by a.batch_no ";
			$sql_ltb="select a.company_id,a.id,a.batch_no,a.batch_date,a.batch_weight,a.color_id,a.booking_no_id,a.extention_no,a.batch_against,SUM(b.batch_qnty) AS batch_qnty,b.item_description,b.po_id,b.prod_id,b.width_dia_type,group_concat( distinct c.po_number ) AS po_number,c.job_no_mst,d.job_no_prefix_num,d.buyer_name,f.process_end_date,f.end_hours,f.end_minutes,f.machine_id,f.load_unload_id,f.ltb_btb_id,f.result,f.remarks
	from pro_batch_create_dtls b,wo_po_break_down c,wo_po_details_master d,pro_batch_create_mst a,pro_fab_subprocess f,lib_color g where a.company_id=$company and  f.batch_id=a.id $dates_com $jobdata $batch_num $buyerdata $order_no $year_cond $color_name and  g.id=a.color_id and a.entry_form=0 and f.batch_id=a.id  and a.id=b.mst_id and a.batch_against in(1) and f.ltb_btb_id=2 and f.result=1 and f.entry_form=35 and f.load_unload_id in(2)  and b.po_id=c.id and d.job_no=c.job_no_mst and b.status_active=1 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0 GROUP BY b.id,b.po_id, b.item_description order by a.batch_no";
			}
			if(str_replace("'",'',$cbo_batch_type)==0 || str_replace("'",'',$cbo_batch_type)==2)
			{
		  $sql_subcon="select a.company_id,a.id,a.batch_no,a.batch_date,a.batch_weight,a.color_id,a.booking_no_id,a.extention_no,a.batch_against,d.party_id as buyer_name, SUM(b.batch_qnty) AS sub_batch_qnty,b.item_description,b.po_id,b.prod_id,b.width_dia_type,group_concat(distinct c.order_no) AS po_number,d.subcon_job,d.job_no_prefix_num,d.party_id,f.process_end_date,f.ltb_btb_id,f.end_hours,f.end_minutes,f.machine_id,f.load_unload_id,f.result,f.remarks from pro_batch_create_dtls b,  subcon_ord_dtls c, subcon_ord_mst d, pro_batch_create_mst a, pro_fab_subprocess f, lib_color g where a.company_id=$company and f.batch_id=a.id and a.entry_form=36 and g.id=a.color_id and a.id=b.mst_id and f.entry_form=38 and f.load_unload_id in(2) and a.batch_against in(1) and f.result=1 and f.ltb_btb_id=1  and b.po_id=c.id and d.subcon_job=c.job_no_mst and b.status_active=1 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0  $dates_com $sub_job_cond $batch_num $sub_buyer_cond $suborder_no $year_cond $color_name $result_name_cond GROUP BY a.id,a.batch_no,a.company_id, d.party_id,a.batch_date,a.batch_weight,a.color_id,a.booking_no_id,a.extention_no,a.batch_against,b.item_description,b.po_id,b.prod_id,b.width_dia_type,d.subcon_job,d.job_no_prefix_num,d.party_id,f.process_end_date,f.end_hours,f.end_minutes,f.machine_id,f.load_unload_id,f.ltb_btb_id,f.result,f.remarks ";
		 
		 $sql_subcon_ltb="select a.company_id,a.id,a.batch_no,a.batch_date,a.batch_weight,a.color_id,a.booking_no_id,a.extention_no,a.batch_against,d.party_id as buyer_name, SUM(b.batch_qnty) AS sub_batch_qnty,b.item_description,b.po_id,b.prod_id,b.width_dia_type,group_concat(distinct c.order_no) as po_number,d.subcon_job,d.job_no_prefix_num,d.party_id,f.ltb_btb_id,f.process_end_date,f.end_hours,f.end_minutes,f.machine_id,f.load_unload_id,f.result,f.remarks from pro_batch_create_dtls b, subcon_ord_dtls c, subcon_ord_mst d, pro_batch_create_mst a, pro_fab_subprocess f, lib_color g where a.company_id=$company and f.batch_id=a.id and a.entry_form=36 and g.id=a.color_id and a.id=b.mst_id and f.entry_form=38 and f.load_unload_id in(2)   and a.batch_against in(1) and f.result=1 and f.ltb_btb_id=2 and b.po_id=c.id and d.subcon_job=c.job_no_mst and b.status_active=1 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0  $dates_com $sub_job_cond $batch_num $sub_buyer_cond $suborder_no $year_cond $color_name $result_name_cond GROUP BY a.id,a.batch_no,a.company_id, d.party_id,a.batch_date,a.batch_weight,a.color_id,a.booking_no_id,a.extention_no,a.batch_against,b.item_description,b.po_id,b.prod_id,b.width_dia_type,d.subcon_job,d.job_no_prefix_num,d.party_id,f.process_end_date,f.end_hours,f.end_minutes,f.machine_id,f.load_unload_id,f.result,f.remarks,f.ltb_btb_id ";
			}
		}
		else if($cbo_type==4)//  For Daily Dyeing Reprocess Summary
		{
			$sql="select a.company_id, a.id,a.batch_no,a.batch_date,a.batch_weight,a.color_id,a.booking_no_id,a.extention_no,a.batch_against,SUM(b.batch_qnty) AS batch_qnty,b.item_description,b.po_id,b.prod_id,b.width_dia_type,group_concat( distinct c.po_number ) AS po_number,c.job_no_mst,d.job_no_prefix_num,d.buyer_name,f.process_end_date,f.end_hours,f.end_minutes,f.machine_id,f.load_unload_id,f.result,f.remarks
	from pro_batch_create_dtls b,wo_po_break_down c,wo_po_details_master d,pro_batch_create_mst a,pro_fab_subprocess f,lib_color g
	where a.company_id=$company and  f.batch_id=a.id $dates_com $jobdata $batch_num $buyerdata $order_no $year_cond $color_name  $result_name_cond and a.entry_form=0 and  g.id=a.color_id and a.id=b.mst_id and f.entry_form=35 and f.load_unload_id=2  and a.extention_no!=0 and a.batch_against in(2) and b.po_id=c.id and d.job_no=c.job_no_mst and b.status_active=1 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0 GROUP BY b.id,b.po_id, b.item_description order by a.batch_no ";
		}
	}
	else if($db_type==2)// Oracle start here
	{
		if($cbo_type==1)//  For Dyeing WIP
		{
			
			if(str_replace("'",'',$cbo_batch_type)==0 || str_replace("'",'',$cbo_batch_type)==1)
			{
			$sql="select a.company_id,a.batch_no,a.batch_date,a.batch_weight,a.color_id,a.booking_no_id,a.extention_no,a.batch_against,SUM(b.batch_qnty) AS batch_qnty,b.item_description,b.po_id,b.prod_id,b.width_dia_type, LISTAGG(CAST(c.po_number AS VARCHAR2(4000)),',') WITHIN GROUP ( ORDER BY c.po_number) AS po_number,c.job_no_mst,d.job_no_prefix_num,d.buyer_name,f.process_end_date,f.end_hours,f.end_minutes,f.machine_id,f.remarks from pro_batch_create_dtls b,wo_po_break_down c,wo_po_details_master d,pro_batch_create_mst a, pro_fab_subprocess f,lib_color g	where a.company_id=$company $dates_com $jobdata $batch_num $buyerdata $order_no $year_cond $color_name  and g.id=a.color_id and a.entry_form=0 and a.id=b.mst_id and f.batch_id=a.id and f.entry_form=35 and f.load_unload_id=1 and a.batch_against in(1,2) and a.id not in ($batch_id) and b.po_id=c.id and d.job_no=c.job_no_mst and b.status_active=1 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0 GROUP BY b.id,a.company_id, a.batch_no, a.batch_date, a.batch_weight, a.color_id, a.booking_no_id, a.extention_no, a.batch_against,b.item_description, b.po_id, b.prod_id, b.width_dia_type,c.job_no_mst, d.job_no_prefix_num, d.buyer_name, f.process_end_date, f.end_hours, f.end_minutes, f.machine_id, f.remarks order by a.batch_no ";
			}
			if(str_replace("'",'',$cbo_batch_type)==0 || str_replace("'",'',$cbo_batch_type)==2)
			{
			 $sql_subcon="select a.company_id,a.id,a.batch_no,a.batch_date,a.batch_weight,a.color_id,a.booking_no_id,a.extention_no,a.batch_against,d.party_id as buyer_name, SUM(b.batch_qnty) AS sub_batch_qnty,b.item_description,b.po_id,b.prod_id,b.width_dia_type,LISTAGG(CAST(c.order_no AS VARCHAR2(4000)),',') WITHIN GROUP ( ORDER BY c.order_no) AS po_number,d.subcon_job,d.job_no_prefix_num,d.party_id,f.process_end_date,f.end_hours,f.end_minutes,f.machine_id,f.load_unload_id,f.result,f.remarks from pro_batch_create_dtls b,  subcon_ord_dtls c, subcon_ord_mst d, pro_batch_create_mst a, pro_fab_subprocess f, lib_color g where a.company_id=$company and f.batch_id=a.id and a.entry_form=36 and g.id=a.color_id and a.id=b.mst_id and f.entry_form=38 and f.load_unload_id=1 and a.batch_against in(1,2) and a.id not in ($sub_batch_id) and b.po_id=c.id and d.subcon_job=c.job_no_mst and b.status_active=1 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0  $dates_com $sub_job_cond $batch_num $sub_buyer_cond $suborder_no $year_cond $color_name  GROUP BY a.id,a.batch_no,a.company_id, d.party_id,a.batch_date,a.batch_weight,a.color_id,a.booking_no_id,a.extention_no,a.batch_against,b.item_description,b.po_id,b.prod_id,b.width_dia_type,d.subcon_job,d.job_no_prefix_num,d.party_id,f.process_end_date,f.end_hours,f.end_minutes,f.machine_id,f.load_unload_id,f.result,f.remarks ";
			}
		}
		else if($cbo_type==2)//  For  Dyeing  Production
		{
			
			if(str_replace("'",'',$cbo_batch_type)==0 || str_replace("'",'',$cbo_batch_type)==1)
			{
			$sql="select a.company_id,a.id,a.batch_no,a.batch_date,a.batch_weight,a.color_id,a.booking_no_id,a.extention_no,a.batch_against,SUM(b.batch_qnty) AS batch_qnty,b.item_description,b.po_id,b.prod_id,b.width_dia_type,LISTAGG(CAST(c.po_number AS VARCHAR2(4000)),',') WITHIN GROUP ( ORDER BY c.po_number) AS po_number,c.job_no_mst,d.job_no_prefix_num,d.buyer_name,f.process_end_date,f.end_hours,f.end_minutes,f.machine_id,f.load_unload_id,f.result,f.remarks from pro_batch_create_dtls b,wo_po_break_down c,wo_po_details_master d,pro_batch_create_mst a,pro_fab_subprocess f,lib_color g where a.company_id=$company and  f.batch_id=a.id $dates_com $jobdata $batch_num $buyerdata $order_no $year_cond $color_name  $result_name_cond and a.entry_form=0 and  g.id=a.color_id and a.id=b.mst_id and f.entry_form=35 and f.load_unload_id=2 and a.batch_against in(1,2) and b.po_id=c.id and d.job_no=c.job_no_mst and b.status_active=1 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0 GROUP BY b.id,b.po_id, b.item_description,a.company_id, a.id, a.batch_no, a.batch_date, a.batch_weight, a.color_id, a.booking_no_id, a.extention_no, a.batch_against,b.prod_id, b.width_dia_type,c.job_no_mst, d.job_no_prefix_num, d.buyer_name, f.process_end_date, f.end_hours, f.end_minutes, f.machine_id, f.load_unload_id, f.result, f.remarks  order by a.batch_no ";
			}
			if(str_replace("'",'',$cbo_batch_type)==0 || str_replace("'",'',$cbo_batch_type)==2)
			{
			 $sql_subcon="select a.company_id,a.id,a.batch_no,a.batch_date,a.batch_weight,a.color_id,a.booking_no_id,a.extention_no,a.batch_against,d.party_id as buyer_name, SUM(b.batch_qnty) AS sub_batch_qnty,b.item_description,b.po_id,b.prod_id,b.width_dia_type,LISTAGG(CAST(c.order_no AS VARCHAR2(4000)),',') WITHIN GROUP ( ORDER BY c.order_no) AS po_number,d.subcon_job,d.job_no_prefix_num,d.party_id,f.process_end_date,f.end_hours,f.end_minutes,f.machine_id,f.load_unload_id,f.result,f.remarks from pro_batch_create_dtls b,  subcon_ord_dtls c, subcon_ord_mst d, pro_batch_create_mst a, pro_fab_subprocess f, lib_color g where a.company_id=$company and f.batch_id=a.id and a.entry_form=36 and g.id=a.color_id and a.id=b.mst_id and f.entry_form=38 and f.load_unload_id=2 and a.batch_against in(1,2)  and b.po_id=c.id and d.subcon_job=c.job_no_mst and b.status_active=1 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0  $dates_com $sub_job_cond $batch_num  $sub_buyer_cond $suborder_no  $result_name_cond $year_cond $color_name GROUP BY a.id,a.batch_no,a.company_id, d.party_id,a.batch_date,a.batch_weight,a.color_id,a.booking_no_id,a.extention_no,a.batch_against,b.item_description,b.po_id,b.prod_id,b.width_dia_type,d.subcon_job,d.job_no_prefix_num,d.party_id,f.process_end_date,f.end_hours,f.end_minutes,f.machine_id,f.load_unload_id,f.result,f.remarks ";
			}
		}
		else if($cbo_type==3)//  For Daily Right First Time 
		{
			
			if(str_replace("'",'',$cbo_batch_type)==0 || str_replace("'",'',$cbo_batch_type)==1)
			{
			$sql="select a.company_id,a.id,a.batch_no,a.batch_date,a.batch_weight,a.color_id,a.booking_no_id,a.extention_no,a.batch_against,SUM(b.batch_qnty) AS batch_qnty,b.item_description,b.po_id,b.prod_id,b.width_dia_type,LISTAGG(CAST(c.po_number AS VARCHAR2(4000)),',') WITHIN GROUP ( ORDER BY c.po_number) AS po_number,c.job_no_mst,d.job_no_prefix_num,d.buyer_name,f.process_end_date,f.end_hours,f.end_minutes,f.machine_id,f.load_unload_id,f.ltb_btb_id,f.result,f.remarks from pro_batch_create_dtls b,wo_po_break_down c,wo_po_details_master d,pro_batch_create_mst a,pro_fab_subprocess f,lib_color g	where a.company_id=$company and  f.batch_id=a.id  $dates_com $jobdata $batch_num $buyerdata $order_no $year_cond $color_name	and  g.id=a.color_id and a.entry_form=0 and f.batch_id=a.id  and a.id=b.mst_id and a.batch_against in(1) and f.result=1 and f.ltb_btb_id=1 and f.entry_form=35 and f.load_unload_id in(2)  and b.po_id=c.id and d.job_no=c.job_no_mst and b.status_active=1 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0 GROUP BY b.id,b.po_id, b.item_description,a.company_id, a.id, a.batch_no, a.batch_date, a.batch_weight, a.color_id, a.booking_no_id, a.extention_no, a.batch_against,b.prod_id, b.width_dia_type,c.job_no_mst, d.job_no_prefix_num, d.buyer_name, f.process_end_date, f.end_hours, f.end_minutes, f.machine_id, f.load_unload_id, f.ltb_btb_id, f.result, f.remarks order by a.batch_no ";
			$sql_ltb="select a.company_id,a.id,a.batch_no,a.batch_date,a.batch_weight,a.color_id,a.booking_no_id,a.extention_no,a.batch_against,SUM(b.batch_qnty) AS batch_qnty,b.item_description,b.po_id,b.prod_id,b.width_dia_type,LISTAGG(CAST(c.po_number AS VARCHAR2(4000)),',') WITHIN GROUP ( ORDER BY c.po_number) AS po_number,c.job_no_mst,d.job_no_prefix_num,d.buyer_name,f.process_end_date,f.end_hours,f.end_minutes,f.machine_id,f.load_unload_id,f.ltb_btb_id,f.result,f.remarks
	from pro_batch_create_dtls b,wo_po_break_down c,wo_po_details_master d,pro_batch_create_mst a,pro_fab_subprocess f,lib_color g where a.company_id=$company and  f.batch_id=a.id $dates_com $jobdata $batch_num $buyerdata $order_no $year_cond $color_name and  g.id=a.color_id and a.entry_form=0 and f.batch_id=a.id  and a.id=b.mst_id and a.batch_against in(1) and f.ltb_btb_id=2 and f.result=1 and f.entry_form=35 and f.load_unload_id in(2)  and b.po_id=c.id and d.job_no=c.job_no_mst and b.status_active=1 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0 GROUP BY b.id,b.po_id, b.item_description,a.company_id, a.id, a.batch_no, a.batch_date, a.batch_weight, a.color_id, a.booking_no_id, a.extention_no, a.batch_against,b.prod_id, b.width_dia_type,c.job_no_mst, d.job_no_prefix_num, d.buyer_name, f.process_end_date, f.end_hours, f.end_minutes, f.machine_id, f.load_unload_id, f.ltb_btb_id, f.result, f.remarks order by a.batch_no";
			}
	
	if(str_replace("'",'',$cbo_batch_type)==0 || str_replace("'",'',$cbo_batch_type)==2)
			{
		  $sql_subcon="select a.company_id,a.id,a.batch_no,a.batch_date,a.batch_weight,a.color_id,a.booking_no_id,a.extention_no,a.batch_against,d.party_id as buyer_name, SUM(b.batch_qnty) AS sub_batch_qnty,b.item_description,b.po_id,b.prod_id,b.width_dia_type,LISTAGG(CAST(c.order_no AS VARCHAR2(4000)),',') WITHIN GROUP ( ORDER BY c.order_no)  AS po_number,d.subcon_job,d.job_no_prefix_num,d.party_id,f.process_end_date,f.ltb_btb_id,f.end_hours,f.end_minutes,f.machine_id,f.load_unload_id,f.result,f.remarks from pro_batch_create_dtls b,  subcon_ord_dtls c, subcon_ord_mst d, pro_batch_create_mst a, pro_fab_subprocess f, lib_color g where a.company_id=$company and f.batch_id=a.id and a.entry_form=36 and g.id=a.color_id and a.id=b.mst_id and f.entry_form=38 and f.load_unload_id in(2) and a.batch_against in(1) and f.result=1 and f.ltb_btb_id=1  and b.po_id=c.id and d.subcon_job=c.job_no_mst and b.status_active=1 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0  $dates_com $sub_job_cond $batch_num $sub_buyer_cond $suborder_no $year_cond $color_name $result_name_cond GROUP BY a.id,a.batch_no,a.company_id, d.party_id,a.batch_date,a.batch_weight,a.color_id,a.booking_no_id,a.extention_no,a.batch_against,b.item_description,b.po_id,b.prod_id,b.width_dia_type,d.subcon_job,d.job_no_prefix_num,d.party_id,f.process_end_date,f.end_hours,f.end_minutes,f.machine_id,f.load_unload_id,f.ltb_btb_id,f.result,f.remarks ";
		 
		 $sql_subcon_ltb="select a.company_id,a.id,a.batch_no,a.batch_date,a.batch_weight,a.color_id,a.booking_no_id,a.extention_no,a.batch_against,d.party_id as buyer_name, SUM(b.batch_qnty) AS sub_batch_qnty,b.item_description,b.po_id,b.prod_id,b.width_dia_type,LISTAGG(CAST(c.order_no AS VARCHAR2(4000)),',') WITHIN GROUP ( ORDER BY c.order_no) as po_number,d.subcon_job,d.job_no_prefix_num,d.party_id,f.ltb_btb_id,f.process_end_date,f.end_hours,f.end_minutes,f.machine_id,f.load_unload_id,f.result,f.remarks from pro_batch_create_dtls b, subcon_ord_dtls c, subcon_ord_mst d, pro_batch_create_mst a, pro_fab_subprocess f, lib_color g where a.company_id=$company and f.batch_id=a.id and a.entry_form=36 and g.id=a.color_id and a.id=b.mst_id and f.entry_form=38 and f.load_unload_id in(2)   and a.batch_against in(1) and f.result=1 and f.ltb_btb_id=2 and b.po_id=c.id and d.subcon_job=c.job_no_mst and b.status_active=1 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0  $dates_com $sub_job_cond $batch_num $sub_buyer_cond $suborder_no $year_cond $color_name $result_name_cond GROUP BY a.id,a.batch_no,a.company_id, d.party_id,a.batch_date,a.batch_weight,a.color_id,a.booking_no_id,a.extention_no,a.batch_against,b.item_description,b.po_id,b.prod_id,b.width_dia_type,d.subcon_job,d.job_no_prefix_num,d.party_id,f.process_end_date,f.end_hours,f.end_minutes,f.machine_id,f.load_unload_id,f.result,f.remarks,f.ltb_btb_id ";
			}
		}
		else if($cbo_type==4)//  For Daily Dyeing Reprocess Summary
		{
			$sql="select a.company_id, a.id,a.batch_no,a.batch_date,a.batch_weight,a.color_id,a.booking_no_id,a.extention_no,a.batch_against,SUM(b.batch_qnty) AS batch_qnty,b.item_description,b.po_id,b.prod_id,b.width_dia_type,LISTAGG(CAST(c.po_number AS VARCHAR2(4000)),',') WITHIN GROUP ( ORDER BY c.po_number) AS po_number,c.job_no_mst,d.job_no_prefix_num,d.buyer_name,f.process_end_date,f.end_hours,f.end_minutes,f.machine_id,f.load_unload_id,f.result,f.remarks
	from pro_batch_create_dtls b,wo_po_break_down c,wo_po_details_master d,pro_batch_create_mst a,pro_fab_subprocess f,lib_color g
	where a.company_id=$company and  f.batch_id=a.id $dates_com $jobdata $batch_num $buyerdata $order_no $year_cond $color_name  $result_name_cond and a.entry_form=0 and  g.id=a.color_id and a.id=b.mst_id and f.entry_form=35 and f.load_unload_id=2  and a.extention_no!=0 and a.batch_against in(2) and b.po_id=c.id and d.job_no=c.job_no_mst and b.status_active=1 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0 GROUP BY b.id,b.po_id, b.item_description,a.company_id, a.id, a.batch_no, a.batch_date, a.batch_weight, a.color_id, a.booking_no_id, a.extention_no, a.batch_against,b.prod_id, b.width_dia_type,c.job_no_mst, d.job_no_prefix_num, d.buyer_name, f.process_end_date, f.end_hours, f.end_minutes, f.machine_id, f.load_unload_id, f.result, f.remarks  order by a.batch_no ";
		}	
	}
	//$batchdata=sql_select($sql);
		//echo $sql_subcon; die;sql_subcon_ltb
	if($cbo_type==3)
	{
		if(str_replace("'",'',$cbo_batch_type)==0 || str_replace("'",'',$cbo_batch_type)==2)
		{
			$sql_subcon_data_ltb=sql_select($sql_subcon_ltb);
			$sql_subcon_data_btb=sql_select($sql_subcon);
		}
		if(str_replace("'",'',$cbo_batch_type)==0 || str_replace("'",'',$cbo_batch_type)==1)
		{
			//$batchdata=sql_select($sql);
			$batchdata_ltb=sql_select($sql_ltb);
			$batchdata=sql_select($sql);
		}
	}
	if($cbo_type==2)
	{
		if(str_replace("'",'',$cbo_batch_type)==0 || str_replace("'",'',$cbo_batch_type)==2)
		{
			$sql_subcon_data=sql_select($sql_subcon);
		}
		if(str_replace("'",'',$cbo_batch_type)==0 || str_replace("'",'',$cbo_batch_type)==1)
		{
			//echo $sql;
			$batchdata=sql_select($sql);
		}
	//print_r($sql_subcon_data);
	}
	if($cbo_type==1)
	{
		
		if(str_replace("'",'',$cbo_batch_type)==0 || str_replace("'",'',$cbo_batch_type)==1)
		{
			$batchdata=sql_select($sql);
		}
		if(str_replace("'",'',$cbo_batch_type)==0 || str_replace("'",'',$cbo_batch_type)==2)
		{
			//echo $sql_subcon.'xdf';
			$sql_subcon_data=sql_select($sql_subcon);
		}
	//print_r($sql_subcon_data);
	}
	ob_start();
	if($cbo_type==1)//Dyeing WIP Start
	{
	?>
	<div>
	<fieldset style="width:1110px;">
	<div  align="center"> <strong> <?php echo $company_library[$company]; ?> </strong><br><strong> Dyeing WIP </strong> </div>
    <div>
    <?php 
	if(str_replace("'",'',$cbo_batch_type)==0 || str_replace("'",'',$cbo_batch_type)==1)
	{
	if (count($batchdata)>0)
	{
	?>
     <div align="left"> <b>Self batch </b></div>
	 <table class="rpt_table" width="1130" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
			<thead>
				<tr>
					<th width="30">SL</th>
					<th width="80">M/C No</th>
					<th width="100">Buyer</th>
					<th width="80">Job</th>
					<th width="90">PO No</th>
					<th width="100">Fabrics Type</th>
					<th width="70">Dia/Width Type</th>
					<th width="80">Color Name</th>
					<th width="90">Batch No</th>
					<th width="40">Ext. No</th>
					<th width="80">Batch Qty.</th>
					<th width="60">Lot No</th>
					<th width="100">Loading Date & Time</th>
					<th>Reprocess</th>
				</tr>
			</thead>
	</table>
	<div style=" max-height:360px; width:1140px; overflow-y:scroll;" id="scroll_body">
	<table class="rpt_table" id="table_body" width="1110" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
	<tbody>
				<?php 
				$i=1;
				$f=0;
				$btq=0;
				$batch_chk_arr=array();
				foreach($batchdata as $batch)
				{ 
				if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
				$order_id=$batch[csf('po_id')];
				$color_id=$batch[csf('color_id')];
				$desc=explode(",",$batch[csf('item_description')]); 
				$po_number=implode(",",array_unique(explode(",",$batch[csf('po_number')]))); 
				?>
				<tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $i; ?>" onClick="js_set_value(<?php echo $i; ?>)" style="cursor:pointer;">
					<?php if (!in_array($batch[csf('batch_no')],$batch_chk_arr) )
							{ $f++;
								?>
					<td width="30"><?php echo $f; ?></td>
					<td  align="center" width="80" title="<?php echo $machine_arr[$batch[csf('machine_id')]]; ?>"><p><?php echo $machine_arr[$batch[csf('machine_id')]]; ?></p></td>
					<td  width="100" title="<?php echo $buyer_arr[$batch[csf('buyer_name')]]; ?>"><p><?php echo $buyer_arr[$batch[csf('buyer_name')]]; ?></p></td>
					<td  width="80" title="<?php //echo $color_library[$batch[csf('color_id')]]; ?>"><p><?php echo $batch[csf('job_no_prefix_num')]; ?></p></td>
					<td width="90" title="<?php echo $po_number; ?>"><p><?php echo $po_number; ?></p></td>
						<?php	
						$batch_chk_arr[]=$batch[csf('batch_no')];
							} 
							else
							   { ?>
					<td width="30"><?php //echo $sl; ?></td>
					<td  align="center" width="80"><p><?php //echo $machine_id; ?></p></td>
					<td  width="100"><p><?php //echo $buyer_arr[$batch[csf('buyer_name')]]; ?></p></td>
					<td  width="80" title="<?php //echo $color_library[$batch[csf('color_id')]]; ?>"><p><?php //echo $color_library[$batch[csf('color_id')]]; ?></p></td>
					<td width="90"><p><?php //echo $batch[csf('po_number')]; ?></p></td>
							<?php }
							?>
					<td  width="100" title="<?php echo $desc[0]; ?>"><p><?php echo $desc[0]; ?></p></td>
					<td  width="70" title="<?php echo $fabric_typee[$batch[csf('width_dia_type')]]; ?>"><p><?php echo $fabric_typee[$batch[csf('width_dia_type')]]; ?></p></td>
					<td  width="80" title="<?php echo $color_library[$batch[csf('color_id')]]; ?>"><p><?php echo $color_library[$batch[csf('color_id')]]; ?></p></td>
					<td  align="center" width="90" title="<?php echo $batch[csf('batch_no')]; ?>"><p><?php echo $batch[csf('batch_no')]; ?></p></td>
					<td  align="center" width="40" title="<?php echo $batch[csf('extention_no')]; ?>"><p><?php echo $batch[csf('extention_no')]; ?></p></td>
					<td align="right" width="80"   title="<?php echo $batch[csf('batch_qnty')];  ?>"><?php echo number_format($batch[csf('batch_qnty')],2);  ?></td>
					<td align="left" width="60" title="<?php echo $yarn_lot_arr[$batch[csf('prod_id')]][$batch[csf('po_id')]]; ?>"><p><?php echo $yarn_lot_arr[$batch[csf('prod_id')]][$batch[csf('po_id')]];  ?></p></td>
					<td width="100" title="<?php echo change_date_format($batch[csf('process_end_date')]).', '.$batch[csf('end_hours')].':'.$batch[csf('end_minutes')]; ?>"><p><?php  echo change_date_format($batch[csf('process_end_date')]).'<br>'.$batch[csf('end_hours')].':'.$batch[csf('end_minutes')]; ?></p></td>
				  
					<td align="center"  title="<?php if($batch[csf('batch_against')]==2) echo $batch_against[$batch[csf('batch_against')]]; ?>"><?php  if($batch[csf('batch_against')]==2) echo $batch_against[$batch[csf('batch_against')]];?> &nbsp;</td>
				</tr>
	<?php 
	$i++;
	$btq+=$batch[csf('batch_qnty')];
	} //batchdata froeach
	 ?>
	</tbody>
	</table>
	 <table class="rpt_table" width="1110" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
			<tfoot>
				<tr>
					<th width="30">&nbsp;</th>
					<th width="80">&nbsp;</th>
					<th width="100">&nbsp;</th>
					<th width="80">&nbsp;</th>
					<th width="90">&nbsp;</th>
					<th width="100">&nbsp;</th>
					<th width="70">&nbsp;</th>
					<th width="80">&nbsp;</th>
					<th width="130" colspan="" align="right">Batch Total</th>
					<th width="80"><?php echo number_format($btq,2); ?></th>
					<th width="60">&nbsp;</th>
					<th width="100">&nbsp;</th>
					<th>&nbsp;</th>
				</tr>
			</tfoot>
	</table>
	</div>
    <br/>
     <?php 
	}
	}
	 if(str_replace("'",'',$cbo_batch_type)==0 || str_replace("'",'',$cbo_batch_type)==2) 
	{
		if(count($sql_subcon_data)>0)
		{
	?>
    <div align="left"> <b>SubCon batch </b></div>
	 <table class="rpt_table" width="1130" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
			<thead>
				<tr>
					<th width="30">SL</th>
					<th width="80">M/C No</th>
					<th width="100">Buyer</th>
					<th width="80">Job</th>
					<th width="90">PO No</th>
					<th width="100">Fabrics Type</th>
					<th width="70">Dia/Width Type</th>
					<th width="80">Color Name</th>
					<th width="90">Batch No</th>
					<th width="40">Ext. No</th>
					<th width="80">Batch Qty.</th>
					<th width="60">Lot No</th>
					<th width="100">Loading Date & Time</th>
					<th  width="">Reprocess</th>
				</tr>
			</thead>
	</table>
	<div style=" max-height:360px; width:1140px; overflow-y:scroll;" id="scroll_body">
	<table class="rpt_table" id="table_body" width="1110" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
	<tbody>
				<?php 
				$m=1;
				$n=0;
				$sub_btq=0;
				$subbatch_chk_arr=array();
				foreach($sql_subcon_data as $batch)
				{ 
				if ($m%2==0) $subbgcolor="#E9F3FF"; else $subbgcolor="#FFFFFF";
				$order_id=$batch[csf('po_id')];
				$color_id=$batch[csf('color_id')];
				$desc=explode(",",$batch[csf('item_description')]); 
				$po_number=implode(",",array_unique(explode(",",$batch[csf('po_number')]))); 
				?>
				<tr bgcolor="<?php echo $subbgcolor; ?>" id="tr_<?php echo $m; ?>" onClick="js_set_value(<?php echo $m; ?>)" style="cursor:pointer;">
					<?php if (!in_array($batch[csf('batch_no')],$subbatch_chk_arr) )
							{ $n++;
								?>
					<td width="30"><?php echo $n; ?></td>
					<td  align="center" width="80" title="<?php echo $machine_arr[$batch[csf('machine_id')]]; ?>"><p><?php echo $machine_arr[$batch[csf('machine_id')]]; ?></p></td>
					<td  width="100" title="<?php echo $buyer_arr[$batch[csf('buyer_name')]]; ?>"><p><?php echo $buyer_arr[$batch[csf('buyer_name')]]; ?></p></td>
					<td  width="80" title="<?php //echo $color_library[$batch[csf('color_id')]]; ?>"><p><?php echo $batch[csf('job_no_prefix_num')]; ?></p></td>
					<td width="90" title="<?php echo $po_number; ?>"><p><?php echo $po_number; ?></p></td>
						<?php	
						$subbatch_chk_arr[]=$batch[csf('batch_no')];
							} 
							else
							   { ?>
					<td width="30"><?php //echo $sl; ?></td>
					<td  align="center" width="80"><p><?php //echo $machine_id; ?></p></td>
					<td  width="100"><p><?php //echo $buyer_arr[$batch[csf('buyer_name')]]; ?></p></td>
					<td  width="80" title="<?php //echo $color_library[$batch[csf('color_id')]]; ?>"><p><?php //echo $color_library[$batch[csf('color_id')]]; ?></p></td>
					<td width="90"><p><?php //echo $batch[csf('po_number')]; ?></p></td>
							<?php }
							?>
					<td  width="100" title="<?php echo $desc[0]; ?>"><p><?php echo $desc[0]; ?></p></td>
					<td  width="70" title="<?php echo $fabric_typee[$batch[csf('width_dia_type')]]; ?>"><p><?php echo $fabric_typee[$batch[csf('width_dia_type')]]; ?></p></td>
					<td  width="80" title="<?php echo $color_library[$batch[csf('color_id')]]; ?>"><p><?php echo $color_library[$batch[csf('color_id')]]; ?></p></td>
					<td  align="center" width="90" title="<?php echo $batch[csf('batch_no')]; ?>"><p><?php echo $batch[csf('batch_no')]; ?></p></td>
					<td  align="center" width="40" title="<?php echo $batch[csf('extention_no')]; ?>"><p><?php echo $batch[csf('extention_no')]; ?></p></td>
					<td align="right" width="80"   title="<?php echo $batch[csf('sub_batch_qnty')];  ?>"><?php echo number_format($batch[csf('sub_batch_qnty')],2);  ?></td>
					<td align="left" width="60" title="<?php echo $yarn_lot_arr[$batch[csf('prod_id')]][$batch[csf('po_id')]]; ?>"><p><?php echo $yarn_lot_arr[$batch[csf('prod_id')]][$batch[csf('po_id')]];  ?></p></td>
					<td width="100" title="<?php echo change_date_format($batch[csf('process_end_date')]).', '.$batch[csf('end_hours')].':'.$batch[csf('end_minutes')]; ?>"><p><?php  echo change_date_format($batch[csf('process_end_date')]).'<br>'.$batch[csf('end_hours')].':'.$batch[csf('end_minutes')]; ?></p></td>
				  
					<td align="center"  title="<?php if($batch[csf('batch_against')]==2) echo $batch_against[$batch[csf('batch_against')]]; ?>"><?php  if($batch[csf('batch_against')]==2) echo $batch_against[$batch[csf('batch_against')]];?> &nbsp;</td>
				</tr>
	<?php 
	$m++;
	$sub_btq+=$batch[csf('sub_batch_qnty')];
	} //batchdata froeach
	 ?>
	</tbody>
	</table>
	 <table class="rpt_table" width="1110" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
			<tfoot>
				<tr>
					<th width="30">&nbsp;</th>
					<th width="80">&nbsp;</th>
					<th width="100">&nbsp;</th>
					<th width="80">&nbsp;</th>
					<th width="90">&nbsp;</th>
					<th width="100">&nbsp;</th>
					<th width="70">&nbsp;</th>
					<th width="80">&nbsp;</th>
					<th width="90">&nbsp;</th>
					<th width="40">&nbsp;</th>
					<th width="80"><?php echo number_format($sub_btq,2); ?></th>
					<th width="60">&nbsp;</th>
					<th width="100">&nbsp;</th>
					<th>&nbsp;</th>
				</tr>
			</tfoot>
	</table>
	</div>
    </div>
     <?php
	}
	}
	?>
	</fieldset>
	</div>
	<?php
	}
	else if($cbo_type==2) //  Dyeing Production
	{
		 
	?>
	<div>
	<fieldset style="width:1350px;">
	<div align="center"><strong> <?php echo $company_library[$company]; ?> </strong><br> <strong> Daily Dyeing Production </strong><br>
	<?php
		echo  change_date_format($date_from).' '.To.' '.change_date_format($date_to);
		?>
	 </div>
     <?php  
	 if ($cbo_result_name==1 || $cbo_result_name==0)
	 { 
	 
	 ?>
     <div>
              <table cellpadding="0"  width="1310" cellspacing="0" align="center" >
             <tr>
                 <td width="400">
                     <table cellpadding="0"  width="400" cellspacing="0" align="center"  class="rpt_table" rules="all" border="1">
                     	<thead>
                        	<tr>
                            	<th colspan="4">Self Batch (Shade Match)</th>
                            </tr>
                            <tr>
                                <th>SL </th> 
                                <th>Buyer </th>
                                <th>Batch Total</th>
                                <th>%</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
						if ($cbo_result_name==0 || $cbo_result_name==1) $result_name_cond_summary=" and f.result=1"; else $result_name_cond_summary=" ";
						
						if(str_replace("'",'',$cbo_batch_type)==0 || str_replace("'",'',$cbo_batch_type)==1)
						{
                        $sql="select d.buyer_name, SUM(b.batch_qnty) AS batch_qnty from pro_batch_create_dtls b, wo_po_break_down c, wo_po_details_master d, pro_batch_create_mst a, pro_fab_subprocess f, lib_color g where a.company_id=$company and f.batch_id=a.id and a.entry_form=0 and  g.id=a.color_id and a.id=b.mst_id and f.entry_form=35 and f.load_unload_id=2 and a.batch_against in(1,2)  and b.po_id=c.id and d.job_no=c.job_no_mst and b.status_active=1 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0  $dates_com $jobdata $batch_num $buyerdata $order_no $year_cond $color_name  $result_name_cond_summary GROUP BY d.buyer_name ";
						$sql_data=sql_select($sql);
						}
						
						$party_batch_arr=array(); 
						foreach($sql_data as $row)
						{
							$party_batch_arr[$row[csf('buyer_name')]]['qty']=$row[csf('batch_qnty')];
							$party_batch_arr[$row[csf('buyer_name')]]['buyer']=$row[csf('buyer_name')];
							$tot_batch_qty+=$row[csf('batch_qnty')];
						}
						$k=1;
						foreach($party_batch_arr as $key=>$val)
						{
							if ($k%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
							?>
                              <tr bgcolor="<?php echo $bgcolor; ?>" style="cursor:pointer;">
                                  <td><?php echo $k; ?></td>
                                  <td><?php echo $buyer_arr[$key] ?></td>
                                  <td align="right"><?php echo number_format($party_batch_arr[$key]['qty'],2,'.',''); $total_batch_qty+=$party_batch_arr[$key]['qty']; ?></td>
                                  <td align="right"><?php $batch_per=($party_batch_arr[$key]['qty']/$tot_batch_qty)*100; echo number_format($batch_per,2,'.','').'%'; ?></td>
                              </tr>
                        <?php
						$tot_batch_per+=$batch_per;
						$k++;
						
						}
						?>
                        </tbody>
                        <tfoot>
                            <tr> 
                                <th colspan="2" align="right">Total </th>
                                <th align="left"><b><?php echo number_format($total_batch_qty,2,'.','');?></b> </th>
                                <th align="right"><?php echo number_format($tot_batch_per,2,'.','').'%'; ?></th>
                            </tr>
                        </tfoot>
                    </table>
                 </td>
                 <td width="50"></td>
                 <td width="400">
                     <table cellpadding="0"  width="400" cellspacing="0" align="center"  class="rpt_table" rules="all" border="1">
                     	<thead>
                        	<tr>
                            	<th colspan="4">SubContact Batch (Shade Match)</th>
                            </tr>
                            <tr>
                                <th>SL </th> 
                                <th>Buyer </th>
                                <th>Batch Total</th>
                                <th>%</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
						//echo $batch_type=str_replace("'",'',$cbo_batch_type);
						if(str_replace("'",'',$cbo_batch_type)==0 || str_replace("'",'',$cbo_batch_type)==2)
						{
                       $sql_sub="select d.party_id, SUM(b.batch_qnty) AS sub_batch_qnty from pro_batch_create_dtls b,  subcon_ord_dtls c, subcon_ord_mst d, pro_batch_create_mst a, pro_fab_subprocess f, lib_color g where a.company_id=$company and f.batch_id=a.id and a.entry_form=36 and g.id=a.color_id and a.id=b.mst_id and f.entry_form=38 and f.load_unload_id=2 and a.batch_against in(1,2) and b.po_id=c.id and d.subcon_job=c.job_no_mst and b.status_active=1 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0  $dates_com $sub_job_cond $batch_num $sub_buyer_cond $suborder_no $year_cond $color_name $result_name_cond_summary GROUP BY d.party_id ";
					   $sql_data_sub=sql_select($sql_sub);
						}
						$sub_party_batch_arr=array(); 
						foreach($sql_data_sub as $row)
						{
							$sub_party_batch_arr[$row[csf('party_id')]]['qty']=$row[csf('sub_batch_qnty')];
							$sub_party_batch_arr[$row[csf('party_id')]]['buyer']=$row[csf('party_id')];
							$tot_sub_batch_qty+=$row[csf('sub_batch_qnty')];
						}
						$p=1;
						foreach($sub_party_batch_arr as $id=>$sval)
						{
							if ($p%2==0) $bgcolor_sub="#E9F3FF"; else $bgcolor_sub="#FFFFFF";
							
							?>
                              <tr bgcolor="<?php echo $bgcolor_sub; ?>" style="cursor:pointer;">
                                  <td ><?php echo $p; ?></td>
                                  <td><?php echo $buyer_arr[$id] ?></td>
                                  <td align="right"><?php echo number_format($sub_party_batch_arr[$id]['qty'],2,'.',''); $total_sub_batch_qty+=$sub_party_batch_arr[$id]['qty']; ?></td>
                                  <td align="right"><?php $batch_per_sub=($sub_party_batch_arr[$id]['qty']/$tot_sub_batch_qty)*100; echo number_format($batch_per_sub,2,'.','').'%'; ?></td>
                              </tr>
                        <?php
						$total_batch_per_sub+=$batch_per_sub;
						$p++;
						}
						?>
                        </tbody>
                        <tfoot>
                            <tr> 
                                <th colspan="2" align="right">Total </th>
                                <th align="left"><b><?php echo number_format($total_sub_batch_qty,2,'.','');?></b> </th>
                                <th align="right"><?php echo number_format($total_batch_per_sub,2,'.','').'%'; ?></th>
                            </tr>
                        </tfoot>
                    </table>
                 </td>
                 <td width="50"></td>
                 <td width="400">
                     <table cellpadding="0"  width="400" cellspacing="0" align="center"  class="rpt_table" rules="all" border="1">
                     	<thead>
                        	<tr>
                            	<th colspan="5">Summary Total(Shade Match)</th>
                            </tr>
                            <tr>
                                <th>Self Batch</th>
                                <th>SubCon Batch</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                             <tr bgcolor="#FFFFFF"  style="cursor:pointer;">
                                  <td width="30"><?php echo $total_batch_qty; ?></td>
                                  <td width="30"><?php echo $total_sub_batch_qty; ?></td>
                                  <td width="30"><?php  echo $grand_total=$total_batch_qty+$total_sub_batch_qty; ?></td>
                              </tr>
                              <tr bgcolor="#E9F3FF"> 
                               <td><?php 
								  echo number_format(($total_batch_qty/$grand_total)*100,2).'%'; ?></td>
                                  <td><?php  echo number_format(($total_sub_batch_qty/$grand_total)*100,2).'%'; ?></td>
                                  <td><?php  echo '100%'; ?></td>
                              </tr>
                        </tbody>
                    </table>
                 </td>
             </tr>
         </table>
     </div>
     <br />
     <?php 
	}
	
	 if ($cbo_result_name!=1 || $cbo_result_name==0)
	 {
		 
	 ?>
      <div>
         <table cellpadding="0"  width="1310" cellspacing="0" align="center" >
             <tr>
                 <td width="400">
                     <table cellpadding="0"  width="400" cellspacing="0" align="center"  class="rpt_table" rules="all" border="1">
                     	<thead>
                        	<tr>
                            	<th colspan="4">Self Batch (Not Shade Match)</th>
                            </tr>
                            <tr>
                                <th>SL </th> 
                                <th>Buyer </th>
                                <th>Batch Total</th>
                                <th>%</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
						if ($cbo_result_name==0) $result_name_cond_summary_not=" and f.result!=1"; else $result_name_cond_summary_not="  and f.result='".$cbo_result_name."' ";
						if(str_replace("'",'',$cbo_batch_type)==0 || str_replace("'",'',$cbo_batch_type)==1)
						{
                        $sql_not="select d.buyer_name, SUM(b.batch_qnty) AS batch_qnty from pro_batch_create_dtls b, wo_po_break_down c, wo_po_details_master d, pro_batch_create_mst a, pro_fab_subprocess f, lib_color g where a.company_id=$company and f.batch_id=a.id and a.entry_form=0 and  g.id=a.color_id and a.id=b.mst_id and f.entry_form=35 and f.load_unload_id=2 and a.batch_against in(1,2)  and b.po_id=c.id and d.job_no=c.job_no_mst and b.status_active=1 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0  $dates_com $jobdata $batch_num $buyerdata $order_no $year_cond $color_name $result_name_cond_summary_not GROUP BY d.buyer_name ";
						$sql_data_not=sql_select($sql_not);
						}
						
						$party_batch_arr_not=array(); 
						foreach($sql_data_not as $row)
						{
							$party_batch_arr_not[$row[csf('buyer_name')]]['qty']=$row[csf('batch_qnty')];
							$party_batch_arr_not[$row[csf('buyer_name')]]['buyer']=$row[csf('buyer_name')];
							$tot_batch_qty_not+=$row[csf('batch_qnty')];
						}
						$k=1;
						foreach($party_batch_arr_not as $key=>$val)
						{
							if ($k%2==0) $bgcolor_not="#E9F3FF"; else $bgcolor_not="#FFFFFF";
							?>
                              <tr bgcolor="<?php echo $bgcolor_not; ?>" style="cursor:pointer;">
                                  <td><?php echo $k; ?></td>
                                  <td><?php echo $buyer_arr[$key] ?></td>
                                  <td align="right"><?php echo number_format($party_batch_arr_not[$key]['qty'],2,'.',''); $total_batch_qty_not+=$party_batch_arr_not[$key]['qty']; ?></td>                          <td align="right"><?php $batch_per_not=($party_batch_arr_not[$key]['qty']/$tot_batch_qty_not)*100; echo number_format($batch_per,2,'.','').'%'; ?></td>                      	</tr>
                        <?php
						$total_batch_per_not+=$batch_per_not;
						$k++;
						
						}
						?>
                        </tbody>
                        <tfoot>
                            <tr> 
                                <th colspan="2" align="right">Total </th>
                                <th align="left"><b><?php echo number_format($total_batch_qty_not,2,'.','');?></b> </th>
                                <th align="right"><?php echo number_format($total_batch_per_not,2,'.','').'%'; ?></th>
                            </tr>
                        </tfoot>
                    </table>
                 </td>
                 <td width="50"></td>
                 <td width="400">
                     <table cellpadding="0"  width="400" cellspacing="0" align="center"  class="rpt_table" rules="all" border="1">
                     	<thead>
                        	<tr>
                            	<th colspan="4">SubContact Batch (Not Shade Match)</th>
                            </tr>
                            <tr>
                                <th>SL </th> 
                                <th>Buyer </th>
                                <th>Batch Total</th>
                                <th>%</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
						if(str_replace("'",'',$cbo_batch_type)==0 || str_replace("'",'',$cbo_batch_type)==2)
						{
                       $sql_sub_not="select d.party_id, SUM(b.batch_qnty) AS sub_batch_qnty from pro_batch_create_dtls b,  subcon_ord_dtls c, subcon_ord_mst d, pro_batch_create_mst a, pro_fab_subprocess f, lib_color g where a.company_id=$company and f.batch_id=a.id and a.entry_form=36 and g.id=a.color_id and a.id=b.mst_id and f.entry_form=38 and f.load_unload_id=2 and a.batch_against in(1,2) and b.po_id=c.id and d.subcon_job=c.job_no_mst and b.status_active=1 and b.is_deleted=0 and a.status_active=1 and a.is_deleted=0  $dates_com $sub_job_cond $batch_num $sub_buyer_cond $suborder_no $year_cond $color_name $result_name_cond_summary_not GROUP BY d.party_id ";					$sql_data_sub_not=sql_select($sql_sub_not);
						}
						
						
						$sub_party_batch_arr_not=array(); 
						foreach($sql_data_sub_not as $row)
						{
							$sub_party_batch_arr_not[$row[csf('party_id')]]['qty']=$row[csf('sub_batch_qnty')];
							$sub_party_batch_arr_not[$row[csf('party_id')]]['buyer']=$row[csf('party_id')];
							$tot_sub_batch_qty_not+=$row[csf('sub_batch_qnty')];
						}
						$p=1;
						foreach($sub_party_batch_arr_not as $id=>$sval)
						{
							if ($p%2==0) $bgcolor_sub_not="#E9F3FF"; else $bgcolor_sub_not="#FFFFFF";
							
							?>
                              <tr bgcolor="<?php echo $bgcolor_sub_not; ?>" style="cursor:pointer;">
                                  <td ><?php echo $p; ?></td>
                                  <td><?php echo $buyer_arr[$id] ?></td>
                                  <td align="right"><?php echo number_format($sub_party_batch_arr_not[$id]['qty'],2,'.',''); $total_sub_batch_qty_not+=$sub_party_batch_arr_not[$id]['qty']; ?></td>
                                  <td align="right"><?php $batch_per_sub_not=($sub_party_batch_arr_not[$id]['qty']/$tot_sub_batch_qty_not)*100; echo number_format($batch_per_sub,2,'.','').'%'; ?></td>
                              </tr>
                        <?php
						$total_batch_per_sub_not+=$batch_per_sub;
						$p++;
						}
						?>
                        </tbody>
                        <tfoot>
                            <tr> 
                                <th colspan="2" align="right">Total </th>
                                <th align="left"><b><?php echo number_format($total_sub_batch_qty_not,2,'.','');?></b> </th>
                                <th align="right"><?php echo number_format($total_batch_per_sub_not,2,'.','').'%'; ?></th>
                            </tr>
                        </tfoot>
                    </table>
                 </td>
                 <td width="50"></td>
                 <td width="400">
                     <table cellpadding="0"  width="400" cellspacing="0" align="center"  class="rpt_table" rules="all" border="1">
                     	<thead>
                        	<tr>
                            	<th colspan="5">Summary Total( Not Shade Match)</th>
                            </tr>
                            <tr>
                                <th>Self Batch</th>
                                <th>SubCon Batch</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                             <tr bgcolor="#FFFFFF"  style="cursor:pointer;">
                                  <td width="30"><?php echo $total_batch_qty_not; ?></td>
                                  <td width="30"><?php echo $total_sub_batch_qty_not; ?></td>
                                  <td width="30"><?php  echo $grand_total_not=$total_batch_qty_not+$total_sub_batch_qty_not; ?></td>
                              </tr>
                              <tr bgcolor="#E9F3FF"> 
                               <td><?php 
								  echo number_format(($total_batch_qty_not/$grand_total_not)*100,2).'%'; ?></td>
                                  <td><?php  echo number_format(($total_sub_batch_qty_not/$grand_total_not)*100,2).'%'; ?></td>
                                  <td><?php  echo '100%'; ?></td>
                              </tr>
                        </tbody>
                    </table>
                 </td>
             </tr>
         </table>
     </div>
     <br/>
     <?php
	 }
	if(str_replace("'",'',$cbo_batch_type)==0 || str_replace("'",'',$cbo_batch_type)==1)
	 {
		if (count($batchdata)>0)
		{
	 ?>
     <div align="left">
     <div> <b>Self batch </b></div>
	 <table class="rpt_table" width="1320" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
			<thead>
				<tr>
					<th width="30">SL</th>
					<th width="80">Date</th>
					<th width="80">M/C No</th>
					<th width="100">Buyer</th>
					<th width="80">Job</th>
					<th width="90">PO No</th>
					<th width="100">Fabrics Type</th>
					<th width="70">Dia/Width Type</th>
					<th width="80">Color Name</th>
					<th width="90">Batch No</th>
					<th width="40">Extn. No</th>
					<th width="70">Batch Qty.</th>
					<th width="50">Lot No</th>
					<th width="75">Load Date & Time</th>
					<th width="75">UnLoad Date Time</th>
					<th width="60">Time Used</th>
					<th>Result</th>
				</tr>
			</thead>
	</table>
	<div style=" max-height:350px; width:1350px; overflow-y:scroll;;" id="scroll_body">
	<table class="rpt_table" id="table_body" width="1320" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
	<tbody>
				<?php 
				$i=1;
				$btq=0;
				$batch_chk_arr=array();
				foreach($batchdata as $batch)
				{ 
				if ($i%2==0)  
				$bgcolor_dyeing="#E9F3FF";
				else
				$bgcolor_dyeing="#FFFFFF";
				$order_id=$batch[csf('po_id')];
				$color_id=$batch[csf('color_id')];
				$desc=explode(",",$batch[csf('item_description')]); 
				$po_number=implode(",",array_unique(explode(",",$batch[csf('po_number')]))); 
				?>
				<tr bgcolor="<?php echo $bgcolor_dyeing; ?>" id="tr_<?php echo $i; ?>" onClick="js_set_value(<?php echo $i; ?>)" style="cursor:pointer;">
					<td width="30"><?php echo $i; ?></td>
					<td width="80" title="<?php echo change_date_format($batch[csf('process_end_date')]); ?>"><p><?php echo change_date_format($batch[csf('process_end_date')]); $unload_date=$batch[csf('process_end_date')]; ?></p></td>
					<td  align="center" width="80" title="<?php echo $machine_arr[$batch[csf('machine_id')]]; ?>"><p><?php echo $machine_arr[$batch[csf('machine_id')]]; ?></p></td>
					<td  width="100" title="<?php echo $buyer_arr[$batch[csf('buyer_name')]]; ?>"><p><?php echo $buyer_arr[$batch[csf('buyer_name')]]; ?></p></td>
					<td  width="80" title="<?php echo  $batch[csf('job_no_prefix_num')]; ?>"><p><?php echo $batch[csf('job_no_prefix_num')]; ?></p></td>
					<td  width="90" title="<?php echo $po_number; ?>"><p><?php echo $po_number; ?></p></td>
					<td  width="100" title="<?php echo $desc[0]; ?>"><p><?php echo $desc[0]; ?></p></td>
					<td  width="70" title="<?php echo  $fabric_typee[$batch[csf('width_dia_type')]]; ?>"><p><?php echo $fabric_typee[$batch[csf('width_dia_type')]]; ?></p></td>
					<td  width="80" title="<?php echo $color_library[$batch[csf('color_id')]]; ?>"><p><?php echo $color_library[$batch[csf('color_id')]]; ?></p></td>
					<td  align="center" width="90" title="<?php echo $batch[csf('batch_no')]; ?>"><p><?php echo $batch[csf('batch_no')]; ?></p></td>
					<td  align="center" width="40" title="<?php echo $batch[csf('extention_no')]; ?>"><p><?php echo $batch[csf('extention_no')]; ?></p></td>
					<td align="right" width="70" title="<?php echo $batch[csf('batch_qnty')];  ?>"><?php echo number_format($batch[csf('batch_qnty')],2);  ?></td>
					<td align="left" width="50" title="<?php echo $yarn_lot_arr[$batch[csf('prod_id')]][$batch[csf('po_id')]]; ?>"><p><?php echo $yarn_lot_arr[$batch[csf('prod_id')]][$batch[csf('po_id')]];  ?></p></td>
					<td width="75" title="<?php  $load_t=$load_hr[$batch[csf('id')]].':'.$load_min[$batch[csf('id')]]; echo change_date_format($load_date[$batch[csf('id')]]).'  & '.$load_t; ?>"><p><?php   $load_t=$load_hr[$batch[csf('id')]].':'.$load_min[$batch[csf('id')]]; echo change_date_format($load_date[$batch[csf('id')]]).' <br> '.$load_t;
					 ?></p></td>
					<td width="75" title="<?php $hr=strtotime($unload_date,$load_t); $min=($batch[csf('end_minutes')])-($load_min[$batch[csf('id')]]); echo  $unloadd=change_date_format($batch[csf('process_end_date')]).' &amp;'.$unload_time=$batch[csf('end_hours')].':'.$batch[csf('end_minutes')]; ?>"><p><?php $hr=strtotime($unload_date,$load_t); $min=($batch[csf('end_minutes')])-($load_min[$batch[csf('id')]]); echo  $unloadd=change_date_format($batch[csf('process_end_date')]).'<br>'.$unload_time=$batch[csf('end_hours')].':'.$batch[csf('end_minutes')];?></p></td>
					<td align="center" width="60">
				   <?php     
				$new_date_time_unload=($unload_date.' '.$unload_time.':'.'00');
				$new_date_time_load=($load_date[$batch[csf('id')]].' '.$load_t.':'.'00');
				$total_time=datediff(n,$new_date_time_load,$new_date_time_unload);
				echo floor($total_time/60).":".$total_time%60;
				//echo ($total_time/60 - $total_time%3600/60)/60 .':'.$total_time%3600/60;
						?>
					 </td>
					<td align="center" title="<?php  echo $dyeing_result[$batch[csf('result')]];   ?>"><p><?php echo $dyeing_result[$batch[csf('result')]]; ?></p> </td>
				</tr>
	<?php 
	$i++;
	$btq+=$batch[csf('batch_qnty')];
	} //batchdata froeach
	 ?>
		</tbody>
	</table>
	<table class="rpt_table" width="1320" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
			<tfoot>
				<tr>
					<th width="30">&nbsp;</th>
					<th width="80">&nbsp;</th>
					<th width="80">&nbsp;</th>
					<th width="100">&nbsp;</th>
					<th width="80">&nbsp;</th>
					<th width="90">&nbsp;</th>
					<th width="100">&nbsp;</th>
					<th width="70">&nbsp;</th>
					<th width="80">&nbsp;</th>
					<th width="90">&nbsp;</th>
					<th width="40">Total</th>
					<th width="70"><?php echo number_format($btq,2); ?></th>
					<th width="50">&nbsp;</th>
					<th width="75">&nbsp;</th>
					<th width="75">&nbsp;</th>
					<th width="60">&nbsp;</th>
					<th>&nbsp;</th>
				</tr>
			</tfoot>
	</table>
	</div>
    </div>
    <br/>
    <?php }
	}
		
	if(str_replace("'",'',$cbo_batch_type)==0 || str_replace("'",'',$cbo_batch_type)==2) 
	{
		if(count($sql_subcon_data)>0)
		{
	?>
     <div align="left">
      <div> <b> Subcon batch</b> </div>
	 <table class="rpt_table" width="1320" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
			<thead>
				<tr>
					<th width="30">SL</th>
					<th width="80">Date</th>
					<th width="80">M/C No</th>
					<th width="100">Buyer</th>
					<th width="80">Job</th>
					<th width="90">PO No</th>
					<th width="100">Fabrics Type</th>
					<th width="70">Dia/Width Type</th>
					<th width="80">Color Name</th>
					<th width="90">Batch No</th>
					<th width="40">Extn. No</th>
					<th width="70">Batch Qty.</th>
					<th width="50">Lot No</th>
					<th width="75">Load Date & Time</th>
					<th width="75">UnLoad Date Time</th>
					<th width="60">Time Used</th>
					<th>Result</th>
				</tr>
			</thead>
	</table>
	<div style=" max-height:350px; width:1350px; overflow-y:scroll;;" id="scroll_body">
	<table class="rpt_table" id="table_body2" width="1320" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
	<tbody>
				<?php 
				$i=1;
				$btq=0;
				$batch_chk_arr=array();
				foreach($sql_subcon_data as $batch)
				{ 
				if ($i%2==0)  
				$bgcolor_sub2="#E9F3FF";
				else
				$bgcolor_sub2="#FFFFFF";
				$order_id=$batch[csf('po_id')];
				$color_id=$batch[csf('color_id')];
				$desc=explode(",",$batch[csf('item_description')]); 
				$po_number=implode(",",array_unique(explode(",",$batch[csf('po_number')]))); 
				//echo $load_hr[$batch[csf('id')]].'ddddd';
				//echo $batch[csf('id')];
				?>
				<tr bgcolor="<?php echo $bgcolor_sub2; ?>" id="trsub_<?php echo $i; ?>" onclick="change_color('trsub_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" style="cursor:pointer;">
					<td width="30"><?php echo $i; ?></td>
					<td width="80" title="<?php echo change_date_format($batch[csf('process_end_date')]); ?>"><p><?php echo change_date_format($batch[csf('process_end_date')]); $unload_date=$batch[csf('process_end_date')]; ?></p></td>
					<td  align="center" width="80" title="<?php echo $machine_arr[$batch[csf('machine_id')]]; ?>"><p><?php echo $machine_arr[$batch[csf('machine_id')]]; ?></p></td>
					<td  width="100" title="<?php echo $buyer_arr[$batch[csf('buyer_name')]]; ?>"><p><?php echo $buyer_arr[$batch[csf('buyer_name')]]; ?></p></td>
					<td  width="80" title="<?php echo  $batch[csf('job_no_prefix_num')]; ?>"><p><?php echo $batch[csf('job_no_prefix_num')]; ?></p></td>
					<td  width="90" title="<?php echo $po_number; ?>"><p><?php echo $po_number; ?></p></td>
					<td  width="100" title="<?php echo $desc[0]; ?>"><p><?php echo $desc[0]; ?></p></td>
					<td  width="70" title="<?php echo  $fabric_typee[$batch[csf('width_dia_type')]]; ?>"><p><?php echo $fabric_typee[$batch[csf('width_dia_type')]]; ?></p></td>
					<td  width="80" title="<?php echo $color_library[$batch[csf('color_id')]]; ?>"><p><?php echo $color_library[$batch[csf('color_id')]]; ?></p></td>
					<td  align="center" width="90" title="<?php echo $batch[csf('batch_no')]; ?>"><p><?php echo $batch[csf('batch_no')]; ?></p></td>
					<td  align="center" width="40" title="<?php echo $batch[csf('extention_no')]; ?>"><p><?php echo $batch[csf('extention_no')]; ?></p></td>
					<td align="right" width="70" title="<?php echo $batch[csf('batch_qnty')];  ?>"><?php echo number_format($batch[csf('sub_batch_qnty')],2);  ?></td>
					<td align="left" width="50" title="<?php echo $yarn_lot_arr[$batch[csf('prod_id')]][$batch[csf('po_id')]]; ?>"><p><?php echo $yarn_lot_arr[$batch[csf('prod_id')]][$batch[csf('po_id')]];  ?></p></td>
					<td width="75" title="<?php  $sub_load_t=$subcon_load_hr[$batch[csf('id')]].':'.$subcon_load_min[$batch[csf('id')]]; echo change_date_format($subcon_load_date[$batch[csf('id')]]).'  & '.$sub_load_t; ?>"><p><?php   $sub_load_t=$subcon_load_hr[$batch[csf('id')]].':'.$subcon_load_min[$batch[csf('id')]]; echo change_date_format($subcon_load_date[$batch[csf('id')]]).' <br> '.$sub_load_t;
					 ?></p></td>
					<td width="75" title="<?php $hr=strtotime($unload_date,$load_t); $min=($batch[csf('end_minutes')])-($load_min[$batch[csf('id')]]); echo  $unloadd=change_date_format($batch[csf('process_end_date')]).'<br>'.$unload_time=$batch[csf('end_hours')].':'.$batch[csf('end_minutes')]; ?>"><p><?php $hr=strtotime($unload_date,$load_t); $min=($batch[csf('end_minutes')])-($load_min[$batch[csf('id')]]); echo  $unloadd=change_date_format($batch[csf('process_end_date')]).' &amp;'.$unload_time=$batch[csf('end_hours')].':'.$batch[csf('end_minutes')];?></p></td>
					<td align="center" width="60">
				   <?php     
				$new_date_time_unload=($unload_date.' '.$unload_time.':'.'00');
				$new_date_time_load=($subcon_load_date[$batch[csf('id')]].' '.$sub_load_t.':'.'00');
				$total_time=datediff(n,$new_date_time_load,$new_date_time_unload);
				echo floor($total_time/60).":".$total_time%60;
				//echo ($total_time/60 - $total_time%3600/60)/60 .':'.$total_time%3600/60;
						?>
					 </td>
					<td align="center" title="<?php  echo $dyeing_result[$batch[csf('result')]];   ?>"><p><?php echo $dyeing_result[$batch[csf('result')]]; ?></p> </td>
				</tr>
	<?php 
	$i++;
	$btq_sub+=$batch[csf('sub_batch_qnty')];
	} //batchdata froeach
	 ?>
		</tbody>
	</table>
	<table class="rpt_table" width="1320" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
			<tfoot>
				<tr>
					<th width="30">&nbsp;</th>
					<th width="80">&nbsp;</th>
					<th width="80">&nbsp;</th>
					<th width="100">&nbsp;</th>
					<th width="80">&nbsp;</th>
					<th width="90">&nbsp;</th>
					<th width="100">&nbsp;</th>
					<th width="70">&nbsp;</th>
					<th width="80">&nbsp;</th>
					<th width="90">&nbsp;</th>
					<th width="40">Total</th>
					<th width="70"><?php echo number_format($btq_sub,2); ?></th>
					<th width="50">&nbsp;</th>
					<th width="75">&nbsp;</th>
					<th width="75">&nbsp;</th>
					<th width="60">&nbsp;</th>
					<th>&nbsp;</th>
				</tr>
			</tfoot>
	</table>
	</div>
    </div>
    <?php
	}
	}
	?>
	</fieldset>
	</div>
	<?php
	}
	else if($cbo_type==3) // Daily Right First Time
	{
		if(str_replace("'",'',$cbo_batch_type)==0 || str_replace("'",'',$cbo_batch_type)==1) 
		{
	?>
	<div>
	<fieldset style="width:1505px;">
	<div align="center"><strong> <?php echo $company_library[$company]; ?> </strong><br> <strong> Daily Right First Time </strong><br>
	<?php
		echo  change_date_format($date_from).' '.To.' '.change_date_format($date_to);
	?>
	 </div>
     <div>
     <div align="left"> <b>Self batch </b></div>
	 <table class="rpt_table" width="1493" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
	 <caption><Strong>BTB</Strong></caption>
			<thead>
				<tr>
					<th width="30">SL</th>
					<th width="80">Date</th>
					<th width="80">M/C No</th>
					<th width="100">Buyer</th>
					<th width="80">Job</th>
					<th width="90">PO No</th>
					<th width="100">Fabrics Type</th>
					<th width="80">Dia/Width Type</th>
					<th width="80">Color Name</th>
					<th width="90">Batch No</th>
					<th width="70">Batch Qty.</th>
					<th width="45">LTB/ BTB</th>
					<th width="100">Unload Date & Time</th>
					<th width="60">Time Used</th>
					<th width="100">RFT</th>
					<th width="100">Remark</th>
					<th width="100">Machine Utilization</th>
					<th>Lot No</th>
				</tr>
			</thead>
	</table>
	<div style=" max-height:350px; width:1505px; overflow-y:scroll;" id="scroll_body">
	<table class="rpt_table" id="table_body" width="1485" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
	<tbody>
					<?php 
					$i=1;
					$f=0;
					$btq=0;
					$batch_chk_arr=array();
			if (count($batchdata)>0)
			 {
					foreach($batchdata as $batch)
					{ 
					if ($i%2==0)  
					$bgcolor="#E9F3FF";
					else
					$bgcolor="#FFFFFF";
					$order_id=$batch[csf('po_id')];
					$color_id=$batch[csf('color_id')];
					if($batch[csf('result')]==1)
					{
					$shade="Shade Matched";	
					}
					else
					{
					$shade="";		
					}
					$desc=explode(",",$batch[csf('item_description')]); 
					$po_number=implode(",",array_unique(explode(",",$batch[csf('po_number')]))); 
					?>
				<tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $i; ?>" onClick="js_set_value(<?php echo $i; ?>)" style="cursor:pointer;">
					<td width="30"><?php echo $i; ?></td>
					<td width="80" title="<?php echo change_date_format($batch[csf('process_end_date')]); ?>"><p><?php echo change_date_format($batch[csf('process_end_date')]); $unload_date=$batch[csf('process_end_date')]; ?></p></td>
					<td  align="center" width="80" title="<?php echo $machine_arr[$batch[csf('machine_id')]]; ?>"><p><?php echo $machine_arr[$batch[csf('machine_id')]]; ?></p></td>
					<td  width="100" title="<?php echo $buyer_arr[$batch[csf('buyer_name')]]; ?>"><p><?php echo $buyer_arr[$batch[csf('buyer_name')]]; ?></p></td>
					<td  width="80" title="<?php echo $batch[csf('job_no_prefix_num')]; ?>"><p><?php echo $batch[csf('job_no_prefix_num')]; ?></p></td>
					<td width="90" title="<?php echo $po_number; ?>"><p><?php echo $po_number; ?></p></td>
					<td  width="100" title="<?php echo $desc[0]; ?>"><p><?php echo $desc[0]; ?></p></td>
					<td  width="80" title="<?php echo $fabric_typee[$batch[csf('width_dia_type')]]; ?>"><p><?php echo $fabric_typee[$batch[csf('width_dia_type')]]; ?></p></td>
					<td  width="80" title="<?php echo $color_library[$batch[csf('color_id')]]; ?>"><p><?php echo $color_library[$batch[csf('color_id')]]; ?></p></td>
					<td  align="center" width="90" title="<?php echo $batch[csf('batch_no')]; ?>"><p><?php echo $batch[csf('batch_no')]; ?></p></td>
					<td  align="right" width="70" title="<?php echo $batch[csf('batch_qnty')]; ?>"><p><?php echo $batch[csf('batch_qnty')]; ?></p></td>
					<td align="center" width="45" title="<?php echo $ltb_btb[$batch[csf('ltb_btb_id')]]; ?>"><?php echo $ltb_btb[$batch[csf('ltb_btb_id')]]; ?></td>
					<td width="100" align="center" title="<?php //echo $load_hr[$batch[csf('id')]].':'.$load_min[$batch[csf('id')]]; ?>"><p><?php   echo  change_date_format($batch[csf('process_end_date')]) .' <br> '.$batch[csf('end_hours')].':'.$batch[csf('end_minutes')]; ?></p></td>
					  <td align="center" width="60"><?php $hr=$batch[csf('end_hours')]-$load_hr[$batch[csf('id')]]; $min=$batch[csf('end_minutes')]-$load_min[$batch[csf('id')]]; //echo  $hr.':'.$min;
					$load_t=$load_hr[$batch[csf('id')]].':'.$load_min[$batch[csf('id')]];
					$unload_t=$batch[csf('end_hours')].':'.$batch[csf('end_minutes')];
					$new_date_time_unload=($unload_date.' '.$unload_t.':'.'00');
					$new_date_time_load=($load_date[$batch[csf('id')]].' '.$load_t.':'.'00');
				   //$unload_time=strtotime($unload_date,$unload_time);
					//$load_time=strtotime($load_date,$load_t);
					$total_time=datediff(n,$new_date_time_load,$new_date_time_unload);
					echo floor($total_time/60).":".$total_time%60;
				//echo ($total_time/60 - $total_time%3600/60)/60 .':'.$total_time%3600/60;
						?></td>
					<td width="100" title="<?php echo $shade;  ?>"><p><?php  echo $shade; ?></p></td>
					<td width="100" title="<?php ?>"><p><?php echo  $batch[csf('remarks')]; ?></p></td>
				  
					<td align="center" width="100" title="<?php  $utiliz=$batch[csf('batch_qnty')]/$m_capacity[$batch[csf('machine_id')]]*100; echo number_format($utiliz,2).'%'; ?>"><?php  $utiliz=$batch[csf('batch_qnty')]/$m_capacity[$batch[csf('machine_id')]]*100; echo number_format($utiliz,2).'%'; ?> </td>
					<td align="left" title="<?php echo  $yarn_lot_arr[$batch[csf('prod_id')]][$batch[csf('po_id')]];   ?>"><p><?php echo  $yarn_lot_arr[$batch[csf('prod_id')]][$batch[csf('po_id')]]; ?></p> </td>
				</tr>
	<?php 
	$i++;
	$btq+=$batch[csf('batch_qnty')];
	} //batchdata froeach
	 ?>
			  <tr bgcolor="#CCCCCC">
				<td colspan="10" align="right"><Strong>BTB Total:</Strong> <?php //echo $b_qty; ?> </td>
				<td align="right"><?php echo number_format($btq,2); ?>&nbsp;</td>
				<td colspan="7">&nbsp;</td>
			 </tr>
			 <tr bgcolor="#C2DCFF">
				<td colspan="18" align="center"><strong>LTB</strong></td>
			 </tr>
		 <?php
			 }
		 if (count($batchdata_ltb)>0)
		 {
				$d=1;
				foreach($batchdata_ltb as $batch_ltb)
				{ 
					if ($d%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
					$order_id=$batch_ltb[csf('po_id')];
					$color_id=$batch_ltb[csf('color_id')];
					if($batch_ltb[csf('result')]==1)
					{
						$shade="Shade Matched";	
					}
					else
					{
						$shade="";	
					}
					$desc=explode(",",$batch_ltb[csf('item_description')]); 
					$po_number=implode(",",array_unique(explode(",",$batch_ltb[csf('po_number')]))); 
					?>
				<tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $d; ?>" onClick="js_set_value(<?php echo $d; ?>)" style="cursor:pointer;">
					<td width="30"><?php echo $d; ?></td>
					<td width="80" title="<?php echo change_date_format($batch_ltb[csf('process_end_date')]); ?>"><p><?php echo change_date_format($batch_ltb[csf('process_end_date')]);$unload_date1=$batch_ltb[csf('process_end_date')]; ?></p></td>
					<td  align="center" width="80" title="<?php echo $machine_arr[$batch_ltb[csf('machine_id')]]; ?>"><p><?php echo $machine_arr[$batch_ltb[csf('machine_id')]]; ?></p></td>
					<td  width="100" title="<?php echo $buyer_arr[$batch_ltb[csf('buyer_name')]]; ?>"><p><?php echo $buyer_arr[$batch_ltb[csf('buyer_name')]]; ?></p></td>
					<td  width="80" title="<?php echo $batch[csf('job_no_prefix_num')]; ?>"><p><?php echo $batch[csf('job_no_prefix_num')]; ?></p></td>
					<td width="90" title="<?php echo $po_number; ?>"><p><?php echo $po_number; ?></p></td>
					<td  width="100" title="<?php echo $desc[0]; ?>"><p><?php echo $desc[0]; ?></p></td>
					<td  width="80" title="<?php echo $fabric_typee[$batch[csf('width_dia_type')]]; ?>"><p><?php echo $fabric_typee[$batch[csf('width_dia_type')]]; ?></p></td>
					<td  width="80" title="<?php echo $color_library[$batch_ltb[csf('color_id')]]; ?>"><p><?php echo $color_library[$batch_ltb[csf('color_id')]]; ?></p></td>
					<td  align="center" width="90" title="<?php echo $batch_ltb[csf('batch_no')]; ?>"><p><?php echo $batch_ltb[csf('batch_no')]; ?></p></td>
					<td  align="right" width="70" title="<?php echo $batch_ltb[csf('batch_qnty')]; ?>"><p><?php echo $batch_ltb[csf('batch_qnty')]; ?></p></td>
					<td align="center" width="45" title="<?php echo $ltb_btb[$batch_ltb[csf('ltb_btb_id')]]; ?>"><?php echo $ltb_btb[$batch_ltb[csf('ltb_btb_id')]]; ?></td>
					<td width="100" align="center" title="<?php //echo $load_hr[$batch[csf('id')]].':'.$load_min[$batch[csf('id')]]; ?>"><p><?php   echo  change_date_format($batch_ltb[csf('process_end_date')]) .'<br>'.$batch_ltb[csf('end_hours')].':'.$batch_ltb[csf('end_minutes')]; ?></p></td>
					   <td align="center" width="60" title="<?php echo  $hr.':'.$min;  ?>"><?php $hr=$batch_ltb[csf('end_hours')]-$load_hr[$batch_ltb[csf('id')]]; $min=$batch_ltb[csf('end_minutes')]-$load_min[$batch_ltb[csf('id')]]; //echo  $hr.':'.$min; 
					$load_time=$load_hr[$batch_ltb[csf('id')]].':'.$load_min[$batch_ltb[csf('id')]];
					$unload_time=$batch_ltb[csf('end_hours')].':'.$batch_ltb[csf('end_minutes')];
					$new_date_time_unload1=($unload_date1.' '.$unload_time.':'.'00');
					$new_date_time_load1=($load_date[$batch_ltb[csf('id')]].' '.$load_time.':'.'00');
					$total_time1=datediff(n,$new_date_time_load1,$new_date_time_unload1);
					echo floor($total_time1/60).":".$total_time1%60;
			//echo ($total_time/60 - $total_time%3600/60)/60 .':'.$total_time%3600/60;
						?></td>
					<td width="100" title="<?php echo $shade ?>"><p><?php  echo $shade; ?></p></td>
					<td width="100" title="<?php ?>"><p><?php echo  $batch_ltb[csf('remarks')];  ?></p></td>
					<td align="center" width="100" title="<?php //echo $hr.':'.$min;   ?>"><?php  $utiliz=$batch_ltb[csf('batch_qnty')]/$m_capacity[$batch_ltb[csf('machine_id')]]*100; 		
					echo number_format($utiliz,2).'%'; ?> </td>
					<td align="left" title="<?php echo  $yarn_lot_arr[$batch_ltb[csf('prod_id')]][$batch_ltb[csf('po_id')]];   ?>"><p><?php echo  $yarn_lot_arr[$batch_ltb[csf('prod_id')]][$batch_ltb[csf('po_id')]]; ?></p> </td>
				</tr>
						<?php			
				$d++;
				$total_ltb+=$batch_ltb[csf('batch_qnty')];
				}
				?>
				<table class="rpt_table" width="1485" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
			<tfoot>
				<tr>
					<th width="30">&nbsp;</th>
					<th width="80">&nbsp;</th>
					<th width="80">&nbsp;</th>
					<th width="100">&nbsp;</th>
					<th width="80">&nbsp;</th>
					<th width="90">&nbsp;</th>
					<th width="100">&nbsp;</th>
					<th width="80">&nbsp;</th>
					<th width="80">&nbsp;</th>
					<th width="90">&nbsp;</th>
					<th width="70"><?php echo number_format($total_ltb,2); ?></th>
					<th width="45">&nbsp;</th>
					<th width="100">&nbsp;</th>
					<th width="60">&nbsp;</th>
					<th width="100">&nbsp;</th>
					<th width="100">&nbsp;</th>
					<th width="100">&nbsp;</th>
					<th>&nbsp;</th>
				</tr>
			</tfoot>
	</table>
				<?php
			 }
		 ?>    
	</tbody>
	</table>
	</div>
    </div>
    <br>
    <?php
		}
		if(str_replace("'",'',$cbo_batch_type)==0 || str_replace("'",'',$cbo_batch_type)==2) 
		{
	?>
     <div align="left"> <b>SubCon batch </b></div>
	 <table class="rpt_table" width="1493" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
	 <caption><Strong>BTB</Strong></caption>
			<thead>
				<tr>
					<th width="30">SL</th>
					<th width="80">Date</th>
					<th width="80">M/C No</th>
					<th width="100">Buyer</th>
					<th width="80">Job</th>
					<th width="90">PO No</th>
					<th width="100">Fabrics Type</th>
					<th width="80">Dia/Width Type</th>
					<th width="80">Color Name</th>
					<th width="90">Batch No</th>
					<th width="70">Batch Qty.</th>
					<th width="45">LTB/ BTB</th>
					<th width="100">Unload Date & Time</th>
					<th width="60">Time Used</th>
					<th width="100">RFT</th>
					<th width="100">Remark</th>
					<th width="100">Machine Utilization</th>
					<th>Lot No</th>
				</tr>
			</thead>
	</table>
	<div style=" max-height:350px; width:1505px; overflow-y:scroll;" id="scroll_body">
	<table class="rpt_table" id="table_body2" width="1485" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
	<tbody>
					<?php 
					$p=1;
					$f=0;
					$sub_btq=0;
					$batch_chk_arr=array();
					$ltb_btb_sub=array(1=>'BTB',2=>'LTB');
					foreach($sql_subcon_data_btb as $batch)
					{ 
					if ($p%2==0)  
					$sub_bgcolor_btb="#E9F3FF";
					else
					$sub_bgcolor_btb="#FFFFFF";
					$order_id=$batch[csf('po_id')];
					$color_id=$batch[csf('color_id')];
					if($batch[csf('result')]==1)
					{
					$shade="Shade Matched";	
					}
					else
					{
					$shade="";		
					}
					$desc=explode(",",$batch[csf('item_description')]); 
					$po_number=implode(",",array_unique(explode(",",$batch[csf('po_number')]))); 
					?>
				<tr bgcolor="<?php echo $sub_bgcolor_btb; ?>" id="tr_<?php echo $p; ?>" onclick="change_color('trsub_<?php echo $p; ?>','<?php echo $sub_bgcolor_btb; ?>')" style="cursor:pointer;">
					<td width="30"><?php echo $p; ?></td>
					<td width="80" title="<?php echo change_date_format($batch[csf('process_end_date')]); ?>"><p><?php echo change_date_format($batch[csf('process_end_date')]); $unload_date=$batch[csf('process_end_date')]; ?></p></td>
					<td  align="center" width="80" title="<?php echo $machine_arr[$batch[csf('machine_id')]]; ?>"><p><?php echo $machine_arr[$batch[csf('machine_id')]]; ?></p></td>
					<td  width="100" title="<?php echo $buyer_arr[$batch[csf('buyer_name')]]; ?>"><p><?php echo $buyer_arr[$batch[csf('buyer_name')]]; ?></p></td>
					<td  width="80" title="<?php echo $batch[csf('job_no_prefix_num')]; ?>"><p><?php echo $batch[csf('job_no_prefix_num')]; ?></p></td>
					<td width="90" title="<?php echo $po_number; ?>"><p><?php echo $po_number; ?></p></td>
					<td  width="100" title="<?php echo $desc[0]; ?>"><p><?php echo $desc[0]; ?></p></td>
					<td  width="80" title="<?php echo $fabric_typee[$batch[csf('width_dia_type')]]; ?>"><p><?php echo $fabric_typee[$batch[csf('width_dia_type')]]; ?></p></td>
					<td  width="80" title="<?php echo $color_library[$batch[csf('color_id')]]; ?>"><p><?php echo $color_library[$batch[csf('color_id')]]; ?></p></td>
					<td  align="center" width="90" title="<?php echo $batch[csf('batch_no')]; ?>"><p><?php echo $batch[csf('batch_no')]; ?></p></td>
					<td  align="right" width="70" title="<?php echo $batch[csf('sub_batch_qnty')]; ?>"><p><?php echo $batch[csf('sub_batch_qnty')]; ?></p></td>
					<td align="center" width="45" title="<?php echo $ltb_btb_sub[$batch[csf('ltb_btb_id')]]; ?>"><?php echo $ltb_btb_sub[$batch[csf('ltb_btb_id')]]; ?></td>
					<td width="100" align="center" title="<?php //echo $load_hr[$batch[csf('id')]].':'.$load_min[$batch[csf('id')]]; ?>"><p><?php   echo  change_date_format($batch[csf('process_end_date')]) .' <br> '.$batch[csf('end_hours')].':'.$batch[csf('end_minutes')]; ?></p></td>
					  <td align="center" width="60"><?php $hr=$batch[csf('end_hours')]-$load_hr[$batch[csf('id')]]; $min=$batch[csf('end_minutes')]-$load_min[$batch[csf('id')]]; //echo  $hr.':'.$min;
					$load_t=$subcon_load_hr[$batch[csf('id')]].':'.$subcon_load_min[$batch[csf('id')]];
					$unload_t=$batch[csf('end_hours')].':'.$batch[csf('end_minutes')];
					$new_date_time_unload=($unload_date.' '.$unload_t.':'.'00');
					$new_date_time_load=($subcon_load_date[$batch[csf('id')]].' '.$load_t.':'.'00');
				   //$unload_time=strtotime($unload_date,$unload_time);
					//$load_time=strtotime($load_date,$load_t);
					$total_time=datediff(n,$new_date_time_load,$new_date_time_unload);
					echo floor($total_time/60).":".$total_time%60;
				//echo ($total_time/60 - $total_time%3600/60)/60 .':'.$total_time%3600/60;
						?></td>
					<td width="100" title="<?php echo $shade;  ?>"><p><?php  echo $shade; ?></p></td>
					<td width="100" title="<?php ?>"><p><?php echo  $batch[csf('remarks')]; ?></p></td>
				  
					<td align="center" width="100" title="<?php  $utiliz=$batch[csf('batch_qnty')]/$m_capacity[$batch[csf('machine_id')]]*100; echo number_format($utiliz,2).'%'; ?>"><?php  $utiliz=$batch[csf('batch_qnty')]/$m_capacity[$batch[csf('machine_id')]]*100; echo number_format($utiliz,2).'%'; ?> </td>
					<td align="left" title="<?php echo  $yarn_lot_arr[$batch[csf('prod_id')]][$batch[csf('po_id')]];   ?>"><p><?php echo  $yarn_lot_arr[$batch[csf('prod_id')]][$batch[csf('po_id')]]; ?></p> </td>
				</tr>
	<?php 
	$p++;
	$sub_btq+=$batch[csf('sub_batch_qnty')];
	} //batchdata froeach
	 ?>
			  <tr bgcolor="#CCCCCC">
				<td colspan="10" align="right"><Strong>BTB Total:</Strong> <?php //echo $b_qty; ?> </td>
				<td align="right"><?php echo number_format($sub_btq,2); ?>&nbsp;</td>
				<td colspan="7">&nbsp;</td>
			 </tr>
			 <tr bgcolor="#C2DCFF">
				<td colspan="18" align="center"><strong>LTB</strong></td>
			 </tr>
		 <?php
		 if (count($sql_subcon_data_ltb)>0)
		 {
				$h=1;
				$sub_total_ltb=0;
				
				foreach($sql_subcon_data_ltb as $batch_ltb)
				{ 
					if ($h%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
					$order_id=$batch_ltb[csf('po_id')];
					$color_id=$batch_ltb[csf('color_id')];
					if($batch_ltb[csf('result')]==1)
					{
						$shade="Shade Matched";	
					}
					else
					{
						$shade="";	
					}
					$desc=explode(",",$batch_ltb[csf('item_description')]); 
					$po_number=implode(",",array_unique(explode(",",$batch_ltb[csf('po_number')]))); 
					?>
				<tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $h; ?>" onClick="js_set_value(<?php echo $h; ?>)" style="cursor:pointer;">
					<td width="30"><?php echo $h; ?></td>
					<td width="80" title="<?php echo change_date_format($batch_ltb[csf('process_end_date')]); ?>"><p><?php echo change_date_format($batch_ltb[csf('process_end_date')]);$unload_date1=$batch_ltb[csf('process_end_date')]; ?></p></td>
					<td  align="center" width="80" title="<?php echo $machine_arr[$batch_ltb[csf('machine_id')]]; ?>"><p><?php echo $machine_arr[$batch_ltb[csf('machine_id')]]; ?></p></td>
					<td  width="100" title="<?php echo $buyer_arr[$batch_ltb[csf('buyer_name')]]; ?>"><p><?php echo $buyer_arr[$batch_ltb[csf('buyer_name')]]; ?></p></td>
					<td  width="80" title="<?php echo $batch[csf('job_no_prefix_num')]; ?>"><p><?php echo $batch[csf('job_no_prefix_num')]; ?></p></td>
					<td width="90" title="<?php echo $po_number; ?>"><p><?php echo $po_number; ?></p></td>
					<td  width="100" title="<?php echo $desc[0]; ?>"><p><?php echo $desc[0]; ?></p></td>
					<td  width="80" title="<?php echo $fabric_typee[$batch[csf('width_dia_type')]]; ?>"><p><?php echo $fabric_typee[$batch[csf('width_dia_type')]]; ?></p></td>
					<td  width="80" title="<?php echo $color_library[$batch_ltb[csf('color_id')]]; ?>"><p><?php echo $color_library[$batch_ltb[csf('color_id')]]; ?></p></td>
					<td  align="center" width="90" title="<?php echo $batch_ltb[csf('batch_no')]; ?>"><p><?php echo $batch_ltb[csf('batch_no')]; ?></p></td>
					<td  align="right" width="70" title="<?php echo $batch_ltb[csf('sub_batch_qnty')]; ?>"><p><?php echo $batch_ltb[csf('sub_batch_qnty')]; ?></p></td>
					<td align="center" width="45" title="<?php echo $ltb_btb_sub[$batch_ltb[csf('ltb_btb_id')]]; ?>"><?php echo $ltb_btb_sub[$batch_ltb[csf('ltb_btb_id')]]; ?></td>
					<td width="100" align="center" title="<?php //echo $load_hr[$batch[csf('id')]].':'.$load_min[$batch[csf('id')]]; ?>"><p><?php   echo  change_date_format($batch_ltb[csf('process_end_date')]) .' <br>'.$batch_ltb[csf('end_hours')].':'.$batch_ltb[csf('end_minutes')]; ?></p></td>
					   <td align="center" width="60" title="<?php echo  $hr.':'.$min;  ?>"><?php $hr=$batch_ltb[csf('end_hours')]-$load_hr[$batch_ltb[csf('id')]]; $min=$batch_ltb[csf('end_minutes')]-$load_min[$batch_ltb[csf('id')]]; //echo  $hr.':'.$min; 
					$load_time=$subcon_load_hr[$batch_ltb[csf('id')]].':'.$subcon_load_min[$batch_ltb[csf('id')]];
					$unload_time=$batch_ltb[csf('end_hours')].':'.$batch_ltb[csf('end_minutes')];
					$new_date_time_unload1=($unload_date1.' '.$unload_time.':'.'00');
					$new_date_time_load1=($subcon_load_date[$batch_ltb[csf('id')]].' '.$load_time.':'.'00');
					$total_time1=datediff(n,$new_date_time_load1,$new_date_time_unload1);
					echo floor($total_time1/60).":".$total_time1%60;
			//echo ($total_time/60 - $total_time%3600/60)/60 .':'.$total_time%3600/60;
						?></td>
					<td width="100" title="<?php echo $shade ?>"><p><?php  echo $shade; ?></p></td>
					<td width="100" title="<?php ?>"><p><?php echo  $batch_ltb[csf('remarks')];  ?></p></td>
					<td align="center" width="100" title="<?php //echo $hr.':'.$min;   ?>"><?php  $utiliz=$batch_ltb[csf('batch_qnty')]/$m_capacity[$batch_ltb[csf('machine_id')]]*100; 		
					echo number_format($utiliz,2).'%'; ?> </td>
					<td align="left" title="<?php echo  $yarn_lot_arr[$batch_ltb[csf('prod_id')]][$batch_ltb[csf('po_id')]];   ?>"><p><?php echo  $yarn_lot_arr[$batch_ltb[csf('prod_id')]][$batch_ltb[csf('po_id')]]; ?></p> </td>
				</tr>
						<?php			
				$h++;
				$sub_total_ltb+=$batch_ltb[csf('sub_batch_qnty')];
				}
				?>
				<table class="rpt_table" width="1485" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
			<tfoot>
				<tr>
					<th width="30">&nbsp;</th>
					<th width="80">&nbsp;</th>
					<th width="80">&nbsp;</th>
					<th width="100">&nbsp;</th>
					<th width="80">&nbsp;</th>
					<th width="90">&nbsp;</th>
					<th width="100">&nbsp;</th>
					<th width="80">&nbsp;</th>
					<th width="80">&nbsp;</th>
					<th width="90">&nbsp;</th>
					<th width="70"><?php echo number_format($sub_total_ltb,2); ?></th>
					<th width="45">&nbsp;</th>
					<th width="100">&nbsp;</th>
					<th width="60">&nbsp;</th>
					<th width="100">&nbsp;</th>
					<th width="100">&nbsp;</th>
					<th width="100">&nbsp;</th>
					<th>&nbsp;</th>
				</tr>
			</tfoot>
	</table>
				<?php
			 }
		 ?>    
	</tbody>
	</table>
	</div>
   
	</fieldset>
	</div>
	<?php
		}
	}
	else if($cbo_type==4) // Dyeing Reprocess Summary
	{
	?>
	<div>
	<fieldset style="width:1250px;">
	<div align="center"><strong> <?php echo $company_library[$company]; ?> </strong><br> <strong> Daily Dyeing Reprocess Summary </strong><br>
	<?php
		echo  change_date_format($date_from).' '.To.' '.change_date_format($date_to);
		?>
	 </div>
	 <table class="rpt_table" width="1220" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
			<thead>
				<tr>
					<th width="30">SL</th>
					<th width="80">Date</th>
					<th width="80">M/C No</th>
					<th width="100">Buyer</th>
					<th width="90">PO No</th>
					<th width="100">Febrics Type</th>
					<th width="80">Color Name</th>
					<th width="90">Batch No</th>
					<th width="40">Extn. No</th>
					<th width="70">Repro. Qty.</th>
					<th width="80">Lot No</th>
					<th width="90">Load Date & Time</th>
					<th width="90">UnLoad Date Time</th>
					<th width="60">Time Used</th>
					<th>Result</th>
				</tr>
			</thead>
	</table>
	<div style=" max-height:350px; width:1250px; overflow-y:scroll;" id="scroll_body">
	<table class="rpt_table" id="table_body" width="1220" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
	<tbody>
	<?php 
	$sl=1;
	$i=1;
	$f=0;
	$btq=0;
	$buyer_arr = return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
	$batch_chk_arr=array();
	foreach($batchdata as $batch)
	{ 
	if ($i%2==0)  
	$bgcolor="#E9F3FF";
	else
	$bgcolor="#FFFFFF";
	$order_id=$batch[csf('po_id')];
	$color_id=$batch[csf('color_id')];
	$desc=explode(",",$batch[csf('item_description')]); 
	$po_number=implode(",",array_unique(explode(",",$batch[csf('po_number')]))); 
	?>
				<tr bgcolor="<?php echo $bgcolor; ?>" id="tr_<?php echo $i; ?>" onClick="js_set_value(<?php echo $i; ?>)" style="cursor:pointer;">
					<td width="30"><?php echo $i; ?></td>
					<td width="80" title="<?php echo change_date_format($batch[csf('process_end_date')]); ?>"><p><?php echo change_date_format($batch[csf('process_end_date')]); $unload_date=$batch[csf('process_end_date')]; ?></p></td>
					<td  align="center" width="80" title="<?php echo $machine_arr[$batch[csf('machine_id')]]; ?>"><p><?php echo $machine_arr[$batch[csf('machine_id')]]; ?></p></td>
					<td  width="100" title="<?php echo $buyer_arr[$batch[csf('buyer_name')]]; ?>"><p><?php echo $buyer_arr[$batch[csf('buyer_name')]]; ?></p></td>
					<td  width="90" title="<?php echo $po_number; ?>"><p><?php echo $po_number; ?></p></td>
					<td  width="100" title="<?php echo $desc[0]; ?>"><p><?php echo $desc[0]; ?></p></td>
					<td  width="80" title="<?php echo $color_library[$batch[csf('color_id')]]; ?>"><p><?php echo $color_library[$batch[csf('color_id')]]; ?></p></td>
					<td  align="center" width="90" title="<?php echo $batch[csf('batch_no')]; ?>"><p><?php echo $batch[csf('batch_no')]; ?></p></td>
					<td  align="center" width="40" title="<?php echo $batch[csf('extention_no')]; ?>"><p><?php echo $batch[csf('extention_no')]; ?></p></td>
					<td align="right" width="70" title="<?php echo $batch[csf('batch_qnty')];  ?>"><?php echo number_format($batch[csf('batch_qnty')],2);  ?></td>
					<td align="left" width="80" title="<?php echo $yarn_lot_arr[$batch[csf('prod_id')]][$batch[csf('po_id')]]; ?>"><p><?php echo $yarn_lot_arr[$batch[csf('prod_id')]][$batch[csf('po_id')]];  ?></p></td>
					<td width="90" title="<?php  $load_t=$load_hr[$batch[csf('id')]].':'.$load_min[$batch[csf('id')]]; echo change_date_format($load_date[$batch[csf('id')]]).'  & '.$load_t; ?>"><p><?php   $load_t=$load_hr[$batch[csf('id')]].':'.$load_min[$batch[csf('id')]]; echo change_date_format($load_date[$batch[csf('id')]]).'  & '.$load_t;
					 ?></p></td>
					<td width="90" title="<?php $hr=strtotime($unload_date,$load_t); $min=($batch[csf('end_minutes')])-($load_min[$batch[csf('id')]]); echo  $unloadd=change_date_format($batch[csf('process_end_date')]).'<br>'.$unload_time=$batch[csf('end_hours')].':'.$batch[csf('end_minutes')]; ?>"><p><?php $hr=strtotime($unload_date,$load_t); $min=($batch[csf('end_minutes')])-($load_min[$batch[csf('id')]]); echo  $unloadd=change_date_format($batch[csf('process_end_date')]).'<br>'.$unload_time=$batch[csf('end_hours')].':'.$batch[csf('end_minutes')];?></p></td>
					<td align="center" width="60">
				   <?php     
				$new_date_time_unload=($unload_date.' '.$unload_time.':'.'00');
				$new_date_time_load=($load_date[$batch[csf('id')]].' '.$load_t.':'.'00');
				$total_time=datediff(n,$new_date_time_load,$new_date_time_unload);
				echo floor($total_time/60).":".$total_time%60;
				//echo ($total_time/60 - $total_time%3600/60)/60 .':'.$total_time%3600/60;
						?>
					 </td>
					<td align="center" title="<?php  echo $dyeing_result[$batch[csf('result')]];   ?>"><p><?php echo $dyeing_result[$batch[csf('result')]]; ?></p> </td>
				</tr>
	<?php 
	$i++;
	$btq+=$batch[csf('batch_qnty')];
	} //batchdata froeach
	 ?>
		</tbody>
	</table>
	<table class="rpt_table" width="1220" cellpadding="0" cellspacing="0" border="1" rules="all" align="center">
			<tfoot>
				<tr>
					<th width="30">&nbsp;</th>
					<th width="80">&nbsp;</th>
					<th width="80">&nbsp;</th>
					<th width="100">&nbsp;</th>
					<th width="90">&nbsp;</th>
					<th width="100">&nbsp;</th>
					<th width="80">&nbsp;</th>
					<th width="90">&nbsp;</th>
					<th width="40">&nbsp;</th>
					<th width="70"><?php echo number_format($btq,2); ?></th>
					<th width="80">&nbsp;</th>
					<th width="90">&nbsp;</th>
					<th width="90">&nbsp;</th>
					<th width="60">&nbsp;</th>
					<th>&nbsp;</th>
				</tr>
			</tfoot>
	</table>
	</div>
	</fieldset>
	</div>
		<?php }
	exit();
}//Dyeing Report end
?>