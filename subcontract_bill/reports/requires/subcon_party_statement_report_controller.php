<?php 
include('../../../includes/common.php');
session_start();
extract($_REQUEST);
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$date=date('Y-m-d');

$company_arr=return_library_array( "select id, company_name from lib_company", "id", "company_name");
$supplier_arr=return_library_array( "select id, supplier_name from lib_supplier", "id", "supplier_name");
$store_arr=return_library_array( "select id, store_name from lib_store_location", "id", "store_name");
$buyer_arr=return_library_array( "select id, buyer_name from lib_buyer", "id", "buyer_name");
$order_arr=return_library_array( "select id, order_no from  subcon_ord_dtls", "id", "order_no");
$color_arr=return_library_array( "select id, color_name from lib_color", "id", "color_name");
$batch_arr=return_library_array( "select id, batch_no from pro_batch_create_mst", "id", "batch_no");

if ($action=="load_drop_down_buyer")
{ 
	echo create_drop_down( "cbo_party_id", 140, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' $buyer_cond  and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (2,3)) order by buyer_name","id,buyer_name", 1, "--Select Party--", $selected, "" );
	exit();   	 
}

if($action=="report_generate")
{ 
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process ));

	$date_from=str_replace("'","",$txt_date_from);
	$date_to=str_replace("'","",$txt_date_to);
	
	ob_start();

	if($type==1)
	{
		?>
        <div align="center">
         <fieldset style="width:950px;">
            <table cellpadding="0" cellspacing="0" width="930">
                <tr  class="form_caption" style="border:none;">
                   <td align="center" width="100%" colspan="10" style="font-size:20px"><strong><?php echo 'Party Ledger'; ?></strong></td>
                </tr>
                <tr class="form_caption" style="border:none;">
                   <td align="center" width="100%" colspan="10" style="font-size:16px"><strong><?php echo $company_arr[str_replace("'","",$cbo_company_id)]; ?></strong></td>
                </tr>
                <tr class="form_caption" style="border:none;">
                    <td align="center" width="100%" colspan="10" style="font-size:12px">
                        <?php if(str_replace("'","",$txt_date_from)!="" && str_replace("'","",$txt_date_to)!="") echo "From ".change_date_format(str_replace("'","",$txt_date_from),'dd-mm-yyyy')." To ".change_date_format(str_replace("'","",$txt_date_to),'dd-mm-yyyy')."" ;?>
                    </td>
                </tr>
            </table>
            <table width="927" border="1" cellpadding="0" cellspacing="0" rules="all" class="rpt_table">
                <thead>
                    <th width="30">SL</th>
                    <th width="110">Trans. Ref.</th>
                    <th width="110">Invoice No.</th>
                    <th width="70">Trans. Date</th>
                    <th width="80">Bill Type</th>                            
                    <th width="100">Narration</th>
                    <th width="100">Inv. Amount (Debit)</th>
                    <th width="100">Receive (Credit)</th>
                    <th width="100">Non-Cash /LC (Credit)</th>
                    <th>Balance</th>
                </thead>
            </table>
        <div style="max-height:300px; overflow-y:scroll; width:930px" id="scroll_body">
            <table width="910" border="1" class="rpt_table" rules="all" id="table_body">
            <?php
				 $bill_num_arr=return_library_array( "select bill_no, prefix_no_num from subcon_inbound_bill_mst", "bill_no", "prefix_no_num");
                if(str_replace("'","",$cbo_party_id)==0) $party_cond=""; else  $party_cond=" and a.party_name=$cbo_party_id";
                if(str_replace("'","",$cbo_party_id)==0) $party_bill_cond=""; else  $party_bill_cond=" and a.party_id=$cbo_party_id";
    
                if(str_replace("'","",$cbo_bill_type)==0) $bill_type_cond=""; else  $bill_type_cond=" and b.bill_type=$cbo_bill_type";
                if(str_replace("'","",$cbo_bill_type)==0) $process_cond=""; else  $process_cond=" and a.process_id=$cbo_bill_type";
                
                if($db_type==0)
                {
                    if( $date_from==0 && $date_to==0 ) $receipt_date_cond=""; else $receipt_date_cond= " and a.receipt_date between '".change_date_format($date_from,'yyyy-mm-dd')."' and '".change_date_format($date_to,'yyyy-mm-dd')."'";
                    if( $date_from==0 && $date_to==0 ) $bill_date_cond=""; else $bill_date_cond= " and a.bill_date between '".change_date_format($date_from,'yyyy-mm-dd')."' and '".change_date_format($date_to,'yyyy-mm-dd')."'";
                }
                else if($db_type==2)
                {
                    if( $date_from==0 && $date_to==0 ) $receipt_date_cond=""; else $receipt_date_cond= " and a.receipt_date between '".change_date_format($date_from,'','',1)."' and '".change_date_format($date_to,'','',1)."'";
                    if( $date_from=='' && $date_to=='' ) $bill_date_cond=""; else $bill_date_cond= " and a.bill_date between '".change_date_format($date_from,'','',1)."' and '".change_date_format($date_to,'','',1)."'";
                }
                
                if($date_from=="") $bill_date=""; else $bill_date= " and a.bill_date <".$txt_date_from."";
                if($date_from=="") $receipt_date=""; else $receipt_date= " and a.receipt_date <".$txt_date_from."";
                
                $opening_balance_arr=array();
                $ope_bal=sql_select("select a.party_id, sum(b.amount) as amount from subcon_inbound_bill_mst a, subcon_inbound_bill_dtls b where a.company_id=$cbo_company_id and a.id=b.mst_id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 $party_bill_cond $bill_date $process_cond group by a.party_id order by a.party_id");
                foreach ($ope_bal as $row)
                {
                    $opening_balance_arr[$row[csf("party_id")]]=$row[csf("amount")];
                }
                
                $pre_add_array=array();
                $sql_adj=sql_select("select a.party_name, sum(b.total_adjusted) as pre_adjusted from subcon_payment_receive_mst a,subcon_payment_receive_dtls b where a.id=b.master_id and a.company_id=$cbo_company_id and a.status_active=1 and a.is_deleted=0 $party_cond $receipt_date $bill_type_cond group by a.party_name order by a.party_name");
               //echo "select a.party_name, sum(b.total_adjusted) as pre_adjusted from subcon_payment_receive_mst a,subcon_payment_receive_dtls b where a.id=b.master_id and a.company_id=$cbo_company_id and a.status_active=1 and a.is_deleted=0 $party_cond $receipt_date $bill_type_cond group by a.party_name";
    			foreach ($sql_adj as $row)
                {
                    $pre_add_array[$row[csf("party_name")]]=$row[csf("pre_adjusted")];
                }
    			// $sql_adj[0][csf('pre_adjusted')];
				$pay_recv_array=array();	
				$sql_dtls="select a.receive_no, a.party_name, a.receipt_date, a.instrument_id, a.bank_name, a.instrument_date, a.instrument_no, a.adjustment_type, b.bill_type, b.bill_no,  
                sum(b.total_adjusted) as rec_amount
                from subcon_payment_receive_mst a, subcon_payment_receive_dtls b where a.id=b.master_id and a.company_id=$cbo_company_id  and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 $party_cond $receipt_date_cond $bill_type_cond group by a.party_name, b.bill_no, a.receive_no, a.receipt_date, a.instrument_id, a.bank_name, a.instrument_date, a.instrument_no, a.adjustment_type, b.bill_type order by a.party_name, b.bill_no";
				$sql_dtls_result=sql_select($sql_dtls);
				foreach ($sql_dtls_result as $row)
				{
					$instrument_id=$row[csf('instrument_id')];
					 if ($instrument_id==1 || $instrument_id==2 || $instrument_id==3)
					 {
						 if ($row[csf('instrument_date')]=='' || $row[csf('instrument_date')]=="0000-00-00") $instrument_date=''; else $instrument_date=change_date_format($row[csf('instrument_date')]);
						 $narration=$row[csf('instrument_no')].'<br>'.$instrument_date.'<br>'.$row[csf('bank_name')];
						 $rec_amount=$row[csf('rec_amount')];
						 $lc_noncash='-';
					 }
					 else  if ($instrument_id==4 || $instrument_id==5)
					 {
						$narration=$adjustment_type[$row[csf('adjustment_type')]];
						$rec_amount='-';
						$lc_noncash=$row[csf('rec_amount')];
					 }
					$pay_recv_array[$row[csf("party_name")]].=$row[csf('bill_no')]."_".$row[csf('receive_no')]."_".$row[csf('receipt_date')]."_".$row[csf('bill_type')]."_".$narration."_".$rec_amount."_".$lc_noncash.",";
					
				}
				//echo $trans_data;
             // var_dump($pay_recv_array);die;
			   if($db_type==0)
			   {
				   $sql_bill="select a.bill_no, a.prefix_no_num, a.bill_date, a.party_id, a.bill_for, a.process_id, group_concat(b.order_id) as order_id, sum(b.amount) as amount, sum(b.delivery_qty) as delivery_qty from subcon_inbound_bill_mst a, subcon_inbound_bill_dtls b where a.company_id=$cbo_company_id and a.id=b.mst_id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 $party_bill_cond $bill_date_cond $process_cond group by a.bill_no, a.prefix_no_num, a.bill_date, a.party_id, a.bill_for, a.process_id order by a.party_id, a.bill_no";
			   }
			   else if($db_type==2)
			   {
				   $sql_bill="select a.bill_no, a.prefix_no_num, a.bill_date, a.party_id, a.bill_for, a.process_id, listagg(CAST(b.order_id as varchar2(4000)),',') within group (order by b.order_id) as order_id, sum(b.amount) as amount, sum(b.delivery_qty) as delivery_qty from subcon_inbound_bill_mst a, subcon_inbound_bill_dtls b where a.company_id=$cbo_company_id and a.id=b.mst_id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 $party_bill_cond $bill_date_cond $process_cond group by a.party_id, a.bill_no, a.prefix_no_num, a.bill_date, a.bill_for, a.process_id order by a.party_id,a.bill_no";
			   }
                
                $sql_bill_result=sql_select($sql_bill);
                $i=1; $k=1; $party_array=array(); $pay_rec_val_array=array(); 
                foreach ($sql_bill_result as $row)
                {
                    if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
                    $opening_bal=$opening_balance_arr[$row[csf("party_id")]]-$pre_add_array[$row[csf("party_id")]];
					
                    if (!in_array($row[csf("party_id")],$party_array) )
                    {
                        if($k!=1)
                        { 	
							$dataArray=array_filter(explode(",",substr($pay_recv_array[$party],0,-1)));

							foreach($dataArray as $key=>$val2)
							{ 
								if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
								
								$value=explode("_",$val2);
								$bill_no=$value[0];
								$recv_no=$value[1];
								$receipt_date=$value[2];
								$bill_type=$value[3];
								$narration=$value[4];
								$rec_amount=$value[5];
								$lc_noncash=$value[6];
								?>
                                <tr bgcolor="<?php echo $bgcolor;  ?>" onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
                                     <td width="30" ><?php echo $i; ?></td>
                                     <td width="110" ><?php echo $recv_no; ?></td>
                                     <td width="110" align="center" ><?php echo $bill_num_arr[$bill_no]; ?></td>
                                     <td width="70" ><?php echo change_date_format($receipt_date); ?></td>
                                     <td width="80" ><?php echo $production_process[$bill_type]; ?></td>
                                     <td width="100" align="center" ><p><?php echo $narration; ?></p></td>
                                     <td width="100" align="right" ><?php //echo ?></td>
                                     <td width="100" align="right" ><?php echo number_format($rec_amount,2,'.',''); ?></td>
                                     <td width="100" align="right" ><?php echo number_format($lc_noncash,2,'.',''); ?></td>
                                     <td align="right"><?php $bal_row=$bal_row-($rec_amount+$lc_noncash); echo number_format($bal_row,2,'.',''); ?></td>
                                </tr>
                                <?php
								$tot_rec_amount+=$rec_amount;
								$tot_lc_noncash+=$lc_noncash;
								
								$grand_rec_amount+=$rec_amount;
								$grand_lc_noncash+=$lc_noncash;
								$grand_tot_balance=$bal_row;

                                $i++;
							}
                        ?>
                            <tr class="tbl_bottom">
                                <td colspan="6" align="right"><b>Party Total:</b></td>
                                <td align="right"><b><?php echo number_format($inv_amount,2); ?></b></td>
                                <td align="right"><b><?php echo number_format($tot_rec_amount,2); ?></b></td>
                                <td align="right"><b><?php echo number_format($tot_lc_noncash,2); ?></b></td>
                                <td align="right"><b><?php echo number_format($bal_row,2); $grand_balance1=$bal_row; ?></b></td>
                            </tr>
                        <?php
							
                            unset($inv_amount);
							unset($tot_rec_amount);
							unset($tot_lc_noncash);
                        }
                        ?>
                            <tr bgcolor="#dddddd">
                                <td colspan="10" align="left" ><b>Party Name: <?php echo $buyer_arr[$row[csf("party_id")]]; ?></b></td>
                            </tr>
                            <tr bgcolor="#999999">
                                <td colspan="5" align="left" ><b>Opening Balance:</b></td><td colspan="5" align="right" ><b> <?php echo number_format($opening_bal,2); ?></b></td>
                            </tr>
                        <?php
                        //unset($opening_balance_arr[$row[csf("party_id")]]);
                        //$balance_tot=$opening_balance_arr[$row[csf("party_id")]];
                        $bal_row=$opening_bal;


                        $party_array[]=$row[csf('party_id')];            
                        $k++;
                    }

                    $bal_row=$bal_row+$row[csf('amount')];
                    $inv_amount+=$row[csf('amount')];
                    $grand_inv_amount+=$row[csf('amount')];
                    $party=$row[csf('party_id')];
					$order_no='';
					$order_id=array_unique(explode(',',$row[csf('order_id')]));
					foreach ($order_id as $val)
					{
						if($order_no=="") $order_no= $order_arr[$val]; else $order_no.=', '.$order_arr[$val];
					}
                    
                    ?>
                    <tr bgcolor="<?php echo $bgcolor;  ?>" onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
                         <td width="30" ><?php echo $i; ?></td>
                         <td width="110" ><?php echo $row[csf('bill_no')]; ?></td>
                         <td width="110" align="center" ><?php echo $row[csf('prefix_no_num')]; ?></td>
                         <td width="70" ><?php echo change_date_format($row[csf('bill_date')]); ?></td>
                         <td width="80" ><?php echo $production_process[$row[csf('process_id')]]; ?></td>
                         <td width="100" align="center" ><p><?php echo $order_no; ?></p></td>
                         <td width="100" align="right" ><?php echo number_format($row[csf('amount')],2,'.',''); ?></td>
                         <td width="100" align="center" ><?php echo '-'; ?></td>
                         <td width="100" align="center" ><?php echo '-'; ?></td>
                         <td align="right"><?php echo number_format($bal_row,2,'.',''); ?></td>
                    </tr>
                    <?php	
                    $i++;
					$grand_tot_balance=$bal_row;
					$grand_balance2=$bal_row;
                }
                $bill_num_arr=return_library_array( "select bill_no, prefix_no_num from subcon_inbound_bill_mst", "bill_no", "prefix_no_num");
				
				$grand_balance+=(($opening_bal+$grand_inv_amount)-$grand_rec_amount)-$grand_lc_noncash;
				
				$dataArray=array_filter(explode(",",substr($pay_recv_array[$row[csf('party_id')]],0,-1)));
							
				foreach($dataArray as $key=>$val2)
				{
					if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
					//echo $val2.'<br>'; die;
					$value=explode("_",$val2);
					$bill_no=$value[0];
					$recv_no=$value[1];
					$receipt_date=$value[2];
					$bill_type=$value[3];
					$narration=$value[4];
					$rec_amount=$value[5];
					$lc_noncash=$value[6];
					?>
					<tr bgcolor="<?php echo $bgcolor;  ?>" onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
						 <td width="30" ><?php echo $i; ?></td>
						 <td width="110" ><?php echo $recv_no; ?></td>
						 <td width="110" align="center" ><?php echo $bill_num_arr[$bill_no]; ?></td>
						 <td width="70" ><?php echo change_date_format($receipt_date); ?></td>
						 <td width="80" ><?php echo $production_process[$bill_type]; ?></td>
						 <td width="100" align="center" ><p><?php echo $narration; ?></p></td>
						 <td width="100" align="right" >&nbsp;</td>
						 <td width="100" align="right" ><?php echo number_format($rec_amount,2,'.',''); ?></td>
						 <td width="100" align="right" ><?php echo number_format($lc_noncash,2,'.',''); ?></td>
						 <td align="right"><?php $bal_row=$bal_row-($rec_amount+$lc_noncash); echo number_format($bal_row,2,'.',''); ?></td>
					</tr>
					<?php
					$tot_rec_amount+=$rec_amount;
					$tot_lc_noncash+=$lc_noncash;
					$grand_rec_amount+=$rec_amount;
					$grand_lc_noncash+=$lc_noncash;
					$i++;
					$grand_tot_balance=$bal_row;
					$grand_balance3=$bal_row;
				}
				//$grand_balance+=$grand_tot_balance;
				$grand_balance=$grand_balance1+$grand_balance3+$bal_row;
                ?>
                <tr class="tbl_bottom">
                    <td colspan="6" align="right"><b>Party Total:</b></td>
                    <td align="right"><b><?php echo number_format($inv_amount,2); ?></b></td>
                    <td align="right"><b><?php echo number_format($tot_rec_amount,2); ?></b></td>
                    <td align="right"><b><?php echo number_format($tot_lc_noncash,2); ?></b></td>
                    <td align="right"><b><?php echo number_format($bal_row,2); ?></b></td>
                </tr>
                <?php
				if(str_replace("'","",$cbo_party_id)==0)
				{
				?>
                <tr class="tbl_bottom">
                    <td colspan="6" align="right"><b>Grand Total:</b></td>
                    <td align="right"><b><?php echo number_format($grand_inv_amount,2); ?></b></td>
                    <td align="right"><b><?php echo number_format($grand_rec_amount,2); ?></b></td>
                    <td align="right"><b><?php echo number_format($grand_lc_noncash,2); ?></b></td>
                    <td align="right"><b><?php echo number_format($grand_balance,2); ?></b></td>
                </tr>
                <?php } ?>
            </table>
            </div>
            </fieldset>
            </div>
        <?php
	}
	else if($type==2)
	{
	?>
        <div align="center">
         <fieldset style="width:950px;">
            <table cellpadding="0" cellspacing="0" width="930">
                <tr  class="form_caption" style="border:none;">
                   <td align="center" width="100%" colspan="8" style="font-size:20px"><strong><?php echo 'Receivable Statement'; ?></strong></td>
                </tr>
                <tr class="form_caption" style="border:none;">
                   <td align="center" width="100%" colspan="8" style="font-size:16px"><strong><?php echo $company_arr[str_replace("'","",$cbo_company_id)]; ?></strong></td>
                </tr>
                <tr class="form_caption" style="border:none;">
                    <td align="center" width="100%" colspan="8" style="font-size:12px">
                        <?php if(str_replace("'","",$txt_date_to)!="") echo " As On ".change_date_format(str_replace("'","",$txt_date_to),'dd-mm-yyyy')."" ;?>
                    </td>
                </tr>
            </table>
            <table width="927" border="1" cellpadding="0" cellspacing="0" rules="all" class="rpt_table">
                <thead>
                    <th width="40">SL</th>
                    <th width="160">Party</th>
                    <th width="150">Bill Type</th>
                    <th width="120">Inv. Amount (Debit)</th>
                    <th width="120">Receive (Credit)</th>
                    <th width="120">Non-Cash /LC (Credit)</th>
                    <th>Balance</th>
                </thead>
            </table>
        <div style="max-height:300px; overflow-y:scroll; width:930px" id="scroll_body">
            <table width="910" border="1" class="rpt_table" rules="all" id="table_body">
            <?php	
				if(str_replace("'","",$cbo_party_id)==0) $party_cond=""; else  $party_cond=" and a.party_name=$cbo_party_id";
                if(str_replace("'","",$cbo_party_id)==0) $party_bill_cond=""; else  $party_bill_cond=" and a.party_id=$cbo_party_id";
    
                if(str_replace("'","",$cbo_bill_type)==0) $bill_type_cond=""; else  $bill_type_cond=" and b.bill_type=$cbo_bill_type";
                if(str_replace("'","",$cbo_bill_type)==0) $process_cond=""; else  $process_cond=" and a.process_id=$cbo_bill_type";
                
                if($db_type==0)
                {
                    if( $date_from==0 && $date_to==0 ) $receipt_date_cond=""; else $receipt_date_cond= " and a.receipt_date between '".change_date_format($date_from,'yyyy-mm-dd')."' and '".change_date_format($date_to,'yyyy-mm-dd')."'";
                    if( $date_from==0 && $date_to==0 ) $bill_date_cond=""; else $bill_date_cond= " and a.bill_date between '".change_date_format($date_from,'yyyy-mm-dd')."' and '".change_date_format($date_to,'yyyy-mm-dd')."'";
                }
                else if($db_type==2)
                {
                    if( $date_from==0 && $date_to==0 ) $receipt_date_cond=""; else $receipt_date_cond= " and a.receipt_date between '".change_date_format($date_from,'','',1)."' and '".change_date_format($date_to,'','',1)."'";
                    if( $date_from=='' && $date_to=='' ) $bill_date_cond=""; else $bill_date_cond= " and a.bill_date between '".change_date_format($date_from,'','',1)."' and '".change_date_format($date_to,'','',1)."'";
                }
                
                if($date_to=="") $bill_date=""; else $bill_date= " and a.bill_date <=".$txt_date_to."";
                if($date_to=="") $receipt_date=""; else $receipt_date= " and a.receipt_date <=".$txt_date_to."";			
				
				$adjusted_amount_array=array();
                $sql_adj=sql_select("select a.party_name, b.bill_type, sum(b.total_adjusted) as pre_adjusted from subcon_payment_receive_mst a,subcon_payment_receive_dtls b where a.id=b.master_id and a.company_id=$cbo_company_id and a.status_active=1 and a.is_deleted=0 $party_cond $receipt_date $bill_type_cond group by a.party_name, b.bill_type");
				//echo "select a.party_name, sum(b.total_adjusted) as pre_adjusted from subcon_payment_receive_mst a,subcon_payment_receive_dtls b where a.id=b.master_id and a.company_id=$cbo_company_id and a.status_active=1 and a.is_deleted=0 $party_cond $receipt_date $bill_type_cond group by a.party_name";
				foreach($sql_adj as $row)
				{
					$adjusted_amount_array[$row[csf('party_name')]][$row[csf('bill_type')]]=$row[csf('pre_adjusted')];
				}
				
				$pay_rec_array=array();
				
                $sql_rec="select a.party_name, b.bill_type,
				sum(case when a.instrument_id in (1,2,3) then b.total_adjusted else 0 end) as rec_amount,
				sum(case when a.instrument_id in (4,5) then b.total_adjusted else 0 end) as lc_amount
                from subcon_payment_receive_mst a, subcon_payment_receive_dtls b where a.id=b.master_id and a.company_id=$cbo_company_id  and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 $party_cond $receipt_date $bill_type_cond group by a.party_name, b.bill_type order by a.party_name";
				
				$sql_rec_result=sql_select($sql_rec);
				foreach($sql_rec_result as $row)
				{
					$pay_rec_array[$row[csf('party_name')]][$row[csf('bill_type')]]['rec_amount']=$row[csf('rec_amount')];
					$pay_rec_array[$row[csf('party_name')]][$row[csf('bill_type')]]['lc_amount']=$row[csf('lc_amount')];
					//$pay_rec_array[$row[csf('party_name')]]['instrument_id']=$row[csf('instrument_id')];
				}
				//var_dump($pay_rec_array);
				
				$opening_balance_arr=array();
                $ope_bal=sql_select("select a.party_id, a.process_id, sum(b.amount) as amount from subcon_inbound_bill_mst a, subcon_inbound_bill_dtls b where a.company_id=$cbo_company_id and a.id=b.mst_id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 $party_bill_cond $bill_date $process_cond group by a.party_id, a.process_id order by a.party_id");
                foreach ($ope_bal as $row)
                {
                    $opening_balance_arr[$row[csf("party_id")]][$row[csf("process_id")]]=$row[csf("amount")];
                }	
				
				$invoice_amount_arr=array();
                $inv_amt=sql_select("select a.party_id, a.process_id, sum(b.amount) as amount from subcon_inbound_bill_mst a, subcon_inbound_bill_dtls b where a.company_id=$cbo_company_id and a.id=b.mst_id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 $party_bill_cond $bill_date $process_cond group by a.party_id, a.process_id order by a.party_id");
                foreach ($inv_amt as $row)
                {
                    $invoice_amount_arr[$row[csf("party_id")]][$row[csf("process_id")]]=$row[csf("amount")];
                }
				//var_dump($invoice_amount_arr);			
			
				if($db_type==0)
				{
					$bill_dtls="select a.party_id, a.process_id from subcon_inbound_bill_mst a, subcon_inbound_bill_dtls b where a.company_id=$cbo_company_id and a.id=b.mst_id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 $party_bill_cond $bill_date $process_cond group by a.party_id, a.process_id order by a.party_id";
				}
				else if($db_type==2)
				{
					$bill_dtls="select a.party_id, a.process_id from subcon_inbound_bill_mst a, subcon_inbound_bill_dtls b where a.company_id=$cbo_company_id and a.id=b.mst_id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 $party_bill_cond $process_cond group by a.party_id, a.process_id order by a.party_id, a.process_id";
				}//listagg(CAST(a.process_id as varchar2(4000)),',') within group (order by a.process_id) as process_id
				//echo $bill_dtls;
                $bill_dtls_result=sql_select($bill_dtls);
				$i=1; $z=0;
                foreach ($bill_dtls_result as $row)
                {
                    if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
					
					//$opening_balance=$opening_balance_arr[$row[csf("party_id")]][$row[csf('process_id')]]-$adjusted_amount_array[$row[csf('party_id')]][$row[csf('process_id')]];
					$invoice_amount=$invoice_amount_arr[$row[csf("party_id")]][$row[csf('process_id')]];
					$receive_amount=$pay_rec_array[$row[csf('party_id')]][$row[csf('process_id')]]['rec_amount'];
					$lc_amount=$pay_rec_array[$row[csf('party_id')]][$row[csf('process_id')]]['lc_amount'];
					$process_name='';
					$process_id=array_unique(explode(',',$row[csf('process_id')]));
					$col_pro=count($process_id); 
					foreach ($process_id as $val)
					{
						if($process_name=="") $process_name= $production_process[$val]; else $process_name.=', '.$production_process[$val];
					}
					?>
                        <tr bgcolor="<?php echo $bgcolor;  ?>" onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
							<?php
                          //  if($z==0)
							//{
                            ?>
                             <td width="40" rowspan="<?php //echo $col_pro; ?>" ><?php echo $i; ?></td>
                             <td width="160" rowspan="<?php //echo $col_pro; ?>" ><?php echo $buyer_arr[$row[csf('party_id')]]; ?></td>
                             <?php
							//}
							 ?>
                             <td width="150" ><p><?php echo $production_process[$row[csf('process_id')]]; ?></p></td>
							                           
                             <td width="120" align="right" ><?php echo number_format($invoice_amount,2,'.',''); ?></td>
                             <td width="120" align="right" ><?php echo number_format($receive_amount,2,'.',''); ?></td>
                             <td width="120" align="right" ><?php echo number_format($lc_amount,2,'.',''); ?></td>
                             <td align="right"><?php $balance=$invoice_amount-($receive_amount+$lc_amount); echo number_format($balance,2,'.',''); ?></td>
                        </tr>
                    <?php

					//$tot_opening+=$opening_balance;
					$tot_inv_amount+=$invoice_amount;
					$tot_receive+=$receive_amount;
					$tot_lc_noncash+=$lc_amount;
					$tot_balance+=$balance;
                    $i++; $z++;
				}
				?>
                <tr class="tbl_bottom">
                    <td colspan="3" align="right"><b>Total:</b></td>
                    <td align="right"><b><?php echo number_format($tot_inv_amount,2); ?></b></td>
                    <td align="right"><b><?php echo number_format($tot_receive,2); ?></b></td>
                    <td align="right"><b><?php echo number_format($tot_lc_noncash,2); ?></b></td>
                    <td align="right"><b><?php echo number_format($tot_balance,2); ?></b></td>
                </tr>
            </table>
            </div>
            </fieldset>
        </div>
		<?php
	}
	else if($type==3)
	{
		?>
        <div align="center">
         <fieldset style="width:950px;">
            <table cellpadding="0" cellspacing="0" width="930">
                <tr  class="form_caption" style="border:none;">
                   <td align="center" width="100%" colspan="8" style="font-size:20px"><strong><?php echo 'Invoice Wise A/R'; ?></strong></td>
                </tr>
                <tr class="form_caption" style="border:none;">
                   <td align="center" width="100%" colspan="8" style="font-size:16px"><strong><?php echo $company_arr[str_replace("'","",$cbo_company_id)]; ?></strong></td>
                </tr>
                <tr class="form_caption" style="border:none;">
                    <td align="center" width="100%" colspan="8" style="font-size:12px">
                        <?php if( str_replace("'","",$txt_date_to)!="") echo " As On :- ".change_date_format(str_replace("'","",$txt_date_to),'dd-mm-yyyy')."" ;?>
                    </td>
                </tr>
            </table>
            <table width="927" border="1" cellpadding="0" cellspacing="0" rules="all" class="rpt_table">
                <thead>
                    <th width="40">SL</th>
                    <th width="140">Invc. No.</th>
                    <th width="130">Invc. Date</th>
                    <th width="70">Age (Days)</th>
                    <th width="120">Inv. Amount (Debit)</th>
                    <th width="120">Receive (Credit)</th>
                    <th width="120">Non-Cash /LC (Credit)</th>
                    <th>Balance</th>
                </thead>
            </table>
        <div style="max-height:300px; overflow-y:scroll; width:930px" id="scroll_body">
            <table width="910" border="1" class="rpt_table" rules="all" id="table_body">
            <?php
                if(str_replace("'","",$cbo_party_id)==0) $party_cond=""; else  $party_cond=" and a.party_name=$cbo_party_id";
                if(str_replace("'","",$cbo_party_id)==0) $party_bill_cond=""; else  $party_bill_cond=" and a.party_id=$cbo_party_id";
    
                if(str_replace("'","",$cbo_bill_type)==0) $bill_type_cond=""; else  $bill_type_cond=" and b.bill_type=$cbo_bill_type";
                if(str_replace("'","",$cbo_bill_type)==0) $process_cond=""; else  $process_cond=" and a.process_id=$cbo_bill_type";
                
                if($db_type==0)
                {
                    if( $date_from==0 && $date_to==0 ) $receipt_date_cond=""; else $receipt_date_cond= " and a.receipt_date between '".change_date_format($date_from,'yyyy-mm-dd')."' and '".change_date_format($date_to,'yyyy-mm-dd')."'";
                    if( $date_from==0 && $date_to==0 ) $bill_date_cond=""; else $bill_date_cond= " and a.bill_date between '".change_date_format($date_from,'yyyy-mm-dd')."' and '".change_date_format($date_to,'yyyy-mm-dd')."'";
                }
                else if($db_type==2)
                {
                    if( $date_from==0 && $date_to==0 ) $receipt_date_cond=""; else $receipt_date_cond= " and a.receipt_date between '".change_date_format($date_from,'','',1)."' and '".change_date_format($date_to,'','',1)."'";
                    if( $date_from=='' && $date_to=='' ) $bill_date_cond=""; else $bill_date_cond= " and a.bill_date between '".change_date_format($date_from,'','',1)."' and '".change_date_format($date_to,'','',1)."'";
                }
                
                if($date_to=="") $bill_date=""; else $bill_date= " and a.bill_date <=".$txt_date_to."";
                if($date_to=="") $receipt_date=""; else $receipt_date= " and a.receipt_date <=".$txt_date_to."";

                $pre_add_array=array();
                $sql_adj=sql_select("select a.party_name, b.bill_no, max(b.bill_date) as bill_date, b.bill_type,
				sum(case when a.instrument_id in (1,2,3) then b.total_adjusted else 0 end) as rec_amount,
				sum(case when a.instrument_id in (4,5) then b.total_adjusted else 0 end) as lc_amount
 from subcon_payment_receive_mst a,subcon_payment_receive_dtls b where a.id=b.master_id and a.company_id=$cbo_company_id and a.status_active=1 and a.is_deleted=0 $party_cond $receipt_date $bill_type_cond group by a.party_name, b.bill_type, b.bill_no order by a.party_name, b.bill_type, b.bill_no");
              /* echo "select a.party_name, b.bill_no, max(b.bill_date) as bill_date, b.bill_type,
				sum(case when a.instrument_id in (1,2,3) then b.total_adjusted else 0 end) as rec_amount,
				sum(case when a.instrument_id in (4,5) then b.total_adjusted else 0 end) as lc_amount
 from subcon_payment_receive_mst a,subcon_payment_receive_dtls b where a.id=b.master_id and a.company_id=$cbo_company_id and a.status_active=1 and a.is_deleted=0 $party_cond $receipt_date $bill_type_cond group by a.party_name, b.bill_type, b.bill_no order by a.party_name, b.bill_type, b.bill_no";*/
    			foreach ($sql_adj as $row)
                {
                    $pre_add_array[$row[csf("party_name")]][$row[csf("bill_type")]][$row[csf("bill_no")]]['rec_amount']=$row[csf("rec_amount")];
                    $pre_add_array[$row[csf("party_name")]][$row[csf("bill_type")]][$row[csf("bill_no")]]['lc_amount']=$row[csf("lc_amount")];
                    $pre_add_array[$row[csf("party_name")]][$row[csf("bill_type")]][$row[csf("bill_no")]]['bill_date']=$row[csf("bill_date")];
                }
    			// $sql_adj[0][csf('pre_adjusted')];
                //var_dump($pre_add_array);
               $sql_bill="select a.bill_no, a.prefix_no_num, a.party_id, max(a.bill_date) as bill_date, a.process_id, sum(b.amount) as amount from subcon_inbound_bill_mst a, subcon_inbound_bill_dtls b where a.company_id=$cbo_company_id and a.id=b.mst_id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 $party_bill_cond $bill_date $process_cond group by  a.party_id, a.process_id, a.bill_no, a.prefix_no_num order by a.party_id, a.process_id";
                
                $sql_bill_result=sql_select($sql_bill);
                $i=1; $k=1; $j=1; $party_array=array(); $process_array=array();
                foreach ($sql_bill_result as $row)
                {
                    if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
					
                    if (!in_array( $row[csf("party_id")],$party_array) )
                    {
                        if($k!=1)
                        { 
                        ?>
							<tr class="tbl_bottom">
								<td colspan="4" align="right"><b>Process Total:</b></td>
								<td align="right"><b><?php echo number_format($process_inv_amt,2); ?></b></td>
								<td align="right"><b><?php echo number_format($process_receive,2); ?></b></td>
								<td align="right"><b><?php echo number_format($process_lc,2); ?></b></td>
								<td align="right"><b><?php echo number_format($process_balance,2); ?></b></td>
							</tr>
                            <tr class="tbl_bottom">
                                <td colspan="4" align="right"><b>Party Total:</b></td>
                                <td align="right"><b><?php echo number_format($party_inv_amt,2); ?></b></td>
                                <td align="right"><b><?php echo number_format($party_receive,2); ?></b></td>
                                <td align="right"><b><?php echo number_format($party_lc,2); ?></b></td>
                                <td align="right"><b><?php echo number_format($party_balance,2); ?></b></td>
                            </tr>
                        <?php
                            unset($process_inv_amt);
							unset($process_receive);
							unset($process_lc);
							unset($process_balance);
							
							unset($party_inv_amt);
							unset($party_receive);
							unset($party_lc);
							unset($party_balance);
                        }
                        ?>
                            <tr bgcolor="#dddddd">
                                <td colspan="8" align="left" ><b>Party Name: <?php echo $buyer_arr[$row[csf("party_id")]]; ?></b></td>
                            </tr>
                        <?php
						$party_array[]=$row[csf("party_id")];
                        $k++;
                    }
					if (!in_array( $row[csf("process_id")],$process_array) )
					{
						if($j!=1)
						{ 
						?>
							<tr class="tbl_bottom">
								<td colspan="4" align="right"><b>Process Total:</b></td>
								<td align="right"><b><?php echo number_format($process_inv_amt,2); ?></b></td>
								<td align="right"><b><?php echo number_format($process_receive,2); ?></b></td>
								<td align="right"><b><?php echo number_format($process_lc,2); ?></b></td>
								<td align="right"><b><?php echo number_format($process_balance,2); ?></b></td>
							</tr>
						<?php
                            unset($process_inv_amt);
							unset($process_receive);
							unset($process_lc);
							unset($process_balance);
						}
						?>
							<tr bgcolor="#dddddd">
								<td colspan="8" align="left" ><b>Bill Type: <?php echo $production_process[$row[csf('process_id')]]; ?></b></td>
							</tr>
						<?php
						$process_array[]=$row[csf("process_id")];
						$j++;
					}                    
					$daysOnHand = datediff("d",$row[csf('bill_date')],date("Y-m-d"));
                    
                    ?>
                    <tr bgcolor="<?php echo $bgcolor;  ?>" onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
                         <td width="40" ><?php echo $i; ?></td>
                         <td width="140" ><?php echo $row[csf('bill_no')]; ?></td>
                         <td width="130" align="center" ><?php echo change_date_format($row[csf('bill_date')]); ?></td>
                         <td width="70" align="center" ><?php echo $daysOnHand; ?></td>
                         <td width="120" align="right" ><?php echo number_format($row[csf('amount')],2,'.',''); ?></td>
                         <td width="120" align="right" ><?php $rec_amount=$pre_add_array[$row[csf("party_id")]][$row[csf("process_id")]][$row[csf("bill_no")]]['rec_amount']; echo number_format($rec_amount,2,'.',''); ?></td>
                         <td width="120" align="right" ><?php $lc_amount=$pre_add_array[$row[csf("party_id")]][$row[csf("process_id")]][$row[csf("bill_no")]]['lc_amount']; echo number_format($lc_amount,2,'.',''); ?></td>
                         <td align="right"><?php $balance=($row[csf('amount')]-($rec_amount+$lc_amount)); echo number_format($balance,2,'.',''); ?></td>
                    </tr>
                    <?php
					$process_inv_amt+=$row[csf('amount')];
					$process_receive+=$rec_amount;
					$process_lc+=$lc_amount;
					$process_balance+=$balance;
					
					$party_inv_amt+=$row[csf('amount')];
					$party_receive+=$rec_amount;
					$party_lc+=$lc_amount;
					$party_balance+=$balance;
					
					$grand_inv_amt+=$row[csf('amount')];
					$grand_receive+=$rec_amount;
					$grand_lc+=$lc_amount;
					$grand_balance+=$balance;
                    $i++;			
                }
                ?>
                <tr class="tbl_bottom">
                    <td colspan="4" align="right"><b>Process Total:</b></td>
                    <td align="right"><b><?php echo number_format($process_inv_amt,2); ?></b></td>
                    <td align="right"><b><?php echo number_format($process_receive,2); ?></b></td>
                    <td align="right"><b><?php echo number_format($process_lc,2); ?></b></td>
                    <td align="right"><b><?php echo number_format($process_balance,2); ?></b></td>
                </tr>
                <tr class="tbl_bottom">
                    <td colspan="4" align="right"><b>Party Total:</b></td>
                    <td align="right"><b><?php echo number_format($party_inv_amt,2); ?></b></td>
                    <td align="right"><b><?php echo number_format($party_receive,2); ?></b></td>
                    <td align="right"><b><?php echo number_format($party_lc,2); ?></b></td>
                    <td align="right"><b><?php echo number_format($party_balance,2); ?></b></td>
                </tr>
                <tr class="tbl_bottom">
                    <td colspan="4" align="right"><b>Grand Total:</b></td>
                    <td align="right"><b><?php echo number_format($grand_inv_amt,2); ?></b></td>
                    <td align="right"><b><?php echo number_format($grand_receive,2); ?></b></td>
                    <td align="right"><b><?php echo number_format($grand_lc,2); ?></b></td>
                    <td align="right"><b><?php echo number_format($grand_balance,2); ?></b></td>
                </tr>
            </table>
            </div>
            </fieldset>
            </div>
        <?php
	}
	else if($type==4)
	{
		?>
        <div align="center">
         <fieldset style="width:950px;">
            <table cellpadding="0" cellspacing="0" width="930">
                <tr  class="form_caption" style="border:none;">
                   <td align="center" width="100%" colspan="10" style="font-size:20px"><strong><?php echo 'Bill Issue Statement'; ?></strong></td>
                </tr>
                <tr class="form_caption" style="border:none;">
                   <td align="center" width="100%" colspan="10" style="font-size:16px"><strong><?php echo $company_arr[str_replace("'","",$cbo_company_id)]; ?></strong></td>
                </tr>
                <tr class="form_caption" style="border:none;">
                    <td align="center" width="100%" colspan="10" style="font-size:12px">
                        <?php if(str_replace("'","",$txt_date_from)!="" && str_replace("'","",$txt_date_to)!="") echo "From ".change_date_format(str_replace("'","",$txt_date_from),'dd-mm-yyyy')." To ".change_date_format(str_replace("'","",$txt_date_to),'dd-mm-yyyy')."" ;?>
                    </td>
                </tr>
            </table>
            <table width="927" border="1" cellpadding="0" cellspacing="0" rules="all" class="rpt_table">
                <thead>
                    <th width="30">SL</th>
                    <th width="110">Invc. No.</th>
                    <th width="70">Invc. Date</th>
                    <th width="130">Party</th>
                    <th width="70">Challan</th>
                    <th width="90">Batch No</th>
                    <th width="110">Order No</th>
                    <th width="110">Color</th>
                    <th width="100">Bill Qty</th>
                    <th>Bill Amount</th>
                </thead>
            </table>
        <div style="max-height:300px; overflow-y:scroll; width:930px" id="scroll_body">
            <table width="910" border="1" class="rpt_table" rules="all" id="table_body">
            <?php
                if(str_replace("'","",$cbo_party_id)==0) $party_bill_cond=""; else  $party_bill_cond=" and a.party_id=$cbo_party_id";
    
                if(str_replace("'","",$cbo_bill_type)==0) $process_cond=""; else  $process_cond=" and b.process_id=$cbo_bill_type";
                
                if($db_type==0)
                {
                    if( $date_from==0 && $date_to==0 ) $bill_date_cond=""; else $bill_date_cond= " and a.bill_date between '".change_date_format($date_from,'yyyy-mm-dd')."' and '".change_date_format($date_to,'yyyy-mm-dd')."'";
                }
                else if($db_type==2)
                {
                    if( $date_from=='' && $date_to=='' ) $bill_date_cond=""; else $bill_date_cond= " and a.bill_date between '".change_date_format($date_from,'','',1)."' and '".change_date_format($date_to,'','',1)."'";
                }
				
				if ($db_type==0)
				{
					$delivery_id_cond="group_concat(b.delivery_id) as delivery_id";
					$batch_id_cond="group_concat(c.batch_id) as batch_id";
				}
				else if ($db_type==2)
				{
					$delivery_id_cond="listagg(CAST(b.delivery_id as varchar2(4000)),',') within group (order by b.delivery_id) as delivery_id";
					$batch_id_cond="listagg(CAST(c.batch_id as varchar2(4000)),',') within group (order by c.batch_id) as batch_id";
				}
                
            	$sql_bill="select a.bill_no, a.prefix_no_num, a.party_id, a.bill_date, b.color_id, b.order_id, b.challan_no, sum(b.delivery_qty) as bill_qty, sum(b.amount) as amount, c.batch_id from subcon_inbound_bill_mst a, subcon_inbound_bill_dtls b, subcon_delivery_dtls c, subcon_delivery_mst d where a.company_id=$cbo_company_id and a.id=b.mst_id and d.id=c.mst_id and c.id=b.delivery_id and a.status_active=1 and a.is_deleted=0 and b.status_active=1 and b.is_deleted=0 $party_bill_cond $bill_date_cond $process_cond group by a.bill_no, a.prefix_no_num, a.party_id, a.bill_date, b.color_id, b.order_id, b.challan_no, c.batch_id order by a.party_id, a.prefix_no_num";
                
                $sql_bill_result=sql_select($sql_bill);
                $i=1;
                foreach ($sql_bill_result as $row)
                {
                    if ($i%2==0) $bgcolor="#E9F3FF"; else $bgcolor="#FFFFFF";
					$batch_id=array_unique(explode(',',$row[csf('batch_id')]));
					$batch_no="";
					foreach($batch_id as $key)
					{
						if($batch_no=="") $batch_no=$batch_arr[$key]; else $batch_no.=','.$batch_arr[$key];
					}
					
                    ?>
                    <tr bgcolor="<?php echo $bgcolor;  ?>" onclick="change_color('tr_<?php echo $i; ?>','<?php echo $bgcolor; ?>')" id="tr_<?php echo $i; ?>">
                         <td width="30" ><?php echo $i; ?></td>
                         <td width="110" ><p><?php echo $row[csf('bill_no')]; ?></p></td>
                         <td width="70" align="center" ><?php echo change_date_format($row[csf('bill_date')]); ?></td>
                         <td width="130"><?php echo $buyer_arr[$row[csf('party_id')]]; ?></td>
                         <td width="70" align="center" ><p><?php echo $row[csf('challan_no')]; ?></p></td>
                         <td width="90" ><p><?php echo $batch_no;//$batch_arr[$row[csf('batch_id')]]; ?></p></td>
                         <td width="110" ><p><?php echo $order_arr[$row[csf('order_id')]]; ?></p></td>
                         <td width="110" ><p><?php echo $color_arr[$row[csf('color_id')]]; ?></p></td>
                         <td width="100" align="right" ><?php  echo number_format($row[csf('bill_qty')],2,'.',''); ?></td>
                         <td align="right"><?php echo number_format($row[csf('amount')],2,'.',''); ?></td>
                    </tr>
                    <?php
					$bill_qty+=$row[csf('bill_qty')];
					$bill_amount+=$row[csf('amount')];
                    $i++;			
                }
                ?>
                <tr class="tbl_bottom">
                    <td colspan="8" align="right"><b> Total:</b></td>
                    <td align="right"><b><?php echo number_format($bill_qty,2); ?></b></td>
                    <td align="right"><b><?php echo number_format($bill_amount,2); ?></b></td>
                </tr>
            </table>
            </div>
            </fieldset>
            </div>
        <?php
	}
}
?>