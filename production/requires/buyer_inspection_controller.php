<?php
session_start();
include('../../includes/common.php');

$user_id = $_SESSION['logic_erp']["user_id"];
if( $_SESSION['logic_erp']['user_id'] == "" ) { header("location:login.php"); die; }
$permission=$_SESSION['page_permission'];

$data=$_REQUEST['data'];
$action=$_REQUEST['action'];

$po_number=return_library_array( "select id,po_number from wo_po_break_down", "id", "po_number"  );
$color_library=return_library_array( "select id,color_name from lib_color", "id", "color_name"  );
$size_library=return_library_array( "select id,size_name from lib_size", "id", "size_name"  );
$company_library=return_library_array( "select id,company_name from lib_company", "id", "company_name"  );
//------------------------------------------------------------------------------------------------------

if ($action=="load_drop_down_buyer")
{
	echo create_drop_down( "cbo_buyer_name", 172, "select buy.id,buy.buyer_name from lib_buyer buy, lib_buyer_tag_company b where buy.status_active =1 and buy.is_deleted=0 and b.buyer_id=buy.id and b.tag_company='$data' and buy.id in (select  buyer_id from  lib_buyer_party_type where party_type in (1,3,21,90))  $buyer_cond order by buyer_name","id,buyer_name", 1, "-- Select Buyer --", $selected, "" );
} 
if ($action=="load_drop_down_po_number")
{ 
	//echo "select id,po_number from wo_po_break_down where job_no_mst='$data'";
	echo create_drop_down( "cbo_order_id",250, "select id,po_number from wo_po_break_down where job_no_mst='$data'","id,po_number", 1, "--Select--", "", "get_php_form_data( this.value, 'set_po_qnty_ship_date', 'requires/buyer_inspection_controller')","","","","","","" );
}
if ($action=="order_search_popup")
{
  	echo load_html_head_contents("Popup Info","../../", 1, 1, $unicode);
	extract($_REQUEST);

?>
     
	<script>
	function set_checkvalue()
	{
		if(document.getElementById('chk_job_wo_po').value==0) document.getElementById('chk_job_wo_po').value=1;
		else document.getElementById('chk_job_wo_po').value=0;
	}
	
	function js_set_value( job_no )
	{
		document.getElementById('selected_job').value=job_no;
		parent.emailwindow.hide();
	}
	
    </script>

</head>

<body>
<div align="center" style="width:100%;" >
<form name="searchorderfrm_1"  id="searchorderfrm_1" autocomplete="off">
	<table width="750" cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
    	<tr>
        	<td align="center" width="100%">
            	<table  cellspacing="0" cellpadding="0" border="0" class="rpt_table" align="center">
                    <thead>                	 
                        <th width="150" class="must_entry_caption">Company Name</th><th width="150">Buyer Name</th><th width="200">Date Range</th><th><input type="checkbox" value="0" onClick="set_checkvalue()" id="chk_job_wo_po">Job Without PO</th>           
                    </thead>
        			<tr>
                    	<td> <input type="hidden" id="selected_job">
							<?php 
								echo create_drop_down( "cbo_company_mst", 150, "select id,company_name from lib_company comp where status_active =1 and is_deleted=0 $company_cond order by company_name","id,company_name",1, "-- Select Company --", '',"load_drop_down( 'buyer_inspection_controller', this.value, 'load_drop_down_buyer', 'buyer_td' );" );
							?>
                    </td>
                   	<td id="buyer_td">
                     <?php 
						echo create_drop_down( "cbo_buyer_name", 172, $blank_array,'', 1, "-- Select Buyer --" );
					?>	</td>
                    <td><input name="txt_date_from" id="txt_date_from" class="datepicker" style="width:70px">
					  <input name="txt_date_to" id="txt_date_to" class="datepicker" style="width:70px">
					 </td> 
            		 <td align="center">
                     <input type="button" name="button2" class="formbutton" value="Show" onClick="show_list_view ( document.getElementById('cbo_company_mst').value+'_'+document.getElementById('cbo_buyer_name').value+'_'+document.getElementById('chk_job_wo_po').value+'_'+document.getElementById('txt_date_from').value+'_'+document.getElementById('txt_date_to').value, 'create_po_search_list_view', 'search_div', 'buyer_inspection_controller', 'setFilterGrid(\'list_view\',-1)')" style="width:100px;" /></td>
        		</tr>
             </table>
          </td>
        </tr>
        <tr>
            <td  align="center" height="40" valign="middle"><?php echo load_month_buttons(1);  ?>
            </td>
            </tr>
        <tr>
            <td align="center" valign="top" id="search_div"> 
	
            </td>
        </tr>
    </table>    
     
    </form>
   </div>
</body>           
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>
<?php
}


if($action=="create_po_search_list_view")
{
	
	$data=explode('_',$data);
	if ($data[0]!=0) $company=" and a.company_name='$data[0]'"; else { echo "Please Select Company First."; die; }
	if ($data[1]!=0) $buyer=" and a.buyer_name='$data[1]'"; else { echo "Please Select Buyer First."; die; }
	
	if($db_type==0)
	{
	if ($data[3]!="" &&  $data[4]!="") $shipment_date = "and b.pub_shipment_date between '".change_date_format($data[3], "yyyy-mm-dd", "-")."' and '".change_date_format($data[4], "yyyy-mm-dd", "-")."'"; else $shipment_date ="";
	}
	if($db_type==2)
	{
	if ($data[3]!="" &&  $data[4]!="") $shipment_date = "and b.pub_shipment_date between '".change_date_format($data[3],'','',1)."' and '".change_date_format($data[4],'','',1)."'"; else $shipment_date ="";
	}
	$buyer_arr=return_library_array( "select id, buyer_name from lib_buyer",'id','buyer_name');
	$comp=return_library_array( "select id, company_name from lib_company",'id','company_name');
	$arr=array (1=>$comp,2=>$buyer_arr);
	if ($data[2]==0)
	{
	 	 $sql= "select a.job_no,a.company_name,a.buyer_name,a.style_ref_no,a.job_quantity, b.po_number,b.po_quantity,b.shipment_date from wo_po_details_master a, wo_po_break_down b where a.job_no=b.job_no_mst and a.status_active=1 and b.status_active=1 $shipment_date $company $buyer order by a.job_no";  
		 echo  create_list_view("list_view", "Job No,Company,Buyer Name,Style Ref. No,Job Qty.,PO number,PO Quantity,Shipment Date", "90,120,100,100,90,90,90,80","1000","320",0, $sql , "js_set_value", "job_no", "", 1, "0,company_name,buyer_name,0,0,0,0", $arr , "job_no,company_name,buyer_name,style_ref_no,job_quantity,po_number,po_quantity,shipment_date", "",'','0,0,0,0,1,0,1,3') ;
	}
	else
	{
		$sql= "select a.job_no,a.company_name,a.buyer_name,a.style_ref_no from wo_po_details_master a where a.status_active=1  and a.is_deleted=0 $company $buyer order by a.job_no";
		echo  create_list_view("list_view", "Job No,Company,Buyer Name,Style Ref. No,", "90,120,100,100,90","1000","320",0, $sql , "js_set_value", "job_no", "", 1, "0,company_name,buyer_name,0,0,0,0", $arr , "job_no,company_name,buyer_name,style_ref_no", "",'','0,0,0,0,1,0,2,3') ;
	}
} 


if ($action=="populate_order_data_from_search_popup")
{
	//$data=explode("_",$data);
		if($db_type==0) $gro_field="";
	if($db_type==2) $gro_field=" group by a.job_no,a.company_name,a.buyer_name,a.style_ref_no,a.style_description,a.order_uom,a.set_break_down,a.gmts_item_id,a.total_set_qnty ";
	else $gro_field="";
	
	//echo "select a.job_no,a.company_name,a.buyer_name,a.style_ref_no,a.style_description,a.order_uom,a.set_break_down,a.gmts_item_id,a.total_set_qnty, sum(b.po_quantity) as po_quantity   from wo_po_details_master a, wo_po_break_down b where  a.job_no ='".$data."' and a.job_no=b.job_no_mst $gro_field";
	$data_array=sql_select("select a.job_no,a.company_name,a.buyer_name,a.style_ref_no,a.style_description,a.order_uom,a.set_break_down,a.gmts_item_id,a.total_set_qnty, sum(b.po_quantity) as po_quantity   from wo_po_details_master a, wo_po_break_down b where  a.job_no ='".$data."' and a.job_no=b.job_no_mst $gro_field");
	foreach ($data_array as $row)
	{
		echo "document.getElementById('cbo_company_name').value = '".$row[csf("company_name")]."';\n";  
		echo "document.getElementById('cbo_buyer_name').value = '".$row[csf("buyer_name")]."';\n";  
		echo "document.getElementById('txt_job_no').value = '".$row[csf("job_no")]."';\n";
		echo "document.getElementById('txt_style_no').value = '".$row[csf("style_ref_no")]."';\n";
		echo "document.getElementById('txt_style_des').value = '".$row[csf("style_description")]."';\n";
		echo "document.getElementById('txt_order_qty').value = '".$row[csf("po_quantity")]."';\n";
		//echo "document.getElementById('txt_plancut_qty').value = '".$row[csf("plan_cut")]."';\n";
		echo "document.getElementById('cbo_order_uom').value = '".$row[csf("order_uom")]."';\n";
		echo "document.getElementById('set_breck_down').value = '".$row[csf("set_break_down")]."';\n";
		echo "document.getElementById('item_id').value = '".$row[csf("gmts_item_id")]."';\n";
		echo "document.getElementById('tot_set_qnty').value = '".$row[csf("total_set_qnty")]."';\n";
     }
}

?>
<?php
if($action=="open_set_list_view")
{
echo load_html_head_contents("Set Entry","../../", 1, 1, $unicode,'','');
extract($_REQUEST);

?>
<script>
function js_set_value_set()
{
	var rowCount = $('#tbl_set_details tr').length-1;
	var set_breck_down="";
	var item_id=""
	for(var i=1; i<=rowCount; i++)
	{
		if (form_validation('cboitem_'+i+'*txtsetitemratio_'+i,'Gmts Items*Set Ratio')==false)
		{
			return;
		}
		if(set_breck_down=="")
		{
			set_breck_down+=$('#cboitem_'+i).val()+'_'+$('#txtsetitemratio_'+i).val();
			item_id+=$('#cboitem_'+i).val();
		}
		else
		{
			set_breck_down+="__"+$('#cboitem_'+i).val()+'_'+$('#txtsetitemratio_'+i).val();
			item_id+=","+$('#cboitem_'+i).val();
		}
	}
	document.getElementById('set_breck_down').value=set_breck_down;
	document.getElementById('item_id').value=item_id;
	parent.emailwindow.hide();
}
</script>
</head>
<body>
       <div id="set_details"  align="center">            
    	<fieldset>
        	<form id="setdetails_1" autocomplete="off">
            <input type="hidden" id="set_breck_down" />     
            <input type="hidden" id="item_id" />
            <table width="350" cellspacing="0" class="rpt_table" border="0" id="tbl_set_details" rules="all">
                	<thead>
                    	<tr>
                        	<th width="250" class="must_entry_caption">Item</th><th class="must_entry_caption">Set Item Ratio</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
					$tot_set_qnty=0;
					$data_array=explode("__",$set_breck_down);
					if ( count($data_array)>0)
					{
						$i=0;
						foreach( $data_array as $row )
						{
							$i++;
							$data=explode('_',$row);
							$tot_set_qnty=$tot_set_qnty+$data[1];
							?>
                            	<tr id="settr_1" align="center">
                                    <td>
									<?php 
										echo create_drop_down( "cboitem_".$i, 250, $garments_item, "",1,"-- Select Item --", $data[0], "",1,'' ); 
									?>
                                    </td>
                                    <td>
                                    <input type="text" id="txtsetitemratio_<?php echo $i;?>"   name="txtsetitemratio_<?php echo $i;?>" style="width:80px"  class="text_boxes_numeric" onChange="set_sum_value_set( 'tot_set_qnty','txtsetitemratio_' )"  value="<?php echo $data[1]; ?>"  readonly/> 
                                    </td>
                                </tr>
                            <?php
						}
					}
					else
					{
						
					?>
                    <tr id="settr_1" align="center">
                                   <td>
									<?php 
									echo create_drop_down( "cboitem_1", 240, $garments_item, "",1,"--Select--", 0, '',1,'' ); 
									?>
                                    </td>
                                     <td>
                                    <input type="text" id="txtsetitemratio_1" name="txtsetitemratio_1" style="width:80px" class="text_boxes_numeric" onChange="set_sum_value_set( 'tot_set_qnty', 'txtsetitemratio_' )" readonly /> 
                                     </td>
                                </tr>
                    <?php 
					} 
					?>
                </tbody>
                </table>
                <table width="350" cellspacing="0" class="rpt_table" border="0" rules="all">
                <tfoot>
                    	<tr>
                            <th width="250">Total</th>
                            <th>
                            <input type="text" id="tot_set_qnty" name="tot_set_qnty"  class="text_boxes_numeric" style="width:80px"  value="<?php echo $tot_set_qnty; ?>" readonly  />
                            </th>
                        </tr>
                    </tfoot>
                </table>
                <table width="350" cellspacing="0" class="" border="0">
                	<tr>
                        <td align="center" height="15" width="100%"> </td>
                    </tr>
                	<tr>
                        <td align="center" width="100%" class="button_container">
						        <input type="button" class="formbutton" value="Close" onClick="js_set_value_set()"/>
                        </td> 
                    </tr>
                </table>
            </form>
        </fieldset>
        </div>
 </body>           
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>

<?php
}
if($action=="set_po_qnty_ship_date")
{
	$data_array=sql_select("select po_quantity ,plan_cut,pub_shipment_date from  wo_po_break_down  where id=$data");
	foreach ($data_array as $row)
	{
		echo "document.getElementById('txt_po_quantity').value = '".$row[csf("po_quantity")]."';\n";  
		echo "document.getElementById('txt_pub_shipment_date').value = '".change_date_format($row[csf("pub_shipment_date")], "dd-mm-yyyy", "-")."';\n";  
     }
}



if ($action=="save_update_delete")
{
	$process = array( &$_POST );
	extract(check_magic_quote_gpc( $process )); 
	if ($operation==0)  // Insert Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		$id=return_next_id( "id", "pro_buyer_inspection", 1 ) ;
		$field_array="id,job_no,po_break_down_id,inspection_company,inspection_date,inspection_qnty,inspection_status,inspection_cause,comments,inserted_by,insert_date";
		$data_array="(".$id.",".$txt_job_no.",".$cbo_order_id.",".$cbo_inspection_company.",".$txt_inp_date.",".$txt_inspection_qnty.",".$cbo_inspection_status.",".$cbo_cause.",".$txt_comments.",".$_SESSION['logic_erp']['user_id'].",'".$pc_date_time."')";
		
 		$rID=sql_insert("pro_buyer_inspection",$field_array,$data_array,0);

		if($db_type==0)
		{
			if($rID )
			{
				mysql_query("COMMIT");  
				echo "0**".$rID;
			}
			else
			{
				mysql_query("ROLLBACK"); 
				echo "10**".$rID;
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
		if($rID)
			{
				oci_commit($con);
				echo "0**".$rID;
			}
			else
			{
				oci_rollback($con);
				echo "10**".$rID;
			}
		}
		disconnect($con);
		die;
	}
	
	
	else if ($operation==1)   // Update Here
	{
		$con = connect();
		if($db_type==0)
		{
			mysql_query("BEGIN");
		}
		$field_array="job_no*po_break_down_id*inspection_company*inspection_date*inspection_qnty*inspection_status*inspection_cause*comments*updated_by*update_date";
		$data_array="".$txt_job_no."*".$cbo_order_id."*".$cbo_inspection_company."*".$txt_inp_date."*".$txt_inspection_qnty."*".$cbo_inspection_status."*".$cbo_cause."*".$txt_comments."*".$_SESSION['logic_erp']['user_id']."*'".$pc_date_time."'";
		$rID=sql_update("pro_buyer_inspection",$field_array,$data_array,"id","".$txt_mst_id."",1);

		if($db_type==0)
		{
			if($rID )
			{
				mysql_query("COMMIT");  
				echo "1**".$rID;
			}
			else{
				mysql_query("ROLLBACK"); 
				echo "10**".$rID;
			}
		}
		
		if($db_type==2 || $db_type==1 )
		{
			if($rID )
			{
				oci_commit($con);
				echo "1**".$rID;
			}
			else
			{
				oci_rollback($con);
				echo "10**".$rID;
			}
		}
		disconnect($con);
		die;
	}
	
	
	else if ($operation==2)   // Delete Here
	{
		$con = connect();
		
		$field_array="status_active*is_deleted";
		$data_array="'0'*'1'";
		//echo $txt_mst_id;
		$rID=sql_delete("pro_buyer_inspection",$field_array,$data_array,"id","".$txt_mst_id."",1);
		//echo "2**".$rID;
		if($db_type==0)
		{	
			echo "2**".$rID;
		}
		if($db_type==2)
		{	oci_commit($con);
			echo "2**".$rID;
		}
		else
		{
			oci_rollback($con);
			echo "2**".$rID;	
		}
		disconnect($con);die;

	}
}
if($action=="show_active_listview")
{
	$insp_company_library=return_library_array( "select distinct a.id, a.supplier_name from lib_supplier a,lib_supplier_party_type b  where a.id=b.supplier_id and  b.party_type=41 and a.status_active=1 and a.is_deleted=0 order by a.supplier_name", "id", "supplier_name"  );//select distinct a.id, a.supplier_name from lib_supplier a,lib_supplier_party_type b  where a.id=b.supplier_id and  b.party_type=41 and a.status_active=1 and a.is_deleted=0 order by a.supplier_name

	$arr=array (0=>$po_number,1=>$insp_company_library,4=>$inspection_status,5=>$inspection_cause);
 	$sql= "select po_break_down_id,inspection_company,inspection_date,inspection_qnty,inspection_status,inspection_cause,comments,id from  pro_buyer_inspection  where   status_active=1 and is_deleted=0 and job_no='$data'"; 
	 
	 
	echo  create_list_view("list_view", "PO No,Inspection Company,Inspection Date,Inspection Qnty,Inspection Status,Inspection Cause, Comments", "120,120,100,80,80,80,150","800","220",0, $sql , "get_php_form_data", "id", "'populate_inspection_details_form_data'", 1, "po_break_down_id,inspection_company,0,0,inspection_status,inspection_cause,0", $arr , "po_break_down_id,inspection_company,inspection_date,inspection_qnty,inspection_status,inspection_cause,comments", "requires/buyer_inspection_controller",'','0,0,3,1,0,0,0') ;
}

if($action=="populate_inspection_details_form_data")
{
	$data_array=sql_select("select a.po_break_down_id,a.inspection_company,a.inspection_date,a.inspection_qnty,a.inspection_status,a.inspection_cause,a.comments,a.id,b.po_quantity ,b.plan_cut,b.pub_shipment_date from  pro_buyer_inspection a, wo_po_break_down b  where a.po_break_down_id=b.id and a.id =$data");
	foreach ($data_array as $row)
	{
		echo "document.getElementById('cbo_inspection_company').value = '".$row[csf("inspection_company")]."';\n";  
		echo "document.getElementById('txt_inp_date').value = '".change_date_format($row[csf("inspection_date")], "dd-mm-yyyy", "-")."';\n";  
		echo "document.getElementById('cbo_order_id').value = '".$row[csf("po_break_down_id")]."';\n";  
		echo "document.getElementById('txt_po_quantity').value = '".$row[csf("po_quantity")]."';\n";  
		echo "document.getElementById('txt_pub_shipment_date').value = '".change_date_format($row[csf("pub_shipment_date")], "dd-mm-yyyy", "-")."';\n"; 
	    echo "document.getElementById('txt_inspection_qnty').value = '".$row[csf("inspection_qnty")]."';\n"; 
		echo "document.getElementById('cbo_inspection_status').value = '".$row[csf("inspection_status")]."';\n";  
		echo "document.getElementById('cbo_cause').value = '".$row[csf("inspection_cause")]."';\n"; 
		echo "document.getElementById('txt_mst_id').value = '".$row[csf("id")]."';\n";  
		echo "set_button_status(1, '".$_SESSION['page_permission']."', 'fnc_buyer_inspection_entry',1);\n";  


     }
}
?>
