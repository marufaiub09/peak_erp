-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `wo_non_ord_aop_booking_mst`
--

DROP TABLE IF EXISTS `wo_non_ord_aop_booking_mst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wo_non_ord_aop_booking_mst` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wo_no_prefix` varchar(12) CHARACTER SET utf8 NOT NULL,
  `wo_no_prefix_num` int(11) NOT NULL,
  `wo_no` varchar(25) CHARACTER SET utf8 NOT NULL,
  `fab_booking_id` int(11) NOT NULL,
  `fab_booking_no` int(25) NOT NULL,
  `company_id` int(11) NOT NULL,
  `buyer_id` int(11) NOT NULL,
  `booking_date` int(11) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `exchange_rate` double NOT NULL,
  `pay_mode` int(11) NOT NULL,
  `source` int(11) NOT NULL,
  `aop_source` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `attention` varchar(200) CHARACTER SET utf8 NOT NULL,
  `is_approved` int(11) NOT NULL,
  `ready_to_approved` int(11) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `status_active` tinyint(1) NOT NULL,
  `inserted_by` int(11) NOT NULL,
  `insert_date` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `update_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wo_non_ord_aop_booking_mst`
--

LOCK TABLES `wo_non_ord_aop_booking_mst` WRITE;
/*!40000 ALTER TABLE `wo_non_ord_aop_booking_mst` DISABLE KEYS */;
INSERT INTO `wo_non_ord_aop_booking_mst` VALUES (1,'PAL-NAP-15-',1,'PAL-NAP-15-00001',13,0,1,57,2015,2,0,0,2,1,1,'',0,0,0,1,2,'2015-11-14 16:11:36',0,'0000-00-00 00:00:00');
/*!40000 ALTER TABLE `wo_non_ord_aop_booking_mst` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:45
