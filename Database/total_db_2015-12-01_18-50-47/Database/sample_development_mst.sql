-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `sample_development_mst`
--

DROP TABLE IF EXISTS `sample_development_mst`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sample_development_mst` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL DEFAULT '0',
  `buyer_name` int(11) NOT NULL DEFAULT '0',
  `style_ref_no` varchar(100) DEFAULT NULL,
  `product_dept` int(11) NOT NULL DEFAULT '0',
  `article_no` varchar(100) DEFAULT NULL,
  `item_name` int(11) NOT NULL DEFAULT '0',
  `item_category` int(11) NOT NULL DEFAULT '0',
  `region` int(11) NOT NULL DEFAULT '0',
  `agent_name` int(11) NOT NULL DEFAULT '0',
  `team_leader` int(11) NOT NULL DEFAULT '0',
  `dealing_marchant` int(11) NOT NULL DEFAULT '0',
  `estimated_shipdate` date NOT NULL,
  `remarks` varchar(500) DEFAULT NULL,
  `season` varchar(50) NOT NULL,
  `quotation_id` int(11) NOT NULL,
  `inserted_by` int(11) NOT NULL COMMENT 'Employee Code',
  `insert_date` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL COMMENT 'Employee Code',
  `update_date` datetime DEFAULT NULL,
  `status_active` tinyint(1) DEFAULT '1' COMMENT '0=No; 1=Yes',
  `is_deleted` tinyint(1) DEFAULT '0' COMMENT '0=No; 1=Yes',
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `working_factory` (`company_id`),
  KEY `buyer_name` (`buyer_name`),
  KEY `item_category` (`item_category`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sample_development_mst`
--

LOCK TABLES `sample_development_mst` WRITE;
/*!40000 ALTER TABLE `sample_development_mst` DISABLE KEYS */;
INSERT INTO `sample_development_mst` VALUES (1,1,42,'MIA 3/4 ARM',2,'',1,1,5,7,1,9,'2015-08-25','','',0,28,'2015-06-08 18:11:04',2,'2015-11-16 13:38:22',1,0),(2,1,42,'2765',2,'2765',2,1,5,7,1,11,'0000-00-00','','',0,28,'2015-10-24 12:35:48',NULL,NULL,1,0),(3,1,14,'1318/153797',2,'1318/153797',5,1,1,0,1,2,'0000-00-00','','',0,2,'2015-11-18 11:30:09',NULL,NULL,1,0);
/*!40000 ALTER TABLE `sample_development_mst` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:44
