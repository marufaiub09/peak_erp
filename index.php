           <?php
           ob_start();
           error_reporting('0');
		//echo phpinfo(); die;
		session_start();
	extract($_REQUEST);
		if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");
		 
		$from_menu=1;
		//echo $_SESSION['logic_erp']["buyer_id"];
		?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<link rel="shortcut icon" href="images/header/fav_ico.ico">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>goRMG</title>

    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link href="css/style_common.css" rel="stylesheet" type="text/css" />
    <link href="css/modal_window.css" rel="stylesheet" type="text/css" />
    <script src="js/ajaxpage_loader.js" type="text/javascript"></script>
    <script src="js/jquery.js" type="text/javascript"></script>
    <script src="includes/functions.js" type="text/javascript"></script>
	<script type="text/javascript" src="js/modal_window.js"></script>
	<script type="text/javascript">
	
	
 	//alert(add_days( "2012-01-08", '6' ))
        <?php

			if( $_SESSION['logic_erp']['scr_width'] == "" ) { ?>
			window.location.href = "screen.php?width=" + screen.width + "&height=" + screen.height+ "&type=1";
			 <?php }
			$heigt_scr = $_SESSION['logic_erp']['scr_height'] -235; // 265
			$width_scr = $_SESSION['logic_erp']['scr_width'] - 255;

		?>
		 
		function Button1_onclick( button ) {
			var menu = document.getElementById ('menu_conts');
			var wid = <?php echo $width_scr; ?>;
			var height=<?php echo $heigt_scr; ?>;
			
			if( menu.style.display != 'none' ) {
				menu.style.display ='none';
				// button.value = 'Show Menu';
				button.innerHTML = ' Show Menu';
				document.getElementById('main_body').style.width='100%';
				
			}
			else {
				menu.style.display ='block';
				// button.value = 'Hide Menu'
				button.innerHTML = ' Hide Menu';
				document.getElementById('main_body').style.width=wid+"px";
				
			}
		}
		
		function open_calculator()
		{
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', 'calculator.php', "~~~ Logic Calculator ~~~", 'width=320px,height=320px,center=1,resize=0,scrolling=0','home');
		}
		function full_screen()
		{
			var docElm = document.documentElement;
            if (docElm.requestFullscreen) {
                docElm.requestFullscreen();
            }
            else if (docElm.mozRequestFullScreen) {
                docElm.mozRequestFullScreen();
            }
            else if (docElm.webkitRequestFullScreen) {
                docElm.webkitRequestFullScreen();
            }
		}
		
	</script>
    
</head>

<body class="bodys"  ><!--  background:url(images/logic_bgss.png) no-repeat fixed left top transparent; -->
<section class="header-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-7 text-left">
                <div>
                    <a href="."><img src="images/header/goRMG-horizontal.png" alt="goRMG logo" align="Left"></a>
                </div>
            </div>
            <div class="col-md-5 text-right user-name">
                <div>
                    <strong>
                        <?php echo ucfirst($_SESSION['logic_erp']['user_name']); ?> &nbsp;&nbsp;|&nbsp;&nbsp; <a href="logout.php" style="text-decoration:none">Logout</a>&nbsp;&nbsp;
                    </strong>
                    <input type="hidden" name="index_page" id="index_page" value="1" />
                </div>
            </div>
        </div>
    </div>
</section>

<section class="nav-area">
    <nav class="navbar navbar-default">
        <div class="container">
            <div id="navbar">
                <ul class="nav nav-custom navbar-nav">
                    <?php

                    $user_id	= $_SESSION['logic_erp']["user_id"];
                    $module_id	= $_GET['module_id'];

                    foreach( $_SESSION['logic_erp']['user_menu'] AS $module ) {
                        ?>
                        <li <?php if ($module['module_id'] == $module_id) echo "class=\"active\""; ?>>
                            <a <?php if ($module['module_id'] == $module_id) echo "class=\"active\""; ?>
                                    href="index.php?module_id=<?php echo $module['module_id']; ?>"><?php echo $module['module_name']; ?></a>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </nav>
</section>

<section class="content-area">
    <div id="menu_conts">
        <?php include('left_menu.php'); ?>
    </div>

    <div id="main_body">
        <?php  /// include("barsOrColumns.html") ;
        if($g==1)
            include("dash_board.php");
        else if($g==2)
            include("today_production_graph.php");
        else if($g==3)
            include("trend_monthly_graph.php");
        else if($g==4)
            include("trend_daily_graph.php");
        else
            include("graph.php");
        ?>


    </div>
</section>

<footer class="footer-area">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 text-center copyright text-center">
                <p>&copy; All rights reserved. Powerd by <span class="pwdby">Skylark Soft</span></p>
            </div>
    </div>
</footer>

<script src="js/bootstrap.min.js"></script>
</body>
</html>
