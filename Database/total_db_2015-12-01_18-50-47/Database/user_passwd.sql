-- MySQL dump 10.13  Distrib 5.1.73, for redhat-linux-gnu (x86_64)
--
-- Host: localhost    Database: peak_erp
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `user_passwd`
--

DROP TABLE IF EXISTS `user_passwd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_passwd` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` varchar(15) DEFAULT NULL,
  `user_name` varchar(25) DEFAULT NULL,
  `password` varchar(50) NOT NULL,
  `user_full_name` varchar(50) NOT NULL COMMENT 'new add by reza',
  `designation` int(11) NOT NULL DEFAULT '0' COMMENT 'new add by reza',
  `created_on` date NOT NULL,
  `created_by` varchar(25) DEFAULT NULL,
  `access_ip` varchar(25) NOT NULL,
  `access_proxy_ip` varchar(25) NOT NULL,
  `expire_on` date DEFAULT '0000-00-00',
  `user_level` int(2) NOT NULL DEFAULT '0',
  `buyer_id` varchar(35) NOT NULL,
  `unit_id` varchar(35) NOT NULL,
  `is_data_level_secured` int(1) NOT NULL DEFAULT '0',
  `valid` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `employee_id` (`employee_id`),
  KEY `user_name` (`user_name`),
  KEY `password` (`password`),
  KEY `buyer_id` (`buyer_id`),
  KEY `is_data_level_secured` (`is_data_level_secured`),
  KEY `valid` (`valid`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_passwd`
--

LOCK TABLES `user_passwd` WRITE;
/*!40000 ALTER TABLE `user_passwd` DISABLE KEYS */;
INSERT INTO `user_passwd` VALUES (1,'11','sumon','2dvW0KyjkZWjpQ==','Md.Sumon Rahman',10,'2015-03-24','1','','','0000-00-00',2,'136,120','2,7,23',0,1),(2,NULL,'Admin','ztDc0NeUkZg=','Admin',40,'2014-12-07','1','','','0000-00-00',1,'','',0,1),(3,NULL,'yarn','5s3h1ZqVkg==','yarn store',39,'2014-12-29','2','','','2024-12-31',1,'','1',1,1),(4,NULL,'jony','19vd4JqVkg==','Md.Ariful Islam',29,'2015-01-08','1','','','0000-00-00',1,'','',1,1),(5,NULL,'Shaheen','4NTQz87IzZakow==','Md.Shaheen',17,'2015-01-08','1','','','0000-00-00',1,'','',1,1),(6,NULL,'Mostafa','2tvi28rJwJakow==','Mr.Mostafa',29,'2015-01-08','2','','','0000-00-00',1,'','',1,1),(7,NULL,'Nazrul','283p2d7PkJel','Nazrul Islam',29,'2015-01-08','2','','','0000-00-00',1,'','',1,1),(8,NULL,'Mamun','2s3c3NeUkZg=','Sarkar Al Mamun',29,'2015-01-08','2','','','0000-00-00',1,'','',1,1),(9,NULL,'Mouli','2tvk09KUkZg=','Mouli Rubaiyat',29,'2015-01-08','2','','','0000-00-00',1,'','',1,1),(10,NULL,'Nurul','2+Hh3NWUkZg=','Md.Nurul Islam(Forhad)',20,'2015-01-13','2','','','0000-00-00',1,'','',1,1),(11,NULL,'Ismail','1t/cyNLPkJel','Md.Ismail Hossain',47,'2015-01-14','2','','','0000-00-00',1,'','',1,1),(12,NULL,'Sanjib','4M3d0dLFkJel','Md.Sanjib',47,'2015-01-25','2','','','0000-00-00',1,'','',1,1),(13,NULL,'comm','0Nvc1JqVkg==','Humayun Biswas',2,'2015-01-28','1','','','0000-00-00',1,'','',1,1),(14,NULL,'anis','ztrY2pqVkg==','Md.Anis',31,'2015-01-29','2','','','0000-00-00',1,'','',1,1),(15,NULL,'urmi','4t7c0JqVkg==','Urmi Akter',48,'2015-02-01','1','','','0000-00-00',1,'','',1,1),(16,NULL,'rafiq','383V0NqUkZg=','Md.Rafiqul Islam',38,'2015-02-03','2','','','0000-00-00',1,'','',1,1),(17,NULL,'hasan','1c3iyNeUkZg=','Md.Hasan',14,'2015-02-06','1','','','0000-00-00',1,'','',1,1),(18,NULL,'gate','1M3jzJqVkg==','Md.Alauddin',27,'2015-02-12','2','','','0000-00-00',1,'','',1,1),(19,NULL,'nawfal','np6im56Z','Mahamud Hossain',38,'2015-02-15','1','','','0000-00-00',1,'','',1,1),(20,NULL,'IT','3dHQ0tLX','IT DEPARTMENT',40,'2015-02-17','1','','','0000-00-00',2,'','',0,1),(21,NULL,'sazzad','4M3p4crHkJel','SAZZAD AMIN',2,'2015-02-24','1','','','0000-00-00',2,'','',0,1),(22,NULL,'sazzad.md','4M3p4crHkJel','sazzad123',2,'2015-03-10','1','','','0000-00-00',2,'','',1,1),(23,NULL,'mahbub','np6im56Z','123456',27,'2015-03-10','1','','','0000-00-00',1,'','',1,1),(24,NULL,'sobuz','4NvR3OOUkZg=','Md. Abu Naser Sobuz',19,'2015-03-12','2','','','0000-00-00',1,'','',1,1),(25,NULL,'kamal','2M3cyNWUkZg=','Md.Kamal Hossan',29,'2015-04-01','2','','','0000-00-00',1,'','',1,1),(26,NULL,'titu','4dXj3JqVkg==','Saddam Hossain',42,'2015-04-02','20','','','0000-00-00',1,'','',1,1),(27,NULL,'marc','383V0NqUkZg=','Rafiqul Islam',29,'2015-05-13','2','','','0000-00-00',1,'','',1,1),(28,NULL,'raqib','383g0MuUkZg=','Md. Raqib',29,'2015-06-01','2','','','0000-00-00',1,'','',1,1),(29,NULL,'hassan','1c3i2srRkJel','Mahmud Hassan',27,'2015-06-29','2','','','0000-00-00',1,'','',1,1),(30,NULL,'saeed','4M3UzM2UkZg=','Saeed Khan',26,'2015-08-06','1','','','0000-00-00',2,'','',1,1),(31,NULL,'ruhul','3+HX3NWUkZg=','Md.Ruhul Islam',21,'2015-08-19','2','','','0000-00-00',1,'','',1,1),(32,NULL,'ahmed','ztTczM2UkZg=','Ahmed Hossain',25,'2015-08-26','2','','','0000-00-00',1,'','',1,1),(33,NULL,'report','39Hf1tvX','Nawfel',4,'2015-09-02','2','','','0000-00-00',1,'','',1,1),(34,NULL,'tuhin','4eHX0NeUkZg=','Nazmul Islam Tuhin',4,'2015-09-02','2','','','0000-00-00',1,'','',1,1),(35,NULL,'zaheed','583XzM7HkJel','Shafiul Islam',15,'2015-10-03','2','','','0000-00-00',1,'','',1,1),(36,NULL,'audit','zuHT0N2UkZg=','audit',45,'2015-11-14','2','','','0000-00-00',1,'','',1,1),(37,NULL,'firoz','09Xh1uOUkZg=','Firoz Kabir',40,'2015-11-25','2','','','0000-00-00',1,'','',1,1),(38,NULL,'samsul','4M3c2t7PkJel','Md.Samsul Islam',29,'2015-11-25','2','','','0000-00-00',1,'','',1,1);
/*!40000 ALTER TABLE `user_passwd` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:50:44
