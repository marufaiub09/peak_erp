<?php
/*--- ----------------------------------------- Comments
Purpose			: 						
Functionality	:	
JS Functions	:
Created by		:	Md. Abdul Hakim 
Creation date 	: 	31-03-2013
Updated by 		: 		
Update date		:
Oracle Convert 	:	Kausar		
Convert date	: 	20-05-2014	   
QC Performed BY	:		
QC Date			:	
Comments		:
*/
session_start(); 
require_once('../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;
//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Sub-Contract Material Receive Info", "../", 1,1, $unicode,'','');
?>
<script>
	if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../logout.php"; 
	var permission='<?php echo $permission; ?>';

	var str_color = [<?php echo substr(return_library_autocomplete( "select color_name from lib_color group by color_name", "color_name" ), 0, -1); ?>];

	$(document).ready(function(e)
	 {
            $("#txt_color").autocomplete({
			 source: str_color
		  });
     });

	var str_material_description = [<?php echo substr(return_library_autocomplete( "select material_description from sub_material_dtls group by material_description ", "material_description" ), 0, -1); ?> ];

	function set_auto_complete(type)
	{
		if(type=='subcon_material_receive')
		{
			$("#txt_material_description").autocomplete({
			source: str_material_description
			});
		}
	}

	function openmypage_rec_id(page_link,title)
	{ 
		var data=document.getElementById('cbo_company_name').value+"_"+document.getElementById('cbo_location_name').value+"_"+document.getElementById('cbo_party_name').value;
		page_link='requires/sub_contract_material_receive_controller.php?action=receive_popup&data='+data
		
		emailwindow=dhtmlmodal.open('EmailBox','iframe', page_link, title, 'width=780px, height=400px, center=1, resize=0, scrolling=0','')
		emailwindow.onclose=function()
		{
			var theform=this.contentDoc.forms[0]//("search_subcontract_frm"); //Access the form inside the modal window
			var theemail=this.contentDoc.getElementById("selected_job");
			if (theemail.value!="")
			{
				freeze_window(5);
				reset_form('','','txt_receive_no*cbo_company_name*cbo_location_name*cbo_party_name*txt_receive_challan*txt_receive_date*cbo_item_category*txt_material_description*txt_receive_quantity*txt_order_no*cbo_dia_uom*txt_gsm*txt_color','','');
				get_php_form_data( theemail.value, "load_php_data_to_form", "requires/sub_contract_material_receive_controller" );
				show_list_view(theemail.value,'subcontract_receive_dtls_list_view','receive_list_view','requires/sub_contract_material_receive_controller','setFilterGrid("list_view",-1)');
				release_freezing();
			}
		}
	}

	function job_search_popup(page_link,title)
	{
		if ( form_validation('cbo_company_name*cbo_party_name','Company Name*Party Name')==false )
		{
			return;
		}
		else
		{
			var data=document.getElementById('txt_order_no').value+"_"+document.getElementById('cbo_company_name').value+"_"+document.getElementById('cbo_location_name').value+"_"+document.getElementById('cbo_party_name').value;
			page_link='requires/sub_contract_material_receive_controller.php?action=job_popup&data='+data
			
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=780px,height=400px,center=1,resize=0,scrolling=0','')
			emailwindow.onclose=function()
			{
				freeze_window(5);
				var theform=this.contentDoc.forms[0];
				var theemail=this.contentDoc.getElementById("selected_order").value;
				 // alert(theemail);
				get_php_form_data( theemail, "load_php_data_to_form_dtls_order", "requires/sub_contract_material_receive_controller" );
				release_freezing();
			}
		}
	}

	function fnc_material_receive( operation )
	{
		var is_delete=document.getElementById('delete_allowed').value;
		var batch_no=document.getElementById('batch_no').value;
		var cbo_status=document.getElementById('cbo_status').value;
		var zero_val='';
		if ( form_validation('cbo_company_name*cbo_party_name*txt_receive_challan*txt_receive_date*txt_order_no*cbo_item_category*txt_material_description*txt_receive_quantity*cbo_uom','Company Name*Party*Receive Challan*Receive Date*Order No*Item Category*Material Description*Receive Quantity*UOM')==false )
		{
			return;
		}
		else
		{
			if(cbo_status==1)
			{
				if (is_delete==1)
				{
					alert("Delete Not Allowed. Used in Batch No="+ batch_no);
					return;
				}
			}
			if (operation==2)
			{
				if (is_delete==1)
				{
					alert("Delete Not Allowed. Used in Batch No="+ batch_no);
					return;
				}
				else
				{
					var r=confirm('Press \"OK\" to delete all items of this challan.\nPress \"Cancel\" Do Not Delete.');
					if(r==true) 
					{ 
						zero_val="1";
					}
					else
					{
						zero_val="0";
						return;
					}
				}
			}
			//var rec_date=change_date_format(document.getElementById('txt_receive_date').value);
			var data="action=save_update_delete&operation="+operation+'&zero_val='+zero_val+get_submitted_data_string('txt_receive_no*cbo_company_name*cbo_location_name*cbo_party_name*txt_receive_challan*txt_receive_date*cbo_item_category*txt_material_description*txt_receive_quantity*cbo_uom*order_no_id*txt_order_no*cbo_status*txt_roll*txt_cone*txt_gsm*txt_color*txt_grey_dia*txt_fin_dia*cbo_dia_uom*update_id*update_id2',"../");
			//alert (data); return;
			freeze_window(operation);
			http.open("POST","requires/sub_contract_material_receive_controller.php",true);
			http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			http.send(data);
			http.onreadystatechange = fnc_material_receive_response;
		}
	}
	
	function fnc_material_receive_response()
	{
		if(http.readyState == 4) 
		{
			//alert(http.responseText);//return;
			var response=trim(http.responseText).split('**');
			//if (response[0].length>3) reponse[0]=10;	
			show_msg(response[0]);
			//$('#cbo_uom').val(12);
			if(response[0]==0 || response[0]==1)
			{
				document.getElementById('txt_receive_no').value= response[1];
				document.getElementById('update_id').value = response[2];
				//document.getElementById('update_id2').value = response[3];
				reset_form('','','txt_grey_dia*txt_fin_dia*cbo_dia_uom*txt_receive_quantity*txt_roll*txt_cone','','');
				
				set_button_status(0, permission, 'fnc_material_receive',1);
				show_list_view(response[2],'subcontract_receive_dtls_list_view','receive_list_view','requires/sub_contract_material_receive_controller','setFilterGrid("list_view",-1)');
			}
			if(response[0]==2)
			{
				reset_form('materialreceive_1','receive_list_view','','', '');
			}
			release_freezing();
		}
	}

	function change_uom(item)
	{
		if(item==1 || item==2 || item==13)
		{
			document.getElementById('cbo_uom').value= 12;
		}
		else if(item==3 || item==14)
		{
			document.getElementById('cbo_uom').value= 27;
		}
		else if(item==0)
		{
			document.getElementById('cbo_uom').value= 0;
		}
		else
		{
			document.getElementById('cbo_uom').value= 1;
		}
		
		if (item==1)
		{
			$('#txt_cone').removeAttr('disabled','disabled');	
		}
		else
		{
			$('#txt_cone').val('');
			$('#txt_cone').attr('disabled','disabled');
		}
	}
</script>
</head>
<body onLoad="set_hotkey();set_auto_complete('subcon_material_receive')">
<div style="width:100%;" align="center">
	<?php echo load_freeze_divs ("../",$permission);  ?>
    <fieldset style="width:950px;">
    <legend>Sub-Contract Material Receive</legend>
        <form name="materialreceive_1" id="materialreceive_1" autocomplete="off">  
            <table  width="940" cellspacing="2" cellpadding="0" border="0">
                <tr>
                    <td  width="130" height="" align="right">Receive ID</td>
                    <td  width="170">
                        <input class="text_boxes"  type="text" name="txt_receive_no" id="txt_receive_no" onDblClick="openmypage_rec_id('xx','Subcontract Receive')"  placeholder="Double Click" style="width:160px;" readonly/>
                    </td>
                    <td  width="130" align="right" class="must_entry_caption">Company Name </td>
                    <td width="170"> 
                        <?php echo create_drop_down( "cbo_company_name", 172, "select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name","id,company_name", 1, "-- Select Company --", $selected, "load_drop_down( 'requires/sub_contract_material_receive_controller', this.value, 'load_drop_down_location', 'location_td' ); load_drop_down( 'requires/sub_contract_material_receive_controller', this.value, 'load_drop_down_buyer', 'buyer_td' );"); ?>
                    </td>
                    <td width="130" align="right">Location Name</td>
                    <td id="location_td">
                         <?php echo create_drop_down( "cbo_location_name", 172, $blank_array,"", 1, "-- Select Location --", $selected, "" );?>
                    </td>
                </tr>
                <tr>
                    <td align="right" class="must_entry_caption">Party</td>
                    <td id="buyer_td">
                        <?php echo create_drop_down( "cbo_party_name", 172, $blank_array,"", 1, "-- Select Party --", $selected, "" );?>
                    </td>
                    <td  width="130" class="must_entry_caption" align="right">Receive Challan</td>
                    <td  width="170">
                        <input class="text_boxes"  type="text" name="txt_receive_challan" id="txt_receive_challan" style="width:160px;" />  
                    </td>
                    <td  width="130" class="must_entry_caption" align="right">Receive Date</td>
                    <td>
                        <input type="text" name="txt_receive_date" id="txt_receive_date"  class="datepicker" style="width:160px" />             
                    </td>
                </tr>
            </table>
            <br/>
            <fieldset style="width:910px;">
            <legend>Sub-Contract Order Details Entry</legend>
            <table  cellpadding="0" cellspacing="2" border="0" width="900">
                <thead class="form_table_header">
                    <tr align="center" >
                        <th width="80" class="must_entry_caption">Order No</th>
                        <th width="100" class="must_entry_caption">Item Category</th>
                        <th width="140" class="must_entry_caption">Material Description</th>
                        <th width="60">Color</th>
                        <th width="40">GSM</th>
                        <th width="40">Grey Dia/ Width</th>
                        <th width="40">Fin. Dia/ Width</th>
                        <th width="60">Dia UOM</th>
                        <th width="30">Roll /Bag</th>
                        <th width="70" class="must_entry_caption">Receive Qty</th>
                        <th width="40" class="must_entry_caption">UOM</th>
                        <th width="30">Cone</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tr>
                    <td><input type="hidden" name="order_no_id" id="order_no_id">
                        <input class="text_boxes" name="txt_order_no" id="txt_order_no" type="text" style="width:78px" placeholder="Browse" onDblClick="job_search_popup('requires/sub_contract_material_receive_controller.php?action=job_popup','Order Selection Form')" />	
                            
                    </td>
                    <td>
                        <?php echo create_drop_down( "cbo_item_category", 100, $item_category,"", 1, "--Select Item--",0,"change_uom(this.value)", "","1,2,3,13,14" );?>
                    </td>
                    <td>
                        <input type="text" id="txt_material_description" name="txt_material_description" class="text_boxes" style="width:140px" title="Maximum 200 Character">
                    </td>
                    <td>
                        <input name="txt_color" id="txt_color" class="text_boxes" type="text"  style="width:60px" />
                    </td>
                    <td>
                        <input name="txt_gsm" id="txt_gsm" class="text_boxes_numeric" type="text"  style="width:40px" />
                    </td>
                    <td>
                        <input name="txt_grey_dia" id="txt_grey_dia" class="text_boxes_numeric" type="text"  style="width:40px" />
                    </td>
                    <td>
                        <input name="txt_fin_dia" id="txt_fin_dia" class="text_boxes_numeric" type="text"  style="width:40px" />
                    </td>
                    <td>
                    	<?php echo create_drop_down( "cbo_dia_uom",60, $unit_of_measurement,"", 1, "-UOM-",0,"", "","25,29" );?>
                        <!--<input name="txt_dia_uom" id="txt_dia_uom" class="text_boxes" type="text"  style="width:40px" />-->
                    </td>
                    <td>
                        <input name="txt_roll" id="txt_roll" class="text_boxes_numeric" type="text"  style="width:30px" />
                    </td>
                    <td>
                        <input name="txt_receive_quantity" id="txt_receive_quantity" class="text_boxes_numeric" type="text"  style="width:65px" />
                    </td>
                     <td>
                        <?php echo create_drop_down( "cbo_uom",50, $unit_of_measurement,"", 1, "-Select-",12,"", "","" );?>
                    </td>
                    <td>
                        <input name="txt_cone" id="txt_cone" class="text_boxes_numeric" type="text"  style="width:30px" disabled />
                    </td>
                    <td>
                        <?php echo create_drop_down( "cbo_status", 70, $yes_no, "", 0, "",2,'','' ); ?>
                    </td>
                </tr>
             </table>
             </fieldset>  
             <table width="900" cellspacing="2" cellpadding="0" border="0">
                 <tr>
                      <td><input type="hidden" name="update_id" id="update_id"></td>
                      <td><input type="hidden" name="update_id2" id="update_id2"></td>
                      <td><input type="hidden" name="delete_allowed" id="delete_allowed"></td>
                      <td><input type="hidden" name="batch_no" id="batch_no"></td>
                 </tr>
                 <tr>
                    <td align="center" colspan="12" valign="middle" class="button_container">
                        <?php echo load_submit_buttons($permission, "fnc_material_receive", 0,0,"reset_form('materialreceive_1','receive_list_view','','cbo_status,2', 'disable_enable_fields(\'cbo_company_name\',0)')",1); ?>
                    </td>
                 </tr>  
                 <br/>
                 <tr align="center">
                    <td colspan="12" id="receive_list_view"> </td>	
                </tr>               
          </table>
        </form>
    </fieldset>
</div>
</body>
<script language="javascript" type="text/javascript">  
/*	function SetFocus(txt_order_no)  
	{  
	   document.getElementById(txt_order_no).focus();  
	}  
*/</script> 
<script src="../includes/functions_bottom.js" type="text/javascript"></script>
</html>