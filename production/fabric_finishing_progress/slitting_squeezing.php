<?php
/*-------------------------------------------- Comments
Purpose			: 	This form will create Dyeing Production Entry

Functionality	:	
JS Functions	:
Created by		:	Aziz
Creation date 	: 	12-04-2014
Updated by 		: 		
Update date		: 	   
QC Performed BY	:		
QC Date			:	
Comments		:
*/ 

session_start();
if( $_SESSION['logic_erp']['user_id'] == "" ) header("location:login.php");

require_once('../../includes/common.php');
extract($_REQUEST);
$_SESSION['page_permission']=$permission;

//--------------------------------------------------------------------------------------------------------------------
echo load_html_head_contents("Slitting Squeezing Entry Info","../../", 1, 1, "",'1','');
?>
<script>
var permission='<?php echo $permission; ?>';
if( $('#index_page', window.parent.document).val()!=1) window.location.href = "../../logout.php";
	
	var str_color = [<?php echo substr(return_library_autocomplete( "select color_name from lib_color group by color_name", "color_name" ), 0, -1); ?>];
	$(document).ready(function(e)
	 {
            $("#txt_color").autocomplete({
			 source: str_color
		  });
     });

	function openmypage_batchnum()
	{
		var cbo_company_id = $('#cbo_company_id').val();
		if (form_validation('cbo_company_id','Company')==false)
		{
			return;
		}
		else
		{
			var page_link='requires/slitting_squeezing_controller.php?cbo_company_id='+cbo_company_id+'&action=batch_number_popup';
			var title='Batch Number Popup';
			emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=820px,height=420px,center=1,resize=1,scrolling=0','../');
			emailwindow.onclose=function()
			{
				var theform=this.contentDoc.forms[0];
				var batch_id=this.contentDoc.getElementById("hidden_batch_id").value;
				//alert( batch_id)
				if(batch_id!="")
				{
					freeze_window(5);
					get_php_form_data(batch_id, "populate_data_from_batch", "requires/slitting_squeezing_controller" );
					show_list_view(batch_id,'show_fabric_desc_listview','list_fabric_desc_container','requires/slitting_squeezing_controller','');
					release_freezing();
				}
				get_php_form_data(document.getElementById('cbo_company_id').value+'**'+document.getElementById('cbo_floor').value+'**'+document.getElementById('cbo_machine_name').value, 'populate_data_from_machine', 'requires/slitting_squeezing_controller' );
				//get_php_form_data(document.getElementById('cbo_company_id').value+'**'+document.getElementById('cbo_machine_name').value, 'populate_data_from_machine', 'requires/slitting_squeezing_controller' );
			}
		}
	}
	function check_batch()
	{
		var batch_no=$('#txt_batch_no').val();
		var cbo_company_id = $('#cbo_company_id').val();
		if(batch_no!="")
		{
			var response=return_global_ajax_value( cbo_company_id+"**"+batch_no, 'check_batch_no', '', 'requires/slitting_squeezing_controller');
			var response=response.split("_");
			if(response[0]==0)
			{
				alert('Batch no not found.');
				$('#txt_batch_no').val('');
				$('#hidden_batch_id').val(''); 
				//$('#cbo_company_id').val(''); 
				$('#txt_update_id').val(''); 
				$('#cbo_sub_process').val('');
				$('#txt_process_end_date').val('');
				$('#txt_end_hours').val('');
				$('#txt_end_minutes').val('');
				$('#cbo_machine_name').val('');
				$('#txt_remarks').val('');
				reset_form('slittingsqueezing_1','','','','$(\'#list_fabric_desc_container tr:not(:first)\').remove();');
			}
			else
			{
				$('#hidden_batch_id').val(response[1]);
				//get_php_form_data(response[1], "populate_data_from_batch", "requires/slitting_squeezing_controller" );
				get_php_form_data(response[1]+'_'+batch_no, "populate_data_from_batch", "requires/slitting_squeezing_controller" );
				show_list_view(response[1],'show_fabric_desc_listview','list_fabric_desc_container','requires/slitting_squeezing_controller','');
				get_php_form_data(document.getElementById('cbo_company_id').value+'**'+document.getElementById('cbo_floor').value+'**'+document.getElementById('cbo_machine_name').value, 'populate_data_from_machine', 'requires/slitting_squeezing_controller' );
			 $('#cbo_sub_process').focus();	
			}
		}
	}
	function fnc_move_cursor(val,id, field_id,lnth,max_val)
	{
		var str_length=val.length;
		if(str_length==lnth)
		{
			$('#'+field_id).select();
			$('#'+field_id).focus();
		}
		if(val>max_val)
		{
			document.getElementById(id).value=max_val;
		}
	}
		function fnc_pro_fab_subprocess( operation )
		{
			if( form_validation('cbo_company_id*txt_batch_no*txt_process_end_date*txt_process_start_date*txt_process_date*txt_process_end_date*txt_batch_ID','Company* Batch No*Production Date*Start Date*Process Date*Batch ID')==false )
			{
			return;
			} 
			var end_hours=document.getElementById('txt_end_hours').value;	
			var end_minutes=document.getElementById('txt_end_minutes').value;
			var start_hours=document.getElementById('txt_start_hours').value;	
			var start_minutes=document.getElementById('txt_start_minutes').value;
			if( end_hours=="" ||  end_minutes=="" || start_hours=="" ||  start_minutes=="")
			{
			alert('Hour & Minute Must fill Up');
			return;	
			} 
			var row_num=$('#tbl_item_details tbody tr').length-1;
				
			var data_all="";
			for (var i=1; i<=row_num; i++)
			{	
			 data_all+=get_submitted_data_string('txtconscomp_'+i+'*txtgsm_'+i+'*txtbodypart_'+i+'*txtdiawidth_'+i+'*txtbatchqnty_'+i+'*txtprodid_'+i+'*updateiddtls_'+i+'*txtdiawidthID_'+i,"../../",i);
			}
			var data="action=save_update_delete&operation="+operation+get_submitted_data_string('cbo_company_id*txt_batch_no*hidden_batch_id*txt_batch_ID*cbo_sub_process*txt_process_end_date*txt_process_date*txt_end_hours*txt_end_minutes*cbo_machine_name*txt_remarks*txt_ext_id*txt_update_id*cbo_floor*cbo_shift_name*txt_process_start_date*txt_start_hours*txt_start_minutes',"../../")+data_all+'&total_row='+row_num;
		//alert (data);return;
	  freeze_window(operation);
	  http.open("POST","requires/slitting_squeezing_controller.php",true);
	  http.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	  http.send(data);
	  http.onreadystatechange = fnc_pro_fab_subprocess_response;
		}
		
		function fnc_pro_fab_subprocess_response()
		{
		if(http.readyState == 4) 
		{
			//alert (http.responseText);
			var reponse=trim(http.responseText).split('**');
			show_msg(reponse[0]);
			//document.getElementById('txt_batch_no').value = reponse[1];
			document.getElementById('txt_update_id').value = reponse[1];
			show_list_view(document.getElementById('txt_batch_ID').value,'show_fabric_desc_listview','list_fabric_desc_container','requires/slitting_squeezing_controller','');

			set_button_status(1, permission, 'fnc_pro_fab_subprocess',1,1);
			release_freezing();
		}
		}
	function fnResetForm()
	{
		reset_form('slittingsqueezing_1','','','','$(\'#list_fabric_desc_container tr:not(:first)\').remove();');
	}
	function scan_batchnumber(str)
	{
	var cbo_company_id = $('#cbo_company_id').val();
	var response=return_global_ajax_value( cbo_company_id+"**"+str, 'check_batch_no_scan', '', 'requires/slitting_squeezing_controller');
	var response=response.split("_");
	if(response[0]==0)
		{
			$('#txt_batch_no').val('');
			$('#hidden_batch_id').val(''); 
			//$('#cbo_company_id').val(''); 
			$('#txt_update_id').val(''); 
			$('#cbo_sub_process').val('');
			$('#txt_process_end_date').val('');
			$('#txt_end_hours').val('');
			$('#txt_end_minutes').val('');
			$('#cbo_machine_name').val('');
			$('#txt_remarks').val('');
			reset_form('slittingsqueezing_1','','','','$(\'#list_fabric_desc_container tr:not(:first)\').remove();');
		}
	else
		{
	$('#hidden_batch_id').val(response[1]);
	get_php_form_data(response[1]+'_'+str, "populate_data_from_batch", "requires/slitting_squeezing_controller" );
	show_list_view(response[1],'show_fabric_desc_listview','list_fabric_desc_container','requires/slitting_squeezing_controller','');
	get_php_form_data(document.getElementById('cbo_company_id').value+'**'+document.getElementById('cbo_floor').value+'**'+document.getElementById('cbo_machine_name').value, 'populate_data_from_machine', 'requires/slitting_squeezing_controller' );
	$('#txt_batch_no').focus();
		}
	}
$('#txt_batch_no').live('keydown', function(e) {
	
    if (e.keyCode === 13) {
        e.preventDefault();
		 scan_batchnumber(this.value); 
    }
});

function openmypage_process()
	{
		var txt_process_id = $('#txt_process_id').val();
		var title = 'Process Name Selection Form';	
		var page_link = 'requires/slitting_squeezing_controller.php?txt_process_id='+txt_process_id+'&action=process_name_popup';
		emailwindow=dhtmlmodal.open('EmailBox', 'iframe', page_link, title, 'width=400px,height=370px,center=1,resize=1,scrolling=0','../');
		emailwindow.onclose=function()
		{
		var theform=this.contentDoc.forms[0]//("search_order_frm"); //Access the form inside the modal window
		var process_id=this.contentDoc.getElementById("hidden_process_id").value;	 //Access form field with id="emailfield"
		var process_name=this.contentDoc.getElementById("hidden_process_name").value;
		$('#txt_process_id').val(process_id);
		$('#txt_process_name').val(process_name);
		}
	}	
</script>
</head>
<body onLoad="set_hotkey();$('#txt_batch_no').focus();">
<div style="width:100%;" align="center">
<?php echo load_freeze_divs ("../../",$permission); ?>
    <form name="slittingsqueezing_1" id="slittingsqueezing_1" autocomplete="off" >
    <div style="width:1300px; float:left;">   
        <fieldset style="width:1250px;">
        <table cellpadding="0" cellspacing="1" width="1250" border="0" align="left">
            <tr>
                <td width="29%" valign="top">
                    <fieldset>
                    <legend>Input Area</legend>
                    <table cellpadding="0" cellspacing="2" width="100%" id="main_tbl">
                        <tr> 
                            <td width="" class="must_entry_caption">Batch No.</td>
                            <td>
                                <input type="text" name="txt_batch_no" id="txt_batch_no" class="text_boxes" style="width:122px;" placeholder="Write/Browse" maxlength="20" title="Maximum 20 Character" onDblClick="openmypage_batchnum();" onChange="check_batch();" />
                                <input type="hidden" name="hidden_batch_id" id="hidden_batch_id" class="text_boxes" readonly />
                               <!-- <input type="hidden" name="txt_ext_id" id="txt_ext_id" style="width:100px;" class="text_boxes" readonly />-->
                            </td>
                        </tr>
                        <tr>
                         <td class="must_entry_caption" width="130">Company</td>
                            <td>
                                <?php
                                    echo create_drop_down( "cbo_company_id", 135, "select comp.id, comp.company_name from lib_company comp where comp.status_active=1 and comp.is_deleted=0 $company_cond order by comp.company_name","id,company_name", 1, "--Select Company--", $selected, "load_drop_down('requires/slitting_squeezing_controller', this.value, 'load_drop_floor', 'floor_td' );" );
                                ?>
                                <input type="hidden" name="txt_update_id" id="txt_update_id" style="width:100px;" class="text_boxes" readonly />
                            </td>
                        </tr>
                        <tr>
                            <td class="">Process </td>
                            <td>
                               <?php
								   echo create_drop_down( "cbo_sub_process", 135, $conversion_cost_head_array,"", 0, "-- Select --", 0, "","","63" );
                               ?>
                                <!--<input type="text" name="txt_process_name" id="txt_process_name" class="text_boxes" style="width:122px;" placeholder="Double Click To Search" onDblClick="openmypage_process();" readonly />
                            <input type="hidden" name="txt_process_id" id="txt_process_id" />-->
                            </td>
                        </tr>
                        <tr>
                            <td class="must_entry_caption">Production Date</td>
                            <td>
                                <input type="text" name="txt_process_end_date" id="txt_process_end_date" class="datepicker" style="width:122px;" readonly/>
                            </td>
                        </tr>
                        <tr>
                            <td class="must_entry_caption">Process Start Date</td>
                            <td>
                                <input type="text" name="txt_process_start_date" id="txt_process_start_date" class="datepicker" style="width:122px;" readonly/>
                            </td>
                        </tr>
                        <tr>
                            <td class="must_entry_caption">Process Start Time</td>
                            <td>
                                 <input type="text" name="txt_start_hours" id="txt_start_hours" class="text_boxes_numeric" placeholder="Hours" style="width:50px;" onKeyUp="fnc_move_cursor(this.value,'txt_start_hours','txt_start_minutes',2,23)" />
                                <input type="text" name="txt_start_minutes" id="txt_start_minutes" class="text_boxes_numeric" placeholder="Minutes" style="width:50px;" onKeyUp="fnc_move_cursor(this.value,'txt_start_minutes','txt_end_date',2,59)" />
                            </td>
                        </tr>
                        <tr>
                            <td class="must_entry_caption">Process End Date</td>
                            <td>
                                <input type="text" name="txt_process_date" id="txt_process_date" class="datepicker" style="width:122px;" readonly/>
                            </td>
                        </tr>
                        <tr>
                            <td class="must_entry_caption">Process End Time</td>
                            <td>
                                 <input type="text" name="txt_end_hours" id="txt_end_hours" class="text_boxes_numeric" placeholder="Hours" style="width:50px;" onKeyUp="fnc_move_cursor(this.value,'txt_end_hours','txt_end_minutes',2,23)" />
                                <input type="text" name="txt_end_minutes" id="txt_end_minutes" class="text_boxes_numeric" placeholder="Minutes" style="width:50px;" onKeyUp="fnc_move_cursor(this.value,'txt_end_minutes','txt_end_date',2,59)" />
                            </td>
                        </tr>
                           <td>Floor</td>
                            <td id="floor_td">
								<?php
									 echo create_drop_down( "cbo_floor", 135, "select id,floor_name from lib_prod_floor where status_active=1 and is_deleted=0 order by floor_name limit 0 ,0","id,floor_name", 1, "-- Select Floor --", $selected, "",0,"","","","",4 );
                                ?>
                            </td>
                        <tr>
                        <tr>
                            <td>Machine Name</td>
                            <td id="machine_td">
								<?php
									echo create_drop_down("cbo_machine_name", 135, $blank_array,"", 1, "-- Select Machine --", 0, "",0 ); 
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Shift Name</td>
                            <td>
								<?php
									echo create_drop_down("cbo_shift_name", 135, $shift_name,"", 1, "-- Select Shift --", 0, "",0 ,"","","","",""); 
                                ?>
                            </td>
                        </tr>
                    </table>
                    </fieldset>
                </td>
                <td width="1%" valign="top">&nbsp;</td>
                <td width="70%" valign="top">
                    <table cellpadding="0" cellspacing="1" width="100%" border="0" align="left">
                        <tr>
                            <td colspan="3"> <center> <legend>Reference Display</legend></center> </td>
                        </tr>
                        <tr>
                            <td width="45%" valign="top">
                                <fieldset>
                                    <table width="400" align="left" id="tbl_body1">
                                        <tr>
                                            <td width="70">Batch ID</td>
                                            <td width="110">
                                                <input type="text" name="txt_batch_ID" id="txt_batch_ID" class="text_boxes" style="width:100px;" readonly />
                                                
                                            </td>
                                            <td width="90">Dyeing Start</td>
                                            <td width="110">
                                                <input type="text" name="txt_dying_started" id="txt_dying_started" class="text_boxes" style="width:100px;" readonly  />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Color</td>
                                            <td>
                                                <input type="text" name="txt_color" id="txt_color" class="text_boxes" style="width:100px;"  readonly />
                                            </td>
                                            <td >Dyeing End</td>
                                            <td>
                                                <input type="text" name="txt_dying_end" id="txt_dying_end" class="text_boxes" style="width:100px;" readonly  />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Job No</td>
                                            <td>
                                                <input type="text" name="txt_job_no" id="txt_job_no" class="text_boxes" style="width:100px;"  readonly />
                                            </td>
                                            <td >M/C Floor</td>
                                            <td id="machine_fg_td">
                                                <input type="text" name="txt_machine_no" id="txt_machine_no" class="text_boxes" style="width:100px;" readonly />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td >Buyer</td>
                                            <td>
                                                <input type="text" name="txt_buyer" id="txt_buyer" class="text_boxes" style="width:100px;"  readonly />
                                            </td>
                                            <td >M/C Group</td>
                                            <td id="">
                                               <input type="text" name="txt_mc_group" id="txt_mc_group" class="text_boxes" style="width:100px;" value="<?php echo $data;?>" readonly />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Order No.</td>
                                            <td>
                                                <input type="text" name="txt_order_no" id="txt_order_no" class="text_boxes" style="width:100px;" readonly />
                                            </td>
                                             <td>Extn.No.</td>
                                            <td>
                                                 <input type="text" name="txt_ext_id" id="txt_ext_id" style="width:100px;" class="text_boxes" readonly />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3"> <div id="batch_type" style="color:#F00"> </div> </td>
                                        </tr>
                                    </table>
                                </fieldset>
                              </td>
                            <td width="1%" valign="top">&nbsp;</td>
                            <td width="54%" valign="top">
                                <fieldset>
                                     <table width="513" align="right" class="rpt_table" rules="all" id="tbl_item_details">
                                      <caption> <legend> Fabric Details </legend></caption>
                                        <thead>
                                            <th>Const/Composition</th> 
                                            <th>GSM</th>
                                            <th>Dia/Width</th> 
                                            <th>D/W Type</th>
                                            <th>Batch Qty</th>
                                        </thead>
                                        <tbody id="list_fabric_desc_container">
                                            <tr class="general" id="row_1">
                                                <td><input type="text" name="txtconscomp_1" id="txtconscomp_1" class="text_boxes" style="width:170px;" readonly disabled /></td>
                                                <td><input type="text" name="txtgsm_1" id="txtgsm_1" class="text_boxes_numeric" style="width:60px;" /> </td>
                                                <td><input type="text" name="txtbodypart_1" id="txtbodypart_1" class="text_boxes" style="width:110px;" readonly  disabled/></td>
                                                <td><input type="text" name="txtdiawidth_1" id="txtdiawidth_1" class="text_boxes" style="width:70px;" readonly disabled/>
                                                 <input type="hidden" name="txtdiawidthID_1" id="txtdiawidthID_1" readonly />
                                                </td>
                                                <td><input type="text" name="txtbatchqnty_1" id="txtbatchqnty_1" class="text_boxes" style="width:80px;" readonly disabled/>
                                                 <input type="hidden" name="txtprodid_1" id="txtprodid_1" />
                                                 <input type="hidden" name="updateiddtls_1" id="updateiddtls_1" class="text_boxes" readonly />
                                                </td>
                                            </tr>
                                             <tr>
                                                <td colspan="4" align="right"><b>Sum:</b> <?php //echo $b_qty; ?> </td>
                                                <td align="right"></td>
                                            </tr>
                                        </tbody>
                                  </table>
                               </fieldset>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <fieldset style="width:610px;">
                        <table style="width:610">
                            <tr>
                                <td width="100">Remarks:</td>
                                <td>
                                    <input type="text" name="txt_remarks" id="txt_remarks" class="text_boxes" style="width:610px;"  />
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
                <td align="right">&nbsp;</td>
            </tr>
            <tr>
                <td align="center" colspan="4" class="button_container">
                    <?php
                        echo load_submit_buttons($permission, "fnc_pro_fab_subprocess", 0,0,"fnResetForm()",1);
                    ?>
                </td>
            </tr>
        </table>
        </fieldset>
        </div>
	</form>
</div>
</body>
<!--<script> set_multiselect('cbo_sub_process','0','0','','0'); </script>-->
<script src="../../includes/functions_bottom.js" type="text/javascript"></script>
</html>